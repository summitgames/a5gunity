﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.ContextMenuItemAttribute
struct ContextMenuItemAttribute_t5DA92E8ADD9F600E72E7051B37DAC8EB7DDAA4E9;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// System.Xml.Serialization.XmlArrayAttribute
struct XmlArrayAttribute_t8016E5E39EDDEC98A198E56BBAA467772E8AE472;
// System.Xml.Serialization.XmlArrayItemAttribute
struct XmlArrayItemAttribute_tADA59D11B0D5E8CF01A1410D957B873D45F2FC94;
// System.Xml.Serialization.XmlAttributeAttribute
struct XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647;
// System.Xml.Serialization.XmlElementAttribute
struct XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94;
// System.Xml.Serialization.XmlRootAttribute
struct XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96;

IL2CPP_EXTERN_C const RuntimeType* U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetConfigU3Ed__1_1_t27C2E6BD21131A846C1ED0873201EA1DF5599309_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRefreshYieldU3Ed__129_tBF0CB254F9A178BECCDC29B113049D5A390DB47A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CU3CTestDownloadSpeedU3Eb__7_0U3Ed_t5139BC4222B7584C9C6791078DF2E66199658524_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CU3CTestSpeedU3Eb__0U3Ed_t6E992DBE722759CCEA52EFC4D5A3FD4B519C2C47_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CU3CTestUploadSpeedU3Eb__0U3Ed_tDCD2DA70E4DBCAD1F844D600CD63CC92E027EFEF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cgoogle204CheckU3Ed__132_t45EF56476E3EE3D120644CC1647818DEDA335989_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CgoogleBlankCheckU3Ed__133_t4FAE0B9E4E059EE4C8578C9BA9F0AD5742CF648F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CinternetCheckU3Ed__136_t88AFCE419C8A3E7E686E64624EEDAD6C0BD02CD0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CpingU3Ed__41_t6828EFFEDDA314F733151F7CB92A7C4D6B15BC21_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CstartWorkerU3Ed__135_t0692C6D98709C1ECE5AB38F4FA78CF8FA0FA1748_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CtestU3Ed__49_tDD0B93F803E5724477C153898A710AFD7F7DC0A3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CtestU3Ed__57_t39BF4B4A0B29F9833FB294F4058427265F922159_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CwwwCheckU3Ed__131_tDC196BCEE13348B800156D6ECEBDA9ADC921569E_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_oldName_0), (void*)value);
	}
};


// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.HelpURLAttribute::m_Url
	String_t* ___m_Url_0;
	// System.Boolean UnityEngine.HelpURLAttribute::m_Dispatcher
	bool ___m_Dispatcher_1;
	// System.String UnityEngine.HelpURLAttribute::m_DispatchingFieldName
	String_t* ___m_DispatchingFieldName_2;

public:
	inline static int32_t get_offset_of_m_Url_0() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Url_0)); }
	inline String_t* get_m_Url_0() const { return ___m_Url_0; }
	inline String_t** get_address_of_m_Url_0() { return &___m_Url_0; }
	inline void set_m_Url_0(String_t* value)
	{
		___m_Url_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Url_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Dispatcher_1() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Dispatcher_1)); }
	inline bool get_m_Dispatcher_1() const { return ___m_Dispatcher_1; }
	inline bool* get_address_of_m_Dispatcher_1() { return &___m_Dispatcher_1; }
	inline void set_m_Dispatcher_1(bool value)
	{
		___m_Dispatcher_1 = value;
	}

	inline static int32_t get_offset_of_m_DispatchingFieldName_2() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_DispatchingFieldName_2)); }
	inline String_t* get_m_DispatchingFieldName_2() const { return ___m_DispatchingFieldName_2; }
	inline String_t** get_address_of_m_DispatchingFieldName_2() { return &___m_DispatchingFieldName_2; }
	inline void set_m_DispatchingFieldName_2(String_t* value)
	{
		___m_DispatchingFieldName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DispatchingFieldName_2), (void*)value);
	}
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Xml.Serialization.XmlRootAttribute
struct XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Xml.Serialization.XmlRootAttribute::elementName
	String_t* ___elementName_0;
	// System.Boolean System.Xml.Serialization.XmlRootAttribute::isNullable
	bool ___isNullable_1;
	// System.String System.Xml.Serialization.XmlRootAttribute::ns
	String_t* ___ns_2;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___elementName_0), (void*)value);
	}

	inline static int32_t get_offset_of_isNullable_1() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96, ___isNullable_1)); }
	inline bool get_isNullable_1() const { return ___isNullable_1; }
	inline bool* get_address_of_isNullable_1() { return &___isNullable_1; }
	inline void set_isNullable_1(bool value)
	{
		___isNullable_1 = value;
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ns_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ContextMenuItemAttribute
struct ContextMenuItemAttribute_t5DA92E8ADD9F600E72E7051B37DAC8EB7DDAA4E9  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.ContextMenuItemAttribute::name
	String_t* ___name_0;
	// System.String UnityEngine.ContextMenuItemAttribute::function
	String_t* ___function_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ContextMenuItemAttribute_t5DA92E8ADD9F600E72E7051B37DAC8EB7DDAA4E9, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_function_1() { return static_cast<int32_t>(offsetof(ContextMenuItemAttribute_t5DA92E8ADD9F600E72E7051B37DAC8EB7DDAA4E9, ___function_1)); }
	inline String_t* get_function_1() const { return ___function_1; }
	inline String_t** get_address_of_function_1() { return &___function_1; }
	inline void set_function_1(String_t* value)
	{
		___function_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___function_1), (void*)value);
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// UnityEngine.RuntimeInitializeLoadType
struct RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5 
{
public:
	// System.Int32 UnityEngine.RuntimeInitializeLoadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Xml.Schema.XmlSchemaForm
struct XmlSchemaForm_tAFA037CC98C760F6686439643387677185EC9D91 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaForm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaForm_tAFA037CC98C760F6686439643387677185EC9D91, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D  : public PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948
{
public:
	// UnityEngine.RuntimeInitializeLoadType UnityEngine.RuntimeInitializeOnLoadMethodAttribute::m_LoadType
	int32_t ___m_LoadType_0;

public:
	inline static int32_t get_offset_of_m_LoadType_0() { return static_cast<int32_t>(offsetof(RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D, ___m_LoadType_0)); }
	inline int32_t get_m_LoadType_0() const { return ___m_LoadType_0; }
	inline int32_t* get_address_of_m_LoadType_0() { return &___m_LoadType_0; }
	inline void set_m_LoadType_0(int32_t value)
	{
		___m_LoadType_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.Xml.Serialization.XmlArrayAttribute
struct XmlArrayAttribute_t8016E5E39EDDEC98A198E56BBAA467772E8AE472  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Xml.Serialization.XmlArrayAttribute::elementName
	String_t* ___elementName_0;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Serialization.XmlArrayAttribute::form
	int32_t ___form_1;
	// System.Boolean System.Xml.Serialization.XmlArrayAttribute::isNullable
	bool ___isNullable_2;
	// System.String System.Xml.Serialization.XmlArrayAttribute::ns
	String_t* ___ns_3;
	// System.Int32 System.Xml.Serialization.XmlArrayAttribute::order
	int32_t ___order_4;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlArrayAttribute_t8016E5E39EDDEC98A198E56BBAA467772E8AE472, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___elementName_0), (void*)value);
	}

	inline static int32_t get_offset_of_form_1() { return static_cast<int32_t>(offsetof(XmlArrayAttribute_t8016E5E39EDDEC98A198E56BBAA467772E8AE472, ___form_1)); }
	inline int32_t get_form_1() const { return ___form_1; }
	inline int32_t* get_address_of_form_1() { return &___form_1; }
	inline void set_form_1(int32_t value)
	{
		___form_1 = value;
	}

	inline static int32_t get_offset_of_isNullable_2() { return static_cast<int32_t>(offsetof(XmlArrayAttribute_t8016E5E39EDDEC98A198E56BBAA467772E8AE472, ___isNullable_2)); }
	inline bool get_isNullable_2() const { return ___isNullable_2; }
	inline bool* get_address_of_isNullable_2() { return &___isNullable_2; }
	inline void set_isNullable_2(bool value)
	{
		___isNullable_2 = value;
	}

	inline static int32_t get_offset_of_ns_3() { return static_cast<int32_t>(offsetof(XmlArrayAttribute_t8016E5E39EDDEC98A198E56BBAA467772E8AE472, ___ns_3)); }
	inline String_t* get_ns_3() const { return ___ns_3; }
	inline String_t** get_address_of_ns_3() { return &___ns_3; }
	inline void set_ns_3(String_t* value)
	{
		___ns_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ns_3), (void*)value);
	}

	inline static int32_t get_offset_of_order_4() { return static_cast<int32_t>(offsetof(XmlArrayAttribute_t8016E5E39EDDEC98A198E56BBAA467772E8AE472, ___order_4)); }
	inline int32_t get_order_4() const { return ___order_4; }
	inline int32_t* get_address_of_order_4() { return &___order_4; }
	inline void set_order_4(int32_t value)
	{
		___order_4 = value;
	}
};


// System.Xml.Serialization.XmlArrayItemAttribute
struct XmlArrayItemAttribute_tADA59D11B0D5E8CF01A1410D957B873D45F2FC94  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Xml.Serialization.XmlArrayItemAttribute::dataType
	String_t* ___dataType_0;
	// System.String System.Xml.Serialization.XmlArrayItemAttribute::elementName
	String_t* ___elementName_1;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Serialization.XmlArrayItemAttribute::form
	int32_t ___form_2;
	// System.String System.Xml.Serialization.XmlArrayItemAttribute::ns
	String_t* ___ns_3;
	// System.Boolean System.Xml.Serialization.XmlArrayItemAttribute::isNullable
	bool ___isNullable_4;
	// System.Boolean System.Xml.Serialization.XmlArrayItemAttribute::isNullableSpecified
	bool ___isNullableSpecified_5;
	// System.Int32 System.Xml.Serialization.XmlArrayItemAttribute::nestingLevel
	int32_t ___nestingLevel_6;
	// System.Type System.Xml.Serialization.XmlArrayItemAttribute::type
	Type_t * ___type_7;

public:
	inline static int32_t get_offset_of_dataType_0() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_tADA59D11B0D5E8CF01A1410D957B873D45F2FC94, ___dataType_0)); }
	inline String_t* get_dataType_0() const { return ___dataType_0; }
	inline String_t** get_address_of_dataType_0() { return &___dataType_0; }
	inline void set_dataType_0(String_t* value)
	{
		___dataType_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataType_0), (void*)value);
	}

	inline static int32_t get_offset_of_elementName_1() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_tADA59D11B0D5E8CF01A1410D957B873D45F2FC94, ___elementName_1)); }
	inline String_t* get_elementName_1() const { return ___elementName_1; }
	inline String_t** get_address_of_elementName_1() { return &___elementName_1; }
	inline void set_elementName_1(String_t* value)
	{
		___elementName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___elementName_1), (void*)value);
	}

	inline static int32_t get_offset_of_form_2() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_tADA59D11B0D5E8CF01A1410D957B873D45F2FC94, ___form_2)); }
	inline int32_t get_form_2() const { return ___form_2; }
	inline int32_t* get_address_of_form_2() { return &___form_2; }
	inline void set_form_2(int32_t value)
	{
		___form_2 = value;
	}

	inline static int32_t get_offset_of_ns_3() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_tADA59D11B0D5E8CF01A1410D957B873D45F2FC94, ___ns_3)); }
	inline String_t* get_ns_3() const { return ___ns_3; }
	inline String_t** get_address_of_ns_3() { return &___ns_3; }
	inline void set_ns_3(String_t* value)
	{
		___ns_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ns_3), (void*)value);
	}

	inline static int32_t get_offset_of_isNullable_4() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_tADA59D11B0D5E8CF01A1410D957B873D45F2FC94, ___isNullable_4)); }
	inline bool get_isNullable_4() const { return ___isNullable_4; }
	inline bool* get_address_of_isNullable_4() { return &___isNullable_4; }
	inline void set_isNullable_4(bool value)
	{
		___isNullable_4 = value;
	}

	inline static int32_t get_offset_of_isNullableSpecified_5() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_tADA59D11B0D5E8CF01A1410D957B873D45F2FC94, ___isNullableSpecified_5)); }
	inline bool get_isNullableSpecified_5() const { return ___isNullableSpecified_5; }
	inline bool* get_address_of_isNullableSpecified_5() { return &___isNullableSpecified_5; }
	inline void set_isNullableSpecified_5(bool value)
	{
		___isNullableSpecified_5 = value;
	}

	inline static int32_t get_offset_of_nestingLevel_6() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_tADA59D11B0D5E8CF01A1410D957B873D45F2FC94, ___nestingLevel_6)); }
	inline int32_t get_nestingLevel_6() const { return ___nestingLevel_6; }
	inline int32_t* get_address_of_nestingLevel_6() { return &___nestingLevel_6; }
	inline void set_nestingLevel_6(int32_t value)
	{
		___nestingLevel_6 = value;
	}

	inline static int32_t get_offset_of_type_7() { return static_cast<int32_t>(offsetof(XmlArrayItemAttribute_tADA59D11B0D5E8CF01A1410D957B873D45F2FC94, ___type_7)); }
	inline Type_t * get_type_7() const { return ___type_7; }
	inline Type_t ** get_address_of_type_7() { return &___type_7; }
	inline void set_type_7(Type_t * value)
	{
		___type_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___type_7), (void*)value);
	}
};


// System.Xml.Serialization.XmlAttributeAttribute
struct XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Xml.Serialization.XmlAttributeAttribute::attributeName
	String_t* ___attributeName_0;
	// System.String System.Xml.Serialization.XmlAttributeAttribute::dataType
	String_t* ___dataType_1;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Serialization.XmlAttributeAttribute::form
	int32_t ___form_2;
	// System.String System.Xml.Serialization.XmlAttributeAttribute::ns
	String_t* ___ns_3;

public:
	inline static int32_t get_offset_of_attributeName_0() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647, ___attributeName_0)); }
	inline String_t* get_attributeName_0() const { return ___attributeName_0; }
	inline String_t** get_address_of_attributeName_0() { return &___attributeName_0; }
	inline void set_attributeName_0(String_t* value)
	{
		___attributeName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___attributeName_0), (void*)value);
	}

	inline static int32_t get_offset_of_dataType_1() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647, ___dataType_1)); }
	inline String_t* get_dataType_1() const { return ___dataType_1; }
	inline String_t** get_address_of_dataType_1() { return &___dataType_1; }
	inline void set_dataType_1(String_t* value)
	{
		___dataType_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataType_1), (void*)value);
	}

	inline static int32_t get_offset_of_form_2() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647, ___form_2)); }
	inline int32_t get_form_2() const { return ___form_2; }
	inline int32_t* get_address_of_form_2() { return &___form_2; }
	inline void set_form_2(int32_t value)
	{
		___form_2 = value;
	}

	inline static int32_t get_offset_of_ns_3() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647, ___ns_3)); }
	inline String_t* get_ns_3() const { return ___ns_3; }
	inline String_t** get_address_of_ns_3() { return &___ns_3; }
	inline void set_ns_3(String_t* value)
	{
		___ns_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ns_3), (void*)value);
	}
};


// System.Xml.Serialization.XmlElementAttribute
struct XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Xml.Serialization.XmlElementAttribute::dataType
	String_t* ___dataType_0;
	// System.String System.Xml.Serialization.XmlElementAttribute::elementName
	String_t* ___elementName_1;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Serialization.XmlElementAttribute::form
	int32_t ___form_2;
	// System.String System.Xml.Serialization.XmlElementAttribute::ns
	String_t* ___ns_3;
	// System.Boolean System.Xml.Serialization.XmlElementAttribute::isNullable
	bool ___isNullable_4;
	// System.Type System.Xml.Serialization.XmlElementAttribute::type
	Type_t * ___type_5;
	// System.Int32 System.Xml.Serialization.XmlElementAttribute::order
	int32_t ___order_6;

public:
	inline static int32_t get_offset_of_dataType_0() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94, ___dataType_0)); }
	inline String_t* get_dataType_0() const { return ___dataType_0; }
	inline String_t** get_address_of_dataType_0() { return &___dataType_0; }
	inline void set_dataType_0(String_t* value)
	{
		___dataType_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataType_0), (void*)value);
	}

	inline static int32_t get_offset_of_elementName_1() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94, ___elementName_1)); }
	inline String_t* get_elementName_1() const { return ___elementName_1; }
	inline String_t** get_address_of_elementName_1() { return &___elementName_1; }
	inline void set_elementName_1(String_t* value)
	{
		___elementName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___elementName_1), (void*)value);
	}

	inline static int32_t get_offset_of_form_2() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94, ___form_2)); }
	inline int32_t get_form_2() const { return ___form_2; }
	inline int32_t* get_address_of_form_2() { return &___form_2; }
	inline void set_form_2(int32_t value)
	{
		___form_2 = value;
	}

	inline static int32_t get_offset_of_ns_3() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94, ___ns_3)); }
	inline String_t* get_ns_3() const { return ___ns_3; }
	inline String_t** get_address_of_ns_3() { return &___ns_3; }
	inline void set_ns_3(String_t* value)
	{
		___ns_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ns_3), (void*)value);
	}

	inline static int32_t get_offset_of_isNullable_4() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94, ___isNullable_4)); }
	inline bool get_isNullable_4() const { return ___isNullable_4; }
	inline bool* get_address_of_isNullable_4() { return &___isNullable_4; }
	inline void set_isNullable_4(bool value)
	{
		___isNullable_4 = value;
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94, ___type_5)); }
	inline Type_t * get_type_5() const { return ___type_5; }
	inline Type_t ** get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(Type_t * value)
	{
		___type_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___type_5), (void*)value);
	}

	inline static int32_t get_offset_of_order_6() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94, ___order_6)); }
	inline int32_t get_order_6() const { return ___order_6; }
	inline int32_t* get_address_of_order_6() { return &___order_6; }
	inline void set_order_6(int32_t value)
	{
		___order_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HelpURLAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215 (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * __this, String_t* ___url0, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * __this, String_t* ___oldName0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.ContextMenuItemAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContextMenuItemAttribute__ctor_m8673BFA9F81B0C8EFD26A07B5705A0650393FF52 (ContextMenuItemAttribute_t5DA92E8ADD9F600E72E7051B37DAC8EB7DDAA4E9 * __this, String_t* ___name0, String_t* ___function1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516 (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * __this, int32_t ___loadType0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_order(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Xml.Serialization.XmlRootAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlRootAttribute__ctor_mFF794C36E64D16871DECFD78A0344FDCCBAA9E39 (XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 * __this, String_t* ___elementName0, const RuntimeMethod* method);
// System.Void System.Xml.Serialization.XmlAttributeAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749 (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * __this, String_t* ___attributeName0, const RuntimeMethod* method);
// System.Void System.Xml.Serialization.XmlArrayAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlArrayAttribute__ctor_m8A5E563C98D551E290DC924920D36305D9C46A21 (XmlArrayAttribute_t8016E5E39EDDEC98A198E56BBAA467772E8AE472 * __this, String_t* ___elementName0, const RuntimeMethod* method);
// System.Void System.Xml.Serialization.XmlArrayItemAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlArrayItemAttribute__ctor_m49D7F51DCCCC0F5FAC62C017A93796693F5BF8E2 (XmlArrayItemAttribute_tADA59D11B0D5E8CF01A1410D957B873D45F2FC94 * __this, String_t* ___elementName0, const RuntimeMethod* method);
// System.Void System.Xml.Serialization.XmlElementAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlElementAttribute__ctor_m434F875F0651AD028C2EE19262CA0576A98B8381 (XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94 * __this, String_t* ___elementName0, const RuntimeMethod* method);
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeInitializeOnLoadMethodAttribute__ctor_mAEDC96FCA281601682E7207BD386A1553C1B6081 (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * __this, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharpU2Dfirstpass_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void U3CU3Ec_t5DC7F6288A2155D9D8A64DCBF4B08ED1E18371DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToTitleCase_m0FAF40693A916D1BA985195E30AA7B9753C11DE2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTReverse_m375B1B5F94367BC73BC054481C4528E1829758A0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTReplace_mE45837F5BF2ECDD64251F7DC85303D36308AFBB9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTEquals_m70FD226B78B4DD7ED0672F326B16568EA3D9B446(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTContains_m52B3F3D1019BBD1746371264A2671656CDADB3C2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTContainsAny_m50A29F87C6A50663D9B599FEA1D1359EC07969A3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTContainsAll_mAB9F9024F8E76F4449FE666602718634956C2464(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTRemoveNewLines_mF3F6F1FD9F609208AF748156579F02AB68B15C24(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTAddNewLines_m3FDE4E74DB180C8F829329BFFD4934CB93057581(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTisNumeric_mB664DBA85A056E3855D912C93DA7B704584752DC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTisInteger_m9A0F9A80C8B5CDADCD3500534A9FF53271BC5EA3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTisEmail_m2336B119F8D08E08DF5335FEAC1699978FC6405F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTisWebsite_m7DC7369502F9A000269EEC781ABA71452667E85B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTisCreditcard_m62CDA4DAC05FE6307258BB4626FF76CF939C3C78(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTisIPv4_mA58907D12ED35BC0D4A3277E8475EF095B440126(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTisAlphanumeric_m2E1A017EEB60FF2F1E2391C0074E81B406A98A2A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CThasLineEndings_m95A836BD2BE4FFFFD877D5FD894AADF33428A129(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CThasInvalidChars_m3C830A06687FDE6DE20154B64E2E4D531B7A9ED4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTStartsWith_mFCFFAAF1BEE786BD337B3B2296D32744AB0A2542(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTEndsWith_m666D85A99AD0967EBD41A838F37819D298D23ADF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTLastIndexOf_m2F16141C163B099C5BCDF1D4E3426834EAD6A863(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTIndexOf_m3C965AE80FE82E5E12EC02B9019CF48B1D16B1C5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTIndexOf_mD1B263B1E864EE567DD4C711597BA5556B0657E3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToBase64_m95D893BE1209C12BFC96FDCEA54371C1741ED35B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFromBase64_m5018159F11AAAD7AAE89A93D795932112BB18D1C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFromBase64ToByteArray_m66CADB7E423C7A554BB4B0069FCD5665402C67DE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToHex_mE0934C22838185424921F80774725E4F5CD5FEED(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTHexToString_m93D0EABF83200F514CC04FEAB3036266634B67B9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTHexToColor32_mD1A93B26BC2D250FEA2834700CAC106519A62771(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTHexToColor_m1FD7E347868D5E6A6450C36DF93DABAE02C468AC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToByteArray_m8BBB5E420285A72B5F964FF51944FB972837AE8A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTClearTags_m6F0896B414261998FD998D185D75F03B3151875E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTClearSpaces_m39BCF0466C31347D3DB7BAF627B2AF586CCC11B3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTClearLineEndings_m923BE72DD4DC59DB6307F90F2ABC30A90DBB692D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTShuffle_m69617262C775678CA0CFFF468C20E275ECABD211(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m42F2A1DBC9450A01E998ADE7347D234875962FFD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m9372AF9431740E725E0A0EE720EEB4B51EF8D626(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mDD2831261D66781D294ADBFFBD9A397D4A5B4681(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m2A792DFFEECEFF18E99A289916B9165C127E7883(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mEDCBC052999C7F88ECF5897E865B6467FF1F452E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToString_m181115D9882BBB5E201C31443FCFA49DBADE6D30(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToFloatArray_m7E2B709CDA0C352394C296884A120A4FFB1576A3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToByteArray_m11AEF2E58D65C64EF7BBE114385999E73726E236(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToTexture_m12DC9C6DC82C07A891F5EA4AC628DEBB737C5DB2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToSprite_mD7FF7050F7C0BE3EE55C86A445CF3559C0D07BA3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToString_mBAB4670FE933D06A3F39F51219830F8D3CA946F5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToBase64_m0E9D5F1F9174FAA4776F788ECA88A8709A97BCE5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTShuffle_mB67EA3011C43B440B75C4B90B8719323BEE020E4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m9E61BE84CB474608968636B8A40B1C38179FC784(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m59F8DE0A578B3B891B4D8C1ED38688E86940F976(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m6E14577C017276ACD5DA587C029A664694E859C9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mD3A9D8E4C2FAF2BE628602C89238233DBE170FDE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mB00AF342A1C001008DDEE5422E6CE9386ADF044C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToString_m21B12643EEC617E394A1A8C5DB57D3DA63EE0A36(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mE3C3EB3222E33AA52D07C720D926EF8D29F6F21B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTAddRange_mA8E3A743EA575918A5B29284CB5CEDD965D88261(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTReadFully_mCD211B8D82B74DE675FB131FBAC880B034B7B553(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToHexRGB_mEB271EF7DE0D38158B4D1C9E1F5A2DCAD7B97502(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToHexRGB_m1E39198A3F7FD8F35CC5BD27984C09FD5CDFEC5A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToHexRGBA_m1E583B32CC5214340E8C18EA005281D8748D689B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToHexRGBA_m8BF2160A9E4353077DBDE0F048F2B4000FC877EF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTVector3_mCBBA0D200DA6E33DFDF5F968F64682AF279A1F72(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTVector3_m8CFAC4C85BA2694A83E2BDFB3A43B317005BDFE6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTVector4_m4CA428D976ADBCA1876085C9942F953F90DC6592(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTVector4_m3E12FD9E6E3310BE328E10AB105D3ADCFFF49522(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTMultiply_m305DB38572F0613C1602758541DE685CD229B168(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTMultiply_mFF0ED38A9DA8F4B6A0D650560279442844DEA6AE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFlatten_mC4047C6959F5B6BC261F88F4790682FCFC383CC1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTQuaternion_m462BF09597B2909DB2D1128548CB3B1E3D0319A1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTColorRGB_m39E122A470F7CAA46A440D03B2DDB4AB1D8B302B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTMultiply_m5C8CA96F97B488BC1E1DD14854E6603037247CFB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTQuaternion_m4A9E9712734553E922C7789A7051CF06A384A0B0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTColorRGBA_m0423B016C705D61B4186C44E18B5E4A168598A7A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTVector3_m1F8621C9295026E750D0342405B2118B874F52E4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTVector4_m1F9C3149D5060CED2F9DFFF8A3CB39E95ABC5729(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTCorrectLossyScale_m8B4CB2FCAA60A49AEA8FBA793EE49D69AA7B11F7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetLocalCorners_m866B0CC7B69D7A4E17E65B2D870CBED28F9C7307(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetLocalCorners_m93E67F2D92BA9C9F54D41CA6769ADE481D9208AF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetScreenCorners_m2F1E5E3409D23E804EB07803B1317339FDF28B87(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetScreenCorners_m39B94363A1BB9CAA84AC5F4D7C6F652FA07D35E0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetBounds_m2CB978298B520DEDC083F4E6F763D89534850569(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTSetLeft_m7241C29911EB32D2BF73B4F20ED2F0B8E1E465DF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTSetRight_m7B0B7371C3243430FE05D24A4EC4E466F53E1C53(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTSetTop_m7E0B0D5B5F497660AF71936B287C82378579FB40(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTSetBottom_m2DBDB41D978F0AC8191285887530BD93F1CD73BF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetLeft_m81CE42F10665F9DB57E8E9E1E2071CC82CE56202(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetRight_mC83506F109D5492465A5B662C8B44A21BA3A4A33(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetTop_m99A1549D99E13343E61F5EDEA3F8888D64A56C5F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetBottom_mA70B416EF67700E19A23E6ABDFA862839B45688E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetLRTB_mCA1C1A546BA77B9FDA6394815F879F8E91EFA733(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTSetLRTB_m5578C7E73B1BA1C8F17854D347D2EFFDBBE8FC23(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFind_m50A483F38921A7B82DB1B279AD2C2A0DCD213B6C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFind_m665862ACE1F1529E1D5991AA5B2EB50EAB5845BD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFind_mA46461FE50A61DA998A72004CDF0BA8EA85275C6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFind_m5DC06F8BBD60300DC5D2EDC08B60E1B7DD68F4F1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetBounds_mC52C517BCE211AA80A937DFB6BBDA57B5365ACE7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFind_mC05248D7AFAA90746B748E4020E169E1375B6988(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFind_m6796E1BCF88F019C60970603ACB4C198544D2464(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToPNG_m9287BB5CA9F16C447688DF15869B3CB73C8D2EAB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToJPG_m9ED2E438345EDC4ECEBBAD7BBF0C84AF3833956C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToTGA_m4130CAC5FA95B713F6FE6E10056940AFE45CAD9C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToEXR_mD8FB2B651C2ED92132E2A06D332FE7D7F9A99310(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToPNG_mDC05E791A75ED3F86DD665B80986D0A332FF5193(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToJPG_mCD0C94CDD3306AC2B24B63DEF8CF561CCDB1C454(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToTGA_mE9EA3A0A4E9BC648F5291C7C445B5A394CB460FD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToEXR_m3BC7385919EB0ABC13BFA69C725FAE5281112DBB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToSprite_m036A44049C3B80B9A4F9D4D4651D50092F0B289C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTRotate90_mD1AB4357D802DB346FDD7FAC48ACDDBF9E12EC20(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTRotate180_m9B592620D061F71ECF700F0DEA2436C8D24FEB21(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTRotate270_m384C9E9CA403E44863115554B93B988457F1CE63(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToTexture2D_m9A7AE558EB1AE7B2CE6CF7B74F77855AB0E3573F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFlipHorizontal_m1A71CDC27223F806C143D8F1D7E51DECF5A202C4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFlipVertical_m9B31708827E99F4154C9B40E4233852D497CFB62(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTHasActiveClip_m01F0DA4A380C00C95759FDA648D8D51289CC492E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTIsVisibleFrom_m5E3DED552FD8B132DD4E2B700E4BC17BCE83E587(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_tE339584696C7D04D095E648B21D42B2849672875_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_t9ABDA718E418E4638E6BC97DA82D2CF1AD924F14_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__54_1_t82C36EA4E1E7BC0058F422BD22834CE9E81E869D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass81_0_tC0F6FBCADDA07A1F2590A4C33D5BBF7523311811_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x4F\x6E\x6C\x69\x6E\x65\x43\x68\x65\x63\x6B\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x6F\x6E\x6C\x69\x6E\x65\x5F\x63\x68\x65\x63\x6B\x5F\x31\x5F\x31\x5F\x6F\x6E\x6C\x69\x6E\x65\x5F\x63\x68\x65\x63\x6B\x2E\x68\x74\x6D\x6C"), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[2];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_endlessMode(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x64\x6C\x65\x73\x73\x4D\x6F\x64\x65"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x6E\x65\x72\x61\x6C\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x74\x69\x6E\x75\x6F\x75\x73\x6C\x79\x20\x63\x68\x65\x63\x6B\x20\x66\x6F\x72\x20\x49\x6E\x74\x65\x72\x6E\x65\x74\x20\x61\x76\x61\x69\x6C\x61\x62\x69\x6C\x69\x74\x79\x20\x77\x69\x74\x68\x69\x6E\x20\x67\x69\x76\x65\x6E\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[3];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_intervalMin(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x74\x65\x72\x76\x61\x6C\x4D\x69\x6E"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x20\x64\x65\x6C\x61\x79\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x63\x68\x65\x63\x6B\x73\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x38\x2C\x20\x72\x61\x6E\x67\x65\x3A\x20\x33\x20\x2D\x20\x35\x39\x29\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[2];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 3.0f, 59.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[3];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_intervalMax(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 4.0f, 60.0f, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x74\x65\x72\x76\x61\x6C\x4D\x61\x78"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x64\x65\x6C\x61\x79\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x63\x68\x65\x63\x6B\x73\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x32\x2C\x20\x72\x61\x6E\x67\x65\x3A\x20\x34\x20\x2D\x20\x36\x30\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[3];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_timeout(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65\x6F\x75\x74"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 10.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[3];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65\x6F\x75\x74\x20\x66\x6F\x72\x20\x65\x76\x65\x72\x79\x20\x63\x68\x65\x63\x6B\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x32\x2C\x20\x72\x61\x6E\x67\x65\x3A\x20\x31\x20\x2D\x20\x31\x30\x29\x2E"), NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_forceWWW(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x72\x63\x65\x57\x57\x57"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x72\x63\x65\x20\x55\x6E\x69\x74\x79\x57\x65\x62\x52\x65\x71\x75\x65\x73\x74\x20\x69\x6E\x73\x74\x65\x61\x64\x20\x6F\x66\x20\x57\x65\x62\x43\x6C\x69\x65\x6E\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_customCheck(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x61\x20\x63\x75\x73\x74\x6F\x6D\x20\x63\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x68\x65\x63\x6B\x73\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		ContextMenuItemAttribute_t5DA92E8ADD9F600E72E7051B37DAC8EB7DDAA4E9 * tmp = (ContextMenuItemAttribute_t5DA92E8ADD9F600E72E7051B37DAC8EB7DDAA4E9 *)cache->attributes[2];
		ContextMenuItemAttribute__ctor_m8673BFA9F81B0C8EFD26A07B5705A0650393FF52(tmp, il2cpp_codegen_string_new_wrapper("\x43\x72\x65\x61\x74\x65\x20\x43\x75\x73\x74\x6F\x6D\x43\x68\x65\x63\x6B"), il2cpp_codegen_string_new_wrapper("\x63\x72\x65\x61\x74\x65\x43\x75\x73\x74\x6F\x6D\x43\x68\x65\x63\x6B"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[3];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x75\x73\x74\x6F\x6D\x43\x68\x65\x63\x6B"), NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_microsoft(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x74\x69\x76\x65\x20\x43\x68\x65\x63\x6B\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x6F\x72\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x74\x68\x65\x20\x27\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x20\x4E\x43\x53\x49\x27\x20\x63\x68\x65\x63\x6B\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x2C\x20\x31\x38\x34\x20\x42\x79\x74\x65\x73\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_google204(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x6F\x72\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x74\x68\x65\x20\x27\x47\x6F\x6F\x67\x6C\x65\x20\x32\x30\x34\x27\x20\x63\x68\x65\x63\x6B\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x2C\x20\x32\x37\x39\x20\x42\x79\x74\x65\x73\x29\x2E"), NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_googleBlank(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x6F\x72\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x74\x68\x65\x20\x27\x47\x6F\x6F\x67\x6C\x65\x20\x42\x6C\x61\x6E\x6B\x27\x20\x63\x68\x65\x63\x6B\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x2C\x20\x38\x33\x31\x20\x42\x79\x74\x65\x73\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_apple(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x6F\x72\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x74\x68\x65\x20\x27\x41\x70\x70\x6C\x65\x27\x20\x63\x68\x65\x63\x6B\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x2C\x20\x35\x30\x33\x20\x42\x79\x74\x65\x73\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_ubuntu(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x6F\x72\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x74\x68\x65\x20\x27\x55\x62\x75\x6E\x74\x75\x27\x20\x63\x68\x65\x63\x6B\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x2C\x20\x31\x30\x30\x31\x20\x42\x79\x74\x65\x73\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_runOnStart(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x75\x6E\x4F\x6E\x53\x74\x61\x72\x74"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[3];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x72\x74\x20\x61\x74\x20\x72\x75\x6E\x74\x69\x6D\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_delay(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 120.0f, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x6C\x61\x79"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[3];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x6C\x61\x79\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x75\x6E\x74\x69\x6C\x20\x74\x68\x65\x20\x4F\x6E\x6C\x69\x6E\x65\x43\x68\x65\x63\x6B\x20\x73\x74\x61\x72\x74\x73\x20\x63\x68\x65\x63\x6B\x69\x6E\x67\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x30\x2C\x20\x72\x61\x6E\x67\x65\x3A\x20\x30\x20\x2D\x20\x31\x32\x30\x29\x2E"), NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_U3CisInternetAvailableU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_U3CLastCheckU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_U3CDataDownloadedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnStatusChange(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnOnlineStatusChange(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnNetworkReachabilityChange(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnOnlineCheckComplete(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_get_isInternetAvailable_mB8D7ADA954F61774006B5B0CC83EF0BA926241ED(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_set_isInternetAvailable_m300C7DBB2A346EE1446CCA0C261B078F31D23AE1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_get_LastCheck_mB662D5E64AEFC884220C17267A75C630F94809AB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_set_LastCheck_m67A510F9D0DE82359817CFE6A0996F744A8A1C4C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_get_DataDownloaded_m4E94946086AE3150005F2BE61C03E2A4A715D17E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_set_DataDownloaded_mFCA7AA53E70908620761FB4814D07F3442F07A23(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_add_OnOnlineStatusChange_mE60151295E3C4E4453C8D3E317490B3C1B58007C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_remove_OnOnlineStatusChange_m191B25B8B2F624F83CD491A5B8D14ED50C3FEC86(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_add_OnNetworkReachabilityChange_m938758CE29A4DEEFC3653C02C9BF15F43673D1C7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_remove_OnNetworkReachabilityChange_m7D657C300452F527D4BDD45F55B0A14248F67B50(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_add_OnOnlineCheckComplete_mFB6BAEF23BA67D0BD31DF06481A1405680AE8BAC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_remove_OnOnlineCheckComplete_mFA97D1EEFF914837E2BBF273EC72F1C3F7AC1CDC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_RefreshYield_mA1A4B0E79DC8083648A98F1BB2998F2B6469C10E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRefreshYieldU3Ed__129_tBF0CB254F9A178BECCDC29B113049D5A390DB47A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRefreshYieldU3Ed__129_tBF0CB254F9A178BECCDC29B113049D5A390DB47A_0_0_0_var), NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_wwwCheck_m3CB186B15336CE3CE92F62C87806A59B94BC8409(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CwwwCheckU3Ed__131_tDC196BCEE13348B800156D6ECEBDA9ADC921569E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CwwwCheckU3Ed__131_tDC196BCEE13348B800156D6ECEBDA9ADC921569E_0_0_0_var), NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_google204Check_mB487FAC2EDF6B7CC49AB7D521194D04B7E4951A6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cgoogle204CheckU3Ed__132_t45EF56476E3EE3D120644CC1647818DEDA335989_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cgoogle204CheckU3Ed__132_t45EF56476E3EE3D120644CC1647818DEDA335989_0_0_0_var), NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_googleBlankCheck_m83DA4A965E7A9D70219A28CBD4660A8B27FF52D5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CgoogleBlankCheckU3Ed__133_t4FAE0B9E4E059EE4C8578C9BA9F0AD5742CF648F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CgoogleBlankCheckU3Ed__133_t4FAE0B9E4E059EE4C8578C9BA9F0AD5742CF648F_0_0_0_var), NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_startWorker_m712AE1F1F6163081D7ACAA1440E80FB909C84697(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CstartWorkerU3Ed__135_t0692C6D98709C1ECE5AB38F4FA78CF8FA0FA1748_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CstartWorkerU3Ed__135_t0692C6D98709C1ECE5AB38F4FA78CF8FA0FA1748_0_0_0_var), NULL);
	}
}
static void OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_internetCheck_m26BC7810D3F1627A1C90F0160374E8822C6D2092(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CinternetCheckU3Ed__136_t88AFCE419C8A3E7E686E64624EEDAD6C0BD02CD0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CinternetCheckU3Ed__136_t88AFCE419C8A3E7E686E64624EEDAD6C0BD02CD0_0_0_0_var), NULL);
	}
}
static void U3CRefreshYieldU3Ed__129_tBF0CB254F9A178BECCDC29B113049D5A390DB47A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRefreshYieldU3Ed__129_tBF0CB254F9A178BECCDC29B113049D5A390DB47A_CustomAttributesCacheGenerator_U3CRefreshYieldU3Ed__129__ctor_m0932B7AF205C1AFAC47F2B6F0B67AA4AFB84835C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRefreshYieldU3Ed__129_tBF0CB254F9A178BECCDC29B113049D5A390DB47A_CustomAttributesCacheGenerator_U3CRefreshYieldU3Ed__129_System_IDisposable_Dispose_m9EFA937D9FC3AA4516183A844F32319214C5C26A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRefreshYieldU3Ed__129_tBF0CB254F9A178BECCDC29B113049D5A390DB47A_CustomAttributesCacheGenerator_U3CRefreshYieldU3Ed__129_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDEB14477B0E0486BA7DD5773A134C59B9B1A05D4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRefreshYieldU3Ed__129_tBF0CB254F9A178BECCDC29B113049D5A390DB47A_CustomAttributesCacheGenerator_U3CRefreshYieldU3Ed__129_System_Collections_IEnumerator_Reset_m9F40BB9040C52254158400AA661985396BB26A4C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRefreshYieldU3Ed__129_tBF0CB254F9A178BECCDC29B113049D5A390DB47A_CustomAttributesCacheGenerator_U3CRefreshYieldU3Ed__129_System_Collections_IEnumerator_get_Current_m0F62B1BADD0DAB5B954EB34F5DBDEA1F9199A179(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwwwCheckU3Ed__131_tDC196BCEE13348B800156D6ECEBDA9ADC921569E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CwwwCheckU3Ed__131_tDC196BCEE13348B800156D6ECEBDA9ADC921569E_CustomAttributesCacheGenerator_U3CwwwCheckU3Ed__131__ctor_mB464818753B324E9DF2E095A7516F092E22998DE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwwwCheckU3Ed__131_tDC196BCEE13348B800156D6ECEBDA9ADC921569E_CustomAttributesCacheGenerator_U3CwwwCheckU3Ed__131_System_IDisposable_Dispose_mAC2958080A5562AF837FC380C4521D6F32A9A92B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwwwCheckU3Ed__131_tDC196BCEE13348B800156D6ECEBDA9ADC921569E_CustomAttributesCacheGenerator_U3CwwwCheckU3Ed__131_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m078EC1D91098DC6F1C9777A6530976909F538E9A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwwwCheckU3Ed__131_tDC196BCEE13348B800156D6ECEBDA9ADC921569E_CustomAttributesCacheGenerator_U3CwwwCheckU3Ed__131_System_Collections_IEnumerator_Reset_m61592DD78BFB0F4476608325FAE6010134CD4E39(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwwwCheckU3Ed__131_tDC196BCEE13348B800156D6ECEBDA9ADC921569E_CustomAttributesCacheGenerator_U3CwwwCheckU3Ed__131_System_Collections_IEnumerator_get_Current_m0011E9B3464F6805E58DB9A63C700656204AAE70(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cgoogle204CheckU3Ed__132_t45EF56476E3EE3D120644CC1647818DEDA335989_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cgoogle204CheckU3Ed__132_t45EF56476E3EE3D120644CC1647818DEDA335989_CustomAttributesCacheGenerator_U3Cgoogle204CheckU3Ed__132__ctor_mD56DB6A6123E95B4FB5A2DD3468C7DCAD96AB020(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cgoogle204CheckU3Ed__132_t45EF56476E3EE3D120644CC1647818DEDA335989_CustomAttributesCacheGenerator_U3Cgoogle204CheckU3Ed__132_System_IDisposable_Dispose_m258A151CB942BDEFDB9F047FA0C28039BC3B5201(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cgoogle204CheckU3Ed__132_t45EF56476E3EE3D120644CC1647818DEDA335989_CustomAttributesCacheGenerator_U3Cgoogle204CheckU3Ed__132_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FB0EF6D56A3488E55A4367B308EF4119847C346(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cgoogle204CheckU3Ed__132_t45EF56476E3EE3D120644CC1647818DEDA335989_CustomAttributesCacheGenerator_U3Cgoogle204CheckU3Ed__132_System_Collections_IEnumerator_Reset_mE5CDB8B60C91B9F9030380F2E98B378B16B7A63A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cgoogle204CheckU3Ed__132_t45EF56476E3EE3D120644CC1647818DEDA335989_CustomAttributesCacheGenerator_U3Cgoogle204CheckU3Ed__132_System_Collections_IEnumerator_get_Current_m55BA33D3303B0EC5B915D9E93BD8FEEA94C646E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgoogleBlankCheckU3Ed__133_t4FAE0B9E4E059EE4C8578C9BA9F0AD5742CF648F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgoogleBlankCheckU3Ed__133_t4FAE0B9E4E059EE4C8578C9BA9F0AD5742CF648F_CustomAttributesCacheGenerator_U3CgoogleBlankCheckU3Ed__133__ctor_m8F291E2D659EF39C62FC156BCA2D389587B72630(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgoogleBlankCheckU3Ed__133_t4FAE0B9E4E059EE4C8578C9BA9F0AD5742CF648F_CustomAttributesCacheGenerator_U3CgoogleBlankCheckU3Ed__133_System_IDisposable_Dispose_m6823EFCDFC68DDFA593DA76CE12CD88DC653FACE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgoogleBlankCheckU3Ed__133_t4FAE0B9E4E059EE4C8578C9BA9F0AD5742CF648F_CustomAttributesCacheGenerator_U3CgoogleBlankCheckU3Ed__133_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFCB73C941646FEA8753CFC711C28E0174B474F78(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgoogleBlankCheckU3Ed__133_t4FAE0B9E4E059EE4C8578C9BA9F0AD5742CF648F_CustomAttributesCacheGenerator_U3CgoogleBlankCheckU3Ed__133_System_Collections_IEnumerator_Reset_mC9723822D012EDC85EC1965EF661CE135E28CFB0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgoogleBlankCheckU3Ed__133_t4FAE0B9E4E059EE4C8578C9BA9F0AD5742CF648F_CustomAttributesCacheGenerator_U3CgoogleBlankCheckU3Ed__133_System_Collections_IEnumerator_get_Current_m43A1657DB7A5092AA08B859BC0404C0A3635780A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass135_0_t4865BD66DDE04748CE0E3EE7E7D2D2D0FC9F21F9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CstartWorkerU3Ed__135_t0692C6D98709C1ECE5AB38F4FA78CF8FA0FA1748_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CstartWorkerU3Ed__135_t0692C6D98709C1ECE5AB38F4FA78CF8FA0FA1748_CustomAttributesCacheGenerator_U3CstartWorkerU3Ed__135__ctor_mBF48C92F9EF46F426C2CEF9BE406C4F51B2DB278(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CstartWorkerU3Ed__135_t0692C6D98709C1ECE5AB38F4FA78CF8FA0FA1748_CustomAttributesCacheGenerator_U3CstartWorkerU3Ed__135_System_IDisposable_Dispose_mA94CB49D92794B0E95C576FFF365B5235A94CA6A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CstartWorkerU3Ed__135_t0692C6D98709C1ECE5AB38F4FA78CF8FA0FA1748_CustomAttributesCacheGenerator_U3CstartWorkerU3Ed__135_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5404CDC99EF7E2A8AFB47FBCE8153A028333590D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CstartWorkerU3Ed__135_t0692C6D98709C1ECE5AB38F4FA78CF8FA0FA1748_CustomAttributesCacheGenerator_U3CstartWorkerU3Ed__135_System_Collections_IEnumerator_Reset_m4FC0A6B4B6F41C7E92E85FD01D147C1167139853(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CstartWorkerU3Ed__135_t0692C6D98709C1ECE5AB38F4FA78CF8FA0FA1748_CustomAttributesCacheGenerator_U3CstartWorkerU3Ed__135_System_Collections_IEnumerator_get_Current_m47742F8B1A9854677AD992B38704810BE3BCFF94(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CinternetCheckU3Ed__136_t88AFCE419C8A3E7E686E64624EEDAD6C0BD02CD0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CinternetCheckU3Ed__136_t88AFCE419C8A3E7E686E64624EEDAD6C0BD02CD0_CustomAttributesCacheGenerator_U3CinternetCheckU3Ed__136__ctor_mD5D5AE14F1C3C982EAF77FF103C0FD283EED1C89(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CinternetCheckU3Ed__136_t88AFCE419C8A3E7E686E64624EEDAD6C0BD02CD0_CustomAttributesCacheGenerator_U3CinternetCheckU3Ed__136_System_IDisposable_Dispose_m712C823E815029CFFE4C449601DF8F5F87FB60F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CinternetCheckU3Ed__136_t88AFCE419C8A3E7E686E64624EEDAD6C0BD02CD0_CustomAttributesCacheGenerator_U3CinternetCheckU3Ed__136_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA2AF9A7F386BED8090D59D5A41F18DC950D6C4A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CinternetCheckU3Ed__136_t88AFCE419C8A3E7E686E64624EEDAD6C0BD02CD0_CustomAttributesCacheGenerator_U3CinternetCheckU3Ed__136_System_Collections_IEnumerator_Reset_mD083701132826FCCEDBEE0047F6A63CC2F0B8C1C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CinternetCheckU3Ed__136_t88AFCE419C8A3E7E686E64624EEDAD6C0BD02CD0_CustomAttributesCacheGenerator_U3CinternetCheckU3Ed__136_System_Collections_IEnumerator_get_Current_m90BAABE96B6E2DABE6BDDEF853DA73D2C7EF4820(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t5851270268B0137F4EEFC3C295A18073723CE084_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SetupProject_tF6E3CA22AB467421EA5835D7A1911FE188FECCA1_CustomAttributesCacheGenerator_SetupProject_setup_m5E022ECA6395D4AF9E1E8095957FEF8AA40A8931(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 1LL, NULL);
	}
}
static void CustomCheck_tB95B254C3D40A077697776496FA29E32508C879B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x43\x75\x73\x74\x6F\x6D\x43\x68\x65\x63\x6B"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x6C\x69\x6E\x65\x20\x43\x68\x65\x63\x6B\x20\x50\x52\x4F\x2F\x43\x75\x73\x74\x6F\x6D\x43\x68\x65\x63\x6B"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 1000LL, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x4F\x6E\x6C\x69\x6E\x65\x43\x68\x65\x63\x6B\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x6F\x6E\x6C\x69\x6E\x65\x5F\x63\x68\x65\x63\x6B\x5F\x31\x5F\x31\x5F\x64\x61\x74\x61\x5F\x31\x5F\x31\x5F\x63\x75\x73\x74\x6F\x6D\x5F\x63\x68\x65\x63\x6B\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void CustomCheck_tB95B254C3D40A077697776496FA29E32508C879B_CustomAttributesCacheGenerator_URL(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x75\x73\x74\x6F\x6D\x20\x55\x52\x4C\x20\x74\x6F\x20\x70\x65\x72\x66\x6F\x72\x6D\x20\x74\x68\x65\x20\x49\x6E\x74\x65\x72\x6E\x65\x74\x20\x61\x76\x61\x69\x6C\x61\x62\x69\x6C\x69\x74\x79\x20\x74\x65\x73\x74\x73\x20\x65\x2E\x67\x2E\x20\x68\x74\x74\x70\x73\x3A\x2F\x2F\x6D\x79\x64\x6F\x6D\x61\x69\x6E\x2E\x63\x6F\x6D\x2F\x63\x6F\x6E\x6E\x65\x63\x74\x2E\x74\x78\x74\x2E\x20\x54\x68\x65\x20\x68\x6F\x73\x74\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x68\x74\x74\x70\x73\x2D\x62\x61\x73\x65\x64\x20\x61\x6E\x64\x20\x70\x72\x6F\x76\x69\x64\x65\x20\x61\x6E\x20\x27\x41\x63\x63\x65\x73\x73\x2D\x43\x6F\x6E\x74\x72\x6F\x6C\x2D\x41\x6C\x6C\x6F\x77\x2D\x4F\x72\x69\x67\x69\x6E\x27\x20\x68\x65\x61\x64\x65\x72\x2E"), NULL);
	}
}
static void CustomCheck_tB95B254C3D40A077697776496FA29E32508C879B_CustomAttributesCacheGenerator_ExpectedData(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x78\x70\x65\x63\x74\x65\x64\x20\x64\x61\x74\x61\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x63\x75\x73\x74\x6F\x6D\x20\x55\x52\x4C\x20\x28\x61\x73\x20\x73\x74\x72\x69\x6E\x67\x29\x2E"), NULL);
	}
}
static void CustomCheck_tB95B254C3D40A077697776496FA29E32508C879B_CustomAttributesCacheGenerator_DataMustBeEquals(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x70\x61\x72\x65\x73\x20\x74\x68\x65\x20\x63\x75\x73\x74\x6F\x6D\x20\x64\x61\x74\x61\x20\x77\x69\x74\x68\x20\x27\x65\x71\x75\x61\x6C\x73\x27\x20\x74\x6F\x20\x74\x68\x65\x20\x65\x78\x70\x65\x63\x74\x65\x64\x20\x64\x61\x74\x61\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x2C\x20\x66\x61\x6C\x73\x65\x20\x75\x73\x65\x73\x20\x27\x63\x6F\x6E\x74\x61\x69\x6E\x73\x27\x20\x61\x73\x20\x6D\x61\x74\x63\x68\x29\x2E"), NULL);
	}
}
static void CustomCheck_tB95B254C3D40A077697776496FA29E32508C879B_CustomAttributesCacheGenerator_UseOnlyCustom(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x6F\x6E\x6C\x79\x20\x74\x68\x65\x20\x63\x75\x73\x74\x6F\x6D\x20\x75\x72\x6C\x20\x66\x6F\x72\x20\x49\x6E\x74\x65\x72\x6E\x65\x74\x20\x61\x76\x61\x69\x6C\x61\x62\x69\x6C\x69\x74\x79\x20\x74\x65\x73\x74\x73\x20\x61\x6E\x64\x20\x69\x67\x6E\x6F\x72\x65\x73\x20\x61\x6C\x6C\x20\x62\x75\x69\x6C\x74\x2D\x69\x6E\x20\x63\x68\x65\x63\x6B\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void CustomCheck_tB95B254C3D40A077697776496FA29E32508C879B_CustomAttributesCacheGenerator_ShowErrors(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x70\x6C\x61\x79\x73\x20\x61\x6C\x6C\x20\x63\x6F\x6E\x6E\x65\x63\x74\x69\x6F\x6E\x20\x65\x72\x72\x6F\x72\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void CustomCheck_tB95B254C3D40A077697776496FA29E32508C879B_CustomAttributesCacheGenerator_HeaderSize(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x7A\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x72\x65\x71\x75\x65\x73\x74\x20\x68\x65\x61\x64\x65\x72\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x30\x29\x2E"), NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x4F\x6E\x6C\x69\x6E\x65\x43\x68\x65\x63\x6B\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x6F\x6E\x6C\x69\x6E\x65\x5F\x63\x68\x65\x63\x6B\x5F\x31\x5F\x31\x5F\x74\x6F\x6F\x6C\x5F\x31\x5F\x31\x5F\x70\x69\x6E\x67\x5F\x63\x68\x65\x63\x6B\x2E\x68\x74\x6D\x6C"), NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[1];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[2];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_hostName(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x73\x74\x4E\x61\x6D\x65"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x6E\x65\x72\x61\x6C\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x73\x74\x6E\x61\x6D\x65\x20\x6F\x72\x20\x49\x50\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x50\x69\x6E\x67\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[3];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_timeout(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 10.0f, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65\x6F\x75\x74"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65\x6F\x75\x74\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x50\x69\x6E\x67\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x33\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[3];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_runOnStart(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x72\x74\x20\x61\x74\x20\x72\x75\x6E\x74\x69\x6D\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[2];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[3];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x75\x6E\x4F\x6E\x53\x74\x61\x72\x74"), NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_OnPingComplete(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_OnPingCompleted(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_U3CLastHostU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_U3CLastIPU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_U3CisBusyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_add_OnPingCompleted_mEA7E0B5AC5977EAF7382F1469B4FF16F11B60F85(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_remove_OnPingCompleted_m40B6D6EE813A7ADE10EE4611C0293B79C7198282(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_get_LastHost_m28A8825F4DAD9E16E545CE34B9BE3163C503FE78(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_set_LastHost_m2FEDAEFB0E8ABDC31BE0CCD1797BFE4C1400E8C7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_get_LastIP_mB8FF7A2C0226AC642FE9CFEF7D0F88D334C588EE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_set_LastIP_mCC80740815D94DF602FC47F60419524A4670A6E3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_get_isBusy_mE1481885876C2C5992FBD31F6D287C8110E79FEE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_set_isBusy_m57F3210C204477EDE0220BCF94928541DEF4D280(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_ping_m598086683E20BB9CD8864A8D7772863DD963A91A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CpingU3Ed__41_t6828EFFEDDA314F733151F7CB92A7C4D6B15BC21_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CpingU3Ed__41_t6828EFFEDDA314F733151F7CB92A7C4D6B15BC21_0_0_0_var), NULL);
	}
}
static void U3CpingU3Ed__41_t6828EFFEDDA314F733151F7CB92A7C4D6B15BC21_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CpingU3Ed__41_t6828EFFEDDA314F733151F7CB92A7C4D6B15BC21_CustomAttributesCacheGenerator_U3CpingU3Ed__41__ctor_m0952AE64F05F49E60C3A7802B2D62D018B6EFBA1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CpingU3Ed__41_t6828EFFEDDA314F733151F7CB92A7C4D6B15BC21_CustomAttributesCacheGenerator_U3CpingU3Ed__41_System_IDisposable_Dispose_mB7F8AC235AF49225E193B99003ADFD5B42B40DEA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CpingU3Ed__41_t6828EFFEDDA314F733151F7CB92A7C4D6B15BC21_CustomAttributesCacheGenerator_U3CpingU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6632216B79E50B7860AEAB94FD3A015DDB4E393(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CpingU3Ed__41_t6828EFFEDDA314F733151F7CB92A7C4D6B15BC21_CustomAttributesCacheGenerator_U3CpingU3Ed__41_System_Collections_IEnumerator_Reset_m2F3E9B866A0EDB096B724B46371BC59E78C1CFAD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CpingU3Ed__41_t6828EFFEDDA314F733151F7CB92A7C4D6B15BC21_CustomAttributesCacheGenerator_U3CpingU3Ed__41_System_Collections_IEnumerator_get_Current_m24F550144A66BCF6840855ABB5AB697CF4115A21(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x4F\x6E\x6C\x69\x6E\x65\x43\x68\x65\x63\x6B\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x6F\x6E\x6C\x69\x6E\x65\x5F\x63\x68\x65\x63\x6B\x5F\x31\x5F\x31\x5F\x74\x6F\x6F\x6C\x5F\x31\x5F\x31\x5F\x70\x72\x6F\x78\x79\x2E\x68\x74\x6D\x6C"), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[2];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPProxyURL(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x48\x54\x54\x50\x20\x50\x72\x6F\x78\x79\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x52\x4C\x20\x28\x77\x69\x74\x68\x6F\x75\x74\x20\x70\x72\x6F\x74\x6F\x63\x6F\x6C\x29\x20\x6F\x72\x20\x49\x50\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x72\x6F\x78\x79\x20\x73\x65\x72\x76\x65\x72\x2E"), NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPProxyPort(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x72\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x72\x6F\x78\x79\x20\x73\x65\x72\x76\x65\x72\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 65535.0f, NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPProxyUsername(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x72\x6E\x61\x6D\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x70\x72\x6F\x78\x79\x20\x73\x65\x72\x76\x65\x72\x20\x28\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x29\x2E"), NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPProxyPassword(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x73\x73\x77\x6F\x72\x64\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x70\x72\x6F\x78\x79\x20\x73\x65\x72\x76\x65\x72\x20\x28\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x29\x2E"), NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPProxyURLProtocol(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x6F\x74\x6F\x63\x6F\x6C\x20\x28\x65\x2E\x67\x2E\x20\x27\x68\x74\x74\x70\x3A\x2F\x2F\x27\x29\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x70\x72\x6F\x78\x79\x20\x73\x65\x72\x76\x65\x72\x20\x28\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x29\x2E"), NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPSProxyURL(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x48\x54\x54\x50\x53\x20\x50\x72\x6F\x78\x79\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x52\x4C\x20\x28\x77\x69\x74\x68\x6F\x75\x74\x20\x70\x72\x6F\x74\x6F\x63\x6F\x6C\x29\x20\x6F\x72\x20\x49\x50\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x72\x6F\x78\x79\x20\x73\x65\x72\x76\x65\x72\x2E"), NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPSProxyPort(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x72\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x72\x6F\x78\x79\x20\x73\x65\x72\x76\x65\x72\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 65535.0f, NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPSProxyUsername(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x72\x6E\x61\x6D\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x70\x72\x6F\x78\x79\x20\x73\x65\x72\x76\x65\x72\x20\x28\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x29\x2E"), NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPSProxyPassword(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x73\x73\x77\x6F\x72\x64\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x70\x72\x6F\x78\x79\x20\x73\x65\x72\x76\x65\x72\x20\x28\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x29\x2E"), NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPSProxyURLProtocol(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x6F\x74\x6F\x63\x6F\x6C\x20\x28\x65\x2E\x67\x2E\x20\x27\x68\x74\x74\x70\x3A\x2F\x2F\x27\x29\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x70\x72\x6F\x78\x79\x20\x73\x65\x72\x76\x65\x72\x20\x28\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x29\x2E"), NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_EnableOnAwake(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x72\x74\x75\x70\x20\x42\x65\x68\x61\x76\x69\x6F\x75\x72"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x74\x68\x65\x20\x70\x72\x6F\x78\x79\x20\x6F\x6E\x20\x61\x77\x61\x6B\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_U3ChasHTTPProxyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_U3ChasHTTPSProxyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_Proxy_get_hasHTTPProxy_m1184684C993FD6988A6314C9F0829E2FE199FA3F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_Proxy_set_hasHTTPProxy_mACFF6FC5482BF6F9BB6AAEED9015B0D9B7A72BAD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_Proxy_get_hasHTTPSProxy_mFDB3265F542D6C9A234DEDD7C7B661FE04DC48EA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_Proxy_set_hasHTTPSProxy_m62EF9B3308635CDA6648D27ACDB529A7DA0BC624(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x4F\x6E\x6C\x69\x6E\x65\x43\x68\x65\x63\x6B\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x6F\x6E\x6C\x69\x6E\x65\x5F\x63\x68\x65\x63\x6B\x5F\x31\x5F\x31\x5F\x74\x6F\x6F\x6C\x5F\x31\x5F\x31\x5F\x73\x70\x65\x65\x64\x5F\x74\x65\x73\x74\x2E\x68\x74\x6D\x6C"), NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[2];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_dataSize(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61\x53\x69\x7A\x65"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x6E\x65\x72\x61\x6C\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61\x20\x73\x69\x7A\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x73\x70\x65\x65\x64\x20\x74\x65\x73\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x53\x4D\x41\x4C\x4C\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[3];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_smallURL(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6D\x61\x6C\x6C\x55\x52\x4C"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x52\x4C\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x73\x6D\x61\x6C\x6C\x20\x64\x61\x74\x61\x20\x73\x70\x65\x65\x64\x20\x74\x65\x73\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x6C\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x67\x6F\x6F\x67\x6C\x65\x74\x61\x6C\x6B\x2F\x67\x6F\x6F\x67\x6C\x65\x74\x61\x6C\x6B\x2D\x73\x65\x74\x75\x70\x2E\x65\x78\x65\x20\x28\x31\x2E\x35\x4D\x42\x29\x29\x2E"), NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_mediumURL(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x52\x4C\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x6D\x65\x64\x69\x75\x6D\x20\x64\x61\x74\x61\x20\x73\x70\x65\x65\x64\x20\x74\x65\x73\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x6C\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x64\x6C\x2F\x65\x61\x72\x74\x68\x2F\x63\x6C\x69\x65\x6E\x74\x2F\x61\x64\x76\x61\x6E\x63\x65\x64\x2F\x63\x75\x72\x72\x65\x6E\x74\x2F\x67\x6F\x6F\x67\x6C\x65\x65\x61\x72\x74\x68\x70\x72\x6F\x77\x69\x6E\x2D\x37\x2E\x33\x2E\x32\x2D\x78\x36\x34\x2E\x65\x78\x65\x20\x28\x36\x32\x4D\x42\x29\x29\x2E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x64\x69\x75\x6D\x55\x52\x4C"), NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_largeURL(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x52\x4C\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x6C\x61\x72\x67\x65\x20\x64\x61\x74\x61\x20\x73\x70\x65\x65\x64\x20\x74\x65\x73\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x68\x74\x74\x70\x73\x3A\x2F\x2F\x64\x6C\x2E\x67\x6F\x6F\x67\x6C\x65\x2E\x63\x6F\x6D\x2F\x64\x72\x69\x76\x65\x2D\x66\x69\x6C\x65\x2D\x73\x74\x72\x65\x61\x6D\x2F\x47\x6F\x6F\x67\x6C\x65\x44\x72\x69\x76\x65\x46\x53\x53\x65\x74\x75\x70\x2E\x65\x78\x65\x20\x28\x31\x39\x35\x4D\x42\x29\x29\x2E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x61\x72\x67\x65\x55\x52\x4C"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_runOnStart(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x75\x6E\x4F\x6E\x53\x74\x61\x72\x74"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x72\x74\x20\x61\x74\x20\x72\x75\x6E\x74\x69\x6D\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[3];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_OnSpeedTestComplete(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_OnTestCompleted(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_U3CLastURLU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_U3CLastDataSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_U3CLastDurationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_U3CLastSpeedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_U3CisBusyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_add_OnTestCompleted_m8D2C4D2F430B89483F385992F5FC91DEDC91DD42(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_remove_OnTestCompleted_m72DE929DF607E54B47CDB4F40CD1CC3620929D4C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_get_LastURL_m0468950BED96D6ABD0C83672B44BB85D91C161F5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_set_LastURL_m9D96892C9D99B85CBE85E9A3F9E2AFD4BA271FCA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_get_LastDataSize_m4AD20E2A05D4B7C9BBDC59739B849C3EF18EFC77(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_set_LastDataSize_mEA3AD53374F1654942AE07A509737B2C80FB22FD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_get_LastDuration_m214130E3CFA7560BC58B191ADD7A3A0572C18432(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_set_LastDuration_m25A4AFB4524531A862ACA303BB9962659910C616(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_get_LastSpeed_m3AA25CA1067DC3498E40BC8A4E512C2CEA2A3D62(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_set_LastSpeed_m9BEBAC7801B8E92DF84A90C9E668AE4C71613AF1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_get_isBusy_m4D7224F998875FB10633C5CE24DEBAC9781A0F5A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_set_isBusy_m1412307C9B8BC3DC728F9A563B71F7F70AF79D31(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_test_m565BB9ED0A7C216AB10F7606F7F1E825791E3EBA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CtestU3Ed__57_t39BF4B4A0B29F9833FB294F4058427265F922159_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CtestU3Ed__57_t39BF4B4A0B29F9833FB294F4058427265F922159_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass57_0_tC6A242B53522E5BF4F3F5CE26E06357966B0C17D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtestU3Ed__57_t39BF4B4A0B29F9833FB294F4058427265F922159_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtestU3Ed__57_t39BF4B4A0B29F9833FB294F4058427265F922159_CustomAttributesCacheGenerator_U3CtestU3Ed__57__ctor_mA55DD118C29DA2907F4E37B2D786A5C8820E2DC9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtestU3Ed__57_t39BF4B4A0B29F9833FB294F4058427265F922159_CustomAttributesCacheGenerator_U3CtestU3Ed__57_System_IDisposable_Dispose_m5E2A2F8DD8A744C30E33FD237B0022C719BF7583(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtestU3Ed__57_t39BF4B4A0B29F9833FB294F4058427265F922159_CustomAttributesCacheGenerator_U3CtestU3Ed__57_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1B97A242FEAA29891CBB0A292158DDB9A2E2698(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtestU3Ed__57_t39BF4B4A0B29F9833FB294F4058427265F922159_CustomAttributesCacheGenerator_U3CtestU3Ed__57_System_Collections_IEnumerator_Reset_mA927C7FB7D10E03D0383229029126C7DCA117818(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtestU3Ed__57_t39BF4B4A0B29F9833FB294F4058427265F922159_CustomAttributesCacheGenerator_U3CtestU3Ed__57_System_Collections_IEnumerator_get_Current_m1B867BF9B0DC7EC5A88A3BCBE2D714E4FAC07097(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SetupProject_tC550EA2E0182F0E06E04CE3390C1B7EFD09E9048_CustomAttributesCacheGenerator_SetupProject_setup_m104E53DC5254FAF89F40F6E1FC378B81E5D5C848(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 1LL, NULL);
	}
}
static void SpeedTestClient_tC566A0160BE6447A23272C24523D6BB7CE583F4B_CustomAttributesCacheGenerator_SpeedTestClient_GenerateDownloadUrls_m2161B46C3DEFFBAE2A015999637F68FF37AEAE7E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_t4694567A5E71A80C9D25860046825262BF524A03_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t0807953E6AC5D454A7D61CD20A6E1FEF115EB75F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t0807953E6AC5D454A7D61CD20A6E1FEF115EB75F_CustomAttributesCacheGenerator_U3CU3Ec_U3CTestDownloadSpeedU3Eb__7_0_mAA4F34A5FA1AC21845FB0F82E1D1205BDF60FBB1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3CTestDownloadSpeedU3Eb__7_0U3Ed_t5139BC4222B7584C9C6791078DF2E66199658524_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CU3CTestDownloadSpeedU3Eb__7_0U3Ed_t5139BC4222B7584C9C6791078DF2E66199658524_0_0_0_var), NULL);
	}
}
static void U3CU3CTestDownloadSpeedU3Eb__7_0U3Ed_t5139BC4222B7584C9C6791078DF2E66199658524_CustomAttributesCacheGenerator_U3CU3CTestDownloadSpeedU3Eb__7_0U3Ed_SetStateMachine_m8280F2E096D7E15C71E84AFFA607CE6026649EF0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_t81A42CD06887C4F7902C8E0C0E40063434FCBAC7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_t81A42CD06887C4F7902C8E0C0E40063434FCBAC7_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass8_0_U3CTestUploadSpeedU3Eb__0_mDAA8CA8010940F8CE9C4FB058D59C2DB1F261512(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3CTestUploadSpeedU3Eb__0U3Ed_tDCD2DA70E4DBCAD1F844D600CD63CC92E027EFEF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CU3CTestUploadSpeedU3Eb__0U3Ed_tDCD2DA70E4DBCAD1F844D600CD63CC92E027EFEF_0_0_0_var), NULL);
	}
}
static void U3CU3CTestUploadSpeedU3Eb__0U3Ed_tDCD2DA70E4DBCAD1F844D600CD63CC92E027EFEF_CustomAttributesCacheGenerator_U3CU3CTestUploadSpeedU3Eb__0U3Ed_SetStateMachine_mE9DCC6B33A1AD21B4B16B4A8EA77136BC3FAE33F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_1_tB900CC9E31E22EF06649B9D677B1F5134A16356F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_1_tB900CC9E31E22EF06649B9D677B1F5134A16356F_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass9_0_1_U3CTestSpeedU3Eb__0_mD53868A5C3F3CA1C0007300DA57A43D931B9FB0B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3CTestSpeedU3Eb__0U3Ed_t6E992DBE722759CCEA52EFC4D5A3FD4B519C2C47_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CU3CTestSpeedU3Eb__0U3Ed_t6E992DBE722759CCEA52EFC4D5A3FD4B519C2C47_0_0_0_var), NULL);
	}
}
static void U3CU3CTestSpeedU3Eb__0U3Ed_t6E992DBE722759CCEA52EFC4D5A3FD4B519C2C47_CustomAttributesCacheGenerator_U3CU3CTestSpeedU3Eb__0U3Ed_SetStateMachine_m953315F2CEEE0D9D63A821CB0421DF030575746E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__9_1_t69D9342A982FABDAD539DC3BADA031B8568CC62A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_CustomAttributesCacheGenerator_U3CGenerateDownloadUrlsU3Ed__12__ctor_m49AA3916B0CC412A999C63471289DE67A8238189(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_CustomAttributesCacheGenerator_U3CGenerateDownloadUrlsU3Ed__12_System_IDisposable_Dispose_mA02129B3149C89C32BC0A39E1E2D0E1027817D3C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_CustomAttributesCacheGenerator_U3CGenerateDownloadUrlsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m164D472018312DAB261D382BD124DEA9619B8A21(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_CustomAttributesCacheGenerator_U3CGenerateDownloadUrlsU3Ed__12_System_Collections_IEnumerator_Reset_m339DE9C5E3ABD68C27ED2A355B846F7CF0809439(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_CustomAttributesCacheGenerator_U3CGenerateDownloadUrlsU3Ed__12_System_Collections_IEnumerator_get_Current_m7BD616CEEFFE24B1D93B2873127B3F575201EC5F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_CustomAttributesCacheGenerator_U3CGenerateDownloadUrlsU3Ed__12_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m5945E3B01D1CAB268C7D1C10D4C980CDC2D9ACC2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_CustomAttributesCacheGenerator_U3CGenerateDownloadUrlsU3Ed__12_System_Collections_IEnumerable_GetEnumerator_mE0880AC1B187148E1F1EF59C10DC320FBA22E2A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SpeedTestHttpClient_t0B699E8C4DE06A86A6C1F04FE598DB3A0CAB8A3E_CustomAttributesCacheGenerator_SpeedTestHttpClient_GetConfig_m06815E1E1D6C99B4A9C8115CBF721BE434945A9B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetConfigU3Ed__1_1_t27C2E6BD21131A846C1ED0873201EA1DF5599309_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CGetConfigU3Ed__1_1_t27C2E6BD21131A846C1ED0873201EA1DF5599309_0_0_0_var), NULL);
	}
}
static void U3CGetConfigU3Ed__1_1_t27C2E6BD21131A846C1ED0873201EA1DF5599309_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetConfigU3Ed__1_1_t27C2E6BD21131A846C1ED0873201EA1DF5599309_CustomAttributesCacheGenerator_U3CGetConfigU3Ed__1_1_SetStateMachine_m7F1CD5751A65C6291F5C6802576469EF11BF8749(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x4F\x6E\x6C\x69\x6E\x65\x43\x68\x65\x63\x6B\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x6F\x6E\x6C\x69\x6E\x65\x5F\x63\x68\x65\x63\x6B\x5F\x31\x5F\x31\x5F\x74\x6F\x6F\x6C\x5F\x31\x5F\x31\x5F\x73\x70\x65\x65\x64\x5F\x74\x65\x73\x74\x5F\x6E\x5F\x65\x5F\x74\x5F\x31\x5F\x31\x5F\x73\x70\x65\x65\x64\x5F\x74\x65\x73\x74\x5F\x6E\x5F\x65\x5F\x74\x2E\x68\x74\x6D\x6C"), NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[1];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[2];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_testDownload(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x73\x74\x44\x6F\x77\x6E\x6C\x6F\x61\x64"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x6E\x65\x72\x61\x6C\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x73\x74\x20\x74\x68\x65\x20\x64\x6F\x77\x6E\x6C\x6F\x61\x64\x20\x73\x70\x65\x65\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[3];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_testUpload(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x73\x74\x20\x74\x68\x65\x20\x75\x70\x6C\x6F\x61\x64\x20\x73\x70\x65\x65\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x73\x74\x55\x70\x6C\x6F\x61\x64"), NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_runOnStart(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72\x20\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x72\x74\x20\x61\x74\x20\x72\x75\x6E\x74\x69\x6D\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[3];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x75\x6E\x4F\x6E\x53\x74\x61\x72\x74"), NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_OnSpeedTestComplete(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_OnTestCompleted(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_U3CLastServerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_U3CLastDurationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_U3CLastDownloadSpeedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_U3CLastUploadSpeedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_U3CisBusyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_add_OnTestCompleted_m8FDFDE70C23620C1E96C5E02391957C2A6D15E94(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_remove_OnTestCompleted_m06533154A729B79608F982FE1EA7496E26EB2D75(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_get_LastServer_m4160DAEB0C71670367B0A377D484269F6FED2D9D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_set_LastServer_mCBE6FCFCD3555A245B7B2B15525BA94C408DAA4E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_get_LastDuration_m66A52EA200CD945E4144147B034FFCDAF8D5209C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_set_LastDuration_m1FE25D3FE39015C277211676D4EE0D9219699267(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_get_LastDownloadSpeed_m9B8E22094FAD2A38D52AA9474E4427F9444D7FC3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_set_LastDownloadSpeed_m2DAA6161A0EA820A78C3F3412D03B627BCDF5307(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_get_LastUploadSpeed_m908040E942913DEE63BD79667029A41F142979A7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_set_LastUploadSpeed_m113E57EAC1E26360920F3C43D54153B7F3861C40(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_get_isBusy_m9B3DFCD781D218502F70A677C4DCED47045B6B6C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_set_isBusy_mF0A5FAA978A26916776D32EE3335A56B2F615A2E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_test_m42A569CAF7BC7316E7E71F88EDFB21976E4E837C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CtestU3Ed__49_tDD0B93F803E5724477C153898A710AFD7F7DC0A3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CtestU3Ed__49_tDD0B93F803E5724477C153898A710AFD7F7DC0A3_0_0_0_var), NULL);
	}
}
static void U3CtestU3Ed__49_tDD0B93F803E5724477C153898A710AFD7F7DC0A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtestU3Ed__49_tDD0B93F803E5724477C153898A710AFD7F7DC0A3_CustomAttributesCacheGenerator_U3CtestU3Ed__49__ctor_m84BDA458D061319259587AAFA767FDA0512FEFEC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtestU3Ed__49_tDD0B93F803E5724477C153898A710AFD7F7DC0A3_CustomAttributesCacheGenerator_U3CtestU3Ed__49_System_IDisposable_Dispose_m01FFF33E02E0F10C094A89BEE1BFF4462E4894DE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtestU3Ed__49_tDD0B93F803E5724477C153898A710AFD7F7DC0A3_CustomAttributesCacheGenerator_U3CtestU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5169CE1D6561EDC30A2EEBB1E1B9C3C0FBA4AF3A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtestU3Ed__49_tDD0B93F803E5724477C153898A710AFD7F7DC0A3_CustomAttributesCacheGenerator_U3CtestU3Ed__49_System_Collections_IEnumerator_Reset_m379FD9E7D91CF45ED29222C3EE208D03E4A50604(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtestU3Ed__49_tDD0B93F803E5724477C153898A710AFD7F7DC0A3_CustomAttributesCacheGenerator_U3CtestU3Ed__49_System_Collections_IEnumerator_get_Current_mDE60D7E8FFDAA8163CF42DB919A8DF30A5B5669B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t90D70C0F6DBA5511535940FAC5ED6E61368739A8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 * tmp = (XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 *)cache->attributes[0];
		XmlRootAttribute__ctor_mFF794C36E64D16871DECFD78A0344FDCCBAA9E39(tmp, il2cpp_codegen_string_new_wrapper("\x63\x6C\x69\x65\x6E\x74"), NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_U3CIpU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_U3CLatitudeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_U3CLongitudeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_U3CIspU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_U3CIspRatingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_U3CRatingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_U3CIspAvarageDownloadSpeedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_U3CIspAvarageUploadSpeedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_get_Ip_m0B9445853B5D4199CE94F7F5BD0241C1251B77B2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_set_Ip_mF873302D8A710CBDB05FC4388D3AD3D9A09D0A76(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_get_Latitude_mADF069565C2C60552BD35CE0AC10019B6BD1AF2D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_set_Latitude_mD8FAF274E3638BCFECAD9340117F4E240EBCCCBF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_get_Longitude_mE4C0B3E531C0B17AFEF3C737E378852FE1AA03D2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_set_Longitude_m190D851D72EF6D9470D1EFCF6242758CFB074F4E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_get_Isp_mA92D7910AA592D926174B851DA714BC44B60631D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_set_Isp_m649133295A21EC49717640BC84170602CDB3C69A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_get_IspRating_m0188AD85C4C73A2719515B1603FF3DBDACF10B09(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_set_IspRating_m2B96661EB68F8B4049926304C28B07A00F561BE4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_get_Rating_m88C44AF1AEAD628F6D49D59DD02141E944BD630F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_set_Rating_m49097F8083162E99A5B396F1441C392F54911DA3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_get_IspAvarageDownloadSpeed_m32ED04BF66402C1B0BDA39775E56BEAFA4194E8B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_set_IspAvarageDownloadSpeed_m5AD00BF1E8F2691D70DF57173230F442794AD4E8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_get_IspAvarageUploadSpeed_mC605999904C63FC1EDFC297DAF55E9E53B0E5236(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_set_IspAvarageUploadSpeed_m109F22774BA6E37C4847E51DDCC5774B1FC38C5D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_U3C_ctorU3Eb__35_0_m5116CF0671C518DC2DAABABAEBD16AB801DF3B87(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA____Ip_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x69\x70"), NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA____Latitude_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x6C\x61\x74"), NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA____Longitude_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x6C\x6F\x6E"), NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA____Isp_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x69\x73\x70"), NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA____IspRating_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x69\x73\x70\x72\x61\x74\x69\x6E\x67"), NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA____Rating_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x72\x61\x74\x69\x6E\x67"), NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA____IspAvarageDownloadSpeed_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x69\x73\x70\x64\x6C\x61\x76\x67"), NULL);
	}
}
static void Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA____IspAvarageUploadSpeed_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x69\x73\x70\x75\x6C\x61\x76\x67"), NULL);
	}
}
static void Coordinate_tF261EB8C468E414B19D067355CB7B0041751AEAF_CustomAttributesCacheGenerator_U3CLatitudeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Coordinate_tF261EB8C468E414B19D067355CB7B0041751AEAF_CustomAttributesCacheGenerator_U3CLongitudeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Coordinate_tF261EB8C468E414B19D067355CB7B0041751AEAF_CustomAttributesCacheGenerator_Coordinate_get_Latitude_m8D07D3A4B6CDE856EEE577B3E8F1C7A3B169CD65(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Coordinate_tF261EB8C468E414B19D067355CB7B0041751AEAF_CustomAttributesCacheGenerator_Coordinate_set_Latitude_m1ACA8FA6F57F247E89D8F7CEF7EE20F2370C1705(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Coordinate_tF261EB8C468E414B19D067355CB7B0041751AEAF_CustomAttributesCacheGenerator_Coordinate_get_Longitude_mAB87DACDEAFB05CE4CE6CAD0C447F8D56C29E130(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Coordinate_tF261EB8C468E414B19D067355CB7B0041751AEAF_CustomAttributesCacheGenerator_Coordinate_set_Longitude_m16230727D76BB3EAF9332F7F26D202AC2E9E5CF0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 * tmp = (XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 *)cache->attributes[0];
		XmlRootAttribute__ctor_mFF794C36E64D16871DECFD78A0344FDCCBAA9E39(tmp, il2cpp_codegen_string_new_wrapper("\x64\x6F\x77\x6E\x6C\x6F\x61\x64"), NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_U3CTestLengthU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_U3CInitialTestU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_U3CMinTestSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_U3CThreadsPerUrlU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_get_TestLength_m289664666D0F5DECA0D41435591FE75CD7295E09(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_set_TestLength_mFD53362225A0BC710E1CC989047F7FF027214463(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_get_InitialTest_m808C4F980B4198E5882F23BBD6DDB59C040FEE35(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_set_InitialTest_m08BA6DA96AA31BA256EEF1530ED93E250A397E6D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_get_MinTestSize_m3DF159A73D260590CA6906F5E3A4D7BB90B819C4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_set_MinTestSize_m641B5621AC012890DA21BC159DDDBFEDFC2A449F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_get_ThreadsPerUrl_m4D34349486D1BDFB271D6BB1F9297F3EC6348A1E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_set_ThreadsPerUrl_m709C09E224ADF9252B9B55760E627C27A3F14941(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B____TestLength_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x74\x65\x73\x74\x6C\x65\x6E\x67\x74\x68"), NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B____InitialTest_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x69\x6E\x69\x74\x69\x61\x6C\x74\x65\x73\x74"), NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B____MinTestSize_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x69\x6E\x74\x65\x73\x74\x73\x69\x7A\x65"), NULL);
	}
}
static void Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B____ThreadsPerUrl_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x72\x65\x61\x64\x73\x70\x65\x72\x75\x72\x6C"), NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 * tmp = (XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 *)cache->attributes[0];
		XmlRootAttribute__ctor_mFF794C36E64D16871DECFD78A0344FDCCBAA9E39(tmp, il2cpp_codegen_string_new_wrapper("\x73\x65\x72\x76\x65\x72"), NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CCountryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CSponsorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CHostU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CUrlU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CLatitudeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CLongitudeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CDistanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CLatencyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Id_mA2999C7EC3409CEDE72BB7C8B5364C4EC352386E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Id_m019851DDEF130546B9918509A8A181ECD9FEA4B4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Name_mD18CBC3EE46C7D0AD38FC614D8A46D2B9ABAA1EA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Name_m17AAFBAC7179265AFC6E0880EB6EDE0B05D41F99(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Country_mCC14ED05AC9FBAE8811B173060C20C36642DBF26(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Country_m59CF2DCCBA4737CA0DC26F43B1D2B2900ECD5A18(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Sponsor_mCD97CBB658EC7C8B376D879B80898E08316AB7A0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Sponsor_mA8CEE4B9339434697F9A1366F44A0DBEB68BA970(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Host_m71A2D38CB45DF2946191BED1962F14DBD0955E22(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Host_m7303208597252E6E186421C523F744CBB3A7DEB9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Url_m660B03D65EC132A3EB811035498667CD9A4CABA8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Url_mD827DA9B0FE13649BC4EFFA65B8FDEF7ABA20F59(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Latitude_m8B22A17C87780AA0F9744C2728D9622BE2887A46(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Latitude_m77AE62B1568B46095196B41F4B94B2EB588B052E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Longitude_m097F70F6D4388E7715674D4152CAEEBD74777701(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Longitude_m22F4D3DE5E0D31B562D4DDE6BEA2302C97CAE77D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Distance_m086243B6C27057388CDBF0A5C2855FFDF0601908(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Distance_m2994F70C5A3B5667D63E0E26514F9B0D3CB0D6E5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Latency_mA4CAB54DA41B8EBA4584559614C72BC412A7AD6C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Latency_mE1C4F2D76A7A9D7DEE6907001C029275959D1D37(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_U3C_ctorU3Eb__43_0_m11A8527A9A2A89C54099A241207BAF066ACEB075(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_tED9643BADC961426E50AA71234EEAD16C451411C____Id_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x69\x64"), NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_tED9643BADC961426E50AA71234EEAD16C451411C____Name_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x6E\x61\x6D\x65"), NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_tED9643BADC961426E50AA71234EEAD16C451411C____Country_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x63\x6F\x75\x6E\x74\x72\x79"), NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_tED9643BADC961426E50AA71234EEAD16C451411C____Sponsor_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x73\x70\x6F\x6E\x73\x6F\x72"), NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_tED9643BADC961426E50AA71234EEAD16C451411C____Host_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x68\x6F\x73\x74"), NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_tED9643BADC961426E50AA71234EEAD16C451411C____Url_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x75\x72\x6C"), NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_tED9643BADC961426E50AA71234EEAD16C451411C____Latitude_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x6C\x61\x74"), NULL);
	}
}
static void Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_tED9643BADC961426E50AA71234EEAD16C451411C____Longitude_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x6C\x6F\x6E"), NULL);
	}
}
static void ServerConfig_t5792ABA39BA8FC204D1B43981B00549F15225BEB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 * tmp = (XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 *)cache->attributes[0];
		XmlRootAttribute__ctor_mFF794C36E64D16871DECFD78A0344FDCCBAA9E39(tmp, il2cpp_codegen_string_new_wrapper("\x73\x65\x72\x76\x65\x72\x2D\x63\x6F\x6E\x66\x69\x67"), NULL);
	}
}
static void ServerConfig_t5792ABA39BA8FC204D1B43981B00549F15225BEB_CustomAttributesCacheGenerator_U3CIgnoreIdsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ServerConfig_t5792ABA39BA8FC204D1B43981B00549F15225BEB_CustomAttributesCacheGenerator_ServerConfig_get_IgnoreIds_m8AD90272174C1B00F09FAB914D423BAC48B03B70(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ServerConfig_t5792ABA39BA8FC204D1B43981B00549F15225BEB_CustomAttributesCacheGenerator_ServerConfig_set_IgnoreIds_m74CA68265E0D0F82345713DA8A75005ABFE6698D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ServerConfig_t5792ABA39BA8FC204D1B43981B00549F15225BEB_CustomAttributesCacheGenerator_ServerConfig_t5792ABA39BA8FC204D1B43981B00549F15225BEB____IgnoreIds_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x69\x67\x6E\x6F\x72\x65\x69\x64\x73"), NULL);
	}
}
static void ServersList_t890F3EAA1E1DC4517612ED4E1159BC59F7480C6A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 * tmp = (XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 *)cache->attributes[0];
		XmlRootAttribute__ctor_mFF794C36E64D16871DECFD78A0344FDCCBAA9E39(tmp, il2cpp_codegen_string_new_wrapper("\x73\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void ServersList_t890F3EAA1E1DC4517612ED4E1159BC59F7480C6A_CustomAttributesCacheGenerator_U3CServersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ServersList_t890F3EAA1E1DC4517612ED4E1159BC59F7480C6A_CustomAttributesCacheGenerator_ServersList_get_Servers_mA728E4CD054E33150481599D265769246D2BE398(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ServersList_t890F3EAA1E1DC4517612ED4E1159BC59F7480C6A_CustomAttributesCacheGenerator_ServersList_set_Servers_m90E26C0707F47E96012430ADC9A5940AFE0E101C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ServersList_t890F3EAA1E1DC4517612ED4E1159BC59F7480C6A_CustomAttributesCacheGenerator_ServersList_t890F3EAA1E1DC4517612ED4E1159BC59F7480C6A____Servers_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlArrayAttribute_t8016E5E39EDDEC98A198E56BBAA467772E8AE472 * tmp = (XmlArrayAttribute_t8016E5E39EDDEC98A198E56BBAA467772E8AE472 *)cache->attributes[0];
		XmlArrayAttribute__ctor_m8A5E563C98D551E290DC924920D36305D9C46A21(tmp, il2cpp_codegen_string_new_wrapper("\x73\x65\x72\x76\x65\x72\x73"), NULL);
	}
	{
		XmlArrayItemAttribute_tADA59D11B0D5E8CF01A1410D957B873D45F2FC94 * tmp = (XmlArrayItemAttribute_tADA59D11B0D5E8CF01A1410D957B873D45F2FC94 *)cache->attributes[1];
		XmlArrayItemAttribute__ctor_m49D7F51DCCCC0F5FAC62C017A93796693F5BF8E2(tmp, il2cpp_codegen_string_new_wrapper("\x73\x65\x72\x76\x65\x72"), NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 * tmp = (XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 *)cache->attributes[0];
		XmlRootAttribute__ctor_mFF794C36E64D16871DECFD78A0344FDCCBAA9E39(tmp, il2cpp_codegen_string_new_wrapper("\x73\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_U3CClientU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_U3CTimesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_U3CDownloadU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_U3CUploadU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_U3CServerConfigU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_U3CServersU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_get_Client_mA491884009E1D77B12B2BB69EF9403E85BC5075A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_set_Client_mF3BFDC0946EB9C6C7C7718C07AFD36A364604A05(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_get_Times_m8C7E3E2A7EC879BFFF31552B55F9365771DFDB53(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_set_Times_m76A004A32676C30B0A6FE86686CEE58D8444504A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_get_Download_m946D691794AB8B057BC1A7BA72CD48421DB76CF5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_set_Download_mC4B696297672244EC829C93A761FA1466DA34A51(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_get_Upload_mF3ADADA4BD5F74A9FF1E4B9C76FF6084DCE9FBFD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_set_Upload_mA7FEFA826C900B4131D2E90C4DB3CF79C7907670(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_get_ServerConfig_mCC41E378E34CDF96D220C26580252358502250C5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_set_ServerConfig_mD35731AAA2D23F0164EEFB2CF28B21E633C20871(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_get_Servers_m32663154FB14571FAA25BE902138A04DEDE73225(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_set_Servers_m409CAB6BF32ACF58F53120805684366D042A75C7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE____Client_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94 * tmp = (XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94 *)cache->attributes[0];
		XmlElementAttribute__ctor_m434F875F0651AD028C2EE19262CA0576A98B8381(tmp, il2cpp_codegen_string_new_wrapper("\x63\x6C\x69\x65\x6E\x74"), NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE____Times_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94 * tmp = (XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94 *)cache->attributes[0];
		XmlElementAttribute__ctor_m434F875F0651AD028C2EE19262CA0576A98B8381(tmp, il2cpp_codegen_string_new_wrapper("\x74\x69\x6D\x65\x73"), NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE____Download_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94 * tmp = (XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94 *)cache->attributes[0];
		XmlElementAttribute__ctor_m434F875F0651AD028C2EE19262CA0576A98B8381(tmp, il2cpp_codegen_string_new_wrapper("\x64\x6F\x77\x6E\x6C\x6F\x61\x64"), NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE____Upload_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94 * tmp = (XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94 *)cache->attributes[0];
		XmlElementAttribute__ctor_m434F875F0651AD028C2EE19262CA0576A98B8381(tmp, il2cpp_codegen_string_new_wrapper("\x75\x70\x6C\x6F\x61\x64"), NULL);
	}
}
static void Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE____ServerConfig_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94 * tmp = (XmlElementAttribute_t1D20EF830E8F66F43BBDCEB96611CD9233154A94 *)cache->attributes[0];
		XmlElementAttribute__ctor_m434F875F0651AD028C2EE19262CA0576A98B8381(tmp, il2cpp_codegen_string_new_wrapper("\x73\x65\x72\x76\x65\x72\x2D\x63\x6F\x6E\x66\x69\x67"), NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 * tmp = (XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 *)cache->attributes[0];
		XmlRootAttribute__ctor_mFF794C36E64D16871DECFD78A0344FDCCBAA9E39(tmp, il2cpp_codegen_string_new_wrapper("\x74\x69\x6D\x65\x73"), NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_U3CDownload1U3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_U3CDownload2U3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_U3CDownload3U3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_U3CUpload1U3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_U3CUpload2U3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_U3CUpload3U3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_get_Download1_mBD1281097BE6FBA5F56F81A9FDC0890CFF072A83(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_set_Download1_m9FB2ACE46B9DB28FCBDE6943A0356B5F4DF60320(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_get_Download2_mC71E858A945784AC0B3AC13C3D626783AD9F75C8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_set_Download2_m2E8D0CE21DF400DDFCF33298CB408CF786A988AA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_get_Download3_mE0F35479045CA5961E39FD77D5BC17BB4E399863(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_set_Download3_m43AEC1ED6C046DE2FDC8D266D5AAF908916EEE23(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_get_Upload1_m23B4FD1C9029FFA70E84306928F68A33759A9ED3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_set_Upload1_mADE25B3FCC8FEF183C95BBC0C956BE32EB2ACA9C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_get_Upload2_mCEA15219828984DDE6749ACC494CCCA07DC6EFB3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_set_Upload2_mDF56B9576186F2BF6893072BC281F25788E409ED(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_get_Upload3_m33891A4C1DB3C988A09EFB43EABFC38DE4390924(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_set_Upload3_m57A585D4E9D371109D318361B5949E82E1B9121C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274____Download1_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x64\x6C\x31"), NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274____Download2_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x64\x6C\x32"), NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274____Download3_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x64\x6C\x33"), NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274____Upload1_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x75\x6C\x31"), NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274____Upload2_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x75\x6C\x32"), NULL);
	}
}
static void Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274____Upload3_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x75\x6C\x33"), NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 * tmp = (XmlRootAttribute_t509A188575B2B9912E6DBF82CBB9A17715BECB96 *)cache->attributes[0];
		XmlRootAttribute__ctor_mFF794C36E64D16871DECFD78A0344FDCCBAA9E39(tmp, il2cpp_codegen_string_new_wrapper("\x75\x70\x6C\x6F\x61\x64"), NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_U3CTestLengthU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_U3CRatioU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_U3CInitialTestU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_U3CMinTestSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_U3CThreadsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_U3CMaxChunkSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_U3CMaxChunkCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_U3CThreadsPerUrlU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_get_TestLength_m01A66839C2D9D5C7DD1FDA014DCBFB80DBB23731(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_set_TestLength_m0E77363058F1C6C9DD543BBE2820DA78013B0080(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_get_Ratio_m37A83FA6FE6D9A815FAAF8E08840C53EAE29DF44(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_set_Ratio_m91BBBD058C120E6E919AB9843DCDD74087C3B20D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_get_InitialTest_m5A44B984EB443D040BB5067823424FF774E1AE68(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_set_InitialTest_m6461F782AA38863779BD2ED35B0A01A4B38986F6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_get_MinTestSize_m50788DA9D8ECC84CC142274094C16D40E2C2C8CA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_set_MinTestSize_m611A46C9FF55E0E2C7326DB31F57731B7A03A8EC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_get_Threads_m9F2C1842B9753DDCC1BC351475292DACB8264696(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_set_Threads_m80F21E7FED558565879437B55F70DF334C3A7F11(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_get_MaxChunkSize_m19E1298ED0FD82CBFF4CE9EFC434F20AE6E2C8C6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_set_MaxChunkSize_m47EDCE56D373ADCA4FCDB9B59DAD53EA9AFF0705(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_get_MaxChunkCount_m4C6E81A8FA8812934006CFDF4A623FA3669FB11E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_set_MaxChunkCount_m275B9BF318D72FE0202F869C819020B7117D8A69(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_get_ThreadsPerUrl_m66AA71DF8D6469F1BE6FCE1D1976ED0C1894CC71(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_set_ThreadsPerUrl_m775BDBCDA5697DC1D6E7A2C1282424FCEF597507(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1____TestLength_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x74\x65\x73\x74\x6C\x65\x6E\x67\x74\x68"), NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1____Ratio_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x72\x61\x74\x69\x6F"), NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1____InitialTest_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x69\x6E\x69\x74\x69\x61\x6C\x74\x65\x73\x74"), NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1____MinTestSize_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x69\x6E\x74\x65\x73\x74\x73\x69\x7A\x65"), NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1____Threads_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x72\x65\x61\x64\x73"), NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1____MaxChunkSize_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x61\x78\x63\x68\x75\x6E\x6B\x73\x69\x7A\x65"), NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1____MaxChunkCount_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x61\x78\x63\x68\x75\x6E\x6B\x63\x6F\x75\x6E\x74"), NULL);
	}
}
static void Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1____ThreadsPerUrl_PropertyInfo(CustomAttributesCache* cache)
{
	{
		XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 * tmp = (XmlAttributeAttribute_t96949A5112ABB51E8AEB77CE3A43AF5EB44F5647 *)cache->attributes[0];
		XmlAttributeAttribute__ctor_m3C9FC780C480AB65FAA249FC66806584C8D80749(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x72\x65\x61\x64\x73\x70\x65\x72\x75\x72\x6C"), NULL);
	}
}
static void EventTester_tC539BE8A6FD0C1628E27F7902C77032BC717A07A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x77\x77\x77\x2E\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x4F\x6E\x6C\x69\x6E\x65\x43\x68\x65\x63\x6B\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x6F\x6E\x6C\x69\x6E\x65\x5F\x63\x68\x65\x63\x6B\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x65\x76\x65\x6E\x74\x5F\x74\x65\x73\x74\x65\x72\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void GUIMain_tAAD610B8E1060640C8477DD60A1BAA342A70B80E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x4F\x6E\x6C\x69\x6E\x65\x43\x68\x65\x63\x6B\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x6F\x6E\x6C\x69\x6E\x65\x5F\x63\x68\x65\x63\x6B\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x67\x5F\x75\x5F\x69\x5F\x6D\x61\x69\x6E\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void GUIMain_tAAD610B8E1060640C8477DD60A1BAA342A70B80E_CustomAttributesCacheGenerator_Available(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x73"), NULL);
	}
}
static void GUIMain_tAAD610B8E1060640C8477DD60A1BAA342A70B80E_CustomAttributesCacheGenerator_GUIMain_U3CStartU3Eb__21_0_m7297BA87941213DE42946C1BCB2FB56B5A8EDB2D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GUIMain_tAAD610B8E1060640C8477DD60A1BAA342A70B80E_CustomAttributesCacheGenerator_GUIMain_U3CStartU3Eb__21_1_mF4C6CF3E35C104AF588FF18D86FFC124FF4425B1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GUIScenes_t7C0FF16F7468A283FBDFA7DECA0717005E7303A1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x4F\x6E\x6C\x69\x6E\x65\x43\x68\x65\x63\x6B\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x6F\x6E\x6C\x69\x6E\x65\x5F\x63\x68\x65\x63\x6B\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x67\x5F\x75\x5F\x69\x5F\x73\x63\x65\x6E\x65\x73\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void GUIScenes_t7C0FF16F7468A283FBDFA7DECA0717005E7303A1_CustomAttributesCacheGenerator_PreviousScene(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x72\x65\x76\x69\x6F\x75\x73\x20\x73\x63\x65\x6E\x65\x2E"), NULL);
	}
}
static void GUIScenes_t7C0FF16F7468A283FBDFA7DECA0717005E7303A1_CustomAttributesCacheGenerator_NextScene(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x6E\x65\x78\x74\x20\x73\x63\x65\x6E\x65\x2E"), NULL);
	}
}
static void ManageEndlessMode_tA0F8BA38466255518EA1D472F021D6DB001E4D11_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x2E\x63\x6F\x6D\x2F\x6D\x65\x64\x69\x61\x2F\x64\x61\x74\x61\x2F\x61\x73\x73\x65\x74\x73\x2F\x4F\x6E\x6C\x69\x6E\x65\x43\x68\x65\x63\x6B\x2F\x61\x70\x69\x2F\x63\x6C\x61\x73\x73\x5F\x63\x72\x6F\x73\x73\x74\x61\x6C\x65\x73\x5F\x31\x5F\x31\x5F\x6F\x6E\x6C\x69\x6E\x65\x5F\x63\x68\x65\x63\x6B\x5F\x31\x5F\x31\x5F\x64\x65\x6D\x6F\x5F\x31\x5F\x31\x5F\x75\x74\x69\x6C\x5F\x31\x5F\x31\x5F\x6D\x61\x6E\x61\x67\x65\x5F\x65\x6E\x64\x6C\x65\x73\x73\x5F\x6D\x6F\x64\x65\x2E\x68\x74\x6D\x6C"), NULL);
	}
}
static void UIDrag_tAD84C1AAD3684943B7420A4EE83C67760BCC97A8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void UIFocus_t8FA57C45177DF0396463F9D4D9B37C37647BFF40_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void UIFocus_t8FA57C45177DF0396463F9D4D9B37C37647BFF40_CustomAttributesCacheGenerator_ManagerName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x20\x63\x6F\x6E\x74\x61\x69\x6E\x69\x6E\x67\x20\x74\x68\x65\x20\x55\x49\x57\x69\x6E\x64\x6F\x77\x4D\x61\x6E\x61\x67\x65\x72\x2E"), NULL);
	}
}
static void UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_Group(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x47\x72\x6F\x75\x70\x20\x74\x6F\x20\x66\x61\x64\x65\x2E"), NULL);
	}
}
static void UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_Delay(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x6C\x61\x79\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x62\x65\x66\x6F\x72\x65\x20\x66\x61\x64\x69\x6E\x67\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x32\x29\x2E"), NULL);
	}
}
static void UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_FadeTime(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x64\x65\x20\x74\x69\x6D\x65\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x32\x29\x2E"), NULL);
	}
}
static void UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_Disable(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x61\x62\x6C\x65\x20\x55\x49\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x61\x66\x74\x65\x72\x20\x74\x68\x65\x20\x66\x61\x64\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_FadeAtStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x64\x65\x20\x61\x74\x20\x53\x74\x61\x72\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_UIHint_lerpAlphaDown_m6FEC2A6BF3503E0B3046CEB9F4AE8CB7FB05C46C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_0_0_0_var), NULL);
	}
}
static void UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_UIHint_lerpAlphaUp_mD0790C5B2C87FFFB07F78F0E098C08FFBFB70246(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_0_0_0_var), NULL);
	}
}
static void U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8__ctor_m137125C8737CD01C1A17E318878AFB2B0D0CDB14(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8_System_IDisposable_Dispose_m82EA5F02C54838CDFAD7E3C549962D0DB1151FE1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB42DD7F0CDC10AADCD99116A14BF77D16197AE49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8_System_Collections_IEnumerator_Reset_mB4243BFE9BA869109D22485266636F346DE3495F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8_System_Collections_IEnumerator_get_Current_m641305F2EB99B80E211B4E6E99E859C2B7AAE63B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9__ctor_m41FA43BCB2D14D39F0DC1E1AFFCECB0D18E02419(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9_System_IDisposable_Dispose_mD5454F31F93F6B5DCCB420AB1241DD2E2DEE331B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07EF2B75C53B187A10E0ACC00AB20DF4CAB27CD7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9_System_Collections_IEnumerator_Reset_m1C29B48A4BFBB4956CB59DAF52A587455998D965(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9_System_Collections_IEnumerator_get_Current_mA92CCF9323E6BC3E4B224B1CBF4098D02978B980(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UIResize_t37199FAAC3C3D6AC437662F9CF19B9ADDB91F228_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void UIResize_t37199FAAC3C3D6AC437662F9CF19B9ADDB91F228_CustomAttributesCacheGenerator_MinSize(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x20\x73\x69\x7A\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x55\x49\x20\x65\x6C\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void UIResize_t37199FAAC3C3D6AC437662F9CF19B9ADDB91F228_CustomAttributesCacheGenerator_MaxSize(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x73\x69\x7A\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x55\x49\x20\x65\x6C\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void UIResize_t37199FAAC3C3D6AC437662F9CF19B9ADDB91F228_CustomAttributesCacheGenerator_IgnoreMaxSize(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x20\x6D\x61\x78\x69\x6D\x75\x6D\x20\x73\x69\x7A\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x55\x49\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void UIResize_t37199FAAC3C3D6AC437662F9CF19B9ADDB91F228_CustomAttributesCacheGenerator_SpeedFactor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x69\x7A\x65\x20\x73\x70\x65\x65\x64\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x32\x29\x2E"), NULL);
	}
}
static void UIWindowManager_t227DE834418A39C678BF8498F47C8743E421AB46_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void UIWindowManager_t227DE834418A39C678BF8498F47C8743E421AB46_CustomAttributesCacheGenerator_Windows(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6C\x6C\x20\x57\x69\x6E\x64\x6F\x77\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65\x2E"), NULL);
	}
}
static void WindowManager_t8F0E4C96CAFAD4C11F999874779FE1B04137E0ED_CustomAttributesCacheGenerator_Speed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x69\x6E\x64\x6F\x77\x20\x6D\x6F\x76\x65\x6D\x65\x6E\x74\x20\x73\x70\x65\x65\x64\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x33\x29\x2E"), NULL);
	}
}
static void WindowManager_t8F0E4C96CAFAD4C11F999874779FE1B04137E0ED_CustomAttributesCacheGenerator_Dependencies(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x70\x65\x6E\x64\x65\x6E\x74\x20\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x73\x20\x28\x61\x63\x74\x69\x76\x65\x20\x3D\x3D\x20\x6F\x70\x65\x6E\x29\x2E"), NULL);
	}
}
static void WindowManager_t8F0E4C96CAFAD4C11F999874779FE1B04137E0ED_CustomAttributesCacheGenerator_ClosedAtStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x6F\x73\x65\x20\x74\x68\x65\x20\x77\x69\x6E\x64\x6F\x77\x20\x61\x74\x20\x53\x74\x61\x72\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void FPSDisplay_tE1C362120A52899D78B55F41C6639FE89D2AF6F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void FPSDisplay_tE1C362120A52899D78B55F41C6639FE89D2AF6F4_CustomAttributesCacheGenerator_FPS(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x74\x68\x65\x20\x46\x50\x53\x2E"), NULL);
	}
}
static void FPSDisplay_tE1C362120A52899D78B55F41C6639FE89D2AF6F4_CustomAttributesCacheGenerator_FrameUpdate(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x70\x64\x61\x74\x65\x20\x65\x76\x65\x72\x79\x20\x73\x65\x74\x20\x66\x72\x61\x6D\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x35\x29\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 300.0f, NULL);
	}
}
static void FPSDisplay_tE1C362120A52899D78B55F41C6639FE89D2AF6F4_CustomAttributesCacheGenerator_Key(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4B\x65\x79\x20\x74\x6F\x20\x61\x63\x74\x69\x76\x61\x74\x65\x20\x74\x68\x65\x20\x46\x50\x53\x20\x63\x6F\x75\x6E\x74\x65\x72\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x6E\x6F\x6E\x65\x29\x2E"), NULL);
	}
}
static void AudioFilterController_tE30989E3E8CD9E8A8454160C6048BFA26D77EDE8_CustomAttributesCacheGenerator_FindAllAudioFiltersOnStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x61\x72\x63\x68\x65\x73\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x61\x75\x64\x69\x6F\x20\x66\x69\x6C\x74\x65\x72\x73\x20\x69\x6E\x20\x74\x68\x65\x20\x77\x68\x6F\x6C\x65\x20\x73\x63\x65\x6E\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x20\x46\x69\x6C\x74\x65\x72\x73"), NULL);
	}
}
static void AudioFilterController_tE30989E3E8CD9E8A8454160C6048BFA26D77EDE8_CustomAttributesCacheGenerator_ResetAudioFiltersOnStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x65\x74\x73\x20\x61\x6C\x6C\x20\x61\x63\x74\x69\x76\x65\x20\x61\x75\x64\x69\x6F\x20\x66\x69\x6C\x74\x65\x72\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x6F\x6E\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void AudioFilterController_tE30989E3E8CD9E8A8454160C6048BFA26D77EDE8_CustomAttributesCacheGenerator_ReverbFilterDropdown(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x4F\x62\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_FindAllAudioSourcesOnStart(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x20\x53\x6F\x75\x72\x63\x65\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x61\x72\x63\x68\x65\x73\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x41\x75\x64\x69\x6F\x53\x6F\x75\x72\x63\x65\x20\x69\x6E\x20\x74\x68\x65\x20\x77\x68\x6F\x6C\x65\x20\x73\x63\x65\x6E\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_AudioSources(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x74\x69\x76\x65\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x64\x20\x41\x75\x64\x69\x6F\x53\x6F\x75\x72\x63\x65\x73\x2E"), NULL);
	}
}
static void AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_ResetAudioSourcesOnStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x65\x74\x73\x20\x61\x6C\x6C\x20\x61\x63\x74\x69\x76\x65\x20\x41\x75\x64\x69\x6F\x53\x6F\x75\x72\x63\x65\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_Mute(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x75\x74\x65\x20\x6F\x6E\x2F\x6F\x66\x66\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_Loop(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x6F\x6F\x70\x20\x6F\x6E\x2F\x6F\x66\x66\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_Volume(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x6F\x6C\x75\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x61\x75\x64\x69\x6F\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x29"), NULL);
	}
}
static void AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_Pitch(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x69\x74\x63\x68\x20\x6F\x66\x20\x74\x68\x65\x20\x61\x75\x64\x69\x6F\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x29\x2E"), NULL);
	}
}
static void AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_StereoPan(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x65\x72\x65\x6F\x20\x70\x61\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x61\x75\x64\x69\x6F\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x30\x29\x2E"), NULL);
	}
}
static void AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_VolumeText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x4F\x62\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void CTScreenshot_tB6F2227DD4079BB4E195E1DCA7FC02B71B3AAB03_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void CTScreenshot_tB6F2227DD4079BB4E195E1DCA7FC02B71B3AAB03_CustomAttributesCacheGenerator_Prefix(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x66\x69\x78\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x67\x65\x6E\x65\x72\x61\x74\x65\x20\x66\x69\x6C\x65\x20\x6E\x61\x6D\x65\x73\x2E"), NULL);
	}
}
static void CTScreenshot_tB6F2227DD4079BB4E195E1DCA7FC02B71B3AAB03_CustomAttributesCacheGenerator_Scale(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x63\x74\x6F\x72\x20\x62\x79\x20\x77\x68\x69\x63\x68\x20\x74\x6F\x20\x69\x6E\x63\x72\x65\x61\x73\x65\x20\x72\x65\x73\x6F\x6C\x75\x74\x69\x6F\x6E\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x29\x2E"), NULL);
	}
}
static void CTScreenshot_tB6F2227DD4079BB4E195E1DCA7FC02B71B3AAB03_CustomAttributesCacheGenerator_KeyCode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4B\x65\x79\x2D\x70\x72\x65\x73\x73\x20\x74\x6F\x20\x63\x61\x70\x74\x75\x72\x65\x20\x74\x68\x65\x20\x73\x63\x72\x65\x65\x6E\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x46\x38\x29\x2E"), NULL);
	}
}
static void CTScreenshot_tB6F2227DD4079BB4E195E1DCA7FC02B71B3AAB03_CustomAttributesCacheGenerator_ShowFileLocation(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x68\x6F\x77\x20\x66\x69\x6C\x65\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void PlatformController_t0109B938FCC3CF39CF034B9211211DB02853B670_CustomAttributesCacheGenerator_Platforms(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x65\x64\x20\x70\x6C\x61\x74\x66\x6F\x72\x6D\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x66\x69\x67\x75\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void PlatformController_t0109B938FCC3CF39CF034B9211211DB02853B670_CustomAttributesCacheGenerator_Active(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x6F\x72\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x74\x68\x65\x20\x27\x4F\x62\x6A\x65\x63\x74\x73\x27\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x20\x27\x50\x6C\x61\x74\x66\x6F\x72\x6D\x73\x27\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void PlatformController_t0109B938FCC3CF39CF034B9211211DB02853B670_CustomAttributesCacheGenerator_Objects(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x65\x64\x20\x6F\x62\x6A\x65\x63\x74\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void PlatformController_t0109B938FCC3CF39CF034B9211211DB02853B670_CustomAttributesCacheGenerator_Scripts(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x6E\x6F\x42\x65\x68\x61\x76\x69\x6F\x75\x72\x20\x53\x63\x72\x69\x70\x74\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x65\x64\x20\x73\x63\x72\x69\x70\x74\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72\x2E"), NULL);
	}
}
static void U3CU3Ec_t07D8486F1A2DBA03AA4D92725468785D45EBD4E3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_UseInterval(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x73\x20\x74\x6F\x20\x63\x68\x61\x6E\x67\x65\x20\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_ChangeInterval(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x20\x63\x68\x61\x6E\x67\x65\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x6D\x69\x6E\x20\x28\x3D\x20\x78\x29\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x28\x3D\x20\x79\x29\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x78\x20\x3D\x20\x35\x2C\x20\x79\x20\x3D\x20\x31\x30\x29\x2E"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_HueRange(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x20\x68\x75\x65\x20\x72\x61\x6E\x67\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x6D\x69\x6E\x20\x28\x3D\x20\x78\x29\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x28\x3D\x20\x79\x29\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x78\x20\x3D\x20\x30\x2C\x20\x79\x20\x3D\x20\x31\x29\x2E"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_SaturationRange(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x20\x73\x61\x74\x75\x72\x61\x74\x69\x6F\x6E\x20\x72\x61\x6E\x67\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x6D\x69\x6E\x20\x28\x3D\x20\x78\x29\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x28\x3D\x20\x79\x29\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x78\x20\x3D\x20\x31\x2C\x20\x79\x20\x3D\x20\x31\x29\x2E"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_ValueRange(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x20\x76\x61\x6C\x75\x65\x20\x72\x61\x6E\x67\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x6D\x69\x6E\x20\x28\x3D\x20\x78\x29\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x28\x3D\x20\x79\x29\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x78\x20\x3D\x20\x31\x2C\x20\x79\x20\x3D\x20\x31\x29\x2E"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_AlphaRange(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x20\x61\x6C\x70\x68\x61\x20\x72\x61\x6E\x67\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x6D\x69\x6E\x20\x28\x3D\x20\x78\x29\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x28\x3D\x20\x79\x29\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x78\x20\x3D\x20\x31\x2C\x20\x79\x20\x3D\x20\x31\x29\x2E"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_GrayScale(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x67\x72\x61\x79\x20\x73\x63\x61\x6C\x65\x20\x63\x6F\x6C\x6F\x72\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_Material(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x69\x66\x79\x20\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x6F\x66\x20\x61\x20\x6D\x61\x74\x65\x72\x69\x61\x6C\x20\x69\x6E\x73\x74\x65\x61\x64\x20\x6F\x66\x20\x74\x68\x65\x20\x52\x65\x6E\x64\x65\x72\x65\x72\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x6E\x6F\x74\x20\x73\x65\x74\x2C\x20\x6F\x70\x74\x69\x6F\x6E\x61\x6C\x29\x2E"), NULL);
	}
}
static void RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_RandomColorAtStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x74\x6F\x20\x61\x20\x72\x61\x6E\x64\x6F\x6D\x20\x63\x6F\x6C\x6F\x72\x20\x61\x74\x20\x53\x74\x61\x72\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_UseInterval(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x73\x20\x74\x6F\x20\x63\x68\x61\x6E\x67\x65\x20\x74\x68\x65\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_ChangeInterval(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x20\x63\x68\x61\x6E\x67\x65\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x6D\x69\x6E\x20\x28\x3D\x20\x78\x29\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x28\x3D\x20\x79\x29\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x78\x20\x3D\x20\x31\x30\x2C\x20\x79\x20\x3D\x20\x32\x30\x29\x2E"), NULL);
	}
}
static void RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_SpeedMin(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x73\x70\x65\x65\x64\x20\x70\x65\x72\x20\x61\x78\x69\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x35\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x61\x78\x69\x73\x29\x2E"), NULL);
	}
}
static void RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_SpeedMax(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x73\x70\x65\x65\x64\x20\x70\x65\x72\x20\x61\x78\x69\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x31\x35\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x61\x78\x69\x73\x29\x2E"), NULL);
	}
}
static void RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_RandomRotationAtStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x74\x6F\x20\x61\x20\x72\x61\x6E\x64\x6F\x6D\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x61\x74\x20\x53\x74\x61\x72\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_RandomChangeIntervalPerAxis(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x20\x63\x68\x61\x6E\x67\x65\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x20\x70\x65\x72\x20\x61\x78\x69\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_UseInterval(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x73\x20\x74\x6F\x20\x63\x68\x61\x6E\x67\x65\x20\x74\x68\x65\x20\x73\x63\x61\x6C\x65\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_ChangeInterval(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x20\x63\x68\x61\x6E\x67\x65\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x6D\x69\x6E\x20\x28\x3D\x20\x78\x29\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x28\x3D\x20\x79\x29\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x78\x20\x3D\x20\x31\x30\x2C\x20\x79\x20\x3D\x20\x32\x30\x29\x2E"), NULL);
	}
}
static void RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_ScaleMin(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x73\x70\x65\x65\x64\x20\x70\x65\x72\x20\x61\x78\x69\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x35\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x61\x78\x69\x73\x29\x2E"), NULL);
	}
}
static void RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_ScaleMax(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x73\x63\x61\x6C\x65\x20\x70\x65\x72\x20\x61\x78\x69\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x30\x2E\x31\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x61\x78\x69\x73\x29\x2E"), NULL);
	}
}
static void RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_Uniform(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x66\x6F\x72\x6D\x20\x73\x63\x61\x6C\x69\x6E\x67\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x61\x78\x69\x73\x20\x28\x78\x2D\x61\x78\x69\x73\x20\x76\x61\x6C\x75\x65\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x75\x73\x65\x64\x2C\x20\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x74\x72\x75\x65\x29\x2E"), NULL);
	}
}
static void RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_RandomScaleAtStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x74\x6F\x20\x61\x20\x72\x61\x6E\x64\x6F\x6D\x20\x73\x63\x61\x6C\x65\x20\x61\x74\x20\x53\x74\x61\x72\x74\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void BaseHelper_t06A43A26211DD50047AB61E90EC3DCF06758E347_CustomAttributesCacheGenerator_BaseHelper_initialize_mB4C285BF69FBAD4B277ED8AD10DDF08DA38F69C5(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mAEDC96FCA281601682E7207BD386A1553C1B6081(tmp, NULL);
	}
}
static void BaseHelper_t06A43A26211DD50047AB61E90EC3DCF06758E347_CustomAttributesCacheGenerator_BaseHelper_hasActiveClip_m4C947A64FD354BCBAEA2078E0E1A919578D9CCBA(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x74\x68\x65\x20\x65\x78\x74\x65\x6E\x73\x69\x6F\x6E\x20\x6D\x65\x74\x68\x6F\x64\x20\x27\x43\x54\x48\x61\x73\x41\x63\x74\x69\x76\x65\x43\x6C\x69\x70\x27\x20\x69\x6E\x73\x74\x65\x61\x64"), NULL);
	}
}
static void BaseHelper_t06A43A26211DD50047AB61E90EC3DCF06758E347_CustomAttributesCacheGenerator_BaseHelper_ClearTags_mD93B30D4885D2E5DCCA1672188CD20EB76B01C4F(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x74\x68\x65\x20\x65\x78\x74\x65\x6E\x73\x69\x6F\x6E\x20\x6D\x65\x74\x68\x6F\x64\x20\x27\x43\x54\x43\x6C\x65\x61\x72\x54\x61\x67\x73\x27\x20\x69\x6E\x73\x74\x65\x61\x64"), NULL);
	}
}
static void BaseHelper_t06A43A26211DD50047AB61E90EC3DCF06758E347_CustomAttributesCacheGenerator_BaseHelper_ClearSpaces_m235DA279F449F5AD4E61A2F82D5C414DB86322CD(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x74\x68\x65\x20\x65\x78\x74\x65\x6E\x73\x69\x6F\x6E\x20\x6D\x65\x74\x68\x6F\x64\x20\x27\x43\x54\x43\x6C\x65\x61\x72\x53\x70\x61\x63\x65\x73\x27\x20\x69\x6E\x73\x74\x65\x61\x64"), NULL);
	}
}
static void BaseHelper_t06A43A26211DD50047AB61E90EC3DCF06758E347_CustomAttributesCacheGenerator_BaseHelper_ClearLineEndings_m8A5A3B1AFCE29A057A7D0EF2DE7576734251456D(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x74\x68\x65\x20\x65\x78\x74\x65\x6E\x73\x69\x6F\x6E\x20\x6D\x65\x74\x68\x6F\x64\x20\x27\x43\x54\x43\x6C\x65\x61\x72\x4C\x69\x6E\x65\x45\x6E\x64\x69\x6E\x67\x73\x27\x20\x69\x6E\x73\x74\x65\x61\x64"), NULL);
	}
}
static void BaseHelper_t06A43A26211DD50047AB61E90EC3DCF06758E347_CustomAttributesCacheGenerator_BaseHelper_InvokeMethod_m16B910E20A79713DF61CAF991EEC69FD5832FD5D____parameters2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void U3CU3Ec_tD5BCCFF09947F66BC5DF4853A963AE93DD797BCF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTHelper_t501C6E12D53FDF522624C5D95FDDA59A2ABCE227_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTHelper_t501C6E12D53FDF522624C5D95FDDA59A2ABCE227_CustomAttributesCacheGenerator_CTHelper_get_Instance_m9419E0AE2630EC0841937F6CDE83B330BA39D6C9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTHelper_t501C6E12D53FDF522624C5D95FDDA59A2ABCE227_CustomAttributesCacheGenerator_CTHelper_set_Instance_m7D72BBE9FA37F24A47B37E2068BA10A6FD30ED40(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTHelper_t501C6E12D53FDF522624C5D95FDDA59A2ABCE227_CustomAttributesCacheGenerator_CTHelper_initialize_m4E427FE7AE92A937936DE03A4AE592CA7B1E3BB5(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 1LL, NULL);
	}
}
static void CTHelper_t501C6E12D53FDF522624C5D95FDDA59A2ABCE227_CustomAttributesCacheGenerator_CTHelper_create_m8BCAD76807872BA63E2E48039A13923C9AD30FDD(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mAEDC96FCA281601682E7207BD386A1553C1B6081(tmp, NULL);
	}
}
static void CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_U3CTimeoutU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_U3CConnectionLimitU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_CTWebClient_get_Timeout_m05E7AC7AD82CFD0A255A1D753B3AB413E0C15C9E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_CTWebClient_set_Timeout_mC93FB85FA7A3F30D35CE10A64A25ABD18D206079(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_CTWebClient_get_ConnectionLimit_m74A0F4DC526B36E55EA0857E3F953453622189CE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_CTWebClient_set_ConnectionLimit_mFBDBF8F1C18BC022FC5B773E3874E8B9569F9068(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileHelper_t3637C5C131C495F206247B3FE3CF746E3E02E1E5_CustomAttributesCacheGenerator_FileHelper_initialize_m6603998B02EF452F39EA024FE1770F766C11EBC6(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mAEDC96FCA281601682E7207BD386A1553C1B6081(tmp, NULL);
	}
}
static void FileHelper_t3637C5C131C495F206247B3FE3CF746E3E02E1E5_CustomAttributesCacheGenerator_FileHelper_GetFiles_m599BE482715EA35B5198B77E79713FC266FFACB9____extensions2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void U3CU3Ec_t94A0EE37A5F77540C8A1B1B3DF2C7AB0E9A6EAC8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tE1D16066DF4862D0A51881B843A4FC55563A25F8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Singleton_1_t83DE2710B40EE32173A76E5AA92D1605456F7F67_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void Singleton_1_t83DE2710B40EE32173A76E5AA92D1605456F7F67_CustomAttributesCacheGenerator_dontDestroy(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x6F\x6E\x27\x74\x20\x64\x65\x73\x74\x72\x6F\x79\x20\x67\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x20\x64\x75\x72\x69\x6E\x67\x20\x73\x63\x65\x6E\x65\x20\x73\x77\x69\x74\x63\x68\x65\x73\x20\x28\x64\x65\x66\x61\x75\x6C\x74\x3A\x20\x66\x61\x6C\x73\x65\x29\x2E"), NULL);
	}
}
static void SingletonHelper_tB766F047E35A58385809F9EB65029AF836DC54FB_CustomAttributesCacheGenerator_U3CisQuittingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SingletonHelper_tB766F047E35A58385809F9EB65029AF836DC54FB_CustomAttributesCacheGenerator_SingletonHelper_get_isQuitting_mA9E2C23D74C48DCBB8FF9D9380A6C2F279B840C0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SingletonHelper_tB766F047E35A58385809F9EB65029AF836DC54FB_CustomAttributesCacheGenerator_SingletonHelper_set_isQuitting_m53D01E30152CDE243B85A357E5B55BAF6F346169(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SingletonHelper_tB766F047E35A58385809F9EB65029AF836DC54FB_CustomAttributesCacheGenerator_SingletonHelper_initialize_mC53AE8824DF8FAAD29911D0F684E9174D96551DB(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 1LL, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t2672E9E7EEBD118534BF12C6E47D2B3DD7D9E562_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[645] = 
{
	U3CU3Ec_t5DC7F6288A2155D9D8A64DCBF4B08ED1E18371DD_CustomAttributesCacheGenerator,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_tE339584696C7D04D095E648B21D42B2849672875_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_t9ABDA718E418E4638E6BC97DA82D2CF1AD924F14_CustomAttributesCacheGenerator,
	U3CU3Ec__54_1_t82C36EA4E1E7BC0058F422BD22834CE9E81E869D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass81_0_tC0F6FBCADDA07A1F2590A4C33D5BBF7523311811_CustomAttributesCacheGenerator,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator,
	U3CRefreshYieldU3Ed__129_tBF0CB254F9A178BECCDC29B113049D5A390DB47A_CustomAttributesCacheGenerator,
	U3CwwwCheckU3Ed__131_tDC196BCEE13348B800156D6ECEBDA9ADC921569E_CustomAttributesCacheGenerator,
	U3Cgoogle204CheckU3Ed__132_t45EF56476E3EE3D120644CC1647818DEDA335989_CustomAttributesCacheGenerator,
	U3CgoogleBlankCheckU3Ed__133_t4FAE0B9E4E059EE4C8578C9BA9F0AD5742CF648F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass135_0_t4865BD66DDE04748CE0E3EE7E7D2D2D0FC9F21F9_CustomAttributesCacheGenerator,
	U3CstartWorkerU3Ed__135_t0692C6D98709C1ECE5AB38F4FA78CF8FA0FA1748_CustomAttributesCacheGenerator,
	U3CinternetCheckU3Ed__136_t88AFCE419C8A3E7E686E64624EEDAD6C0BD02CD0_CustomAttributesCacheGenerator,
	U3CU3Ec_t5851270268B0137F4EEFC3C295A18073723CE084_CustomAttributesCacheGenerator,
	CustomCheck_tB95B254C3D40A077697776496FA29E32508C879B_CustomAttributesCacheGenerator,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator,
	U3CpingU3Ed__41_t6828EFFEDDA314F733151F7CB92A7C4D6B15BC21_CustomAttributesCacheGenerator,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass57_0_tC6A242B53522E5BF4F3F5CE26E06357966B0C17D_CustomAttributesCacheGenerator,
	U3CtestU3Ed__57_t39BF4B4A0B29F9833FB294F4058427265F922159_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_t4694567A5E71A80C9D25860046825262BF524A03_CustomAttributesCacheGenerator,
	U3CU3Ec_t0807953E6AC5D454A7D61CD20A6E1FEF115EB75F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_t81A42CD06887C4F7902C8E0C0E40063434FCBAC7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_1_tB900CC9E31E22EF06649B9D677B1F5134A16356F_CustomAttributesCacheGenerator,
	U3CU3Ec__9_1_t69D9342A982FABDAD539DC3BADA031B8568CC62A_CustomAttributesCacheGenerator,
	U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_CustomAttributesCacheGenerator,
	U3CGetConfigU3Ed__1_1_t27C2E6BD21131A846C1ED0873201EA1DF5599309_CustomAttributesCacheGenerator,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator,
	U3CtestU3Ed__49_tDD0B93F803E5724477C153898A710AFD7F7DC0A3_CustomAttributesCacheGenerator,
	U3CU3Ec_t90D70C0F6DBA5511535940FAC5ED6E61368739A8_CustomAttributesCacheGenerator,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator,
	ServerConfig_t5792ABA39BA8FC204D1B43981B00549F15225BEB_CustomAttributesCacheGenerator,
	ServersList_t890F3EAA1E1DC4517612ED4E1159BC59F7480C6A_CustomAttributesCacheGenerator,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator,
	EventTester_tC539BE8A6FD0C1628E27F7902C77032BC717A07A_CustomAttributesCacheGenerator,
	GUIMain_tAAD610B8E1060640C8477DD60A1BAA342A70B80E_CustomAttributesCacheGenerator,
	GUIScenes_t7C0FF16F7468A283FBDFA7DECA0717005E7303A1_CustomAttributesCacheGenerator,
	ManageEndlessMode_tA0F8BA38466255518EA1D472F021D6DB001E4D11_CustomAttributesCacheGenerator,
	UIDrag_tAD84C1AAD3684943B7420A4EE83C67760BCC97A8_CustomAttributesCacheGenerator,
	UIFocus_t8FA57C45177DF0396463F9D4D9B37C37647BFF40_CustomAttributesCacheGenerator,
	U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator,
	U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator,
	UIResize_t37199FAAC3C3D6AC437662F9CF19B9ADDB91F228_CustomAttributesCacheGenerator,
	UIWindowManager_t227DE834418A39C678BF8498F47C8743E421AB46_CustomAttributesCacheGenerator,
	FPSDisplay_tE1C362120A52899D78B55F41C6639FE89D2AF6F4_CustomAttributesCacheGenerator,
	CTScreenshot_tB6F2227DD4079BB4E195E1DCA7FC02B71B3AAB03_CustomAttributesCacheGenerator,
	U3CU3Ec_t07D8486F1A2DBA03AA4D92725468785D45EBD4E3_CustomAttributesCacheGenerator,
	RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator,
	RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator,
	U3CU3Ec_tD5BCCFF09947F66BC5DF4853A963AE93DD797BCF_CustomAttributesCacheGenerator,
	U3CU3Ec_t94A0EE37A5F77540C8A1B1B3DF2C7AB0E9A6EAC8_CustomAttributesCacheGenerator,
	U3CU3Ec_tE1D16066DF4862D0A51881B843A4FC55563A25F8_CustomAttributesCacheGenerator,
	Singleton_1_t83DE2710B40EE32173A76E5AA92D1605456F7F67_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t2672E9E7EEBD118534BF12C6E47D2B3DD7D9E562_CustomAttributesCacheGenerator,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_endlessMode,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_intervalMin,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_intervalMax,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_timeout,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_forceWWW,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_customCheck,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_microsoft,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_google204,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_googleBlank,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_apple,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_ubuntu,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_runOnStart,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_delay,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_U3CisInternetAvailableU3Ek__BackingField,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_U3CLastCheckU3Ek__BackingField,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_U3CDataDownloadedU3Ek__BackingField,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnStatusChange,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnOnlineStatusChange,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnNetworkReachabilityChange,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnOnlineCheckComplete,
	CustomCheck_tB95B254C3D40A077697776496FA29E32508C879B_CustomAttributesCacheGenerator_URL,
	CustomCheck_tB95B254C3D40A077697776496FA29E32508C879B_CustomAttributesCacheGenerator_ExpectedData,
	CustomCheck_tB95B254C3D40A077697776496FA29E32508C879B_CustomAttributesCacheGenerator_DataMustBeEquals,
	CustomCheck_tB95B254C3D40A077697776496FA29E32508C879B_CustomAttributesCacheGenerator_UseOnlyCustom,
	CustomCheck_tB95B254C3D40A077697776496FA29E32508C879B_CustomAttributesCacheGenerator_ShowErrors,
	CustomCheck_tB95B254C3D40A077697776496FA29E32508C879B_CustomAttributesCacheGenerator_HeaderSize,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_hostName,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_timeout,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_runOnStart,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_OnPingComplete,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_OnPingCompleted,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_U3CLastHostU3Ek__BackingField,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_U3CLastIPU3Ek__BackingField,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_U3CisBusyU3Ek__BackingField,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPProxyURL,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPProxyPort,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPProxyUsername,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPProxyPassword,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPProxyURLProtocol,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPSProxyURL,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPSProxyPort,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPSProxyUsername,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPSProxyPassword,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_HTTPSProxyURLProtocol,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_EnableOnAwake,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_U3ChasHTTPProxyU3Ek__BackingField,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_U3ChasHTTPSProxyU3Ek__BackingField,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_dataSize,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_smallURL,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_mediumURL,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_largeURL,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_runOnStart,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_OnSpeedTestComplete,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_OnTestCompleted,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_U3CLastURLU3Ek__BackingField,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_U3CLastDataSizeU3Ek__BackingField,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_U3CLastDurationU3Ek__BackingField,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_U3CLastSpeedU3Ek__BackingField,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_U3CisBusyU3Ek__BackingField,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_testDownload,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_testUpload,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_runOnStart,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_OnSpeedTestComplete,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_OnTestCompleted,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_U3CLastServerU3Ek__BackingField,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_U3CLastDurationU3Ek__BackingField,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_U3CLastDownloadSpeedU3Ek__BackingField,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_U3CLastUploadSpeedU3Ek__BackingField,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_U3CisBusyU3Ek__BackingField,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_U3CIpU3Ek__BackingField,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_U3CLatitudeU3Ek__BackingField,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_U3CLongitudeU3Ek__BackingField,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_U3CIspU3Ek__BackingField,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_U3CIspRatingU3Ek__BackingField,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_U3CRatingU3Ek__BackingField,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_U3CIspAvarageDownloadSpeedU3Ek__BackingField,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_U3CIspAvarageUploadSpeedU3Ek__BackingField,
	Coordinate_tF261EB8C468E414B19D067355CB7B0041751AEAF_CustomAttributesCacheGenerator_U3CLatitudeU3Ek__BackingField,
	Coordinate_tF261EB8C468E414B19D067355CB7B0041751AEAF_CustomAttributesCacheGenerator_U3CLongitudeU3Ek__BackingField,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_U3CTestLengthU3Ek__BackingField,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_U3CInitialTestU3Ek__BackingField,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_U3CMinTestSizeU3Ek__BackingField,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_U3CThreadsPerUrlU3Ek__BackingField,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CIdU3Ek__BackingField,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CCountryU3Ek__BackingField,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CSponsorU3Ek__BackingField,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CHostU3Ek__BackingField,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CUrlU3Ek__BackingField,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CLatitudeU3Ek__BackingField,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CLongitudeU3Ek__BackingField,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CDistanceU3Ek__BackingField,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_U3CLatencyU3Ek__BackingField,
	ServerConfig_t5792ABA39BA8FC204D1B43981B00549F15225BEB_CustomAttributesCacheGenerator_U3CIgnoreIdsU3Ek__BackingField,
	ServersList_t890F3EAA1E1DC4517612ED4E1159BC59F7480C6A_CustomAttributesCacheGenerator_U3CServersU3Ek__BackingField,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_U3CClientU3Ek__BackingField,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_U3CTimesU3Ek__BackingField,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_U3CDownloadU3Ek__BackingField,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_U3CUploadU3Ek__BackingField,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_U3CServerConfigU3Ek__BackingField,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_U3CServersU3Ek__BackingField,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_U3CDownload1U3Ek__BackingField,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_U3CDownload2U3Ek__BackingField,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_U3CDownload3U3Ek__BackingField,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_U3CUpload1U3Ek__BackingField,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_U3CUpload2U3Ek__BackingField,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_U3CUpload3U3Ek__BackingField,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_U3CTestLengthU3Ek__BackingField,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_U3CRatioU3Ek__BackingField,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_U3CInitialTestU3Ek__BackingField,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_U3CMinTestSizeU3Ek__BackingField,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_U3CThreadsU3Ek__BackingField,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_U3CMaxChunkSizeU3Ek__BackingField,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_U3CMaxChunkCountU3Ek__BackingField,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_U3CThreadsPerUrlU3Ek__BackingField,
	GUIMain_tAAD610B8E1060640C8477DD60A1BAA342A70B80E_CustomAttributesCacheGenerator_Available,
	GUIScenes_t7C0FF16F7468A283FBDFA7DECA0717005E7303A1_CustomAttributesCacheGenerator_PreviousScene,
	GUIScenes_t7C0FF16F7468A283FBDFA7DECA0717005E7303A1_CustomAttributesCacheGenerator_NextScene,
	UIFocus_t8FA57C45177DF0396463F9D4D9B37C37647BFF40_CustomAttributesCacheGenerator_ManagerName,
	UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_Group,
	UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_Delay,
	UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_FadeTime,
	UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_Disable,
	UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_FadeAtStart,
	UIResize_t37199FAAC3C3D6AC437662F9CF19B9ADDB91F228_CustomAttributesCacheGenerator_MinSize,
	UIResize_t37199FAAC3C3D6AC437662F9CF19B9ADDB91F228_CustomAttributesCacheGenerator_MaxSize,
	UIResize_t37199FAAC3C3D6AC437662F9CF19B9ADDB91F228_CustomAttributesCacheGenerator_IgnoreMaxSize,
	UIResize_t37199FAAC3C3D6AC437662F9CF19B9ADDB91F228_CustomAttributesCacheGenerator_SpeedFactor,
	UIWindowManager_t227DE834418A39C678BF8498F47C8743E421AB46_CustomAttributesCacheGenerator_Windows,
	WindowManager_t8F0E4C96CAFAD4C11F999874779FE1B04137E0ED_CustomAttributesCacheGenerator_Speed,
	WindowManager_t8F0E4C96CAFAD4C11F999874779FE1B04137E0ED_CustomAttributesCacheGenerator_Dependencies,
	WindowManager_t8F0E4C96CAFAD4C11F999874779FE1B04137E0ED_CustomAttributesCacheGenerator_ClosedAtStart,
	FPSDisplay_tE1C362120A52899D78B55F41C6639FE89D2AF6F4_CustomAttributesCacheGenerator_FPS,
	FPSDisplay_tE1C362120A52899D78B55F41C6639FE89D2AF6F4_CustomAttributesCacheGenerator_FrameUpdate,
	FPSDisplay_tE1C362120A52899D78B55F41C6639FE89D2AF6F4_CustomAttributesCacheGenerator_Key,
	AudioFilterController_tE30989E3E8CD9E8A8454160C6048BFA26D77EDE8_CustomAttributesCacheGenerator_FindAllAudioFiltersOnStart,
	AudioFilterController_tE30989E3E8CD9E8A8454160C6048BFA26D77EDE8_CustomAttributesCacheGenerator_ResetAudioFiltersOnStart,
	AudioFilterController_tE30989E3E8CD9E8A8454160C6048BFA26D77EDE8_CustomAttributesCacheGenerator_ReverbFilterDropdown,
	AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_FindAllAudioSourcesOnStart,
	AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_AudioSources,
	AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_ResetAudioSourcesOnStart,
	AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_Mute,
	AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_Loop,
	AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_Volume,
	AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_Pitch,
	AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_StereoPan,
	AudioSourceController_t82B6696D73E1A1283325E09E9A72A26EE6ABC84D_CustomAttributesCacheGenerator_VolumeText,
	CTScreenshot_tB6F2227DD4079BB4E195E1DCA7FC02B71B3AAB03_CustomAttributesCacheGenerator_Prefix,
	CTScreenshot_tB6F2227DD4079BB4E195E1DCA7FC02B71B3AAB03_CustomAttributesCacheGenerator_Scale,
	CTScreenshot_tB6F2227DD4079BB4E195E1DCA7FC02B71B3AAB03_CustomAttributesCacheGenerator_KeyCode,
	CTScreenshot_tB6F2227DD4079BB4E195E1DCA7FC02B71B3AAB03_CustomAttributesCacheGenerator_ShowFileLocation,
	PlatformController_t0109B938FCC3CF39CF034B9211211DB02853B670_CustomAttributesCacheGenerator_Platforms,
	PlatformController_t0109B938FCC3CF39CF034B9211211DB02853B670_CustomAttributesCacheGenerator_Active,
	PlatformController_t0109B938FCC3CF39CF034B9211211DB02853B670_CustomAttributesCacheGenerator_Objects,
	PlatformController_t0109B938FCC3CF39CF034B9211211DB02853B670_CustomAttributesCacheGenerator_Scripts,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_UseInterval,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_ChangeInterval,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_HueRange,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_SaturationRange,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_ValueRange,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_AlphaRange,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_GrayScale,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_Material,
	RandomColor_tF868105D2E2823ACD264C32AACFC0C572FFC4C4A_CustomAttributesCacheGenerator_RandomColorAtStart,
	RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_UseInterval,
	RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_ChangeInterval,
	RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_SpeedMin,
	RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_SpeedMax,
	RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_RandomRotationAtStart,
	RandomRotator_t2F779136B71B324B8401B297A8978D14D82F4AF8_CustomAttributesCacheGenerator_RandomChangeIntervalPerAxis,
	RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_UseInterval,
	RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_ChangeInterval,
	RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_ScaleMin,
	RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_ScaleMax,
	RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_Uniform,
	RandomScaler_tBD5EA91666AEFA8CF01ED1DD1C749F50A5DB84D3_CustomAttributesCacheGenerator_RandomScaleAtStart,
	CTHelper_t501C6E12D53FDF522624C5D95FDDA59A2ABCE227_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_U3CTimeoutU3Ek__BackingField,
	CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_U3CConnectionLimitU3Ek__BackingField,
	Singleton_1_t83DE2710B40EE32173A76E5AA92D1605456F7F67_CustomAttributesCacheGenerator_dontDestroy,
	SingletonHelper_tB766F047E35A58385809F9EB65029AF836DC54FB_CustomAttributesCacheGenerator_U3CisQuittingU3Ek__BackingField,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToTitleCase_m0FAF40693A916D1BA985195E30AA7B9753C11DE2,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTReverse_m375B1B5F94367BC73BC054481C4528E1829758A0,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTReplace_mE45837F5BF2ECDD64251F7DC85303D36308AFBB9,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTEquals_m70FD226B78B4DD7ED0672F326B16568EA3D9B446,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTContains_m52B3F3D1019BBD1746371264A2671656CDADB3C2,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTContainsAny_m50A29F87C6A50663D9B599FEA1D1359EC07969A3,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTContainsAll_mAB9F9024F8E76F4449FE666602718634956C2464,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTRemoveNewLines_mF3F6F1FD9F609208AF748156579F02AB68B15C24,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTAddNewLines_m3FDE4E74DB180C8F829329BFFD4934CB93057581,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTisNumeric_mB664DBA85A056E3855D912C93DA7B704584752DC,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTisInteger_m9A0F9A80C8B5CDADCD3500534A9FF53271BC5EA3,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTisEmail_m2336B119F8D08E08DF5335FEAC1699978FC6405F,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTisWebsite_m7DC7369502F9A000269EEC781ABA71452667E85B,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTisCreditcard_m62CDA4DAC05FE6307258BB4626FF76CF939C3C78,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTisIPv4_mA58907D12ED35BC0D4A3277E8475EF095B440126,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTisAlphanumeric_m2E1A017EEB60FF2F1E2391C0074E81B406A98A2A,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CThasLineEndings_m95A836BD2BE4FFFFD877D5FD894AADF33428A129,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CThasInvalidChars_m3C830A06687FDE6DE20154B64E2E4D531B7A9ED4,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTStartsWith_mFCFFAAF1BEE786BD337B3B2296D32744AB0A2542,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTEndsWith_m666D85A99AD0967EBD41A838F37819D298D23ADF,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTLastIndexOf_m2F16141C163B099C5BCDF1D4E3426834EAD6A863,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTIndexOf_m3C965AE80FE82E5E12EC02B9019CF48B1D16B1C5,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTIndexOf_mD1B263B1E864EE567DD4C711597BA5556B0657E3,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToBase64_m95D893BE1209C12BFC96FDCEA54371C1741ED35B,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFromBase64_m5018159F11AAAD7AAE89A93D795932112BB18D1C,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFromBase64ToByteArray_m66CADB7E423C7A554BB4B0069FCD5665402C67DE,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToHex_mE0934C22838185424921F80774725E4F5CD5FEED,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTHexToString_m93D0EABF83200F514CC04FEAB3036266634B67B9,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTHexToColor32_mD1A93B26BC2D250FEA2834700CAC106519A62771,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTHexToColor_m1FD7E347868D5E6A6450C36DF93DABAE02C468AC,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToByteArray_m8BBB5E420285A72B5F964FF51944FB972837AE8A,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTClearTags_m6F0896B414261998FD998D185D75F03B3151875E,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTClearSpaces_m39BCF0466C31347D3DB7BAF627B2AF586CCC11B3,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTClearLineEndings_m923BE72DD4DC59DB6307F90F2ABC30A90DBB692D,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTShuffle_m69617262C775678CA0CFFF468C20E275ECABD211,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m42F2A1DBC9450A01E998ADE7347D234875962FFD,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m9372AF9431740E725E0A0EE720EEB4B51EF8D626,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mDD2831261D66781D294ADBFFBD9A397D4A5B4681,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m2A792DFFEECEFF18E99A289916B9165C127E7883,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mEDCBC052999C7F88ECF5897E865B6467FF1F452E,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToString_m181115D9882BBB5E201C31443FCFA49DBADE6D30,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToFloatArray_m7E2B709CDA0C352394C296884A120A4FFB1576A3,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToByteArray_m11AEF2E58D65C64EF7BBE114385999E73726E236,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToTexture_m12DC9C6DC82C07A891F5EA4AC628DEBB737C5DB2,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToSprite_mD7FF7050F7C0BE3EE55C86A445CF3559C0D07BA3,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToString_mBAB4670FE933D06A3F39F51219830F8D3CA946F5,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToBase64_m0E9D5F1F9174FAA4776F788ECA88A8709A97BCE5,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTShuffle_mB67EA3011C43B440B75C4B90B8719323BEE020E4,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m9E61BE84CB474608968636B8A40B1C38179FC784,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m59F8DE0A578B3B891B4D8C1ED38688E86940F976,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_m6E14577C017276ACD5DA587C029A664694E859C9,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mD3A9D8E4C2FAF2BE628602C89238233DBE170FDE,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mB00AF342A1C001008DDEE5422E6CE9386ADF044C,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToString_m21B12643EEC617E394A1A8C5DB57D3DA63EE0A36,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTDump_mE3C3EB3222E33AA52D07C720D926EF8D29F6F21B,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTAddRange_mA8E3A743EA575918A5B29284CB5CEDD965D88261,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTReadFully_mCD211B8D82B74DE675FB131FBAC880B034B7B553,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToHexRGB_mEB271EF7DE0D38158B4D1C9E1F5A2DCAD7B97502,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToHexRGB_m1E39198A3F7FD8F35CC5BD27984C09FD5CDFEC5A,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToHexRGBA_m1E583B32CC5214340E8C18EA005281D8748D689B,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToHexRGBA_m8BF2160A9E4353077DBDE0F048F2B4000FC877EF,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTVector3_mCBBA0D200DA6E33DFDF5F968F64682AF279A1F72,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTVector3_m8CFAC4C85BA2694A83E2BDFB3A43B317005BDFE6,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTVector4_m4CA428D976ADBCA1876085C9942F953F90DC6592,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTVector4_m3E12FD9E6E3310BE328E10AB105D3ADCFFF49522,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTMultiply_m305DB38572F0613C1602758541DE685CD229B168,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTMultiply_mFF0ED38A9DA8F4B6A0D650560279442844DEA6AE,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFlatten_mC4047C6959F5B6BC261F88F4790682FCFC383CC1,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTQuaternion_m462BF09597B2909DB2D1128548CB3B1E3D0319A1,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTColorRGB_m39E122A470F7CAA46A440D03B2DDB4AB1D8B302B,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTMultiply_m5C8CA96F97B488BC1E1DD14854E6603037247CFB,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTQuaternion_m4A9E9712734553E922C7789A7051CF06A384A0B0,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTColorRGBA_m0423B016C705D61B4186C44E18B5E4A168598A7A,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTVector3_m1F8621C9295026E750D0342405B2118B874F52E4,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTVector4_m1F9C3149D5060CED2F9DFFF8A3CB39E95ABC5729,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTCorrectLossyScale_m8B4CB2FCAA60A49AEA8FBA793EE49D69AA7B11F7,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetLocalCorners_m866B0CC7B69D7A4E17E65B2D870CBED28F9C7307,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetLocalCorners_m93E67F2D92BA9C9F54D41CA6769ADE481D9208AF,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetScreenCorners_m2F1E5E3409D23E804EB07803B1317339FDF28B87,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetScreenCorners_m39B94363A1BB9CAA84AC5F4D7C6F652FA07D35E0,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetBounds_m2CB978298B520DEDC083F4E6F763D89534850569,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTSetLeft_m7241C29911EB32D2BF73B4F20ED2F0B8E1E465DF,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTSetRight_m7B0B7371C3243430FE05D24A4EC4E466F53E1C53,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTSetTop_m7E0B0D5B5F497660AF71936B287C82378579FB40,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTSetBottom_m2DBDB41D978F0AC8191285887530BD93F1CD73BF,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetLeft_m81CE42F10665F9DB57E8E9E1E2071CC82CE56202,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetRight_mC83506F109D5492465A5B662C8B44A21BA3A4A33,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetTop_m99A1549D99E13343E61F5EDEA3F8888D64A56C5F,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetBottom_mA70B416EF67700E19A23E6ABDFA862839B45688E,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetLRTB_mCA1C1A546BA77B9FDA6394815F879F8E91EFA733,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTSetLRTB_m5578C7E73B1BA1C8F17854D347D2EFFDBBE8FC23,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFind_m50A483F38921A7B82DB1B279AD2C2A0DCD213B6C,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFind_m665862ACE1F1529E1D5991AA5B2EB50EAB5845BD,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFind_mA46461FE50A61DA998A72004CDF0BA8EA85275C6,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFind_m5DC06F8BBD60300DC5D2EDC08B60E1B7DD68F4F1,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTGetBounds_mC52C517BCE211AA80A937DFB6BBDA57B5365ACE7,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFind_mC05248D7AFAA90746B748E4020E169E1375B6988,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFind_m6796E1BCF88F019C60970603ACB4C198544D2464,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToPNG_m9287BB5CA9F16C447688DF15869B3CB73C8D2EAB,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToJPG_m9ED2E438345EDC4ECEBBAD7BBF0C84AF3833956C,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToTGA_m4130CAC5FA95B713F6FE6E10056940AFE45CAD9C,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToEXR_mD8FB2B651C2ED92132E2A06D332FE7D7F9A99310,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToPNG_mDC05E791A75ED3F86DD665B80986D0A332FF5193,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToJPG_mCD0C94CDD3306AC2B24B63DEF8CF561CCDB1C454,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToTGA_mE9EA3A0A4E9BC648F5291C7C445B5A394CB460FD,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToEXR_m3BC7385919EB0ABC13BFA69C725FAE5281112DBB,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToSprite_m036A44049C3B80B9A4F9D4D4651D50092F0B289C,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTRotate90_mD1AB4357D802DB346FDD7FAC48ACDDBF9E12EC20,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTRotate180_m9B592620D061F71ECF700F0DEA2436C8D24FEB21,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTRotate270_m384C9E9CA403E44863115554B93B988457F1CE63,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTToTexture2D_m9A7AE558EB1AE7B2CE6CF7B74F77855AB0E3573F,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFlipHorizontal_m1A71CDC27223F806C143D8F1D7E51DECF5A202C4,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTFlipVertical_m9B31708827E99F4154C9B40E4233852D497CFB62,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTHasActiveClip_m01F0DA4A380C00C95759FDA648D8D51289CC492E,
	ExtensionMethods_t3A63E01D154FF156B5910983BBD9656044FCBAEF_CustomAttributesCacheGenerator_ExtensionMethods_CTIsVisibleFrom_m5E3DED552FD8B132DD4E2B700E4BC17BCE83E587,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_get_isInternetAvailable_mB8D7ADA954F61774006B5B0CC83EF0BA926241ED,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_set_isInternetAvailable_m300C7DBB2A346EE1446CCA0C261B078F31D23AE1,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_get_LastCheck_mB662D5E64AEFC884220C17267A75C630F94809AB,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_set_LastCheck_m67A510F9D0DE82359817CFE6A0996F744A8A1C4C,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_get_DataDownloaded_m4E94946086AE3150005F2BE61C03E2A4A715D17E,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_set_DataDownloaded_mFCA7AA53E70908620761FB4814D07F3442F07A23,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_add_OnOnlineStatusChange_mE60151295E3C4E4453C8D3E317490B3C1B58007C,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_remove_OnOnlineStatusChange_m191B25B8B2F624F83CD491A5B8D14ED50C3FEC86,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_add_OnNetworkReachabilityChange_m938758CE29A4DEEFC3653C02C9BF15F43673D1C7,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_remove_OnNetworkReachabilityChange_m7D657C300452F527D4BDD45F55B0A14248F67B50,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_add_OnOnlineCheckComplete_mFB6BAEF23BA67D0BD31DF06481A1405680AE8BAC,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_remove_OnOnlineCheckComplete_mFA97D1EEFF914837E2BBF273EC72F1C3F7AC1CDC,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_RefreshYield_mA1A4B0E79DC8083648A98F1BB2998F2B6469C10E,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_wwwCheck_m3CB186B15336CE3CE92F62C87806A59B94BC8409,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_google204Check_mB487FAC2EDF6B7CC49AB7D521194D04B7E4951A6,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_googleBlankCheck_m83DA4A965E7A9D70219A28CBD4660A8B27FF52D5,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_startWorker_m712AE1F1F6163081D7ACAA1440E80FB909C84697,
	OnlineCheck_t9B2A607820E915E76A2403AFC60256FD0AD5F8F6_CustomAttributesCacheGenerator_OnlineCheck_internetCheck_m26BC7810D3F1627A1C90F0160374E8822C6D2092,
	U3CRefreshYieldU3Ed__129_tBF0CB254F9A178BECCDC29B113049D5A390DB47A_CustomAttributesCacheGenerator_U3CRefreshYieldU3Ed__129__ctor_m0932B7AF205C1AFAC47F2B6F0B67AA4AFB84835C,
	U3CRefreshYieldU3Ed__129_tBF0CB254F9A178BECCDC29B113049D5A390DB47A_CustomAttributesCacheGenerator_U3CRefreshYieldU3Ed__129_System_IDisposable_Dispose_m9EFA937D9FC3AA4516183A844F32319214C5C26A,
	U3CRefreshYieldU3Ed__129_tBF0CB254F9A178BECCDC29B113049D5A390DB47A_CustomAttributesCacheGenerator_U3CRefreshYieldU3Ed__129_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDEB14477B0E0486BA7DD5773A134C59B9B1A05D4,
	U3CRefreshYieldU3Ed__129_tBF0CB254F9A178BECCDC29B113049D5A390DB47A_CustomAttributesCacheGenerator_U3CRefreshYieldU3Ed__129_System_Collections_IEnumerator_Reset_m9F40BB9040C52254158400AA661985396BB26A4C,
	U3CRefreshYieldU3Ed__129_tBF0CB254F9A178BECCDC29B113049D5A390DB47A_CustomAttributesCacheGenerator_U3CRefreshYieldU3Ed__129_System_Collections_IEnumerator_get_Current_m0F62B1BADD0DAB5B954EB34F5DBDEA1F9199A179,
	U3CwwwCheckU3Ed__131_tDC196BCEE13348B800156D6ECEBDA9ADC921569E_CustomAttributesCacheGenerator_U3CwwwCheckU3Ed__131__ctor_mB464818753B324E9DF2E095A7516F092E22998DE,
	U3CwwwCheckU3Ed__131_tDC196BCEE13348B800156D6ECEBDA9ADC921569E_CustomAttributesCacheGenerator_U3CwwwCheckU3Ed__131_System_IDisposable_Dispose_mAC2958080A5562AF837FC380C4521D6F32A9A92B,
	U3CwwwCheckU3Ed__131_tDC196BCEE13348B800156D6ECEBDA9ADC921569E_CustomAttributesCacheGenerator_U3CwwwCheckU3Ed__131_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m078EC1D91098DC6F1C9777A6530976909F538E9A,
	U3CwwwCheckU3Ed__131_tDC196BCEE13348B800156D6ECEBDA9ADC921569E_CustomAttributesCacheGenerator_U3CwwwCheckU3Ed__131_System_Collections_IEnumerator_Reset_m61592DD78BFB0F4476608325FAE6010134CD4E39,
	U3CwwwCheckU3Ed__131_tDC196BCEE13348B800156D6ECEBDA9ADC921569E_CustomAttributesCacheGenerator_U3CwwwCheckU3Ed__131_System_Collections_IEnumerator_get_Current_m0011E9B3464F6805E58DB9A63C700656204AAE70,
	U3Cgoogle204CheckU3Ed__132_t45EF56476E3EE3D120644CC1647818DEDA335989_CustomAttributesCacheGenerator_U3Cgoogle204CheckU3Ed__132__ctor_mD56DB6A6123E95B4FB5A2DD3468C7DCAD96AB020,
	U3Cgoogle204CheckU3Ed__132_t45EF56476E3EE3D120644CC1647818DEDA335989_CustomAttributesCacheGenerator_U3Cgoogle204CheckU3Ed__132_System_IDisposable_Dispose_m258A151CB942BDEFDB9F047FA0C28039BC3B5201,
	U3Cgoogle204CheckU3Ed__132_t45EF56476E3EE3D120644CC1647818DEDA335989_CustomAttributesCacheGenerator_U3Cgoogle204CheckU3Ed__132_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FB0EF6D56A3488E55A4367B308EF4119847C346,
	U3Cgoogle204CheckU3Ed__132_t45EF56476E3EE3D120644CC1647818DEDA335989_CustomAttributesCacheGenerator_U3Cgoogle204CheckU3Ed__132_System_Collections_IEnumerator_Reset_mE5CDB8B60C91B9F9030380F2E98B378B16B7A63A,
	U3Cgoogle204CheckU3Ed__132_t45EF56476E3EE3D120644CC1647818DEDA335989_CustomAttributesCacheGenerator_U3Cgoogle204CheckU3Ed__132_System_Collections_IEnumerator_get_Current_m55BA33D3303B0EC5B915D9E93BD8FEEA94C646E0,
	U3CgoogleBlankCheckU3Ed__133_t4FAE0B9E4E059EE4C8578C9BA9F0AD5742CF648F_CustomAttributesCacheGenerator_U3CgoogleBlankCheckU3Ed__133__ctor_m8F291E2D659EF39C62FC156BCA2D389587B72630,
	U3CgoogleBlankCheckU3Ed__133_t4FAE0B9E4E059EE4C8578C9BA9F0AD5742CF648F_CustomAttributesCacheGenerator_U3CgoogleBlankCheckU3Ed__133_System_IDisposable_Dispose_m6823EFCDFC68DDFA593DA76CE12CD88DC653FACE,
	U3CgoogleBlankCheckU3Ed__133_t4FAE0B9E4E059EE4C8578C9BA9F0AD5742CF648F_CustomAttributesCacheGenerator_U3CgoogleBlankCheckU3Ed__133_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFCB73C941646FEA8753CFC711C28E0174B474F78,
	U3CgoogleBlankCheckU3Ed__133_t4FAE0B9E4E059EE4C8578C9BA9F0AD5742CF648F_CustomAttributesCacheGenerator_U3CgoogleBlankCheckU3Ed__133_System_Collections_IEnumerator_Reset_mC9723822D012EDC85EC1965EF661CE135E28CFB0,
	U3CgoogleBlankCheckU3Ed__133_t4FAE0B9E4E059EE4C8578C9BA9F0AD5742CF648F_CustomAttributesCacheGenerator_U3CgoogleBlankCheckU3Ed__133_System_Collections_IEnumerator_get_Current_m43A1657DB7A5092AA08B859BC0404C0A3635780A,
	U3CstartWorkerU3Ed__135_t0692C6D98709C1ECE5AB38F4FA78CF8FA0FA1748_CustomAttributesCacheGenerator_U3CstartWorkerU3Ed__135__ctor_mBF48C92F9EF46F426C2CEF9BE406C4F51B2DB278,
	U3CstartWorkerU3Ed__135_t0692C6D98709C1ECE5AB38F4FA78CF8FA0FA1748_CustomAttributesCacheGenerator_U3CstartWorkerU3Ed__135_System_IDisposable_Dispose_mA94CB49D92794B0E95C576FFF365B5235A94CA6A,
	U3CstartWorkerU3Ed__135_t0692C6D98709C1ECE5AB38F4FA78CF8FA0FA1748_CustomAttributesCacheGenerator_U3CstartWorkerU3Ed__135_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5404CDC99EF7E2A8AFB47FBCE8153A028333590D,
	U3CstartWorkerU3Ed__135_t0692C6D98709C1ECE5AB38F4FA78CF8FA0FA1748_CustomAttributesCacheGenerator_U3CstartWorkerU3Ed__135_System_Collections_IEnumerator_Reset_m4FC0A6B4B6F41C7E92E85FD01D147C1167139853,
	U3CstartWorkerU3Ed__135_t0692C6D98709C1ECE5AB38F4FA78CF8FA0FA1748_CustomAttributesCacheGenerator_U3CstartWorkerU3Ed__135_System_Collections_IEnumerator_get_Current_m47742F8B1A9854677AD992B38704810BE3BCFF94,
	U3CinternetCheckU3Ed__136_t88AFCE419C8A3E7E686E64624EEDAD6C0BD02CD0_CustomAttributesCacheGenerator_U3CinternetCheckU3Ed__136__ctor_mD5D5AE14F1C3C982EAF77FF103C0FD283EED1C89,
	U3CinternetCheckU3Ed__136_t88AFCE419C8A3E7E686E64624EEDAD6C0BD02CD0_CustomAttributesCacheGenerator_U3CinternetCheckU3Ed__136_System_IDisposable_Dispose_m712C823E815029CFFE4C449601DF8F5F87FB60F0,
	U3CinternetCheckU3Ed__136_t88AFCE419C8A3E7E686E64624EEDAD6C0BD02CD0_CustomAttributesCacheGenerator_U3CinternetCheckU3Ed__136_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA2AF9A7F386BED8090D59D5A41F18DC950D6C4A1,
	U3CinternetCheckU3Ed__136_t88AFCE419C8A3E7E686E64624EEDAD6C0BD02CD0_CustomAttributesCacheGenerator_U3CinternetCheckU3Ed__136_System_Collections_IEnumerator_Reset_mD083701132826FCCEDBEE0047F6A63CC2F0B8C1C,
	U3CinternetCheckU3Ed__136_t88AFCE419C8A3E7E686E64624EEDAD6C0BD02CD0_CustomAttributesCacheGenerator_U3CinternetCheckU3Ed__136_System_Collections_IEnumerator_get_Current_m90BAABE96B6E2DABE6BDDEF853DA73D2C7EF4820,
	SetupProject_tF6E3CA22AB467421EA5835D7A1911FE188FECCA1_CustomAttributesCacheGenerator_SetupProject_setup_m5E022ECA6395D4AF9E1E8095957FEF8AA40A8931,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_add_OnPingCompleted_mEA7E0B5AC5977EAF7382F1469B4FF16F11B60F85,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_remove_OnPingCompleted_m40B6D6EE813A7ADE10EE4611C0293B79C7198282,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_get_LastHost_m28A8825F4DAD9E16E545CE34B9BE3163C503FE78,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_set_LastHost_m2FEDAEFB0E8ABDC31BE0CCD1797BFE4C1400E8C7,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_get_LastIP_mB8FF7A2C0226AC642FE9CFEF7D0F88D334C588EE,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_set_LastIP_mCC80740815D94DF602FC47F60419524A4670A6E3,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_get_isBusy_mE1481885876C2C5992FBD31F6D287C8110E79FEE,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_set_isBusy_m57F3210C204477EDE0220BCF94928541DEF4D280,
	PingCheck_t2B4B8F39C75B5DB518ABDA2E53E1FD32278E74B0_CustomAttributesCacheGenerator_PingCheck_ping_m598086683E20BB9CD8864A8D7772863DD963A91A,
	U3CpingU3Ed__41_t6828EFFEDDA314F733151F7CB92A7C4D6B15BC21_CustomAttributesCacheGenerator_U3CpingU3Ed__41__ctor_m0952AE64F05F49E60C3A7802B2D62D018B6EFBA1,
	U3CpingU3Ed__41_t6828EFFEDDA314F733151F7CB92A7C4D6B15BC21_CustomAttributesCacheGenerator_U3CpingU3Ed__41_System_IDisposable_Dispose_mB7F8AC235AF49225E193B99003ADFD5B42B40DEA,
	U3CpingU3Ed__41_t6828EFFEDDA314F733151F7CB92A7C4D6B15BC21_CustomAttributesCacheGenerator_U3CpingU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6632216B79E50B7860AEAB94FD3A015DDB4E393,
	U3CpingU3Ed__41_t6828EFFEDDA314F733151F7CB92A7C4D6B15BC21_CustomAttributesCacheGenerator_U3CpingU3Ed__41_System_Collections_IEnumerator_Reset_m2F3E9B866A0EDB096B724B46371BC59E78C1CFAD,
	U3CpingU3Ed__41_t6828EFFEDDA314F733151F7CB92A7C4D6B15BC21_CustomAttributesCacheGenerator_U3CpingU3Ed__41_System_Collections_IEnumerator_get_Current_m24F550144A66BCF6840855ABB5AB697CF4115A21,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_Proxy_get_hasHTTPProxy_m1184684C993FD6988A6314C9F0829E2FE199FA3F,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_Proxy_set_hasHTTPProxy_mACFF6FC5482BF6F9BB6AAEED9015B0D9B7A72BAD,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_Proxy_get_hasHTTPSProxy_mFDB3265F542D6C9A234DEDD7C7B661FE04DC48EA,
	Proxy_tEBC121D5FFA824D12BF23EFEF5EEE71789938774_CustomAttributesCacheGenerator_Proxy_set_hasHTTPSProxy_m62EF9B3308635CDA6648D27ACDB529A7DA0BC624,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_add_OnTestCompleted_m8D2C4D2F430B89483F385992F5FC91DEDC91DD42,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_remove_OnTestCompleted_m72DE929DF607E54B47CDB4F40CD1CC3620929D4C,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_get_LastURL_m0468950BED96D6ABD0C83672B44BB85D91C161F5,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_set_LastURL_m9D96892C9D99B85CBE85E9A3F9E2AFD4BA271FCA,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_get_LastDataSize_m4AD20E2A05D4B7C9BBDC59739B849C3EF18EFC77,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_set_LastDataSize_mEA3AD53374F1654942AE07A509737B2C80FB22FD,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_get_LastDuration_m214130E3CFA7560BC58B191ADD7A3A0572C18432,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_set_LastDuration_m25A4AFB4524531A862ACA303BB9962659910C616,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_get_LastSpeed_m3AA25CA1067DC3498E40BC8A4E512C2CEA2A3D62,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_set_LastSpeed_m9BEBAC7801B8E92DF84A90C9E668AE4C71613AF1,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_get_isBusy_m4D7224F998875FB10633C5CE24DEBAC9781A0F5A,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_set_isBusy_m1412307C9B8BC3DC728F9A563B71F7F70AF79D31,
	SpeedTest_t4B276317FE87F29347D2820E932C09D837858740_CustomAttributesCacheGenerator_SpeedTest_test_m565BB9ED0A7C216AB10F7606F7F1E825791E3EBA,
	U3CtestU3Ed__57_t39BF4B4A0B29F9833FB294F4058427265F922159_CustomAttributesCacheGenerator_U3CtestU3Ed__57__ctor_mA55DD118C29DA2907F4E37B2D786A5C8820E2DC9,
	U3CtestU3Ed__57_t39BF4B4A0B29F9833FB294F4058427265F922159_CustomAttributesCacheGenerator_U3CtestU3Ed__57_System_IDisposable_Dispose_m5E2A2F8DD8A744C30E33FD237B0022C719BF7583,
	U3CtestU3Ed__57_t39BF4B4A0B29F9833FB294F4058427265F922159_CustomAttributesCacheGenerator_U3CtestU3Ed__57_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1B97A242FEAA29891CBB0A292158DDB9A2E2698,
	U3CtestU3Ed__57_t39BF4B4A0B29F9833FB294F4058427265F922159_CustomAttributesCacheGenerator_U3CtestU3Ed__57_System_Collections_IEnumerator_Reset_mA927C7FB7D10E03D0383229029126C7DCA117818,
	U3CtestU3Ed__57_t39BF4B4A0B29F9833FB294F4058427265F922159_CustomAttributesCacheGenerator_U3CtestU3Ed__57_System_Collections_IEnumerator_get_Current_m1B867BF9B0DC7EC5A88A3BCBE2D714E4FAC07097,
	SetupProject_tC550EA2E0182F0E06E04CE3390C1B7EFD09E9048_CustomAttributesCacheGenerator_SetupProject_setup_m104E53DC5254FAF89F40F6E1FC378B81E5D5C848,
	SpeedTestClient_tC566A0160BE6447A23272C24523D6BB7CE583F4B_CustomAttributesCacheGenerator_SpeedTestClient_GenerateDownloadUrls_m2161B46C3DEFFBAE2A015999637F68FF37AEAE7E,
	U3CU3Ec_t0807953E6AC5D454A7D61CD20A6E1FEF115EB75F_CustomAttributesCacheGenerator_U3CU3Ec_U3CTestDownloadSpeedU3Eb__7_0_mAA4F34A5FA1AC21845FB0F82E1D1205BDF60FBB1,
	U3CU3CTestDownloadSpeedU3Eb__7_0U3Ed_t5139BC4222B7584C9C6791078DF2E66199658524_CustomAttributesCacheGenerator_U3CU3CTestDownloadSpeedU3Eb__7_0U3Ed_SetStateMachine_m8280F2E096D7E15C71E84AFFA607CE6026649EF0,
	U3CU3Ec__DisplayClass8_0_t81A42CD06887C4F7902C8E0C0E40063434FCBAC7_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass8_0_U3CTestUploadSpeedU3Eb__0_mDAA8CA8010940F8CE9C4FB058D59C2DB1F261512,
	U3CU3CTestUploadSpeedU3Eb__0U3Ed_tDCD2DA70E4DBCAD1F844D600CD63CC92E027EFEF_CustomAttributesCacheGenerator_U3CU3CTestUploadSpeedU3Eb__0U3Ed_SetStateMachine_mE9DCC6B33A1AD21B4B16B4A8EA77136BC3FAE33F,
	U3CU3Ec__DisplayClass9_0_1_tB900CC9E31E22EF06649B9D677B1F5134A16356F_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass9_0_1_U3CTestSpeedU3Eb__0_mD53868A5C3F3CA1C0007300DA57A43D931B9FB0B,
	U3CU3CTestSpeedU3Eb__0U3Ed_t6E992DBE722759CCEA52EFC4D5A3FD4B519C2C47_CustomAttributesCacheGenerator_U3CU3CTestSpeedU3Eb__0U3Ed_SetStateMachine_m953315F2CEEE0D9D63A821CB0421DF030575746E,
	U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_CustomAttributesCacheGenerator_U3CGenerateDownloadUrlsU3Ed__12__ctor_m49AA3916B0CC412A999C63471289DE67A8238189,
	U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_CustomAttributesCacheGenerator_U3CGenerateDownloadUrlsU3Ed__12_System_IDisposable_Dispose_mA02129B3149C89C32BC0A39E1E2D0E1027817D3C,
	U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_CustomAttributesCacheGenerator_U3CGenerateDownloadUrlsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m164D472018312DAB261D382BD124DEA9619B8A21,
	U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_CustomAttributesCacheGenerator_U3CGenerateDownloadUrlsU3Ed__12_System_Collections_IEnumerator_Reset_m339DE9C5E3ABD68C27ED2A355B846F7CF0809439,
	U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_CustomAttributesCacheGenerator_U3CGenerateDownloadUrlsU3Ed__12_System_Collections_IEnumerator_get_Current_m7BD616CEEFFE24B1D93B2873127B3F575201EC5F,
	U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_CustomAttributesCacheGenerator_U3CGenerateDownloadUrlsU3Ed__12_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m5945E3B01D1CAB268C7D1C10D4C980CDC2D9ACC2,
	U3CGenerateDownloadUrlsU3Ed__12_t58FB98CEB044E35CE680C79037214E1169C619A4_CustomAttributesCacheGenerator_U3CGenerateDownloadUrlsU3Ed__12_System_Collections_IEnumerable_GetEnumerator_mE0880AC1B187148E1F1EF59C10DC320FBA22E2A1,
	SpeedTestHttpClient_t0B699E8C4DE06A86A6C1F04FE598DB3A0CAB8A3E_CustomAttributesCacheGenerator_SpeedTestHttpClient_GetConfig_m06815E1E1D6C99B4A9C8115CBF721BE434945A9B,
	U3CGetConfigU3Ed__1_1_t27C2E6BD21131A846C1ED0873201EA1DF5599309_CustomAttributesCacheGenerator_U3CGetConfigU3Ed__1_1_SetStateMachine_m7F1CD5751A65C6291F5C6802576469EF11BF8749,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_add_OnTestCompleted_m8FDFDE70C23620C1E96C5E02391957C2A6D15E94,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_remove_OnTestCompleted_m06533154A729B79608F982FE1EA7496E26EB2D75,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_get_LastServer_m4160DAEB0C71670367B0A377D484269F6FED2D9D,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_set_LastServer_mCBE6FCFCD3555A245B7B2B15525BA94C408DAA4E,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_get_LastDuration_m66A52EA200CD945E4144147B034FFCDAF8D5209C,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_set_LastDuration_m1FE25D3FE39015C277211676D4EE0D9219699267,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_get_LastDownloadSpeed_m9B8E22094FAD2A38D52AA9474E4427F9444D7FC3,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_set_LastDownloadSpeed_m2DAA6161A0EA820A78C3F3412D03B627BCDF5307,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_get_LastUploadSpeed_m908040E942913DEE63BD79667029A41F142979A7,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_set_LastUploadSpeed_m113E57EAC1E26360920F3C43D54153B7F3861C40,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_get_isBusy_m9B3DFCD781D218502F70A677C4DCED47045B6B6C,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_set_isBusy_mF0A5FAA978A26916776D32EE3335A56B2F615A2E,
	SpeedTestNET_t5E17C10FE2DEBC927ED9BBBB9F6CFF2DCE89103F_CustomAttributesCacheGenerator_SpeedTestNET_test_m42A569CAF7BC7316E7E71F88EDFB21976E4E837C,
	U3CtestU3Ed__49_tDD0B93F803E5724477C153898A710AFD7F7DC0A3_CustomAttributesCacheGenerator_U3CtestU3Ed__49__ctor_m84BDA458D061319259587AAFA767FDA0512FEFEC,
	U3CtestU3Ed__49_tDD0B93F803E5724477C153898A710AFD7F7DC0A3_CustomAttributesCacheGenerator_U3CtestU3Ed__49_System_IDisposable_Dispose_m01FFF33E02E0F10C094A89BEE1BFF4462E4894DE,
	U3CtestU3Ed__49_tDD0B93F803E5724477C153898A710AFD7F7DC0A3_CustomAttributesCacheGenerator_U3CtestU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5169CE1D6561EDC30A2EEBB1E1B9C3C0FBA4AF3A,
	U3CtestU3Ed__49_tDD0B93F803E5724477C153898A710AFD7F7DC0A3_CustomAttributesCacheGenerator_U3CtestU3Ed__49_System_Collections_IEnumerator_Reset_m379FD9E7D91CF45ED29222C3EE208D03E4A50604,
	U3CtestU3Ed__49_tDD0B93F803E5724477C153898A710AFD7F7DC0A3_CustomAttributesCacheGenerator_U3CtestU3Ed__49_System_Collections_IEnumerator_get_Current_mDE60D7E8FFDAA8163CF42DB919A8DF30A5B5669B,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_get_Ip_m0B9445853B5D4199CE94F7F5BD0241C1251B77B2,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_set_Ip_mF873302D8A710CBDB05FC4388D3AD3D9A09D0A76,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_get_Latitude_mADF069565C2C60552BD35CE0AC10019B6BD1AF2D,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_set_Latitude_mD8FAF274E3638BCFECAD9340117F4E240EBCCCBF,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_get_Longitude_mE4C0B3E531C0B17AFEF3C737E378852FE1AA03D2,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_set_Longitude_m190D851D72EF6D9470D1EFCF6242758CFB074F4E,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_get_Isp_mA92D7910AA592D926174B851DA714BC44B60631D,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_set_Isp_m649133295A21EC49717640BC84170602CDB3C69A,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_get_IspRating_m0188AD85C4C73A2719515B1603FF3DBDACF10B09,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_set_IspRating_m2B96661EB68F8B4049926304C28B07A00F561BE4,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_get_Rating_m88C44AF1AEAD628F6D49D59DD02141E944BD630F,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_set_Rating_m49097F8083162E99A5B396F1441C392F54911DA3,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_get_IspAvarageDownloadSpeed_m32ED04BF66402C1B0BDA39775E56BEAFA4194E8B,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_set_IspAvarageDownloadSpeed_m5AD00BF1E8F2691D70DF57173230F442794AD4E8,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_get_IspAvarageUploadSpeed_mC605999904C63FC1EDFC297DAF55E9E53B0E5236,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_set_IspAvarageUploadSpeed_m109F22774BA6E37C4847E51DDCC5774B1FC38C5D,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_U3C_ctorU3Eb__35_0_m5116CF0671C518DC2DAABABAEBD16AB801DF3B87,
	Coordinate_tF261EB8C468E414B19D067355CB7B0041751AEAF_CustomAttributesCacheGenerator_Coordinate_get_Latitude_m8D07D3A4B6CDE856EEE577B3E8F1C7A3B169CD65,
	Coordinate_tF261EB8C468E414B19D067355CB7B0041751AEAF_CustomAttributesCacheGenerator_Coordinate_set_Latitude_m1ACA8FA6F57F247E89D8F7CEF7EE20F2370C1705,
	Coordinate_tF261EB8C468E414B19D067355CB7B0041751AEAF_CustomAttributesCacheGenerator_Coordinate_get_Longitude_mAB87DACDEAFB05CE4CE6CAD0C447F8D56C29E130,
	Coordinate_tF261EB8C468E414B19D067355CB7B0041751AEAF_CustomAttributesCacheGenerator_Coordinate_set_Longitude_m16230727D76BB3EAF9332F7F26D202AC2E9E5CF0,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_get_TestLength_m289664666D0F5DECA0D41435591FE75CD7295E09,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_set_TestLength_mFD53362225A0BC710E1CC989047F7FF027214463,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_get_InitialTest_m808C4F980B4198E5882F23BBD6DDB59C040FEE35,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_set_InitialTest_m08BA6DA96AA31BA256EEF1530ED93E250A397E6D,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_get_MinTestSize_m3DF159A73D260590CA6906F5E3A4D7BB90B819C4,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_set_MinTestSize_m641B5621AC012890DA21BC159DDDBFEDFC2A449F,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_get_ThreadsPerUrl_m4D34349486D1BDFB271D6BB1F9297F3EC6348A1E,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_set_ThreadsPerUrl_m709C09E224ADF9252B9B55760E627C27A3F14941,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Id_mA2999C7EC3409CEDE72BB7C8B5364C4EC352386E,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Id_m019851DDEF130546B9918509A8A181ECD9FEA4B4,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Name_mD18CBC3EE46C7D0AD38FC614D8A46D2B9ABAA1EA,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Name_m17AAFBAC7179265AFC6E0880EB6EDE0B05D41F99,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Country_mCC14ED05AC9FBAE8811B173060C20C36642DBF26,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Country_m59CF2DCCBA4737CA0DC26F43B1D2B2900ECD5A18,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Sponsor_mCD97CBB658EC7C8B376D879B80898E08316AB7A0,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Sponsor_mA8CEE4B9339434697F9A1366F44A0DBEB68BA970,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Host_m71A2D38CB45DF2946191BED1962F14DBD0955E22,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Host_m7303208597252E6E186421C523F744CBB3A7DEB9,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Url_m660B03D65EC132A3EB811035498667CD9A4CABA8,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Url_mD827DA9B0FE13649BC4EFFA65B8FDEF7ABA20F59,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Latitude_m8B22A17C87780AA0F9744C2728D9622BE2887A46,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Latitude_m77AE62B1568B46095196B41F4B94B2EB588B052E,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Longitude_m097F70F6D4388E7715674D4152CAEEBD74777701,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Longitude_m22F4D3DE5E0D31B562D4DDE6BEA2302C97CAE77D,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Distance_m086243B6C27057388CDBF0A5C2855FFDF0601908,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Distance_m2994F70C5A3B5667D63E0E26514F9B0D3CB0D6E5,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_get_Latency_mA4CAB54DA41B8EBA4584559614C72BC412A7AD6C,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_set_Latency_mE1C4F2D76A7A9D7DEE6907001C029275959D1D37,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_U3C_ctorU3Eb__43_0_m11A8527A9A2A89C54099A241207BAF066ACEB075,
	ServerConfig_t5792ABA39BA8FC204D1B43981B00549F15225BEB_CustomAttributesCacheGenerator_ServerConfig_get_IgnoreIds_m8AD90272174C1B00F09FAB914D423BAC48B03B70,
	ServerConfig_t5792ABA39BA8FC204D1B43981B00549F15225BEB_CustomAttributesCacheGenerator_ServerConfig_set_IgnoreIds_m74CA68265E0D0F82345713DA8A75005ABFE6698D,
	ServersList_t890F3EAA1E1DC4517612ED4E1159BC59F7480C6A_CustomAttributesCacheGenerator_ServersList_get_Servers_mA728E4CD054E33150481599D265769246D2BE398,
	ServersList_t890F3EAA1E1DC4517612ED4E1159BC59F7480C6A_CustomAttributesCacheGenerator_ServersList_set_Servers_m90E26C0707F47E96012430ADC9A5940AFE0E101C,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_get_Client_mA491884009E1D77B12B2BB69EF9403E85BC5075A,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_set_Client_mF3BFDC0946EB9C6C7C7718C07AFD36A364604A05,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_get_Times_m8C7E3E2A7EC879BFFF31552B55F9365771DFDB53,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_set_Times_m76A004A32676C30B0A6FE86686CEE58D8444504A,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_get_Download_m946D691794AB8B057BC1A7BA72CD48421DB76CF5,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_set_Download_mC4B696297672244EC829C93A761FA1466DA34A51,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_get_Upload_mF3ADADA4BD5F74A9FF1E4B9C76FF6084DCE9FBFD,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_set_Upload_mA7FEFA826C900B4131D2E90C4DB3CF79C7907670,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_get_ServerConfig_mCC41E378E34CDF96D220C26580252358502250C5,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_set_ServerConfig_mD35731AAA2D23F0164EEFB2CF28B21E633C20871,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_get_Servers_m32663154FB14571FAA25BE902138A04DEDE73225,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_set_Servers_m409CAB6BF32ACF58F53120805684366D042A75C7,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_get_Download1_mBD1281097BE6FBA5F56F81A9FDC0890CFF072A83,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_set_Download1_m9FB2ACE46B9DB28FCBDE6943A0356B5F4DF60320,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_get_Download2_mC71E858A945784AC0B3AC13C3D626783AD9F75C8,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_set_Download2_m2E8D0CE21DF400DDFCF33298CB408CF786A988AA,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_get_Download3_mE0F35479045CA5961E39FD77D5BC17BB4E399863,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_set_Download3_m43AEC1ED6C046DE2FDC8D266D5AAF908916EEE23,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_get_Upload1_m23B4FD1C9029FFA70E84306928F68A33759A9ED3,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_set_Upload1_mADE25B3FCC8FEF183C95BBC0C956BE32EB2ACA9C,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_get_Upload2_mCEA15219828984DDE6749ACC494CCCA07DC6EFB3,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_set_Upload2_mDF56B9576186F2BF6893072BC281F25788E409ED,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_get_Upload3_m33891A4C1DB3C988A09EFB43EABFC38DE4390924,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_set_Upload3_m57A585D4E9D371109D318361B5949E82E1B9121C,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_get_TestLength_m01A66839C2D9D5C7DD1FDA014DCBFB80DBB23731,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_set_TestLength_m0E77363058F1C6C9DD543BBE2820DA78013B0080,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_get_Ratio_m37A83FA6FE6D9A815FAAF8E08840C53EAE29DF44,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_set_Ratio_m91BBBD058C120E6E919AB9843DCDD74087C3B20D,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_get_InitialTest_m5A44B984EB443D040BB5067823424FF774E1AE68,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_set_InitialTest_m6461F782AA38863779BD2ED35B0A01A4B38986F6,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_get_MinTestSize_m50788DA9D8ECC84CC142274094C16D40E2C2C8CA,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_set_MinTestSize_m611A46C9FF55E0E2C7326DB31F57731B7A03A8EC,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_get_Threads_m9F2C1842B9753DDCC1BC351475292DACB8264696,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_set_Threads_m80F21E7FED558565879437B55F70DF334C3A7F11,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_get_MaxChunkSize_m19E1298ED0FD82CBFF4CE9EFC434F20AE6E2C8C6,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_set_MaxChunkSize_m47EDCE56D373ADCA4FCDB9B59DAD53EA9AFF0705,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_get_MaxChunkCount_m4C6E81A8FA8812934006CFDF4A623FA3669FB11E,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_set_MaxChunkCount_m275B9BF318D72FE0202F869C819020B7117D8A69,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_get_ThreadsPerUrl_m66AA71DF8D6469F1BE6FCE1D1976ED0C1894CC71,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_set_ThreadsPerUrl_m775BDBCDA5697DC1D6E7A2C1282424FCEF597507,
	GUIMain_tAAD610B8E1060640C8477DD60A1BAA342A70B80E_CustomAttributesCacheGenerator_GUIMain_U3CStartU3Eb__21_0_m7297BA87941213DE42946C1BCB2FB56B5A8EDB2D,
	GUIMain_tAAD610B8E1060640C8477DD60A1BAA342A70B80E_CustomAttributesCacheGenerator_GUIMain_U3CStartU3Eb__21_1_mF4C6CF3E35C104AF588FF18D86FFC124FF4425B1,
	UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_UIHint_lerpAlphaDown_m6FEC2A6BF3503E0B3046CEB9F4AE8CB7FB05C46C,
	UIHint_t5B5786B738951189B871A6FF5D23A473222E03C7_CustomAttributesCacheGenerator_UIHint_lerpAlphaUp_mD0790C5B2C87FFFB07F78F0E098C08FFBFB70246,
	U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8__ctor_m137125C8737CD01C1A17E318878AFB2B0D0CDB14,
	U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8_System_IDisposable_Dispose_m82EA5F02C54838CDFAD7E3C549962D0DB1151FE1,
	U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB42DD7F0CDC10AADCD99116A14BF77D16197AE49,
	U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8_System_Collections_IEnumerator_Reset_mB4243BFE9BA869109D22485266636F346DE3495F,
	U3ClerpAlphaDownU3Ed__8_tFA588D110070DFAE10E2AAC390DE45F2C6DCE063_CustomAttributesCacheGenerator_U3ClerpAlphaDownU3Ed__8_System_Collections_IEnumerator_get_Current_m641305F2EB99B80E211B4E6E99E859C2B7AAE63B,
	U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9__ctor_m41FA43BCB2D14D39F0DC1E1AFFCECB0D18E02419,
	U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9_System_IDisposable_Dispose_mD5454F31F93F6B5DCCB420AB1241DD2E2DEE331B,
	U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07EF2B75C53B187A10E0ACC00AB20DF4CAB27CD7,
	U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9_System_Collections_IEnumerator_Reset_m1C29B48A4BFBB4956CB59DAF52A587455998D965,
	U3ClerpAlphaUpU3Ed__9_tA4B9A2133A395822BDCF87133798169872E8B6B1_CustomAttributesCacheGenerator_U3ClerpAlphaUpU3Ed__9_System_Collections_IEnumerator_get_Current_mA92CCF9323E6BC3E4B224B1CBF4098D02978B980,
	BaseHelper_t06A43A26211DD50047AB61E90EC3DCF06758E347_CustomAttributesCacheGenerator_BaseHelper_initialize_mB4C285BF69FBAD4B277ED8AD10DDF08DA38F69C5,
	BaseHelper_t06A43A26211DD50047AB61E90EC3DCF06758E347_CustomAttributesCacheGenerator_BaseHelper_hasActiveClip_m4C947A64FD354BCBAEA2078E0E1A919578D9CCBA,
	BaseHelper_t06A43A26211DD50047AB61E90EC3DCF06758E347_CustomAttributesCacheGenerator_BaseHelper_ClearTags_mD93B30D4885D2E5DCCA1672188CD20EB76B01C4F,
	BaseHelper_t06A43A26211DD50047AB61E90EC3DCF06758E347_CustomAttributesCacheGenerator_BaseHelper_ClearSpaces_m235DA279F449F5AD4E61A2F82D5C414DB86322CD,
	BaseHelper_t06A43A26211DD50047AB61E90EC3DCF06758E347_CustomAttributesCacheGenerator_BaseHelper_ClearLineEndings_m8A5A3B1AFCE29A057A7D0EF2DE7576734251456D,
	CTHelper_t501C6E12D53FDF522624C5D95FDDA59A2ABCE227_CustomAttributesCacheGenerator_CTHelper_get_Instance_m9419E0AE2630EC0841937F6CDE83B330BA39D6C9,
	CTHelper_t501C6E12D53FDF522624C5D95FDDA59A2ABCE227_CustomAttributesCacheGenerator_CTHelper_set_Instance_m7D72BBE9FA37F24A47B37E2068BA10A6FD30ED40,
	CTHelper_t501C6E12D53FDF522624C5D95FDDA59A2ABCE227_CustomAttributesCacheGenerator_CTHelper_initialize_m4E427FE7AE92A937936DE03A4AE592CA7B1E3BB5,
	CTHelper_t501C6E12D53FDF522624C5D95FDDA59A2ABCE227_CustomAttributesCacheGenerator_CTHelper_create_m8BCAD76807872BA63E2E48039A13923C9AD30FDD,
	CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_CTWebClient_get_Timeout_m05E7AC7AD82CFD0A255A1D753B3AB413E0C15C9E,
	CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_CTWebClient_set_Timeout_mC93FB85FA7A3F30D35CE10A64A25ABD18D206079,
	CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_CTWebClient_get_ConnectionLimit_m74A0F4DC526B36E55EA0857E3F953453622189CE,
	CTWebClient_t32C9C802CE03700DC8C37D1952056B588BE7D709_CustomAttributesCacheGenerator_CTWebClient_set_ConnectionLimit_mFBDBF8F1C18BC022FC5B773E3874E8B9569F9068,
	FileHelper_t3637C5C131C495F206247B3FE3CF746E3E02E1E5_CustomAttributesCacheGenerator_FileHelper_initialize_m6603998B02EF452F39EA024FE1770F766C11EBC6,
	SingletonHelper_tB766F047E35A58385809F9EB65029AF836DC54FB_CustomAttributesCacheGenerator_SingletonHelper_get_isQuitting_mA9E2C23D74C48DCBB8FF9D9380A6C2F279B840C0,
	SingletonHelper_tB766F047E35A58385809F9EB65029AF836DC54FB_CustomAttributesCacheGenerator_SingletonHelper_set_isQuitting_m53D01E30152CDE243B85A357E5B55BAF6F346169,
	SingletonHelper_tB766F047E35A58385809F9EB65029AF836DC54FB_CustomAttributesCacheGenerator_SingletonHelper_initialize_mC53AE8824DF8FAAD29911D0F684E9174D96551DB,
	BaseHelper_t06A43A26211DD50047AB61E90EC3DCF06758E347_CustomAttributesCacheGenerator_BaseHelper_InvokeMethod_m16B910E20A79713DF61CAF991EEC69FD5832FD5D____parameters2,
	FileHelper_t3637C5C131C495F206247B3FE3CF746E3E02E1E5_CustomAttributesCacheGenerator_FileHelper_GetFiles_m599BE482715EA35B5198B77E79713FC266FFACB9____extensions2,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA____Ip_PropertyInfo,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA____Latitude_PropertyInfo,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA____Longitude_PropertyInfo,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA____Isp_PropertyInfo,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA____IspRating_PropertyInfo,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA____Rating_PropertyInfo,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA____IspAvarageDownloadSpeed_PropertyInfo,
	Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA_CustomAttributesCacheGenerator_Client_t513253F260C71AF7D7FC2667B4DDE30A0CD419BA____IspAvarageUploadSpeed_PropertyInfo,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B____TestLength_PropertyInfo,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B____InitialTest_PropertyInfo,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B____MinTestSize_PropertyInfo,
	Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B_CustomAttributesCacheGenerator_Download_t1FBFC55422F2C05A8D4D159FA87316B5AFF3C12B____ThreadsPerUrl_PropertyInfo,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_tED9643BADC961426E50AA71234EEAD16C451411C____Id_PropertyInfo,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_tED9643BADC961426E50AA71234EEAD16C451411C____Name_PropertyInfo,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_tED9643BADC961426E50AA71234EEAD16C451411C____Country_PropertyInfo,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_tED9643BADC961426E50AA71234EEAD16C451411C____Sponsor_PropertyInfo,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_tED9643BADC961426E50AA71234EEAD16C451411C____Host_PropertyInfo,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_tED9643BADC961426E50AA71234EEAD16C451411C____Url_PropertyInfo,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_tED9643BADC961426E50AA71234EEAD16C451411C____Latitude_PropertyInfo,
	Server_tED9643BADC961426E50AA71234EEAD16C451411C_CustomAttributesCacheGenerator_Server_tED9643BADC961426E50AA71234EEAD16C451411C____Longitude_PropertyInfo,
	ServerConfig_t5792ABA39BA8FC204D1B43981B00549F15225BEB_CustomAttributesCacheGenerator_ServerConfig_t5792ABA39BA8FC204D1B43981B00549F15225BEB____IgnoreIds_PropertyInfo,
	ServersList_t890F3EAA1E1DC4517612ED4E1159BC59F7480C6A_CustomAttributesCacheGenerator_ServersList_t890F3EAA1E1DC4517612ED4E1159BC59F7480C6A____Servers_PropertyInfo,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE____Client_PropertyInfo,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE____Times_PropertyInfo,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE____Download_PropertyInfo,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE____Upload_PropertyInfo,
	Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE_CustomAttributesCacheGenerator_Settings_t65AF37E51A4228FDDCC39E12D43F3E6C6A462BEE____ServerConfig_PropertyInfo,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274____Download1_PropertyInfo,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274____Download2_PropertyInfo,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274____Download3_PropertyInfo,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274____Upload1_PropertyInfo,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274____Upload2_PropertyInfo,
	Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274_CustomAttributesCacheGenerator_Times_t1C971D6DB6305CE67A7BF6958664D82621FC4274____Upload3_PropertyInfo,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1____TestLength_PropertyInfo,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1____Ratio_PropertyInfo,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1____InitialTest_PropertyInfo,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1____MinTestSize_PropertyInfo,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1____Threads_PropertyInfo,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1____MaxChunkSize_PropertyInfo,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1____MaxChunkCount_PropertyInfo,
	Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1_CustomAttributesCacheGenerator_Upload_t067C30ECECE00C4F5791A88DF1002791715DB9A1____ThreadsPerUrl_PropertyInfo,
	AssemblyU2DCSharpU2Dfirstpass_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CorderU3Ek__BackingField_2(L_0);
		return;
	}
}
