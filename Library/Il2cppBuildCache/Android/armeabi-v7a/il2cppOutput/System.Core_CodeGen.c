﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_mD7FC73A3473F4F165E55F8B4A7088F2E9F9CC412 (void);
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_m79644F6BCD0E8C2D8BAF1B1E22E90D3C364F5C57 (void);
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mCFE4C56DFF81F5E616CE535AB7D9E37DC1B7A937 (void);
// 0x00000004 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mB1D7896A5F5E71B8B7938A5DF3A743FC2E444018 (void);
// 0x00000005 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m1DBDC4FDAE66A5F2FA99AA4A4E76769BB8897D1E (void);
// 0x00000006 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_m4CC3B2D28A918B935AD42F3F8D54E93A6CB2FA31 (void);
// 0x00000007 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m35D61E5FD8942054840B1F24E685E91E3E6CA6E1 (void);
// 0x00000008 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_mBE6EA533BD5978099974A74FF3DE3ECB8B173CD6 (void);
// 0x00000009 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m2003A2B9200003C23B544F56E949A0630AA87F93 (void);
// 0x0000000A System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_mF9D7222B2AB685AC46F4564B6F2247114244AEF6 (void);
// 0x0000000B System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mA5CF4C1F3B41503C6E09373ADB0B8983A6F61460 (void);
// 0x0000000C System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mD81B3F96D3421F6CD2189A01D65736A9098ACD45 (void);
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m6B07EC4A0F1F451417DC0AC64E9D637D7916866B (void);
// 0x0000000E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m41AE4428FE60C9FD485640F3A09F1BF345452A3C (void);
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_m7240F8C38B99CE73159DE7455046E951C4900268 (void);
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_mB2BBCAB8753A59FFB572091D2EF80F287CD951BF (void);
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_m1E4EB80DE75FCF9E940228E1D7664C0EA1378153 (void);
// 0x00000012 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_mB0D969841D51825F37095A93E73A50C15C1A1477 (void);
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_mBB19651CC37782273A882055D4E63370268F2D91 (void);
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_mF6673B955AE82377595277C6B78C7DA8A16F480E (void);
// 0x00000015 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_mA9857852BC34D8AB0F463C1AF1837CBBD9102265 (void);
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_m18539D5136BA9A2FC71F439150D16E35AD3BF5C4 (void);
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_m574F877FD23D1F07033FC035E89BE232303F3502 (void);
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_mAB5FB857F549A86D986461C8665BE6B2393305D1 (void);
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_m6BF20D5D8424DB627CD3010D9E4C8555C6BD0465 (void);
// 0x0000001A System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m6A46F1C255ABE41F98BEE8C0C37D6AFBB9F29D34 (void);
// 0x0000001B System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_mCB88C0F651B17F3EC7575F16E14C9E3BD2DB24DB (void);
// 0x0000001C System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_mAC979BC922E8F1F15B36220E77972AC9CE5D5252 (void);
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m65785032C270005BC120157A0C9D019F6F6BC96F (void);
// 0x0000001E System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m3081171DF6C11CA55ECEBA29B9559D18E78D8058 (void);
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA994D2D3098216C0B8C4F02C0F0A0F63D4256218 (void);
// 0x00000020 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_m9DC2E1C3E84CC674ADB2D7E6B06066F333BEC89D (void);
// 0x00000021 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m3E1CBFD4D7CE748F3AB615EB88DE1A5D7238285D (void);
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mFE7044929761BABE312D1146B0ED51B331E35D63 (void);
// 0x00000023 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_m89D49B05949BA2C6C557EFA5211B4934D279C7AD (void);
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_mD3353CD8F4B931AA00203000140520775643F96E (void);
// 0x00000025 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mB1F90A7339DA65542795E17DF9C37810BD088DDF (void);
// 0x00000026 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m9555DFFCA344DF06C8B88DDE2EB987B3958EC6BB (void);
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_m7123198904819E2BF2B1398E20047B316C3D7D1E (void);
// 0x00000028 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m3903A599E8B2C3F7AB3B70E1258980151D639598 (void);
// 0x00000029 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_m2E2F4E2B307B0D34FEADF38684007E622FCEDFD1 (void);
// 0x0000002A System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_m2D77D545ABD3D84C04741B80ABB74BEFE8C55679 (void);
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m57DA74A7E05818DFD92F2614F8F65B0D1E696129 (void);
// 0x0000002C System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m075F7BA40A4CFECA6F6A379065B731586EDDB23A (void);
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mAC6D46ED54345C2D23DFCA026C69029757222CFD (void);
// 0x0000002E System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E (void);
// 0x0000002F System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2 (void);
// 0x00000030 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8 (void);
// 0x00000031 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_mB89E91246572F009281D79730950808F17C3F353 (void);
// 0x00000032 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000033 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000034 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000035 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000036 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000037 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000038 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000039 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003A System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003B System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003C System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003D System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000003E TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003F System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000041 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000042 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000043 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000044 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000045 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000046 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000047 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000048 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000049 System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004A System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004B System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000004C System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000004D System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Sum_m6CFC8CEAC70AE3C469A5D1993FAF8EEEC6A06FB5 (void);
// 0x0000004E System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Int32>)
// 0x0000004F System.Void System.Linq.Enumerable/Iterator`1::.ctor()
// 0x00000050 TSource System.Linq.Enumerable/Iterator`1::get_Current()
// 0x00000051 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1::Clone()
// 0x00000052 System.Void System.Linq.Enumerable/Iterator`1::Dispose()
// 0x00000053 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1::GetEnumerator()
// 0x00000054 System.Boolean System.Linq.Enumerable/Iterator`1::MoveNext()
// 0x00000055 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000056 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000057 System.Object System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000058 System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000059 System.Void System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000005A System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000005B System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Clone()
// 0x0000005C System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::Dispose()
// 0x0000005D System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1::MoveNext()
// 0x0000005E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000005F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000060 System.Void System.Linq.Enumerable/WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000061 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Clone()
// 0x00000062 System.Boolean System.Linq.Enumerable/WhereArrayIterator`1::MoveNext()
// 0x00000063 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000064 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000065 System.Void System.Linq.Enumerable/WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000066 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Clone()
// 0x00000067 System.Boolean System.Linq.Enumerable/WhereListIterator`1::MoveNext()
// 0x00000068 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000069 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000006A System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000006B System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Clone()
// 0x0000006C System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Dispose()
// 0x0000006D System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000006E System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000006F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000070 System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000071 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Clone()
// 0x00000072 System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2::MoveNext()
// 0x00000073 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000074 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000075 System.Void System.Linq.Enumerable/WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000076 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Clone()
// 0x00000077 System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2::MoveNext()
// 0x00000078 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000079 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000007A System.Void System.Linq.Enumerable/<>c__DisplayClass6_0`1::.ctor()
// 0x0000007B System.Boolean System.Linq.Enumerable/<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000007C System.Void System.Linq.Enumerable/<>c__DisplayClass7_0`3::.ctor()
// 0x0000007D TResult System.Linq.Enumerable/<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000007E System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x0000007F System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x00000080 System.Boolean System.Linq.Enumerable/<SelectManyIterator>d__17`2::MoveNext()
// 0x00000081 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x00000082 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x00000083 TResult System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000084 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x00000085 System.Object System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x00000086 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000087 System.Collections.IEnumerator System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000088 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x00000089 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x0000008A System.Boolean System.Linq.Enumerable/<TakeIterator>d__25`1::MoveNext()
// 0x0000008B System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::<>m__Finally1()
// 0x0000008C TSource System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000008D System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x0000008E System.Object System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x0000008F System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000090 System.Collections.IEnumerator System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000091 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::.ctor(System.Int32)
// 0x00000092 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x00000093 System.Boolean System.Linq.Enumerable/<CastIterator>d__99`1::MoveNext()
// 0x00000094 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::<>m__Finally1()
// 0x00000095 TResult System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000096 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x00000097 System.Object System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x00000098 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000099 System.Collections.IEnumerator System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009A System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000009B System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x0000009C System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000009D System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009E System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000009F System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000A0 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000A1 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000A2 System.Boolean System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::MoveNext()
// 0x000000A3 TElement System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000A4 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000A5 System.Object System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000A6 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000A7 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000A8 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000A9 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000AA System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000AB System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000AC System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000AD System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000AE System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000AF System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000B0 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000B1 TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000B2 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x000000B3 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000B4 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000B5 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000B6 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000B7 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000B8 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x000000B9 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x000000BA System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x000000BB System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x000000BC System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x000000BD System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000BE System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000BF System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000C0 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000C1 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000C2 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000C3 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000C4 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000C5 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000C6 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000C7 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000C8 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000C9 System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000CA System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
// 0x000000CB System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
// 0x000000CC T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
// 0x000000CD System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000CE System.Void System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[206] = 
{
	SR_GetString_mD7FC73A3473F4F165E55F8B4A7088F2E9F9CC412,
	AesManaged__ctor_m79644F6BCD0E8C2D8BAF1B1E22E90D3C364F5C57,
	AesManaged_get_FeedbackSize_mCFE4C56DFF81F5E616CE535AB7D9E37DC1B7A937,
	AesManaged_get_IV_mB1D7896A5F5E71B8B7938A5DF3A743FC2E444018,
	AesManaged_set_IV_m1DBDC4FDAE66A5F2FA99AA4A4E76769BB8897D1E,
	AesManaged_get_Key_m4CC3B2D28A918B935AD42F3F8D54E93A6CB2FA31,
	AesManaged_set_Key_m35D61E5FD8942054840B1F24E685E91E3E6CA6E1,
	AesManaged_get_KeySize_mBE6EA533BD5978099974A74FF3DE3ECB8B173CD6,
	AesManaged_set_KeySize_m2003A2B9200003C23B544F56E949A0630AA87F93,
	AesManaged_get_Mode_mF9D7222B2AB685AC46F4564B6F2247114244AEF6,
	AesManaged_set_Mode_mA5CF4C1F3B41503C6E09373ADB0B8983A6F61460,
	AesManaged_get_Padding_mD81B3F96D3421F6CD2189A01D65736A9098ACD45,
	AesManaged_set_Padding_m6B07EC4A0F1F451417DC0AC64E9D637D7916866B,
	AesManaged_CreateDecryptor_m41AE4428FE60C9FD485640F3A09F1BF345452A3C,
	AesManaged_CreateDecryptor_m7240F8C38B99CE73159DE7455046E951C4900268,
	AesManaged_CreateEncryptor_mB2BBCAB8753A59FFB572091D2EF80F287CD951BF,
	AesManaged_CreateEncryptor_m1E4EB80DE75FCF9E940228E1D7664C0EA1378153,
	AesManaged_Dispose_mB0D969841D51825F37095A93E73A50C15C1A1477,
	AesManaged_GenerateIV_mBB19651CC37782273A882055D4E63370268F2D91,
	AesManaged_GenerateKey_mF6673B955AE82377595277C6B78C7DA8A16F480E,
	AesCryptoServiceProvider__ctor_mA9857852BC34D8AB0F463C1AF1837CBBD9102265,
	AesCryptoServiceProvider_GenerateIV_m18539D5136BA9A2FC71F439150D16E35AD3BF5C4,
	AesCryptoServiceProvider_GenerateKey_m574F877FD23D1F07033FC035E89BE232303F3502,
	AesCryptoServiceProvider_CreateDecryptor_mAB5FB857F549A86D986461C8665BE6B2393305D1,
	AesCryptoServiceProvider_CreateEncryptor_m6BF20D5D8424DB627CD3010D9E4C8555C6BD0465,
	AesCryptoServiceProvider_get_IV_m6A46F1C255ABE41F98BEE8C0C37D6AFBB9F29D34,
	AesCryptoServiceProvider_set_IV_mCB88C0F651B17F3EC7575F16E14C9E3BD2DB24DB,
	AesCryptoServiceProvider_get_Key_mAC979BC922E8F1F15B36220E77972AC9CE5D5252,
	AesCryptoServiceProvider_set_Key_m65785032C270005BC120157A0C9D019F6F6BC96F,
	AesCryptoServiceProvider_get_KeySize_m3081171DF6C11CA55ECEBA29B9559D18E78D8058,
	AesCryptoServiceProvider_set_KeySize_mA994D2D3098216C0B8C4F02C0F0A0F63D4256218,
	AesCryptoServiceProvider_get_FeedbackSize_m9DC2E1C3E84CC674ADB2D7E6B06066F333BEC89D,
	AesCryptoServiceProvider_get_Mode_m3E1CBFD4D7CE748F3AB615EB88DE1A5D7238285D,
	AesCryptoServiceProvider_set_Mode_mFE7044929761BABE312D1146B0ED51B331E35D63,
	AesCryptoServiceProvider_get_Padding_m89D49B05949BA2C6C557EFA5211B4934D279C7AD,
	AesCryptoServiceProvider_set_Padding_mD3353CD8F4B931AA00203000140520775643F96E,
	AesCryptoServiceProvider_CreateDecryptor_mB1F90A7339DA65542795E17DF9C37810BD088DDF,
	AesCryptoServiceProvider_CreateEncryptor_m9555DFFCA344DF06C8B88DDE2EB987B3958EC6BB,
	AesCryptoServiceProvider_Dispose_m7123198904819E2BF2B1398E20047B316C3D7D1E,
	AesTransform__ctor_m3903A599E8B2C3F7AB3B70E1258980151D639598,
	AesTransform_ECB_m2E2F4E2B307B0D34FEADF38684007E622FCEDFD1,
	AesTransform_SubByte_m2D77D545ABD3D84C04741B80ABB74BEFE8C55679,
	AesTransform_Encrypt128_m57DA74A7E05818DFD92F2614F8F65B0D1E696129,
	AesTransform_Decrypt128_m075F7BA40A4CFECA6F6A379065B731586EDDB23A,
	AesTransform__cctor_mAC6D46ED54345C2D23DFCA026C69029757222CFD,
	Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E,
	Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2,
	Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8,
	Error_NoElements_mB89E91246572F009281D79730950808F17C3F353,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Sum_m6CFC8CEAC70AE3C469A5D1993FAF8EEEC6A06FB5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[206] = 
{
	4927,
	3244,
	3179,
	3195,
	2666,
	3195,
	2666,
	3179,
	2651,
	3179,
	2651,
	3179,
	2651,
	3195,
	1146,
	3195,
	1146,
	2686,
	3244,
	3244,
	3244,
	3244,
	3244,
	1146,
	1146,
	3195,
	2666,
	3195,
	2666,
	3179,
	2651,
	3179,
	3179,
	2651,
	3179,
	2651,
	3195,
	3195,
	2686,
	636,
	1542,
	1872,
	955,
	955,
	5093,
	4927,
	4927,
	5074,
	5074,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4841,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[55] = 
{
	{ 0x02000008, { 88, 4 } },
	{ 0x02000009, { 92, 9 } },
	{ 0x0200000A, { 103, 7 } },
	{ 0x0200000B, { 112, 10 } },
	{ 0x0200000C, { 124, 11 } },
	{ 0x0200000D, { 138, 9 } },
	{ 0x0200000E, { 150, 12 } },
	{ 0x0200000F, { 165, 1 } },
	{ 0x02000010, { 166, 2 } },
	{ 0x02000011, { 168, 12 } },
	{ 0x02000012, { 180, 8 } },
	{ 0x02000013, { 188, 6 } },
	{ 0x02000015, { 194, 3 } },
	{ 0x02000016, { 199, 5 } },
	{ 0x02000017, { 204, 7 } },
	{ 0x02000018, { 211, 3 } },
	{ 0x02000019, { 214, 7 } },
	{ 0x0200001A, { 221, 4 } },
	{ 0x0200001B, { 225, 21 } },
	{ 0x0200001D, { 246, 2 } },
	{ 0x06000032, { 0, 10 } },
	{ 0x06000033, { 10, 10 } },
	{ 0x06000034, { 20, 5 } },
	{ 0x06000035, { 25, 5 } },
	{ 0x06000036, { 30, 1 } },
	{ 0x06000037, { 31, 2 } },
	{ 0x06000038, { 33, 1 } },
	{ 0x06000039, { 34, 2 } },
	{ 0x0600003A, { 36, 2 } },
	{ 0x0600003B, { 38, 1 } },
	{ 0x0600003C, { 39, 1 } },
	{ 0x0600003D, { 40, 5 } },
	{ 0x0600003E, { 45, 3 } },
	{ 0x0600003F, { 48, 2 } },
	{ 0x06000040, { 50, 2 } },
	{ 0x06000041, { 52, 2 } },
	{ 0x06000042, { 54, 4 } },
	{ 0x06000043, { 58, 3 } },
	{ 0x06000044, { 61, 4 } },
	{ 0x06000045, { 65, 3 } },
	{ 0x06000046, { 68, 3 } },
	{ 0x06000047, { 71, 1 } },
	{ 0x06000048, { 72, 3 } },
	{ 0x06000049, { 75, 3 } },
	{ 0x0600004A, { 78, 2 } },
	{ 0x0600004B, { 80, 2 } },
	{ 0x0600004C, { 82, 5 } },
	{ 0x0600004E, { 87, 1 } },
	{ 0x0600005E, { 101, 2 } },
	{ 0x06000063, { 110, 2 } },
	{ 0x06000068, { 122, 2 } },
	{ 0x0600006E, { 135, 3 } },
	{ 0x06000073, { 147, 3 } },
	{ 0x06000078, { 162, 3 } },
	{ 0x0600009E, { 197, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[248] = 
{
	{ (Il2CppRGCTXDataType)2, 2397 },
	{ (Il2CppRGCTXDataType)3, 9461 },
	{ (Il2CppRGCTXDataType)2, 4021 },
	{ (Il2CppRGCTXDataType)2, 3540 },
	{ (Il2CppRGCTXDataType)3, 17681 },
	{ (Il2CppRGCTXDataType)2, 2520 },
	{ (Il2CppRGCTXDataType)2, 3547 },
	{ (Il2CppRGCTXDataType)3, 17731 },
	{ (Il2CppRGCTXDataType)2, 3542 },
	{ (Il2CppRGCTXDataType)3, 17698 },
	{ (Il2CppRGCTXDataType)2, 2398 },
	{ (Il2CppRGCTXDataType)3, 9462 },
	{ (Il2CppRGCTXDataType)2, 4051 },
	{ (Il2CppRGCTXDataType)2, 3549 },
	{ (Il2CppRGCTXDataType)3, 17748 },
	{ (Il2CppRGCTXDataType)2, 2543 },
	{ (Il2CppRGCTXDataType)2, 3557 },
	{ (Il2CppRGCTXDataType)3, 17822 },
	{ (Il2CppRGCTXDataType)2, 3553 },
	{ (Il2CppRGCTXDataType)3, 17782 },
	{ (Il2CppRGCTXDataType)2, 790 },
	{ (Il2CppRGCTXDataType)3, 136 },
	{ (Il2CppRGCTXDataType)3, 137 },
	{ (Il2CppRGCTXDataType)2, 1539 },
	{ (Il2CppRGCTXDataType)3, 7262 },
	{ (Il2CppRGCTXDataType)2, 791 },
	{ (Il2CppRGCTXDataType)3, 148 },
	{ (Il2CppRGCTXDataType)3, 149 },
	{ (Il2CppRGCTXDataType)2, 1551 },
	{ (Il2CppRGCTXDataType)3, 7267 },
	{ (Il2CppRGCTXDataType)3, 20563 },
	{ (Il2CppRGCTXDataType)2, 805 },
	{ (Il2CppRGCTXDataType)3, 204 },
	{ (Il2CppRGCTXDataType)3, 20575 },
	{ (Il2CppRGCTXDataType)2, 809 },
	{ (Il2CppRGCTXDataType)3, 240 },
	{ (Il2CppRGCTXDataType)2, 3068 },
	{ (Il2CppRGCTXDataType)3, 14861 },
	{ (Il2CppRGCTXDataType)3, 8084 },
	{ (Il2CppRGCTXDataType)3, 20567 },
	{ (Il2CppRGCTXDataType)3, 6550 },
	{ (Il2CppRGCTXDataType)2, 1411 },
	{ (Il2CppRGCTXDataType)2, 1873 },
	{ (Il2CppRGCTXDataType)2, 1998 },
	{ (Il2CppRGCTXDataType)2, 2105 },
	{ (Il2CppRGCTXDataType)2, 1034 },
	{ (Il2CppRGCTXDataType)3, 2092 },
	{ (Il2CppRGCTXDataType)3, 2093 },
	{ (Il2CppRGCTXDataType)2, 2521 },
	{ (Il2CppRGCTXDataType)3, 10229 },
	{ (Il2CppRGCTXDataType)2, 1874 },
	{ (Il2CppRGCTXDataType)3, 20501 },
	{ (Il2CppRGCTXDataType)2, 794 },
	{ (Il2CppRGCTXDataType)3, 170 },
	{ (Il2CppRGCTXDataType)2, 2291 },
	{ (Il2CppRGCTXDataType)2, 1749 },
	{ (Il2CppRGCTXDataType)2, 1883 },
	{ (Il2CppRGCTXDataType)2, 2000 },
	{ (Il2CppRGCTXDataType)2, 1884 },
	{ (Il2CppRGCTXDataType)2, 2001 },
	{ (Il2CppRGCTXDataType)3, 7263 },
	{ (Il2CppRGCTXDataType)2, 2292 },
	{ (Il2CppRGCTXDataType)2, 1750 },
	{ (Il2CppRGCTXDataType)2, 1885 },
	{ (Il2CppRGCTXDataType)2, 2002 },
	{ (Il2CppRGCTXDataType)2, 1886 },
	{ (Il2CppRGCTXDataType)2, 2003 },
	{ (Il2CppRGCTXDataType)3, 7264 },
	{ (Il2CppRGCTXDataType)2, 2290 },
	{ (Il2CppRGCTXDataType)2, 1882 },
	{ (Il2CppRGCTXDataType)2, 1999 },
	{ (Il2CppRGCTXDataType)2, 1868 },
	{ (Il2CppRGCTXDataType)2, 1869 },
	{ (Il2CppRGCTXDataType)2, 1996 },
	{ (Il2CppRGCTXDataType)3, 7261 },
	{ (Il2CppRGCTXDataType)2, 1867 },
	{ (Il2CppRGCTXDataType)2, 1995 },
	{ (Il2CppRGCTXDataType)3, 7260 },
	{ (Il2CppRGCTXDataType)2, 1748 },
	{ (Il2CppRGCTXDataType)2, 1880 },
	{ (Il2CppRGCTXDataType)2, 1747 },
	{ (Il2CppRGCTXDataType)3, 20524 },
	{ (Il2CppRGCTXDataType)3, 6549 },
	{ (Il2CppRGCTXDataType)2, 1410 },
	{ (Il2CppRGCTXDataType)2, 1871 },
	{ (Il2CppRGCTXDataType)2, 1997 },
	{ (Il2CppRGCTXDataType)2, 2104 },
	{ (Il2CppRGCTXDataType)3, 20551 },
	{ (Il2CppRGCTXDataType)3, 9463 },
	{ (Il2CppRGCTXDataType)3, 9465 },
	{ (Il2CppRGCTXDataType)2, 511 },
	{ (Il2CppRGCTXDataType)3, 9464 },
	{ (Il2CppRGCTXDataType)3, 9473 },
	{ (Il2CppRGCTXDataType)2, 2401 },
	{ (Il2CppRGCTXDataType)2, 3543 },
	{ (Il2CppRGCTXDataType)3, 17699 },
	{ (Il2CppRGCTXDataType)3, 9474 },
	{ (Il2CppRGCTXDataType)2, 1938 },
	{ (Il2CppRGCTXDataType)2, 2036 },
	{ (Il2CppRGCTXDataType)3, 7274 },
	{ (Il2CppRGCTXDataType)3, 20504 },
	{ (Il2CppRGCTXDataType)2, 3554 },
	{ (Il2CppRGCTXDataType)3, 17783 },
	{ (Il2CppRGCTXDataType)3, 9466 },
	{ (Il2CppRGCTXDataType)2, 2400 },
	{ (Il2CppRGCTXDataType)2, 3541 },
	{ (Il2CppRGCTXDataType)3, 17682 },
	{ (Il2CppRGCTXDataType)3, 7273 },
	{ (Il2CppRGCTXDataType)3, 9467 },
	{ (Il2CppRGCTXDataType)3, 20503 },
	{ (Il2CppRGCTXDataType)2, 3550 },
	{ (Il2CppRGCTXDataType)3, 17749 },
	{ (Il2CppRGCTXDataType)3, 9480 },
	{ (Il2CppRGCTXDataType)2, 2402 },
	{ (Il2CppRGCTXDataType)2, 3548 },
	{ (Il2CppRGCTXDataType)3, 17732 },
	{ (Il2CppRGCTXDataType)3, 10294 },
	{ (Il2CppRGCTXDataType)3, 5257 },
	{ (Il2CppRGCTXDataType)3, 7275 },
	{ (Il2CppRGCTXDataType)3, 5256 },
	{ (Il2CppRGCTXDataType)3, 9481 },
	{ (Il2CppRGCTXDataType)3, 20505 },
	{ (Il2CppRGCTXDataType)2, 3558 },
	{ (Il2CppRGCTXDataType)3, 17823 },
	{ (Il2CppRGCTXDataType)3, 9494 },
	{ (Il2CppRGCTXDataType)2, 2404 },
	{ (Il2CppRGCTXDataType)2, 3556 },
	{ (Il2CppRGCTXDataType)3, 17785 },
	{ (Il2CppRGCTXDataType)3, 9495 },
	{ (Il2CppRGCTXDataType)2, 1941 },
	{ (Il2CppRGCTXDataType)2, 2039 },
	{ (Il2CppRGCTXDataType)3, 7279 },
	{ (Il2CppRGCTXDataType)3, 7278 },
	{ (Il2CppRGCTXDataType)2, 3545 },
	{ (Il2CppRGCTXDataType)3, 17701 },
	{ (Il2CppRGCTXDataType)3, 20512 },
	{ (Il2CppRGCTXDataType)2, 3555 },
	{ (Il2CppRGCTXDataType)3, 17784 },
	{ (Il2CppRGCTXDataType)3, 9487 },
	{ (Il2CppRGCTXDataType)2, 2403 },
	{ (Il2CppRGCTXDataType)2, 3552 },
	{ (Il2CppRGCTXDataType)3, 17751 },
	{ (Il2CppRGCTXDataType)3, 7277 },
	{ (Il2CppRGCTXDataType)3, 7276 },
	{ (Il2CppRGCTXDataType)3, 9488 },
	{ (Il2CppRGCTXDataType)2, 3544 },
	{ (Il2CppRGCTXDataType)3, 17700 },
	{ (Il2CppRGCTXDataType)3, 20511 },
	{ (Il2CppRGCTXDataType)2, 3551 },
	{ (Il2CppRGCTXDataType)3, 17750 },
	{ (Il2CppRGCTXDataType)3, 9501 },
	{ (Il2CppRGCTXDataType)2, 2405 },
	{ (Il2CppRGCTXDataType)2, 3560 },
	{ (Il2CppRGCTXDataType)3, 17825 },
	{ (Il2CppRGCTXDataType)3, 10295 },
	{ (Il2CppRGCTXDataType)3, 5259 },
	{ (Il2CppRGCTXDataType)3, 7281 },
	{ (Il2CppRGCTXDataType)3, 7280 },
	{ (Il2CppRGCTXDataType)3, 5258 },
	{ (Il2CppRGCTXDataType)3, 9502 },
	{ (Il2CppRGCTXDataType)2, 3546 },
	{ (Il2CppRGCTXDataType)3, 17702 },
	{ (Il2CppRGCTXDataType)3, 20513 },
	{ (Il2CppRGCTXDataType)2, 3559 },
	{ (Il2CppRGCTXDataType)3, 17824 },
	{ (Il2CppRGCTXDataType)3, 7270 },
	{ (Il2CppRGCTXDataType)3, 7271 },
	{ (Il2CppRGCTXDataType)3, 7282 },
	{ (Il2CppRGCTXDataType)3, 207 },
	{ (Il2CppRGCTXDataType)3, 206 },
	{ (Il2CppRGCTXDataType)2, 1930 },
	{ (Il2CppRGCTXDataType)2, 2030 },
	{ (Il2CppRGCTXDataType)3, 7272 },
	{ (Il2CppRGCTXDataType)2, 1947 },
	{ (Il2CppRGCTXDataType)2, 2052 },
	{ (Il2CppRGCTXDataType)3, 209 },
	{ (Il2CppRGCTXDataType)2, 688 },
	{ (Il2CppRGCTXDataType)2, 806 },
	{ (Il2CppRGCTXDataType)3, 205 },
	{ (Il2CppRGCTXDataType)3, 208 },
	{ (Il2CppRGCTXDataType)3, 242 },
	{ (Il2CppRGCTXDataType)2, 1933 },
	{ (Il2CppRGCTXDataType)2, 2032 },
	{ (Il2CppRGCTXDataType)3, 244 },
	{ (Il2CppRGCTXDataType)2, 509 },
	{ (Il2CppRGCTXDataType)2, 810 },
	{ (Il2CppRGCTXDataType)3, 241 },
	{ (Il2CppRGCTXDataType)3, 243 },
	{ (Il2CppRGCTXDataType)3, 172 },
	{ (Il2CppRGCTXDataType)2, 506 },
	{ (Il2CppRGCTXDataType)3, 174 },
	{ (Il2CppRGCTXDataType)2, 795 },
	{ (Il2CppRGCTXDataType)3, 171 },
	{ (Il2CppRGCTXDataType)3, 173 },
	{ (Il2CppRGCTXDataType)2, 799 },
	{ (Il2CppRGCTXDataType)3, 186 },
	{ (Il2CppRGCTXDataType)3, 14848 },
	{ (Il2CppRGCTXDataType)2, 3069 },
	{ (Il2CppRGCTXDataType)3, 14862 },
	{ (Il2CppRGCTXDataType)2, 1035 },
	{ (Il2CppRGCTXDataType)3, 2094 },
	{ (Il2CppRGCTXDataType)3, 14854 },
	{ (Il2CppRGCTXDataType)3, 5220 },
	{ (Il2CppRGCTXDataType)2, 541 },
	{ (Il2CppRGCTXDataType)3, 14849 },
	{ (Il2CppRGCTXDataType)2, 3065 },
	{ (Il2CppRGCTXDataType)3, 2197 },
	{ (Il2CppRGCTXDataType)2, 1064 },
	{ (Il2CppRGCTXDataType)2, 1371 },
	{ (Il2CppRGCTXDataType)3, 5226 },
	{ (Il2CppRGCTXDataType)3, 14850 },
	{ (Il2CppRGCTXDataType)3, 5215 },
	{ (Il2CppRGCTXDataType)3, 5216 },
	{ (Il2CppRGCTXDataType)3, 5214 },
	{ (Il2CppRGCTXDataType)3, 5217 },
	{ (Il2CppRGCTXDataType)2, 1367 },
	{ (Il2CppRGCTXDataType)2, 4105 },
	{ (Il2CppRGCTXDataType)3, 7269 },
	{ (Il2CppRGCTXDataType)3, 5219 },
	{ (Il2CppRGCTXDataType)2, 1844 },
	{ (Il2CppRGCTXDataType)3, 5218 },
	{ (Il2CppRGCTXDataType)2, 1755 },
	{ (Il2CppRGCTXDataType)2, 4056 },
	{ (Il2CppRGCTXDataType)2, 1904 },
	{ (Il2CppRGCTXDataType)2, 2008 },
	{ (Il2CppRGCTXDataType)3, 6566 },
	{ (Il2CppRGCTXDataType)2, 1419 },
	{ (Il2CppRGCTXDataType)3, 7829 },
	{ (Il2CppRGCTXDataType)3, 7830 },
	{ (Il2CppRGCTXDataType)3, 7835 },
	{ (Il2CppRGCTXDataType)2, 2113 },
	{ (Il2CppRGCTXDataType)3, 7832 },
	{ (Il2CppRGCTXDataType)3, 21057 },
	{ (Il2CppRGCTXDataType)2, 1375 },
	{ (Il2CppRGCTXDataType)3, 5245 },
	{ (Il2CppRGCTXDataType)1, 1833 },
	{ (Il2CppRGCTXDataType)2, 4065 },
	{ (Il2CppRGCTXDataType)3, 7831 },
	{ (Il2CppRGCTXDataType)1, 4065 },
	{ (Il2CppRGCTXDataType)1, 2113 },
	{ (Il2CppRGCTXDataType)2, 4126 },
	{ (Il2CppRGCTXDataType)2, 4065 },
	{ (Il2CppRGCTXDataType)3, 7836 },
	{ (Il2CppRGCTXDataType)3, 7834 },
	{ (Il2CppRGCTXDataType)3, 7833 },
	{ (Il2CppRGCTXDataType)2, 379 },
	{ (Il2CppRGCTXDataType)3, 5260 },
	{ (Il2CppRGCTXDataType)2, 521 },
};
extern const CustomAttributesCacheGenerator g_System_Core_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Core_CodeGenModule;
const Il2CppCodeGenModule g_System_Core_CodeGenModule = 
{
	"System.Core.dll",
	206,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	55,
	s_rgctxIndices,
	248,
	s_rgctxValues,
	NULL,
	g_System_Core_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
