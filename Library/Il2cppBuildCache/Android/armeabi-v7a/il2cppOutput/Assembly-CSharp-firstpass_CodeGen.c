﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void System.Web.HttpUtility::HtmlAttributeEncode(System.String,System.IO.TextWriter)
extern void HttpUtility_HtmlAttributeEncode_mFB310B3118FCBDEE520B3B66076E7C41A63F9B42 (void);
// 0x00000002 System.String System.Web.HttpUtility::HtmlAttributeEncode(System.String)
extern void HttpUtility_HtmlAttributeEncode_m76ABB6A0A9945B7A2D541FD345B3B2FF5EA3DEBA (void);
// 0x00000003 System.String System.Web.HttpUtility::UrlDecode(System.String)
extern void HttpUtility_UrlDecode_mDCE0DC8A8B2EEAB1DABFBBFD1D8A72674E00CB64 (void);
// 0x00000004 System.Char[] System.Web.HttpUtility::GetChars(System.IO.MemoryStream,System.Text.Encoding)
extern void HttpUtility_GetChars_m10FE2F88ED6C53BDFB106FB7C07EF0AB72E4B5EB (void);
// 0x00000005 System.Void System.Web.HttpUtility::WriteCharBytes(System.Collections.IList,System.Char,System.Text.Encoding)
extern void HttpUtility_WriteCharBytes_m2F4F0AD276CE4A8EA6A38B6C4EA1442E90A290D2 (void);
// 0x00000006 System.String System.Web.HttpUtility::UrlDecode(System.String,System.Text.Encoding)
extern void HttpUtility_UrlDecode_m25AD5F3B91B1424A81419D730B71E2A6A2160CE6 (void);
// 0x00000007 System.String System.Web.HttpUtility::UrlDecode(System.Byte[],System.Text.Encoding)
extern void HttpUtility_UrlDecode_mEB6BC975712FC342ED2ADDF45CEF1B79D4CBD28C (void);
// 0x00000008 System.Int32 System.Web.HttpUtility::GetInt(System.Byte)
extern void HttpUtility_GetInt_m822542991D7FA03EDB478A5CB22619F713449CD7 (void);
// 0x00000009 System.Int32 System.Web.HttpUtility::GetChar(System.Byte[],System.Int32,System.Int32)
extern void HttpUtility_GetChar_mBC124F1928642E4D97362707DA3531B38895DE42 (void);
// 0x0000000A System.Int32 System.Web.HttpUtility::GetChar(System.String,System.Int32,System.Int32)
extern void HttpUtility_GetChar_mA72B0E4325E8DDDAF038FD08DE9A66A6D3CB837B (void);
// 0x0000000B System.String System.Web.HttpUtility::UrlDecode(System.Byte[],System.Int32,System.Int32,System.Text.Encoding)
extern void HttpUtility_UrlDecode_mF3D2958AAF0CD705C0A74CBD0576EE0B250FC80F (void);
// 0x0000000C System.Byte[] System.Web.HttpUtility::UrlDecodeToBytes(System.Byte[])
extern void HttpUtility_UrlDecodeToBytes_m7B3E224594640CDD2E841E5EF0E4CCFF500EB5A4 (void);
// 0x0000000D System.Byte[] System.Web.HttpUtility::UrlDecodeToBytes(System.String)
extern void HttpUtility_UrlDecodeToBytes_mEEF17ECBC2186BCF7C628603CE34AFD449ABC15F (void);
// 0x0000000E System.Byte[] System.Web.HttpUtility::UrlDecodeToBytes(System.String,System.Text.Encoding)
extern void HttpUtility_UrlDecodeToBytes_m5E1930BAD4E6805FE07466B0BD948842A264D170 (void);
// 0x0000000F System.Byte[] System.Web.HttpUtility::UrlDecodeToBytes(System.Byte[],System.Int32,System.Int32)
extern void HttpUtility_UrlDecodeToBytes_mF6D06476481DD8423E6098F20D4B8C9B0184E546 (void);
// 0x00000010 System.String System.Web.HttpUtility::UrlEncode(System.String)
extern void HttpUtility_UrlEncode_mBD64946EBFC3A2B7248A9FAA38161E7CD12BE7CD (void);
// 0x00000011 System.String System.Web.HttpUtility::UrlEncode(System.String,System.Text.Encoding)
extern void HttpUtility_UrlEncode_m456C7D073217581F09233FFA03EAE8427B6C48D0 (void);
// 0x00000012 System.String System.Web.HttpUtility::UrlEncode(System.Byte[])
extern void HttpUtility_UrlEncode_mCD575B5B2FC1279555239FAFCFB1910F10970C8D (void);
// 0x00000013 System.String System.Web.HttpUtility::UrlEncode(System.Byte[],System.Int32,System.Int32)
extern void HttpUtility_UrlEncode_m87E304E827FD60E5E210C07FE5059352555065A0 (void);
// 0x00000014 System.Byte[] System.Web.HttpUtility::UrlEncodeToBytes(System.String)
extern void HttpUtility_UrlEncodeToBytes_m3A992052697CEB7D2E9DF9E52C07324C4AB35776 (void);
// 0x00000015 System.Byte[] System.Web.HttpUtility::UrlEncodeToBytes(System.String,System.Text.Encoding)
extern void HttpUtility_UrlEncodeToBytes_mD13BF036320C80DF0C9EC9B23E333358441374A8 (void);
// 0x00000016 System.Byte[] System.Web.HttpUtility::UrlEncodeToBytes(System.Byte[])
extern void HttpUtility_UrlEncodeToBytes_m415E4FF6CABDFA90FE561D2DE4731234F4401D76 (void);
// 0x00000017 System.Byte[] System.Web.HttpUtility::UrlEncodeToBytes(System.Byte[],System.Int32,System.Int32)
extern void HttpUtility_UrlEncodeToBytes_mD19F3DEA461605E11798CFBAB201B5941A9F0E42 (void);
// 0x00000018 System.String System.Web.HttpUtility::UrlEncodeUnicode(System.String)
extern void HttpUtility_UrlEncodeUnicode_mE75F2D069EC0D410B0A2D6621FE43FED2BA6EDA2 (void);
// 0x00000019 System.Byte[] System.Web.HttpUtility::UrlEncodeUnicodeToBytes(System.String)
extern void HttpUtility_UrlEncodeUnicodeToBytes_mEFD047B2F98F1C2D0A5A749E58CA6D00C890A403 (void);
// 0x0000001A System.String System.Web.HttpUtility::HtmlDecode(System.String)
extern void HttpUtility_HtmlDecode_m89865A9C9E38F40F6238AF90200DDF2512F4FCDB (void);
// 0x0000001B System.Void System.Web.HttpUtility::HtmlDecode(System.String,System.IO.TextWriter)
extern void HttpUtility_HtmlDecode_m2301529A30CC2D1FDA4A000E7F1D8865B28DCA43 (void);
// 0x0000001C System.String System.Web.HttpUtility::HtmlEncode(System.String)
extern void HttpUtility_HtmlEncode_m4B58D7D0DDA493AAAD183A892EDF155D5B37DE20 (void);
// 0x0000001D System.Void System.Web.HttpUtility::HtmlEncode(System.String,System.IO.TextWriter)
extern void HttpUtility_HtmlEncode_m03D7BBDFBF2822E88BE58F136890896578FBCC8F (void);
// 0x0000001E System.String System.Web.HttpUtility::HtmlEncode(System.Object)
extern void HttpUtility_HtmlEncode_m86FD4FE9ACB77E95954C9FBAE0A86CE4C0226FD8 (void);
// 0x0000001F System.String System.Web.HttpUtility::JavaScriptStringEncode(System.String)
extern void HttpUtility_JavaScriptStringEncode_mEA9D451EA516ECF3395237B45033C7B34760382B (void);
// 0x00000020 System.String System.Web.HttpUtility::JavaScriptStringEncode(System.String,System.Boolean)
extern void HttpUtility_JavaScriptStringEncode_m538A719B92DFACAC9E213E4734A1C0533F74DF9F (void);
// 0x00000021 System.String System.Web.HttpUtility::UrlPathEncode(System.String)
extern void HttpUtility_UrlPathEncode_mA98DCEB39FE9582BFEEA6AB5A9E2664E0BE80C78 (void);
// 0x00000022 System.Collections.Specialized.NameValueCollection System.Web.HttpUtility::ParseQueryString(System.String)
extern void HttpUtility_ParseQueryString_mB325C5BD841A4B1C0B56AA3EEBD259EFD85B67D8 (void);
// 0x00000023 System.Collections.Specialized.NameValueCollection System.Web.HttpUtility::ParseQueryString(System.String,System.Text.Encoding)
extern void HttpUtility_ParseQueryString_m9F35D508CDF4D4213BC118FB941A107D626CAFC5 (void);
// 0x00000024 System.Void System.Web.HttpUtility::ParseQueryString(System.String,System.Text.Encoding,System.Collections.Specialized.NameValueCollection)
extern void HttpUtility_ParseQueryString_m78D0C846989BC7A1BA1928D7344BE2D3057330FB (void);
// 0x00000025 System.Void System.Web.HttpUtility::.ctor()
extern void HttpUtility__ctor_m76A71DD56230E87B1D2FCB3BA0EA3505FD032965 (void);
// 0x00000026 System.String System.Web.HttpUtility/HttpQSCollection::ToString()
extern void HttpQSCollection_ToString_m9C58AAB57BF0207D5FEA293177393246BFEB18D4 (void);
// 0x00000027 System.Void System.Web.HttpUtility/HttpQSCollection::.ctor()
extern void HttpQSCollection__ctor_mB389555A49ADC48AD83FD08D48AA3BB4C109F231 (void);
// 0x00000028 System.Collections.Generic.IDictionary`2<System.String,System.Char> System.Web.Util.HttpEncoder::get_Entities()
extern void HttpEncoder_get_Entities_m54C0BE50D899420656EAD97AD36BE46CE035E795 (void);
// 0x00000029 System.Web.Util.HttpEncoder System.Web.Util.HttpEncoder::get_Current()
extern void HttpEncoder_get_Current_m2C148C53CD1782FB28CFA8D81A9267D95C68356E (void);
// 0x0000002A System.Void System.Web.Util.HttpEncoder::set_Current(System.Web.Util.HttpEncoder)
extern void HttpEncoder_set_Current_m13F47C04002980F9AD4A22CD78DDF27CAC96F14A (void);
// 0x0000002B System.Web.Util.HttpEncoder System.Web.Util.HttpEncoder::get_Default()
extern void HttpEncoder_get_Default_m0F86D1400927A0889893B0FAD06C40C888305627 (void);
// 0x0000002C System.Void System.Web.Util.HttpEncoder::.cctor()
extern void HttpEncoder__cctor_m5588B60D3A267C51709E86B1785EAA0808C629A4 (void);
// 0x0000002D System.Void System.Web.Util.HttpEncoder::HeaderNameValueEncode(System.String,System.String,System.String&,System.String&)
extern void HttpEncoder_HeaderNameValueEncode_m2DA9357552A34277C8FFABBB6D1977436F66657D (void);
// 0x0000002E System.Void System.Web.Util.HttpEncoder::StringBuilderAppend(System.String,System.Text.StringBuilder&)
extern void HttpEncoder_StringBuilderAppend_m42D772F6C0CF7B72388F4582165B6AE14E77C735 (void);
// 0x0000002F System.String System.Web.Util.HttpEncoder::EncodeHeaderString(System.String)
extern void HttpEncoder_EncodeHeaderString_mFFCDE1400E1723437DC974558D0E65628305F59E (void);
// 0x00000030 System.Void System.Web.Util.HttpEncoder::HtmlAttributeEncode(System.String,System.IO.TextWriter)
extern void HttpEncoder_HtmlAttributeEncode_m711698F231D441AC6EC219C4B45E3CB78607E2E8 (void);
// 0x00000031 System.Void System.Web.Util.HttpEncoder::HtmlDecode(System.String,System.IO.TextWriter)
extern void HttpEncoder_HtmlDecode_mFD44693A6DC61CF05B362588043F9DF87B4EAB65 (void);
// 0x00000032 System.Void System.Web.Util.HttpEncoder::HtmlEncode(System.String,System.IO.TextWriter)
extern void HttpEncoder_HtmlEncode_m3EFD95242BD31ABB43D2CD6A37684DF44CC9A7BA (void);
// 0x00000033 System.Byte[] System.Web.Util.HttpEncoder::UrlEncode(System.Byte[],System.Int32,System.Int32)
extern void HttpEncoder_UrlEncode_m94AA2673FED83A8A432021773C2E0EA0B2F5CEFC (void);
// 0x00000034 System.Web.Util.HttpEncoder System.Web.Util.HttpEncoder::GetCustomEncoderFromConfig()
extern void HttpEncoder_GetCustomEncoderFromConfig_m7105173C28832B8A3E1A279369309D5DCC746590 (void);
// 0x00000035 System.String System.Web.Util.HttpEncoder::UrlPathEncode(System.String)
extern void HttpEncoder_UrlPathEncode_m4799353783767D40A539D524591E038C8EB9449D (void);
// 0x00000036 System.Byte[] System.Web.Util.HttpEncoder::UrlEncodeToBytes(System.Byte[],System.Int32,System.Int32)
extern void HttpEncoder_UrlEncodeToBytes_m54605E546A567509DD80E184B3E64B1990B6956C (void);
// 0x00000037 System.String System.Web.Util.HttpEncoder::HtmlEncode(System.String)
extern void HttpEncoder_HtmlEncode_m9BF97452F4FF897E2D8AC094DFA57BD1E2A8269F (void);
// 0x00000038 System.String System.Web.Util.HttpEncoder::HtmlAttributeEncode(System.String)
extern void HttpEncoder_HtmlAttributeEncode_m1DF47A3525AE3D88033219A2528ECAB689FC4A42 (void);
// 0x00000039 System.String System.Web.Util.HttpEncoder::HtmlDecode(System.String)
extern void HttpEncoder_HtmlDecode_mD0EDD86498EDF3CDBB4CE4521B827AB7E5FCCFBC (void);
// 0x0000003A System.Boolean System.Web.Util.HttpEncoder::NotEncoded(System.Char)
extern void HttpEncoder_NotEncoded_m0337D8C19410E0A5F3000991DEB3ED60DC79FCC9 (void);
// 0x0000003B System.Void System.Web.Util.HttpEncoder::UrlEncodeChar(System.Char,System.IO.Stream,System.Boolean)
extern void HttpEncoder_UrlEncodeChar_mC171E4812EE815673E62FE0C77BD053A82DC8C77 (void);
// 0x0000003C System.Void System.Web.Util.HttpEncoder::UrlPathEncodeChar(System.Char,System.IO.Stream)
extern void HttpEncoder_UrlPathEncodeChar_m2C4BF7FA9BCBB4F9FD8C9BA01C55CC64DDE6BDBB (void);
// 0x0000003D System.Void System.Web.Util.HttpEncoder::InitEntities()
extern void HttpEncoder_InitEntities_m1B0CCCA2C51AD06644E416588820BABE11230343 (void);
// 0x0000003E System.Void System.Web.Util.HttpEncoder::.ctor()
extern void HttpEncoder__ctor_m16B6CE891F3BD847B3F25397628DD9EFE4C693F1 (void);
// 0x0000003F System.Void System.Web.Util.HttpEncoder/<>c::.cctor()
extern void U3CU3Ec__cctor_m64940D4CF4CB9C2AE5230188FBBF118A625FCCD5 (void);
// 0x00000040 System.Void System.Web.Util.HttpEncoder/<>c::.ctor()
extern void U3CU3Ec__ctor_mB2D9CC35899D065F44FA0CEBABCEB5A8513B7DA0 (void);
// 0x00000041 System.Web.Util.HttpEncoder System.Web.Util.HttpEncoder/<>c::<.cctor>b__13_0()
extern void U3CU3Ec_U3C_cctorU3Eb__13_0_m19DC89A621E62FBF1D83B7C6C4C0FC89B2449CB1 (void);
// 0x00000042 System.Boolean System.Web.Util.HttpEncoder/<>c::<EncodeHeaderString>b__16_0(System.Char)
extern void U3CU3Ec_U3CEncodeHeaderStringU3Eb__16_0_mD1308E53DE31892F56627928D48130F41A881ED8 (void);
// 0x00000043 System.Boolean System.Web.Util.HttpEncoder/<>c::<HtmlEncode>b__24_0(System.Char)
extern void U3CU3Ec_U3CHtmlEncodeU3Eb__24_0_m4F030B816C961EFC363199A4DE17625D14769C2B (void);
// 0x00000044 System.Boolean System.Web.Util.HttpEncoder/<>c::<HtmlAttributeEncode>b__25_0(System.Char)
extern void U3CU3Ec_U3CHtmlAttributeEncodeU3Eb__25_0_m0093B091713A8781FE24255E0214890BF6EE68B2 (void);
// 0x00000045 System.String Crosstales.ExtensionMethods::CTToTitleCase(System.String)
extern void ExtensionMethods_CTToTitleCase_m0FAF40693A916D1BA985195E30AA7B9753C11DE2 (void);
// 0x00000046 System.String Crosstales.ExtensionMethods::CTReverse(System.String)
extern void ExtensionMethods_CTReverse_m375B1B5F94367BC73BC054481C4528E1829758A0 (void);
// 0x00000047 System.String Crosstales.ExtensionMethods::CTReplace(System.String,System.String,System.String,System.StringComparison)
extern void ExtensionMethods_CTReplace_mE45837F5BF2ECDD64251F7DC85303D36308AFBB9 (void);
// 0x00000048 System.Boolean Crosstales.ExtensionMethods::CTEquals(System.String,System.String,System.StringComparison)
extern void ExtensionMethods_CTEquals_m70FD226B78B4DD7ED0672F326B16568EA3D9B446 (void);
// 0x00000049 System.Boolean Crosstales.ExtensionMethods::CTContains(System.String,System.String,System.StringComparison)
extern void ExtensionMethods_CTContains_m52B3F3D1019BBD1746371264A2671656CDADB3C2 (void);
// 0x0000004A System.Boolean Crosstales.ExtensionMethods::CTContainsAny(System.String,System.String,System.Char)
extern void ExtensionMethods_CTContainsAny_m50A29F87C6A50663D9B599FEA1D1359EC07969A3 (void);
// 0x0000004B System.Boolean Crosstales.ExtensionMethods::CTContainsAll(System.String,System.String,System.Char)
extern void ExtensionMethods_CTContainsAll_mAB9F9024F8E76F4449FE666602718634956C2464 (void);
// 0x0000004C System.String Crosstales.ExtensionMethods::CTRemoveNewLines(System.String,System.String,System.String)
extern void ExtensionMethods_CTRemoveNewLines_mF3F6F1FD9F609208AF748156579F02AB68B15C24 (void);
// 0x0000004D System.String Crosstales.ExtensionMethods::CTAddNewLines(System.String,System.String,System.String)
extern void ExtensionMethods_CTAddNewLines_m3FDE4E74DB180C8F829329BFFD4934CB93057581 (void);
// 0x0000004E System.Boolean Crosstales.ExtensionMethods::CTisNumeric(System.String)
extern void ExtensionMethods_CTisNumeric_mB664DBA85A056E3855D912C93DA7B704584752DC (void);
// 0x0000004F System.Boolean Crosstales.ExtensionMethods::CTisInteger(System.String)
extern void ExtensionMethods_CTisInteger_m9A0F9A80C8B5CDADCD3500534A9FF53271BC5EA3 (void);
// 0x00000050 System.Boolean Crosstales.ExtensionMethods::CTisEmail(System.String)
extern void ExtensionMethods_CTisEmail_m2336B119F8D08E08DF5335FEAC1699978FC6405F (void);
// 0x00000051 System.Boolean Crosstales.ExtensionMethods::CTisWebsite(System.String)
extern void ExtensionMethods_CTisWebsite_m7DC7369502F9A000269EEC781ABA71452667E85B (void);
// 0x00000052 System.Boolean Crosstales.ExtensionMethods::CTisCreditcard(System.String)
extern void ExtensionMethods_CTisCreditcard_m62CDA4DAC05FE6307258BB4626FF76CF939C3C78 (void);
// 0x00000053 System.Boolean Crosstales.ExtensionMethods::CTisIPv4(System.String)
extern void ExtensionMethods_CTisIPv4_mA58907D12ED35BC0D4A3277E8475EF095B440126 (void);
// 0x00000054 System.Boolean Crosstales.ExtensionMethods::CTisAlphanumeric(System.String)
extern void ExtensionMethods_CTisAlphanumeric_m2E1A017EEB60FF2F1E2391C0074E81B406A98A2A (void);
// 0x00000055 System.Boolean Crosstales.ExtensionMethods::CThasLineEndings(System.String)
extern void ExtensionMethods_CThasLineEndings_m95A836BD2BE4FFFFD877D5FD894AADF33428A129 (void);
// 0x00000056 System.Boolean Crosstales.ExtensionMethods::CThasInvalidChars(System.String)
extern void ExtensionMethods_CThasInvalidChars_m3C830A06687FDE6DE20154B64E2E4D531B7A9ED4 (void);
// 0x00000057 System.Boolean Crosstales.ExtensionMethods::CTStartsWith(System.String,System.String,System.StringComparison)
extern void ExtensionMethods_CTStartsWith_mFCFFAAF1BEE786BD337B3B2296D32744AB0A2542 (void);
// 0x00000058 System.Boolean Crosstales.ExtensionMethods::CTEndsWith(System.String,System.String,System.StringComparison)
extern void ExtensionMethods_CTEndsWith_m666D85A99AD0967EBD41A838F37819D298D23ADF (void);
// 0x00000059 System.Int32 Crosstales.ExtensionMethods::CTLastIndexOf(System.String,System.String,System.StringComparison)
extern void ExtensionMethods_CTLastIndexOf_m2F16141C163B099C5BCDF1D4E3426834EAD6A863 (void);
// 0x0000005A System.Int32 Crosstales.ExtensionMethods::CTIndexOf(System.String,System.String,System.StringComparison)
extern void ExtensionMethods_CTIndexOf_m3C965AE80FE82E5E12EC02B9019CF48B1D16B1C5 (void);
// 0x0000005B System.Int32 Crosstales.ExtensionMethods::CTIndexOf(System.String,System.String,System.Int32,System.StringComparison)
extern void ExtensionMethods_CTIndexOf_mD1B263B1E864EE567DD4C711597BA5556B0657E3 (void);
// 0x0000005C System.String Crosstales.ExtensionMethods::CTToBase64(System.String,System.Text.Encoding)
extern void ExtensionMethods_CTToBase64_m95D893BE1209C12BFC96FDCEA54371C1741ED35B (void);
// 0x0000005D System.String Crosstales.ExtensionMethods::CTFromBase64(System.String,System.Text.Encoding)
extern void ExtensionMethods_CTFromBase64_m5018159F11AAAD7AAE89A93D795932112BB18D1C (void);
// 0x0000005E System.Byte[] Crosstales.ExtensionMethods::CTFromBase64ToByteArray(System.String)
extern void ExtensionMethods_CTFromBase64ToByteArray_m66CADB7E423C7A554BB4B0069FCD5665402C67DE (void);
// 0x0000005F System.String Crosstales.ExtensionMethods::CTToHex(System.String,System.Boolean)
extern void ExtensionMethods_CTToHex_mE0934C22838185424921F80774725E4F5CD5FEED (void);
// 0x00000060 System.String Crosstales.ExtensionMethods::CTHexToString(System.String)
extern void ExtensionMethods_CTHexToString_m93D0EABF83200F514CC04FEAB3036266634B67B9 (void);
// 0x00000061 UnityEngine.Color32 Crosstales.ExtensionMethods::CTHexToColor32(System.String)
extern void ExtensionMethods_CTHexToColor32_mD1A93B26BC2D250FEA2834700CAC106519A62771 (void);
// 0x00000062 UnityEngine.Color Crosstales.ExtensionMethods::CTHexToColor(System.String)
extern void ExtensionMethods_CTHexToColor_m1FD7E347868D5E6A6450C36DF93DABAE02C468AC (void);
// 0x00000063 System.Byte[] Crosstales.ExtensionMethods::CTToByteArray(System.String,System.Text.Encoding)
extern void ExtensionMethods_CTToByteArray_m8BBB5E420285A72B5F964FF51944FB972837AE8A (void);
// 0x00000064 System.String Crosstales.ExtensionMethods::CTClearTags(System.String)
extern void ExtensionMethods_CTClearTags_m6F0896B414261998FD998D185D75F03B3151875E (void);
// 0x00000065 System.String Crosstales.ExtensionMethods::CTClearSpaces(System.String)
extern void ExtensionMethods_CTClearSpaces_m39BCF0466C31347D3DB7BAF627B2AF586CCC11B3 (void);
// 0x00000066 System.String Crosstales.ExtensionMethods::CTClearLineEndings(System.String)
extern void ExtensionMethods_CTClearLineEndings_m923BE72DD4DC59DB6307F90F2ABC30A90DBB692D (void);
// 0x00000067 System.Void Crosstales.ExtensionMethods::CTShuffle(T[],System.Int32)
// 0x00000068 System.String Crosstales.ExtensionMethods::CTDump(T[],System.String,System.String,System.Boolean,System.String)
// 0x00000069 System.String Crosstales.ExtensionMethods::CTDump(UnityEngine.Quaternion[])
extern void ExtensionMethods_CTDump_m9372AF9431740E725E0A0EE720EEB4B51EF8D626 (void);
// 0x0000006A System.String Crosstales.ExtensionMethods::CTDump(UnityEngine.Vector2[])
extern void ExtensionMethods_CTDump_mDD2831261D66781D294ADBFFBD9A397D4A5B4681 (void);
// 0x0000006B System.String Crosstales.ExtensionMethods::CTDump(UnityEngine.Vector3[])
extern void ExtensionMethods_CTDump_m2A792DFFEECEFF18E99A289916B9165C127E7883 (void);
// 0x0000006C System.String Crosstales.ExtensionMethods::CTDump(UnityEngine.Vector4[])
extern void ExtensionMethods_CTDump_mEDCBC052999C7F88ECF5897E865B6467FF1F452E (void);
// 0x0000006D System.String[] Crosstales.ExtensionMethods::CTToString(T[])
// 0x0000006E System.Single[] Crosstales.ExtensionMethods::CTToFloatArray(System.Byte[],System.Int32)
extern void ExtensionMethods_CTToFloatArray_m7E2B709CDA0C352394C296884A120A4FFB1576A3 (void);
// 0x0000006F System.Byte[] Crosstales.ExtensionMethods::CTToByteArray(System.Single[],System.Int32)
extern void ExtensionMethods_CTToByteArray_m11AEF2E58D65C64EF7BBE114385999E73726E236 (void);
// 0x00000070 UnityEngine.Texture2D Crosstales.ExtensionMethods::CTToTexture(System.Byte[])
extern void ExtensionMethods_CTToTexture_m12DC9C6DC82C07A891F5EA4AC628DEBB737C5DB2 (void);
// 0x00000071 UnityEngine.Sprite Crosstales.ExtensionMethods::CTToSprite(System.Byte[])
extern void ExtensionMethods_CTToSprite_mD7FF7050F7C0BE3EE55C86A445CF3559C0D07BA3 (void);
// 0x00000072 System.String Crosstales.ExtensionMethods::CTToString(System.Byte[],System.Text.Encoding)
extern void ExtensionMethods_CTToString_mBAB4670FE933D06A3F39F51219830F8D3CA946F5 (void);
// 0x00000073 System.String Crosstales.ExtensionMethods::CTToBase64(System.Byte[])
extern void ExtensionMethods_CTToBase64_m0E9D5F1F9174FAA4776F788ECA88A8709A97BCE5 (void);
// 0x00000074 System.Void Crosstales.ExtensionMethods::CTShuffle(System.Collections.Generic.IList`1<T>,System.Int32)
// 0x00000075 System.String Crosstales.ExtensionMethods::CTDump(System.Collections.Generic.IList`1<T>,System.String,System.String,System.Boolean,System.String)
// 0x00000076 System.String Crosstales.ExtensionMethods::CTDump(System.Collections.Generic.IList`1<UnityEngine.Quaternion>)
extern void ExtensionMethods_CTDump_m59F8DE0A578B3B891B4D8C1ED38688E86940F976 (void);
// 0x00000077 System.String Crosstales.ExtensionMethods::CTDump(System.Collections.Generic.IList`1<UnityEngine.Vector2>)
extern void ExtensionMethods_CTDump_m6E14577C017276ACD5DA587C029A664694E859C9 (void);
// 0x00000078 System.String Crosstales.ExtensionMethods::CTDump(System.Collections.Generic.IList`1<UnityEngine.Vector3>)
extern void ExtensionMethods_CTDump_mD3A9D8E4C2FAF2BE628602C89238233DBE170FDE (void);
// 0x00000079 System.String Crosstales.ExtensionMethods::CTDump(System.Collections.Generic.IList`1<UnityEngine.Vector4>)
extern void ExtensionMethods_CTDump_mB00AF342A1C001008DDEE5422E6CE9386ADF044C (void);
// 0x0000007A System.Collections.Generic.List`1<System.String> Crosstales.ExtensionMethods::CTToString(System.Collections.Generic.IList`1<T>)
// 0x0000007B System.String Crosstales.ExtensionMethods::CTDump(System.Collections.Generic.IDictionary`2<K,V>,System.String,System.String,System.Boolean,System.String)
// 0x0000007C System.Void Crosstales.ExtensionMethods::CTAddRange(System.Collections.Generic.IDictionary`2<K,V>,System.Collections.Generic.IDictionary`2<K,V>)
// 0x0000007D System.Byte[] Crosstales.ExtensionMethods::CTReadFully(System.IO.Stream)
extern void ExtensionMethods_CTReadFully_mCD211B8D82B74DE675FB131FBAC880B034B7B553 (void);
// 0x0000007E System.String Crosstales.ExtensionMethods::CTToHexRGB(UnityEngine.Color32)
extern void ExtensionMethods_CTToHexRGB_mEB271EF7DE0D38158B4D1C9E1F5A2DCAD7B97502 (void);
// 0x0000007F System.String Crosstales.ExtensionMethods::CTToHexRGB(UnityEngine.Color)
extern void ExtensionMethods_CTToHexRGB_m1E39198A3F7FD8F35CC5BD27984C09FD5CDFEC5A (void);
// 0x00000080 System.String Crosstales.ExtensionMethods::CTToHexRGBA(UnityEngine.Color32)
extern void ExtensionMethods_CTToHexRGBA_m1E583B32CC5214340E8C18EA005281D8748D689B (void);
// 0x00000081 System.String Crosstales.ExtensionMethods::CTToHexRGBA(UnityEngine.Color)
extern void ExtensionMethods_CTToHexRGBA_m8BF2160A9E4353077DBDE0F048F2B4000FC877EF (void);
// 0x00000082 UnityEngine.Vector3 Crosstales.ExtensionMethods::CTVector3(UnityEngine.Color32)
extern void ExtensionMethods_CTVector3_mCBBA0D200DA6E33DFDF5F968F64682AF279A1F72 (void);
// 0x00000083 UnityEngine.Vector3 Crosstales.ExtensionMethods::CTVector3(UnityEngine.Color)
extern void ExtensionMethods_CTVector3_m8CFAC4C85BA2694A83E2BDFB3A43B317005BDFE6 (void);
// 0x00000084 UnityEngine.Vector4 Crosstales.ExtensionMethods::CTVector4(UnityEngine.Color32)
extern void ExtensionMethods_CTVector4_m4CA428D976ADBCA1876085C9942F953F90DC6592 (void);
// 0x00000085 UnityEngine.Vector4 Crosstales.ExtensionMethods::CTVector4(UnityEngine.Color)
extern void ExtensionMethods_CTVector4_m3E12FD9E6E3310BE328E10AB105D3ADCFFF49522 (void);
// 0x00000086 UnityEngine.Vector2 Crosstales.ExtensionMethods::CTMultiply(UnityEngine.Vector2,UnityEngine.Vector2)
extern void ExtensionMethods_CTMultiply_m305DB38572F0613C1602758541DE685CD229B168 (void);
// 0x00000087 UnityEngine.Vector3 Crosstales.ExtensionMethods::CTMultiply(UnityEngine.Vector3,UnityEngine.Vector3)
extern void ExtensionMethods_CTMultiply_mFF0ED38A9DA8F4B6A0D650560279442844DEA6AE (void);
// 0x00000088 UnityEngine.Vector3 Crosstales.ExtensionMethods::CTFlatten(UnityEngine.Vector3)
extern void ExtensionMethods_CTFlatten_mC4047C6959F5B6BC261F88F4790682FCFC383CC1 (void);
// 0x00000089 UnityEngine.Quaternion Crosstales.ExtensionMethods::CTQuaternion(UnityEngine.Vector3)
extern void ExtensionMethods_CTQuaternion_m462BF09597B2909DB2D1128548CB3B1E3D0319A1 (void);
// 0x0000008A UnityEngine.Color Crosstales.ExtensionMethods::CTColorRGB(UnityEngine.Vector3,System.Single)
extern void ExtensionMethods_CTColorRGB_m39E122A470F7CAA46A440D03B2DDB4AB1D8B302B (void);
// 0x0000008B UnityEngine.Vector4 Crosstales.ExtensionMethods::CTMultiply(UnityEngine.Vector4,UnityEngine.Vector4)
extern void ExtensionMethods_CTMultiply_m5C8CA96F97B488BC1E1DD14854E6603037247CFB (void);
// 0x0000008C UnityEngine.Quaternion Crosstales.ExtensionMethods::CTQuaternion(UnityEngine.Vector4)
extern void ExtensionMethods_CTQuaternion_m4A9E9712734553E922C7789A7051CF06A384A0B0 (void);
// 0x0000008D UnityEngine.Color Crosstales.ExtensionMethods::CTColorRGBA(UnityEngine.Vector4)
extern void ExtensionMethods_CTColorRGBA_m0423B016C705D61B4186C44E18B5E4A168598A7A (void);
// 0x0000008E UnityEngine.Vector3 Crosstales.ExtensionMethods::CTVector3(UnityEngine.Quaternion)
extern void ExtensionMethods_CTVector3_m1F8621C9295026E750D0342405B2118B874F52E4 (void);
// 0x0000008F UnityEngine.Vector4 Crosstales.ExtensionMethods::CTVector4(UnityEngine.Quaternion)
extern void ExtensionMethods_CTVector4_m1F9C3149D5060CED2F9DFFF8A3CB39E95ABC5729 (void);
// 0x00000090 UnityEngine.Vector3 Crosstales.ExtensionMethods::CTCorrectLossyScale(UnityEngine.Canvas)
extern void ExtensionMethods_CTCorrectLossyScale_m8B4CB2FCAA60A49AEA8FBA793EE49D69AA7B11F7 (void);
// 0x00000091 System.Void Crosstales.ExtensionMethods::CTGetLocalCorners(UnityEngine.RectTransform,UnityEngine.Vector3[],UnityEngine.Canvas,System.Single,System.Boolean)
extern void ExtensionMethods_CTGetLocalCorners_m866B0CC7B69D7A4E17E65B2D870CBED28F9C7307 (void);
// 0x00000092 UnityEngine.Vector3[] Crosstales.ExtensionMethods::CTGetLocalCorners(UnityEngine.RectTransform,UnityEngine.Canvas,System.Single,System.Boolean)
extern void ExtensionMethods_CTGetLocalCorners_m93E67F2D92BA9C9F54D41CA6769ADE481D9208AF (void);
// 0x00000093 System.Void Crosstales.ExtensionMethods::CTGetScreenCorners(UnityEngine.RectTransform,UnityEngine.Vector3[],UnityEngine.Canvas,System.Single,System.Boolean)
extern void ExtensionMethods_CTGetScreenCorners_m2F1E5E3409D23E804EB07803B1317339FDF28B87 (void);
// 0x00000094 UnityEngine.Vector3[] Crosstales.ExtensionMethods::CTGetScreenCorners(UnityEngine.RectTransform,UnityEngine.Canvas,System.Single,System.Boolean)
extern void ExtensionMethods_CTGetScreenCorners_m39B94363A1BB9CAA84AC5F4D7C6F652FA07D35E0 (void);
// 0x00000095 UnityEngine.Bounds Crosstales.ExtensionMethods::CTGetBounds(UnityEngine.RectTransform,System.Single)
extern void ExtensionMethods_CTGetBounds_m2CB978298B520DEDC083F4E6F763D89534850569 (void);
// 0x00000096 System.Void Crosstales.ExtensionMethods::CTSetLeft(UnityEngine.RectTransform,System.Single)
extern void ExtensionMethods_CTSetLeft_m7241C29911EB32D2BF73B4F20ED2F0B8E1E465DF (void);
// 0x00000097 System.Void Crosstales.ExtensionMethods::CTSetRight(UnityEngine.RectTransform,System.Single)
extern void ExtensionMethods_CTSetRight_m7B0B7371C3243430FE05D24A4EC4E466F53E1C53 (void);
// 0x00000098 System.Void Crosstales.ExtensionMethods::CTSetTop(UnityEngine.RectTransform,System.Single)
extern void ExtensionMethods_CTSetTop_m7E0B0D5B5F497660AF71936B287C82378579FB40 (void);
// 0x00000099 System.Void Crosstales.ExtensionMethods::CTSetBottom(UnityEngine.RectTransform,System.Single)
extern void ExtensionMethods_CTSetBottom_m2DBDB41D978F0AC8191285887530BD93F1CD73BF (void);
// 0x0000009A System.Single Crosstales.ExtensionMethods::CTGetLeft(UnityEngine.RectTransform)
extern void ExtensionMethods_CTGetLeft_m81CE42F10665F9DB57E8E9E1E2071CC82CE56202 (void);
// 0x0000009B System.Single Crosstales.ExtensionMethods::CTGetRight(UnityEngine.RectTransform)
extern void ExtensionMethods_CTGetRight_mC83506F109D5492465A5B662C8B44A21BA3A4A33 (void);
// 0x0000009C System.Single Crosstales.ExtensionMethods::CTGetTop(UnityEngine.RectTransform)
extern void ExtensionMethods_CTGetTop_m99A1549D99E13343E61F5EDEA3F8888D64A56C5F (void);
// 0x0000009D System.Single Crosstales.ExtensionMethods::CTGetBottom(UnityEngine.RectTransform)
extern void ExtensionMethods_CTGetBottom_mA70B416EF67700E19A23E6ABDFA862839B45688E (void);
// 0x0000009E UnityEngine.Vector4 Crosstales.ExtensionMethods::CTGetLRTB(UnityEngine.RectTransform)
extern void ExtensionMethods_CTGetLRTB_mCA1C1A546BA77B9FDA6394815F879F8E91EFA733 (void);
// 0x0000009F System.Void Crosstales.ExtensionMethods::CTSetLRTB(UnityEngine.RectTransform,UnityEngine.Vector4)
extern void ExtensionMethods_CTSetLRTB_m5578C7E73B1BA1C8F17854D347D2EFFDBBE8FC23 (void);
// 0x000000A0 UnityEngine.GameObject Crosstales.ExtensionMethods::CTFind(UnityEngine.MonoBehaviour,System.String)
extern void ExtensionMethods_CTFind_m50A483F38921A7B82DB1B279AD2C2A0DCD213B6C (void);
// 0x000000A1 T Crosstales.ExtensionMethods::CTFind(UnityEngine.MonoBehaviour,System.String)
// 0x000000A2 UnityEngine.GameObject Crosstales.ExtensionMethods::CTFind(UnityEngine.GameObject,System.String)
extern void ExtensionMethods_CTFind_mA46461FE50A61DA998A72004CDF0BA8EA85275C6 (void);
// 0x000000A3 T Crosstales.ExtensionMethods::CTFind(UnityEngine.GameObject,System.String)
// 0x000000A4 UnityEngine.Bounds Crosstales.ExtensionMethods::CTGetBounds(UnityEngine.GameObject)
extern void ExtensionMethods_CTGetBounds_mC52C517BCE211AA80A937DFB6BBDA57B5365ACE7 (void);
// 0x000000A5 UnityEngine.Transform Crosstales.ExtensionMethods::CTFind(UnityEngine.Transform,System.String)
extern void ExtensionMethods_CTFind_mC05248D7AFAA90746B748E4020E169E1375B6988 (void);
// 0x000000A6 T Crosstales.ExtensionMethods::CTFind(UnityEngine.Transform,System.String)
// 0x000000A7 System.Byte[] Crosstales.ExtensionMethods::CTToPNG(UnityEngine.Sprite)
extern void ExtensionMethods_CTToPNG_m9287BB5CA9F16C447688DF15869B3CB73C8D2EAB (void);
// 0x000000A8 System.Byte[] Crosstales.ExtensionMethods::CTToJPG(UnityEngine.Sprite)
extern void ExtensionMethods_CTToJPG_m9ED2E438345EDC4ECEBBAD7BBF0C84AF3833956C (void);
// 0x000000A9 System.Byte[] Crosstales.ExtensionMethods::CTToTGA(UnityEngine.Sprite)
extern void ExtensionMethods_CTToTGA_m4130CAC5FA95B713F6FE6E10056940AFE45CAD9C (void);
// 0x000000AA System.Byte[] Crosstales.ExtensionMethods::CTToEXR(UnityEngine.Sprite)
extern void ExtensionMethods_CTToEXR_mD8FB2B651C2ED92132E2A06D332FE7D7F9A99310 (void);
// 0x000000AB System.Byte[] Crosstales.ExtensionMethods::CTToPNG(UnityEngine.Texture2D)
extern void ExtensionMethods_CTToPNG_mDC05E791A75ED3F86DD665B80986D0A332FF5193 (void);
// 0x000000AC System.Byte[] Crosstales.ExtensionMethods::CTToJPG(UnityEngine.Texture2D)
extern void ExtensionMethods_CTToJPG_mCD0C94CDD3306AC2B24B63DEF8CF561CCDB1C454 (void);
// 0x000000AD System.Byte[] Crosstales.ExtensionMethods::CTToTGA(UnityEngine.Texture2D)
extern void ExtensionMethods_CTToTGA_mE9EA3A0A4E9BC648F5291C7C445B5A394CB460FD (void);
// 0x000000AE System.Byte[] Crosstales.ExtensionMethods::CTToEXR(UnityEngine.Texture2D)
extern void ExtensionMethods_CTToEXR_m3BC7385919EB0ABC13BFA69C725FAE5281112DBB (void);
// 0x000000AF UnityEngine.Sprite Crosstales.ExtensionMethods::CTToSprite(UnityEngine.Texture2D,System.Single)
extern void ExtensionMethods_CTToSprite_m036A44049C3B80B9A4F9D4D4651D50092F0B289C (void);
// 0x000000B0 UnityEngine.Texture2D Crosstales.ExtensionMethods::CTRotate90(UnityEngine.Texture2D)
extern void ExtensionMethods_CTRotate90_mD1AB4357D802DB346FDD7FAC48ACDDBF9E12EC20 (void);
// 0x000000B1 UnityEngine.Texture2D Crosstales.ExtensionMethods::CTRotate180(UnityEngine.Texture2D)
extern void ExtensionMethods_CTRotate180_m9B592620D061F71ECF700F0DEA2436C8D24FEB21 (void);
// 0x000000B2 UnityEngine.Texture2D Crosstales.ExtensionMethods::CTRotate270(UnityEngine.Texture2D)
extern void ExtensionMethods_CTRotate270_m384C9E9CA403E44863115554B93B988457F1CE63 (void);
// 0x000000B3 UnityEngine.Texture2D Crosstales.ExtensionMethods::CTToTexture2D(UnityEngine.Texture)
extern void ExtensionMethods_CTToTexture2D_m9A7AE558EB1AE7B2CE6CF7B74F77855AB0E3573F (void);
// 0x000000B4 UnityEngine.Texture2D Crosstales.ExtensionMethods::CTFlipHorizontal(UnityEngine.Texture2D)
extern void ExtensionMethods_CTFlipHorizontal_m1A71CDC27223F806C143D8F1D7E51DECF5A202C4 (void);
// 0x000000B5 UnityEngine.Texture2D Crosstales.ExtensionMethods::CTFlipVertical(UnityEngine.Texture2D)
extern void ExtensionMethods_CTFlipVertical_m9B31708827E99F4154C9B40E4233852D497CFB62 (void);
// 0x000000B6 System.Boolean Crosstales.ExtensionMethods::CTHasActiveClip(UnityEngine.AudioSource)
extern void ExtensionMethods_CTHasActiveClip_m01F0DA4A380C00C95759FDA648D8D51289CC492E (void);
// 0x000000B7 System.Boolean Crosstales.ExtensionMethods::CTIsVisibleFrom(UnityEngine.Renderer,UnityEngine.Camera)
extern void ExtensionMethods_CTIsVisibleFrom_m5E3DED552FD8B132DD4E2B700E4BC17BCE83E587 (void);
// 0x000000B8 UnityEngine.Transform Crosstales.ExtensionMethods::deepSearch(UnityEngine.Transform,System.String)
extern void ExtensionMethods_deepSearch_m78B1F8D6139E2F196B37815757A5A29BD388FE59 (void);
// 0x000000B9 System.Single Crosstales.ExtensionMethods::bytesToFloat(System.Byte,System.Byte)
extern void ExtensionMethods_bytesToFloat_mA18BE713E7BFEBD57BC8A5D5D5353DCCDB29CBA1 (void);
// 0x000000BA System.Void Crosstales.ExtensionMethods::.cctor()
extern void ExtensionMethods__cctor_mE090B3D1469E9F7E1E7D1014E6E16F328170E7BC (void);
// 0x000000BB System.Void Crosstales.ExtensionMethods/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m2C47077153CBA7277CC4630ED5F134A341294672 (void);
// 0x000000BC System.Boolean Crosstales.ExtensionMethods/<>c__DisplayClass6_0::<CTContainsAny>b__0(System.String)
extern void U3CU3Ec__DisplayClass6_0_U3CCTContainsAnyU3Eb__0_m1C1AC3F24EC2227866E4F06EBEE7F7F7F74ED0B6 (void);
// 0x000000BD System.Void Crosstales.ExtensionMethods/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mB32411BDFA046D17ECFBB99C6C6443D1CB278451 (void);
// 0x000000BE System.Boolean Crosstales.ExtensionMethods/<>c__DisplayClass7_0::<CTContainsAll>b__0(System.String)
extern void U3CU3Ec__DisplayClass7_0_U3CCTContainsAllU3Eb__0_m70EA7F4283082A12DCE6D3005999FB5D3A78C747 (void);
// 0x000000BF System.Void Crosstales.ExtensionMethods/<>c__54`1::.cctor()
// 0x000000C0 System.Void Crosstales.ExtensionMethods/<>c__54`1::.ctor()
// 0x000000C1 System.String Crosstales.ExtensionMethods/<>c__54`1::<CTToString>b__54_0(T)
// 0x000000C2 System.Void Crosstales.ExtensionMethods/<>c__DisplayClass81_0::.ctor()
extern void U3CU3Ec__DisplayClass81_0__ctor_m8E43366EA85C9C13298797A5B0168694A89734BB (void);
// 0x000000C3 UnityEngine.Bounds Crosstales.ExtensionMethods/<>c__DisplayClass81_0::<CTGetBounds>b__0(UnityEngine.RectTransform)
extern void U3CU3Ec__DisplayClass81_0_U3CCTGetBoundsU3Eb__0_m15B5CB3C6584E57545865C3C6E3050196DBF8EB6 (void);
// 0x000000C4 System.Void Crosstales.OnlineCheck.StatusChangeEvent::.ctor()
extern void StatusChangeEvent__ctor_m3A04F9FA07A10715B49CAA90637167AF00DA0C54 (void);
// 0x000000C5 System.Boolean Crosstales.OnlineCheck.OnlineCheck::get_EndlessMode()
extern void OnlineCheck_get_EndlessMode_mFE6601437C30CB5E0800F13DEDCDFDB5634C6E72 (void);
// 0x000000C6 System.Void Crosstales.OnlineCheck.OnlineCheck::set_EndlessMode(System.Boolean)
extern void OnlineCheck_set_EndlessMode_mE2F515FA780525E09081840FC4F17EC94F951843 (void);
// 0x000000C7 System.Int32 Crosstales.OnlineCheck.OnlineCheck::get_IntervalMin()
extern void OnlineCheck_get_IntervalMin_mDE33E661FE5E80BDA66D8D5094D89EBAAC5A628C (void);
// 0x000000C8 System.Void Crosstales.OnlineCheck.OnlineCheck::set_IntervalMin(System.Int32)
extern void OnlineCheck_set_IntervalMin_m8E302ACC2C9F61DD6F8B43A9B3942AD4ED9DD9EC (void);
// 0x000000C9 System.Int32 Crosstales.OnlineCheck.OnlineCheck::get_IntervalMax()
extern void OnlineCheck_get_IntervalMax_mD584354F4FD03C36DA2AA34235321BF838C9A746 (void);
// 0x000000CA System.Void Crosstales.OnlineCheck.OnlineCheck::set_IntervalMax(System.Int32)
extern void OnlineCheck_set_IntervalMax_m5C5C7B1C88AB2CDA4C892114644B10C54DC75AA7 (void);
// 0x000000CB System.Int32 Crosstales.OnlineCheck.OnlineCheck::get_Timeout()
extern void OnlineCheck_get_Timeout_m44104F89C15998A0239D5BC55944425E6712B7D1 (void);
// 0x000000CC System.Void Crosstales.OnlineCheck.OnlineCheck::set_Timeout(System.Int32)
extern void OnlineCheck_set_Timeout_m9F5C7D118CDC03ED23D55F80C9188B34C43D9011 (void);
// 0x000000CD System.Boolean Crosstales.OnlineCheck.OnlineCheck::get_ForceWWW()
extern void OnlineCheck_get_ForceWWW_mAC076AF4AA6563981C165DE04D31D79806BDECE3 (void);
// 0x000000CE System.Void Crosstales.OnlineCheck.OnlineCheck::set_ForceWWW(System.Boolean)
extern void OnlineCheck_set_ForceWWW_mEA4C637FA472BB8BBF32A665DD6EB34C3FF50AE7 (void);
// 0x000000CF Crosstales.OnlineCheck.Data.CustomCheck Crosstales.OnlineCheck.OnlineCheck::get_CustomCheck()
extern void OnlineCheck_get_CustomCheck_mA389AC805D9EDD6A44881220B75F69BCBCB91BC4 (void);
// 0x000000D0 System.Void Crosstales.OnlineCheck.OnlineCheck::set_CustomCheck(Crosstales.OnlineCheck.Data.CustomCheck)
extern void OnlineCheck_set_CustomCheck_m7BC17170E2FCE2B52A76FA6F01507D957ED1F3E9 (void);
// 0x000000D1 System.Boolean Crosstales.OnlineCheck.OnlineCheck::get_Google204()
extern void OnlineCheck_get_Google204_mEDFD7C17757ABB025BC73DA46FC446551E10480D (void);
// 0x000000D2 System.Void Crosstales.OnlineCheck.OnlineCheck::set_Google204(System.Boolean)
extern void OnlineCheck_set_Google204_m00E4CFC3BB1BAEE645E291E30A491B4D7885A123 (void);
// 0x000000D3 System.Boolean Crosstales.OnlineCheck.OnlineCheck::get_GoogleBlank()
extern void OnlineCheck_get_GoogleBlank_mD45D754D00A8AAE87BFC19C81589EA930BD31184 (void);
// 0x000000D4 System.Void Crosstales.OnlineCheck.OnlineCheck::set_GoogleBlank(System.Boolean)
extern void OnlineCheck_set_GoogleBlank_m5DD888B47344A77A3399E7370D1D902C400B98CA (void);
// 0x000000D5 System.Boolean Crosstales.OnlineCheck.OnlineCheck::get_Microsoft()
extern void OnlineCheck_get_Microsoft_m1B7E66BD4EC21300315AB60A8F5E585557132DDE (void);
// 0x000000D6 System.Void Crosstales.OnlineCheck.OnlineCheck::set_Microsoft(System.Boolean)
extern void OnlineCheck_set_Microsoft_m1CDFC2558A018DE2B298DE6D49618306C83139E9 (void);
// 0x000000D7 System.Boolean Crosstales.OnlineCheck.OnlineCheck::get_Apple()
extern void OnlineCheck_get_Apple_mBDE98D3F32B447CB8E5D249DA9F679993A58DE6D (void);
// 0x000000D8 System.Void Crosstales.OnlineCheck.OnlineCheck::set_Apple(System.Boolean)
extern void OnlineCheck_set_Apple_mD0B3898C5B02502B9D95A94E66170AE0B934D342 (void);
// 0x000000D9 System.Boolean Crosstales.OnlineCheck.OnlineCheck::get_Ubuntu()
extern void OnlineCheck_get_Ubuntu_m9D36A591C91A98CEE47405893C0EB0AD50EC2E22 (void);
// 0x000000DA System.Void Crosstales.OnlineCheck.OnlineCheck::set_Ubuntu(System.Boolean)
extern void OnlineCheck_set_Ubuntu_mFCFFC571B714E691154C2FEF0272C2F466FB7E24 (void);
// 0x000000DB System.Boolean Crosstales.OnlineCheck.OnlineCheck::get_RunOnStart()
extern void OnlineCheck_get_RunOnStart_m7BC9AE00A737822BFE674FC143E352A8CAB96D7C (void);
// 0x000000DC System.Void Crosstales.OnlineCheck.OnlineCheck::set_RunOnStart(System.Boolean)
extern void OnlineCheck_set_RunOnStart_mC16B0A76398070312FFE46367C41660630C64C12 (void);
// 0x000000DD System.Single Crosstales.OnlineCheck.OnlineCheck::get_Delay()
extern void OnlineCheck_get_Delay_mCDCD8758BAB9EA8AF9AB5FE5EF646A2FA996BBBF (void);
// 0x000000DE System.Void Crosstales.OnlineCheck.OnlineCheck::set_Delay(System.Single)
extern void OnlineCheck_set_Delay_m1B9424EDCA2662E97D5115A9A4FC0A5DE4C5202E (void);
// 0x000000DF System.Boolean Crosstales.OnlineCheck.OnlineCheck::get_isInternetAvailable()
extern void OnlineCheck_get_isInternetAvailable_mB8D7ADA954F61774006B5B0CC83EF0BA926241ED (void);
// 0x000000E0 System.Void Crosstales.OnlineCheck.OnlineCheck::set_isInternetAvailable(System.Boolean)
extern void OnlineCheck_set_isInternetAvailable_m300C7DBB2A346EE1446CCA0C261B078F31D23AE1 (void);
// 0x000000E1 UnityEngine.NetworkReachability Crosstales.OnlineCheck.OnlineCheck::get_NetworkReachability()
extern void OnlineCheck_get_NetworkReachability_mC54E5BC6FC8758EAAE21BD0FEAD5DE0AC66542E6 (void);
// 0x000000E2 System.String Crosstales.OnlineCheck.OnlineCheck::get_NetworkReachabilityShort()
extern void OnlineCheck_get_NetworkReachabilityShort_m2459856073AB8AF7194F43B0A24BD887FCAB6051 (void);
// 0x000000E3 System.DateTime Crosstales.OnlineCheck.OnlineCheck::get_LastCheck()
extern void OnlineCheck_get_LastCheck_mB662D5E64AEFC884220C17267A75C630F94809AB (void);
// 0x000000E4 System.Void Crosstales.OnlineCheck.OnlineCheck::set_LastCheck(System.DateTime)
extern void OnlineCheck_set_LastCheck_m67A510F9D0DE82359817CFE6A0996F744A8A1C4C (void);
// 0x000000E5 System.Int64 Crosstales.OnlineCheck.OnlineCheck::get_DataDownloaded()
extern void OnlineCheck_get_DataDownloaded_m4E94946086AE3150005F2BE61C03E2A4A715D17E (void);
// 0x000000E6 System.Void Crosstales.OnlineCheck.OnlineCheck::set_DataDownloaded(System.Int64)
extern void OnlineCheck_set_DataDownloaded_mFCA7AA53E70908620761FB4814D07F3442F07A23 (void);
// 0x000000E7 System.Boolean Crosstales.OnlineCheck.OnlineCheck::get_isBusy()
extern void OnlineCheck_get_isBusy_m3750C24E7FB4837535846BDF55C2D70884CC2A79 (void);
// 0x000000E8 System.Void Crosstales.OnlineCheck.OnlineCheck::add_OnOnlineStatusChange(Crosstales.OnlineCheck.OnlineCheck/OnlineStatusChange)
extern void OnlineCheck_add_OnOnlineStatusChange_mE60151295E3C4E4453C8D3E317490B3C1B58007C (void);
// 0x000000E9 System.Void Crosstales.OnlineCheck.OnlineCheck::remove_OnOnlineStatusChange(Crosstales.OnlineCheck.OnlineCheck/OnlineStatusChange)
extern void OnlineCheck_remove_OnOnlineStatusChange_m191B25B8B2F624F83CD491A5B8D14ED50C3FEC86 (void);
// 0x000000EA System.Void Crosstales.OnlineCheck.OnlineCheck::add_OnNetworkReachabilityChange(Crosstales.OnlineCheck.OnlineCheck/NetworkReachabilityChange)
extern void OnlineCheck_add_OnNetworkReachabilityChange_m938758CE29A4DEEFC3653C02C9BF15F43673D1C7 (void);
// 0x000000EB System.Void Crosstales.OnlineCheck.OnlineCheck::remove_OnNetworkReachabilityChange(Crosstales.OnlineCheck.OnlineCheck/NetworkReachabilityChange)
extern void OnlineCheck_remove_OnNetworkReachabilityChange_m7D657C300452F527D4BDD45F55B0A14248F67B50 (void);
// 0x000000EC System.Void Crosstales.OnlineCheck.OnlineCheck::add_OnOnlineCheckComplete(Crosstales.OnlineCheck.OnlineCheck/OnlineCheckComplete)
extern void OnlineCheck_add_OnOnlineCheckComplete_mFB6BAEF23BA67D0BD31DF06481A1405680AE8BAC (void);
// 0x000000ED System.Void Crosstales.OnlineCheck.OnlineCheck::remove_OnOnlineCheckComplete(Crosstales.OnlineCheck.OnlineCheck/OnlineCheckComplete)
extern void OnlineCheck_remove_OnOnlineCheckComplete_mFA97D1EEFF914837E2BBF273EC72F1C3F7AC1CDC (void);
// 0x000000EE System.Void Crosstales.OnlineCheck.OnlineCheck::Awake()
extern void OnlineCheck_Awake_mCD464F402183A94AC95E7E0CBD99E34FA617543D (void);
// 0x000000EF System.Void Crosstales.OnlineCheck.OnlineCheck::Start()
extern void OnlineCheck_Start_m115F993E5C0228895B8B6FC88D879B541588D610 (void);
// 0x000000F0 System.Void Crosstales.OnlineCheck.OnlineCheck::Update()
extern void OnlineCheck_Update_mA2AFBCFAA7F3EF0406B0E5C9BE45ACB29A1059DF (void);
// 0x000000F1 System.Void Crosstales.OnlineCheck.OnlineCheck::OnApplicationQuit()
extern void OnlineCheck_OnApplicationQuit_m0D4FECA1A72F42A1CB9039C9CC28AF09D568C906 (void);
// 0x000000F2 System.Void Crosstales.OnlineCheck.OnlineCheck::OnValidate()
extern void OnlineCheck_OnValidate_m2C500D2B478094298F16E9EAF1547B613ACAA276 (void);
// 0x000000F3 System.Void Crosstales.OnlineCheck.OnlineCheck::ResetObject()
extern void OnlineCheck_ResetObject_m22CB37D35D47031422A34C7E9E8255F9D68DD5EF (void);
// 0x000000F4 System.Void Crosstales.OnlineCheck.OnlineCheck::Refresh(System.Boolean)
extern void OnlineCheck_Refresh_mD683B345922617666CE12EAAFBF0F3D1D3F55B22 (void);
// 0x000000F5 System.Collections.IEnumerator Crosstales.OnlineCheck.OnlineCheck::RefreshYield(System.Boolean)
extern void OnlineCheck_RefreshYield_mA1A4B0E79DC8083648A98F1BB2998F2B6469C10E (void);
// 0x000000F6 System.Void Crosstales.OnlineCheck.OnlineCheck::run()
extern void OnlineCheck_run_mD87AA95585CD731F6B9F96E2144FA4EA9E61846F (void);
// 0x000000F7 System.Collections.IEnumerator Crosstales.OnlineCheck.OnlineCheck::wwwCheck(System.String,System.String,System.Boolean,System.String,System.Int32,System.Boolean)
extern void OnlineCheck_wwwCheck_m3CB186B15336CE3CE92F62C87806A59B94BC8409 (void);
// 0x000000F8 System.Collections.IEnumerator Crosstales.OnlineCheck.OnlineCheck::google204Check(System.Boolean)
extern void OnlineCheck_google204Check_mB487FAC2EDF6B7CC49AB7D521194D04B7E4951A6 (void);
// 0x000000F9 System.Collections.IEnumerator Crosstales.OnlineCheck.OnlineCheck::googleBlankCheck(System.Boolean)
extern void OnlineCheck_googleBlankCheck_m83DA4A965E7A9D70219A28CBD4660A8B27FF52D5 (void);
// 0x000000FA System.Void Crosstales.OnlineCheck.OnlineCheck::threadCheck(System.Boolean&,System.String,System.String,System.Boolean,System.String,System.Int32,System.Boolean)
extern void OnlineCheck_threadCheck_mCE004BA671CB3A98C5F52101B6DA707BA9A0985C (void);
// 0x000000FB System.Collections.IEnumerator Crosstales.OnlineCheck.OnlineCheck::startWorker(System.String,System.String,System.Boolean,System.String,System.Int32,System.Boolean)
extern void OnlineCheck_startWorker_m712AE1F1F6163081D7ACAA1440E80FB909C84697 (void);
// 0x000000FC System.Collections.IEnumerator Crosstales.OnlineCheck.OnlineCheck::internetCheck()
extern void OnlineCheck_internetCheck_m26BC7810D3F1627A1C90F0160374E8822C6D2092 (void);
// 0x000000FD System.String Crosstales.OnlineCheck.OnlineCheck::URLAntiCacheRandomizer(System.String)
extern void OnlineCheck_URLAntiCacheRandomizer_m709DA7C524C0145F2E32E583926795561C507A7A (void);
// 0x000000FE System.Void Crosstales.OnlineCheck.OnlineCheck::onInternetStatusChange(System.Boolean)
extern void OnlineCheck_onInternetStatusChange_m6E9A0326368050D5C05412D9227AF50E3FBBE788 (void);
// 0x000000FF System.Void Crosstales.OnlineCheck.OnlineCheck::onNetworkReachabilityChange(UnityEngine.NetworkReachability)
extern void OnlineCheck_onNetworkReachabilityChange_m97979459943C17989F17A188F6C440E7342A4A80 (void);
// 0x00000100 System.Void Crosstales.OnlineCheck.OnlineCheck::onInternetCheckComplete(System.Boolean,UnityEngine.NetworkReachability)
extern void OnlineCheck_onInternetCheckComplete_mAC0DE391C3B624FC3805A6D660734EEB50A959AA (void);
// 0x00000101 System.Void Crosstales.OnlineCheck.OnlineCheck::.ctor()
extern void OnlineCheck__ctor_mF7D9AF1A8CCCD6D26A3CCD53771DD94DD099E65D (void);
// 0x00000102 System.Void Crosstales.OnlineCheck.OnlineCheck/OnlineStatusChange::.ctor(System.Object,System.IntPtr)
extern void OnlineStatusChange__ctor_m383226E646461948CA52E5EC011D1BE5F00E47CC (void);
// 0x00000103 System.Void Crosstales.OnlineCheck.OnlineCheck/OnlineStatusChange::Invoke(System.Boolean)
extern void OnlineStatusChange_Invoke_m3EBD9294701F09EF7A01D577D2702859AE62AADB (void);
// 0x00000104 System.IAsyncResult Crosstales.OnlineCheck.OnlineCheck/OnlineStatusChange::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void OnlineStatusChange_BeginInvoke_m9073E2045630CEB708D44175DF0324C7F6A277A4 (void);
// 0x00000105 System.Void Crosstales.OnlineCheck.OnlineCheck/OnlineStatusChange::EndInvoke(System.IAsyncResult)
extern void OnlineStatusChange_EndInvoke_mB94D496FBF1DE2A018ABFAD210A053C582B29145 (void);
// 0x00000106 System.Void Crosstales.OnlineCheck.OnlineCheck/NetworkReachabilityChange::.ctor(System.Object,System.IntPtr)
extern void NetworkReachabilityChange__ctor_m8B9C21490C4151DA9BBDD7FB80B8485A43558DF9 (void);
// 0x00000107 System.Void Crosstales.OnlineCheck.OnlineCheck/NetworkReachabilityChange::Invoke(UnityEngine.NetworkReachability)
extern void NetworkReachabilityChange_Invoke_m98EF3E65B2B7BE4E80E7673DAEF24B18EE45669F (void);
// 0x00000108 System.IAsyncResult Crosstales.OnlineCheck.OnlineCheck/NetworkReachabilityChange::BeginInvoke(UnityEngine.NetworkReachability,System.AsyncCallback,System.Object)
extern void NetworkReachabilityChange_BeginInvoke_m25F4C3075249EEF6D0170F6A62C99BF0F7B2E5E3 (void);
// 0x00000109 System.Void Crosstales.OnlineCheck.OnlineCheck/NetworkReachabilityChange::EndInvoke(System.IAsyncResult)
extern void NetworkReachabilityChange_EndInvoke_m4E75DCFB26FA0EEED5A1043863FB7DA0773F44BC (void);
// 0x0000010A System.Void Crosstales.OnlineCheck.OnlineCheck/OnlineCheckComplete::.ctor(System.Object,System.IntPtr)
extern void OnlineCheckComplete__ctor_mB464E11FA5FCCB5EC1B350A671C77DD3A2F78F14 (void);
// 0x0000010B System.Void Crosstales.OnlineCheck.OnlineCheck/OnlineCheckComplete::Invoke(System.Boolean,UnityEngine.NetworkReachability)
extern void OnlineCheckComplete_Invoke_mA36878652CF6BBBC05C7CA3F0D58E5E98641F77A (void);
// 0x0000010C System.IAsyncResult Crosstales.OnlineCheck.OnlineCheck/OnlineCheckComplete::BeginInvoke(System.Boolean,UnityEngine.NetworkReachability,System.AsyncCallback,System.Object)
extern void OnlineCheckComplete_BeginInvoke_mEF67ACF6E81185D7B099FAE6D374EF0EB51C4CB4 (void);
// 0x0000010D System.Void Crosstales.OnlineCheck.OnlineCheck/OnlineCheckComplete::EndInvoke(System.IAsyncResult)
extern void OnlineCheckComplete_EndInvoke_mA863EAD2A2AAF17871710C0665784F81E43226E3 (void);
// 0x0000010E System.Void Crosstales.OnlineCheck.OnlineCheck/<RefreshYield>d__129::.ctor(System.Int32)
extern void U3CRefreshYieldU3Ed__129__ctor_m0932B7AF205C1AFAC47F2B6F0B67AA4AFB84835C (void);
// 0x0000010F System.Void Crosstales.OnlineCheck.OnlineCheck/<RefreshYield>d__129::System.IDisposable.Dispose()
extern void U3CRefreshYieldU3Ed__129_System_IDisposable_Dispose_m9EFA937D9FC3AA4516183A844F32319214C5C26A (void);
// 0x00000110 System.Boolean Crosstales.OnlineCheck.OnlineCheck/<RefreshYield>d__129::MoveNext()
extern void U3CRefreshYieldU3Ed__129_MoveNext_m31D680917E9AA2AC6FAEA3BAD5DE41CEB77E0DD1 (void);
// 0x00000111 System.Object Crosstales.OnlineCheck.OnlineCheck/<RefreshYield>d__129::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRefreshYieldU3Ed__129_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDEB14477B0E0486BA7DD5773A134C59B9B1A05D4 (void);
// 0x00000112 System.Void Crosstales.OnlineCheck.OnlineCheck/<RefreshYield>d__129::System.Collections.IEnumerator.Reset()
extern void U3CRefreshYieldU3Ed__129_System_Collections_IEnumerator_Reset_m9F40BB9040C52254158400AA661985396BB26A4C (void);
// 0x00000113 System.Object Crosstales.OnlineCheck.OnlineCheck/<RefreshYield>d__129::System.Collections.IEnumerator.get_Current()
extern void U3CRefreshYieldU3Ed__129_System_Collections_IEnumerator_get_Current_m0F62B1BADD0DAB5B954EB34F5DBDEA1F9199A179 (void);
// 0x00000114 System.Void Crosstales.OnlineCheck.OnlineCheck/<wwwCheck>d__131::.ctor(System.Int32)
extern void U3CwwwCheckU3Ed__131__ctor_mB464818753B324E9DF2E095A7516F092E22998DE (void);
// 0x00000115 System.Void Crosstales.OnlineCheck.OnlineCheck/<wwwCheck>d__131::System.IDisposable.Dispose()
extern void U3CwwwCheckU3Ed__131_System_IDisposable_Dispose_mAC2958080A5562AF837FC380C4521D6F32A9A92B (void);
// 0x00000116 System.Boolean Crosstales.OnlineCheck.OnlineCheck/<wwwCheck>d__131::MoveNext()
extern void U3CwwwCheckU3Ed__131_MoveNext_mD9BC71A282D662A9ABFA578FD69D1895B02F133E (void);
// 0x00000117 System.Void Crosstales.OnlineCheck.OnlineCheck/<wwwCheck>d__131::<>m__Finally1()
extern void U3CwwwCheckU3Ed__131_U3CU3Em__Finally1_m172DA4043C63C673A5294EAB9C88CFE42B8F1E11 (void);
// 0x00000118 System.Object Crosstales.OnlineCheck.OnlineCheck/<wwwCheck>d__131::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CwwwCheckU3Ed__131_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m078EC1D91098DC6F1C9777A6530976909F538E9A (void);
// 0x00000119 System.Void Crosstales.OnlineCheck.OnlineCheck/<wwwCheck>d__131::System.Collections.IEnumerator.Reset()
extern void U3CwwwCheckU3Ed__131_System_Collections_IEnumerator_Reset_m61592DD78BFB0F4476608325FAE6010134CD4E39 (void);
// 0x0000011A System.Object Crosstales.OnlineCheck.OnlineCheck/<wwwCheck>d__131::System.Collections.IEnumerator.get_Current()
extern void U3CwwwCheckU3Ed__131_System_Collections_IEnumerator_get_Current_m0011E9B3464F6805E58DB9A63C700656204AAE70 (void);
// 0x0000011B System.Void Crosstales.OnlineCheck.OnlineCheck/<google204Check>d__132::.ctor(System.Int32)
extern void U3Cgoogle204CheckU3Ed__132__ctor_mD56DB6A6123E95B4FB5A2DD3468C7DCAD96AB020 (void);
// 0x0000011C System.Void Crosstales.OnlineCheck.OnlineCheck/<google204Check>d__132::System.IDisposable.Dispose()
extern void U3Cgoogle204CheckU3Ed__132_System_IDisposable_Dispose_m258A151CB942BDEFDB9F047FA0C28039BC3B5201 (void);
// 0x0000011D System.Boolean Crosstales.OnlineCheck.OnlineCheck/<google204Check>d__132::MoveNext()
extern void U3Cgoogle204CheckU3Ed__132_MoveNext_m0011D7BD3E4A7A238A981BA62E26AC2A24768F9B (void);
// 0x0000011E System.Void Crosstales.OnlineCheck.OnlineCheck/<google204Check>d__132::<>m__Finally1()
extern void U3Cgoogle204CheckU3Ed__132_U3CU3Em__Finally1_m3E6426E52D0976FEB6EAB2AC071C6A3751F613B9 (void);
// 0x0000011F System.Object Crosstales.OnlineCheck.OnlineCheck/<google204Check>d__132::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Cgoogle204CheckU3Ed__132_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FB0EF6D56A3488E55A4367B308EF4119847C346 (void);
// 0x00000120 System.Void Crosstales.OnlineCheck.OnlineCheck/<google204Check>d__132::System.Collections.IEnumerator.Reset()
extern void U3Cgoogle204CheckU3Ed__132_System_Collections_IEnumerator_Reset_mE5CDB8B60C91B9F9030380F2E98B378B16B7A63A (void);
// 0x00000121 System.Object Crosstales.OnlineCheck.OnlineCheck/<google204Check>d__132::System.Collections.IEnumerator.get_Current()
extern void U3Cgoogle204CheckU3Ed__132_System_Collections_IEnumerator_get_Current_m55BA33D3303B0EC5B915D9E93BD8FEEA94C646E0 (void);
// 0x00000122 System.Void Crosstales.OnlineCheck.OnlineCheck/<googleBlankCheck>d__133::.ctor(System.Int32)
extern void U3CgoogleBlankCheckU3Ed__133__ctor_m8F291E2D659EF39C62FC156BCA2D389587B72630 (void);
// 0x00000123 System.Void Crosstales.OnlineCheck.OnlineCheck/<googleBlankCheck>d__133::System.IDisposable.Dispose()
extern void U3CgoogleBlankCheckU3Ed__133_System_IDisposable_Dispose_m6823EFCDFC68DDFA593DA76CE12CD88DC653FACE (void);
// 0x00000124 System.Boolean Crosstales.OnlineCheck.OnlineCheck/<googleBlankCheck>d__133::MoveNext()
extern void U3CgoogleBlankCheckU3Ed__133_MoveNext_m9339E0ABC2571F3AA8CB8D3E8276C55EC9015E44 (void);
// 0x00000125 System.Void Crosstales.OnlineCheck.OnlineCheck/<googleBlankCheck>d__133::<>m__Finally1()
extern void U3CgoogleBlankCheckU3Ed__133_U3CU3Em__Finally1_m9338289AB5925ED76BD57D4CC8D6E9AD1DB3F08B (void);
// 0x00000126 System.Object Crosstales.OnlineCheck.OnlineCheck/<googleBlankCheck>d__133::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CgoogleBlankCheckU3Ed__133_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFCB73C941646FEA8753CFC711C28E0174B474F78 (void);
// 0x00000127 System.Void Crosstales.OnlineCheck.OnlineCheck/<googleBlankCheck>d__133::System.Collections.IEnumerator.Reset()
extern void U3CgoogleBlankCheckU3Ed__133_System_Collections_IEnumerator_Reset_mC9723822D012EDC85EC1965EF661CE135E28CFB0 (void);
// 0x00000128 System.Object Crosstales.OnlineCheck.OnlineCheck/<googleBlankCheck>d__133::System.Collections.IEnumerator.get_Current()
extern void U3CgoogleBlankCheckU3Ed__133_System_Collections_IEnumerator_get_Current_m43A1657DB7A5092AA08B859BC0404C0A3635780A (void);
// 0x00000129 System.Void Crosstales.OnlineCheck.OnlineCheck/<>c__DisplayClass135_0::.ctor()
extern void U3CU3Ec__DisplayClass135_0__ctor_m5A502B55D7490AD0A09C0679AFDD7961079EDAED (void);
// 0x0000012A System.Void Crosstales.OnlineCheck.OnlineCheck/<>c__DisplayClass135_0::<startWorker>b__0()
extern void U3CU3Ec__DisplayClass135_0_U3CstartWorkerU3Eb__0_m1C2E01C116DC0840D663E83F33CB4E05C97F1377 (void);
// 0x0000012B System.Void Crosstales.OnlineCheck.OnlineCheck/<startWorker>d__135::.ctor(System.Int32)
extern void U3CstartWorkerU3Ed__135__ctor_mBF48C92F9EF46F426C2CEF9BE406C4F51B2DB278 (void);
// 0x0000012C System.Void Crosstales.OnlineCheck.OnlineCheck/<startWorker>d__135::System.IDisposable.Dispose()
extern void U3CstartWorkerU3Ed__135_System_IDisposable_Dispose_mA94CB49D92794B0E95C576FFF365B5235A94CA6A (void);
// 0x0000012D System.Boolean Crosstales.OnlineCheck.OnlineCheck/<startWorker>d__135::MoveNext()
extern void U3CstartWorkerU3Ed__135_MoveNext_mE992CB14487BA2B52CADAA9CC3D90B2A4408552E (void);
// 0x0000012E System.Object Crosstales.OnlineCheck.OnlineCheck/<startWorker>d__135::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstartWorkerU3Ed__135_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5404CDC99EF7E2A8AFB47FBCE8153A028333590D (void);
// 0x0000012F System.Void Crosstales.OnlineCheck.OnlineCheck/<startWorker>d__135::System.Collections.IEnumerator.Reset()
extern void U3CstartWorkerU3Ed__135_System_Collections_IEnumerator_Reset_m4FC0A6B4B6F41C7E92E85FD01D147C1167139853 (void);
// 0x00000130 System.Object Crosstales.OnlineCheck.OnlineCheck/<startWorker>d__135::System.Collections.IEnumerator.get_Current()
extern void U3CstartWorkerU3Ed__135_System_Collections_IEnumerator_get_Current_m47742F8B1A9854677AD992B38704810BE3BCFF94 (void);
// 0x00000131 System.Void Crosstales.OnlineCheck.OnlineCheck/<internetCheck>d__136::.ctor(System.Int32)
extern void U3CinternetCheckU3Ed__136__ctor_mD5D5AE14F1C3C982EAF77FF103C0FD283EED1C89 (void);
// 0x00000132 System.Void Crosstales.OnlineCheck.OnlineCheck/<internetCheck>d__136::System.IDisposable.Dispose()
extern void U3CinternetCheckU3Ed__136_System_IDisposable_Dispose_m712C823E815029CFFE4C449601DF8F5F87FB60F0 (void);
// 0x00000133 System.Boolean Crosstales.OnlineCheck.OnlineCheck/<internetCheck>d__136::MoveNext()
extern void U3CinternetCheckU3Ed__136_MoveNext_mECB84DDBFE47BCFC8536C800C60607CBFBDA1AE0 (void);
// 0x00000134 System.Object Crosstales.OnlineCheck.OnlineCheck/<internetCheck>d__136::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CinternetCheckU3Ed__136_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA2AF9A7F386BED8090D59D5A41F18DC950D6C4A1 (void);
// 0x00000135 System.Void Crosstales.OnlineCheck.OnlineCheck/<internetCheck>d__136::System.Collections.IEnumerator.Reset()
extern void U3CinternetCheckU3Ed__136_System_Collections_IEnumerator_Reset_mD083701132826FCCEDBEE0047F6A63CC2F0B8C1C (void);
// 0x00000136 System.Object Crosstales.OnlineCheck.OnlineCheck/<internetCheck>d__136::System.Collections.IEnumerator.get_Current()
extern void U3CinternetCheckU3Ed__136_System_Collections_IEnumerator_get_Current_m90BAABE96B6E2DABE6BDDEF853DA73D2C7EF4820 (void);
// 0x00000137 System.Void Crosstales.OnlineCheck.Util.CTWebClientNotCached::.ctor()
extern void CTWebClientNotCached__ctor_mA5C72B001DF6698A15ED61C3978743310D985867 (void);
// 0x00000138 System.Void Crosstales.OnlineCheck.Util.CTWebClientNotCached::.ctor(System.Int32,System.Int32)
extern void CTWebClientNotCached__ctor_m8C1124A30A097F4FFC776145E1CC49CA61102D09 (void);
// 0x00000139 System.Net.WebRequest Crosstales.OnlineCheck.Util.CTWebClientNotCached::GetWebRequest(System.Uri)
extern void CTWebClientNotCached_GetWebRequest_mF2FB7234CE2842757828521B3CC748819188C606 (void);
// 0x0000013A System.Void Crosstales.OnlineCheck.Util.Config::.cctor()
extern void Config__cctor_m43C1B465937A8CBBD02B3F0DE7D38E7212AD1F5B (void);
// 0x0000013B System.Void Crosstales.OnlineCheck.Util.Constants::.ctor()
extern void Constants__ctor_m0AC084A744EC053B7AD3EC08964B425BA0F42B28 (void);
// 0x0000013C System.Void Crosstales.OnlineCheck.Util.Constants::.cctor()
extern void Constants__cctor_m522E24832468232B540F93961AF082B2AA01E282 (void);
// 0x0000013D System.Single Crosstales.OnlineCheck.Util.Context::get_ChecksPerMinute()
extern void Context_get_ChecksPerMinute_mCBE956B6D1A5F21A210C11926A93A64605BC7084 (void);
// 0x0000013E System.Single Crosstales.OnlineCheck.Util.Context::get_Downtime()
extern void Context_get_Downtime_m41597711C34557EA586785F8F1B53DE213BD80DF (void);
// 0x0000013F System.Void Crosstales.OnlineCheck.Util.Context::.cctor()
extern void Context__cctor_mB85BA0900F86AA8F3F9AC32288C370A8CDAA5D70 (void);
// 0x00000140 System.Void Crosstales.OnlineCheck.Util.Helper::CreateCustomCheck()
extern void Helper_CreateCustomCheck_mB3C29EDE863F494730B2C99CF3558CCAA6305B91 (void);
// 0x00000141 System.Void Crosstales.OnlineCheck.Util.Helper::.ctor()
extern void Helper__ctor_mA8A0D6CC0BE8725B52B45C3732F8E7E5D8E71839 (void);
// 0x00000142 System.String Crosstales.OnlineCheck.Util.NetworkInfo::get_PublicIP()
extern void NetworkInfo_get_PublicIP_mBC5ED6357183A4BAF8BC7AED4CE763ACA165C2AF (void);
// 0x00000143 System.Collections.Generic.List`1<Crosstales.OnlineCheck.Model.NetworkInterface> Crosstales.OnlineCheck.Util.NetworkInfo::get_LastNetworkInterfaces()
extern void NetworkInfo_get_LastNetworkInterfaces_m42497C95D2C79874E3EADE637D746C363B133EF9 (void);
// 0x00000144 System.String Crosstales.OnlineCheck.Util.NetworkInfo::get_LastPublicIP()
extern void NetworkInfo_get_LastPublicIP_mFE5821ECF2B92421D5826D387AA8EBBC4FD20A14 (void);
// 0x00000145 System.Void Crosstales.OnlineCheck.Util.NetworkInfo::Refresh()
extern void NetworkInfo_Refresh_mEC08F246AD10B37F4771F9DB5A75A4E468EE43D7 (void);
// 0x00000146 System.Collections.Generic.List`1<Crosstales.OnlineCheck.Model.NetworkInterface> Crosstales.OnlineCheck.Util.NetworkInfo::getNetworkInterfaces(System.Boolean)
extern void NetworkInfo_getNetworkInterfaces_m608BE6959F202830463DF323179BFBF9686AB0A6 (void);
// 0x00000147 System.Boolean Crosstales.OnlineCheck.Util.NetworkInfo::get_isPlatformSupported()
extern void NetworkInfo_get_isPlatformSupported_m97DB368C1CF3C8A5EC217A0CC7BA0EC65B8965A3 (void);
// 0x00000148 System.Void Crosstales.OnlineCheck.Util.NetworkInfo/<>c::.cctor()
extern void U3CU3Ec__cctor_m82BCFEBBD0E667CA01566A3F8E223B3ABF8B25E7 (void);
// 0x00000149 System.Void Crosstales.OnlineCheck.Util.NetworkInfo/<>c::.ctor()
extern void U3CU3Ec__ctor_m2E7E6BB3B25589A2F51E6E963935187E8198BE65 (void);
// 0x0000014A System.Boolean Crosstales.OnlineCheck.Util.NetworkInfo/<>c::<getNetworkInterfaces>b__9_0(System.Net.NetworkInformation.NetworkInterface)
extern void U3CU3Ec_U3CgetNetworkInterfacesU3Eb__9_0_m07EB33AD1B65F90D75FE669F55F69FCF5F7711D6 (void);
// 0x0000014B System.String Crosstales.OnlineCheck.Util.NetworkInfo/<>c::<getNetworkInterfaces>b__9_1(System.Byte)
extern void U3CU3Ec_U3CgetNetworkInterfacesU3Eb__9_1_mABDA3454371D9793F38104E2588BE543F1835F83 (void);
// 0x0000014C System.Boolean Crosstales.OnlineCheck.Util.NetworkInfo/<>c::<getNetworkInterfaces>b__9_2(System.Net.NetworkInformation.UnicastIPAddressInformation)
extern void U3CU3Ec_U3CgetNetworkInterfacesU3Eb__9_2_mB1DE918C36EC2CD61EB62C90570F47A315C3A0A8 (void);
// 0x0000014D System.Net.IPAddress Crosstales.OnlineCheck.Util.NetworkInfo/<>c::<getNetworkInterfaces>b__9_3(System.Net.NetworkInformation.GatewayIPAddressInformation)
extern void U3CU3Ec_U3CgetNetworkInterfacesU3Eb__9_3_m7188A1FCC288B9E2E44FA8A482041C1B35599CF6 (void);
// 0x0000014E System.Boolean Crosstales.OnlineCheck.Util.NetworkInfo/<>c::<getNetworkInterfaces>b__9_4(System.Net.IPAddress)
extern void U3CU3Ec_U3CgetNetworkInterfacesU3Eb__9_4_m7ED5E717763A18ED4EC10915ED94BA6010512B85 (void);
// 0x0000014F System.Void Crosstales.OnlineCheck.Util.SetupProject::.cctor()
extern void SetupProject__cctor_mEC95DF42E91FFA0E68649767A1C5FB1C44DEF921 (void);
// 0x00000150 System.Void Crosstales.OnlineCheck.Util.SetupProject::setup()
extern void SetupProject_setup_m5E022ECA6395D4AF9E1E8095957FEF8AA40A8931 (void);
// 0x00000151 System.Void Crosstales.OnlineCheck.Util.SetupProject::.ctor()
extern void SetupProject__ctor_m97A0EBF819072E49601E251B39DE504A8A058EFC (void);
// 0x00000152 System.Void Crosstales.OnlineCheck.Model.NetworkInterface::.ctor(System.String,System.String,System.Net.NetworkInformation.NetworkInterfaceType,System.Net.IPAddress,System.Net.IPAddress,System.String,System.Net.IPAddress,System.Int64,System.Net.NetworkInformation.OperationalStatus)
extern void NetworkInterface__ctor_m6D1F4B83988451D4CF39A1C01FF76CEE0CEE3BC9 (void);
// 0x00000153 System.String Crosstales.OnlineCheck.Model.NetworkInterface::ToString()
extern void NetworkInterface_ToString_mF2A79DDE1C176BC3116A48BF2E380B173617E2B5 (void);
// 0x00000154 System.String Crosstales.OnlineCheck.Data.CustomCheck::ToString()
extern void CustomCheck_ToString_m6E88886B468D899018D369AE25C87E1E32DC347E (void);
// 0x00000155 System.Boolean Crosstales.OnlineCheck.Data.CustomCheck::Equals(System.Object)
extern void CustomCheck_Equals_m3DD1E324E38F9B9FEE6E481B7C4B5296BA3C9039 (void);
// 0x00000156 System.Int32 Crosstales.OnlineCheck.Data.CustomCheck::GetHashCode()
extern void CustomCheck_GetHashCode_mBB495324566E62B663A38077EE998E9E51CD94C8 (void);
// 0x00000157 System.Void Crosstales.OnlineCheck.Data.CustomCheck::.ctor()
extern void CustomCheck__ctor_mD1147BD78321C6386EF13680769B876F79621F90 (void);
// 0x00000158 System.Void Crosstales.OnlineCheck.Tool.PingCompleteEvent::.ctor()
extern void PingCompleteEvent__ctor_m2350D7704AF82EDAD04FABBA78DE391919FD41FC (void);
// 0x00000159 System.Void Crosstales.OnlineCheck.Tool.PingCheck::add_OnPingCompleted(Crosstales.OnlineCheck.Tool.PingCheck/PingCompleted)
extern void PingCheck_add_OnPingCompleted_mEA7E0B5AC5977EAF7382F1469B4FF16F11B60F85 (void);
// 0x0000015A System.Void Crosstales.OnlineCheck.Tool.PingCheck::remove_OnPingCompleted(Crosstales.OnlineCheck.Tool.PingCheck/PingCompleted)
extern void PingCheck_remove_OnPingCompleted_m40B6D6EE813A7ADE10EE4611C0293B79C7198282 (void);
// 0x0000015B System.String Crosstales.OnlineCheck.Tool.PingCheck::get_HostName()
extern void PingCheck_get_HostName_m5ADE1C02FF3D3B68EE5660475E37D7C932EFFC0D (void);
// 0x0000015C System.Void Crosstales.OnlineCheck.Tool.PingCheck::set_HostName(System.String)
extern void PingCheck_set_HostName_m8E707DE8E864AA0B488606DE69208454F614FB33 (void);
// 0x0000015D System.Single Crosstales.OnlineCheck.Tool.PingCheck::get_Timeout()
extern void PingCheck_get_Timeout_mC4B9EE925B0C10249D7E327D93CEED341AAEB861 (void);
// 0x0000015E System.Void Crosstales.OnlineCheck.Tool.PingCheck::set_Timeout(System.Single)
extern void PingCheck_set_Timeout_mAA9411D5E2D707AAA7C660DFB1A1BDAA9D61E375 (void);
// 0x0000015F System.Boolean Crosstales.OnlineCheck.Tool.PingCheck::get_RunOnStart()
extern void PingCheck_get_RunOnStart_m61B19A7926EE59C0C20CAA1435AC8F60795B20C6 (void);
// 0x00000160 System.Void Crosstales.OnlineCheck.Tool.PingCheck::set_RunOnStart(System.Boolean)
extern void PingCheck_set_RunOnStart_m0F766632EFD0F499B646036E2341FD23DCC22BB6 (void);
// 0x00000161 System.String Crosstales.OnlineCheck.Tool.PingCheck::get_LastHost()
extern void PingCheck_get_LastHost_m28A8825F4DAD9E16E545CE34B9BE3163C503FE78 (void);
// 0x00000162 System.Void Crosstales.OnlineCheck.Tool.PingCheck::set_LastHost(System.String)
extern void PingCheck_set_LastHost_m2FEDAEFB0E8ABDC31BE0CCD1797BFE4C1400E8C7 (void);
// 0x00000163 System.String Crosstales.OnlineCheck.Tool.PingCheck::get_LastIP()
extern void PingCheck_get_LastIP_mB8FF7A2C0226AC642FE9CFEF7D0F88D334C588EE (void);
// 0x00000164 System.Void Crosstales.OnlineCheck.Tool.PingCheck::set_LastIP(System.String)
extern void PingCheck_set_LastIP_mCC80740815D94DF602FC47F60419524A4670A6E3 (void);
// 0x00000165 System.Single Crosstales.OnlineCheck.Tool.PingCheck::get_LastPingTime()
extern void PingCheck_get_LastPingTime_m0A9B7373550A0B6E214DC85E4810E0F492075AC6 (void);
// 0x00000166 System.Int32 Crosstales.OnlineCheck.Tool.PingCheck::get_LastPingTimeMilliseconds()
extern void PingCheck_get_LastPingTimeMilliseconds_m7288D1EC12E3C366054DF2BFFE28B5A23151B249 (void);
// 0x00000167 System.Void Crosstales.OnlineCheck.Tool.PingCheck::set_LastPingTimeMilliseconds(System.Int32)
extern void PingCheck_set_LastPingTimeMilliseconds_mE38F8911802C0358F00EFD57FF05A4FE0BC09FA4 (void);
// 0x00000168 System.Boolean Crosstales.OnlineCheck.Tool.PingCheck::get_isBusy()
extern void PingCheck_get_isBusy_mE1481885876C2C5992FBD31F6D287C8110E79FEE (void);
// 0x00000169 System.Void Crosstales.OnlineCheck.Tool.PingCheck::set_isBusy(System.Boolean)
extern void PingCheck_set_isBusy_m57F3210C204477EDE0220BCF94928541DEF4D280 (void);
// 0x0000016A System.Boolean Crosstales.OnlineCheck.Tool.PingCheck::get_isPlatformSupported()
extern void PingCheck_get_isPlatformSupported_m222F90709E709BB2C8BB42B9A3EACFD89A41F00C (void);
// 0x0000016B System.Void Crosstales.OnlineCheck.Tool.PingCheck::Awake()
extern void PingCheck_Awake_mC7B1B6033428B95991230B4B0F1790F122C0E6F7 (void);
// 0x0000016C System.Void Crosstales.OnlineCheck.Tool.PingCheck::Start()
extern void PingCheck_Start_m0976BE20FA2AF230CA89E775B4DAD361691879AE (void);
// 0x0000016D System.Void Crosstales.OnlineCheck.Tool.PingCheck::Ping()
extern void PingCheck_Ping_m952368905FEAA337729538CEA6699F9BE970E82D (void);
// 0x0000016E System.Void Crosstales.OnlineCheck.Tool.PingCheck::Ping(System.String)
extern void PingCheck_Ping_mAD46DFFD6A0B69AB0F0C9FE89DB7ACA97BDA24E9 (void);
// 0x0000016F System.Collections.IEnumerator Crosstales.OnlineCheck.Tool.PingCheck::ping(System.String)
extern void PingCheck_ping_m598086683E20BB9CD8864A8D7772863DD963A91A (void);
// 0x00000170 System.Void Crosstales.OnlineCheck.Tool.PingCheck::onPingCompleted(System.String,System.String,System.Single)
extern void PingCheck_onPingCompleted_m273DEDC157EE3387D019E09E43F57B24480DE4FD (void);
// 0x00000171 System.Void Crosstales.OnlineCheck.Tool.PingCheck::.ctor()
extern void PingCheck__ctor_m43EEC6DBFF7730E73BE34CACC33E525CD100F6C9 (void);
// 0x00000172 System.Void Crosstales.OnlineCheck.Tool.PingCheck/PingCompleted::.ctor(System.Object,System.IntPtr)
extern void PingCompleted__ctor_m0E67C6006238356C1D67A60E161C7A5643E28877 (void);
// 0x00000173 System.Void Crosstales.OnlineCheck.Tool.PingCheck/PingCompleted::Invoke(System.String,System.String,System.Single)
extern void PingCompleted_Invoke_m41476E1D660B013E1D73BD0798291C3FECD9A207 (void);
// 0x00000174 System.IAsyncResult Crosstales.OnlineCheck.Tool.PingCheck/PingCompleted::BeginInvoke(System.String,System.String,System.Single,System.AsyncCallback,System.Object)
extern void PingCompleted_BeginInvoke_mC338BA6051A6EF0869091AACD0A7291EF6481E1E (void);
// 0x00000175 System.Void Crosstales.OnlineCheck.Tool.PingCheck/PingCompleted::EndInvoke(System.IAsyncResult)
extern void PingCompleted_EndInvoke_mE86091F28863919DE41FFB22D7B4506D8D108B65 (void);
// 0x00000176 System.Void Crosstales.OnlineCheck.Tool.PingCheck/<ping>d__41::.ctor(System.Int32)
extern void U3CpingU3Ed__41__ctor_m0952AE64F05F49E60C3A7802B2D62D018B6EFBA1 (void);
// 0x00000177 System.Void Crosstales.OnlineCheck.Tool.PingCheck/<ping>d__41::System.IDisposable.Dispose()
extern void U3CpingU3Ed__41_System_IDisposable_Dispose_mB7F8AC235AF49225E193B99003ADFD5B42B40DEA (void);
// 0x00000178 System.Boolean Crosstales.OnlineCheck.Tool.PingCheck/<ping>d__41::MoveNext()
extern void U3CpingU3Ed__41_MoveNext_mC0BAA2BBD7559F6B16AD92F73589268A2830A311 (void);
// 0x00000179 System.Object Crosstales.OnlineCheck.Tool.PingCheck/<ping>d__41::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CpingU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6632216B79E50B7860AEAB94FD3A015DDB4E393 (void);
// 0x0000017A System.Void Crosstales.OnlineCheck.Tool.PingCheck/<ping>d__41::System.Collections.IEnumerator.Reset()
extern void U3CpingU3Ed__41_System_Collections_IEnumerator_Reset_m2F3E9B866A0EDB096B724B46371BC59E78C1CFAD (void);
// 0x0000017B System.Object Crosstales.OnlineCheck.Tool.PingCheck/<ping>d__41::System.Collections.IEnumerator.get_Current()
extern void U3CpingU3Ed__41_System_Collections_IEnumerator_get_Current_m24F550144A66BCF6840855ABB5AB697CF4115A21 (void);
// 0x0000017C System.Void Crosstales.OnlineCheck.Tool.Proxy::.cctor()
extern void Proxy__cctor_mE857A50630A00BD5535C0124A2EEF10EBA5A28FD (void);
// 0x0000017D System.Boolean Crosstales.OnlineCheck.Tool.Proxy::get_hasHTTPProxy()
extern void Proxy_get_hasHTTPProxy_m1184684C993FD6988A6314C9F0829E2FE199FA3F (void);
// 0x0000017E System.Void Crosstales.OnlineCheck.Tool.Proxy::set_hasHTTPProxy(System.Boolean)
extern void Proxy_set_hasHTTPProxy_mACFF6FC5482BF6F9BB6AAEED9015B0D9B7A72BAD (void);
// 0x0000017F System.Boolean Crosstales.OnlineCheck.Tool.Proxy::get_hasHTTPSProxy()
extern void Proxy_get_hasHTTPSProxy_mFDB3265F542D6C9A234DEDD7C7B661FE04DC48EA (void);
// 0x00000180 System.Void Crosstales.OnlineCheck.Tool.Proxy::set_hasHTTPSProxy(System.Boolean)
extern void Proxy_set_hasHTTPSProxy_m62EF9B3308635CDA6648D27ACDB529A7DA0BC624 (void);
// 0x00000181 System.Void Crosstales.OnlineCheck.Tool.Proxy::Awake()
extern void Proxy_Awake_m93422FD87C9E528FC86D4C0FC93ABD202CE70AE2 (void);
// 0x00000182 System.Void Crosstales.OnlineCheck.Tool.Proxy::Update()
extern void Proxy_Update_mBFB017B4FBE09A2057219616E7904F0D239EDCB4 (void);
// 0x00000183 System.Void Crosstales.OnlineCheck.Tool.Proxy::EnableHTTPProxy()
extern void Proxy_EnableHTTPProxy_m5346CBBE46128F0BB969C7D1C713E8269E9A4ACB (void);
// 0x00000184 System.Void Crosstales.OnlineCheck.Tool.Proxy::EnableHTTPSProxy()
extern void Proxy_EnableHTTPSProxy_m8ABFDF540FADE13996F9B5816B8ADBB9A7CBDA34 (void);
// 0x00000185 System.Void Crosstales.OnlineCheck.Tool.Proxy::EnableHTTPProxy(System.String,System.Int32,System.String,System.String,System.String)
extern void Proxy_EnableHTTPProxy_m55F8DC4D070E41A53D4DCF4FD717458103E765CB (void);
// 0x00000186 System.Void Crosstales.OnlineCheck.Tool.Proxy::EnableHTTPSProxy(System.String,System.Int32,System.String,System.String,System.String)
extern void Proxy_EnableHTTPSProxy_m2FA5216AEDEACAA2E9C17D81B228B35627D490CE (void);
// 0x00000187 System.Void Crosstales.OnlineCheck.Tool.Proxy::DisableHTTPProxy()
extern void Proxy_DisableHTTPProxy_mEB51F5A2A937CE145484DB0951C19D3C443F240D (void);
// 0x00000188 System.Void Crosstales.OnlineCheck.Tool.Proxy::DisableHTTPSProxy()
extern void Proxy_DisableHTTPSProxy_m5F81A74F573062DEFD61D25CF93D8F4303DC31B9 (void);
// 0x00000189 System.Boolean Crosstales.OnlineCheck.Tool.Proxy::validPort(System.Int32)
extern void Proxy_validPort_m99046FE9EA42599A844CA249CE80AB8CF808DFDF (void);
// 0x0000018A System.Void Crosstales.OnlineCheck.Tool.Proxy::.ctor()
extern void Proxy__ctor_m591F1FA4D30CD58CC84BDCBA08EF611BF64A0A1F (void);
// 0x0000018B System.Void Crosstales.OnlineCheck.Tool.SpeedTestCompleteEvent::.ctor()
extern void SpeedTestCompleteEvent__ctor_mF9FF434408C2E17209E9427C8EC11209E8A6C0F5 (void);
// 0x0000018C System.Void Crosstales.OnlineCheck.Tool.SpeedTest::add_OnTestCompleted(Crosstales.OnlineCheck.Tool.SpeedTest/TestCompleted)
extern void SpeedTest_add_OnTestCompleted_m8D2C4D2F430B89483F385992F5FC91DEDC91DD42 (void);
// 0x0000018D System.Void Crosstales.OnlineCheck.Tool.SpeedTest::remove_OnTestCompleted(Crosstales.OnlineCheck.Tool.SpeedTest/TestCompleted)
extern void SpeedTest_remove_OnTestCompleted_m72DE929DF607E54B47CDB4F40CD1CC3620929D4C (void);
// 0x0000018E Crosstales.OnlineCheck.Model.Enum.TestSize Crosstales.OnlineCheck.Tool.SpeedTest::get_DataSize()
extern void SpeedTest_get_DataSize_m641A449260EA1A889F5F76BDE102175F29729D71 (void);
// 0x0000018F System.Void Crosstales.OnlineCheck.Tool.SpeedTest::set_DataSize(Crosstales.OnlineCheck.Model.Enum.TestSize)
extern void SpeedTest_set_DataSize_mEDF3F7E4FF70F172606856D475465153D0D48DB8 (void);
// 0x00000190 System.String Crosstales.OnlineCheck.Tool.SpeedTest::get_SmallUrl()
extern void SpeedTest_get_SmallUrl_m7E8F8AC382C89DBE9EE08D6C046B228E3E828817 (void);
// 0x00000191 System.Void Crosstales.OnlineCheck.Tool.SpeedTest::set_SmallUrl(System.String)
extern void SpeedTest_set_SmallUrl_mD8CDD777356AFD68D436741AFB4AE41F8923DD62 (void);
// 0x00000192 System.String Crosstales.OnlineCheck.Tool.SpeedTest::get_MediumUrl()
extern void SpeedTest_get_MediumUrl_mD3FB142A5D53F4DBA126CC54A389B9BC9E7A55ED (void);
// 0x00000193 System.Void Crosstales.OnlineCheck.Tool.SpeedTest::set_MediumUrl(System.String)
extern void SpeedTest_set_MediumUrl_m3E2E3C11F8CF48B0498E4BA54A337B85E7839872 (void);
// 0x00000194 System.String Crosstales.OnlineCheck.Tool.SpeedTest::get_LargeUrl()
extern void SpeedTest_get_LargeUrl_m21F2F1610651AEFF32FD8957A55977309FEEB9B8 (void);
// 0x00000195 System.Void Crosstales.OnlineCheck.Tool.SpeedTest::set_LargeUrl(System.String)
extern void SpeedTest_set_LargeUrl_m8716C958CA623EFA62E05CCFA1334B3DE6408B51 (void);
// 0x00000196 System.Boolean Crosstales.OnlineCheck.Tool.SpeedTest::get_RunOnStart()
extern void SpeedTest_get_RunOnStart_m71A65D3F4DF2A2424A915EF6A3B3F7552C103D38 (void);
// 0x00000197 System.Void Crosstales.OnlineCheck.Tool.SpeedTest::set_RunOnStart(System.Boolean)
extern void SpeedTest_set_RunOnStart_m8ED7FCC156D8D2CAAF2484B8A94E589378453755 (void);
// 0x00000198 System.String Crosstales.OnlineCheck.Tool.SpeedTest::get_LastURL()
extern void SpeedTest_get_LastURL_m0468950BED96D6ABD0C83672B44BB85D91C161F5 (void);
// 0x00000199 System.Void Crosstales.OnlineCheck.Tool.SpeedTest::set_LastURL(System.String)
extern void SpeedTest_set_LastURL_m9D96892C9D99B85CBE85E9A3F9E2AFD4BA271FCA (void);
// 0x0000019A System.Int64 Crosstales.OnlineCheck.Tool.SpeedTest::get_LastDataSize()
extern void SpeedTest_get_LastDataSize_m4AD20E2A05D4B7C9BBDC59739B849C3EF18EFC77 (void);
// 0x0000019B System.Void Crosstales.OnlineCheck.Tool.SpeedTest::set_LastDataSize(System.Int64)
extern void SpeedTest_set_LastDataSize_mEA3AD53374F1654942AE07A509737B2C80FB22FD (void);
// 0x0000019C System.Double Crosstales.OnlineCheck.Tool.SpeedTest::get_LastDataSizeMB()
extern void SpeedTest_get_LastDataSizeMB_m8F662E4A1D86758351B6A05DCBD41DF202C28C49 (void);
// 0x0000019D System.Double Crosstales.OnlineCheck.Tool.SpeedTest::get_LastDuration()
extern void SpeedTest_get_LastDuration_m214130E3CFA7560BC58B191ADD7A3A0572C18432 (void);
// 0x0000019E System.Void Crosstales.OnlineCheck.Tool.SpeedTest::set_LastDuration(System.Double)
extern void SpeedTest_set_LastDuration_m25A4AFB4524531A862ACA303BB9962659910C616 (void);
// 0x0000019F System.Double Crosstales.OnlineCheck.Tool.SpeedTest::get_LastSpeed()
extern void SpeedTest_get_LastSpeed_m3AA25CA1067DC3498E40BC8A4E512C2CEA2A3D62 (void);
// 0x000001A0 System.Void Crosstales.OnlineCheck.Tool.SpeedTest::set_LastSpeed(System.Double)
extern void SpeedTest_set_LastSpeed_m9BEBAC7801B8E92DF84A90C9E668AE4C71613AF1 (void);
// 0x000001A1 System.Double Crosstales.OnlineCheck.Tool.SpeedTest::get_LastSpeedMBps()
extern void SpeedTest_get_LastSpeedMBps_m354E20C7A15402363DCF215C1F9A8112D0E45032 (void);
// 0x000001A2 System.Boolean Crosstales.OnlineCheck.Tool.SpeedTest::get_isBusy()
extern void SpeedTest_get_isBusy_m4D7224F998875FB10633C5CE24DEBAC9781A0F5A (void);
// 0x000001A3 System.Void Crosstales.OnlineCheck.Tool.SpeedTest::set_isBusy(System.Boolean)
extern void SpeedTest_set_isBusy_m1412307C9B8BC3DC728F9A563B71F7F70AF79D31 (void);
// 0x000001A4 System.Boolean Crosstales.OnlineCheck.Tool.SpeedTest::get_isPlatformSupported()
extern void SpeedTest_get_isPlatformSupported_m6C6C49895799E31C2C402E2583746FFBC86A5EB6 (void);
// 0x000001A5 System.Void Crosstales.OnlineCheck.Tool.SpeedTest::OnApplicationQuit()
extern void SpeedTest_OnApplicationQuit_m9029088EFA403E6121B498C876A33E161C060B29 (void);
// 0x000001A6 System.Void Crosstales.OnlineCheck.Tool.SpeedTest::Start()
extern void SpeedTest_Start_m9CD54725AA022B481F7F0B63ADDD2456194F67ED (void);
// 0x000001A7 System.Void Crosstales.OnlineCheck.Tool.SpeedTest::Test()
extern void SpeedTest_Test_mCDE783A81E3222906ABCA198A1E0F78475458452 (void);
// 0x000001A8 System.Void Crosstales.OnlineCheck.Tool.SpeedTest::Test(Crosstales.OnlineCheck.Model.Enum.TestSize)
extern void SpeedTest_Test_m96D170092942B2AE946B9C34D9CC597C32267922 (void);
// 0x000001A9 System.Void Crosstales.OnlineCheck.Tool.SpeedTest::Test(System.String)
extern void SpeedTest_Test_m3ECA30B9153F2DEEEE17D0B58C90AE9C56742D35 (void);
// 0x000001AA System.Collections.IEnumerator Crosstales.OnlineCheck.Tool.SpeedTest::test(Crosstales.OnlineCheck.Model.Enum.TestSize,System.String)
extern void SpeedTest_test_m565BB9ED0A7C216AB10F7606F7F1E825791E3EBA (void);
// 0x000001AB System.Void Crosstales.OnlineCheck.Tool.SpeedTest::speedTest(Crosstales.OnlineCheck.Model.Enum.TestSize,System.String)
extern void SpeedTest_speedTest_mBA814FEB9576E59A54A0CFB6BA8E62506F60AC29 (void);
// 0x000001AC System.Void Crosstales.OnlineCheck.Tool.SpeedTest::onTestCompleted(System.String,System.Int64,System.Double,System.Double)
extern void SpeedTest_onTestCompleted_mCA217F6D80E529F0975782045727FB6E5E1BE8D0 (void);
// 0x000001AD System.Void Crosstales.OnlineCheck.Tool.SpeedTest::.ctor()
extern void SpeedTest__ctor_m7815AC94B50B0DB322F813330755EAC59A007AB9 (void);
// 0x000001AE System.Void Crosstales.OnlineCheck.Tool.SpeedTest/TestCompleted::.ctor(System.Object,System.IntPtr)
extern void TestCompleted__ctor_m7C5C41A958545480A41B18FD303B35387FA58F25 (void);
// 0x000001AF System.Void Crosstales.OnlineCheck.Tool.SpeedTest/TestCompleted::Invoke(System.String,System.Int64,System.Double,System.Double)
extern void TestCompleted_Invoke_m3C5389932F3E6D3DE2DD543AFF716FC85C51DC27 (void);
// 0x000001B0 System.IAsyncResult Crosstales.OnlineCheck.Tool.SpeedTest/TestCompleted::BeginInvoke(System.String,System.Int64,System.Double,System.Double,System.AsyncCallback,System.Object)
extern void TestCompleted_BeginInvoke_mAA5FA645EA00946534A0B4919BBF187BE0E42DC0 (void);
// 0x000001B1 System.Void Crosstales.OnlineCheck.Tool.SpeedTest/TestCompleted::EndInvoke(System.IAsyncResult)
extern void TestCompleted_EndInvoke_m6D2AFFB0B5F50ED41FB6D170EE06F82FA2E0E72D (void);
// 0x000001B2 System.Void Crosstales.OnlineCheck.Tool.SpeedTest/<>c__DisplayClass57_0::.ctor()
extern void U3CU3Ec__DisplayClass57_0__ctor_m1E48D89BC59EF3401CBA27A45B925062E86AA09E (void);
// 0x000001B3 System.Void Crosstales.OnlineCheck.Tool.SpeedTest/<>c__DisplayClass57_0::<test>b__0()
extern void U3CU3Ec__DisplayClass57_0_U3CtestU3Eb__0_m57E382A27E68E23EE57DAB2C5AE9E359DE39E6A8 (void);
// 0x000001B4 System.Void Crosstales.OnlineCheck.Tool.SpeedTest/<test>d__57::.ctor(System.Int32)
extern void U3CtestU3Ed__57__ctor_mA55DD118C29DA2907F4E37B2D786A5C8820E2DC9 (void);
// 0x000001B5 System.Void Crosstales.OnlineCheck.Tool.SpeedTest/<test>d__57::System.IDisposable.Dispose()
extern void U3CtestU3Ed__57_System_IDisposable_Dispose_m5E2A2F8DD8A744C30E33FD237B0022C719BF7583 (void);
// 0x000001B6 System.Boolean Crosstales.OnlineCheck.Tool.SpeedTest/<test>d__57::MoveNext()
extern void U3CtestU3Ed__57_MoveNext_m3B22C01079302C5F8A0CFFF4328C7D244712E521 (void);
// 0x000001B7 System.Object Crosstales.OnlineCheck.Tool.SpeedTest/<test>d__57::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtestU3Ed__57_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1B97A242FEAA29891CBB0A292158DDB9A2E2698 (void);
// 0x000001B8 System.Void Crosstales.OnlineCheck.Tool.SpeedTest/<test>d__57::System.Collections.IEnumerator.Reset()
extern void U3CtestU3Ed__57_System_Collections_IEnumerator_Reset_mA927C7FB7D10E03D0383229029126C7DCA117818 (void);
// 0x000001B9 System.Object Crosstales.OnlineCheck.Tool.SpeedTest/<test>d__57::System.Collections.IEnumerator.get_Current()
extern void U3CtestU3Ed__57_System_Collections_IEnumerator_get_Current_m1B867BF9B0DC7EC5A88A3BCBE2D714E4FAC07097 (void);
// 0x000001BA Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Settings Crosstales.OnlineCheck.Tool.SpeedTestNET.ISpeedTestClient::GetSettings()
// 0x000001BB System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.ISpeedTestClient::TestServerLatency(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server,System.Int32)
// 0x000001BC System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.ISpeedTestClient::TestDownloadSpeed(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server,System.Int32,System.Int32)
// 0x000001BD System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.ISpeedTestClient::TestUploadSpeed(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server,System.Int32,System.Int32)
// 0x000001BE System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SetupProject::.cctor()
extern void SetupProject__cctor_m3C7003EB6357B6B19EFD155D16E3C903BE9F60F9 (void);
// 0x000001BF System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SetupProject::setup()
extern void SetupProject_setup_m104E53DC5254FAF89F40F6E1FC378B81E5D5C848 (void);
// 0x000001C0 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SetupProject::.ctor()
extern void SetupProject__ctor_m60A9E59152BA40BAEAAC056406362E16E3D5828E (void);
// 0x000001C1 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Settings Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient::GetSettings()
extern void SpeedTestClient_GetSettings_m369BC18051BF8B94C9E340DE2182B9B7A799159E (void);
// 0x000001C2 System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient::TestServerLatency(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server,System.Int32)
extern void SpeedTestClient_TestServerLatency_mA48F3A012BC767C248F1052F9CD0D380676E8B7F (void);
// 0x000001C3 System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient::TestDownloadSpeed(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server,System.Int32,System.Int32)
extern void SpeedTestClient_TestDownloadSpeed_mDDB6B860DF08F516DD612D92C05A475E09DAB082 (void);
// 0x000001C4 System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient::TestUploadSpeed(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server,System.Int32,System.Int32)
extern void SpeedTestClient_TestUploadSpeed_m89C49E0EA246059ECED9C5C536B1FD1CD1DBB4A0 (void);
// 0x000001C5 System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient::TestSpeed(System.Collections.Generic.IEnumerable`1<T>,System.Func`3<System.Net.Http.HttpClient,T,System.Threading.Tasks.Task`1<System.Int32>>,System.Int32)
// 0x000001C6 System.Collections.Generic.IEnumerable`1<System.String> Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient::GenerateUploadData(System.Int32)
extern void SpeedTestClient_GenerateUploadData_m62716BFCE2C5DC7C9552E926A86115EA5DD9A3A3 (void);
// 0x000001C7 System.String Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient::CreateTestUrl(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server,System.String)
extern void SpeedTestClient_CreateTestUrl_mB50804B7A9CD1BCC8E1751E17D706A9F026FEC01 (void);
// 0x000001C8 System.Collections.Generic.IEnumerable`1<System.String> Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient::GenerateDownloadUrls(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server,System.Int32)
extern void SpeedTestClient_GenerateDownloadUrls_m2161B46C3DEFFBAE2A015999637F68FF37AEAE7E (void);
// 0x000001C9 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient::.ctor()
extern void SpeedTestClient__ctor_m3E2BEA1154CC0F5E734C5A4884A66CBE66373B7E (void);
// 0x000001CA System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient::.cctor()
extern void SpeedTestClient__cctor_mE4914823411322EE37DE1738967736CF7C42DB1D (void);
// 0x000001CB System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m67B4135F083659E51AFBF7579EB5C287663782D2 (void);
// 0x000001CC System.Boolean Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c__DisplayClass5_0::<GetSettings>b__0(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server)
extern void U3CU3Ec__DisplayClass5_0_U3CGetSettingsU3Eb__0_mB88AC654ADAE1A743C7736B1B81C9C210C43FEAD (void);
// 0x000001CD System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c::.cctor()
extern void U3CU3Ec__cctor_mBB3B2BA7DB77BD16B1068F5192A2D05E0B6C4CA8 (void);
// 0x000001CE System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c::.ctor()
extern void U3CU3Ec__ctor_m484135B426AD21031336E885770C7BC6E828D561 (void);
// 0x000001CF System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c::<GetSettings>b__5_1(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server)
extern void U3CU3Ec_U3CGetSettingsU3Eb__5_1_mCE139BCE89006AE9F8BA82AEA93C25DB877D1A97 (void);
// 0x000001D0 System.Threading.Tasks.Task`1<System.Int32> Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c::<TestDownloadSpeed>b__7_0(System.Net.Http.HttpClient,System.String)
extern void U3CU3Ec_U3CTestDownloadSpeedU3Eb__7_0_mAA4F34A5FA1AC21845FB0F82E1D1205BDF60FBB1 (void);
// 0x000001D1 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c/<<TestDownloadSpeed>b__7_0>d::MoveNext()
extern void U3CU3CTestDownloadSpeedU3Eb__7_0U3Ed_MoveNext_m286443176C3DBAF50BE3AEC73FF694E869D3EFE9 (void);
// 0x000001D2 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c/<<TestDownloadSpeed>b__7_0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CU3CTestDownloadSpeedU3Eb__7_0U3Ed_SetStateMachine_m8280F2E096D7E15C71E84AFFA607CE6026649EF0 (void);
// 0x000001D3 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m84E131CB018FE90336E794DB0A0D25A84E43122E (void);
// 0x000001D4 System.Threading.Tasks.Task`1<System.Int32> Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c__DisplayClass8_0::<TestUploadSpeed>b__0(System.Net.Http.HttpClient,System.String)
extern void U3CU3Ec__DisplayClass8_0_U3CTestUploadSpeedU3Eb__0_mDAA8CA8010940F8CE9C4FB058D59C2DB1F261512 (void);
// 0x000001D5 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c__DisplayClass8_0/<<TestUploadSpeed>b__0>d::MoveNext()
extern void U3CU3CTestUploadSpeedU3Eb__0U3Ed_MoveNext_m519E493A0AE6E4A4C42731310694C3EAE8AA706C (void);
// 0x000001D6 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c__DisplayClass8_0/<<TestUploadSpeed>b__0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CU3CTestUploadSpeedU3Eb__0U3Ed_SetStateMachine_mE9DCC6B33A1AD21B4B16B4A8EA77136BC3FAE33F (void);
// 0x000001D7 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c__DisplayClass9_0`1::.ctor()
// 0x000001D8 System.Threading.Tasks.Task`1<System.Int32> Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c__DisplayClass9_0`1::<TestSpeed>b__0(T)
// 0x000001D9 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c__DisplayClass9_0`1/<<TestSpeed>b__0>d::MoveNext()
// 0x000001DA System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c__DisplayClass9_0`1/<<TestSpeed>b__0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000001DB System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c__9`1::.cctor()
// 0x000001DC System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c__9`1::.ctor()
// 0x000001DD System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<>c__9`1::<TestSpeed>b__9_1(System.Threading.Tasks.Task`1<System.Int32>)
// 0x000001DE System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<GenerateDownloadUrls>d__12::.ctor(System.Int32)
extern void U3CGenerateDownloadUrlsU3Ed__12__ctor_m49AA3916B0CC412A999C63471289DE67A8238189 (void);
// 0x000001DF System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<GenerateDownloadUrls>d__12::System.IDisposable.Dispose()
extern void U3CGenerateDownloadUrlsU3Ed__12_System_IDisposable_Dispose_mA02129B3149C89C32BC0A39E1E2D0E1027817D3C (void);
// 0x000001E0 System.Boolean Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<GenerateDownloadUrls>d__12::MoveNext()
extern void U3CGenerateDownloadUrlsU3Ed__12_MoveNext_mDA3A25A47FB2A2EC6AE3EC50B8DEE9F732044505 (void);
// 0x000001E1 System.String Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<GenerateDownloadUrls>d__12::System.Collections.Generic.IEnumerator<System.String>.get_Current()
extern void U3CGenerateDownloadUrlsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m164D472018312DAB261D382BD124DEA9619B8A21 (void);
// 0x000001E2 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<GenerateDownloadUrls>d__12::System.Collections.IEnumerator.Reset()
extern void U3CGenerateDownloadUrlsU3Ed__12_System_Collections_IEnumerator_Reset_m339DE9C5E3ABD68C27ED2A355B846F7CF0809439 (void);
// 0x000001E3 System.Object Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<GenerateDownloadUrls>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateDownloadUrlsU3Ed__12_System_Collections_IEnumerator_get_Current_m7BD616CEEFFE24B1D93B2873127B3F575201EC5F (void);
// 0x000001E4 System.Collections.Generic.IEnumerator`1<System.String> Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<GenerateDownloadUrls>d__12::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void U3CGenerateDownloadUrlsU3Ed__12_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m5945E3B01D1CAB268C7D1C10D4C980CDC2D9ACC2 (void);
// 0x000001E5 System.Collections.IEnumerator Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestClient/<GenerateDownloadUrls>d__12::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGenerateDownloadUrlsU3Ed__12_System_Collections_IEnumerable_GetEnumerator_mE0880AC1B187148E1F1EF59C10DC320FBA22E2A1 (void);
// 0x000001E6 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestHttpClient::.ctor()
extern void SpeedTestHttpClient__ctor_mCDB1FB90F02A41E1C6585CBC6D4C3ACA02244769 (void);
// 0x000001E7 System.Threading.Tasks.Task`1<T> Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestHttpClient::GetConfig(System.String)
// 0x000001E8 System.Uri Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestHttpClient::AddTimeStamp(System.Uri)
extern void SpeedTestHttpClient_AddTimeStamp_m13D5AB7CE6134C251468C409E2FD00CC5D12AB01 (void);
// 0x000001E9 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestHttpClient/<GetConfig>d__1`1::MoveNext()
// 0x000001EA System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestHttpClient/<GetConfig>d__1`1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000001EB System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNETCompleteEvent::.ctor()
extern void SpeedTestNETCompleteEvent__ctor_m85379887EC494DBC59B4B652AFCF56D5A559B889 (void);
// 0x000001EC System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::add_OnTestCompleted(Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET/TestCompleted)
extern void SpeedTestNET_add_OnTestCompleted_m8FDFDE70C23620C1E96C5E02391957C2A6D15E94 (void);
// 0x000001ED System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::remove_OnTestCompleted(Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET/TestCompleted)
extern void SpeedTestNET_remove_OnTestCompleted_m06533154A729B79608F982FE1EA7496E26EB2D75 (void);
// 0x000001EE System.Boolean Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::get_TestDownload()
extern void SpeedTestNET_get_TestDownload_mF30058D092504C697520DB4E274F683F8305A90C (void);
// 0x000001EF System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::set_TestDownload(System.Boolean)
extern void SpeedTestNET_set_TestDownload_m7E7C593DA039217B186148BC368D2E0D9FD8AE11 (void);
// 0x000001F0 System.Boolean Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::get_TestUpload()
extern void SpeedTestNET_get_TestUpload_m2206D6E6DBF39F07965DEA38900DFEC9E3109C4B (void);
// 0x000001F1 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::set_TestUpload(System.Boolean)
extern void SpeedTestNET_set_TestUpload_mCF3A1B346A2061F893552CDD3BB91608404C0A51 (void);
// 0x000001F2 System.Boolean Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::get_RunOnStart()
extern void SpeedTestNET_get_RunOnStart_mDB55C37FC8691F223A75F1282850CD0051DE7269 (void);
// 0x000001F3 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::set_RunOnStart(System.Boolean)
extern void SpeedTestNET_set_RunOnStart_m3F9B4EB24E843380C6D16A6D78B6F52D3D288D96 (void);
// 0x000001F4 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::get_LastServer()
extern void SpeedTestNET_get_LastServer_m4160DAEB0C71670367B0A377D484269F6FED2D9D (void);
// 0x000001F5 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::set_LastServer(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server)
extern void SpeedTestNET_set_LastServer_mCBE6FCFCD3555A245B7B2B15525BA94C408DAA4E (void);
// 0x000001F6 System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::get_LastDuration()
extern void SpeedTestNET_get_LastDuration_m66A52EA200CD945E4144147B034FFCDAF8D5209C (void);
// 0x000001F7 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::set_LastDuration(System.Double)
extern void SpeedTestNET_set_LastDuration_m1FE25D3FE39015C277211676D4EE0D9219699267 (void);
// 0x000001F8 System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::get_LastDownloadSpeed()
extern void SpeedTestNET_get_LastDownloadSpeed_m9B8E22094FAD2A38D52AA9474E4427F9444D7FC3 (void);
// 0x000001F9 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::set_LastDownloadSpeed(System.Double)
extern void SpeedTestNET_set_LastDownloadSpeed_m2DAA6161A0EA820A78C3F3412D03B627BCDF5307 (void);
// 0x000001FA System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::get_LastDownloadSpeedMBps()
extern void SpeedTestNET_get_LastDownloadSpeedMBps_m7099551A6E4C472D586C0B379A9FAED0C4C4646E (void);
// 0x000001FB System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::get_LastUploadSpeed()
extern void SpeedTestNET_get_LastUploadSpeed_m908040E942913DEE63BD79667029A41F142979A7 (void);
// 0x000001FC System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::set_LastUploadSpeed(System.Double)
extern void SpeedTestNET_set_LastUploadSpeed_m113E57EAC1E26360920F3C43D54153B7F3861C40 (void);
// 0x000001FD System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::get_LastUploadSpeedMBps()
extern void SpeedTestNET_get_LastUploadSpeedMBps_m2011E085A9066EE7CFE5A12A992848CAD9932175 (void);
// 0x000001FE System.Boolean Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::get_isBusy()
extern void SpeedTestNET_get_isBusy_m9B3DFCD781D218502F70A677C4DCED47045B6B6C (void);
// 0x000001FF System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::set_isBusy(System.Boolean)
extern void SpeedTestNET_set_isBusy_mF0A5FAA978A26916776D32EE3335A56B2F615A2E (void);
// 0x00000200 System.Boolean Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::get_isPlatformSupported()
extern void SpeedTestNET_get_isPlatformSupported_m3B226C33EDBF214225C484E477B8AAA8DAF2F8E6 (void);
// 0x00000201 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::OnApplicationQuit()
extern void SpeedTestNET_OnApplicationQuit_mC453D0B9326DF769A803A0A9AD710C4BAE89C9E2 (void);
// 0x00000202 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::Start()
extern void SpeedTestNET_Start_m70220C42F23E8CDD5395A8A32CC9067BAFD55727 (void);
// 0x00000203 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::Test()
extern void SpeedTestNET_Test_mA584D170D56791DD95986E3AAA17C0FC50414ACB (void);
// 0x00000204 System.Collections.IEnumerator Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::test()
extern void SpeedTestNET_test_m42A569CAF7BC7316E7E71F88EDFB21976E4E837C (void);
// 0x00000205 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::speedTest()
extern void SpeedTestNET_speedTest_mC6C3DCBE64A44003BA1320525B624E7F16B2D6D8 (void);
// 0x00000206 System.Collections.Generic.IEnumerable`1<Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server> Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::selectServers()
extern void SpeedTestNET_selectServers_mF1E09D9D2B7A9C89BDABCA6E0BD92436AA157700 (void);
// 0x00000207 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::onTestCompleted(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server,System.Double,System.Double,System.Double)
extern void SpeedTestNET_onTestCompleted_m774400D99DBC816313A4F39B6E5CC5E5000C2E0D (void);
// 0x00000208 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET::.ctor()
extern void SpeedTestNET__ctor_m98F9F01490F9B2840D27385A3F56E197756A2612 (void);
// 0x00000209 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET/TestCompleted::.ctor(System.Object,System.IntPtr)
extern void TestCompleted__ctor_mC9882303996B32C209E4F9C86767ECC9DE1FE7B3 (void);
// 0x0000020A System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET/TestCompleted::Invoke(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server,System.Double,System.Double,System.Double)
extern void TestCompleted_Invoke_m65BD1B19C3D4045FF2EAA3D44937CEBB73B777A7 (void);
// 0x0000020B System.IAsyncResult Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET/TestCompleted::BeginInvoke(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server,System.Double,System.Double,System.Double,System.AsyncCallback,System.Object)
extern void TestCompleted_BeginInvoke_m7BD0E4C9926E12D894927F4264A1213C86CFC0F3 (void);
// 0x0000020C System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET/TestCompleted::EndInvoke(System.IAsyncResult)
extern void TestCompleted_EndInvoke_mA1891114334673290098309578FA0498532921CE (void);
// 0x0000020D System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET/<test>d__49::.ctor(System.Int32)
extern void U3CtestU3Ed__49__ctor_m84BDA458D061319259587AAFA767FDA0512FEFEC (void);
// 0x0000020E System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET/<test>d__49::System.IDisposable.Dispose()
extern void U3CtestU3Ed__49_System_IDisposable_Dispose_m01FFF33E02E0F10C094A89BEE1BFF4462E4894DE (void);
// 0x0000020F System.Boolean Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET/<test>d__49::MoveNext()
extern void U3CtestU3Ed__49_MoveNext_m6AD7DC40D4AA145C5C3CBE4AE1E6062FE0FBFF9D (void);
// 0x00000210 System.Object Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET/<test>d__49::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtestU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5169CE1D6561EDC30A2EEBB1E1B9C3C0FBA4AF3A (void);
// 0x00000211 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET/<test>d__49::System.Collections.IEnumerator.Reset()
extern void U3CtestU3Ed__49_System_Collections_IEnumerator_Reset_m379FD9E7D91CF45ED29222C3EE208D03E4A50604 (void);
// 0x00000212 System.Object Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET/<test>d__49::System.Collections.IEnumerator.get_Current()
extern void U3CtestU3Ed__49_System_Collections_IEnumerator_get_Current_mDE60D7E8FFDAA8163CF42DB919A8DF30A5B5669B (void);
// 0x00000213 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET/<>c::.cctor()
extern void U3CU3Ec__cctor_mD2C1CEB5BFD8B887701A72F2F6EC0FD463D5ADC2 (void);
// 0x00000214 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET/<>c::.ctor()
extern void U3CU3Ec__ctor_mBDFDDF7E4DEC488FE41C71839D366D0C431F944F (void);
// 0x00000215 System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.SpeedTestNET/<>c::<speedTest>b__50_0(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server)
extern void U3CU3Ec_U3CspeedTestU3Eb__50_0_m1C22FA8F8FE7D6DC8AC033C8C07F863DB40A1695 (void);
// 0x00000216 System.String Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::get_Ip()
extern void Client_get_Ip_m0B9445853B5D4199CE94F7F5BD0241C1251B77B2 (void);
// 0x00000217 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::set_Ip(System.String)
extern void Client_set_Ip_mF873302D8A710CBDB05FC4388D3AD3D9A09D0A76 (void);
// 0x00000218 System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::get_Latitude()
extern void Client_get_Latitude_mADF069565C2C60552BD35CE0AC10019B6BD1AF2D (void);
// 0x00000219 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::set_Latitude(System.Double)
extern void Client_set_Latitude_mD8FAF274E3638BCFECAD9340117F4E240EBCCCBF (void);
// 0x0000021A System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::get_Longitude()
extern void Client_get_Longitude_mE4C0B3E531C0B17AFEF3C737E378852FE1AA03D2 (void);
// 0x0000021B System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::set_Longitude(System.Double)
extern void Client_set_Longitude_m190D851D72EF6D9470D1EFCF6242758CFB074F4E (void);
// 0x0000021C System.String Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::get_Isp()
extern void Client_get_Isp_mA92D7910AA592D926174B851DA714BC44B60631D (void);
// 0x0000021D System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::set_Isp(System.String)
extern void Client_set_Isp_m649133295A21EC49717640BC84170602CDB3C69A (void);
// 0x0000021E System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::get_IspRating()
extern void Client_get_IspRating_m0188AD85C4C73A2719515B1603FF3DBDACF10B09 (void);
// 0x0000021F System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::set_IspRating(System.Double)
extern void Client_set_IspRating_m2B96661EB68F8B4049926304C28B07A00F561BE4 (void);
// 0x00000220 System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::get_Rating()
extern void Client_get_Rating_m88C44AF1AEAD628F6D49D59DD02141E944BD630F (void);
// 0x00000221 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::set_Rating(System.Double)
extern void Client_set_Rating_m49097F8083162E99A5B396F1441C392F54911DA3 (void);
// 0x00000222 System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::get_IspAvarageDownloadSpeed()
extern void Client_get_IspAvarageDownloadSpeed_m32ED04BF66402C1B0BDA39775E56BEAFA4194E8B (void);
// 0x00000223 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::set_IspAvarageDownloadSpeed(System.Int32)
extern void Client_set_IspAvarageDownloadSpeed_m5AD00BF1E8F2691D70DF57173230F442794AD4E8 (void);
// 0x00000224 System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::get_IspAvarageUploadSpeed()
extern void Client_get_IspAvarageUploadSpeed_mC605999904C63FC1EDFC297DAF55E9E53B0E5236 (void);
// 0x00000225 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::set_IspAvarageUploadSpeed(System.Int32)
extern void Client_set_IspAvarageUploadSpeed_m109F22774BA6E37C4847E51DDCC5774B1FC38C5D (void);
// 0x00000226 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Coordinate Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::get_GeoCoordinate()
extern void Client_get_GeoCoordinate_m5A1B0752D985D5B1E1939056EB36CBD95CF632C2 (void);
// 0x00000227 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::.ctor()
extern void Client__ctor_mAB6496DCEFF582A0A67AFB137D23EBE4D332CECE (void);
// 0x00000228 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Coordinate Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client::<.ctor>b__35_0()
extern void Client_U3C_ctorU3Eb__35_0_m5116CF0671C518DC2DAABABAEBD16AB801DF3B87 (void);
// 0x00000229 System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Coordinate::get_Latitude()
extern void Coordinate_get_Latitude_m8D07D3A4B6CDE856EEE577B3E8F1C7A3B169CD65 (void);
// 0x0000022A System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Coordinate::set_Latitude(System.Double)
extern void Coordinate_set_Latitude_m1ACA8FA6F57F247E89D8F7CEF7EE20F2370C1705 (void);
// 0x0000022B System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Coordinate::get_Longitude()
extern void Coordinate_get_Longitude_mAB87DACDEAFB05CE4CE6CAD0C447F8D56C29E130 (void);
// 0x0000022C System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Coordinate::set_Longitude(System.Double)
extern void Coordinate_set_Longitude_m16230727D76BB3EAF9332F7F26D202AC2E9E5CF0 (void);
// 0x0000022D System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Coordinate::.ctor(System.Double,System.Double)
extern void Coordinate__ctor_m0886EF2747AD15E2DFF55A667954660184F4F780 (void);
// 0x0000022E System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Coordinate::GetDistanceTo(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Coordinate)
extern void Coordinate_GetDistanceTo_m102B52E0A341913C08DD14FD0F0EA7E55A1797B4 (void);
// 0x0000022F System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Download::get_TestLength()
extern void Download_get_TestLength_m289664666D0F5DECA0D41435591FE75CD7295E09 (void);
// 0x00000230 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Download::set_TestLength(System.Int32)
extern void Download_set_TestLength_mFD53362225A0BC710E1CC989047F7FF027214463 (void);
// 0x00000231 System.String Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Download::get_InitialTest()
extern void Download_get_InitialTest_m808C4F980B4198E5882F23BBD6DDB59C040FEE35 (void);
// 0x00000232 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Download::set_InitialTest(System.String)
extern void Download_set_InitialTest_m08BA6DA96AA31BA256EEF1530ED93E250A397E6D (void);
// 0x00000233 System.String Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Download::get_MinTestSize()
extern void Download_get_MinTestSize_m3DF159A73D260590CA6906F5E3A4D7BB90B819C4 (void);
// 0x00000234 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Download::set_MinTestSize(System.String)
extern void Download_set_MinTestSize_m641B5621AC012890DA21BC159DDDBFEDFC2A449F (void);
// 0x00000235 System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Download::get_ThreadsPerUrl()
extern void Download_get_ThreadsPerUrl_m4D34349486D1BDFB271D6BB1F9297F3EC6348A1E (void);
// 0x00000236 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Download::set_ThreadsPerUrl(System.Int32)
extern void Download_set_ThreadsPerUrl_m709C09E224ADF9252B9B55760E627C27A3F14941 (void);
// 0x00000237 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Download::.ctor()
extern void Download__ctor_mB11758016E4D4CD64566E0F54B3AD9BC49EEBB9F (void);
// 0x00000238 System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::get_Id()
extern void Server_get_Id_mA2999C7EC3409CEDE72BB7C8B5364C4EC352386E (void);
// 0x00000239 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::set_Id(System.Int32)
extern void Server_set_Id_m019851DDEF130546B9918509A8A181ECD9FEA4B4 (void);
// 0x0000023A System.String Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::get_Name()
extern void Server_get_Name_mD18CBC3EE46C7D0AD38FC614D8A46D2B9ABAA1EA (void);
// 0x0000023B System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::set_Name(System.String)
extern void Server_set_Name_m17AAFBAC7179265AFC6E0880EB6EDE0B05D41F99 (void);
// 0x0000023C System.String Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::get_Country()
extern void Server_get_Country_mCC14ED05AC9FBAE8811B173060C20C36642DBF26 (void);
// 0x0000023D System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::set_Country(System.String)
extern void Server_set_Country_m59CF2DCCBA4737CA0DC26F43B1D2B2900ECD5A18 (void);
// 0x0000023E System.String Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::get_Sponsor()
extern void Server_get_Sponsor_mCD97CBB658EC7C8B376D879B80898E08316AB7A0 (void);
// 0x0000023F System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::set_Sponsor(System.String)
extern void Server_set_Sponsor_mA8CEE4B9339434697F9A1366F44A0DBEB68BA970 (void);
// 0x00000240 System.String Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::get_Host()
extern void Server_get_Host_m71A2D38CB45DF2946191BED1962F14DBD0955E22 (void);
// 0x00000241 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::set_Host(System.String)
extern void Server_set_Host_m7303208597252E6E186421C523F744CBB3A7DEB9 (void);
// 0x00000242 System.String Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::get_Url()
extern void Server_get_Url_m660B03D65EC132A3EB811035498667CD9A4CABA8 (void);
// 0x00000243 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::set_Url(System.String)
extern void Server_set_Url_mD827DA9B0FE13649BC4EFFA65B8FDEF7ABA20F59 (void);
// 0x00000244 System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::get_Latitude()
extern void Server_get_Latitude_m8B22A17C87780AA0F9744C2728D9622BE2887A46 (void);
// 0x00000245 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::set_Latitude(System.Double)
extern void Server_set_Latitude_m77AE62B1568B46095196B41F4B94B2EB588B052E (void);
// 0x00000246 System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::get_Longitude()
extern void Server_get_Longitude_m097F70F6D4388E7715674D4152CAEEBD74777701 (void);
// 0x00000247 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::set_Longitude(System.Double)
extern void Server_set_Longitude_m22F4D3DE5E0D31B562D4DDE6BEA2302C97CAE77D (void);
// 0x00000248 System.Double Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::get_Distance()
extern void Server_get_Distance_m086243B6C27057388CDBF0A5C2855FFDF0601908 (void);
// 0x00000249 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::set_Distance(System.Double)
extern void Server_set_Distance_m2994F70C5A3B5667D63E0E26514F9B0D3CB0D6E5 (void);
// 0x0000024A System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::get_Latency()
extern void Server_get_Latency_mA4CAB54DA41B8EBA4584559614C72BC412A7AD6C (void);
// 0x0000024B System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::set_Latency(System.Int32)
extern void Server_set_Latency_mE1C4F2D76A7A9D7DEE6907001C029275959D1D37 (void);
// 0x0000024C Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Coordinate Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::get_GeoCoordinate()
extern void Server_get_GeoCoordinate_m44A1A3C557D32C9DC9CEBCA8F73DE0D5A11CDF1F (void);
// 0x0000024D System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::.ctor()
extern void Server__ctor_m2531A1696D2F74D276057157FD1F2EA9FF1940C1 (void);
// 0x0000024E System.String Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::ToString()
extern void Server_ToString_m9B9B60E0EC1FAA3468462F1306D5930287E9DFDB (void);
// 0x0000024F Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Coordinate Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server::<.ctor>b__43_0()
extern void Server_U3C_ctorU3Eb__43_0_m11A8527A9A2A89C54099A241207BAF066ACEB075 (void);
// 0x00000250 System.String Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.ServerConfig::get_IgnoreIds()
extern void ServerConfig_get_IgnoreIds_m8AD90272174C1B00F09FAB914D423BAC48B03B70 (void);
// 0x00000251 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.ServerConfig::set_IgnoreIds(System.String)
extern void ServerConfig_set_IgnoreIds_m74CA68265E0D0F82345713DA8A75005ABFE6698D (void);
// 0x00000252 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.ServerConfig::.ctor()
extern void ServerConfig__ctor_m159B08799616725568118075C72FE852458E1DA7 (void);
// 0x00000253 System.Collections.Generic.List`1<Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server> Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.ServersList::get_Servers()
extern void ServersList_get_Servers_mA728E4CD054E33150481599D265769246D2BE398 (void);
// 0x00000254 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.ServersList::set_Servers(System.Collections.Generic.List`1<Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server>)
extern void ServersList_set_Servers_m90E26C0707F47E96012430ADC9A5940AFE0E101C (void);
// 0x00000255 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.ServersList::.ctor()
extern void ServersList__ctor_m0151113C77A48B7E78E80F20E46EAB0C7FCAB48B (void);
// 0x00000256 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.ServersList::CalculateDistances(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Coordinate)
extern void ServersList_CalculateDistances_m6AFAF7597CA9D3E4073450B8A27B5BE4C6011D37 (void);
// 0x00000257 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Settings::get_Client()
extern void Settings_get_Client_mA491884009E1D77B12B2BB69EF9403E85BC5075A (void);
// 0x00000258 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Settings::set_Client(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Client)
extern void Settings_set_Client_mF3BFDC0946EB9C6C7C7718C07AFD36A364604A05 (void);
// 0x00000259 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Times Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Settings::get_Times()
extern void Settings_get_Times_m8C7E3E2A7EC879BFFF31552B55F9365771DFDB53 (void);
// 0x0000025A System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Settings::set_Times(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Times)
extern void Settings_set_Times_m76A004A32676C30B0A6FE86686CEE58D8444504A (void);
// 0x0000025B Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Download Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Settings::get_Download()
extern void Settings_get_Download_m946D691794AB8B057BC1A7BA72CD48421DB76CF5 (void);
// 0x0000025C System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Settings::set_Download(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Download)
extern void Settings_set_Download_mC4B696297672244EC829C93A761FA1466DA34A51 (void);
// 0x0000025D Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Settings::get_Upload()
extern void Settings_get_Upload_mF3ADADA4BD5F74A9FF1E4B9C76FF6084DCE9FBFD (void);
// 0x0000025E System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Settings::set_Upload(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload)
extern void Settings_set_Upload_mA7FEFA826C900B4131D2E90C4DB3CF79C7907670 (void);
// 0x0000025F Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.ServerConfig Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Settings::get_ServerConfig()
extern void Settings_get_ServerConfig_mCC41E378E34CDF96D220C26580252358502250C5 (void);
// 0x00000260 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Settings::set_ServerConfig(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.ServerConfig)
extern void Settings_set_ServerConfig_mD35731AAA2D23F0164EEFB2CF28B21E633C20871 (void);
// 0x00000261 System.Collections.Generic.List`1<Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server> Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Settings::get_Servers()
extern void Settings_get_Servers_m32663154FB14571FAA25BE902138A04DEDE73225 (void);
// 0x00000262 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Settings::set_Servers(System.Collections.Generic.List`1<Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server>)
extern void Settings_set_Servers_m409CAB6BF32ACF58F53120805684366D042A75C7 (void);
// 0x00000263 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Settings::.ctor()
extern void Settings__ctor_mEC68800AD1332A4417728B9A98015408AD617F4A (void);
// 0x00000264 System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Times::get_Download1()
extern void Times_get_Download1_mBD1281097BE6FBA5F56F81A9FDC0890CFF072A83 (void);
// 0x00000265 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Times::set_Download1(System.Int32)
extern void Times_set_Download1_m9FB2ACE46B9DB28FCBDE6943A0356B5F4DF60320 (void);
// 0x00000266 System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Times::get_Download2()
extern void Times_get_Download2_mC71E858A945784AC0B3AC13C3D626783AD9F75C8 (void);
// 0x00000267 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Times::set_Download2(System.Int32)
extern void Times_set_Download2_m2E8D0CE21DF400DDFCF33298CB408CF786A988AA (void);
// 0x00000268 System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Times::get_Download3()
extern void Times_get_Download3_mE0F35479045CA5961E39FD77D5BC17BB4E399863 (void);
// 0x00000269 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Times::set_Download3(System.Int32)
extern void Times_set_Download3_m43AEC1ED6C046DE2FDC8D266D5AAF908916EEE23 (void);
// 0x0000026A System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Times::get_Upload1()
extern void Times_get_Upload1_m23B4FD1C9029FFA70E84306928F68A33759A9ED3 (void);
// 0x0000026B System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Times::set_Upload1(System.Int32)
extern void Times_set_Upload1_mADE25B3FCC8FEF183C95BBC0C956BE32EB2ACA9C (void);
// 0x0000026C System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Times::get_Upload2()
extern void Times_get_Upload2_mCEA15219828984DDE6749ACC494CCCA07DC6EFB3 (void);
// 0x0000026D System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Times::set_Upload2(System.Int32)
extern void Times_set_Upload2_mDF56B9576186F2BF6893072BC281F25788E409ED (void);
// 0x0000026E System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Times::get_Upload3()
extern void Times_get_Upload3_m33891A4C1DB3C988A09EFB43EABFC38DE4390924 (void);
// 0x0000026F System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Times::set_Upload3(System.Int32)
extern void Times_set_Upload3_m57A585D4E9D371109D318361B5949E82E1B9121C (void);
// 0x00000270 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Times::.ctor()
extern void Times__ctor_m94E40C91965571BC4C4C2514B73023A138FBF0DC (void);
// 0x00000271 System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::get_TestLength()
extern void Upload_get_TestLength_m01A66839C2D9D5C7DD1FDA014DCBFB80DBB23731 (void);
// 0x00000272 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::set_TestLength(System.Int32)
extern void Upload_set_TestLength_m0E77363058F1C6C9DD543BBE2820DA78013B0080 (void);
// 0x00000273 System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::get_Ratio()
extern void Upload_get_Ratio_m37A83FA6FE6D9A815FAAF8E08840C53EAE29DF44 (void);
// 0x00000274 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::set_Ratio(System.Int32)
extern void Upload_set_Ratio_m91BBBD058C120E6E919AB9843DCDD74087C3B20D (void);
// 0x00000275 System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::get_InitialTest()
extern void Upload_get_InitialTest_m5A44B984EB443D040BB5067823424FF774E1AE68 (void);
// 0x00000276 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::set_InitialTest(System.Int32)
extern void Upload_set_InitialTest_m6461F782AA38863779BD2ED35B0A01A4B38986F6 (void);
// 0x00000277 System.String Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::get_MinTestSize()
extern void Upload_get_MinTestSize_m50788DA9D8ECC84CC142274094C16D40E2C2C8CA (void);
// 0x00000278 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::set_MinTestSize(System.String)
extern void Upload_set_MinTestSize_m611A46C9FF55E0E2C7326DB31F57731B7A03A8EC (void);
// 0x00000279 System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::get_Threads()
extern void Upload_get_Threads_m9F2C1842B9753DDCC1BC351475292DACB8264696 (void);
// 0x0000027A System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::set_Threads(System.Int32)
extern void Upload_set_Threads_m80F21E7FED558565879437B55F70DF334C3A7F11 (void);
// 0x0000027B System.String Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::get_MaxChunkSize()
extern void Upload_get_MaxChunkSize_m19E1298ED0FD82CBFF4CE9EFC434F20AE6E2C8C6 (void);
// 0x0000027C System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::set_MaxChunkSize(System.String)
extern void Upload_set_MaxChunkSize_m47EDCE56D373ADCA4FCDB9B59DAD53EA9AFF0705 (void);
// 0x0000027D System.String Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::get_MaxChunkCount()
extern void Upload_get_MaxChunkCount_m4C6E81A8FA8812934006CFDF4A623FA3669FB11E (void);
// 0x0000027E System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::set_MaxChunkCount(System.String)
extern void Upload_set_MaxChunkCount_m275B9BF318D72FE0202F869C819020B7117D8A69 (void);
// 0x0000027F System.Int32 Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::get_ThreadsPerUrl()
extern void Upload_get_ThreadsPerUrl_m66AA71DF8D6469F1BE6FCE1D1976ED0C1894CC71 (void);
// 0x00000280 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::set_ThreadsPerUrl(System.Int32)
extern void Upload_set_ThreadsPerUrl_m775BDBCDA5697DC1D6E7A2C1282424FCEF597507 (void);
// 0x00000281 System.Void Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Upload::.ctor()
extern void Upload__ctor_mD9FF71211EA19B10EA178CB46C33CEC63C5197EE (void);
// 0x00000282 System.Void Crosstales.OnlineCheck.Demo.EventTester::OnStatusChange(System.Boolean)
extern void EventTester_OnStatusChange_m92A2DE0F6CDF321F787CF96788E8E6F4FD65ED55 (void);
// 0x00000283 System.Void Crosstales.OnlineCheck.Demo.EventTester::OnPingComplete(System.Single)
extern void EventTester_OnPingComplete_mC534A30E8506911949406319A54BEE8AB25E7F8C (void);
// 0x00000284 System.Void Crosstales.OnlineCheck.Demo.EventTester::OnSpeedTestComplete(System.Double,System.Double)
extern void EventTester_OnSpeedTestComplete_mA75C7ACABBE341115F5CE60DC4C88E4A0C46BB0A (void);
// 0x00000285 System.Void Crosstales.OnlineCheck.Demo.EventTester::OnSpeedTestNETComplete(System.Double,System.Double,System.Double)
extern void EventTester_OnSpeedTestNETComplete_mE5991E09850A475C6C9D8EF45A8A0F3CA520C726 (void);
// 0x00000286 System.Void Crosstales.OnlineCheck.Demo.EventTester::.ctor()
extern void EventTester__ctor_m6B3B2AEA1402035CE85EE77BE76298E3B8263399 (void);
// 0x00000287 System.Void Crosstales.OnlineCheck.Demo.GUIMain::Start()
extern void GUIMain_Start_m19E15972FF096F019331693332755C389BFAF4B5 (void);
// 0x00000288 System.Void Crosstales.OnlineCheck.Demo.GUIMain::Update()
extern void GUIMain_Update_mEE0C78DAC0DFB1E6AD1EBF2662FB5711B1D96C42 (void);
// 0x00000289 System.Void Crosstales.OnlineCheck.Demo.GUIMain::OnDestroy()
extern void GUIMain_OnDestroy_m725BC03195678761AE8187783799CA81400A0637 (void);
// 0x0000028A System.Void Crosstales.OnlineCheck.Demo.GUIMain::Check()
extern void GUIMain_Check_mE38A767A8928F398D7A317FC3A4C3D5D6946215D (void);
// 0x0000028B System.Void Crosstales.OnlineCheck.Demo.GUIMain::ChangeIntervalMin()
extern void GUIMain_ChangeIntervalMin_m12AB6190F5E5178D59585A4E66B66BF9FFFE3650 (void);
// 0x0000028C System.Void Crosstales.OnlineCheck.Demo.GUIMain::ChangeIntervalMax()
extern void GUIMain_ChangeIntervalMax_mB3EB7AD5D0AA934BD07D70DD5054A59FBF12BC4C (void);
// 0x0000028D System.Void Crosstales.OnlineCheck.Demo.GUIMain::colorKnob(System.Boolean,UnityEngine.NetworkReachability)
extern void GUIMain_colorKnob_mF2A2E9E1EA43EA9F985CDAA6696A552BE20C411B (void);
// 0x0000028E System.Void Crosstales.OnlineCheck.Demo.GUIMain::.ctor()
extern void GUIMain__ctor_mE16B71FA337D24AC53E8B39EB21C292C1B00AAD8 (void);
// 0x0000028F System.Void Crosstales.OnlineCheck.Demo.GUIMain::<Start>b__21_0(System.Single)
extern void GUIMain_U3CStartU3Eb__21_0_m7297BA87941213DE42946C1BCB2FB56B5A8EDB2D (void);
// 0x00000290 System.Void Crosstales.OnlineCheck.Demo.GUIMain::<Start>b__21_1(System.Single)
extern void GUIMain_U3CStartU3Eb__21_1_mF4C6CF3E35C104AF588FF18D86FFC124FF4425B1 (void);
// 0x00000291 System.Void Crosstales.OnlineCheck.Demo.GUINetworkInfo::Start()
extern void GUINetworkInfo_Start_m417E4739EA1556B667D9274F76A156B3C9E2A2EB (void);
// 0x00000292 System.Void Crosstales.OnlineCheck.Demo.GUINetworkInfo::Refresh()
extern void GUINetworkInfo_Refresh_m2B0E176DAEC16BFD7C3679672C9EA2A81C13D4CF (void);
// 0x00000293 System.Void Crosstales.OnlineCheck.Demo.GUINetworkInfo::.ctor()
extern void GUINetworkInfo__ctor_m8308778EA563E8DAC1DF644D1EFB2B3C994C7E40 (void);
// 0x00000294 System.Void Crosstales.OnlineCheck.Demo.GUIPing::Start()
extern void GUIPing_Start_mE7DD2821034140C87ED14DA790CA50F487BC1B4C (void);
// 0x00000295 System.Void Crosstales.OnlineCheck.Demo.GUIPing::OnDestroy()
extern void GUIPing_OnDestroy_mC826DCC781B70AEAB335D9D7594510FA61254CA0 (void);
// 0x00000296 System.Void Crosstales.OnlineCheck.Demo.GUIPing::Ping()
extern void GUIPing_Ping_m8DE5CD76E790EB2E72BC2EE1904254393C58157F (void);
// 0x00000297 System.Void Crosstales.OnlineCheck.Demo.GUIPing::onPingCompleted(System.String,System.String,System.Single)
extern void GUIPing_onPingCompleted_m42A318934F78F0CB8C387D711F42D4BA9360095C (void);
// 0x00000298 System.Void Crosstales.OnlineCheck.Demo.GUIPing::.ctor()
extern void GUIPing__ctor_m87910288450F0CB621BD40CB8CFB2431DA9B286F (void);
// 0x00000299 System.Void Crosstales.OnlineCheck.Demo.GUIScenes::LoadPreviousScene()
extern void GUIScenes_LoadPreviousScene_m6EF52A8494DDA9FCE39BA5B8CF014E54ABFF952E (void);
// 0x0000029A System.Void Crosstales.OnlineCheck.Demo.GUIScenes::LoadNextScene()
extern void GUIScenes_LoadNextScene_mA365BABF7F7FC0852941A7C826C90A70DEBE8366 (void);
// 0x0000029B System.Void Crosstales.OnlineCheck.Demo.GUIScenes::OpenAssetURL()
extern void GUIScenes_OpenAssetURL_m805BD8ADDAED2F9A24A82EA8B93EDEEB8D99F994 (void);
// 0x0000029C System.Void Crosstales.OnlineCheck.Demo.GUIScenes::OpenCTURL()
extern void GUIScenes_OpenCTURL_m49C3A77364AC9F8841814367575EEB73B1770A0F (void);
// 0x0000029D System.Void Crosstales.OnlineCheck.Demo.GUIScenes::Quit()
extern void GUIScenes_Quit_mC5A7F8BE47789E7AFC64394B5C64E0D2F420093C (void);
// 0x0000029E System.Void Crosstales.OnlineCheck.Demo.GUIScenes::.ctor()
extern void GUIScenes__ctor_m635CC7956C21D8163F74030332A3A8CF6D85C9E3 (void);
// 0x0000029F System.Void Crosstales.OnlineCheck.Demo.GUISpeed::Start()
extern void GUISpeed_Start_mA5E332EE86234D0D1D3F4D0474277F4D9B9C0173 (void);
// 0x000002A0 System.Void Crosstales.OnlineCheck.Demo.GUISpeed::OnDestroy()
extern void GUISpeed_OnDestroy_m33711AD892C2233A59ED1E326DD591ADBB7A1638 (void);
// 0x000002A1 System.Void Crosstales.OnlineCheck.Demo.GUISpeed::Test()
extern void GUISpeed_Test_m521FB9AFF57B43835F5DA86BFE3AE48FEB6EB99D (void);
// 0x000002A2 System.Void Crosstales.OnlineCheck.Demo.GUISpeed::SetSize(System.Int32)
extern void GUISpeed_SetSize_mB9E9FC32A128143DA6202D16A101C6D4EBC1FBD4 (void);
// 0x000002A3 System.Void Crosstales.OnlineCheck.Demo.GUISpeed::onTestCompleted(System.String,System.Int64,System.Double,System.Double)
extern void GUISpeed_onTestCompleted_m650004CA3AF714868FD9F7618FE61841B1A8166D (void);
// 0x000002A4 System.Void Crosstales.OnlineCheck.Demo.GUISpeed::.ctor()
extern void GUISpeed__ctor_m953BB7C61D21D4DAFAEC5E31EEFE2820B03CEB78 (void);
// 0x000002A5 System.Void Crosstales.OnlineCheck.Demo.GUISpeedNET::Start()
extern void GUISpeedNET_Start_mFE74B8B414889441F6962182664FD7806A67C908 (void);
// 0x000002A6 System.Void Crosstales.OnlineCheck.Demo.GUISpeedNET::OnDestroy()
extern void GUISpeedNET_OnDestroy_mAF1B82B4A340209FF036D0E2DE5BED1A507ADCD6 (void);
// 0x000002A7 System.Void Crosstales.OnlineCheck.Demo.GUISpeedNET::Test()
extern void GUISpeedNET_Test_m50C6E38A314A8627A43A60540A6E9B323324CF01 (void);
// 0x000002A8 System.Void Crosstales.OnlineCheck.Demo.GUISpeedNET::onTestCompleted(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server,System.Double,System.Double,System.Double)
extern void GUISpeedNET_onTestCompleted_m110FA8E44F1CE37737F1F9885071D8A6B71DA73B (void);
// 0x000002A9 System.Void Crosstales.OnlineCheck.Demo.GUISpeedNET::.ctor()
extern void GUISpeedNET__ctor_m4361DB1207F1C63291C604F9FA44AF211BC91926 (void);
// 0x000002AA System.Void Crosstales.OnlineCheck.Demo.Util.ManageEndlessMode::Start()
extern void ManageEndlessMode_Start_m7D3B02F404CECCFDD05584006C2B87CFD3079CC3 (void);
// 0x000002AB System.Void Crosstales.OnlineCheck.Demo.Util.ManageEndlessMode::.ctor()
extern void ManageEndlessMode__ctor_mB7C28FEE5ECED92C96707FCFEEFD3E3C9130BD74 (void);
// 0x000002AC System.Void Crosstales.Internal.WebGLCopyAndPaste::.ctor()
extern void WebGLCopyAndPaste__ctor_m9ABA4FDD0719BD76811EC9D6731B127B4DB05D07 (void);
// 0x000002AD System.Void Crosstales.UI.Social::Facebook()
extern void Social_Facebook_mC2E09264868CD8006AD4D69B817D855488422A7C (void);
// 0x000002AE System.Void Crosstales.UI.Social::Twitter()
extern void Social_Twitter_m492C7F14348366FA7719E69749DBB30A76DA3B8A (void);
// 0x000002AF System.Void Crosstales.UI.Social::LinkedIn()
extern void Social_LinkedIn_m69D1E11EDE1C1748E8ED8463D54DD59513371D94 (void);
// 0x000002B0 System.Void Crosstales.UI.Social::Youtube()
extern void Social_Youtube_m5C4E721503773BADFE0544DD381114729E7F31A5 (void);
// 0x000002B1 System.Void Crosstales.UI.Social::Discord()
extern void Social_Discord_m5C364639DE37DCF942CCF9A29944B535E5E39EC1 (void);
// 0x000002B2 System.Void Crosstales.UI.Social::.ctor()
extern void Social__ctor_m5661EFE8FEE7EC2324FDAA88191E3B5C6F284F67 (void);
// 0x000002B3 System.Void Crosstales.UI.StaticManager::Quit()
extern void StaticManager_Quit_mAC39DA93EB2E5BA9D210BDE9C23A09A4D4266504 (void);
// 0x000002B4 System.Void Crosstales.UI.StaticManager::OpenCrosstales()
extern void StaticManager_OpenCrosstales_m5E5E7FC3F7DE7D9ED7330C6D4B60E5A686E9F5BF (void);
// 0x000002B5 System.Void Crosstales.UI.StaticManager::OpenAssetstore()
extern void StaticManager_OpenAssetstore_m949B8AD95A36C1364BEA2416A9E32D134340890F (void);
// 0x000002B6 System.Void Crosstales.UI.StaticManager::.ctor()
extern void StaticManager__ctor_m745654B0948B4DF9A2FD0FB65806A03164ABCC65 (void);
// 0x000002B7 System.Void Crosstales.UI.UIDrag::Start()
extern void UIDrag_Start_mDB1DCDDEB984EDD5DF799896F85B936BFB18D54D (void);
// 0x000002B8 System.Void Crosstales.UI.UIDrag::BeginDrag()
extern void UIDrag_BeginDrag_m583EA2730DACC92BC96F551DABD1FBF04EA5FF8B (void);
// 0x000002B9 System.Void Crosstales.UI.UIDrag::OnDrag()
extern void UIDrag_OnDrag_mD59F3EA41622E034504FFFB95F247C6CA398BF2A (void);
// 0x000002BA System.Void Crosstales.UI.UIDrag::.ctor()
extern void UIDrag__ctor_mF4C53D49822FE9B02461E092AE6F7AC45A011113 (void);
// 0x000002BB System.Void Crosstales.UI.UIFocus::Start()
extern void UIFocus_Start_m880CB1FB7349D96ED9EB3CF27BE27A16BD2E5F24 (void);
// 0x000002BC System.Void Crosstales.UI.UIFocus::Awake()
extern void UIFocus_Awake_mADE277AE5D1040D6968B5C9E59C422C3066A6A8C (void);
// 0x000002BD System.Void Crosstales.UI.UIFocus::OnPanelEnter()
extern void UIFocus_OnPanelEnter_m33CF9E28098755A82CE6764377DF302C10F71973 (void);
// 0x000002BE System.Void Crosstales.UI.UIFocus::.ctor()
extern void UIFocus__ctor_mDEF7E588D05EBA2BF8ECFB87875BCD5A614829A0 (void);
// 0x000002BF System.Void Crosstales.UI.UIHint::Start()
extern void UIHint_Start_m96229DC6152E6DC547F659396A38AB349084473D (void);
// 0x000002C0 System.Void Crosstales.UI.UIHint::FadeUp()
extern void UIHint_FadeUp_m9D94298919C4378BE30465D8A82923CBA316D3C1 (void);
// 0x000002C1 System.Void Crosstales.UI.UIHint::FadeDown()
extern void UIHint_FadeDown_m3C90D1A432527CE936339156444BD7D108D729C7 (void);
// 0x000002C2 System.Collections.IEnumerator Crosstales.UI.UIHint::lerpAlphaDown(System.Single,System.Single,System.Single,System.Single,UnityEngine.Component)
extern void UIHint_lerpAlphaDown_m6FEC2A6BF3503E0B3046CEB9F4AE8CB7FB05C46C (void);
// 0x000002C3 System.Collections.IEnumerator Crosstales.UI.UIHint::lerpAlphaUp(System.Single,System.Single,System.Single,System.Single,UnityEngine.Component)
extern void UIHint_lerpAlphaUp_mD0790C5B2C87FFFB07F78F0E098C08FFBFB70246 (void);
// 0x000002C4 System.Void Crosstales.UI.UIHint::.ctor()
extern void UIHint__ctor_mD54D6ED200DAA952DD7D97643AE270D572902BCD (void);
// 0x000002C5 System.Void Crosstales.UI.UIHint/<lerpAlphaDown>d__8::.ctor(System.Int32)
extern void U3ClerpAlphaDownU3Ed__8__ctor_m137125C8737CD01C1A17E318878AFB2B0D0CDB14 (void);
// 0x000002C6 System.Void Crosstales.UI.UIHint/<lerpAlphaDown>d__8::System.IDisposable.Dispose()
extern void U3ClerpAlphaDownU3Ed__8_System_IDisposable_Dispose_m82EA5F02C54838CDFAD7E3C549962D0DB1151FE1 (void);
// 0x000002C7 System.Boolean Crosstales.UI.UIHint/<lerpAlphaDown>d__8::MoveNext()
extern void U3ClerpAlphaDownU3Ed__8_MoveNext_m954DF2644E926149C6AC6AA1695D54705969FCAE (void);
// 0x000002C8 System.Object Crosstales.UI.UIHint/<lerpAlphaDown>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3ClerpAlphaDownU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB42DD7F0CDC10AADCD99116A14BF77D16197AE49 (void);
// 0x000002C9 System.Void Crosstales.UI.UIHint/<lerpAlphaDown>d__8::System.Collections.IEnumerator.Reset()
extern void U3ClerpAlphaDownU3Ed__8_System_Collections_IEnumerator_Reset_mB4243BFE9BA869109D22485266636F346DE3495F (void);
// 0x000002CA System.Object Crosstales.UI.UIHint/<lerpAlphaDown>d__8::System.Collections.IEnumerator.get_Current()
extern void U3ClerpAlphaDownU3Ed__8_System_Collections_IEnumerator_get_Current_m641305F2EB99B80E211B4E6E99E859C2B7AAE63B (void);
// 0x000002CB System.Void Crosstales.UI.UIHint/<lerpAlphaUp>d__9::.ctor(System.Int32)
extern void U3ClerpAlphaUpU3Ed__9__ctor_m41FA43BCB2D14D39F0DC1E1AFFCECB0D18E02419 (void);
// 0x000002CC System.Void Crosstales.UI.UIHint/<lerpAlphaUp>d__9::System.IDisposable.Dispose()
extern void U3ClerpAlphaUpU3Ed__9_System_IDisposable_Dispose_mD5454F31F93F6B5DCCB420AB1241DD2E2DEE331B (void);
// 0x000002CD System.Boolean Crosstales.UI.UIHint/<lerpAlphaUp>d__9::MoveNext()
extern void U3ClerpAlphaUpU3Ed__9_MoveNext_mB9C62184F82A3D1EF71E0A03CE960D9F020EA556 (void);
// 0x000002CE System.Object Crosstales.UI.UIHint/<lerpAlphaUp>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3ClerpAlphaUpU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07EF2B75C53B187A10E0ACC00AB20DF4CAB27CD7 (void);
// 0x000002CF System.Void Crosstales.UI.UIHint/<lerpAlphaUp>d__9::System.Collections.IEnumerator.Reset()
extern void U3ClerpAlphaUpU3Ed__9_System_Collections_IEnumerator_Reset_m1C29B48A4BFBB4956CB59DAF52A587455998D965 (void);
// 0x000002D0 System.Object Crosstales.UI.UIHint/<lerpAlphaUp>d__9::System.Collections.IEnumerator.get_Current()
extern void U3ClerpAlphaUpU3Ed__9_System_Collections_IEnumerator_get_Current_mA92CCF9323E6BC3E4B224B1CBF4098D02978B980 (void);
// 0x000002D1 System.Void Crosstales.UI.UIResize::Awake()
extern void UIResize_Awake_m84548F0C66BF42D9B3D85FDC887AF9F592314470 (void);
// 0x000002D2 System.Void Crosstales.UI.UIResize::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void UIResize_OnPointerDown_m1E7B045EF724B9EFEF997DE80322BBBC494E202C (void);
// 0x000002D3 System.Void Crosstales.UI.UIResize::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void UIResize_OnDrag_m8F2FC62C5CD268AA28FB7B2EED64DDCA2079BB9E (void);
// 0x000002D4 System.Void Crosstales.UI.UIResize::.ctor()
extern void UIResize__ctor_m4B0D088874E80F5BF9D3321949439328C8BA3744 (void);
// 0x000002D5 System.Void Crosstales.UI.UIWindowManager::Start()
extern void UIWindowManager_Start_mFE0D244F094A34B2D47F20D9DA45C19166630F1A (void);
// 0x000002D6 System.Void Crosstales.UI.UIWindowManager::ChangeState(UnityEngine.GameObject)
extern void UIWindowManager_ChangeState_m98045ADD1B1A1380FF64496BFC4756B5F1051C59 (void);
// 0x000002D7 System.Void Crosstales.UI.UIWindowManager::.ctor()
extern void UIWindowManager__ctor_m12928DA297937399EBC6FD9D27BEEF646E068ADE (void);
// 0x000002D8 System.Void Crosstales.UI.WindowManager::Start()
extern void WindowManager_Start_m1AFD4847D6D468A27278A2D72509D0D28362A7B5 (void);
// 0x000002D9 System.Void Crosstales.UI.WindowManager::Update()
extern void WindowManager_Update_m17970A83622480894EA2395FF8A4C5560CEB8115 (void);
// 0x000002DA System.Void Crosstales.UI.WindowManager::SwitchPanel()
extern void WindowManager_SwitchPanel_m6B16C8B9EA436D051DF3D864DD4C97B915F4DF55 (void);
// 0x000002DB System.Void Crosstales.UI.WindowManager::OpenPanel()
extern void WindowManager_OpenPanel_mCAACC7E648449EC4F47E3FDBC730E00B319A680B (void);
// 0x000002DC System.Void Crosstales.UI.WindowManager::ClosePanel()
extern void WindowManager_ClosePanel_m5C051458EB873C2A040A526707370BD675CACD21 (void);
// 0x000002DD System.Void Crosstales.UI.WindowManager::.ctor()
extern void WindowManager__ctor_m7A48D60A914B455DE9A65EF38F6718DD9E92B782 (void);
// 0x000002DE System.Void Crosstales.UI.Util.FPSDisplay::Update()
extern void FPSDisplay_Update_m3DA30CE583BE8D5C3CF67953C12F519B237E77B5 (void);
// 0x000002DF System.Void Crosstales.UI.Util.FPSDisplay::.ctor()
extern void FPSDisplay__ctor_mE69C95D9F1718F2320861BEDD4AE137497BE4942 (void);
// 0x000002E0 System.Void Crosstales.UI.Util.ScrollRectHandler::Start()
extern void ScrollRectHandler_Start_m0D04331DA1FFEA8B60FC0ED473CAD5E51613B8CB (void);
// 0x000002E1 System.Void Crosstales.UI.Util.ScrollRectHandler::.ctor()
extern void ScrollRectHandler__ctor_mC6321DE1AAC54297ED22ED0319EB34682DE8DF12 (void);
// 0x000002E2 System.Void Crosstales.UI.Audio.AudioFilterController::Start()
extern void AudioFilterController_Start_m023E8A3B5C26AD20DD95401CFAB4F1ADE22C5FDE (void);
// 0x000002E3 System.Void Crosstales.UI.Audio.AudioFilterController::Update()
extern void AudioFilterController_Update_m3BC6A60259FA43DBDE01104A8702CAFD6C0672B2 (void);
// 0x000002E4 System.Void Crosstales.UI.Audio.AudioFilterController::FindAllAudioFilters()
extern void AudioFilterController_FindAllAudioFilters_mE584B35EC62589392B26052788D929FAE0077E3E (void);
// 0x000002E5 System.Void Crosstales.UI.Audio.AudioFilterController::ResetAudioFilters()
extern void AudioFilterController_ResetAudioFilters_m8143029366B8B9F1D02DF851A16B140A0322D4D7 (void);
// 0x000002E6 System.Void Crosstales.UI.Audio.AudioFilterController::ReverbFilterDropdownChanged(System.Int32)
extern void AudioFilterController_ReverbFilterDropdownChanged_m652A3031CDC599A6ABBAFDB9191BCAD9D07CD705 (void);
// 0x000002E7 System.Void Crosstales.UI.Audio.AudioFilterController::ChorusFilterEnabled(System.Boolean)
extern void AudioFilterController_ChorusFilterEnabled_mA275519F7D4A16CD4FF99698A461680D3D76C28A (void);
// 0x000002E8 System.Void Crosstales.UI.Audio.AudioFilterController::EchoFilterEnabled(System.Boolean)
extern void AudioFilterController_EchoFilterEnabled_m2AFF8AC9034A6335B2DFA78AEF2C45932DC998BB (void);
// 0x000002E9 System.Void Crosstales.UI.Audio.AudioFilterController::DistortionFilterEnabled(System.Boolean)
extern void AudioFilterController_DistortionFilterEnabled_m350F8A3B6DC0B0DB58BF247F8B9DE590D765F212 (void);
// 0x000002EA System.Void Crosstales.UI.Audio.AudioFilterController::DistortionFilterChanged(System.Single)
extern void AudioFilterController_DistortionFilterChanged_m461E2E9B59017BA19A54DD364804871920DBFAFF (void);
// 0x000002EB System.Void Crosstales.UI.Audio.AudioFilterController::LowPassFilterEnabled(System.Boolean)
extern void AudioFilterController_LowPassFilterEnabled_mF4170BEA6788FE1C5AA52D23DD270CF28A865AE8 (void);
// 0x000002EC System.Void Crosstales.UI.Audio.AudioFilterController::LowPassFilterChanged(System.Single)
extern void AudioFilterController_LowPassFilterChanged_m468A4ACAFBDA9DBF84E05D1D1B52695EFB93ADB1 (void);
// 0x000002ED System.Void Crosstales.UI.Audio.AudioFilterController::HighPassFilterEnabled(System.Boolean)
extern void AudioFilterController_HighPassFilterEnabled_m8289B95060189959E53DF7BA5C5F912EFCF8673D (void);
// 0x000002EE System.Void Crosstales.UI.Audio.AudioFilterController::HighPassFilterChanged(System.Single)
extern void AudioFilterController_HighPassFilterChanged_m6999A3F68779CB25498CFC9C74FBC6E302C89C72 (void);
// 0x000002EF System.Void Crosstales.UI.Audio.AudioFilterController::.ctor()
extern void AudioFilterController__ctor_m0ADDFC812BAF558E74733A8982FB517F0AF88BA8 (void);
// 0x000002F0 System.Void Crosstales.UI.Audio.AudioSourceController::Update()
extern void AudioSourceController_Update_m151D413A0028DC4DCC04AED331760F39FD75FE01 (void);
// 0x000002F1 System.Void Crosstales.UI.Audio.AudioSourceController::FindAllAudioSources()
extern void AudioSourceController_FindAllAudioSources_m7274214CC9FDEDD31530D475D242DE09509C3B7D (void);
// 0x000002F2 System.Void Crosstales.UI.Audio.AudioSourceController::ResetAllAudioSources()
extern void AudioSourceController_ResetAllAudioSources_m8024212FF7FD895E3E65A9FF3F522D9C8C91349A (void);
// 0x000002F3 System.Void Crosstales.UI.Audio.AudioSourceController::MuteEnabled(System.Boolean)
extern void AudioSourceController_MuteEnabled_m5482448F4EEEF3E25D8D6A2A47CCE74921D04D9B (void);
// 0x000002F4 System.Void Crosstales.UI.Audio.AudioSourceController::LoopEnabled(System.Boolean)
extern void AudioSourceController_LoopEnabled_m5678F2214274A8AEBE5F5767C2B160749A9AB7D5 (void);
// 0x000002F5 System.Void Crosstales.UI.Audio.AudioSourceController::VolumeChanged(System.Single)
extern void AudioSourceController_VolumeChanged_m128B79F2ED89F6534425EFB70B9948BB9D621E6D (void);
// 0x000002F6 System.Void Crosstales.UI.Audio.AudioSourceController::PitchChanged(System.Single)
extern void AudioSourceController_PitchChanged_mEFDBBA929C79B9CDD4DE9404FBFF6C763F4E3459 (void);
// 0x000002F7 System.Void Crosstales.UI.Audio.AudioSourceController::StereoPanChanged(System.Single)
extern void AudioSourceController_StereoPanChanged_m05093FA99C5C1E3C34B84E8E4972BD10AAF86040 (void);
// 0x000002F8 System.Void Crosstales.UI.Audio.AudioSourceController::.ctor()
extern void AudioSourceController__ctor_m01BF58E50660DC7A1DC6252D21678449766DC54A (void);
// 0x000002F9 System.Void Crosstales.Common.Util.CTScreenshot::Update()
extern void CTScreenshot_Update_mFA432D568E042B0FD7043DA0546D1BAB1B73BAF3 (void);
// 0x000002FA System.Void Crosstales.Common.Util.CTScreenshot::Capture()
extern void CTScreenshot_Capture_mE9EC74BDDCDE0B1B523E52660D6E7F4AB4A5CF16 (void);
// 0x000002FB System.Void Crosstales.Common.Util.CTScreenshot::.ctor()
extern void CTScreenshot__ctor_m77D46CC657F1B4D7383A520431D43B66078BDAEF (void);
// 0x000002FC System.Void Crosstales.Common.Util.PlatformController::Awake()
extern void PlatformController_Awake_m546EB944C79655473CE0328872609FC3F0C39910 (void);
// 0x000002FD System.Void Crosstales.Common.Util.PlatformController::Start()
extern void PlatformController_Start_m45C32ED17D6746F9DC0C139B05EDC89CEFBF0C3C (void);
// 0x000002FE System.Void Crosstales.Common.Util.PlatformController::selectPlatform()
extern void PlatformController_selectPlatform_m44CD923C6E139A6DB86AC3D1BCAE13099EDBCC3B (void);
// 0x000002FF System.Void Crosstales.Common.Util.PlatformController::activateGameObjects()
extern void PlatformController_activateGameObjects_m625D36196A5A24CA594A9ACF9A7FEA9FAE45389C (void);
// 0x00000300 System.Void Crosstales.Common.Util.PlatformController::activateScripts()
extern void PlatformController_activateScripts_m0AA8240F5DEC792EC31FE78B4D642664ED5B076F (void);
// 0x00000301 System.Void Crosstales.Common.Util.PlatformController::.ctor()
extern void PlatformController__ctor_m246265302FC8CBCE52B8C4DB078CB7B5593B4FAE (void);
// 0x00000302 System.Void Crosstales.Common.Util.PlatformController/<>c::.cctor()
extern void U3CU3Ec__cctor_m8FB2EB90255FD11CD4C37EF7142156A9234A45D9 (void);
// 0x00000303 System.Void Crosstales.Common.Util.PlatformController/<>c::.ctor()
extern void U3CU3Ec__ctor_m1E5F842F54FA495A8CD30B47EA5279A11B78913C (void);
// 0x00000304 System.Boolean Crosstales.Common.Util.PlatformController/<>c::<activateGameObjects>b__8_0(UnityEngine.GameObject)
extern void U3CU3Ec_U3CactivateGameObjectsU3Eb__8_0_mD375A3DE0420A1CBBDE8B0695E56FD9F058F8AC8 (void);
// 0x00000305 System.Boolean Crosstales.Common.Util.PlatformController/<>c::<activateScripts>b__9_0(UnityEngine.MonoBehaviour)
extern void U3CU3Ec_U3CactivateScriptsU3Eb__9_0_m40E2FE8F4B779E265F42B10B6A5EA0288FE67621 (void);
// 0x00000306 System.Void Crosstales.Common.Util.RandomColor::Start()
extern void RandomColor_Start_m391CF84B08071FF3ACB0963EB28D2730BAD3F21C (void);
// 0x00000307 System.Void Crosstales.Common.Util.RandomColor::Update()
extern void RandomColor_Update_m80AF1317FD5A6E02EE266F7916C32167C811A267 (void);
// 0x00000308 System.Void Crosstales.Common.Util.RandomColor::.ctor()
extern void RandomColor__ctor_m9D12F994D18E58B4E6917402BF320A05B4C07D38 (void);
// 0x00000309 System.Void Crosstales.Common.Util.RandomColor::.cctor()
extern void RandomColor__cctor_m5AD1035B777C4BE87CF66D0D0512337CF39E1069 (void);
// 0x0000030A System.Void Crosstales.Common.Util.RandomRotator::Start()
extern void RandomRotator_Start_mD8CAF4274B1003904752C232D60FF6D740AE46D8 (void);
// 0x0000030B System.Void Crosstales.Common.Util.RandomRotator::Update()
extern void RandomRotator_Update_m2B257E670DEC91784B8CF373497F18B3B88FBEA5 (void);
// 0x0000030C System.Void Crosstales.Common.Util.RandomRotator::.ctor()
extern void RandomRotator__ctor_m5B8F4189F25BED4B6E89D524DA329BF1F641416A (void);
// 0x0000030D System.Void Crosstales.Common.Util.RandomScaler::Start()
extern void RandomScaler_Start_m846A6856675D0177F6879AF36DB91392267F17A0 (void);
// 0x0000030E System.Void Crosstales.Common.Util.RandomScaler::Update()
extern void RandomScaler_Update_m7DF95D0941A662AA475C77084EBA89A60C0908B0 (void);
// 0x0000030F System.Void Crosstales.Common.Util.RandomScaler::.ctor()
extern void RandomScaler__ctor_mF4467ADA2B0DB1F1B451D3568A5CAF22042121D7 (void);
// 0x00000310 System.String Crosstales.Common.Util.BaseConstants::get_PREFIX_FILE()
extern void BaseConstants_get_PREFIX_FILE_mF05531A4738FEE2B43B56F613C8543B56677A58C (void);
// 0x00000311 System.String Crosstales.Common.Util.BaseConstants::get_APPLICATION_PATH()
extern void BaseConstants_get_APPLICATION_PATH_mE87E6E9FF38895E8F8E1D78DB610DD417DDC85BB (void);
// 0x00000312 System.Void Crosstales.Common.Util.BaseConstants::.ctor()
extern void BaseConstants__ctor_m423B5C856F2C469942209CF30A993C3BD4A969E3 (void);
// 0x00000313 System.Void Crosstales.Common.Util.BaseConstants::.cctor()
extern void BaseConstants__cctor_m08244E973BC852F1CD2C3F222AA017980377C085 (void);
// 0x00000314 System.Boolean Crosstales.Common.Util.BaseHelper::get_isWindowsPlatform()
extern void BaseHelper_get_isWindowsPlatform_mA89734268B9F4512EED642DF4FD30718BF0698DB (void);
// 0x00000315 System.Boolean Crosstales.Common.Util.BaseHelper::get_isMacOSPlatform()
extern void BaseHelper_get_isMacOSPlatform_mC7A38F4150094D9E1C539292EF669C43FA2567B0 (void);
// 0x00000316 System.Boolean Crosstales.Common.Util.BaseHelper::get_isLinuxPlatform()
extern void BaseHelper_get_isLinuxPlatform_m417DC75B6784EF54E5B5BE01CD05705C3BD1D7DE (void);
// 0x00000317 System.Boolean Crosstales.Common.Util.BaseHelper::get_isStandalonePlatform()
extern void BaseHelper_get_isStandalonePlatform_mF704F98AD14E6122F66B2A8AE7AC406129FEA067 (void);
// 0x00000318 System.Boolean Crosstales.Common.Util.BaseHelper::get_isAndroidPlatform()
extern void BaseHelper_get_isAndroidPlatform_mCE5CA1352C1F80F40F41A29BF1C27292E840394F (void);
// 0x00000319 System.Boolean Crosstales.Common.Util.BaseHelper::get_isIOSPlatform()
extern void BaseHelper_get_isIOSPlatform_mC37175C891506DEB337DA607BC64D69709286A75 (void);
// 0x0000031A System.Boolean Crosstales.Common.Util.BaseHelper::get_isTvOSPlatform()
extern void BaseHelper_get_isTvOSPlatform_m9875AE921ECCE6CC71E9839BFAE77AE234876AFD (void);
// 0x0000031B System.Boolean Crosstales.Common.Util.BaseHelper::get_isWSAPlatform()
extern void BaseHelper_get_isWSAPlatform_m6C69C0C0891C3113A253E643E0821F65BD87B5EA (void);
// 0x0000031C System.Boolean Crosstales.Common.Util.BaseHelper::get_isXboxOnePlatform()
extern void BaseHelper_get_isXboxOnePlatform_mC6F3A25D6E8CFC89ECBA413420C9B8AC9BA4BAD9 (void);
// 0x0000031D System.Boolean Crosstales.Common.Util.BaseHelper::get_isPS4Platform()
extern void BaseHelper_get_isPS4Platform_m0E05F11A7177556AB547379BF883888973A5BD6E (void);
// 0x0000031E System.Boolean Crosstales.Common.Util.BaseHelper::get_isWebGLPlatform()
extern void BaseHelper_get_isWebGLPlatform_m33211FB09A47645AA051EC76948852BE0025D0EE (void);
// 0x0000031F System.Boolean Crosstales.Common.Util.BaseHelper::get_isWebPlatform()
extern void BaseHelper_get_isWebPlatform_m5A3E8B17AD90A821B6DC419FBECC6CB800ECBFB3 (void);
// 0x00000320 System.Boolean Crosstales.Common.Util.BaseHelper::get_isWindowsBasedPlatform()
extern void BaseHelper_get_isWindowsBasedPlatform_mF5EEC2933706EDA1EDFEB2F1FD8E3FE8A41431A5 (void);
// 0x00000321 System.Boolean Crosstales.Common.Util.BaseHelper::get_isWSABasedPlatform()
extern void BaseHelper_get_isWSABasedPlatform_mA559B2E3E0D1646C0C427B113909B963B3D40A74 (void);
// 0x00000322 System.Boolean Crosstales.Common.Util.BaseHelper::get_isAppleBasedPlatform()
extern void BaseHelper_get_isAppleBasedPlatform_m831F2787A6CD87908977B20FB1F2233D84D17E62 (void);
// 0x00000323 System.Boolean Crosstales.Common.Util.BaseHelper::get_isIOSBasedPlatform()
extern void BaseHelper_get_isIOSBasedPlatform_m5DAABF1EC5DC3B8947CC4B636EB59C8AD81C4A14 (void);
// 0x00000324 System.Boolean Crosstales.Common.Util.BaseHelper::get_isMobilePlatform()
extern void BaseHelper_get_isMobilePlatform_mC99F5223DA8B271FD45AAA36B48163FB2C0F3B7C (void);
// 0x00000325 System.Boolean Crosstales.Common.Util.BaseHelper::get_isEditor()
extern void BaseHelper_get_isEditor_m0454F7BB9528D9AEEEA24C95BC322A44839DF874 (void);
// 0x00000326 System.Boolean Crosstales.Common.Util.BaseHelper::get_isWindowsEditor()
extern void BaseHelper_get_isWindowsEditor_m1ADAFD1FB58136C7BE74018F625BF8C9ED655DFE (void);
// 0x00000327 System.Boolean Crosstales.Common.Util.BaseHelper::get_isMacOSEditor()
extern void BaseHelper_get_isMacOSEditor_m96E71133D83F40307304E28B3F103B14ECEAB0C4 (void);
// 0x00000328 System.Boolean Crosstales.Common.Util.BaseHelper::get_isLinuxEditor()
extern void BaseHelper_get_isLinuxEditor_m0FEEFCA649971D8A75CEF0B563B6965866EF5240 (void);
// 0x00000329 System.Boolean Crosstales.Common.Util.BaseHelper::get_isEditorMode()
extern void BaseHelper_get_isEditorMode_m72F760C50ED845E97E3A98BAEAC42398B3BD2C11 (void);
// 0x0000032A System.Boolean Crosstales.Common.Util.BaseHelper::get_isIL2CPP()
extern void BaseHelper_get_isIL2CPP_m266EBBA79AEE873C4A8D4D820D19CC7BFB4376A8 (void);
// 0x0000032B Crosstales.Common.Model.Enum.Platform Crosstales.Common.Util.BaseHelper::get_CurrentPlatform()
extern void BaseHelper_get_CurrentPlatform_m69DEAD89DD0EB052044CCE8FA2E94578A17609AF (void);
// 0x0000032C System.Int32 Crosstales.Common.Util.BaseHelper::get_AndroidAPILevel()
extern void BaseHelper_get_AndroidAPILevel_mB3A1455F94DB4A7D5C55134B58572F78A0955D22 (void);
// 0x0000032D System.Void Crosstales.Common.Util.BaseHelper::.cctor()
extern void BaseHelper__cctor_m8F4F00BB889949CF9761BA21335D45D71CEB629D (void);
// 0x0000032E System.Void Crosstales.Common.Util.BaseHelper::initialize()
extern void BaseHelper_initialize_mB4C285BF69FBAD4B277ED8AD10DDF08DA38F69C5 (void);
// 0x0000032F System.String Crosstales.Common.Util.BaseHelper::CreateString(System.String,System.Int32)
extern void BaseHelper_CreateString_m8187B680400CD1F2A77511DEFBC91152D1F6D9EB (void);
// 0x00000330 System.Boolean Crosstales.Common.Util.BaseHelper::hasActiveClip(UnityEngine.AudioSource)
extern void BaseHelper_hasActiveClip_m4C947A64FD354BCBAEA2078E0E1A919578D9CCBA (void);
// 0x00000331 System.String Crosstales.Common.Util.BaseHelper::ClearTags(System.String)
extern void BaseHelper_ClearTags_mD93B30D4885D2E5DCCA1672188CD20EB76B01C4F (void);
// 0x00000332 System.String Crosstales.Common.Util.BaseHelper::ClearSpaces(System.String)
extern void BaseHelper_ClearSpaces_m235DA279F449F5AD4E61A2F82D5C414DB86322CD (void);
// 0x00000333 System.String Crosstales.Common.Util.BaseHelper::ClearLineEndings(System.String)
extern void BaseHelper_ClearLineEndings_m8A5A3B1AFCE29A057A7D0EF2DE7576734251456D (void);
// 0x00000334 System.Collections.Generic.List`1<System.String> Crosstales.Common.Util.BaseHelper::SplitStringToLines(System.String,System.Boolean,System.Int32,System.Int32)
extern void BaseHelper_SplitStringToLines_mE1E3904BDE694387F27CDAD680DEF2A490ADC9FF (void);
// 0x00000335 System.String Crosstales.Common.Util.BaseHelper::FormatBytesToHRF(System.Int64,System.Boolean)
extern void BaseHelper_FormatBytesToHRF_mFBC55F381ACD1E80BB86FEBBFCB9789E3B541DD5 (void);
// 0x00000336 System.String Crosstales.Common.Util.BaseHelper::FormatSecondsToHourMinSec(System.Double)
extern void BaseHelper_FormatSecondsToHourMinSec_m3822B32615C718A3D267851AD914A984189F59F8 (void);
// 0x00000337 System.String Crosstales.Common.Util.BaseHelper::FormatSecondsToHRF(System.Double)
extern void BaseHelper_FormatSecondsToHRF_m4262478A6927A4D4D102CB9B98E6703CF965C9A0 (void);
// 0x00000338 UnityEngine.Color Crosstales.Common.Util.BaseHelper::HSVToRGB(System.Single,System.Single,System.Single,System.Single)
extern void BaseHelper_HSVToRGB_m20DA8918AE1E0B80A7E6DA9AE4F20A63CB4840F4 (void);
// 0x00000339 System.String Crosstales.Common.Util.BaseHelper::GenerateLoremIpsum(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void BaseHelper_GenerateLoremIpsum_m44F59DCB6D935E906B21AECA461F470D26CAF4BA (void);
// 0x0000033A System.String Crosstales.Common.Util.BaseHelper::LanguageToISO639(UnityEngine.SystemLanguage)
extern void BaseHelper_LanguageToISO639_m87092C04889562791B7B93374C6929CB0B9DA918 (void);
// 0x0000033B UnityEngine.SystemLanguage Crosstales.Common.Util.BaseHelper::ISO639ToLanguage(System.String)
extern void BaseHelper_ISO639ToLanguage_m1E5C667E7CB97F2039CD18892996D458647EC557 (void);
// 0x0000033C System.Object Crosstales.Common.Util.BaseHelper::InvokeMethod(System.String,System.String,System.Object[])
extern void BaseHelper_InvokeMethod_m16B910E20A79713DF61CAF991EEC69FD5832FD5D (void);
// 0x0000033D System.String Crosstales.Common.Util.BaseHelper::GetArgument(System.String)
extern void BaseHelper_GetArgument_m3A8731AE152381E9F2B611F619B435D5DA9F1528 (void);
// 0x0000033E System.String[] Crosstales.Common.Util.BaseHelper::GetArguments()
extern void BaseHelper_GetArguments_m8FEA2135EEFC1A9DBC6E530CD973F3DF3E3541D1 (void);
// 0x0000033F System.String Crosstales.Common.Util.BaseHelper::addLeadingZero(System.Int32)
extern void BaseHelper_addLeadingZero_mEB1CB744F357C1DD36B41202500BAFD00A2400D2 (void);
// 0x00000340 System.Void Crosstales.Common.Util.BaseHelper::.ctor()
extern void BaseHelper__ctor_mEF441B4F46F49B04BF884A05686974DED620C20C (void);
// 0x00000341 System.Void Crosstales.Common.Util.BaseHelper/<>c::.cctor()
extern void U3CU3Ec__cctor_m9CAEDD2D496807D1F6D85855AFA7AE14980E6C6B (void);
// 0x00000342 System.Void Crosstales.Common.Util.BaseHelper/<>c::.ctor()
extern void U3CU3Ec__ctor_m4871038D3BC7C094E3FA3BA86564AAB953E35B3F (void);
// 0x00000343 System.Collections.Generic.IEnumerable`1<System.Type> Crosstales.Common.Util.BaseHelper/<>c::<InvokeMethod>b__70_0(System.Reflection.Assembly)
extern void U3CU3Ec_U3CInvokeMethodU3Eb__70_0_m4485246A935516AEEE50BE3BE72C983693C1D7FE (void);
// 0x00000344 Crosstales.Common.Util.CTHelper Crosstales.Common.Util.CTHelper::get_Instance()
extern void CTHelper_get_Instance_m9419E0AE2630EC0841937F6CDE83B330BA39D6C9 (void);
// 0x00000345 System.Void Crosstales.Common.Util.CTHelper::set_Instance(Crosstales.Common.Util.CTHelper)
extern void CTHelper_set_Instance_m7D72BBE9FA37F24A47B37E2068BA10A6FD30ED40 (void);
// 0x00000346 System.Void Crosstales.Common.Util.CTHelper::initialize()
extern void CTHelper_initialize_m4E427FE7AE92A937936DE03A4AE592CA7B1E3BB5 (void);
// 0x00000347 System.Void Crosstales.Common.Util.CTHelper::create()
extern void CTHelper_create_m8BCAD76807872BA63E2E48039A13923C9AD30FDD (void);
// 0x00000348 System.Void Crosstales.Common.Util.CTHelper::Awake()
extern void CTHelper_Awake_m6ABEEA501E826225724F3F733DC05665C01DF14C (void);
// 0x00000349 System.Void Crosstales.Common.Util.CTHelper::OnDestroy()
extern void CTHelper_OnDestroy_m724E9967AAA52691C900954395D7543AB7716537 (void);
// 0x0000034A System.Void Crosstales.Common.Util.CTHelper::.ctor()
extern void CTHelper__ctor_mE9E3363D3C96E6E1EAA25A55602E7AD9EEF181A2 (void);
// 0x0000034B System.Boolean Crosstales.Common.Util.CTPlayerPrefs::HasKey(System.String)
extern void CTPlayerPrefs_HasKey_m343530DB4F0540483EBAA38852F22F4BDC494AB8 (void);
// 0x0000034C System.Void Crosstales.Common.Util.CTPlayerPrefs::DeleteAll()
extern void CTPlayerPrefs_DeleteAll_m691A7F70901A1FDE9195F9B8D6CB9D79F67EFDD6 (void);
// 0x0000034D System.Void Crosstales.Common.Util.CTPlayerPrefs::DeleteKey(System.String)
extern void CTPlayerPrefs_DeleteKey_mE952677FC2CF7F1F6A21B435FFC7B700247487DF (void);
// 0x0000034E System.Void Crosstales.Common.Util.CTPlayerPrefs::Save()
extern void CTPlayerPrefs_Save_m4B4CD209257285EB483F161A464D599D614B69D4 (void);
// 0x0000034F System.String Crosstales.Common.Util.CTPlayerPrefs::GetString(System.String)
extern void CTPlayerPrefs_GetString_m4A24B355B496A4C92544CEE431188605D675A7CA (void);
// 0x00000350 System.Single Crosstales.Common.Util.CTPlayerPrefs::GetFloat(System.String)
extern void CTPlayerPrefs_GetFloat_mC82EA1654D6558D8854AC0B2FB6C1EFD273E8BAA (void);
// 0x00000351 System.Int32 Crosstales.Common.Util.CTPlayerPrefs::GetInt(System.String)
extern void CTPlayerPrefs_GetInt_m1AF5A20A28BAE9A740A05FCE2261C8BC4EE85C1A (void);
// 0x00000352 System.Boolean Crosstales.Common.Util.CTPlayerPrefs::GetBool(System.String)
extern void CTPlayerPrefs_GetBool_mCBBC02069CA83ABC141BA0EBA91D5D2EDE3B6CD9 (void);
// 0x00000353 System.DateTime Crosstales.Common.Util.CTPlayerPrefs::GetDate(System.String)
extern void CTPlayerPrefs_GetDate_mD193EF979FE51EC4CA7E507BD3F7DC7C0746A5C3 (void);
// 0x00000354 UnityEngine.Vector2 Crosstales.Common.Util.CTPlayerPrefs::GetVector2(System.String)
extern void CTPlayerPrefs_GetVector2_mA4B3425E924194777E2235E85EEDCA2FCC018B22 (void);
// 0x00000355 UnityEngine.Vector3 Crosstales.Common.Util.CTPlayerPrefs::GetVector3(System.String)
extern void CTPlayerPrefs_GetVector3_m196D95045D7643D5BC2F0EE4A764FFA308AC127A (void);
// 0x00000356 UnityEngine.Vector4 Crosstales.Common.Util.CTPlayerPrefs::GetVector4(System.String)
extern void CTPlayerPrefs_GetVector4_m56F8D06161C24EF672DAF4C276C79F86EDA7CCC8 (void);
// 0x00000357 UnityEngine.Quaternion Crosstales.Common.Util.CTPlayerPrefs::GetQuaternion(System.String)
extern void CTPlayerPrefs_GetQuaternion_m532B69657E9F1697BE479EFB03F34B1189C3027C (void);
// 0x00000358 UnityEngine.Color Crosstales.Common.Util.CTPlayerPrefs::GetColor(System.String)
extern void CTPlayerPrefs_GetColor_m387F6468E13B7BD546A80CB4AEE105A73ED79E24 (void);
// 0x00000359 UnityEngine.SystemLanguage Crosstales.Common.Util.CTPlayerPrefs::GetLanguage(System.String)
extern void CTPlayerPrefs_GetLanguage_m8299A105273A79C9B9C795A8516E9C4ED9870E9E (void);
// 0x0000035A System.Void Crosstales.Common.Util.CTPlayerPrefs::SetString(System.String,System.String)
extern void CTPlayerPrefs_SetString_mB5D86CF95542A08310B0FE892F040B246770C234 (void);
// 0x0000035B System.Void Crosstales.Common.Util.CTPlayerPrefs::SetFloat(System.String,System.Single)
extern void CTPlayerPrefs_SetFloat_m257A15CE9D9E94A0A32E829BAAEC438502558CA8 (void);
// 0x0000035C System.Void Crosstales.Common.Util.CTPlayerPrefs::SetInt(System.String,System.Int32)
extern void CTPlayerPrefs_SetInt_mE40F4D337B12BD39EE80C14CE386A3D703E245EE (void);
// 0x0000035D System.Void Crosstales.Common.Util.CTPlayerPrefs::SetBool(System.String,System.Boolean)
extern void CTPlayerPrefs_SetBool_m4D6D3AB173DD3E9996FB57DCB2E47B88D1F00690 (void);
// 0x0000035E System.Void Crosstales.Common.Util.CTPlayerPrefs::SetDate(System.String,System.DateTime)
extern void CTPlayerPrefs_SetDate_mB2C8336DC4873AB8D8119D727B0CDDFA87990594 (void);
// 0x0000035F System.Void Crosstales.Common.Util.CTPlayerPrefs::SetVector2(System.String,UnityEngine.Vector2)
extern void CTPlayerPrefs_SetVector2_mAE6CB1FBFAA6C510B7D2DB6E6DD89589CD6DEA62 (void);
// 0x00000360 System.Void Crosstales.Common.Util.CTPlayerPrefs::SetVector3(System.String,UnityEngine.Vector3)
extern void CTPlayerPrefs_SetVector3_m1FB98E2E5AF96DD4FCE8BE5A730D19AC4E326B16 (void);
// 0x00000361 System.Void Crosstales.Common.Util.CTPlayerPrefs::SetVector4(System.String,UnityEngine.Vector4)
extern void CTPlayerPrefs_SetVector4_m2084892DF831A43562DE1E196CF9C33DEC7B66D6 (void);
// 0x00000362 System.Void Crosstales.Common.Util.CTPlayerPrefs::SetQuaternion(System.String,UnityEngine.Quaternion)
extern void CTPlayerPrefs_SetQuaternion_mBAE0430F0D90C2A41F193BFB19B2ABFA01044EA4 (void);
// 0x00000363 System.Void Crosstales.Common.Util.CTPlayerPrefs::SetColor(System.String,UnityEngine.Color)
extern void CTPlayerPrefs_SetColor_m3C4CB7D6C7C5FB6649E1CF02AE321FC0EA8A65C7 (void);
// 0x00000364 System.Void Crosstales.Common.Util.CTPlayerPrefs::SetLanguage(System.String,UnityEngine.SystemLanguage)
extern void CTPlayerPrefs_SetLanguage_m6BF49A3D9EFF6026644A41BC587399089D134B07 (void);
// 0x00000365 System.Void Crosstales.Common.Util.CTPlayerPrefs::.ctor()
extern void CTPlayerPrefs__ctor_m348876A7CF7E89D43ABAA44C42583305E1B13797 (void);
// 0x00000366 System.Int32 Crosstales.Common.Util.CTWebClient::get_Timeout()
extern void CTWebClient_get_Timeout_m05E7AC7AD82CFD0A255A1D753B3AB413E0C15C9E (void);
// 0x00000367 System.Void Crosstales.Common.Util.CTWebClient::set_Timeout(System.Int32)
extern void CTWebClient_set_Timeout_mC93FB85FA7A3F30D35CE10A64A25ABD18D206079 (void);
// 0x00000368 System.Int32 Crosstales.Common.Util.CTWebClient::get_ConnectionLimit()
extern void CTWebClient_get_ConnectionLimit_m74A0F4DC526B36E55EA0857E3F953453622189CE (void);
// 0x00000369 System.Void Crosstales.Common.Util.CTWebClient::set_ConnectionLimit(System.Int32)
extern void CTWebClient_set_ConnectionLimit_mFBDBF8F1C18BC022FC5B773E3874E8B9569F9068 (void);
// 0x0000036A System.Void Crosstales.Common.Util.CTWebClient::.ctor()
extern void CTWebClient__ctor_m2C17945D74024B83C76B5BB86DBF0DF78E40B537 (void);
// 0x0000036B System.Void Crosstales.Common.Util.CTWebClient::.ctor(System.Int32,System.Int32)
extern void CTWebClient__ctor_m60AC97676C10006B910233B2C508C5CE4E37B67E (void);
// 0x0000036C System.Net.WebRequest Crosstales.Common.Util.CTWebClient::CTGetWebRequest(System.String)
extern void CTWebClient_CTGetWebRequest_m73349F6C28991D04220A3D78ADA73D30D5400260 (void);
// 0x0000036D System.Net.WebRequest Crosstales.Common.Util.CTWebClient::GetWebRequest(System.Uri)
extern void CTWebClient_GetWebRequest_m6AB86C07D3BA594406B31E2C21ACAF1F4AFF8650 (void);
// 0x0000036E System.String Crosstales.Common.Util.FileHelper::get_StreamingAssetsPath()
extern void FileHelper_get_StreamingAssetsPath_m4E5AB6DB58209D9D49637CB78AE3118EE7A49A38 (void);
// 0x0000036F System.Void Crosstales.Common.Util.FileHelper::.cctor()
extern void FileHelper__cctor_m02C0AAB32878D8AE95F2804062B69AB1E7845CA2 (void);
// 0x00000370 System.Void Crosstales.Common.Util.FileHelper::initialize()
extern void FileHelper_initialize_m6603998B02EF452F39EA024FE1770F766C11EBC6 (void);
// 0x00000371 System.String Crosstales.Common.Util.FileHelper::ValidatePath(System.String,System.Boolean,System.Boolean)
extern void FileHelper_ValidatePath_mC33A5D4608F10381042A3C769355168FBF663F6C (void);
// 0x00000372 System.String Crosstales.Common.Util.FileHelper::ValidateFile(System.String)
extern void FileHelper_ValidateFile_mDFFE24BEEF3F80A0283903E81F765545686B8EF8 (void);
// 0x00000373 System.Boolean Crosstales.Common.Util.FileHelper::PathHasInvalidChars(System.String)
extern void FileHelper_PathHasInvalidChars_mCE73D25E334A44CFBDBC7B103A873B496668F65D (void);
// 0x00000374 System.Boolean Crosstales.Common.Util.FileHelper::FileHasInvalidChars(System.String)
extern void FileHelper_FileHasInvalidChars_m020222D85D2F6FAEE816FAAD7381D597C1E793E2 (void);
// 0x00000375 System.String[] Crosstales.Common.Util.FileHelper::GetFiles(System.String,System.Boolean,System.String[])
extern void FileHelper_GetFiles_m599BE482715EA35B5198B77E79713FC266FFACB9 (void);
// 0x00000376 System.String[] Crosstales.Common.Util.FileHelper::GetDirectories(System.String,System.Boolean)
extern void FileHelper_GetDirectories_mD5A7507379E69911435BEF4078D8065C9B5A4D08 (void);
// 0x00000377 System.String[] Crosstales.Common.Util.FileHelper::GetDrives()
extern void FileHelper_GetDrives_mCD7ED284CF74773F8ADB013BADC0BA2F58F34A3A (void);
// 0x00000378 System.Void Crosstales.Common.Util.FileHelper::CopyPath(System.String,System.String,System.Boolean)
extern void FileHelper_CopyPath_m5E1C94380A6DD84735FC4B72872D646C8004CFC2 (void);
// 0x00000379 System.Void Crosstales.Common.Util.FileHelper::CopyFile(System.String,System.String,System.Boolean)
extern void FileHelper_CopyFile_mBF3C0BD70CFBF9B0AD2470120342732DD457B775 (void);
// 0x0000037A System.Void Crosstales.Common.Util.FileHelper::ShowPath(System.String)
extern void FileHelper_ShowPath_m2DD2F1BDA570314F8457A26CA3DA8287778C41AA (void);
// 0x0000037B System.Void Crosstales.Common.Util.FileHelper::ShowFile(System.String)
extern void FileHelper_ShowFile_m948285872AE204C99F232E36B9044D7662D5ECE8 (void);
// 0x0000037C System.Void Crosstales.Common.Util.FileHelper::OpenFile(System.String)
extern void FileHelper_OpenFile_m7E3CB79CC5A565D5D9434C841F8FFB672C7D78E3 (void);
// 0x0000037D System.Void Crosstales.Common.Util.FileHelper::copyAll(System.IO.DirectoryInfo,System.IO.DirectoryInfo)
extern void FileHelper_copyAll_m5F3C822743C6C087E40CFA0D24720362CFF513FB (void);
// 0x0000037E System.Void Crosstales.Common.Util.FileHelper::.ctor()
extern void FileHelper__ctor_m61D51C1B2CFE64771C1E0C3DD4F9855375A4C71C (void);
// 0x0000037F System.Void Crosstales.Common.Util.FileHelper/<>c::.cctor()
extern void U3CU3Ec__cctor_m5813CAAC58D5639CBA063C4791613A99AE1A1D5B (void);
// 0x00000380 System.Void Crosstales.Common.Util.FileHelper/<>c::.ctor()
extern void U3CU3Ec__ctor_mBAFD1D5A93FD498CC1AFCC92405D87762B751D9A (void);
// 0x00000381 System.Boolean Crosstales.Common.Util.FileHelper/<>c::<GetFiles>b__9_0(System.String)
extern void U3CU3Ec_U3CGetFilesU3Eb__9_0_m6CC95DCEAA66EFC6A2FFA707CF64A62489173CBC (void);
// 0x00000382 System.String Crosstales.Common.Util.FileHelper/<>c::<GetFiles>b__9_1(System.String)
extern void U3CU3Ec_U3CGetFilesU3Eb__9_1_m64D4E8AAB0A38B460650E8AF15F21AC5FED29CE2 (void);
// 0x00000383 System.Void Crosstales.Common.Util.MemoryCacheStream::.ctor(System.Int32,System.Int32)
extern void MemoryCacheStream__ctor_m40170499193384760602B5CBAB91B07CBE927344 (void);
// 0x00000384 System.Boolean Crosstales.Common.Util.MemoryCacheStream::get_CanRead()
extern void MemoryCacheStream_get_CanRead_mF29BEF03308C1D07B902FA4A20DAD8A52C421576 (void);
// 0x00000385 System.Boolean Crosstales.Common.Util.MemoryCacheStream::get_CanSeek()
extern void MemoryCacheStream_get_CanSeek_m160BB37D087E2DFA069CD23E9D631D23D35181B7 (void);
// 0x00000386 System.Boolean Crosstales.Common.Util.MemoryCacheStream::get_CanWrite()
extern void MemoryCacheStream_get_CanWrite_m809E30F6287E5E7CE0521E847799A0C08D7DBD92 (void);
// 0x00000387 System.Int64 Crosstales.Common.Util.MemoryCacheStream::get_Position()
extern void MemoryCacheStream_get_Position_m87F36FC9A3C82CAAF85E59B0F0AE18FFA7D6A71C (void);
// 0x00000388 System.Void Crosstales.Common.Util.MemoryCacheStream::set_Position(System.Int64)
extern void MemoryCacheStream_set_Position_mF5A5556EE64B5A296CE4DB17CE2C83819616FD62 (void);
// 0x00000389 System.Int64 Crosstales.Common.Util.MemoryCacheStream::get_Length()
extern void MemoryCacheStream_get_Length_m76EC80DED25B746ACC0561071A9FE2BA6E897ED6 (void);
// 0x0000038A System.Void Crosstales.Common.Util.MemoryCacheStream::Flush()
extern void MemoryCacheStream_Flush_m8C478B46CE9BC988351CCFEF6332CDF38380E788 (void);
// 0x0000038B System.Int64 Crosstales.Common.Util.MemoryCacheStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void MemoryCacheStream_Seek_mC36176922316F2F3A643E3173D5F1BCD2B2F1D1A (void);
// 0x0000038C System.Void Crosstales.Common.Util.MemoryCacheStream::SetLength(System.Int64)
extern void MemoryCacheStream_SetLength_mE65E670C20884319228CA0F4B01DDAFC20C5B454 (void);
// 0x0000038D System.Int32 Crosstales.Common.Util.MemoryCacheStream::Read(System.Byte[],System.Int32,System.Int32)
extern void MemoryCacheStream_Read_mDD54151F5A800A1C12A8901949C749A978ECB950 (void);
// 0x0000038E System.Void Crosstales.Common.Util.MemoryCacheStream::Write(System.Byte[],System.Int32,System.Int32)
extern void MemoryCacheStream_Write_m6FF8ED70BC871A06BCF176A5A57AF1070FD0AB7C (void);
// 0x0000038F System.Int32 Crosstales.Common.Util.MemoryCacheStream::read(System.Byte[],System.Int32,System.Int32)
extern void MemoryCacheStream_read_m192B46EA7033C77496F8D0F52D8F5704A4131DA3 (void);
// 0x00000390 System.Void Crosstales.Common.Util.MemoryCacheStream::write(System.Byte[],System.Int32,System.Int32)
extern void MemoryCacheStream_write_mA361C3FC58B5C1898872BE6E0E66CB9887F2971D (void);
// 0x00000391 System.Void Crosstales.Common.Util.MemoryCacheStream::createCache()
extern void MemoryCacheStream_createCache_m8EA614C1C5062058051EFDF9A4176DE4971E63D8 (void);
// 0x00000392 System.Boolean Crosstales.Common.Util.NetworkHelper::get_isInternetAvailable()
extern void NetworkHelper_get_isInternetAvailable_m6F5E2844598A6D921748992C651E3D8A780653A2 (void);
// 0x00000393 System.Boolean Crosstales.Common.Util.NetworkHelper::OpenURL(System.String)
extern void NetworkHelper_OpenURL_m7D5B0C92473554A3AE515E3B0236A58D71EC06B8 (void);
// 0x00000394 System.Boolean Crosstales.Common.Util.NetworkHelper::RemoteCertificateValidationCallback(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void NetworkHelper_RemoteCertificateValidationCallback_mC30A8BD07A16AF302D5A084E969D824B37D90609 (void);
// 0x00000395 System.String Crosstales.Common.Util.NetworkHelper::ValidURLFromFilePath(System.String)
extern void NetworkHelper_ValidURLFromFilePath_mB9526E571334DED87344C0F002C0DD991C5AD3E8 (void);
// 0x00000396 System.String Crosstales.Common.Util.NetworkHelper::CleanUrl(System.String,System.Boolean,System.Boolean,System.Boolean)
extern void NetworkHelper_CleanUrl_m443D4EBEFE8D6459F41B706EBA1E0EB6F041EAB7 (void);
// 0x00000397 System.Boolean Crosstales.Common.Util.NetworkHelper::isValidURL(System.String)
extern void NetworkHelper_isValidURL_mD13744A8999E3B04359201FCC8152513A2D9A919 (void);
// 0x00000398 System.String Crosstales.Common.Util.NetworkHelper::GetIP(System.String)
extern void NetworkHelper_GetIP_m3A4B2E9379398548EBD765C4DBDA95A80A0548F2 (void);
// 0x00000399 System.Void Crosstales.Common.Util.NetworkHelper::openURL(System.String)
extern void NetworkHelper_openURL_m79A8EB78C6B19DD841D7E3260369B30E2C78830D (void);
// 0x0000039A System.Void Crosstales.Common.Util.NetworkHelper::.ctor()
extern void NetworkHelper__ctor_m71B486C709399BE9BE73F5651BAC39A9ACCDECAC (void);
// 0x0000039B System.Void Crosstales.Common.Util.NetworkHelper/<>c::.cctor()
extern void U3CU3Ec__cctor_m3C751253FC6DC556714F342215707EA9E9EF6C84 (void);
// 0x0000039C System.Void Crosstales.Common.Util.NetworkHelper/<>c::.ctor()
extern void U3CU3Ec__ctor_m22AD43D2037CEEA28886D5ED535393B8805567C3 (void);
// 0x0000039D System.Boolean Crosstales.Common.Util.NetworkHelper/<>c::<RemoteCertificateValidationCallback>b__5_0(System.Security.Cryptography.X509Certificates.X509ChainStatus)
extern void U3CU3Ec_U3CRemoteCertificateValidationCallbackU3Eb__5_0_mFBFBC5681529E306BAD1725AF81972C0DC2C1765 (void);
// 0x0000039E T Crosstales.Common.Util.Singleton`1::get_Instance()
// 0x0000039F System.Void Crosstales.Common.Util.Singleton`1::set_Instance(T)
// 0x000003A0 System.Boolean Crosstales.Common.Util.Singleton`1::get_DontDestroy()
// 0x000003A1 System.Void Crosstales.Common.Util.Singleton`1::set_DontDestroy(System.Boolean)
// 0x000003A2 System.Void Crosstales.Common.Util.Singleton`1::Awake()
// 0x000003A3 System.Void Crosstales.Common.Util.Singleton`1::OnDestroy()
// 0x000003A4 System.Void Crosstales.Common.Util.Singleton`1::OnApplicationQuit()
// 0x000003A5 System.Void Crosstales.Common.Util.Singleton`1::CreateInstance(System.Boolean,System.Boolean)
// 0x000003A6 System.Void Crosstales.Common.Util.Singleton`1::DeleteInstance()
// 0x000003A7 System.Void Crosstales.Common.Util.Singleton`1::.ctor()
// 0x000003A8 System.Void Crosstales.Common.Util.Singleton`1::.cctor()
// 0x000003A9 System.Boolean Crosstales.Common.Util.SingletonHelper::get_isQuitting()
extern void SingletonHelper_get_isQuitting_mA9E2C23D74C48DCBB8FF9D9380A6C2F279B840C0 (void);
// 0x000003AA System.Void Crosstales.Common.Util.SingletonHelper::set_isQuitting(System.Boolean)
extern void SingletonHelper_set_isQuitting_m53D01E30152CDE243B85A357E5B55BAF6F346169 (void);
// 0x000003AB System.Void Crosstales.Common.Util.SingletonHelper::.cctor()
extern void SingletonHelper__cctor_m21D108C7AC8D3075DA8DF356BA734327AE6F1051 (void);
// 0x000003AC System.Void Crosstales.Common.Util.SingletonHelper::initialize()
extern void SingletonHelper_initialize_mC53AE8824DF8FAAD29911D0F684E9174D96551DB (void);
// 0x000003AD System.Void Crosstales.Common.Util.SingletonHelper::onSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void SingletonHelper_onSceneLoaded_mB6ACCBC7368B74C518C65AC80CC871C4E63CAADC (void);
// 0x000003AE System.Void Crosstales.Common.Util.SingletonHelper::onSceneUnloaded(UnityEngine.SceneManagement.Scene)
extern void SingletonHelper_onSceneUnloaded_mBD26DDE74009DDEA082871A39B5C5BD72C5EFECF (void);
// 0x000003AF System.Void Crosstales.Common.Util.SingletonHelper::onQuitting()
extern void SingletonHelper_onQuitting_mD1095EB0956AD487EB7F002CDF156B7EEA476C55 (void);
// 0x000003B0 System.Void Crosstales.Common.Util.SingletonHelper::.ctor()
extern void SingletonHelper__ctor_m623904A0145A0A36E397BB4B414133F5D6F28F47 (void);
// 0x000003B1 System.Void Crosstales.Common.Util.XmlHelper::SerializeToFile(T,System.String)
// 0x000003B2 T Crosstales.Common.Util.XmlHelper::DeserializeFromFile(System.String,System.Boolean)
// 0x000003B3 System.String Crosstales.Common.Util.XmlHelper::SerializeToString(T)
// 0x000003B4 T Crosstales.Common.Util.XmlHelper::DeserializeFromString(System.String,System.Boolean)
// 0x000003B5 T Crosstales.Common.Util.XmlHelper::DeserializeFromResource(System.String,System.Boolean)
// 0x000003B6 System.Void Crosstales.Common.Util.XmlHelper::.ctor()
extern void XmlHelper__ctor_m654E053ADB7377E528EA5D3C770930ECF9F5F15E (void);
// 0x000003B7 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_m93FD10C4CC1978E570BB23B36EB64D5DC41D3E8B (void);
static Il2CppMethodPointer s_methodPointers[951] = 
{
	HttpUtility_HtmlAttributeEncode_mFB310B3118FCBDEE520B3B66076E7C41A63F9B42,
	HttpUtility_HtmlAttributeEncode_m76ABB6A0A9945B7A2D541FD345B3B2FF5EA3DEBA,
	HttpUtility_UrlDecode_mDCE0DC8A8B2EEAB1DABFBBFD1D8A72674E00CB64,
	HttpUtility_GetChars_m10FE2F88ED6C53BDFB106FB7C07EF0AB72E4B5EB,
	HttpUtility_WriteCharBytes_m2F4F0AD276CE4A8EA6A38B6C4EA1442E90A290D2,
	HttpUtility_UrlDecode_m25AD5F3B91B1424A81419D730B71E2A6A2160CE6,
	HttpUtility_UrlDecode_mEB6BC975712FC342ED2ADDF45CEF1B79D4CBD28C,
	HttpUtility_GetInt_m822542991D7FA03EDB478A5CB22619F713449CD7,
	HttpUtility_GetChar_mBC124F1928642E4D97362707DA3531B38895DE42,
	HttpUtility_GetChar_mA72B0E4325E8DDDAF038FD08DE9A66A6D3CB837B,
	HttpUtility_UrlDecode_mF3D2958AAF0CD705C0A74CBD0576EE0B250FC80F,
	HttpUtility_UrlDecodeToBytes_m7B3E224594640CDD2E841E5EF0E4CCFF500EB5A4,
	HttpUtility_UrlDecodeToBytes_mEEF17ECBC2186BCF7C628603CE34AFD449ABC15F,
	HttpUtility_UrlDecodeToBytes_m5E1930BAD4E6805FE07466B0BD948842A264D170,
	HttpUtility_UrlDecodeToBytes_mF6D06476481DD8423E6098F20D4B8C9B0184E546,
	HttpUtility_UrlEncode_mBD64946EBFC3A2B7248A9FAA38161E7CD12BE7CD,
	HttpUtility_UrlEncode_m456C7D073217581F09233FFA03EAE8427B6C48D0,
	HttpUtility_UrlEncode_mCD575B5B2FC1279555239FAFCFB1910F10970C8D,
	HttpUtility_UrlEncode_m87E304E827FD60E5E210C07FE5059352555065A0,
	HttpUtility_UrlEncodeToBytes_m3A992052697CEB7D2E9DF9E52C07324C4AB35776,
	HttpUtility_UrlEncodeToBytes_mD13BF036320C80DF0C9EC9B23E333358441374A8,
	HttpUtility_UrlEncodeToBytes_m415E4FF6CABDFA90FE561D2DE4731234F4401D76,
	HttpUtility_UrlEncodeToBytes_mD19F3DEA461605E11798CFBAB201B5941A9F0E42,
	HttpUtility_UrlEncodeUnicode_mE75F2D069EC0D410B0A2D6621FE43FED2BA6EDA2,
	HttpUtility_UrlEncodeUnicodeToBytes_mEFD047B2F98F1C2D0A5A749E58CA6D00C890A403,
	HttpUtility_HtmlDecode_m89865A9C9E38F40F6238AF90200DDF2512F4FCDB,
	HttpUtility_HtmlDecode_m2301529A30CC2D1FDA4A000E7F1D8865B28DCA43,
	HttpUtility_HtmlEncode_m4B58D7D0DDA493AAAD183A892EDF155D5B37DE20,
	HttpUtility_HtmlEncode_m03D7BBDFBF2822E88BE58F136890896578FBCC8F,
	HttpUtility_HtmlEncode_m86FD4FE9ACB77E95954C9FBAE0A86CE4C0226FD8,
	HttpUtility_JavaScriptStringEncode_mEA9D451EA516ECF3395237B45033C7B34760382B,
	HttpUtility_JavaScriptStringEncode_m538A719B92DFACAC9E213E4734A1C0533F74DF9F,
	HttpUtility_UrlPathEncode_mA98DCEB39FE9582BFEEA6AB5A9E2664E0BE80C78,
	HttpUtility_ParseQueryString_mB325C5BD841A4B1C0B56AA3EEBD259EFD85B67D8,
	HttpUtility_ParseQueryString_m9F35D508CDF4D4213BC118FB941A107D626CAFC5,
	HttpUtility_ParseQueryString_m78D0C846989BC7A1BA1928D7344BE2D3057330FB,
	HttpUtility__ctor_m76A71DD56230E87B1D2FCB3BA0EA3505FD032965,
	HttpQSCollection_ToString_m9C58AAB57BF0207D5FEA293177393246BFEB18D4,
	HttpQSCollection__ctor_mB389555A49ADC48AD83FD08D48AA3BB4C109F231,
	HttpEncoder_get_Entities_m54C0BE50D899420656EAD97AD36BE46CE035E795,
	HttpEncoder_get_Current_m2C148C53CD1782FB28CFA8D81A9267D95C68356E,
	HttpEncoder_set_Current_m13F47C04002980F9AD4A22CD78DDF27CAC96F14A,
	HttpEncoder_get_Default_m0F86D1400927A0889893B0FAD06C40C888305627,
	HttpEncoder__cctor_m5588B60D3A267C51709E86B1785EAA0808C629A4,
	HttpEncoder_HeaderNameValueEncode_m2DA9357552A34277C8FFABBB6D1977436F66657D,
	HttpEncoder_StringBuilderAppend_m42D772F6C0CF7B72388F4582165B6AE14E77C735,
	HttpEncoder_EncodeHeaderString_mFFCDE1400E1723437DC974558D0E65628305F59E,
	HttpEncoder_HtmlAttributeEncode_m711698F231D441AC6EC219C4B45E3CB78607E2E8,
	HttpEncoder_HtmlDecode_mFD44693A6DC61CF05B362588043F9DF87B4EAB65,
	HttpEncoder_HtmlEncode_m3EFD95242BD31ABB43D2CD6A37684DF44CC9A7BA,
	HttpEncoder_UrlEncode_m94AA2673FED83A8A432021773C2E0EA0B2F5CEFC,
	HttpEncoder_GetCustomEncoderFromConfig_m7105173C28832B8A3E1A279369309D5DCC746590,
	HttpEncoder_UrlPathEncode_m4799353783767D40A539D524591E038C8EB9449D,
	HttpEncoder_UrlEncodeToBytes_m54605E546A567509DD80E184B3E64B1990B6956C,
	HttpEncoder_HtmlEncode_m9BF97452F4FF897E2D8AC094DFA57BD1E2A8269F,
	HttpEncoder_HtmlAttributeEncode_m1DF47A3525AE3D88033219A2528ECAB689FC4A42,
	HttpEncoder_HtmlDecode_mD0EDD86498EDF3CDBB4CE4521B827AB7E5FCCFBC,
	HttpEncoder_NotEncoded_m0337D8C19410E0A5F3000991DEB3ED60DC79FCC9,
	HttpEncoder_UrlEncodeChar_mC171E4812EE815673E62FE0C77BD053A82DC8C77,
	HttpEncoder_UrlPathEncodeChar_m2C4BF7FA9BCBB4F9FD8C9BA01C55CC64DDE6BDBB,
	HttpEncoder_InitEntities_m1B0CCCA2C51AD06644E416588820BABE11230343,
	HttpEncoder__ctor_m16B6CE891F3BD847B3F25397628DD9EFE4C693F1,
	U3CU3Ec__cctor_m64940D4CF4CB9C2AE5230188FBBF118A625FCCD5,
	U3CU3Ec__ctor_mB2D9CC35899D065F44FA0CEBABCEB5A8513B7DA0,
	U3CU3Ec_U3C_cctorU3Eb__13_0_m19DC89A621E62FBF1D83B7C6C4C0FC89B2449CB1,
	U3CU3Ec_U3CEncodeHeaderStringU3Eb__16_0_mD1308E53DE31892F56627928D48130F41A881ED8,
	U3CU3Ec_U3CHtmlEncodeU3Eb__24_0_m4F030B816C961EFC363199A4DE17625D14769C2B,
	U3CU3Ec_U3CHtmlAttributeEncodeU3Eb__25_0_m0093B091713A8781FE24255E0214890BF6EE68B2,
	ExtensionMethods_CTToTitleCase_m0FAF40693A916D1BA985195E30AA7B9753C11DE2,
	ExtensionMethods_CTReverse_m375B1B5F94367BC73BC054481C4528E1829758A0,
	ExtensionMethods_CTReplace_mE45837F5BF2ECDD64251F7DC85303D36308AFBB9,
	ExtensionMethods_CTEquals_m70FD226B78B4DD7ED0672F326B16568EA3D9B446,
	ExtensionMethods_CTContains_m52B3F3D1019BBD1746371264A2671656CDADB3C2,
	ExtensionMethods_CTContainsAny_m50A29F87C6A50663D9B599FEA1D1359EC07969A3,
	ExtensionMethods_CTContainsAll_mAB9F9024F8E76F4449FE666602718634956C2464,
	ExtensionMethods_CTRemoveNewLines_mF3F6F1FD9F609208AF748156579F02AB68B15C24,
	ExtensionMethods_CTAddNewLines_m3FDE4E74DB180C8F829329BFFD4934CB93057581,
	ExtensionMethods_CTisNumeric_mB664DBA85A056E3855D912C93DA7B704584752DC,
	ExtensionMethods_CTisInteger_m9A0F9A80C8B5CDADCD3500534A9FF53271BC5EA3,
	ExtensionMethods_CTisEmail_m2336B119F8D08E08DF5335FEAC1699978FC6405F,
	ExtensionMethods_CTisWebsite_m7DC7369502F9A000269EEC781ABA71452667E85B,
	ExtensionMethods_CTisCreditcard_m62CDA4DAC05FE6307258BB4626FF76CF939C3C78,
	ExtensionMethods_CTisIPv4_mA58907D12ED35BC0D4A3277E8475EF095B440126,
	ExtensionMethods_CTisAlphanumeric_m2E1A017EEB60FF2F1E2391C0074E81B406A98A2A,
	ExtensionMethods_CThasLineEndings_m95A836BD2BE4FFFFD877D5FD894AADF33428A129,
	ExtensionMethods_CThasInvalidChars_m3C830A06687FDE6DE20154B64E2E4D531B7A9ED4,
	ExtensionMethods_CTStartsWith_mFCFFAAF1BEE786BD337B3B2296D32744AB0A2542,
	ExtensionMethods_CTEndsWith_m666D85A99AD0967EBD41A838F37819D298D23ADF,
	ExtensionMethods_CTLastIndexOf_m2F16141C163B099C5BCDF1D4E3426834EAD6A863,
	ExtensionMethods_CTIndexOf_m3C965AE80FE82E5E12EC02B9019CF48B1D16B1C5,
	ExtensionMethods_CTIndexOf_mD1B263B1E864EE567DD4C711597BA5556B0657E3,
	ExtensionMethods_CTToBase64_m95D893BE1209C12BFC96FDCEA54371C1741ED35B,
	ExtensionMethods_CTFromBase64_m5018159F11AAAD7AAE89A93D795932112BB18D1C,
	ExtensionMethods_CTFromBase64ToByteArray_m66CADB7E423C7A554BB4B0069FCD5665402C67DE,
	ExtensionMethods_CTToHex_mE0934C22838185424921F80774725E4F5CD5FEED,
	ExtensionMethods_CTHexToString_m93D0EABF83200F514CC04FEAB3036266634B67B9,
	ExtensionMethods_CTHexToColor32_mD1A93B26BC2D250FEA2834700CAC106519A62771,
	ExtensionMethods_CTHexToColor_m1FD7E347868D5E6A6450C36DF93DABAE02C468AC,
	ExtensionMethods_CTToByteArray_m8BBB5E420285A72B5F964FF51944FB972837AE8A,
	ExtensionMethods_CTClearTags_m6F0896B414261998FD998D185D75F03B3151875E,
	ExtensionMethods_CTClearSpaces_m39BCF0466C31347D3DB7BAF627B2AF586CCC11B3,
	ExtensionMethods_CTClearLineEndings_m923BE72DD4DC59DB6307F90F2ABC30A90DBB692D,
	NULL,
	NULL,
	ExtensionMethods_CTDump_m9372AF9431740E725E0A0EE720EEB4B51EF8D626,
	ExtensionMethods_CTDump_mDD2831261D66781D294ADBFFBD9A397D4A5B4681,
	ExtensionMethods_CTDump_m2A792DFFEECEFF18E99A289916B9165C127E7883,
	ExtensionMethods_CTDump_mEDCBC052999C7F88ECF5897E865B6467FF1F452E,
	NULL,
	ExtensionMethods_CTToFloatArray_m7E2B709CDA0C352394C296884A120A4FFB1576A3,
	ExtensionMethods_CTToByteArray_m11AEF2E58D65C64EF7BBE114385999E73726E236,
	ExtensionMethods_CTToTexture_m12DC9C6DC82C07A891F5EA4AC628DEBB737C5DB2,
	ExtensionMethods_CTToSprite_mD7FF7050F7C0BE3EE55C86A445CF3559C0D07BA3,
	ExtensionMethods_CTToString_mBAB4670FE933D06A3F39F51219830F8D3CA946F5,
	ExtensionMethods_CTToBase64_m0E9D5F1F9174FAA4776F788ECA88A8709A97BCE5,
	NULL,
	NULL,
	ExtensionMethods_CTDump_m59F8DE0A578B3B891B4D8C1ED38688E86940F976,
	ExtensionMethods_CTDump_m6E14577C017276ACD5DA587C029A664694E859C9,
	ExtensionMethods_CTDump_mD3A9D8E4C2FAF2BE628602C89238233DBE170FDE,
	ExtensionMethods_CTDump_mB00AF342A1C001008DDEE5422E6CE9386ADF044C,
	NULL,
	NULL,
	NULL,
	ExtensionMethods_CTReadFully_mCD211B8D82B74DE675FB131FBAC880B034B7B553,
	ExtensionMethods_CTToHexRGB_mEB271EF7DE0D38158B4D1C9E1F5A2DCAD7B97502,
	ExtensionMethods_CTToHexRGB_m1E39198A3F7FD8F35CC5BD27984C09FD5CDFEC5A,
	ExtensionMethods_CTToHexRGBA_m1E583B32CC5214340E8C18EA005281D8748D689B,
	ExtensionMethods_CTToHexRGBA_m8BF2160A9E4353077DBDE0F048F2B4000FC877EF,
	ExtensionMethods_CTVector3_mCBBA0D200DA6E33DFDF5F968F64682AF279A1F72,
	ExtensionMethods_CTVector3_m8CFAC4C85BA2694A83E2BDFB3A43B317005BDFE6,
	ExtensionMethods_CTVector4_m4CA428D976ADBCA1876085C9942F953F90DC6592,
	ExtensionMethods_CTVector4_m3E12FD9E6E3310BE328E10AB105D3ADCFFF49522,
	ExtensionMethods_CTMultiply_m305DB38572F0613C1602758541DE685CD229B168,
	ExtensionMethods_CTMultiply_mFF0ED38A9DA8F4B6A0D650560279442844DEA6AE,
	ExtensionMethods_CTFlatten_mC4047C6959F5B6BC261F88F4790682FCFC383CC1,
	ExtensionMethods_CTQuaternion_m462BF09597B2909DB2D1128548CB3B1E3D0319A1,
	ExtensionMethods_CTColorRGB_m39E122A470F7CAA46A440D03B2DDB4AB1D8B302B,
	ExtensionMethods_CTMultiply_m5C8CA96F97B488BC1E1DD14854E6603037247CFB,
	ExtensionMethods_CTQuaternion_m4A9E9712734553E922C7789A7051CF06A384A0B0,
	ExtensionMethods_CTColorRGBA_m0423B016C705D61B4186C44E18B5E4A168598A7A,
	ExtensionMethods_CTVector3_m1F8621C9295026E750D0342405B2118B874F52E4,
	ExtensionMethods_CTVector4_m1F9C3149D5060CED2F9DFFF8A3CB39E95ABC5729,
	ExtensionMethods_CTCorrectLossyScale_m8B4CB2FCAA60A49AEA8FBA793EE49D69AA7B11F7,
	ExtensionMethods_CTGetLocalCorners_m866B0CC7B69D7A4E17E65B2D870CBED28F9C7307,
	ExtensionMethods_CTGetLocalCorners_m93E67F2D92BA9C9F54D41CA6769ADE481D9208AF,
	ExtensionMethods_CTGetScreenCorners_m2F1E5E3409D23E804EB07803B1317339FDF28B87,
	ExtensionMethods_CTGetScreenCorners_m39B94363A1BB9CAA84AC5F4D7C6F652FA07D35E0,
	ExtensionMethods_CTGetBounds_m2CB978298B520DEDC083F4E6F763D89534850569,
	ExtensionMethods_CTSetLeft_m7241C29911EB32D2BF73B4F20ED2F0B8E1E465DF,
	ExtensionMethods_CTSetRight_m7B0B7371C3243430FE05D24A4EC4E466F53E1C53,
	ExtensionMethods_CTSetTop_m7E0B0D5B5F497660AF71936B287C82378579FB40,
	ExtensionMethods_CTSetBottom_m2DBDB41D978F0AC8191285887530BD93F1CD73BF,
	ExtensionMethods_CTGetLeft_m81CE42F10665F9DB57E8E9E1E2071CC82CE56202,
	ExtensionMethods_CTGetRight_mC83506F109D5492465A5B662C8B44A21BA3A4A33,
	ExtensionMethods_CTGetTop_m99A1549D99E13343E61F5EDEA3F8888D64A56C5F,
	ExtensionMethods_CTGetBottom_mA70B416EF67700E19A23E6ABDFA862839B45688E,
	ExtensionMethods_CTGetLRTB_mCA1C1A546BA77B9FDA6394815F879F8E91EFA733,
	ExtensionMethods_CTSetLRTB_m5578C7E73B1BA1C8F17854D347D2EFFDBBE8FC23,
	ExtensionMethods_CTFind_m50A483F38921A7B82DB1B279AD2C2A0DCD213B6C,
	NULL,
	ExtensionMethods_CTFind_mA46461FE50A61DA998A72004CDF0BA8EA85275C6,
	NULL,
	ExtensionMethods_CTGetBounds_mC52C517BCE211AA80A937DFB6BBDA57B5365ACE7,
	ExtensionMethods_CTFind_mC05248D7AFAA90746B748E4020E169E1375B6988,
	NULL,
	ExtensionMethods_CTToPNG_m9287BB5CA9F16C447688DF15869B3CB73C8D2EAB,
	ExtensionMethods_CTToJPG_m9ED2E438345EDC4ECEBBAD7BBF0C84AF3833956C,
	ExtensionMethods_CTToTGA_m4130CAC5FA95B713F6FE6E10056940AFE45CAD9C,
	ExtensionMethods_CTToEXR_mD8FB2B651C2ED92132E2A06D332FE7D7F9A99310,
	ExtensionMethods_CTToPNG_mDC05E791A75ED3F86DD665B80986D0A332FF5193,
	ExtensionMethods_CTToJPG_mCD0C94CDD3306AC2B24B63DEF8CF561CCDB1C454,
	ExtensionMethods_CTToTGA_mE9EA3A0A4E9BC648F5291C7C445B5A394CB460FD,
	ExtensionMethods_CTToEXR_m3BC7385919EB0ABC13BFA69C725FAE5281112DBB,
	ExtensionMethods_CTToSprite_m036A44049C3B80B9A4F9D4D4651D50092F0B289C,
	ExtensionMethods_CTRotate90_mD1AB4357D802DB346FDD7FAC48ACDDBF9E12EC20,
	ExtensionMethods_CTRotate180_m9B592620D061F71ECF700F0DEA2436C8D24FEB21,
	ExtensionMethods_CTRotate270_m384C9E9CA403E44863115554B93B988457F1CE63,
	ExtensionMethods_CTToTexture2D_m9A7AE558EB1AE7B2CE6CF7B74F77855AB0E3573F,
	ExtensionMethods_CTFlipHorizontal_m1A71CDC27223F806C143D8F1D7E51DECF5A202C4,
	ExtensionMethods_CTFlipVertical_m9B31708827E99F4154C9B40E4233852D497CFB62,
	ExtensionMethods_CTHasActiveClip_m01F0DA4A380C00C95759FDA648D8D51289CC492E,
	ExtensionMethods_CTIsVisibleFrom_m5E3DED552FD8B132DD4E2B700E4BC17BCE83E587,
	ExtensionMethods_deepSearch_m78B1F8D6139E2F196B37815757A5A29BD388FE59,
	ExtensionMethods_bytesToFloat_mA18BE713E7BFEBD57BC8A5D5D5353DCCDB29CBA1,
	ExtensionMethods__cctor_mE090B3D1469E9F7E1E7D1014E6E16F328170E7BC,
	U3CU3Ec__DisplayClass6_0__ctor_m2C47077153CBA7277CC4630ED5F134A341294672,
	U3CU3Ec__DisplayClass6_0_U3CCTContainsAnyU3Eb__0_m1C1AC3F24EC2227866E4F06EBEE7F7F7F74ED0B6,
	U3CU3Ec__DisplayClass7_0__ctor_mB32411BDFA046D17ECFBB99C6C6443D1CB278451,
	U3CU3Ec__DisplayClass7_0_U3CCTContainsAllU3Eb__0_m70EA7F4283082A12DCE6D3005999FB5D3A78C747,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass81_0__ctor_m8E43366EA85C9C13298797A5B0168694A89734BB,
	U3CU3Ec__DisplayClass81_0_U3CCTGetBoundsU3Eb__0_m15B5CB3C6584E57545865C3C6E3050196DBF8EB6,
	StatusChangeEvent__ctor_m3A04F9FA07A10715B49CAA90637167AF00DA0C54,
	OnlineCheck_get_EndlessMode_mFE6601437C30CB5E0800F13DEDCDFDB5634C6E72,
	OnlineCheck_set_EndlessMode_mE2F515FA780525E09081840FC4F17EC94F951843,
	OnlineCheck_get_IntervalMin_mDE33E661FE5E80BDA66D8D5094D89EBAAC5A628C,
	OnlineCheck_set_IntervalMin_m8E302ACC2C9F61DD6F8B43A9B3942AD4ED9DD9EC,
	OnlineCheck_get_IntervalMax_mD584354F4FD03C36DA2AA34235321BF838C9A746,
	OnlineCheck_set_IntervalMax_m5C5C7B1C88AB2CDA4C892114644B10C54DC75AA7,
	OnlineCheck_get_Timeout_m44104F89C15998A0239D5BC55944425E6712B7D1,
	OnlineCheck_set_Timeout_m9F5C7D118CDC03ED23D55F80C9188B34C43D9011,
	OnlineCheck_get_ForceWWW_mAC076AF4AA6563981C165DE04D31D79806BDECE3,
	OnlineCheck_set_ForceWWW_mEA4C637FA472BB8BBF32A665DD6EB34C3FF50AE7,
	OnlineCheck_get_CustomCheck_mA389AC805D9EDD6A44881220B75F69BCBCB91BC4,
	OnlineCheck_set_CustomCheck_m7BC17170E2FCE2B52A76FA6F01507D957ED1F3E9,
	OnlineCheck_get_Google204_mEDFD7C17757ABB025BC73DA46FC446551E10480D,
	OnlineCheck_set_Google204_m00E4CFC3BB1BAEE645E291E30A491B4D7885A123,
	OnlineCheck_get_GoogleBlank_mD45D754D00A8AAE87BFC19C81589EA930BD31184,
	OnlineCheck_set_GoogleBlank_m5DD888B47344A77A3399E7370D1D902C400B98CA,
	OnlineCheck_get_Microsoft_m1B7E66BD4EC21300315AB60A8F5E585557132DDE,
	OnlineCheck_set_Microsoft_m1CDFC2558A018DE2B298DE6D49618306C83139E9,
	OnlineCheck_get_Apple_mBDE98D3F32B447CB8E5D249DA9F679993A58DE6D,
	OnlineCheck_set_Apple_mD0B3898C5B02502B9D95A94E66170AE0B934D342,
	OnlineCheck_get_Ubuntu_m9D36A591C91A98CEE47405893C0EB0AD50EC2E22,
	OnlineCheck_set_Ubuntu_mFCFFC571B714E691154C2FEF0272C2F466FB7E24,
	OnlineCheck_get_RunOnStart_m7BC9AE00A737822BFE674FC143E352A8CAB96D7C,
	OnlineCheck_set_RunOnStart_mC16B0A76398070312FFE46367C41660630C64C12,
	OnlineCheck_get_Delay_mCDCD8758BAB9EA8AF9AB5FE5EF646A2FA996BBBF,
	OnlineCheck_set_Delay_m1B9424EDCA2662E97D5115A9A4FC0A5DE4C5202E,
	OnlineCheck_get_isInternetAvailable_mB8D7ADA954F61774006B5B0CC83EF0BA926241ED,
	OnlineCheck_set_isInternetAvailable_m300C7DBB2A346EE1446CCA0C261B078F31D23AE1,
	OnlineCheck_get_NetworkReachability_mC54E5BC6FC8758EAAE21BD0FEAD5DE0AC66542E6,
	OnlineCheck_get_NetworkReachabilityShort_m2459856073AB8AF7194F43B0A24BD887FCAB6051,
	OnlineCheck_get_LastCheck_mB662D5E64AEFC884220C17267A75C630F94809AB,
	OnlineCheck_set_LastCheck_m67A510F9D0DE82359817CFE6A0996F744A8A1C4C,
	OnlineCheck_get_DataDownloaded_m4E94946086AE3150005F2BE61C03E2A4A715D17E,
	OnlineCheck_set_DataDownloaded_mFCA7AA53E70908620761FB4814D07F3442F07A23,
	OnlineCheck_get_isBusy_m3750C24E7FB4837535846BDF55C2D70884CC2A79,
	OnlineCheck_add_OnOnlineStatusChange_mE60151295E3C4E4453C8D3E317490B3C1B58007C,
	OnlineCheck_remove_OnOnlineStatusChange_m191B25B8B2F624F83CD491A5B8D14ED50C3FEC86,
	OnlineCheck_add_OnNetworkReachabilityChange_m938758CE29A4DEEFC3653C02C9BF15F43673D1C7,
	OnlineCheck_remove_OnNetworkReachabilityChange_m7D657C300452F527D4BDD45F55B0A14248F67B50,
	OnlineCheck_add_OnOnlineCheckComplete_mFB6BAEF23BA67D0BD31DF06481A1405680AE8BAC,
	OnlineCheck_remove_OnOnlineCheckComplete_mFA97D1EEFF914837E2BBF273EC72F1C3F7AC1CDC,
	OnlineCheck_Awake_mCD464F402183A94AC95E7E0CBD99E34FA617543D,
	OnlineCheck_Start_m115F993E5C0228895B8B6FC88D879B541588D610,
	OnlineCheck_Update_mA2AFBCFAA7F3EF0406B0E5C9BE45ACB29A1059DF,
	OnlineCheck_OnApplicationQuit_m0D4FECA1A72F42A1CB9039C9CC28AF09D568C906,
	OnlineCheck_OnValidate_m2C500D2B478094298F16E9EAF1547B613ACAA276,
	OnlineCheck_ResetObject_m22CB37D35D47031422A34C7E9E8255F9D68DD5EF,
	OnlineCheck_Refresh_mD683B345922617666CE12EAAFBF0F3D1D3F55B22,
	OnlineCheck_RefreshYield_mA1A4B0E79DC8083648A98F1BB2998F2B6469C10E,
	OnlineCheck_run_mD87AA95585CD731F6B9F96E2144FA4EA9E61846F,
	OnlineCheck_wwwCheck_m3CB186B15336CE3CE92F62C87806A59B94BC8409,
	OnlineCheck_google204Check_mB487FAC2EDF6B7CC49AB7D521194D04B7E4951A6,
	OnlineCheck_googleBlankCheck_m83DA4A965E7A9D70219A28CBD4660A8B27FF52D5,
	OnlineCheck_threadCheck_mCE004BA671CB3A98C5F52101B6DA707BA9A0985C,
	OnlineCheck_startWorker_m712AE1F1F6163081D7ACAA1440E80FB909C84697,
	OnlineCheck_internetCheck_m26BC7810D3F1627A1C90F0160374E8822C6D2092,
	OnlineCheck_URLAntiCacheRandomizer_m709DA7C524C0145F2E32E583926795561C507A7A,
	OnlineCheck_onInternetStatusChange_m6E9A0326368050D5C05412D9227AF50E3FBBE788,
	OnlineCheck_onNetworkReachabilityChange_m97979459943C17989F17A188F6C440E7342A4A80,
	OnlineCheck_onInternetCheckComplete_mAC0DE391C3B624FC3805A6D660734EEB50A959AA,
	OnlineCheck__ctor_mF7D9AF1A8CCCD6D26A3CCD53771DD94DD099E65D,
	OnlineStatusChange__ctor_m383226E646461948CA52E5EC011D1BE5F00E47CC,
	OnlineStatusChange_Invoke_m3EBD9294701F09EF7A01D577D2702859AE62AADB,
	OnlineStatusChange_BeginInvoke_m9073E2045630CEB708D44175DF0324C7F6A277A4,
	OnlineStatusChange_EndInvoke_mB94D496FBF1DE2A018ABFAD210A053C582B29145,
	NetworkReachabilityChange__ctor_m8B9C21490C4151DA9BBDD7FB80B8485A43558DF9,
	NetworkReachabilityChange_Invoke_m98EF3E65B2B7BE4E80E7673DAEF24B18EE45669F,
	NetworkReachabilityChange_BeginInvoke_m25F4C3075249EEF6D0170F6A62C99BF0F7B2E5E3,
	NetworkReachabilityChange_EndInvoke_m4E75DCFB26FA0EEED5A1043863FB7DA0773F44BC,
	OnlineCheckComplete__ctor_mB464E11FA5FCCB5EC1B350A671C77DD3A2F78F14,
	OnlineCheckComplete_Invoke_mA36878652CF6BBBC05C7CA3F0D58E5E98641F77A,
	OnlineCheckComplete_BeginInvoke_mEF67ACF6E81185D7B099FAE6D374EF0EB51C4CB4,
	OnlineCheckComplete_EndInvoke_mA863EAD2A2AAF17871710C0665784F81E43226E3,
	U3CRefreshYieldU3Ed__129__ctor_m0932B7AF205C1AFAC47F2B6F0B67AA4AFB84835C,
	U3CRefreshYieldU3Ed__129_System_IDisposable_Dispose_m9EFA937D9FC3AA4516183A844F32319214C5C26A,
	U3CRefreshYieldU3Ed__129_MoveNext_m31D680917E9AA2AC6FAEA3BAD5DE41CEB77E0DD1,
	U3CRefreshYieldU3Ed__129_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDEB14477B0E0486BA7DD5773A134C59B9B1A05D4,
	U3CRefreshYieldU3Ed__129_System_Collections_IEnumerator_Reset_m9F40BB9040C52254158400AA661985396BB26A4C,
	U3CRefreshYieldU3Ed__129_System_Collections_IEnumerator_get_Current_m0F62B1BADD0DAB5B954EB34F5DBDEA1F9199A179,
	U3CwwwCheckU3Ed__131__ctor_mB464818753B324E9DF2E095A7516F092E22998DE,
	U3CwwwCheckU3Ed__131_System_IDisposable_Dispose_mAC2958080A5562AF837FC380C4521D6F32A9A92B,
	U3CwwwCheckU3Ed__131_MoveNext_mD9BC71A282D662A9ABFA578FD69D1895B02F133E,
	U3CwwwCheckU3Ed__131_U3CU3Em__Finally1_m172DA4043C63C673A5294EAB9C88CFE42B8F1E11,
	U3CwwwCheckU3Ed__131_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m078EC1D91098DC6F1C9777A6530976909F538E9A,
	U3CwwwCheckU3Ed__131_System_Collections_IEnumerator_Reset_m61592DD78BFB0F4476608325FAE6010134CD4E39,
	U3CwwwCheckU3Ed__131_System_Collections_IEnumerator_get_Current_m0011E9B3464F6805E58DB9A63C700656204AAE70,
	U3Cgoogle204CheckU3Ed__132__ctor_mD56DB6A6123E95B4FB5A2DD3468C7DCAD96AB020,
	U3Cgoogle204CheckU3Ed__132_System_IDisposable_Dispose_m258A151CB942BDEFDB9F047FA0C28039BC3B5201,
	U3Cgoogle204CheckU3Ed__132_MoveNext_m0011D7BD3E4A7A238A981BA62E26AC2A24768F9B,
	U3Cgoogle204CheckU3Ed__132_U3CU3Em__Finally1_m3E6426E52D0976FEB6EAB2AC071C6A3751F613B9,
	U3Cgoogle204CheckU3Ed__132_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FB0EF6D56A3488E55A4367B308EF4119847C346,
	U3Cgoogle204CheckU3Ed__132_System_Collections_IEnumerator_Reset_mE5CDB8B60C91B9F9030380F2E98B378B16B7A63A,
	U3Cgoogle204CheckU3Ed__132_System_Collections_IEnumerator_get_Current_m55BA33D3303B0EC5B915D9E93BD8FEEA94C646E0,
	U3CgoogleBlankCheckU3Ed__133__ctor_m8F291E2D659EF39C62FC156BCA2D389587B72630,
	U3CgoogleBlankCheckU3Ed__133_System_IDisposable_Dispose_m6823EFCDFC68DDFA593DA76CE12CD88DC653FACE,
	U3CgoogleBlankCheckU3Ed__133_MoveNext_m9339E0ABC2571F3AA8CB8D3E8276C55EC9015E44,
	U3CgoogleBlankCheckU3Ed__133_U3CU3Em__Finally1_m9338289AB5925ED76BD57D4CC8D6E9AD1DB3F08B,
	U3CgoogleBlankCheckU3Ed__133_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFCB73C941646FEA8753CFC711C28E0174B474F78,
	U3CgoogleBlankCheckU3Ed__133_System_Collections_IEnumerator_Reset_mC9723822D012EDC85EC1965EF661CE135E28CFB0,
	U3CgoogleBlankCheckU3Ed__133_System_Collections_IEnumerator_get_Current_m43A1657DB7A5092AA08B859BC0404C0A3635780A,
	U3CU3Ec__DisplayClass135_0__ctor_m5A502B55D7490AD0A09C0679AFDD7961079EDAED,
	U3CU3Ec__DisplayClass135_0_U3CstartWorkerU3Eb__0_m1C2E01C116DC0840D663E83F33CB4E05C97F1377,
	U3CstartWorkerU3Ed__135__ctor_mBF48C92F9EF46F426C2CEF9BE406C4F51B2DB278,
	U3CstartWorkerU3Ed__135_System_IDisposable_Dispose_mA94CB49D92794B0E95C576FFF365B5235A94CA6A,
	U3CstartWorkerU3Ed__135_MoveNext_mE992CB14487BA2B52CADAA9CC3D90B2A4408552E,
	U3CstartWorkerU3Ed__135_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5404CDC99EF7E2A8AFB47FBCE8153A028333590D,
	U3CstartWorkerU3Ed__135_System_Collections_IEnumerator_Reset_m4FC0A6B4B6F41C7E92E85FD01D147C1167139853,
	U3CstartWorkerU3Ed__135_System_Collections_IEnumerator_get_Current_m47742F8B1A9854677AD992B38704810BE3BCFF94,
	U3CinternetCheckU3Ed__136__ctor_mD5D5AE14F1C3C982EAF77FF103C0FD283EED1C89,
	U3CinternetCheckU3Ed__136_System_IDisposable_Dispose_m712C823E815029CFFE4C449601DF8F5F87FB60F0,
	U3CinternetCheckU3Ed__136_MoveNext_mECB84DDBFE47BCFC8536C800C60607CBFBDA1AE0,
	U3CinternetCheckU3Ed__136_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA2AF9A7F386BED8090D59D5A41F18DC950D6C4A1,
	U3CinternetCheckU3Ed__136_System_Collections_IEnumerator_Reset_mD083701132826FCCEDBEE0047F6A63CC2F0B8C1C,
	U3CinternetCheckU3Ed__136_System_Collections_IEnumerator_get_Current_m90BAABE96B6E2DABE6BDDEF853DA73D2C7EF4820,
	CTWebClientNotCached__ctor_mA5C72B001DF6698A15ED61C3978743310D985867,
	CTWebClientNotCached__ctor_m8C1124A30A097F4FFC776145E1CC49CA61102D09,
	CTWebClientNotCached_GetWebRequest_mF2FB7234CE2842757828521B3CC748819188C606,
	Config__cctor_m43C1B465937A8CBBD02B3F0DE7D38E7212AD1F5B,
	Constants__ctor_m0AC084A744EC053B7AD3EC08964B425BA0F42B28,
	Constants__cctor_m522E24832468232B540F93961AF082B2AA01E282,
	Context_get_ChecksPerMinute_mCBE956B6D1A5F21A210C11926A93A64605BC7084,
	Context_get_Downtime_m41597711C34557EA586785F8F1B53DE213BD80DF,
	Context__cctor_mB85BA0900F86AA8F3F9AC32288C370A8CDAA5D70,
	Helper_CreateCustomCheck_mB3C29EDE863F494730B2C99CF3558CCAA6305B91,
	Helper__ctor_mA8A0D6CC0BE8725B52B45C3732F8E7E5D8E71839,
	NetworkInfo_get_PublicIP_mBC5ED6357183A4BAF8BC7AED4CE763ACA165C2AF,
	NetworkInfo_get_LastNetworkInterfaces_m42497C95D2C79874E3EADE637D746C363B133EF9,
	NetworkInfo_get_LastPublicIP_mFE5821ECF2B92421D5826D387AA8EBBC4FD20A14,
	NetworkInfo_Refresh_mEC08F246AD10B37F4771F9DB5A75A4E468EE43D7,
	NetworkInfo_getNetworkInterfaces_m608BE6959F202830463DF323179BFBF9686AB0A6,
	NetworkInfo_get_isPlatformSupported_m97DB368C1CF3C8A5EC217A0CC7BA0EC65B8965A3,
	U3CU3Ec__cctor_m82BCFEBBD0E667CA01566A3F8E223B3ABF8B25E7,
	U3CU3Ec__ctor_m2E7E6BB3B25589A2F51E6E963935187E8198BE65,
	U3CU3Ec_U3CgetNetworkInterfacesU3Eb__9_0_m07EB33AD1B65F90D75FE669F55F69FCF5F7711D6,
	U3CU3Ec_U3CgetNetworkInterfacesU3Eb__9_1_mABDA3454371D9793F38104E2588BE543F1835F83,
	U3CU3Ec_U3CgetNetworkInterfacesU3Eb__9_2_mB1DE918C36EC2CD61EB62C90570F47A315C3A0A8,
	U3CU3Ec_U3CgetNetworkInterfacesU3Eb__9_3_m7188A1FCC288B9E2E44FA8A482041C1B35599CF6,
	U3CU3Ec_U3CgetNetworkInterfacesU3Eb__9_4_m7ED5E717763A18ED4EC10915ED94BA6010512B85,
	SetupProject__cctor_mEC95DF42E91FFA0E68649767A1C5FB1C44DEF921,
	SetupProject_setup_m5E022ECA6395D4AF9E1E8095957FEF8AA40A8931,
	SetupProject__ctor_m97A0EBF819072E49601E251B39DE504A8A058EFC,
	NetworkInterface__ctor_m6D1F4B83988451D4CF39A1C01FF76CEE0CEE3BC9,
	NetworkInterface_ToString_mF2A79DDE1C176BC3116A48BF2E380B173617E2B5,
	CustomCheck_ToString_m6E88886B468D899018D369AE25C87E1E32DC347E,
	CustomCheck_Equals_m3DD1E324E38F9B9FEE6E481B7C4B5296BA3C9039,
	CustomCheck_GetHashCode_mBB495324566E62B663A38077EE998E9E51CD94C8,
	CustomCheck__ctor_mD1147BD78321C6386EF13680769B876F79621F90,
	PingCompleteEvent__ctor_m2350D7704AF82EDAD04FABBA78DE391919FD41FC,
	PingCheck_add_OnPingCompleted_mEA7E0B5AC5977EAF7382F1469B4FF16F11B60F85,
	PingCheck_remove_OnPingCompleted_m40B6D6EE813A7ADE10EE4611C0293B79C7198282,
	PingCheck_get_HostName_m5ADE1C02FF3D3B68EE5660475E37D7C932EFFC0D,
	PingCheck_set_HostName_m8E707DE8E864AA0B488606DE69208454F614FB33,
	PingCheck_get_Timeout_mC4B9EE925B0C10249D7E327D93CEED341AAEB861,
	PingCheck_set_Timeout_mAA9411D5E2D707AAA7C660DFB1A1BDAA9D61E375,
	PingCheck_get_RunOnStart_m61B19A7926EE59C0C20CAA1435AC8F60795B20C6,
	PingCheck_set_RunOnStart_m0F766632EFD0F499B646036E2341FD23DCC22BB6,
	PingCheck_get_LastHost_m28A8825F4DAD9E16E545CE34B9BE3163C503FE78,
	PingCheck_set_LastHost_m2FEDAEFB0E8ABDC31BE0CCD1797BFE4C1400E8C7,
	PingCheck_get_LastIP_mB8FF7A2C0226AC642FE9CFEF7D0F88D334C588EE,
	PingCheck_set_LastIP_mCC80740815D94DF602FC47F60419524A4670A6E3,
	PingCheck_get_LastPingTime_m0A9B7373550A0B6E214DC85E4810E0F492075AC6,
	PingCheck_get_LastPingTimeMilliseconds_m7288D1EC12E3C366054DF2BFFE28B5A23151B249,
	PingCheck_set_LastPingTimeMilliseconds_mE38F8911802C0358F00EFD57FF05A4FE0BC09FA4,
	PingCheck_get_isBusy_mE1481885876C2C5992FBD31F6D287C8110E79FEE,
	PingCheck_set_isBusy_m57F3210C204477EDE0220BCF94928541DEF4D280,
	PingCheck_get_isPlatformSupported_m222F90709E709BB2C8BB42B9A3EACFD89A41F00C,
	PingCheck_Awake_mC7B1B6033428B95991230B4B0F1790F122C0E6F7,
	PingCheck_Start_m0976BE20FA2AF230CA89E775B4DAD361691879AE,
	PingCheck_Ping_m952368905FEAA337729538CEA6699F9BE970E82D,
	PingCheck_Ping_mAD46DFFD6A0B69AB0F0C9FE89DB7ACA97BDA24E9,
	PingCheck_ping_m598086683E20BB9CD8864A8D7772863DD963A91A,
	PingCheck_onPingCompleted_m273DEDC157EE3387D019E09E43F57B24480DE4FD,
	PingCheck__ctor_m43EEC6DBFF7730E73BE34CACC33E525CD100F6C9,
	PingCompleted__ctor_m0E67C6006238356C1D67A60E161C7A5643E28877,
	PingCompleted_Invoke_m41476E1D660B013E1D73BD0798291C3FECD9A207,
	PingCompleted_BeginInvoke_mC338BA6051A6EF0869091AACD0A7291EF6481E1E,
	PingCompleted_EndInvoke_mE86091F28863919DE41FFB22D7B4506D8D108B65,
	U3CpingU3Ed__41__ctor_m0952AE64F05F49E60C3A7802B2D62D018B6EFBA1,
	U3CpingU3Ed__41_System_IDisposable_Dispose_mB7F8AC235AF49225E193B99003ADFD5B42B40DEA,
	U3CpingU3Ed__41_MoveNext_mC0BAA2BBD7559F6B16AD92F73589268A2830A311,
	U3CpingU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6632216B79E50B7860AEAB94FD3A015DDB4E393,
	U3CpingU3Ed__41_System_Collections_IEnumerator_Reset_m2F3E9B866A0EDB096B724B46371BC59E78C1CFAD,
	U3CpingU3Ed__41_System_Collections_IEnumerator_get_Current_m24F550144A66BCF6840855ABB5AB697CF4115A21,
	Proxy__cctor_mE857A50630A00BD5535C0124A2EEF10EBA5A28FD,
	Proxy_get_hasHTTPProxy_m1184684C993FD6988A6314C9F0829E2FE199FA3F,
	Proxy_set_hasHTTPProxy_mACFF6FC5482BF6F9BB6AAEED9015B0D9B7A72BAD,
	Proxy_get_hasHTTPSProxy_mFDB3265F542D6C9A234DEDD7C7B661FE04DC48EA,
	Proxy_set_hasHTTPSProxy_m62EF9B3308635CDA6648D27ACDB529A7DA0BC624,
	Proxy_Awake_m93422FD87C9E528FC86D4C0FC93ABD202CE70AE2,
	Proxy_Update_mBFB017B4FBE09A2057219616E7904F0D239EDCB4,
	Proxy_EnableHTTPProxy_m5346CBBE46128F0BB969C7D1C713E8269E9A4ACB,
	Proxy_EnableHTTPSProxy_m8ABFDF540FADE13996F9B5816B8ADBB9A7CBDA34,
	Proxy_EnableHTTPProxy_m55F8DC4D070E41A53D4DCF4FD717458103E765CB,
	Proxy_EnableHTTPSProxy_m2FA5216AEDEACAA2E9C17D81B228B35627D490CE,
	Proxy_DisableHTTPProxy_mEB51F5A2A937CE145484DB0951C19D3C443F240D,
	Proxy_DisableHTTPSProxy_m5F81A74F573062DEFD61D25CF93D8F4303DC31B9,
	Proxy_validPort_m99046FE9EA42599A844CA249CE80AB8CF808DFDF,
	Proxy__ctor_m591F1FA4D30CD58CC84BDCBA08EF611BF64A0A1F,
	SpeedTestCompleteEvent__ctor_mF9FF434408C2E17209E9427C8EC11209E8A6C0F5,
	SpeedTest_add_OnTestCompleted_m8D2C4D2F430B89483F385992F5FC91DEDC91DD42,
	SpeedTest_remove_OnTestCompleted_m72DE929DF607E54B47CDB4F40CD1CC3620929D4C,
	SpeedTest_get_DataSize_m641A449260EA1A889F5F76BDE102175F29729D71,
	SpeedTest_set_DataSize_mEDF3F7E4FF70F172606856D475465153D0D48DB8,
	SpeedTest_get_SmallUrl_m7E8F8AC382C89DBE9EE08D6C046B228E3E828817,
	SpeedTest_set_SmallUrl_mD8CDD777356AFD68D436741AFB4AE41F8923DD62,
	SpeedTest_get_MediumUrl_mD3FB142A5D53F4DBA126CC54A389B9BC9E7A55ED,
	SpeedTest_set_MediumUrl_m3E2E3C11F8CF48B0498E4BA54A337B85E7839872,
	SpeedTest_get_LargeUrl_m21F2F1610651AEFF32FD8957A55977309FEEB9B8,
	SpeedTest_set_LargeUrl_m8716C958CA623EFA62E05CCFA1334B3DE6408B51,
	SpeedTest_get_RunOnStart_m71A65D3F4DF2A2424A915EF6A3B3F7552C103D38,
	SpeedTest_set_RunOnStart_m8ED7FCC156D8D2CAAF2484B8A94E589378453755,
	SpeedTest_get_LastURL_m0468950BED96D6ABD0C83672B44BB85D91C161F5,
	SpeedTest_set_LastURL_m9D96892C9D99B85CBE85E9A3F9E2AFD4BA271FCA,
	SpeedTest_get_LastDataSize_m4AD20E2A05D4B7C9BBDC59739B849C3EF18EFC77,
	SpeedTest_set_LastDataSize_mEA3AD53374F1654942AE07A509737B2C80FB22FD,
	SpeedTest_get_LastDataSizeMB_m8F662E4A1D86758351B6A05DCBD41DF202C28C49,
	SpeedTest_get_LastDuration_m214130E3CFA7560BC58B191ADD7A3A0572C18432,
	SpeedTest_set_LastDuration_m25A4AFB4524531A862ACA303BB9962659910C616,
	SpeedTest_get_LastSpeed_m3AA25CA1067DC3498E40BC8A4E512C2CEA2A3D62,
	SpeedTest_set_LastSpeed_m9BEBAC7801B8E92DF84A90C9E668AE4C71613AF1,
	SpeedTest_get_LastSpeedMBps_m354E20C7A15402363DCF215C1F9A8112D0E45032,
	SpeedTest_get_isBusy_m4D7224F998875FB10633C5CE24DEBAC9781A0F5A,
	SpeedTest_set_isBusy_m1412307C9B8BC3DC728F9A563B71F7F70AF79D31,
	SpeedTest_get_isPlatformSupported_m6C6C49895799E31C2C402E2583746FFBC86A5EB6,
	SpeedTest_OnApplicationQuit_m9029088EFA403E6121B498C876A33E161C060B29,
	SpeedTest_Start_m9CD54725AA022B481F7F0B63ADDD2456194F67ED,
	SpeedTest_Test_mCDE783A81E3222906ABCA198A1E0F78475458452,
	SpeedTest_Test_m96D170092942B2AE946B9C34D9CC597C32267922,
	SpeedTest_Test_m3ECA30B9153F2DEEEE17D0B58C90AE9C56742D35,
	SpeedTest_test_m565BB9ED0A7C216AB10F7606F7F1E825791E3EBA,
	SpeedTest_speedTest_mBA814FEB9576E59A54A0CFB6BA8E62506F60AC29,
	SpeedTest_onTestCompleted_mCA217F6D80E529F0975782045727FB6E5E1BE8D0,
	SpeedTest__ctor_m7815AC94B50B0DB322F813330755EAC59A007AB9,
	TestCompleted__ctor_m7C5C41A958545480A41B18FD303B35387FA58F25,
	TestCompleted_Invoke_m3C5389932F3E6D3DE2DD543AFF716FC85C51DC27,
	TestCompleted_BeginInvoke_mAA5FA645EA00946534A0B4919BBF187BE0E42DC0,
	TestCompleted_EndInvoke_m6D2AFFB0B5F50ED41FB6D170EE06F82FA2E0E72D,
	U3CU3Ec__DisplayClass57_0__ctor_m1E48D89BC59EF3401CBA27A45B925062E86AA09E,
	U3CU3Ec__DisplayClass57_0_U3CtestU3Eb__0_m57E382A27E68E23EE57DAB2C5AE9E359DE39E6A8,
	U3CtestU3Ed__57__ctor_mA55DD118C29DA2907F4E37B2D786A5C8820E2DC9,
	U3CtestU3Ed__57_System_IDisposable_Dispose_m5E2A2F8DD8A744C30E33FD237B0022C719BF7583,
	U3CtestU3Ed__57_MoveNext_m3B22C01079302C5F8A0CFFF4328C7D244712E521,
	U3CtestU3Ed__57_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1B97A242FEAA29891CBB0A292158DDB9A2E2698,
	U3CtestU3Ed__57_System_Collections_IEnumerator_Reset_mA927C7FB7D10E03D0383229029126C7DCA117818,
	U3CtestU3Ed__57_System_Collections_IEnumerator_get_Current_m1B867BF9B0DC7EC5A88A3BCBE2D714E4FAC07097,
	NULL,
	NULL,
	NULL,
	NULL,
	SetupProject__cctor_m3C7003EB6357B6B19EFD155D16E3C903BE9F60F9,
	SetupProject_setup_m104E53DC5254FAF89F40F6E1FC378B81E5D5C848,
	SetupProject__ctor_m60A9E59152BA40BAEAAC056406362E16E3D5828E,
	SpeedTestClient_GetSettings_m369BC18051BF8B94C9E340DE2182B9B7A799159E,
	SpeedTestClient_TestServerLatency_mA48F3A012BC767C248F1052F9CD0D380676E8B7F,
	SpeedTestClient_TestDownloadSpeed_mDDB6B860DF08F516DD612D92C05A475E09DAB082,
	SpeedTestClient_TestUploadSpeed_m89C49E0EA246059ECED9C5C536B1FD1CD1DBB4A0,
	NULL,
	SpeedTestClient_GenerateUploadData_m62716BFCE2C5DC7C9552E926A86115EA5DD9A3A3,
	SpeedTestClient_CreateTestUrl_mB50804B7A9CD1BCC8E1751E17D706A9F026FEC01,
	SpeedTestClient_GenerateDownloadUrls_m2161B46C3DEFFBAE2A015999637F68FF37AEAE7E,
	SpeedTestClient__ctor_m3E2BEA1154CC0F5E734C5A4884A66CBE66373B7E,
	SpeedTestClient__cctor_mE4914823411322EE37DE1738967736CF7C42DB1D,
	U3CU3Ec__DisplayClass5_0__ctor_m67B4135F083659E51AFBF7579EB5C287663782D2,
	U3CU3Ec__DisplayClass5_0_U3CGetSettingsU3Eb__0_mB88AC654ADAE1A743C7736B1B81C9C210C43FEAD,
	U3CU3Ec__cctor_mBB3B2BA7DB77BD16B1068F5192A2D05E0B6C4CA8,
	U3CU3Ec__ctor_m484135B426AD21031336E885770C7BC6E828D561,
	U3CU3Ec_U3CGetSettingsU3Eb__5_1_mCE139BCE89006AE9F8BA82AEA93C25DB877D1A97,
	U3CU3Ec_U3CTestDownloadSpeedU3Eb__7_0_mAA4F34A5FA1AC21845FB0F82E1D1205BDF60FBB1,
	U3CU3CTestDownloadSpeedU3Eb__7_0U3Ed_MoveNext_m286443176C3DBAF50BE3AEC73FF694E869D3EFE9,
	U3CU3CTestDownloadSpeedU3Eb__7_0U3Ed_SetStateMachine_m8280F2E096D7E15C71E84AFFA607CE6026649EF0,
	U3CU3Ec__DisplayClass8_0__ctor_m84E131CB018FE90336E794DB0A0D25A84E43122E,
	U3CU3Ec__DisplayClass8_0_U3CTestUploadSpeedU3Eb__0_mDAA8CA8010940F8CE9C4FB058D59C2DB1F261512,
	U3CU3CTestUploadSpeedU3Eb__0U3Ed_MoveNext_m519E493A0AE6E4A4C42731310694C3EAE8AA706C,
	U3CU3CTestUploadSpeedU3Eb__0U3Ed_SetStateMachine_mE9DCC6B33A1AD21B4B16B4A8EA77136BC3FAE33F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CGenerateDownloadUrlsU3Ed__12__ctor_m49AA3916B0CC412A999C63471289DE67A8238189,
	U3CGenerateDownloadUrlsU3Ed__12_System_IDisposable_Dispose_mA02129B3149C89C32BC0A39E1E2D0E1027817D3C,
	U3CGenerateDownloadUrlsU3Ed__12_MoveNext_mDA3A25A47FB2A2EC6AE3EC50B8DEE9F732044505,
	U3CGenerateDownloadUrlsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m164D472018312DAB261D382BD124DEA9619B8A21,
	U3CGenerateDownloadUrlsU3Ed__12_System_Collections_IEnumerator_Reset_m339DE9C5E3ABD68C27ED2A355B846F7CF0809439,
	U3CGenerateDownloadUrlsU3Ed__12_System_Collections_IEnumerator_get_Current_m7BD616CEEFFE24B1D93B2873127B3F575201EC5F,
	U3CGenerateDownloadUrlsU3Ed__12_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m5945E3B01D1CAB268C7D1C10D4C980CDC2D9ACC2,
	U3CGenerateDownloadUrlsU3Ed__12_System_Collections_IEnumerable_GetEnumerator_mE0880AC1B187148E1F1EF59C10DC320FBA22E2A1,
	SpeedTestHttpClient__ctor_mCDB1FB90F02A41E1C6585CBC6D4C3ACA02244769,
	NULL,
	SpeedTestHttpClient_AddTimeStamp_m13D5AB7CE6134C251468C409E2FD00CC5D12AB01,
	NULL,
	NULL,
	SpeedTestNETCompleteEvent__ctor_m85379887EC494DBC59B4B652AFCF56D5A559B889,
	SpeedTestNET_add_OnTestCompleted_m8FDFDE70C23620C1E96C5E02391957C2A6D15E94,
	SpeedTestNET_remove_OnTestCompleted_m06533154A729B79608F982FE1EA7496E26EB2D75,
	SpeedTestNET_get_TestDownload_mF30058D092504C697520DB4E274F683F8305A90C,
	SpeedTestNET_set_TestDownload_m7E7C593DA039217B186148BC368D2E0D9FD8AE11,
	SpeedTestNET_get_TestUpload_m2206D6E6DBF39F07965DEA38900DFEC9E3109C4B,
	SpeedTestNET_set_TestUpload_mCF3A1B346A2061F893552CDD3BB91608404C0A51,
	SpeedTestNET_get_RunOnStart_mDB55C37FC8691F223A75F1282850CD0051DE7269,
	SpeedTestNET_set_RunOnStart_m3F9B4EB24E843380C6D16A6D78B6F52D3D288D96,
	SpeedTestNET_get_LastServer_m4160DAEB0C71670367B0A377D484269F6FED2D9D,
	SpeedTestNET_set_LastServer_mCBE6FCFCD3555A245B7B2B15525BA94C408DAA4E,
	SpeedTestNET_get_LastDuration_m66A52EA200CD945E4144147B034FFCDAF8D5209C,
	SpeedTestNET_set_LastDuration_m1FE25D3FE39015C277211676D4EE0D9219699267,
	SpeedTestNET_get_LastDownloadSpeed_m9B8E22094FAD2A38D52AA9474E4427F9444D7FC3,
	SpeedTestNET_set_LastDownloadSpeed_m2DAA6161A0EA820A78C3F3412D03B627BCDF5307,
	SpeedTestNET_get_LastDownloadSpeedMBps_m7099551A6E4C472D586C0B379A9FAED0C4C4646E,
	SpeedTestNET_get_LastUploadSpeed_m908040E942913DEE63BD79667029A41F142979A7,
	SpeedTestNET_set_LastUploadSpeed_m113E57EAC1E26360920F3C43D54153B7F3861C40,
	SpeedTestNET_get_LastUploadSpeedMBps_m2011E085A9066EE7CFE5A12A992848CAD9932175,
	SpeedTestNET_get_isBusy_m9B3DFCD781D218502F70A677C4DCED47045B6B6C,
	SpeedTestNET_set_isBusy_mF0A5FAA978A26916776D32EE3335A56B2F615A2E,
	SpeedTestNET_get_isPlatformSupported_m3B226C33EDBF214225C484E477B8AAA8DAF2F8E6,
	SpeedTestNET_OnApplicationQuit_mC453D0B9326DF769A803A0A9AD710C4BAE89C9E2,
	SpeedTestNET_Start_m70220C42F23E8CDD5395A8A32CC9067BAFD55727,
	SpeedTestNET_Test_mA584D170D56791DD95986E3AAA17C0FC50414ACB,
	SpeedTestNET_test_m42A569CAF7BC7316E7E71F88EDFB21976E4E837C,
	SpeedTestNET_speedTest_mC6C3DCBE64A44003BA1320525B624E7F16B2D6D8,
	SpeedTestNET_selectServers_mF1E09D9D2B7A9C89BDABCA6E0BD92436AA157700,
	SpeedTestNET_onTestCompleted_m774400D99DBC816313A4F39B6E5CC5E5000C2E0D,
	SpeedTestNET__ctor_m98F9F01490F9B2840D27385A3F56E197756A2612,
	TestCompleted__ctor_mC9882303996B32C209E4F9C86767ECC9DE1FE7B3,
	TestCompleted_Invoke_m65BD1B19C3D4045FF2EAA3D44937CEBB73B777A7,
	TestCompleted_BeginInvoke_m7BD0E4C9926E12D894927F4264A1213C86CFC0F3,
	TestCompleted_EndInvoke_mA1891114334673290098309578FA0498532921CE,
	U3CtestU3Ed__49__ctor_m84BDA458D061319259587AAFA767FDA0512FEFEC,
	U3CtestU3Ed__49_System_IDisposable_Dispose_m01FFF33E02E0F10C094A89BEE1BFF4462E4894DE,
	U3CtestU3Ed__49_MoveNext_m6AD7DC40D4AA145C5C3CBE4AE1E6062FE0FBFF9D,
	U3CtestU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5169CE1D6561EDC30A2EEBB1E1B9C3C0FBA4AF3A,
	U3CtestU3Ed__49_System_Collections_IEnumerator_Reset_m379FD9E7D91CF45ED29222C3EE208D03E4A50604,
	U3CtestU3Ed__49_System_Collections_IEnumerator_get_Current_mDE60D7E8FFDAA8163CF42DB919A8DF30A5B5669B,
	U3CU3Ec__cctor_mD2C1CEB5BFD8B887701A72F2F6EC0FD463D5ADC2,
	U3CU3Ec__ctor_mBDFDDF7E4DEC488FE41C71839D366D0C431F944F,
	U3CU3Ec_U3CspeedTestU3Eb__50_0_m1C22FA8F8FE7D6DC8AC033C8C07F863DB40A1695,
	Client_get_Ip_m0B9445853B5D4199CE94F7F5BD0241C1251B77B2,
	Client_set_Ip_mF873302D8A710CBDB05FC4388D3AD3D9A09D0A76,
	Client_get_Latitude_mADF069565C2C60552BD35CE0AC10019B6BD1AF2D,
	Client_set_Latitude_mD8FAF274E3638BCFECAD9340117F4E240EBCCCBF,
	Client_get_Longitude_mE4C0B3E531C0B17AFEF3C737E378852FE1AA03D2,
	Client_set_Longitude_m190D851D72EF6D9470D1EFCF6242758CFB074F4E,
	Client_get_Isp_mA92D7910AA592D926174B851DA714BC44B60631D,
	Client_set_Isp_m649133295A21EC49717640BC84170602CDB3C69A,
	Client_get_IspRating_m0188AD85C4C73A2719515B1603FF3DBDACF10B09,
	Client_set_IspRating_m2B96661EB68F8B4049926304C28B07A00F561BE4,
	Client_get_Rating_m88C44AF1AEAD628F6D49D59DD02141E944BD630F,
	Client_set_Rating_m49097F8083162E99A5B396F1441C392F54911DA3,
	Client_get_IspAvarageDownloadSpeed_m32ED04BF66402C1B0BDA39775E56BEAFA4194E8B,
	Client_set_IspAvarageDownloadSpeed_m5AD00BF1E8F2691D70DF57173230F442794AD4E8,
	Client_get_IspAvarageUploadSpeed_mC605999904C63FC1EDFC297DAF55E9E53B0E5236,
	Client_set_IspAvarageUploadSpeed_m109F22774BA6E37C4847E51DDCC5774B1FC38C5D,
	Client_get_GeoCoordinate_m5A1B0752D985D5B1E1939056EB36CBD95CF632C2,
	Client__ctor_mAB6496DCEFF582A0A67AFB137D23EBE4D332CECE,
	Client_U3C_ctorU3Eb__35_0_m5116CF0671C518DC2DAABABAEBD16AB801DF3B87,
	Coordinate_get_Latitude_m8D07D3A4B6CDE856EEE577B3E8F1C7A3B169CD65,
	Coordinate_set_Latitude_m1ACA8FA6F57F247E89D8F7CEF7EE20F2370C1705,
	Coordinate_get_Longitude_mAB87DACDEAFB05CE4CE6CAD0C447F8D56C29E130,
	Coordinate_set_Longitude_m16230727D76BB3EAF9332F7F26D202AC2E9E5CF0,
	Coordinate__ctor_m0886EF2747AD15E2DFF55A667954660184F4F780,
	Coordinate_GetDistanceTo_m102B52E0A341913C08DD14FD0F0EA7E55A1797B4,
	Download_get_TestLength_m289664666D0F5DECA0D41435591FE75CD7295E09,
	Download_set_TestLength_mFD53362225A0BC710E1CC989047F7FF027214463,
	Download_get_InitialTest_m808C4F980B4198E5882F23BBD6DDB59C040FEE35,
	Download_set_InitialTest_m08BA6DA96AA31BA256EEF1530ED93E250A397E6D,
	Download_get_MinTestSize_m3DF159A73D260590CA6906F5E3A4D7BB90B819C4,
	Download_set_MinTestSize_m641B5621AC012890DA21BC159DDDBFEDFC2A449F,
	Download_get_ThreadsPerUrl_m4D34349486D1BDFB271D6BB1F9297F3EC6348A1E,
	Download_set_ThreadsPerUrl_m709C09E224ADF9252B9B55760E627C27A3F14941,
	Download__ctor_mB11758016E4D4CD64566E0F54B3AD9BC49EEBB9F,
	Server_get_Id_mA2999C7EC3409CEDE72BB7C8B5364C4EC352386E,
	Server_set_Id_m019851DDEF130546B9918509A8A181ECD9FEA4B4,
	Server_get_Name_mD18CBC3EE46C7D0AD38FC614D8A46D2B9ABAA1EA,
	Server_set_Name_m17AAFBAC7179265AFC6E0880EB6EDE0B05D41F99,
	Server_get_Country_mCC14ED05AC9FBAE8811B173060C20C36642DBF26,
	Server_set_Country_m59CF2DCCBA4737CA0DC26F43B1D2B2900ECD5A18,
	Server_get_Sponsor_mCD97CBB658EC7C8B376D879B80898E08316AB7A0,
	Server_set_Sponsor_mA8CEE4B9339434697F9A1366F44A0DBEB68BA970,
	Server_get_Host_m71A2D38CB45DF2946191BED1962F14DBD0955E22,
	Server_set_Host_m7303208597252E6E186421C523F744CBB3A7DEB9,
	Server_get_Url_m660B03D65EC132A3EB811035498667CD9A4CABA8,
	Server_set_Url_mD827DA9B0FE13649BC4EFFA65B8FDEF7ABA20F59,
	Server_get_Latitude_m8B22A17C87780AA0F9744C2728D9622BE2887A46,
	Server_set_Latitude_m77AE62B1568B46095196B41F4B94B2EB588B052E,
	Server_get_Longitude_m097F70F6D4388E7715674D4152CAEEBD74777701,
	Server_set_Longitude_m22F4D3DE5E0D31B562D4DDE6BEA2302C97CAE77D,
	Server_get_Distance_m086243B6C27057388CDBF0A5C2855FFDF0601908,
	Server_set_Distance_m2994F70C5A3B5667D63E0E26514F9B0D3CB0D6E5,
	Server_get_Latency_mA4CAB54DA41B8EBA4584559614C72BC412A7AD6C,
	Server_set_Latency_mE1C4F2D76A7A9D7DEE6907001C029275959D1D37,
	Server_get_GeoCoordinate_m44A1A3C557D32C9DC9CEBCA8F73DE0D5A11CDF1F,
	Server__ctor_m2531A1696D2F74D276057157FD1F2EA9FF1940C1,
	Server_ToString_m9B9B60E0EC1FAA3468462F1306D5930287E9DFDB,
	Server_U3C_ctorU3Eb__43_0_m11A8527A9A2A89C54099A241207BAF066ACEB075,
	ServerConfig_get_IgnoreIds_m8AD90272174C1B00F09FAB914D423BAC48B03B70,
	ServerConfig_set_IgnoreIds_m74CA68265E0D0F82345713DA8A75005ABFE6698D,
	ServerConfig__ctor_m159B08799616725568118075C72FE852458E1DA7,
	ServersList_get_Servers_mA728E4CD054E33150481599D265769246D2BE398,
	ServersList_set_Servers_m90E26C0707F47E96012430ADC9A5940AFE0E101C,
	ServersList__ctor_m0151113C77A48B7E78E80F20E46EAB0C7FCAB48B,
	ServersList_CalculateDistances_m6AFAF7597CA9D3E4073450B8A27B5BE4C6011D37,
	Settings_get_Client_mA491884009E1D77B12B2BB69EF9403E85BC5075A,
	Settings_set_Client_mF3BFDC0946EB9C6C7C7718C07AFD36A364604A05,
	Settings_get_Times_m8C7E3E2A7EC879BFFF31552B55F9365771DFDB53,
	Settings_set_Times_m76A004A32676C30B0A6FE86686CEE58D8444504A,
	Settings_get_Download_m946D691794AB8B057BC1A7BA72CD48421DB76CF5,
	Settings_set_Download_mC4B696297672244EC829C93A761FA1466DA34A51,
	Settings_get_Upload_mF3ADADA4BD5F74A9FF1E4B9C76FF6084DCE9FBFD,
	Settings_set_Upload_mA7FEFA826C900B4131D2E90C4DB3CF79C7907670,
	Settings_get_ServerConfig_mCC41E378E34CDF96D220C26580252358502250C5,
	Settings_set_ServerConfig_mD35731AAA2D23F0164EEFB2CF28B21E633C20871,
	Settings_get_Servers_m32663154FB14571FAA25BE902138A04DEDE73225,
	Settings_set_Servers_m409CAB6BF32ACF58F53120805684366D042A75C7,
	Settings__ctor_mEC68800AD1332A4417728B9A98015408AD617F4A,
	Times_get_Download1_mBD1281097BE6FBA5F56F81A9FDC0890CFF072A83,
	Times_set_Download1_m9FB2ACE46B9DB28FCBDE6943A0356B5F4DF60320,
	Times_get_Download2_mC71E858A945784AC0B3AC13C3D626783AD9F75C8,
	Times_set_Download2_m2E8D0CE21DF400DDFCF33298CB408CF786A988AA,
	Times_get_Download3_mE0F35479045CA5961E39FD77D5BC17BB4E399863,
	Times_set_Download3_m43AEC1ED6C046DE2FDC8D266D5AAF908916EEE23,
	Times_get_Upload1_m23B4FD1C9029FFA70E84306928F68A33759A9ED3,
	Times_set_Upload1_mADE25B3FCC8FEF183C95BBC0C956BE32EB2ACA9C,
	Times_get_Upload2_mCEA15219828984DDE6749ACC494CCCA07DC6EFB3,
	Times_set_Upload2_mDF56B9576186F2BF6893072BC281F25788E409ED,
	Times_get_Upload3_m33891A4C1DB3C988A09EFB43EABFC38DE4390924,
	Times_set_Upload3_m57A585D4E9D371109D318361B5949E82E1B9121C,
	Times__ctor_m94E40C91965571BC4C4C2514B73023A138FBF0DC,
	Upload_get_TestLength_m01A66839C2D9D5C7DD1FDA014DCBFB80DBB23731,
	Upload_set_TestLength_m0E77363058F1C6C9DD543BBE2820DA78013B0080,
	Upload_get_Ratio_m37A83FA6FE6D9A815FAAF8E08840C53EAE29DF44,
	Upload_set_Ratio_m91BBBD058C120E6E919AB9843DCDD74087C3B20D,
	Upload_get_InitialTest_m5A44B984EB443D040BB5067823424FF774E1AE68,
	Upload_set_InitialTest_m6461F782AA38863779BD2ED35B0A01A4B38986F6,
	Upload_get_MinTestSize_m50788DA9D8ECC84CC142274094C16D40E2C2C8CA,
	Upload_set_MinTestSize_m611A46C9FF55E0E2C7326DB31F57731B7A03A8EC,
	Upload_get_Threads_m9F2C1842B9753DDCC1BC351475292DACB8264696,
	Upload_set_Threads_m80F21E7FED558565879437B55F70DF334C3A7F11,
	Upload_get_MaxChunkSize_m19E1298ED0FD82CBFF4CE9EFC434F20AE6E2C8C6,
	Upload_set_MaxChunkSize_m47EDCE56D373ADCA4FCDB9B59DAD53EA9AFF0705,
	Upload_get_MaxChunkCount_m4C6E81A8FA8812934006CFDF4A623FA3669FB11E,
	Upload_set_MaxChunkCount_m275B9BF318D72FE0202F869C819020B7117D8A69,
	Upload_get_ThreadsPerUrl_m66AA71DF8D6469F1BE6FCE1D1976ED0C1894CC71,
	Upload_set_ThreadsPerUrl_m775BDBCDA5697DC1D6E7A2C1282424FCEF597507,
	Upload__ctor_mD9FF71211EA19B10EA178CB46C33CEC63C5197EE,
	EventTester_OnStatusChange_m92A2DE0F6CDF321F787CF96788E8E6F4FD65ED55,
	EventTester_OnPingComplete_mC534A30E8506911949406319A54BEE8AB25E7F8C,
	EventTester_OnSpeedTestComplete_mA75C7ACABBE341115F5CE60DC4C88E4A0C46BB0A,
	EventTester_OnSpeedTestNETComplete_mE5991E09850A475C6C9D8EF45A8A0F3CA520C726,
	EventTester__ctor_m6B3B2AEA1402035CE85EE77BE76298E3B8263399,
	GUIMain_Start_m19E15972FF096F019331693332755C389BFAF4B5,
	GUIMain_Update_mEE0C78DAC0DFB1E6AD1EBF2662FB5711B1D96C42,
	GUIMain_OnDestroy_m725BC03195678761AE8187783799CA81400A0637,
	GUIMain_Check_mE38A767A8928F398D7A317FC3A4C3D5D6946215D,
	GUIMain_ChangeIntervalMin_m12AB6190F5E5178D59585A4E66B66BF9FFFE3650,
	GUIMain_ChangeIntervalMax_mB3EB7AD5D0AA934BD07D70DD5054A59FBF12BC4C,
	GUIMain_colorKnob_mF2A2E9E1EA43EA9F985CDAA6696A552BE20C411B,
	GUIMain__ctor_mE16B71FA337D24AC53E8B39EB21C292C1B00AAD8,
	GUIMain_U3CStartU3Eb__21_0_m7297BA87941213DE42946C1BCB2FB56B5A8EDB2D,
	GUIMain_U3CStartU3Eb__21_1_mF4C6CF3E35C104AF588FF18D86FFC124FF4425B1,
	GUINetworkInfo_Start_m417E4739EA1556B667D9274F76A156B3C9E2A2EB,
	GUINetworkInfo_Refresh_m2B0E176DAEC16BFD7C3679672C9EA2A81C13D4CF,
	GUINetworkInfo__ctor_m8308778EA563E8DAC1DF644D1EFB2B3C994C7E40,
	GUIPing_Start_mE7DD2821034140C87ED14DA790CA50F487BC1B4C,
	GUIPing_OnDestroy_mC826DCC781B70AEAB335D9D7594510FA61254CA0,
	GUIPing_Ping_m8DE5CD76E790EB2E72BC2EE1904254393C58157F,
	GUIPing_onPingCompleted_m42A318934F78F0CB8C387D711F42D4BA9360095C,
	GUIPing__ctor_m87910288450F0CB621BD40CB8CFB2431DA9B286F,
	GUIScenes_LoadPreviousScene_m6EF52A8494DDA9FCE39BA5B8CF014E54ABFF952E,
	GUIScenes_LoadNextScene_mA365BABF7F7FC0852941A7C826C90A70DEBE8366,
	GUIScenes_OpenAssetURL_m805BD8ADDAED2F9A24A82EA8B93EDEEB8D99F994,
	GUIScenes_OpenCTURL_m49C3A77364AC9F8841814367575EEB73B1770A0F,
	GUIScenes_Quit_mC5A7F8BE47789E7AFC64394B5C64E0D2F420093C,
	GUIScenes__ctor_m635CC7956C21D8163F74030332A3A8CF6D85C9E3,
	GUISpeed_Start_mA5E332EE86234D0D1D3F4D0474277F4D9B9C0173,
	GUISpeed_OnDestroy_m33711AD892C2233A59ED1E326DD591ADBB7A1638,
	GUISpeed_Test_m521FB9AFF57B43835F5DA86BFE3AE48FEB6EB99D,
	GUISpeed_SetSize_mB9E9FC32A128143DA6202D16A101C6D4EBC1FBD4,
	GUISpeed_onTestCompleted_m650004CA3AF714868FD9F7618FE61841B1A8166D,
	GUISpeed__ctor_m953BB7C61D21D4DAFAEC5E31EEFE2820B03CEB78,
	GUISpeedNET_Start_mFE74B8B414889441F6962182664FD7806A67C908,
	GUISpeedNET_OnDestroy_mAF1B82B4A340209FF036D0E2DE5BED1A507ADCD6,
	GUISpeedNET_Test_m50C6E38A314A8627A43A60540A6E9B323324CF01,
	GUISpeedNET_onTestCompleted_m110FA8E44F1CE37737F1F9885071D8A6B71DA73B,
	GUISpeedNET__ctor_m4361DB1207F1C63291C604F9FA44AF211BC91926,
	ManageEndlessMode_Start_m7D3B02F404CECCFDD05584006C2B87CFD3079CC3,
	ManageEndlessMode__ctor_mB7C28FEE5ECED92C96707FCFEEFD3E3C9130BD74,
	WebGLCopyAndPaste__ctor_m9ABA4FDD0719BD76811EC9D6731B127B4DB05D07,
	Social_Facebook_mC2E09264868CD8006AD4D69B817D855488422A7C,
	Social_Twitter_m492C7F14348366FA7719E69749DBB30A76DA3B8A,
	Social_LinkedIn_m69D1E11EDE1C1748E8ED8463D54DD59513371D94,
	Social_Youtube_m5C4E721503773BADFE0544DD381114729E7F31A5,
	Social_Discord_m5C364639DE37DCF942CCF9A29944B535E5E39EC1,
	Social__ctor_m5661EFE8FEE7EC2324FDAA88191E3B5C6F284F67,
	StaticManager_Quit_mAC39DA93EB2E5BA9D210BDE9C23A09A4D4266504,
	StaticManager_OpenCrosstales_m5E5E7FC3F7DE7D9ED7330C6D4B60E5A686E9F5BF,
	StaticManager_OpenAssetstore_m949B8AD95A36C1364BEA2416A9E32D134340890F,
	StaticManager__ctor_m745654B0948B4DF9A2FD0FB65806A03164ABCC65,
	UIDrag_Start_mDB1DCDDEB984EDD5DF799896F85B936BFB18D54D,
	UIDrag_BeginDrag_m583EA2730DACC92BC96F551DABD1FBF04EA5FF8B,
	UIDrag_OnDrag_mD59F3EA41622E034504FFFB95F247C6CA398BF2A,
	UIDrag__ctor_mF4C53D49822FE9B02461E092AE6F7AC45A011113,
	UIFocus_Start_m880CB1FB7349D96ED9EB3CF27BE27A16BD2E5F24,
	UIFocus_Awake_mADE277AE5D1040D6968B5C9E59C422C3066A6A8C,
	UIFocus_OnPanelEnter_m33CF9E28098755A82CE6764377DF302C10F71973,
	UIFocus__ctor_mDEF7E588D05EBA2BF8ECFB87875BCD5A614829A0,
	UIHint_Start_m96229DC6152E6DC547F659396A38AB349084473D,
	UIHint_FadeUp_m9D94298919C4378BE30465D8A82923CBA316D3C1,
	UIHint_FadeDown_m3C90D1A432527CE936339156444BD7D108D729C7,
	UIHint_lerpAlphaDown_m6FEC2A6BF3503E0B3046CEB9F4AE8CB7FB05C46C,
	UIHint_lerpAlphaUp_mD0790C5B2C87FFFB07F78F0E098C08FFBFB70246,
	UIHint__ctor_mD54D6ED200DAA952DD7D97643AE270D572902BCD,
	U3ClerpAlphaDownU3Ed__8__ctor_m137125C8737CD01C1A17E318878AFB2B0D0CDB14,
	U3ClerpAlphaDownU3Ed__8_System_IDisposable_Dispose_m82EA5F02C54838CDFAD7E3C549962D0DB1151FE1,
	U3ClerpAlphaDownU3Ed__8_MoveNext_m954DF2644E926149C6AC6AA1695D54705969FCAE,
	U3ClerpAlphaDownU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB42DD7F0CDC10AADCD99116A14BF77D16197AE49,
	U3ClerpAlphaDownU3Ed__8_System_Collections_IEnumerator_Reset_mB4243BFE9BA869109D22485266636F346DE3495F,
	U3ClerpAlphaDownU3Ed__8_System_Collections_IEnumerator_get_Current_m641305F2EB99B80E211B4E6E99E859C2B7AAE63B,
	U3ClerpAlphaUpU3Ed__9__ctor_m41FA43BCB2D14D39F0DC1E1AFFCECB0D18E02419,
	U3ClerpAlphaUpU3Ed__9_System_IDisposable_Dispose_mD5454F31F93F6B5DCCB420AB1241DD2E2DEE331B,
	U3ClerpAlphaUpU3Ed__9_MoveNext_mB9C62184F82A3D1EF71E0A03CE960D9F020EA556,
	U3ClerpAlphaUpU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07EF2B75C53B187A10E0ACC00AB20DF4CAB27CD7,
	U3ClerpAlphaUpU3Ed__9_System_Collections_IEnumerator_Reset_m1C29B48A4BFBB4956CB59DAF52A587455998D965,
	U3ClerpAlphaUpU3Ed__9_System_Collections_IEnumerator_get_Current_mA92CCF9323E6BC3E4B224B1CBF4098D02978B980,
	UIResize_Awake_m84548F0C66BF42D9B3D85FDC887AF9F592314470,
	UIResize_OnPointerDown_m1E7B045EF724B9EFEF997DE80322BBBC494E202C,
	UIResize_OnDrag_m8F2FC62C5CD268AA28FB7B2EED64DDCA2079BB9E,
	UIResize__ctor_m4B0D088874E80F5BF9D3321949439328C8BA3744,
	UIWindowManager_Start_mFE0D244F094A34B2D47F20D9DA45C19166630F1A,
	UIWindowManager_ChangeState_m98045ADD1B1A1380FF64496BFC4756B5F1051C59,
	UIWindowManager__ctor_m12928DA297937399EBC6FD9D27BEEF646E068ADE,
	WindowManager_Start_m1AFD4847D6D468A27278A2D72509D0D28362A7B5,
	WindowManager_Update_m17970A83622480894EA2395FF8A4C5560CEB8115,
	WindowManager_SwitchPanel_m6B16C8B9EA436D051DF3D864DD4C97B915F4DF55,
	WindowManager_OpenPanel_mCAACC7E648449EC4F47E3FDBC730E00B319A680B,
	WindowManager_ClosePanel_m5C051458EB873C2A040A526707370BD675CACD21,
	WindowManager__ctor_m7A48D60A914B455DE9A65EF38F6718DD9E92B782,
	FPSDisplay_Update_m3DA30CE583BE8D5C3CF67953C12F519B237E77B5,
	FPSDisplay__ctor_mE69C95D9F1718F2320861BEDD4AE137497BE4942,
	ScrollRectHandler_Start_m0D04331DA1FFEA8B60FC0ED473CAD5E51613B8CB,
	ScrollRectHandler__ctor_mC6321DE1AAC54297ED22ED0319EB34682DE8DF12,
	AudioFilterController_Start_m023E8A3B5C26AD20DD95401CFAB4F1ADE22C5FDE,
	AudioFilterController_Update_m3BC6A60259FA43DBDE01104A8702CAFD6C0672B2,
	AudioFilterController_FindAllAudioFilters_mE584B35EC62589392B26052788D929FAE0077E3E,
	AudioFilterController_ResetAudioFilters_m8143029366B8B9F1D02DF851A16B140A0322D4D7,
	AudioFilterController_ReverbFilterDropdownChanged_m652A3031CDC599A6ABBAFDB9191BCAD9D07CD705,
	AudioFilterController_ChorusFilterEnabled_mA275519F7D4A16CD4FF99698A461680D3D76C28A,
	AudioFilterController_EchoFilterEnabled_m2AFF8AC9034A6335B2DFA78AEF2C45932DC998BB,
	AudioFilterController_DistortionFilterEnabled_m350F8A3B6DC0B0DB58BF247F8B9DE590D765F212,
	AudioFilterController_DistortionFilterChanged_m461E2E9B59017BA19A54DD364804871920DBFAFF,
	AudioFilterController_LowPassFilterEnabled_mF4170BEA6788FE1C5AA52D23DD270CF28A865AE8,
	AudioFilterController_LowPassFilterChanged_m468A4ACAFBDA9DBF84E05D1D1B52695EFB93ADB1,
	AudioFilterController_HighPassFilterEnabled_m8289B95060189959E53DF7BA5C5F912EFCF8673D,
	AudioFilterController_HighPassFilterChanged_m6999A3F68779CB25498CFC9C74FBC6E302C89C72,
	AudioFilterController__ctor_m0ADDFC812BAF558E74733A8982FB517F0AF88BA8,
	AudioSourceController_Update_m151D413A0028DC4DCC04AED331760F39FD75FE01,
	AudioSourceController_FindAllAudioSources_m7274214CC9FDEDD31530D475D242DE09509C3B7D,
	AudioSourceController_ResetAllAudioSources_m8024212FF7FD895E3E65A9FF3F522D9C8C91349A,
	AudioSourceController_MuteEnabled_m5482448F4EEEF3E25D8D6A2A47CCE74921D04D9B,
	AudioSourceController_LoopEnabled_m5678F2214274A8AEBE5F5767C2B160749A9AB7D5,
	AudioSourceController_VolumeChanged_m128B79F2ED89F6534425EFB70B9948BB9D621E6D,
	AudioSourceController_PitchChanged_mEFDBBA929C79B9CDD4DE9404FBFF6C763F4E3459,
	AudioSourceController_StereoPanChanged_m05093FA99C5C1E3C34B84E8E4972BD10AAF86040,
	AudioSourceController__ctor_m01BF58E50660DC7A1DC6252D21678449766DC54A,
	CTScreenshot_Update_mFA432D568E042B0FD7043DA0546D1BAB1B73BAF3,
	CTScreenshot_Capture_mE9EC74BDDCDE0B1B523E52660D6E7F4AB4A5CF16,
	CTScreenshot__ctor_m77D46CC657F1B4D7383A520431D43B66078BDAEF,
	PlatformController_Awake_m546EB944C79655473CE0328872609FC3F0C39910,
	PlatformController_Start_m45C32ED17D6746F9DC0C139B05EDC89CEFBF0C3C,
	PlatformController_selectPlatform_m44CD923C6E139A6DB86AC3D1BCAE13099EDBCC3B,
	PlatformController_activateGameObjects_m625D36196A5A24CA594A9ACF9A7FEA9FAE45389C,
	PlatformController_activateScripts_m0AA8240F5DEC792EC31FE78B4D642664ED5B076F,
	PlatformController__ctor_m246265302FC8CBCE52B8C4DB078CB7B5593B4FAE,
	U3CU3Ec__cctor_m8FB2EB90255FD11CD4C37EF7142156A9234A45D9,
	U3CU3Ec__ctor_m1E5F842F54FA495A8CD30B47EA5279A11B78913C,
	U3CU3Ec_U3CactivateGameObjectsU3Eb__8_0_mD375A3DE0420A1CBBDE8B0695E56FD9F058F8AC8,
	U3CU3Ec_U3CactivateScriptsU3Eb__9_0_m40E2FE8F4B779E265F42B10B6A5EA0288FE67621,
	RandomColor_Start_m391CF84B08071FF3ACB0963EB28D2730BAD3F21C,
	RandomColor_Update_m80AF1317FD5A6E02EE266F7916C32167C811A267,
	RandomColor__ctor_m9D12F994D18E58B4E6917402BF320A05B4C07D38,
	RandomColor__cctor_m5AD1035B777C4BE87CF66D0D0512337CF39E1069,
	RandomRotator_Start_mD8CAF4274B1003904752C232D60FF6D740AE46D8,
	RandomRotator_Update_m2B257E670DEC91784B8CF373497F18B3B88FBEA5,
	RandomRotator__ctor_m5B8F4189F25BED4B6E89D524DA329BF1F641416A,
	RandomScaler_Start_m846A6856675D0177F6879AF36DB91392267F17A0,
	RandomScaler_Update_m7DF95D0941A662AA475C77084EBA89A60C0908B0,
	RandomScaler__ctor_mF4467ADA2B0DB1F1B451D3568A5CAF22042121D7,
	BaseConstants_get_PREFIX_FILE_mF05531A4738FEE2B43B56F613C8543B56677A58C,
	BaseConstants_get_APPLICATION_PATH_mE87E6E9FF38895E8F8E1D78DB610DD417DDC85BB,
	BaseConstants__ctor_m423B5C856F2C469942209CF30A993C3BD4A969E3,
	BaseConstants__cctor_m08244E973BC852F1CD2C3F222AA017980377C085,
	BaseHelper_get_isWindowsPlatform_mA89734268B9F4512EED642DF4FD30718BF0698DB,
	BaseHelper_get_isMacOSPlatform_mC7A38F4150094D9E1C539292EF669C43FA2567B0,
	BaseHelper_get_isLinuxPlatform_m417DC75B6784EF54E5B5BE01CD05705C3BD1D7DE,
	BaseHelper_get_isStandalonePlatform_mF704F98AD14E6122F66B2A8AE7AC406129FEA067,
	BaseHelper_get_isAndroidPlatform_mCE5CA1352C1F80F40F41A29BF1C27292E840394F,
	BaseHelper_get_isIOSPlatform_mC37175C891506DEB337DA607BC64D69709286A75,
	BaseHelper_get_isTvOSPlatform_m9875AE921ECCE6CC71E9839BFAE77AE234876AFD,
	BaseHelper_get_isWSAPlatform_m6C69C0C0891C3113A253E643E0821F65BD87B5EA,
	BaseHelper_get_isXboxOnePlatform_mC6F3A25D6E8CFC89ECBA413420C9B8AC9BA4BAD9,
	BaseHelper_get_isPS4Platform_m0E05F11A7177556AB547379BF883888973A5BD6E,
	BaseHelper_get_isWebGLPlatform_m33211FB09A47645AA051EC76948852BE0025D0EE,
	BaseHelper_get_isWebPlatform_m5A3E8B17AD90A821B6DC419FBECC6CB800ECBFB3,
	BaseHelper_get_isWindowsBasedPlatform_mF5EEC2933706EDA1EDFEB2F1FD8E3FE8A41431A5,
	BaseHelper_get_isWSABasedPlatform_mA559B2E3E0D1646C0C427B113909B963B3D40A74,
	BaseHelper_get_isAppleBasedPlatform_m831F2787A6CD87908977B20FB1F2233D84D17E62,
	BaseHelper_get_isIOSBasedPlatform_m5DAABF1EC5DC3B8947CC4B636EB59C8AD81C4A14,
	BaseHelper_get_isMobilePlatform_mC99F5223DA8B271FD45AAA36B48163FB2C0F3B7C,
	BaseHelper_get_isEditor_m0454F7BB9528D9AEEEA24C95BC322A44839DF874,
	BaseHelper_get_isWindowsEditor_m1ADAFD1FB58136C7BE74018F625BF8C9ED655DFE,
	BaseHelper_get_isMacOSEditor_m96E71133D83F40307304E28B3F103B14ECEAB0C4,
	BaseHelper_get_isLinuxEditor_m0FEEFCA649971D8A75CEF0B563B6965866EF5240,
	BaseHelper_get_isEditorMode_m72F760C50ED845E97E3A98BAEAC42398B3BD2C11,
	BaseHelper_get_isIL2CPP_m266EBBA79AEE873C4A8D4D820D19CC7BFB4376A8,
	BaseHelper_get_CurrentPlatform_m69DEAD89DD0EB052044CCE8FA2E94578A17609AF,
	BaseHelper_get_AndroidAPILevel_mB3A1455F94DB4A7D5C55134B58572F78A0955D22,
	BaseHelper__cctor_m8F4F00BB889949CF9761BA21335D45D71CEB629D,
	BaseHelper_initialize_mB4C285BF69FBAD4B277ED8AD10DDF08DA38F69C5,
	BaseHelper_CreateString_m8187B680400CD1F2A77511DEFBC91152D1F6D9EB,
	BaseHelper_hasActiveClip_m4C947A64FD354BCBAEA2078E0E1A919578D9CCBA,
	BaseHelper_ClearTags_mD93B30D4885D2E5DCCA1672188CD20EB76B01C4F,
	BaseHelper_ClearSpaces_m235DA279F449F5AD4E61A2F82D5C414DB86322CD,
	BaseHelper_ClearLineEndings_m8A5A3B1AFCE29A057A7D0EF2DE7576734251456D,
	BaseHelper_SplitStringToLines_mE1E3904BDE694387F27CDAD680DEF2A490ADC9FF,
	BaseHelper_FormatBytesToHRF_mFBC55F381ACD1E80BB86FEBBFCB9789E3B541DD5,
	BaseHelper_FormatSecondsToHourMinSec_m3822B32615C718A3D267851AD914A984189F59F8,
	BaseHelper_FormatSecondsToHRF_m4262478A6927A4D4D102CB9B98E6703CF965C9A0,
	BaseHelper_HSVToRGB_m20DA8918AE1E0B80A7E6DA9AE4F20A63CB4840F4,
	BaseHelper_GenerateLoremIpsum_m44F59DCB6D935E906B21AECA461F470D26CAF4BA,
	BaseHelper_LanguageToISO639_m87092C04889562791B7B93374C6929CB0B9DA918,
	BaseHelper_ISO639ToLanguage_m1E5C667E7CB97F2039CD18892996D458647EC557,
	BaseHelper_InvokeMethod_m16B910E20A79713DF61CAF991EEC69FD5832FD5D,
	BaseHelper_GetArgument_m3A8731AE152381E9F2B611F619B435D5DA9F1528,
	BaseHelper_GetArguments_m8FEA2135EEFC1A9DBC6E530CD973F3DF3E3541D1,
	BaseHelper_addLeadingZero_mEB1CB744F357C1DD36B41202500BAFD00A2400D2,
	BaseHelper__ctor_mEF441B4F46F49B04BF884A05686974DED620C20C,
	U3CU3Ec__cctor_m9CAEDD2D496807D1F6D85855AFA7AE14980E6C6B,
	U3CU3Ec__ctor_m4871038D3BC7C094E3FA3BA86564AAB953E35B3F,
	U3CU3Ec_U3CInvokeMethodU3Eb__70_0_m4485246A935516AEEE50BE3BE72C983693C1D7FE,
	CTHelper_get_Instance_m9419E0AE2630EC0841937F6CDE83B330BA39D6C9,
	CTHelper_set_Instance_m7D72BBE9FA37F24A47B37E2068BA10A6FD30ED40,
	CTHelper_initialize_m4E427FE7AE92A937936DE03A4AE592CA7B1E3BB5,
	CTHelper_create_m8BCAD76807872BA63E2E48039A13923C9AD30FDD,
	CTHelper_Awake_m6ABEEA501E826225724F3F733DC05665C01DF14C,
	CTHelper_OnDestroy_m724E9967AAA52691C900954395D7543AB7716537,
	CTHelper__ctor_mE9E3363D3C96E6E1EAA25A55602E7AD9EEF181A2,
	CTPlayerPrefs_HasKey_m343530DB4F0540483EBAA38852F22F4BDC494AB8,
	CTPlayerPrefs_DeleteAll_m691A7F70901A1FDE9195F9B8D6CB9D79F67EFDD6,
	CTPlayerPrefs_DeleteKey_mE952677FC2CF7F1F6A21B435FFC7B700247487DF,
	CTPlayerPrefs_Save_m4B4CD209257285EB483F161A464D599D614B69D4,
	CTPlayerPrefs_GetString_m4A24B355B496A4C92544CEE431188605D675A7CA,
	CTPlayerPrefs_GetFloat_mC82EA1654D6558D8854AC0B2FB6C1EFD273E8BAA,
	CTPlayerPrefs_GetInt_m1AF5A20A28BAE9A740A05FCE2261C8BC4EE85C1A,
	CTPlayerPrefs_GetBool_mCBBC02069CA83ABC141BA0EBA91D5D2EDE3B6CD9,
	CTPlayerPrefs_GetDate_mD193EF979FE51EC4CA7E507BD3F7DC7C0746A5C3,
	CTPlayerPrefs_GetVector2_mA4B3425E924194777E2235E85EEDCA2FCC018B22,
	CTPlayerPrefs_GetVector3_m196D95045D7643D5BC2F0EE4A764FFA308AC127A,
	CTPlayerPrefs_GetVector4_m56F8D06161C24EF672DAF4C276C79F86EDA7CCC8,
	CTPlayerPrefs_GetQuaternion_m532B69657E9F1697BE479EFB03F34B1189C3027C,
	CTPlayerPrefs_GetColor_m387F6468E13B7BD546A80CB4AEE105A73ED79E24,
	CTPlayerPrefs_GetLanguage_m8299A105273A79C9B9C795A8516E9C4ED9870E9E,
	CTPlayerPrefs_SetString_mB5D86CF95542A08310B0FE892F040B246770C234,
	CTPlayerPrefs_SetFloat_m257A15CE9D9E94A0A32E829BAAEC438502558CA8,
	CTPlayerPrefs_SetInt_mE40F4D337B12BD39EE80C14CE386A3D703E245EE,
	CTPlayerPrefs_SetBool_m4D6D3AB173DD3E9996FB57DCB2E47B88D1F00690,
	CTPlayerPrefs_SetDate_mB2C8336DC4873AB8D8119D727B0CDDFA87990594,
	CTPlayerPrefs_SetVector2_mAE6CB1FBFAA6C510B7D2DB6E6DD89589CD6DEA62,
	CTPlayerPrefs_SetVector3_m1FB98E2E5AF96DD4FCE8BE5A730D19AC4E326B16,
	CTPlayerPrefs_SetVector4_m2084892DF831A43562DE1E196CF9C33DEC7B66D6,
	CTPlayerPrefs_SetQuaternion_mBAE0430F0D90C2A41F193BFB19B2ABFA01044EA4,
	CTPlayerPrefs_SetColor_m3C4CB7D6C7C5FB6649E1CF02AE321FC0EA8A65C7,
	CTPlayerPrefs_SetLanguage_m6BF49A3D9EFF6026644A41BC587399089D134B07,
	CTPlayerPrefs__ctor_m348876A7CF7E89D43ABAA44C42583305E1B13797,
	CTWebClient_get_Timeout_m05E7AC7AD82CFD0A255A1D753B3AB413E0C15C9E,
	CTWebClient_set_Timeout_mC93FB85FA7A3F30D35CE10A64A25ABD18D206079,
	CTWebClient_get_ConnectionLimit_m74A0F4DC526B36E55EA0857E3F953453622189CE,
	CTWebClient_set_ConnectionLimit_mFBDBF8F1C18BC022FC5B773E3874E8B9569F9068,
	CTWebClient__ctor_m2C17945D74024B83C76B5BB86DBF0DF78E40B537,
	CTWebClient__ctor_m60AC97676C10006B910233B2C508C5CE4E37B67E,
	CTWebClient_CTGetWebRequest_m73349F6C28991D04220A3D78ADA73D30D5400260,
	CTWebClient_GetWebRequest_m6AB86C07D3BA594406B31E2C21ACAF1F4AFF8650,
	FileHelper_get_StreamingAssetsPath_m4E5AB6DB58209D9D49637CB78AE3118EE7A49A38,
	FileHelper__cctor_m02C0AAB32878D8AE95F2804062B69AB1E7845CA2,
	FileHelper_initialize_m6603998B02EF452F39EA024FE1770F766C11EBC6,
	FileHelper_ValidatePath_mC33A5D4608F10381042A3C769355168FBF663F6C,
	FileHelper_ValidateFile_mDFFE24BEEF3F80A0283903E81F765545686B8EF8,
	FileHelper_PathHasInvalidChars_mCE73D25E334A44CFBDBC7B103A873B496668F65D,
	FileHelper_FileHasInvalidChars_m020222D85D2F6FAEE816FAAD7381D597C1E793E2,
	FileHelper_GetFiles_m599BE482715EA35B5198B77E79713FC266FFACB9,
	FileHelper_GetDirectories_mD5A7507379E69911435BEF4078D8065C9B5A4D08,
	FileHelper_GetDrives_mCD7ED284CF74773F8ADB013BADC0BA2F58F34A3A,
	FileHelper_CopyPath_m5E1C94380A6DD84735FC4B72872D646C8004CFC2,
	FileHelper_CopyFile_mBF3C0BD70CFBF9B0AD2470120342732DD457B775,
	FileHelper_ShowPath_m2DD2F1BDA570314F8457A26CA3DA8287778C41AA,
	FileHelper_ShowFile_m948285872AE204C99F232E36B9044D7662D5ECE8,
	FileHelper_OpenFile_m7E3CB79CC5A565D5D9434C841F8FFB672C7D78E3,
	FileHelper_copyAll_m5F3C822743C6C087E40CFA0D24720362CFF513FB,
	FileHelper__ctor_m61D51C1B2CFE64771C1E0C3DD4F9855375A4C71C,
	U3CU3Ec__cctor_m5813CAAC58D5639CBA063C4791613A99AE1A1D5B,
	U3CU3Ec__ctor_mBAFD1D5A93FD498CC1AFCC92405D87762B751D9A,
	U3CU3Ec_U3CGetFilesU3Eb__9_0_m6CC95DCEAA66EFC6A2FFA707CF64A62489173CBC,
	U3CU3Ec_U3CGetFilesU3Eb__9_1_m64D4E8AAB0A38B460650E8AF15F21AC5FED29CE2,
	MemoryCacheStream__ctor_m40170499193384760602B5CBAB91B07CBE927344,
	MemoryCacheStream_get_CanRead_mF29BEF03308C1D07B902FA4A20DAD8A52C421576,
	MemoryCacheStream_get_CanSeek_m160BB37D087E2DFA069CD23E9D631D23D35181B7,
	MemoryCacheStream_get_CanWrite_m809E30F6287E5E7CE0521E847799A0C08D7DBD92,
	MemoryCacheStream_get_Position_m87F36FC9A3C82CAAF85E59B0F0AE18FFA7D6A71C,
	MemoryCacheStream_set_Position_mF5A5556EE64B5A296CE4DB17CE2C83819616FD62,
	MemoryCacheStream_get_Length_m76EC80DED25B746ACC0561071A9FE2BA6E897ED6,
	MemoryCacheStream_Flush_m8C478B46CE9BC988351CCFEF6332CDF38380E788,
	MemoryCacheStream_Seek_mC36176922316F2F3A643E3173D5F1BCD2B2F1D1A,
	MemoryCacheStream_SetLength_mE65E670C20884319228CA0F4B01DDAFC20C5B454,
	MemoryCacheStream_Read_mDD54151F5A800A1C12A8901949C749A978ECB950,
	MemoryCacheStream_Write_m6FF8ED70BC871A06BCF176A5A57AF1070FD0AB7C,
	MemoryCacheStream_read_m192B46EA7033C77496F8D0F52D8F5704A4131DA3,
	MemoryCacheStream_write_mA361C3FC58B5C1898872BE6E0E66CB9887F2971D,
	MemoryCacheStream_createCache_m8EA614C1C5062058051EFDF9A4176DE4971E63D8,
	NetworkHelper_get_isInternetAvailable_m6F5E2844598A6D921748992C651E3D8A780653A2,
	NetworkHelper_OpenURL_m7D5B0C92473554A3AE515E3B0236A58D71EC06B8,
	NetworkHelper_RemoteCertificateValidationCallback_mC30A8BD07A16AF302D5A084E969D824B37D90609,
	NetworkHelper_ValidURLFromFilePath_mB9526E571334DED87344C0F002C0DD991C5AD3E8,
	NetworkHelper_CleanUrl_m443D4EBEFE8D6459F41B706EBA1E0EB6F041EAB7,
	NetworkHelper_isValidURL_mD13744A8999E3B04359201FCC8152513A2D9A919,
	NetworkHelper_GetIP_m3A4B2E9379398548EBD765C4DBDA95A80A0548F2,
	NetworkHelper_openURL_m79A8EB78C6B19DD841D7E3260369B30E2C78830D,
	NetworkHelper__ctor_m71B486C709399BE9BE73F5651BAC39A9ACCDECAC,
	U3CU3Ec__cctor_m3C751253FC6DC556714F342215707EA9E9EF6C84,
	U3CU3Ec__ctor_m22AD43D2037CEEA28886D5ED535393B8805567C3,
	U3CU3Ec_U3CRemoteCertificateValidationCallbackU3Eb__5_0_mFBFBC5681529E306BAD1725AF81972C0DC2C1765,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SingletonHelper_get_isQuitting_mA9E2C23D74C48DCBB8FF9D9380A6C2F279B840C0,
	SingletonHelper_set_isQuitting_m53D01E30152CDE243B85A357E5B55BAF6F346169,
	SingletonHelper__cctor_m21D108C7AC8D3075DA8DF356BA734327AE6F1051,
	SingletonHelper_initialize_mC53AE8824DF8FAAD29911D0F684E9174D96551DB,
	SingletonHelper_onSceneLoaded_mB6ACCBC7368B74C518C65AC80CC871C4E63CAADC,
	SingletonHelper_onSceneUnloaded_mBD26DDE74009DDEA082871A39B5C5BD72C5EFECF,
	SingletonHelper_onQuitting_mD1095EB0956AD487EB7F002CDF156B7EEA476C55,
	SingletonHelper__ctor_m623904A0145A0A36E397BB4B414133F5D6F28F47,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	XmlHelper__ctor_m654E053ADB7377E528EA5D3C770930ECF9F5F15E,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_m93FD10C4CC1978E570BB23B36EB64D5DC41D3E8B,
};
extern void U3CU3CTestDownloadSpeedU3Eb__7_0U3Ed_MoveNext_m286443176C3DBAF50BE3AEC73FF694E869D3EFE9_AdjustorThunk (void);
extern void U3CU3CTestDownloadSpeedU3Eb__7_0U3Ed_SetStateMachine_m8280F2E096D7E15C71E84AFFA607CE6026649EF0_AdjustorThunk (void);
extern void U3CU3CTestUploadSpeedU3Eb__0U3Ed_MoveNext_m519E493A0AE6E4A4C42731310694C3EAE8AA706C_AdjustorThunk (void);
extern void U3CU3CTestUploadSpeedU3Eb__0U3Ed_SetStateMachine_mE9DCC6B33A1AD21B4B16B4A8EA77136BC3FAE33F_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[4] = 
{
	{ 0x060001D1, U3CU3CTestDownloadSpeedU3Eb__7_0U3Ed_MoveNext_m286443176C3DBAF50BE3AEC73FF694E869D3EFE9_AdjustorThunk },
	{ 0x060001D2, U3CU3CTestDownloadSpeedU3Eb__7_0U3Ed_SetStateMachine_m8280F2E096D7E15C71E84AFFA607CE6026649EF0_AdjustorThunk },
	{ 0x060001D5, U3CU3CTestUploadSpeedU3Eb__0U3Ed_MoveNext_m519E493A0AE6E4A4C42731310694C3EAE8AA706C_AdjustorThunk },
	{ 0x060001D6, U3CU3CTestUploadSpeedU3Eb__0U3Ed_SetStateMachine_mE9DCC6B33A1AD21B4B16B4A8EA77136BC3FAE33F_AdjustorThunk },
};
static const int32_t s_InvokerIndices[951] = 
{
	4661,
	4927,
	4927,
	4369,
	4205,
	4369,
	4369,
	4842,
	3946,
	3946,
	3763,
	4927,
	4927,
	4369,
	4010,
	4927,
	4369,
	4927,
	4010,
	4927,
	4369,
	4927,
	4010,
	4927,
	4927,
	4927,
	4661,
	4927,
	4661,
	4927,
	4927,
	4370,
	4927,
	4927,
	4369,
	4217,
	3244,
	3195,
	3244,
	5074,
	5074,
	5023,
	5074,
	5093,
	618,
	4651,
	4927,
	1542,
	1542,
	1542,
	770,
	5074,
	2009,
	4010,
	4927,
	4927,
	4927,
	4958,
	4176,
	4637,
	5093,
	3244,
	5093,
	3244,
	3195,
	2278,
	2278,
	2278,
	4927,
	4927,
	3771,
	4059,
	4059,
	4058,
	4058,
	4016,
	4016,
	4962,
	4962,
	4962,
	4962,
	4962,
	4962,
	4962,
	4962,
	4962,
	4059,
	4059,
	3951,
	3951,
	3684,
	4369,
	4369,
	4927,
	4370,
	4927,
	4791,
	4787,
	4369,
	4927,
	4927,
	4927,
	-1,
	-1,
	4927,
	4927,
	4927,
	4927,
	-1,
	4365,
	4365,
	4927,
	4927,
	4369,
	4927,
	-1,
	-1,
	4927,
	4927,
	4927,
	4927,
	-1,
	-1,
	-1,
	4927,
	4914,
	4913,
	4914,
	4913,
	4998,
	4997,
	5005,
	5004,
	4570,
	4578,
	5002,
	4948,
	4251,
	4581,
	4949,
	4789,
	5000,
	5007,
	4999,
	3630,
	3775,
	3630,
	3775,
	4248,
	4667,
	4667,
	4667,
	4667,
	4982,
	4982,
	4982,
	4982,
	5006,
	4670,
	4369,
	-1,
	4369,
	-1,
	4785,
	4369,
	-1,
	4927,
	4927,
	4927,
	4927,
	4927,
	4927,
	4927,
	4927,
	4371,
	4927,
	4927,
	4927,
	4927,
	4927,
	4927,
	4962,
	4472,
	4369,
	4548,
	5093,
	3244,
	2295,
	3244,
	2295,
	-1,
	-1,
	-1,
	3244,
	1715,
	3244,
	3217,
	2686,
	3179,
	2651,
	3179,
	2651,
	3179,
	2651,
	3217,
	2686,
	3195,
	2666,
	3217,
	2686,
	3217,
	2686,
	3217,
	2686,
	3217,
	2686,
	3217,
	2686,
	3217,
	2686,
	3219,
	2688,
	3217,
	2686,
	3179,
	3195,
	3158,
	2627,
	3180,
	2652,
	3217,
	2666,
	2666,
	2666,
	2666,
	2666,
	2666,
	3244,
	3244,
	3244,
	3244,
	3244,
	5093,
	2686,
	2011,
	3244,
	140,
	2011,
	2011,
	92,
	140,
	3195,
	4927,
	2686,
	2651,
	1565,
	3244,
	1541,
	2686,
	789,
	2666,
	1541,
	2651,
	761,
	2666,
	1541,
	1565,
	498,
	2666,
	2651,
	3244,
	3217,
	3195,
	3244,
	3195,
	2651,
	3244,
	3217,
	3244,
	3195,
	3244,
	3195,
	2651,
	3244,
	3217,
	3244,
	3195,
	3244,
	3195,
	2651,
	3244,
	3217,
	3244,
	3195,
	3244,
	3195,
	3244,
	3244,
	2651,
	3244,
	3217,
	3195,
	3244,
	3195,
	2651,
	3244,
	3217,
	3195,
	3244,
	3195,
	3244,
	1419,
	2009,
	5093,
	3244,
	5093,
	5085,
	5085,
	5093,
	5093,
	3244,
	5074,
	5074,
	5074,
	5093,
	4933,
	5082,
	5093,
	3244,
	2295,
	2011,
	2295,
	2009,
	2295,
	5093,
	5093,
	3244,
	34,
	3195,
	3195,
	2295,
	3179,
	3244,
	3244,
	2666,
	2666,
	3195,
	2666,
	3219,
	2688,
	3217,
	2686,
	3195,
	2666,
	3195,
	2666,
	3219,
	3179,
	2651,
	3217,
	2686,
	3217,
	3244,
	3244,
	3244,
	2666,
	2009,
	957,
	3244,
	1541,
	957,
	251,
	2666,
	2651,
	3244,
	3217,
	3195,
	3244,
	3195,
	5093,
	5082,
	5025,
	5082,
	5025,
	3244,
	3244,
	3244,
	3244,
	3622,
	3622,
	5093,
	5093,
	4959,
	3244,
	3244,
	2666,
	2666,
	3179,
	2651,
	3195,
	2666,
	3195,
	2666,
	3195,
	2666,
	3217,
	2686,
	3195,
	2666,
	3180,
	2652,
	3162,
	3162,
	2631,
	3162,
	2631,
	3162,
	3217,
	2686,
	3217,
	3244,
	3244,
	3244,
	2651,
	2666,
	1138,
	1432,
	616,
	3244,
	1541,
	616,
	134,
	2666,
	3244,
	3244,
	2651,
	3244,
	3217,
	3195,
	3244,
	3195,
	3195,
	1062,
	678,
	678,
	5093,
	5093,
	3244,
	3195,
	1062,
	678,
	678,
	-1,
	4924,
	4369,
	4365,
	3244,
	5093,
	3244,
	2295,
	5093,
	3244,
	1752,
	1146,
	3244,
	2666,
	3244,
	1146,
	3244,
	2666,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2651,
	3244,
	3217,
	3195,
	3244,
	3195,
	3195,
	3195,
	3244,
	-1,
	4927,
	-1,
	-1,
	3244,
	2666,
	2666,
	3217,
	2686,
	3217,
	2686,
	3217,
	2686,
	3195,
	2666,
	3162,
	2631,
	3162,
	2631,
	3162,
	3162,
	2631,
	3162,
	3217,
	2686,
	3217,
	3244,
	3244,
	3244,
	3195,
	3244,
	5074,
	603,
	3244,
	1541,
	603,
	129,
	2666,
	2651,
	3244,
	3217,
	3195,
	3244,
	3195,
	5093,
	3244,
	1884,
	3195,
	2666,
	3162,
	2631,
	3162,
	2631,
	3195,
	2666,
	3162,
	2631,
	3162,
	2631,
	3179,
	2651,
	3179,
	2651,
	3195,
	3244,
	3195,
	3162,
	2631,
	3162,
	2631,
	1320,
	1752,
	3179,
	2651,
	3195,
	2666,
	3195,
	2666,
	3179,
	2651,
	3244,
	3179,
	2651,
	3195,
	2666,
	3195,
	2666,
	3195,
	2666,
	3195,
	2666,
	3195,
	2666,
	3162,
	2631,
	3162,
	2631,
	3162,
	2631,
	3179,
	2651,
	3195,
	3244,
	3195,
	3195,
	3195,
	2666,
	3244,
	3195,
	2666,
	3244,
	2666,
	3195,
	2666,
	3195,
	2666,
	3195,
	2666,
	3195,
	2666,
	3195,
	2666,
	3195,
	2666,
	3244,
	3179,
	2651,
	3179,
	2651,
	3179,
	2651,
	3179,
	2651,
	3179,
	2651,
	3179,
	2651,
	3244,
	3179,
	2651,
	3179,
	2651,
	3179,
	2651,
	3195,
	2666,
	3179,
	2651,
	3195,
	2666,
	3195,
	2666,
	3179,
	2651,
	3244,
	2686,
	2688,
	1320,
	899,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	1565,
	3244,
	2688,
	2688,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	957,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	2651,
	616,
	3244,
	3244,
	3244,
	3244,
	603,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	256,
	256,
	3244,
	2651,
	3244,
	3217,
	3195,
	3244,
	3195,
	2651,
	3244,
	3217,
	3195,
	3244,
	3195,
	3244,
	2666,
	2666,
	3244,
	3244,
	2666,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	2651,
	2686,
	2686,
	2686,
	2688,
	2686,
	2688,
	2686,
	2688,
	3244,
	3244,
	3244,
	3244,
	2686,
	2686,
	2688,
	2688,
	2688,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	5093,
	3244,
	2295,
	2295,
	3244,
	3244,
	3244,
	5093,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	5074,
	5074,
	3244,
	5093,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5082,
	5068,
	5068,
	5093,
	5093,
	4365,
	4962,
	4927,
	4927,
	4927,
	3779,
	4358,
	4919,
	4919,
	3648,
	3532,
	4924,
	4841,
	4016,
	4927,
	5074,
	4924,
	3244,
	5093,
	3244,
	2009,
	5074,
	5023,
	5093,
	5093,
	3244,
	3244,
	3244,
	4962,
	5093,
	5023,
	5093,
	4927,
	4982,
	4841,
	4962,
	4797,
	4993,
	4999,
	5006,
	4946,
	4787,
	4841,
	4661,
	4667,
	4656,
	4666,
	4654,
	4668,
	4669,
	4670,
	4663,
	4652,
	4656,
	3244,
	3179,
	2651,
	3179,
	2651,
	3244,
	1419,
	2009,
	2009,
	5074,
	5093,
	5093,
	4021,
	4927,
	4962,
	4962,
	4020,
	4370,
	5074,
	4218,
	4218,
	5023,
	5023,
	5023,
	4661,
	3244,
	5093,
	3244,
	2295,
	2009,
	1419,
	3217,
	3217,
	3217,
	3180,
	2652,
	3180,
	3244,
	1117,
	2652,
	700,
	942,
	700,
	942,
	3244,
	5082,
	4962,
	3825,
	4927,
	3783,
	4962,
	4927,
	5023,
	3244,
	5093,
	3244,
	2347,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5082,
	5025,
	5093,
	5093,
	4676,
	5026,
	5093,
	3244,
	-1,
	-1,
	-1,
	-1,
	-1,
	3244,
	4841,
};
static const Il2CppTokenRangePair s_rgctxIndices[23] = 
{
	{ 0x02000009, { 28, 4 } },
	{ 0x02000034, { 40, 1 } },
	{ 0x02000035, { 41, 3 } },
	{ 0x02000036, { 44, 3 } },
	{ 0x02000039, { 51, 6 } },
	{ 0x02000072, { 57, 11 } },
	{ 0x06000068, { 0, 1 } },
	{ 0x0600006D, { 1, 1 } },
	{ 0x06000074, { 2, 2 } },
	{ 0x06000075, { 4, 3 } },
	{ 0x0600007A, { 7, 6 } },
	{ 0x0600007B, { 13, 6 } },
	{ 0x0600007C, { 19, 6 } },
	{ 0x060000A1, { 25, 1 } },
	{ 0x060000A3, { 26, 1 } },
	{ 0x060000A6, { 27, 1 } },
	{ 0x060001C5, { 32, 8 } },
	{ 0x060001E7, { 47, 4 } },
	{ 0x060003B1, { 68, 2 } },
	{ 0x060003B2, { 70, 1 } },
	{ 0x060003B3, { 71, 1 } },
	{ 0x060003B4, { 72, 2 } },
	{ 0x060003B5, { 74, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[75] = 
{
	{ (Il2CppRGCTXDataType)2, 131 },
	{ (Il2CppRGCTXDataType)2, 132 },
	{ (Il2CppRGCTXDataType)2, 1752 },
	{ (Il2CppRGCTXDataType)2, 2295 },
	{ (Il2CppRGCTXDataType)2, 1888 },
	{ (Il2CppRGCTXDataType)2, 2004 },
	{ (Il2CppRGCTXDataType)2, 130 },
	{ (Il2CppRGCTXDataType)2, 1751 },
	{ (Il2CppRGCTXDataType)2, 774 },
	{ (Il2CppRGCTXDataType)3, 54 },
	{ (Il2CppRGCTXDataType)2, 1543 },
	{ (Il2CppRGCTXDataType)3, 7265 },
	{ (Il2CppRGCTXDataType)3, 20552 },
	{ (Il2CppRGCTXDataType)2, 1952 },
	{ (Il2CppRGCTXDataType)2, 2061 },
	{ (Il2CppRGCTXDataType)3, 9812 },
	{ (Il2CppRGCTXDataType)2, 321 },
	{ (Il2CppRGCTXDataType)3, 9813 },
	{ (Il2CppRGCTXDataType)2, 610 },
	{ (Il2CppRGCTXDataType)2, 1953 },
	{ (Il2CppRGCTXDataType)2, 2062 },
	{ (Il2CppRGCTXDataType)3, 9814 },
	{ (Il2CppRGCTXDataType)2, 1859 },
	{ (Il2CppRGCTXDataType)3, 9815 },
	{ (Il2CppRGCTXDataType)2, 322 },
	{ (Il2CppRGCTXDataType)3, 20646 },
	{ (Il2CppRGCTXDataType)3, 20645 },
	{ (Il2CppRGCTXDataType)3, 20689 },
	{ (Il2CppRGCTXDataType)2, 775 },
	{ (Il2CppRGCTXDataType)3, 55 },
	{ (Il2CppRGCTXDataType)2, 775 },
	{ (Il2CppRGCTXDataType)2, 520 },
	{ (Il2CppRGCTXDataType)2, 792 },
	{ (Il2CppRGCTXDataType)3, 166 },
	{ (Il2CppRGCTXDataType)3, 167 },
	{ (Il2CppRGCTXDataType)2, 1544 },
	{ (Il2CppRGCTXDataType)3, 7266 },
	{ (Il2CppRGCTXDataType)3, 20553 },
	{ (Il2CppRGCTXDataType)2, 777 },
	{ (Il2CppRGCTXDataType)3, 59 },
	{ (Il2CppRGCTXDataType)3, 1947 },
	{ (Il2CppRGCTXDataType)3, 1944 },
	{ (Il2CppRGCTXDataType)3, 7436 },
	{ (Il2CppRGCTXDataType)3, 1934 },
	{ (Il2CppRGCTXDataType)2, 778 },
	{ (Il2CppRGCTXDataType)3, 60 },
	{ (Il2CppRGCTXDataType)2, 778 },
	{ (Il2CppRGCTXDataType)3, 1772 },
	{ (Il2CppRGCTXDataType)2, 995 },
	{ (Il2CppRGCTXDataType)3, 1771 },
	{ (Il2CppRGCTXDataType)3, 1773 },
	{ (Il2CppRGCTXDataType)3, 1781 },
	{ (Il2CppRGCTXDataType)1, 563 },
	{ (Il2CppRGCTXDataType)2, 563 },
	{ (Il2CppRGCTXDataType)3, 1782 },
	{ (Il2CppRGCTXDataType)3, 1783 },
	{ (Il2CppRGCTXDataType)3, 1784 },
	{ (Il2CppRGCTXDataType)1, 434 },
	{ (Il2CppRGCTXDataType)2, 3135 },
	{ (Il2CppRGCTXDataType)2, 434 },
	{ (Il2CppRGCTXDataType)3, 15269 },
	{ (Il2CppRGCTXDataType)3, 20405 },
	{ (Il2CppRGCTXDataType)3, 15272 },
	{ (Il2CppRGCTXDataType)3, 15270 },
	{ (Il2CppRGCTXDataType)3, 21050 },
	{ (Il2CppRGCTXDataType)3, 21021 },
	{ (Il2CppRGCTXDataType)3, 15271 },
	{ (Il2CppRGCTXDataType)3, 20660 },
	{ (Il2CppRGCTXDataType)2, 281 },
	{ (Il2CppRGCTXDataType)3, 21558 },
	{ (Il2CppRGCTXDataType)3, 21554 },
	{ (Il2CppRGCTXDataType)2, 280 },
	{ (Il2CppRGCTXDataType)1, 284 },
	{ (Il2CppRGCTXDataType)2, 284 },
	{ (Il2CppRGCTXDataType)3, 21555 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	951,
	s_methodPointers,
	4,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	23,
	s_rgctxIndices,
	75,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
