﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.ContextMenu
struct ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.MultilineAttribute
struct MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* ARTrackedImageManager_tB916E34D053E6712190F2BAE46E21D76A0882FF2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.ContextMenu
struct ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.ContextMenu::menuItem
	String_t* ___menuItem_0;
	// System.Boolean UnityEngine.ContextMenu::validate
	bool ___validate_1;
	// System.Int32 UnityEngine.ContextMenu::priority
	int32_t ___priority_2;

public:
	inline static int32_t get_offset_of_menuItem_0() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___menuItem_0)); }
	inline String_t* get_menuItem_0() const { return ___menuItem_0; }
	inline String_t** get_address_of_menuItem_0() { return &___menuItem_0; }
	inline void set_menuItem_0(String_t* value)
	{
		___menuItem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___menuItem_0), (void*)value);
	}

	inline static int32_t get_offset_of_validate_1() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___validate_1)); }
	inline bool get_validate_1() const { return ___validate_1; }
	inline bool* get_address_of_validate_1() { return &___validate_1; }
	inline void set_validate_1(bool value)
	{
		___validate_1 = value;
	}

	inline static int32_t get_offset_of_priority_2() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___priority_2)); }
	inline int32_t get_priority_2() const { return ___priority_2; }
	inline int32_t* get_address_of_priority_2() { return &___priority_2; }
	inline void set_priority_2(int32_t value)
	{
		___priority_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.HelpURLAttribute::m_Url
	String_t* ___m_Url_0;
	// System.Boolean UnityEngine.HelpURLAttribute::m_Dispatcher
	bool ___m_Dispatcher_1;
	// System.String UnityEngine.HelpURLAttribute::m_DispatchingFieldName
	String_t* ___m_DispatchingFieldName_2;

public:
	inline static int32_t get_offset_of_m_Url_0() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Url_0)); }
	inline String_t* get_m_Url_0() const { return ___m_Url_0; }
	inline String_t** get_address_of_m_Url_0() { return &___m_Url_0; }
	inline void set_m_Url_0(String_t* value)
	{
		___m_Url_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Url_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Dispatcher_1() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Dispatcher_1)); }
	inline bool get_m_Dispatcher_1() const { return ___m_Dispatcher_1; }
	inline bool* get_address_of_m_Dispatcher_1() { return &___m_Dispatcher_1; }
	inline void set_m_Dispatcher_1(bool value)
	{
		___m_Dispatcher_1 = value;
	}

	inline static int32_t get_offset_of_m_DispatchingFieldName_2() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_DispatchingFieldName_2)); }
	inline String_t* get_m_DispatchingFieldName_2() const { return ___m_DispatchingFieldName_2; }
	inline String_t** get_address_of_m_DispatchingFieldName_2() { return &___m_DispatchingFieldName_2; }
	inline void set_m_DispatchingFieldName_2(String_t* value)
	{
		___m_DispatchingFieldName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DispatchingFieldName_2), (void*)value);
	}
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.MultilineAttribute
struct MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Int32 UnityEngine.MultilineAttribute::lines
	int32_t ___lines_0;

public:
	inline static int32_t get_offset_of_lines_0() { return static_cast<int32_t>(offsetof(MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291, ___lines_0)); }
	inline int32_t get_lines_0() const { return ___lines_0; }
	inline int32_t* get_address_of_lines_0() { return &___lines_0; }
	inline void set_lines_0(int32_t value)
	{
		___lines_0 = value;
	}
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ContextMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * __this, String_t* ___itemName0, const RuntimeMethod* method);
// System.Void UnityEngine.HelpURLAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215 (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * __this, String_t* ___url0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, int32_t ___order1, const RuntimeMethod* method);
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44 (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * __this, float ___height0, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.MultilineAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultilineAttribute__ctor_m353DC377C95D4C341F5DDD0F9894878F64E5703E (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void CameraRotator_t93FDDAA18A400F46A922F8F948BDF616B424CEA4_CustomAttributesCacheGenerator_mouseRotateSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 5.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x77\x20\x73\x65\x6E\x73\x69\x74\x69\x76\x65\x20\x74\x68\x65\x20\x6D\x6F\x75\x73\x65\x20\x64\x72\x61\x67\x20\x74\x6F\x20\x63\x61\x6D\x65\x72\x61\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void CameraRotator_t93FDDAA18A400F46A922F8F948BDF616B424CEA4_CustomAttributesCacheGenerator_touchRotateSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.00999999978f, 100.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x77\x20\x73\x65\x6E\x73\x69\x74\x69\x76\x65\x20\x74\x68\x65\x20\x74\x6F\x75\x63\x68\x20\x64\x72\x61\x67\x20\x74\x6F\x20\x63\x61\x6D\x65\x72\x61\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void CameraRotator_t93FDDAA18A400F46A922F8F948BDF616B424CEA4_CustomAttributesCacheGenerator_slerpValue(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6D\x61\x6C\x6C\x65\x72\x20\x70\x6F\x73\x69\x74\x69\x76\x65\x20\x76\x61\x6C\x75\x65\x20\x6D\x65\x61\x6E\x73\x20\x73\x6D\x6F\x6F\x74\x68\x65\x72\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x2C\x20\x31\x20\x6D\x65\x61\x6E\x73\x20\x6E\x6F\x20\x73\x6D\x6F\x6F\x74\x68\x20\x61\x70\x70\x6C\x79"), NULL);
	}
}
static void CameraRotator_t93FDDAA18A400F46A922F8F948BDF616B424CEA4_CustomAttributesCacheGenerator_rotateMethod(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x77\x20\x64\x6F\x20\x79\x6F\x75\x20\x6C\x69\x6B\x65\x20\x74\x6F\x20\x72\x6F\x74\x61\x74\x65\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void IntroScreenInteraction_t3B2F28011E885686E5DDB54652F686C0BFA73A28_CustomAttributesCacheGenerator_arTrackedImageManager(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x46\x6F\x75\x6E\x64\x61\x74\x69\x6F\x6E\x20\x52\x65\x66\x65\x72\x65\x6E\x63\x65"), NULL);
	}
}
static void PreloaderController_tD3EB462D0A9954E9D9785017331EFFC5C9072801_CustomAttributesCacheGenerator_PreloaderController_DisablePreloaderPanel_m5B3399000BD62311676914B199EF4140B6F37CD5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A_0_0_0_var), NULL);
	}
}
static void U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A_CustomAttributesCacheGenerator_U3CDisablePreloaderPanelU3Ed__3__ctor_m78A4AD06EDA904CACCAC2FCFE42949BB0D950658(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A_CustomAttributesCacheGenerator_U3CDisablePreloaderPanelU3Ed__3_System_IDisposable_Dispose_mB1FFDC5C6C5877BA63FC0D5EDD299367F062C01E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A_CustomAttributesCacheGenerator_U3CDisablePreloaderPanelU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5E78A5B68092F270355E899D99CE2D748705A525(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A_CustomAttributesCacheGenerator_U3CDisablePreloaderPanelU3Ed__3_System_Collections_IEnumerator_Reset_mF42974D782E8ABEF2D7C266ACC18B0C5F80EF45D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A_CustomAttributesCacheGenerator_U3CDisablePreloaderPanelU3Ed__3_System_Collections_IEnumerator_get_Current_mF638D8B9A629459A3E4DE1EAB729369B457AFD82(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579_CustomAttributesCacheGenerator_mediaPlayerA(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x64\x69\x61\x70\x6C\x61\x79\x65\x72\x73"), NULL);
	}
}
static void VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579_CustomAttributesCacheGenerator_displayUGUIMain(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x56\x69\x64\x65\x6F\x44\x69\x73\x70\x6C\x61\x79"), NULL);
	}
}
static void VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579_CustomAttributesCacheGenerator_VideoPlayerController_StartVideoPlaying_mBA95547320C9617A544BC8A692280E0F2040F6EF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_0_0_0_var), NULL);
	}
}
static void U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_CustomAttributesCacheGenerator_U3CStartVideoPlayingU3Ed__13__ctor_mA06BAE3A6CF5203156A73FD7AF9108A145AF0EC6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_CustomAttributesCacheGenerator_U3CStartVideoPlayingU3Ed__13_System_IDisposable_Dispose_m8E449EF5BA253A939DCF11CAE39A4C8355DA6489(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_CustomAttributesCacheGenerator_U3CStartVideoPlayingU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDAAAB33056322C26FB2E7185CA14F33A10CE42D0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_CustomAttributesCacheGenerator_U3CStartVideoPlayingU3Ed__13_System_Collections_IEnumerator_Reset_m149BF3B02F1A023E95FAC7BBD2A5CF729F128F37(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_CustomAttributesCacheGenerator_U3CStartVideoPlayingU3Ed__13_System_Collections_IEnumerator_get_Current_mEC51760B8923B9C8CC3193FD8D8275B4A5E8FC9B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5_CustomAttributesCacheGenerator_SpeedCheck_ResetSpeedTestCheck_mA096A0F8602D61EC9D018A6115B092ED25F254BC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817_0_0_0_var), NULL);
	}
}
static void U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817_CustomAttributesCacheGenerator_U3CResetSpeedTestCheckU3Ed__16__ctor_m3D053FE8D19035A8F7E29F2EEF8D7E1892B0B68F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817_CustomAttributesCacheGenerator_U3CResetSpeedTestCheckU3Ed__16_System_IDisposable_Dispose_mCD152DEBBE8174D003FF33217C5BD072053482C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817_CustomAttributesCacheGenerator_U3CResetSpeedTestCheckU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD32DC79D428A42EE1AFBAD15A32C1E8A132EC22(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817_CustomAttributesCacheGenerator_U3CResetSpeedTestCheckU3Ed__16_System_Collections_IEnumerator_Reset_m4DF85D13FFF1B60EFF83963ECAA4A5728B292A01(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817_CustomAttributesCacheGenerator_U3CResetSpeedTestCheckU3Ed__16_System_Collections_IEnumerator_get_Current_m9A98DBB9701B673D1CEE0E764B117C6A24593E87(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DynamicPrefab_tFBC886C4E3009A6A81856222A53B2A0F7D3B6E36_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARTrackedImageManager_tB916E34D053E6712190F2BAE46E21D76A0882FF2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARTrackedImageManager_tB916E34D053E6712190F2BAE46E21D76A0882FF2_0_0_0_var), NULL);
	}
}
static void DynamicPrefab_tFBC886C4E3009A6A81856222A53B2A0F7D3B6E36_CustomAttributesCacheGenerator_m_AlternativePrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PrefabImagePairManager_t02A2EF0A6C8CCDBB935F9DF00EAFE57290E84349_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARTrackedImageManager_tB916E34D053E6712190F2BAE46E21D76A0882FF2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARTrackedImageManager_tB916E34D053E6712190F2BAE46E21D76A0882FF2_0_0_0_var), NULL);
	}
}
static void PrefabImagePairManager_t02A2EF0A6C8CCDBB935F9DF00EAFE57290E84349_CustomAttributesCacheGenerator_m_PrefabsList(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void PrefabImagePairManager_t02A2EF0A6C8CCDBB935F9DF00EAFE57290E84349_CustomAttributesCacheGenerator_m_ImageLibrary(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x20\x49\x6D\x61\x67\x65\x20\x4C\x69\x62\x72\x61\x72\x79"), NULL);
	}
}
static void PrefabImagePairManager_t02A2EF0A6C8CCDBB935F9DF00EAFE57290E84349_CustomAttributesCacheGenerator_introScreenInteraction(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x74\x72\x6F\x53\x63\x72\x65\x65\x6E\x20\x48\x61\x6E\x64\x6C\x65\x72"), NULL);
	}
}
static void PrefabImagePairManager_t02A2EF0A6C8CCDBB935F9DF00EAFE57290E84349_CustomAttributesCacheGenerator_PrefabImagePairManager_Interaction1Editor_m4130DB0CCFDB7964330885CE97B75CD92DBFC2B6(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x74\x65\x72\x61\x63\x74\x69\x6F\x6E\x31"), NULL);
	}
}
static void PrefabImagePairManager_t02A2EF0A6C8CCDBB935F9DF00EAFE57290E84349_CustomAttributesCacheGenerator_PrefabImagePairManager_Interaction2Editor_mE918E1C78F95613301EF36F8FD822C257E505ACC(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x74\x65\x72\x61\x63\x74\x69\x6F\x6E\x32"), NULL);
	}
}
static void ApplyToMaterial_tFE4B04F713C8BB44F9541EC00B32BA808DE07965_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x3A\x2F\x2F\x72\x65\x6E\x64\x65\x72\x68\x65\x61\x64\x73\x2E\x63\x6F\x6D\x2F\x70\x72\x6F\x64\x75\x63\x74\x73\x2F\x61\x76\x70\x72\x6F\x2D\x76\x69\x64\x65\x6F\x2F"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x41\x56\x50\x72\x6F\x20\x56\x69\x64\x65\x6F\x2F\x41\x70\x70\x6C\x79\x20\x54\x6F\x20\x4D\x61\x74\x65\x72\x69\x61\x6C"), 300LL, NULL);
	}
}
static void ApplyToMaterial_tFE4B04F713C8BB44F9541EC00B32BA808DE07965_CustomAttributesCacheGenerator__media(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x64\x69\x61\x20\x53\x6F\x75\x72\x63\x65"), NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 8.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ApplyToMaterial_tFE4B04F713C8BB44F9541EC00B32BA808DE07965_CustomAttributesCacheGenerator__defaultTexture(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x61\x75\x6C\x74\x20\x74\x65\x78\x74\x75\x72\x65\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x76\x69\x64\x65\x6F\x20\x74\x65\x78\x74\x75\x72\x65\x20\x69\x73\x20\x70\x72\x65\x70\x61\x72\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ApplyToMaterial_tFE4B04F713C8BB44F9541EC00B32BA808DE07965_CustomAttributesCacheGenerator__material(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x65\x72\x69\x61\x6C\x20\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[2];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 8.0f, NULL);
	}
}
static void ApplyToMaterial_tFE4B04F713C8BB44F9541EC00B32BA808DE07965_CustomAttributesCacheGenerator__texturePropertyName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ApplyToMaterial_tFE4B04F713C8BB44F9541EC00B32BA808DE07965_CustomAttributesCacheGenerator__offset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ApplyToMaterial_tFE4B04F713C8BB44F9541EC00B32BA808DE07965_CustomAttributesCacheGenerator__scale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x3A\x2F\x2F\x72\x65\x6E\x64\x65\x72\x68\x65\x61\x64\x73\x2E\x63\x6F\x6D\x2F\x70\x72\x6F\x64\x75\x63\x74\x73\x2F\x61\x76\x70\x72\x6F\x2D\x76\x69\x64\x65\x6F\x2F"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x41\x56\x50\x72\x6F\x20\x56\x69\x64\x65\x6F\x2F\x41\x70\x70\x6C\x79\x20\x54\x6F\x20\x4D\x65\x73\x68"), 300LL, NULL);
	}
}
static void ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2_CustomAttributesCacheGenerator__media(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x64\x69\x61\x20\x53\x6F\x75\x72\x63\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2_CustomAttributesCacheGenerator__defaultTexture(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x61\x75\x6C\x74\x20\x74\x65\x78\x74\x75\x72\x65\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x76\x69\x64\x65\x6F\x20\x74\x65\x78\x74\x75\x72\x65\x20\x69\x73\x20\x70\x72\x65\x70\x61\x72\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2_CustomAttributesCacheGenerator__mesh(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x6E\x64\x65\x72\x65\x72\x20\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[2];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 8.0f, NULL);
	}
}
static void ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2_CustomAttributesCacheGenerator__texturePropertyName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2_CustomAttributesCacheGenerator__offset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2_CustomAttributesCacheGenerator__scale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioChannelMixer_tFF855B15B8F6159A608283BAB221888288C34243_CustomAttributesCacheGenerator__channels(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioOutput_t2338563FDAACA576F36819FB7D9308856E5D1297_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x41\x56\x50\x72\x6F\x20\x56\x69\x64\x65\x6F\x2F\x41\x75\x64\x69\x6F\x20\x4F\x75\x74\x70\x75\x74"), 400LL, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[2];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x3A\x2F\x2F\x72\x65\x6E\x64\x65\x72\x68\x65\x61\x64\x73\x2E\x63\x6F\x6D\x2F\x70\x72\x6F\x64\x75\x63\x74\x73\x2F\x61\x76\x70\x72\x6F\x2D\x76\x69\x64\x65\x6F\x2F"), NULL);
	}
}
static void AudioOutput_t2338563FDAACA576F36819FB7D9308856E5D1297_CustomAttributesCacheGenerator__mediaPlayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioOutput_t2338563FDAACA576F36819FB7D9308856E5D1297_CustomAttributesCacheGenerator__channelMask(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CubemapCube_t0C4B6C987C5A1B2005D9B02E871ACD1E1C905963_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x41\x56\x50\x72\x6F\x20\x56\x69\x64\x65\x6F\x2F\x43\x75\x62\x65\x6D\x61\x70\x20\x43\x75\x62\x65\x20\x28\x56\x52\x29"), 400LL, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[3];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x3A\x2F\x2F\x72\x65\x6E\x64\x65\x72\x68\x65\x61\x64\x73\x2E\x63\x6F\x6D\x2F\x70\x72\x6F\x64\x75\x63\x74\x73\x2F\x61\x76\x70\x72\x6F\x2D\x76\x69\x64\x65\x6F\x2F"), NULL);
	}
}
static void CubemapCube_t0C4B6C987C5A1B2005D9B02E871ACD1E1C905963_CustomAttributesCacheGenerator__material(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CubemapCube_t0C4B6C987C5A1B2005D9B02E871ACD1E1C905963_CustomAttributesCacheGenerator__mediaPlayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CubemapCube_t0C4B6C987C5A1B2005D9B02E871ACD1E1C905963_CustomAttributesCacheGenerator_expansion_coeff(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CubemapCube_t0C4B6C987C5A1B2005D9B02E871ACD1E1C905963_CustomAttributesCacheGenerator__layout(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebugOverlay_tA47024917BBBBE5921A20043B20D790B906B56A1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x41\x56\x50\x72\x6F\x20\x56\x69\x64\x65\x6F\x2F\x44\x65\x62\x75\x67\x20\x4F\x76\x65\x72\x6C\x61\x79"), -99LL, NULL);
	}
}
static void DebugOverlay_tA47024917BBBBE5921A20043B20D790B906B56A1_CustomAttributesCacheGenerator__mediaPlayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebugOverlay_tA47024917BBBBE5921A20043B20D790B906B56A1_CustomAttributesCacheGenerator__guiDepth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebugOverlay_tA47024917BBBBE5921A20043B20D790B906B56A1_CustomAttributesCacheGenerator__displaySize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebugOverlay_tA47024917BBBBE5921A20043B20D790B906B56A1_CustomAttributesCacheGenerator__displayControls(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisplayBackground_t089575A140150F58308B9696F0CD5E0BA7E85D7E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x3A\x2F\x2F\x72\x65\x6E\x64\x65\x72\x68\x65\x61\x64\x73\x2E\x63\x6F\x6D\x2F\x70\x72\x6F\x64\x75\x63\x74\x73\x2F\x61\x76\x70\x72\x6F\x2D\x76\x69\x64\x65\x6F\x2F"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x41\x56\x50\x72\x6F\x20\x56\x69\x64\x65\x6F\x2F\x44\x69\x73\x70\x6C\x61\x79\x20\x42\x61\x63\x6B\x67\x72\x6F\x75\x6E\x64"), 200LL, NULL);
	}
}
static void DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x3A\x2F\x2F\x72\x65\x6E\x64\x65\x72\x68\x65\x61\x64\x73\x2E\x63\x6F\x6D\x2F\x70\x72\x6F\x64\x75\x63\x74\x73\x2F\x61\x76\x70\x72\x6F\x2D\x76\x69\x64\x65\x6F\x2F"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x41\x56\x50\x72\x6F\x20\x56\x69\x64\x65\x6F\x2F\x44\x69\x73\x70\x6C\x61\x79\x20\x49\x4D\x47\x55\x49"), 200LL, NULL);
	}
}
static void DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_CustomAttributesCacheGenerator__useDepth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_CustomAttributesCacheGenerator__x(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_CustomAttributesCacheGenerator__y(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_CustomAttributesCacheGenerator__width(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_CustomAttributesCacheGenerator__height(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x3A\x2F\x2F\x72\x65\x6E\x64\x65\x72\x68\x65\x61\x64\x73\x2E\x63\x6F\x6D\x2F\x70\x72\x6F\x64\x75\x63\x74\x73\x2F\x61\x76\x70\x72\x6F\x2D\x76\x69\x64\x65\x6F\x2F"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x41\x56\x50\x72\x6F\x20\x56\x69\x64\x65\x6F\x2F\x44\x69\x73\x70\x6C\x61\x79\x20\x75\x47\x55\x49"), 200LL, NULL);
	}
}
static void DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator__mediaPlayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator_m_UVRect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator__setNativeSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator__scaleMode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator__noDefaultDisplay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator__displayInEditor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator__defaultTexture(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator_DisplayUGUI_SetNativeSize_m342646EAEE1E4D4D4AAF41E8DB65AD538C461F39(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x20\x4E\x61\x74\x69\x76\x65\x20\x53\x69\x7A\x65"), NULL);
	}
}
static void DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator_DisplayUGUI_OnFillVBO_mC3C5089E9453CF286F0E86574C63D47814AF537A(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x6D\x65\x74\x68\x6F\x64\x20\x69\x73\x20\x6E\x6F\x74\x20\x63\x61\x6C\x6C\x65\x64\x20\x66\x72\x6F\x6D\x20\x55\x6E\x69\x74\x79\x20\x35\x2E\x32\x20\x61\x6E\x64\x20\x61\x62\x6F\x76\x65"), NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x41\x56\x50\x72\x6F\x20\x56\x69\x64\x65\x6F\x2F\x4D\x65\x64\x69\x61\x20\x50\x6C\x61\x79\x65\x72"), -100LL, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x3A\x2F\x2F\x72\x65\x6E\x64\x65\x72\x68\x65\x61\x64\x73\x2E\x63\x6F\x6D\x2F\x70\x72\x6F\x64\x75\x63\x74\x73\x2F\x61\x76\x70\x72\x6F\x2D\x76\x69\x64\x65\x6F\x2F"), NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_Volume(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_Balance(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -1.0f, 1.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_PlaybackRate(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -4.0f, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_ResampleBufferSize(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 3.0f, 10.0f, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_Persistent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_videoMapping(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_AnisoLevel(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 16.0f, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_LoadSubtitles(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_SubtitleLocation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_SubtitlePath(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_AudioHeadTransform(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_AudioFocusEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_AudioFocusTransform(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_AudioFocusWidthDegrees(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 40.0f, 120.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_AudioFocusOffLevelDB(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -24.0f, 0.0f, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_events(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_eventMask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_forceFileFormat(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__pauseMediaOnAppPause(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__playMediaOnAppUnpause(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_sourceSampleRate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_sourceChannels(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_manuallySetAudioSourceProperties(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsWindows(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsMacOSX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsIOS(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsTVOS(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsAndroid(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsWindowsPhone(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsWindowsUWP(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsWebGL(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsPS4(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_MediaPlayer_LoadSubtitlesCoroutine_mED609F44A8A99CD62D067088B7AF30B2FB347B00(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB_0_0_0_var), NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_MediaPlayer_FinalRenderCapture_m58C8CA21EA62D3E54003358527F735B1D3AA5E4D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2_0_0_0_var), NULL);
	}
}
static void MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_MediaPlayer_ExtractFrameCoroutine_mD4F518A5EEB7146A9BA59E65744A9B23BC328326(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010_0_0_0_var), NULL);
	}
}
static void OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E_CustomAttributesCacheGenerator_httpHeaderJson(CustomAttributesCache* cache)
{
	{
		MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * tmp = (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 *)cache->attributes[0];
		MultilineAttribute__ctor_m353DC377C95D4C341F5DDD0F9894878F64E5703E(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E_CustomAttributesCacheGenerator_base64EncodedKeyBlob(CustomAttributesCache* cache)
{
	{
		MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * tmp = (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 *)cache->attributes[0];
		MultilineAttribute__ctor_m353DC377C95D4C341F5DDD0F9894878F64E5703E(tmp, NULL);
	}
}
static void OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393_CustomAttributesCacheGenerator_httpHeaderJson(CustomAttributesCache* cache)
{
	{
		MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * tmp = (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 *)cache->attributes[0];
		MultilineAttribute__ctor_m353DC377C95D4C341F5DDD0F9894878F64E5703E(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393_CustomAttributesCacheGenerator_fileOffset(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x42\x79\x74\x65\x20\x6F\x66\x66\x73\x65\x74\x20\x69\x6E\x74\x6F\x20\x74\x68\x65\x20\x66\x69\x6C\x65\x20\x77\x68\x65\x72\x65\x20\x74\x68\x65\x20\x6D\x65\x64\x69\x61\x20\x66\x69\x6C\x65\x20\x69\x73\x20\x6C\x6F\x63\x61\x74\x65\x64\x2E\x20\x20\x54\x68\x69\x73\x20\x69\x73\x20\x75\x73\x65\x66\x75\x6C\x20\x77\x68\x65\x6E\x20\x68\x69\x64\x69\x6E\x67\x20\x6F\x72\x20\x70\x61\x63\x6B\x69\x6E\x67\x20\x6D\x65\x64\x69\x61\x20\x66\x69\x6C\x65\x73\x20\x77\x69\x74\x68\x69\x6E\x20\x61\x6E\x6F\x74\x68\x65\x72\x20\x66\x69\x6C\x65\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB_CustomAttributesCacheGenerator_U3CLoadSubtitlesCoroutineU3Ed__166__ctor_mAF47DBCEE88F6F816BE1F9BA4F33F3BAA344957E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB_CustomAttributesCacheGenerator_U3CLoadSubtitlesCoroutineU3Ed__166_System_IDisposable_Dispose_m7A3DE1124596B52D5185CEE3AEE6322EA3FBF978(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB_CustomAttributesCacheGenerator_U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC6D7BB5CC4CDCB04948293FC163F9E337D46A775(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB_CustomAttributesCacheGenerator_U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_IEnumerator_Reset_m625A44CCFB701C03434E399A2A8AFD83D81C3628(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB_CustomAttributesCacheGenerator_U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_IEnumerator_get_Current_m5653252B6D3E7BDF005249225ABEEB9B867D1C55(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2_CustomAttributesCacheGenerator_U3CFinalRenderCaptureU3Ed__188__ctor_m498CF3113FF234A782856D3BE00DED342F375BEC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2_CustomAttributesCacheGenerator_U3CFinalRenderCaptureU3Ed__188_System_IDisposable_Dispose_m8A0688DA71BC5C7ED01C0415FD9CDA8D2B52D5F4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2_CustomAttributesCacheGenerator_U3CFinalRenderCaptureU3Ed__188_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF833D26918BB2E538C49FF8C63CE97F8CBCC3EE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2_CustomAttributesCacheGenerator_U3CFinalRenderCaptureU3Ed__188_System_Collections_IEnumerator_Reset_m22429013FDB26856E4D1FD9EF1749DD600BF547C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2_CustomAttributesCacheGenerator_U3CFinalRenderCaptureU3Ed__188_System_Collections_IEnumerator_get_Current_m021BA9DE6276EB6F40BC8FFA6746895F17044C30(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010_CustomAttributesCacheGenerator_U3CExtractFrameCoroutineU3Ed__209__ctor_m42A11F4ACB5EABD7F3B2FF42F17377F8988B55F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010_CustomAttributesCacheGenerator_U3CExtractFrameCoroutineU3Ed__209_System_IDisposable_Dispose_m2813638F893988A28291B082C41B4C2CCFA5E316(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010_CustomAttributesCacheGenerator_U3CExtractFrameCoroutineU3Ed__209_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EB04B181B3AF0D8F31D45DB7FE4073A601B9FD7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010_CustomAttributesCacheGenerator_U3CExtractFrameCoroutineU3Ed__209_System_Collections_IEnumerator_Reset_m03E84C88DE36A133F5C616284E160B269D57B779(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010_CustomAttributesCacheGenerator_U3CExtractFrameCoroutineU3Ed__209_System_Collections_IEnumerator_get_Current_mC4C324C31F1A54748026CBCAA87DC42C1925C5A7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MediaPlaylist_t59C2705D3E06C8684FDA895F959FFFEFA8BF4BF8_CustomAttributesCacheGenerator__items(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_fileLocation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_filePath(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_loop(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_startMode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_progressMode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_progressTimeSeconds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_autoPlay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_stereoPacking(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_alphaPacking(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_isOverrideTransition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_overrideTransition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_overrideTransitionDuration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_overrideTransitionEasing(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x41\x56\x50\x72\x6F\x20\x56\x69\x64\x65\x6F\x2F\x50\x6C\x61\x79\x6C\x69\x73\x74\x20\x4D\x65\x64\x69\x61\x20\x50\x6C\x61\x79\x65\x72\x20\x28\x42\x45\x54\x41\x29"), -100LL, NULL);
	}
}
static void PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__playerA(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__playerB(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__playlistAutoProgress(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__autoCloseVideo(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x6F\x73\x65\x20\x74\x68\x65\x20\x76\x69\x64\x65\x6F\x20\x6F\x6E\x20\x74\x68\x65\x20\x6F\x74\x68\x65\x72\x20\x4D\x65\x64\x69\x61\x50\x6C\x61\x79\x65\x72\x20\x77\x68\x65\x6E\x20\x69\x74\x20\x69\x73\x20\x6E\x6F\x74\x20\x76\x69\x73\x69\x62\x6C\x65\x20\x61\x6E\x79\x20\x6D\x6F\x72\x65\x2E\x20\x20\x54\x68\x69\x73\x20\x69\x73\x20\x75\x73\x65\x66\x75\x6C\x20\x66\x6F\x72\x20\x66\x72\x65\x65\x69\x6E\x67\x20\x75\x70\x20\x6D\x65\x6D\x6F\x72\x79\x20\x61\x6E\x64\x20\x47\x50\x55\x20\x64\x65\x63\x6F\x64\x69\x6E\x67\x20\x72\x65\x73\x6F\x75\x72\x63\x65\x73\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__playlistLoopMode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__playlist(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__pausePreviousOnTransition(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x75\x73\x65\x20\x74\x68\x65\x20\x70\x72\x65\x76\x69\x6F\x75\x73\x6C\x79\x20\x70\x6C\x61\x79\x69\x6E\x67\x20\x76\x69\x64\x65\x6F\x2E\x20\x54\x68\x69\x73\x20\x69\x73\x20\x75\x73\x65\x66\x75\x6C\x20\x66\x6F\x72\x20\x73\x79\x73\x74\x65\x6D\x73\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x73\x74\x72\x75\x67\x67\x6C\x65\x20\x74\x6F\x20\x70\x6C\x61\x79\x20\x32\x20\x76\x69\x64\x65\x6F\x73\x20\x61\x74\x20\x6F\x6E\x63\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__nextTransition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__transitionDuration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__transitionEasing(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SubtitlesUGUI_tC568ED275DD251EC273AB15366CD1A3E39034225_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x41\x56\x50\x72\x6F\x20\x56\x69\x64\x65\x6F\x2F\x53\x75\x62\x74\x69\x74\x6C\x65\x73\x20\x75\x47\x55\x49"), 201LL, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x3A\x2F\x2F\x72\x65\x6E\x64\x65\x72\x68\x65\x61\x64\x73\x2E\x63\x6F\x6D\x2F\x70\x72\x6F\x64\x75\x63\x74\x73\x2F\x61\x76\x70\x72\x6F\x2D\x76\x69\x64\x65\x6F\x2F"), NULL);
	}
}
static void SubtitlesUGUI_tC568ED275DD251EC273AB15366CD1A3E39034225_CustomAttributesCacheGenerator__mediaPlayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SubtitlesUGUI_tC568ED275DD251EC273AB15366CD1A3E39034225_CustomAttributesCacheGenerator__text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpdateStereoMaterial_tEBB7F84D0D948F277458D08721511D3AAF825075_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x3A\x2F\x2F\x72\x65\x6E\x64\x65\x72\x68\x65\x61\x64\x73\x2E\x63\x6F\x6D\x2F\x70\x72\x6F\x64\x75\x63\x74\x73\x2F\x61\x76\x70\x72\x6F\x2D\x76\x69\x64\x65\x6F\x2F"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x41\x56\x50\x72\x6F\x20\x56\x69\x64\x65\x6F\x2F\x55\x70\x64\x61\x74\x65\x20\x53\x74\x65\x72\x65\x6F\x20\x4D\x61\x74\x65\x72\x69\x61\x6C"), 400LL, NULL);
	}
}
static void UpdateStereoMaterial_tEBB7F84D0D948F277458D08721511D3AAF825075_CustomAttributesCacheGenerator__camera(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x65\x72\x65\x6F\x20\x63\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void UpdateStereoMaterial_tEBB7F84D0D948F277458D08721511D3AAF825075_CustomAttributesCacheGenerator__renderer(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x6E\x64\x65\x72\x69\x6E\x67\x20\x65\x6C\x65\x6D\x65\x6E\x74\x73"), NULL);
	}
}
static void UpdateStereoMaterial_tEBB7F84D0D948F277458D08721511D3AAF825075_CustomAttributesCacheGenerator__forceEyeMode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876_CustomAttributesCacheGenerator_U3CLastTU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876_CustomAttributesCacheGenerator_U3CTextureTimeStampU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876_CustomAttributesCacheGenerator_Resampler_get_LastT_m14664B1A60E1A35DEC05847B385D1B0F20D53BC4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876_CustomAttributesCacheGenerator_Resampler_set_LastT_mA6EEBE901C8F444E4C0755399E984A17348F798F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876_CustomAttributesCacheGenerator_Resampler_get_TextureTimeStamp_mE0BF12F7A977BE1B3461A8315EB26D1BB0654205(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876_CustomAttributesCacheGenerator_Resampler_set_TextureTimeStamp_m45BAB50E6FF3E9CB4BC9F410FD36A311781FDAF8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AutoRotate_tD3FCE4EA299571DD8C213BB0EACF8FC27AFCD314_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var), NULL);
	}
}
static void DemoInfo_tFC8CE5653450E76B5095669CD562A80FFD6712B7_CustomAttributesCacheGenerator__description(CustomAttributesCache* cache)
{
	{
		MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * tmp = (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 *)cache->attributes[0];
		MultilineAttribute__ctor_m353DC377C95D4C341F5DDD0F9894878F64E5703E(tmp, NULL);
	}
}
static void Mapping3D_tD245FFB24F152207D7C99D9AAB180568116C5D27_CustomAttributesCacheGenerator__cubePrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SampleApp_Multiple_tEC8B83D898EFB18F16A1D2A8A5BA3D26A8141F4A_CustomAttributesCacheGenerator_m_videoPath(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SampleApp_Multiple_tEC8B83D898EFB18F16A1D2A8A5BA3D26A8141F4A_CustomAttributesCacheGenerator_m_videoLocation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ChangeVideoExample_t7A4744ABFD531F36394852968842D3CD8D832B23_CustomAttributesCacheGenerator__mediaPlayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadFromBuffer_t4FDF3E21E03A6270CA1D4DABA65CC603A8280189_CustomAttributesCacheGenerator__mp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadFromBuffer_t4FDF3E21E03A6270CA1D4DABA65CC603A8280189_CustomAttributesCacheGenerator__filename(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadFromBufferInChunks_tFFCF1D595509528875CCD737262722559BE45788_CustomAttributesCacheGenerator__mp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadFromBufferInChunks_tFFCF1D595509528875CCD737262722559BE45788_CustomAttributesCacheGenerator__filename(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NativeMediaOpen_t97AA3DE898F9247ECB3A886B6A10BD345FA2D19C_CustomAttributesCacheGenerator__player(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StartEndPoint_t963F5A2BBE3B9E2CFE0166DCBF7FB0263CD74A5D_CustomAttributesCacheGenerator__startLoopSeconds(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x6C\x6F\x6F\x70\x69\x6E\x67\x20\x69\x73\x20\x65\x6E\x61\x62\x6C\x65\x64\x2C\x20\x74\x68\x69\x73\x20\x69\x73\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x20\x74\x68\x65\x20\x76\x69\x64\x65\x6F\x20\x72\x65\x77\x69\x6E\x64\x73\x20\x74\x6F\x20\x77\x68\x65\x6E\x20\x69\x74\x20\x72\x65\x61\x63\x68\x65\x73\x20\x74\x68\x65\x20\x65\x6E\x64\x20\x70\x6F\x69\x6E\x74"), NULL);
	}
}
static void VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656_CustomAttributesCacheGenerator__mediaPlayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656_CustomAttributesCacheGenerator__fadeTimeMs(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65_CustomAttributesCacheGenerator_SimpleController_LoadVideoWithFading_mEA1895F18C5A29022FFB95B240850EF57A01DEBE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9_0_0_0_var), NULL);
	}
}
static void U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9_CustomAttributesCacheGenerator_U3CLoadVideoWithFadingU3Ed__23__ctor_m82DE7FDB17C4F4F5D90E14A3642D40C45237FA91(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9_CustomAttributesCacheGenerator_U3CLoadVideoWithFadingU3Ed__23_System_IDisposable_Dispose_mF6B335E78513CCE1A4BA3C32A075E004595E0B7A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9_CustomAttributesCacheGenerator_U3CLoadVideoWithFadingU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB51738CA40EBA5A1083400C83666804A6D24A214(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9_CustomAttributesCacheGenerator_U3CLoadVideoWithFadingU3Ed__23_System_Collections_IEnumerator_Reset_m44DA52D0E0CC3CD7D5F21583F357767AF45EBF94(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9_CustomAttributesCacheGenerator_U3CLoadVideoWithFadingU3Ed__23_System_Collections_IEnumerator_get_Current_m6AEC2F56DE7688F3E09466C723AEBB3ABCA718FE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SphereDemo_t59016787C067365A7A981577502AC5BDC6B911E7_CustomAttributesCacheGenerator__zeroCameraPosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SphereDemo_t59016787C067365A7A981577502AC5BDC6B911E7_CustomAttributesCacheGenerator__allowRecenter(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SphereDemo_t59016787C067365A7A981577502AC5BDC6B911E7_CustomAttributesCacheGenerator__allowVrToggle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SphereDemo_t59016787C067365A7A981577502AC5BDC6B911E7_CustomAttributesCacheGenerator__lockPitch(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[204] = 
{
	U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A_CustomAttributesCacheGenerator,
	U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_CustomAttributesCacheGenerator,
	U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817_CustomAttributesCacheGenerator,
	DynamicPrefab_tFBC886C4E3009A6A81856222A53B2A0F7D3B6E36_CustomAttributesCacheGenerator,
	PrefabImagePairManager_t02A2EF0A6C8CCDBB935F9DF00EAFE57290E84349_CustomAttributesCacheGenerator,
	ApplyToMaterial_tFE4B04F713C8BB44F9541EC00B32BA808DE07965_CustomAttributesCacheGenerator,
	ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2_CustomAttributesCacheGenerator,
	AudioOutput_t2338563FDAACA576F36819FB7D9308856E5D1297_CustomAttributesCacheGenerator,
	CubemapCube_t0C4B6C987C5A1B2005D9B02E871ACD1E1C905963_CustomAttributesCacheGenerator,
	DebugOverlay_tA47024917BBBBE5921A20043B20D790B906B56A1_CustomAttributesCacheGenerator,
	DisplayBackground_t089575A140150F58308B9696F0CD5E0BA7E85D7E_CustomAttributesCacheGenerator,
	DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_CustomAttributesCacheGenerator,
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator,
	U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB_CustomAttributesCacheGenerator,
	U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2_CustomAttributesCacheGenerator,
	U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010_CustomAttributesCacheGenerator,
	PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator,
	SubtitlesUGUI_tC568ED275DD251EC273AB15366CD1A3E39034225_CustomAttributesCacheGenerator,
	UpdateStereoMaterial_tEBB7F84D0D948F277458D08721511D3AAF825075_CustomAttributesCacheGenerator,
	AutoRotate_tD3FCE4EA299571DD8C213BB0EACF8FC27AFCD314_CustomAttributesCacheGenerator,
	U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator,
	CameraRotator_t93FDDAA18A400F46A922F8F948BDF616B424CEA4_CustomAttributesCacheGenerator_mouseRotateSpeed,
	CameraRotator_t93FDDAA18A400F46A922F8F948BDF616B424CEA4_CustomAttributesCacheGenerator_touchRotateSpeed,
	CameraRotator_t93FDDAA18A400F46A922F8F948BDF616B424CEA4_CustomAttributesCacheGenerator_slerpValue,
	CameraRotator_t93FDDAA18A400F46A922F8F948BDF616B424CEA4_CustomAttributesCacheGenerator_rotateMethod,
	IntroScreenInteraction_t3B2F28011E885686E5DDB54652F686C0BFA73A28_CustomAttributesCacheGenerator_arTrackedImageManager,
	VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579_CustomAttributesCacheGenerator_mediaPlayerA,
	VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579_CustomAttributesCacheGenerator_displayUGUIMain,
	DynamicPrefab_tFBC886C4E3009A6A81856222A53B2A0F7D3B6E36_CustomAttributesCacheGenerator_m_AlternativePrefab,
	PrefabImagePairManager_t02A2EF0A6C8CCDBB935F9DF00EAFE57290E84349_CustomAttributesCacheGenerator_m_PrefabsList,
	PrefabImagePairManager_t02A2EF0A6C8CCDBB935F9DF00EAFE57290E84349_CustomAttributesCacheGenerator_m_ImageLibrary,
	PrefabImagePairManager_t02A2EF0A6C8CCDBB935F9DF00EAFE57290E84349_CustomAttributesCacheGenerator_introScreenInteraction,
	ApplyToMaterial_tFE4B04F713C8BB44F9541EC00B32BA808DE07965_CustomAttributesCacheGenerator__media,
	ApplyToMaterial_tFE4B04F713C8BB44F9541EC00B32BA808DE07965_CustomAttributesCacheGenerator__defaultTexture,
	ApplyToMaterial_tFE4B04F713C8BB44F9541EC00B32BA808DE07965_CustomAttributesCacheGenerator__material,
	ApplyToMaterial_tFE4B04F713C8BB44F9541EC00B32BA808DE07965_CustomAttributesCacheGenerator__texturePropertyName,
	ApplyToMaterial_tFE4B04F713C8BB44F9541EC00B32BA808DE07965_CustomAttributesCacheGenerator__offset,
	ApplyToMaterial_tFE4B04F713C8BB44F9541EC00B32BA808DE07965_CustomAttributesCacheGenerator__scale,
	ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2_CustomAttributesCacheGenerator__media,
	ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2_CustomAttributesCacheGenerator__defaultTexture,
	ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2_CustomAttributesCacheGenerator__mesh,
	ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2_CustomAttributesCacheGenerator__texturePropertyName,
	ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2_CustomAttributesCacheGenerator__offset,
	ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2_CustomAttributesCacheGenerator__scale,
	AudioChannelMixer_tFF855B15B8F6159A608283BAB221888288C34243_CustomAttributesCacheGenerator__channels,
	AudioOutput_t2338563FDAACA576F36819FB7D9308856E5D1297_CustomAttributesCacheGenerator__mediaPlayer,
	AudioOutput_t2338563FDAACA576F36819FB7D9308856E5D1297_CustomAttributesCacheGenerator__channelMask,
	CubemapCube_t0C4B6C987C5A1B2005D9B02E871ACD1E1C905963_CustomAttributesCacheGenerator__material,
	CubemapCube_t0C4B6C987C5A1B2005D9B02E871ACD1E1C905963_CustomAttributesCacheGenerator__mediaPlayer,
	CubemapCube_t0C4B6C987C5A1B2005D9B02E871ACD1E1C905963_CustomAttributesCacheGenerator_expansion_coeff,
	CubemapCube_t0C4B6C987C5A1B2005D9B02E871ACD1E1C905963_CustomAttributesCacheGenerator__layout,
	DebugOverlay_tA47024917BBBBE5921A20043B20D790B906B56A1_CustomAttributesCacheGenerator__mediaPlayer,
	DebugOverlay_tA47024917BBBBE5921A20043B20D790B906B56A1_CustomAttributesCacheGenerator__guiDepth,
	DebugOverlay_tA47024917BBBBE5921A20043B20D790B906B56A1_CustomAttributesCacheGenerator__displaySize,
	DebugOverlay_tA47024917BBBBE5921A20043B20D790B906B56A1_CustomAttributesCacheGenerator__displayControls,
	DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_CustomAttributesCacheGenerator__useDepth,
	DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_CustomAttributesCacheGenerator__x,
	DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_CustomAttributesCacheGenerator__y,
	DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_CustomAttributesCacheGenerator__width,
	DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_CustomAttributesCacheGenerator__height,
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator__mediaPlayer,
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator_m_UVRect,
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator__setNativeSize,
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator__scaleMode,
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator__noDefaultDisplay,
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator__displayInEditor,
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator__defaultTexture,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_Volume,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_Balance,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_PlaybackRate,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_ResampleBufferSize,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_Persistent,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_videoMapping,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_AnisoLevel,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_LoadSubtitles,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_SubtitleLocation,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_SubtitlePath,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_AudioHeadTransform,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_AudioFocusEnabled,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_AudioFocusTransform,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_AudioFocusWidthDegrees,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_AudioFocusOffLevelDB,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_events,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_eventMask,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_forceFileFormat,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__pauseMediaOnAppPause,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__playMediaOnAppUnpause,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_sourceSampleRate,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_sourceChannels,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_m_manuallySetAudioSourceProperties,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsWindows,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsMacOSX,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsIOS,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsTVOS,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsAndroid,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsWindowsPhone,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsWindowsUWP,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsWebGL,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator__optionsPS4,
	OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E_CustomAttributesCacheGenerator_httpHeaderJson,
	OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E_CustomAttributesCacheGenerator_base64EncodedKeyBlob,
	OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393_CustomAttributesCacheGenerator_httpHeaderJson,
	OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393_CustomAttributesCacheGenerator_fileOffset,
	MediaPlaylist_t59C2705D3E06C8684FDA895F959FFFEFA8BF4BF8_CustomAttributesCacheGenerator__items,
	MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_fileLocation,
	MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_filePath,
	MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_loop,
	MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_startMode,
	MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_progressMode,
	MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_progressTimeSeconds,
	MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_autoPlay,
	MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_stereoPacking,
	MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_alphaPacking,
	MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_isOverrideTransition,
	MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_overrideTransition,
	MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_overrideTransitionDuration,
	MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF_CustomAttributesCacheGenerator_overrideTransitionEasing,
	PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__playerA,
	PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__playerB,
	PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__playlistAutoProgress,
	PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__autoCloseVideo,
	PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__playlistLoopMode,
	PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__playlist,
	PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__pausePreviousOnTransition,
	PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__nextTransition,
	PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__transitionDuration,
	PlaylistMediaPlayer_tD5F375987F25381FF74E2AFBF27BFA13856BD1F5_CustomAttributesCacheGenerator__transitionEasing,
	SubtitlesUGUI_tC568ED275DD251EC273AB15366CD1A3E39034225_CustomAttributesCacheGenerator__mediaPlayer,
	SubtitlesUGUI_tC568ED275DD251EC273AB15366CD1A3E39034225_CustomAttributesCacheGenerator__text,
	UpdateStereoMaterial_tEBB7F84D0D948F277458D08721511D3AAF825075_CustomAttributesCacheGenerator__camera,
	UpdateStereoMaterial_tEBB7F84D0D948F277458D08721511D3AAF825075_CustomAttributesCacheGenerator__renderer,
	UpdateStereoMaterial_tEBB7F84D0D948F277458D08721511D3AAF825075_CustomAttributesCacheGenerator__forceEyeMode,
	Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876_CustomAttributesCacheGenerator_U3CLastTU3Ek__BackingField,
	Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876_CustomAttributesCacheGenerator_U3CTextureTimeStampU3Ek__BackingField,
	DemoInfo_tFC8CE5653450E76B5095669CD562A80FFD6712B7_CustomAttributesCacheGenerator__description,
	Mapping3D_tD245FFB24F152207D7C99D9AAB180568116C5D27_CustomAttributesCacheGenerator__cubePrefab,
	SampleApp_Multiple_tEC8B83D898EFB18F16A1D2A8A5BA3D26A8141F4A_CustomAttributesCacheGenerator_m_videoPath,
	SampleApp_Multiple_tEC8B83D898EFB18F16A1D2A8A5BA3D26A8141F4A_CustomAttributesCacheGenerator_m_videoLocation,
	ChangeVideoExample_t7A4744ABFD531F36394852968842D3CD8D832B23_CustomAttributesCacheGenerator__mediaPlayer,
	LoadFromBuffer_t4FDF3E21E03A6270CA1D4DABA65CC603A8280189_CustomAttributesCacheGenerator__mp,
	LoadFromBuffer_t4FDF3E21E03A6270CA1D4DABA65CC603A8280189_CustomAttributesCacheGenerator__filename,
	LoadFromBufferInChunks_tFFCF1D595509528875CCD737262722559BE45788_CustomAttributesCacheGenerator__mp,
	LoadFromBufferInChunks_tFFCF1D595509528875CCD737262722559BE45788_CustomAttributesCacheGenerator__filename,
	NativeMediaOpen_t97AA3DE898F9247ECB3A886B6A10BD345FA2D19C_CustomAttributesCacheGenerator__player,
	StartEndPoint_t963F5A2BBE3B9E2CFE0166DCBF7FB0263CD74A5D_CustomAttributesCacheGenerator__startLoopSeconds,
	VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656_CustomAttributesCacheGenerator__mediaPlayer,
	VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656_CustomAttributesCacheGenerator__fadeTimeMs,
	SphereDemo_t59016787C067365A7A981577502AC5BDC6B911E7_CustomAttributesCacheGenerator__zeroCameraPosition,
	SphereDemo_t59016787C067365A7A981577502AC5BDC6B911E7_CustomAttributesCacheGenerator__allowRecenter,
	SphereDemo_t59016787C067365A7A981577502AC5BDC6B911E7_CustomAttributesCacheGenerator__allowVrToggle,
	SphereDemo_t59016787C067365A7A981577502AC5BDC6B911E7_CustomAttributesCacheGenerator__lockPitch,
	PreloaderController_tD3EB462D0A9954E9D9785017331EFFC5C9072801_CustomAttributesCacheGenerator_PreloaderController_DisablePreloaderPanel_m5B3399000BD62311676914B199EF4140B6F37CD5,
	U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A_CustomAttributesCacheGenerator_U3CDisablePreloaderPanelU3Ed__3__ctor_m78A4AD06EDA904CACCAC2FCFE42949BB0D950658,
	U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A_CustomAttributesCacheGenerator_U3CDisablePreloaderPanelU3Ed__3_System_IDisposable_Dispose_mB1FFDC5C6C5877BA63FC0D5EDD299367F062C01E,
	U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A_CustomAttributesCacheGenerator_U3CDisablePreloaderPanelU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5E78A5B68092F270355E899D99CE2D748705A525,
	U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A_CustomAttributesCacheGenerator_U3CDisablePreloaderPanelU3Ed__3_System_Collections_IEnumerator_Reset_mF42974D782E8ABEF2D7C266ACC18B0C5F80EF45D,
	U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A_CustomAttributesCacheGenerator_U3CDisablePreloaderPanelU3Ed__3_System_Collections_IEnumerator_get_Current_mF638D8B9A629459A3E4DE1EAB729369B457AFD82,
	VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579_CustomAttributesCacheGenerator_VideoPlayerController_StartVideoPlaying_mBA95547320C9617A544BC8A692280E0F2040F6EF,
	U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_CustomAttributesCacheGenerator_U3CStartVideoPlayingU3Ed__13__ctor_mA06BAE3A6CF5203156A73FD7AF9108A145AF0EC6,
	U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_CustomAttributesCacheGenerator_U3CStartVideoPlayingU3Ed__13_System_IDisposable_Dispose_m8E449EF5BA253A939DCF11CAE39A4C8355DA6489,
	U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_CustomAttributesCacheGenerator_U3CStartVideoPlayingU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDAAAB33056322C26FB2E7185CA14F33A10CE42D0,
	U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_CustomAttributesCacheGenerator_U3CStartVideoPlayingU3Ed__13_System_Collections_IEnumerator_Reset_m149BF3B02F1A023E95FAC7BBD2A5CF729F128F37,
	U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_CustomAttributesCacheGenerator_U3CStartVideoPlayingU3Ed__13_System_Collections_IEnumerator_get_Current_mEC51760B8923B9C8CC3193FD8D8275B4A5E8FC9B,
	SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5_CustomAttributesCacheGenerator_SpeedCheck_ResetSpeedTestCheck_mA096A0F8602D61EC9D018A6115B092ED25F254BC,
	U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817_CustomAttributesCacheGenerator_U3CResetSpeedTestCheckU3Ed__16__ctor_m3D053FE8D19035A8F7E29F2EEF8D7E1892B0B68F,
	U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817_CustomAttributesCacheGenerator_U3CResetSpeedTestCheckU3Ed__16_System_IDisposable_Dispose_mCD152DEBBE8174D003FF33217C5BD072053482C8,
	U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817_CustomAttributesCacheGenerator_U3CResetSpeedTestCheckU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD32DC79D428A42EE1AFBAD15A32C1E8A132EC22,
	U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817_CustomAttributesCacheGenerator_U3CResetSpeedTestCheckU3Ed__16_System_Collections_IEnumerator_Reset_m4DF85D13FFF1B60EFF83963ECAA4A5728B292A01,
	U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817_CustomAttributesCacheGenerator_U3CResetSpeedTestCheckU3Ed__16_System_Collections_IEnumerator_get_Current_m9A98DBB9701B673D1CEE0E764B117C6A24593E87,
	PrefabImagePairManager_t02A2EF0A6C8CCDBB935F9DF00EAFE57290E84349_CustomAttributesCacheGenerator_PrefabImagePairManager_Interaction1Editor_m4130DB0CCFDB7964330885CE97B75CD92DBFC2B6,
	PrefabImagePairManager_t02A2EF0A6C8CCDBB935F9DF00EAFE57290E84349_CustomAttributesCacheGenerator_PrefabImagePairManager_Interaction2Editor_mE918E1C78F95613301EF36F8FD822C257E505ACC,
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator_DisplayUGUI_SetNativeSize_m342646EAEE1E4D4D4AAF41E8DB65AD538C461F39,
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_CustomAttributesCacheGenerator_DisplayUGUI_OnFillVBO_mC3C5089E9453CF286F0E86574C63D47814AF537A,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_MediaPlayer_LoadSubtitlesCoroutine_mED609F44A8A99CD62D067088B7AF30B2FB347B00,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_MediaPlayer_FinalRenderCapture_m58C8CA21EA62D3E54003358527F735B1D3AA5E4D,
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_CustomAttributesCacheGenerator_MediaPlayer_ExtractFrameCoroutine_mD4F518A5EEB7146A9BA59E65744A9B23BC328326,
	U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB_CustomAttributesCacheGenerator_U3CLoadSubtitlesCoroutineU3Ed__166__ctor_mAF47DBCEE88F6F816BE1F9BA4F33F3BAA344957E,
	U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB_CustomAttributesCacheGenerator_U3CLoadSubtitlesCoroutineU3Ed__166_System_IDisposable_Dispose_m7A3DE1124596B52D5185CEE3AEE6322EA3FBF978,
	U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB_CustomAttributesCacheGenerator_U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC6D7BB5CC4CDCB04948293FC163F9E337D46A775,
	U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB_CustomAttributesCacheGenerator_U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_IEnumerator_Reset_m625A44CCFB701C03434E399A2A8AFD83D81C3628,
	U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB_CustomAttributesCacheGenerator_U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_IEnumerator_get_Current_m5653252B6D3E7BDF005249225ABEEB9B867D1C55,
	U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2_CustomAttributesCacheGenerator_U3CFinalRenderCaptureU3Ed__188__ctor_m498CF3113FF234A782856D3BE00DED342F375BEC,
	U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2_CustomAttributesCacheGenerator_U3CFinalRenderCaptureU3Ed__188_System_IDisposable_Dispose_m8A0688DA71BC5C7ED01C0415FD9CDA8D2B52D5F4,
	U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2_CustomAttributesCacheGenerator_U3CFinalRenderCaptureU3Ed__188_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF833D26918BB2E538C49FF8C63CE97F8CBCC3EE,
	U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2_CustomAttributesCacheGenerator_U3CFinalRenderCaptureU3Ed__188_System_Collections_IEnumerator_Reset_m22429013FDB26856E4D1FD9EF1749DD600BF547C,
	U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2_CustomAttributesCacheGenerator_U3CFinalRenderCaptureU3Ed__188_System_Collections_IEnumerator_get_Current_m021BA9DE6276EB6F40BC8FFA6746895F17044C30,
	U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010_CustomAttributesCacheGenerator_U3CExtractFrameCoroutineU3Ed__209__ctor_m42A11F4ACB5EABD7F3B2FF42F17377F8988B55F0,
	U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010_CustomAttributesCacheGenerator_U3CExtractFrameCoroutineU3Ed__209_System_IDisposable_Dispose_m2813638F893988A28291B082C41B4C2CCFA5E316,
	U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010_CustomAttributesCacheGenerator_U3CExtractFrameCoroutineU3Ed__209_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EB04B181B3AF0D8F31D45DB7FE4073A601B9FD7,
	U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010_CustomAttributesCacheGenerator_U3CExtractFrameCoroutineU3Ed__209_System_Collections_IEnumerator_Reset_m03E84C88DE36A133F5C616284E160B269D57B779,
	U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010_CustomAttributesCacheGenerator_U3CExtractFrameCoroutineU3Ed__209_System_Collections_IEnumerator_get_Current_mC4C324C31F1A54748026CBCAA87DC42C1925C5A7,
	Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876_CustomAttributesCacheGenerator_Resampler_get_LastT_m14664B1A60E1A35DEC05847B385D1B0F20D53BC4,
	Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876_CustomAttributesCacheGenerator_Resampler_set_LastT_mA6EEBE901C8F444E4C0755399E984A17348F798F,
	Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876_CustomAttributesCacheGenerator_Resampler_get_TextureTimeStamp_mE0BF12F7A977BE1B3461A8315EB26D1BB0654205,
	Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876_CustomAttributesCacheGenerator_Resampler_set_TextureTimeStamp_m45BAB50E6FF3E9CB4BC9F410FD36A311781FDAF8,
	SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65_CustomAttributesCacheGenerator_SimpleController_LoadVideoWithFading_mEA1895F18C5A29022FFB95B240850EF57A01DEBE,
	U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9_CustomAttributesCacheGenerator_U3CLoadVideoWithFadingU3Ed__23__ctor_m82DE7FDB17C4F4F5D90E14A3642D40C45237FA91,
	U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9_CustomAttributesCacheGenerator_U3CLoadVideoWithFadingU3Ed__23_System_IDisposable_Dispose_mF6B335E78513CCE1A4BA3C32A075E004595E0B7A,
	U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9_CustomAttributesCacheGenerator_U3CLoadVideoWithFadingU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB51738CA40EBA5A1083400C83666804A6D24A214,
	U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9_CustomAttributesCacheGenerator_U3CLoadVideoWithFadingU3Ed__23_System_Collections_IEnumerator_Reset_m44DA52D0E0CC3CD7D5F21583F357767AF45EBF94,
	U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9_CustomAttributesCacheGenerator_U3CLoadVideoWithFadingU3Ed__23_System_Collections_IEnumerator_get_Current_m6AEC2F56DE7688F3E09466C723AEBB3ABCA718FE,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
