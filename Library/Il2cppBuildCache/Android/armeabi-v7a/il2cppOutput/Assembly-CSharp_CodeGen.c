﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AnimController::Update()
extern void AnimController_Update_m62BF22EDB961D930B5C47C37CD39A85B9FD6BCD6 (void);
// 0x00000002 System.Void AnimController::OnVideoSeekSlider()
extern void AnimController_OnVideoSeekSlider_m97E38D19C384286555B65C9BF061D334E9A7DD53 (void);
// 0x00000003 System.Void AnimController::OnVideoSliderDown()
extern void AnimController_OnVideoSliderDown_mA0269AD69FFE9BD501C66C3B6CE35E7D0A4D8BDB (void);
// 0x00000004 System.Void AnimController::OnVideoSliderUp()
extern void AnimController_OnVideoSliderUp_m3E3DC3C4026ADC7B7777B4C0A38C68E46AF51915 (void);
// 0x00000005 System.Boolean AnimController::AnimatorIsPlaying()
extern void AnimController_AnimatorIsPlaying_m9E3D3BD45749386977C5609A3C0EAFCA660D45DF (void);
// 0x00000006 System.Void AnimController::OnPlayButtonClick()
extern void AnimController_OnPlayButtonClick_mBDE53ED2D5DF7A3CA639B181EDD192F2AE098770 (void);
// 0x00000007 System.Void AnimController::OnPauseButtonClick()
extern void AnimController_OnPauseButtonClick_m44347B3B4BAA4361522DE815DCBFE48131422B3C (void);
// 0x00000008 System.Void AnimController::.ctor()
extern void AnimController__ctor_mAAE2BBA5BF1073E17D9467F41758607C12ADEE81 (void);
// 0x00000009 System.Void CameraRotator::Awake()
extern void CameraRotator_Awake_m5AA3F2BBC511618BECD9E5EA08D71750D4522200 (void);
// 0x0000000A System.Void CameraRotator::Start()
extern void CameraRotator_Start_mC6E87BF2AB2273215EA011F2270A709A26DDD1F1 (void);
// 0x0000000B System.Void CameraRotator::Update()
extern void CameraRotator_Update_m4F5601D14C4E9D02EFF48E41B31E793DCDCB9818 (void);
// 0x0000000C System.Void CameraRotator::LateUpdate()
extern void CameraRotator_LateUpdate_m2D6A96DB32FB22127B3D23F84638FE647935C534 (void);
// 0x0000000D System.Void CameraRotator::SetCamPos()
extern void CameraRotator_SetCamPos_m526BABFF2202592FD21411706FD3630E6E9A1F47 (void);
// 0x0000000E System.Void CameraRotator::.ctor()
extern void CameraRotator__ctor_mD803289E569C0F7265EBAD8FCDC37C5F62D7A8BB (void);
// 0x0000000F System.Void HotspotTap::Start()
extern void HotspotTap_Start_mACCDD1BC5002355168339126DA00732691780C4A (void);
// 0x00000010 System.Void HotspotTap::OnButtonTap()
extern void HotspotTap_OnButtonTap_m93BFD0C52481FC75D28CED13C89190213A0F20C2 (void);
// 0x00000011 System.Void HotspotTap::.ctor()
extern void HotspotTap__ctor_mEE35A82DB49A8D49E927ED579F25934B1F68E159 (void);
// 0x00000012 System.Void IntroScreenInteraction::OnEnable()
extern void IntroScreenInteraction_OnEnable_mF86035837AD455CFEBFE3161338EA0C7FC08CEC8 (void);
// 0x00000013 System.Void IntroScreenInteraction::OnValueChange(System.Single)
extern void IntroScreenInteraction_OnValueChange_m82EABD7C220921A30D8D0F4A0CED16F8620CEACF (void);
// 0x00000014 System.Void IntroScreenInteraction::InitUI()
extern void IntroScreenInteraction_InitUI_m153FB04F2A1F4E32CB6D16CAC3AFED2211EAEF4A (void);
// 0x00000015 System.Void IntroScreenInteraction::ToggleARFoundationImageTracking(System.Boolean)
extern void IntroScreenInteraction_ToggleARFoundationImageTracking_m289318574A11CC5F04260A2DE4DDA1F619133811 (void);
// 0x00000016 System.Void IntroScreenInteraction::.ctor()
extern void IntroScreenInteraction__ctor_mE876D0F3D3D9BCF02E104722D05DBD23B45836E8 (void);
// 0x00000017 System.Void MediaPlayerConfig::Awake()
extern void MediaPlayerConfig_Awake_m45FA577B2B9AC5C2931BBD9371BA897017D3E9A0 (void);
// 0x00000018 System.Void MediaPlayerConfig::.ctor()
extern void MediaPlayerConfig__ctor_m9E9E71C77DB5E1EF5922239A716F38DCA62DEB1B (void);
// 0x00000019 System.Void ObjectPositionUpdate::Awake()
extern void ObjectPositionUpdate_Awake_m4F27CD742E2A60BF6262945F870D718A25642CEB (void);
// 0x0000001A System.Void ObjectPositionUpdate::OnEnable()
extern void ObjectPositionUpdate_OnEnable_m07FE1D359D60E197369ACE7DA91D0EC1D5E39E15 (void);
// 0x0000001B System.Void ObjectPositionUpdate::LateUpdate()
extern void ObjectPositionUpdate_LateUpdate_m02514C4458EDA9045C6828B55CFDC9345C827BE3 (void);
// 0x0000001C System.Void ObjectPositionUpdate::.ctor()
extern void ObjectPositionUpdate__ctor_m611A04727AD89618B6322B3E6AA4D04C5AF6D747 (void);
// 0x0000001D System.Void PreloaderController::Start()
extern void PreloaderController_Start_mC76B8BFBE38CB643C446F6EFFA3AE956A168A755 (void);
// 0x0000001E System.Collections.IEnumerator PreloaderController::DisablePreloaderPanel()
extern void PreloaderController_DisablePreloaderPanel_m5B3399000BD62311676914B199EF4140B6F37CD5 (void);
// 0x0000001F System.Void PreloaderController::.ctor()
extern void PreloaderController__ctor_mBB2B6D0AF69CB8F8372E1A80AD7CD8FE60F790AF (void);
// 0x00000020 System.Void PreloaderController/<DisablePreloaderPanel>d__3::.ctor(System.Int32)
extern void U3CDisablePreloaderPanelU3Ed__3__ctor_m78A4AD06EDA904CACCAC2FCFE42949BB0D950658 (void);
// 0x00000021 System.Void PreloaderController/<DisablePreloaderPanel>d__3::System.IDisposable.Dispose()
extern void U3CDisablePreloaderPanelU3Ed__3_System_IDisposable_Dispose_mB1FFDC5C6C5877BA63FC0D5EDD299367F062C01E (void);
// 0x00000022 System.Boolean PreloaderController/<DisablePreloaderPanel>d__3::MoveNext()
extern void U3CDisablePreloaderPanelU3Ed__3_MoveNext_m1295B0BBDDE925FC3BAC462DC722EF59F4301A3E (void);
// 0x00000023 System.Object PreloaderController/<DisablePreloaderPanel>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisablePreloaderPanelU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5E78A5B68092F270355E899D99CE2D748705A525 (void);
// 0x00000024 System.Void PreloaderController/<DisablePreloaderPanel>d__3::System.Collections.IEnumerator.Reset()
extern void U3CDisablePreloaderPanelU3Ed__3_System_Collections_IEnumerator_Reset_mF42974D782E8ABEF2D7C266ACC18B0C5F80EF45D (void);
// 0x00000025 System.Object PreloaderController/<DisablePreloaderPanel>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CDisablePreloaderPanelU3Ed__3_System_Collections_IEnumerator_get_Current_mF638D8B9A629459A3E4DE1EAB729369B457AFD82 (void);
// 0x00000026 System.Void RotateLoader::Update()
extern void RotateLoader_Update_m317AFE64800E56B51476F10523A505852CE33431 (void);
// 0x00000027 System.Void RotateLoader::.ctor()
extern void RotateLoader__ctor_mFE2DD9D37030A6953D0D2851B0EBAA892EE7EDE3 (void);
// 0x00000028 System.Void RotateSphere::.ctor()
extern void RotateSphere__ctor_m48C2ED24E2F00620B54810B89C23B5F4CE52C54A (void);
// 0x00000029 System.Void SafeArea::Awake()
extern void SafeArea_Awake_m83B9F45A0F995E167FCBB289812CA2A8F5286A47 (void);
// 0x0000002A System.Void SafeArea::Update()
extern void SafeArea_Update_mC20518F2F8D9C1949F1E15D905E3C505D55B7A56 (void);
// 0x0000002B System.Void SafeArea::Refresh()
extern void SafeArea_Refresh_m7802DCD0A93F2BBE509BEB7B43EB519B9671A9F1 (void);
// 0x0000002C UnityEngine.Rect SafeArea::GetSafeArea()
extern void SafeArea_GetSafeArea_mA3F3655E84713B922EE78D97FFBE3A80DA1B09C9 (void);
// 0x0000002D System.Void SafeArea::ApplySafeArea(UnityEngine.Rect)
extern void SafeArea_ApplySafeArea_m12670F705115000C12AFBC1F426E71E558115A87 (void);
// 0x0000002E System.Void SafeArea::.ctor()
extern void SafeArea__ctor_m8BEAF767B44EA70C714E36C298FC7DF3F810C3E5 (void);
// 0x0000002F System.Void SphereRotation::Start()
extern void SphereRotation_Start_mB8D37EA2CD913BC0C058A6B36A0027A939E927EA (void);
// 0x00000030 System.Void SphereRotation::Update()
extern void SphereRotation_Update_m12DF0B48D4E34CA74B87CC3A368A29D908CF5403 (void);
// 0x00000031 System.Void SphereRotation::.ctor()
extern void SphereRotation__ctor_mDC557F1C3ED6E800FA70137DE8C55CF043E99339 (void);
// 0x00000032 System.Void VideoTimeDisplay::Start()
extern void VideoTimeDisplay_Start_mFA4D6284EDA3B3AB7D7022BEAB98C9527968AB22 (void);
// 0x00000033 System.Void VideoTimeDisplay::Update()
extern void VideoTimeDisplay_Update_m524CCDF8A8837D4396BCE9549A1880CB038A73D1 (void);
// 0x00000034 System.Void VideoTimeDisplay::.ctor()
extern void VideoTimeDisplay__ctor_m5CD881BC6D41DF1DA09C7632EEBC7D037CCCAB93 (void);
// 0x00000035 System.Void OptionPanelManager::LoadScene(System.String)
extern void OptionPanelManager_LoadScene_mD7098291659EE92D1FAD0B25FDF5F4C97B9FD249 (void);
// 0x00000036 System.Void OptionPanelManager::.ctor()
extern void OptionPanelManager__ctor_m81960761A72AB856A636A4C63E9720B065D36F6C (void);
// 0x00000037 System.Void VideoPlayerController::Start()
extern void VideoPlayerController_Start_mBCAB3295960ECCA9AD99039F2F176B1D9E6A8FCA (void);
// 0x00000038 System.Void VideoPlayerController::Update()
extern void VideoPlayerController_Update_m348487057CC7EB8BAB768D127A11DBDE72BC0861 (void);
// 0x00000039 System.Void VideoPlayerController::InitComponent()
extern void VideoPlayerController_InitComponent_mFA3B6FF2C70D94290C042B40C7763AB528EF02E6 (void);
// 0x0000003A System.Collections.IEnumerator VideoPlayerController::StartVideoPlaying()
extern void VideoPlayerController_StartVideoPlaying_mBA95547320C9617A544BC8A692280E0F2040F6EF (void);
// 0x0000003B System.Void VideoPlayerController::OnVideoButtonClick(System.String)
extern void VideoPlayerController_OnVideoButtonClick_m4524089D8D6A643A7F464F8EAA9BFC87321154AC (void);
// 0x0000003C System.Void VideoPlayerController::.ctor()
extern void VideoPlayerController__ctor_m37CB855AE1301D1DD45D07E62CB382896A550D46 (void);
// 0x0000003D System.Void VideoPlayerController/<StartVideoPlaying>d__13::.ctor(System.Int32)
extern void U3CStartVideoPlayingU3Ed__13__ctor_mA06BAE3A6CF5203156A73FD7AF9108A145AF0EC6 (void);
// 0x0000003E System.Void VideoPlayerController/<StartVideoPlaying>d__13::System.IDisposable.Dispose()
extern void U3CStartVideoPlayingU3Ed__13_System_IDisposable_Dispose_m8E449EF5BA253A939DCF11CAE39A4C8355DA6489 (void);
// 0x0000003F System.Boolean VideoPlayerController/<StartVideoPlaying>d__13::MoveNext()
extern void U3CStartVideoPlayingU3Ed__13_MoveNext_m5012F6C3645CCBD3E50073D4D641AF494BD1A12B (void);
// 0x00000040 System.Object VideoPlayerController/<StartVideoPlaying>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartVideoPlayingU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDAAAB33056322C26FB2E7185CA14F33A10CE42D0 (void);
// 0x00000041 System.Void VideoPlayerController/<StartVideoPlaying>d__13::System.Collections.IEnumerator.Reset()
extern void U3CStartVideoPlayingU3Ed__13_System_Collections_IEnumerator_Reset_m149BF3B02F1A023E95FAC7BBD2A5CF729F128F37 (void);
// 0x00000042 System.Object VideoPlayerController/<StartVideoPlaying>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CStartVideoPlayingU3Ed__13_System_Collections_IEnumerator_get_Current_mEC51760B8923B9C8CC3193FD8D8275B4A5E8FC9B (void);
// 0x00000043 RenderHeads.Media.AVProVideo.MediaPlayer VideoVCR::get_PlayingPlayer()
extern void VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B (void);
// 0x00000044 RenderHeads.Media.AVProVideo.MediaPlayer VideoVCR::get_LoadingPlayer()
extern void VideoVCR_get_LoadingPlayer_m8B3CEF700E0B37E6D1287D8F37E9264BF9C1B67E (void);
// 0x00000045 System.Void VideoVCR::Start()
extern void VideoVCR_Start_m833675486A1F6707D25A6F64A4D84F9C5EEEA3B8 (void);
// 0x00000046 System.Void VideoVCR::OnOpenVideoFile()
extern void VideoVCR_OnOpenVideoFile_m63A5EFAA337F2DE7919B1A623776E3B85CE4E310 (void);
// 0x00000047 System.Void VideoVCR::Update()
extern void VideoVCR_Update_m3D389660CD8DB181C6F3BB44D523AB359057F6F4 (void);
// 0x00000048 System.Void VideoVCR::OnVideoEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void VideoVCR_OnVideoEvent_m5A55B048083EAAD1F09A39BA3416B3F44100CD7F (void);
// 0x00000049 System.Void VideoVCR::OnNewMediaReady()
extern void VideoVCR_OnNewMediaReady_mEDC2336A6CEA55B77B6A3A7631F66019A527CF21 (void);
// 0x0000004A System.Void VideoVCR::OnVideoSeekSlider()
extern void VideoVCR_OnVideoSeekSlider_m6BA20F479F74EC5A5A6ABBF271ED25B4F5C947AB (void);
// 0x0000004B System.Void VideoVCR::OnVideoSliderDown()
extern void VideoVCR_OnVideoSliderDown_mCBE099D7D6C532F1EA05175253E3FB4050B67548 (void);
// 0x0000004C System.Void VideoVCR::OnVideoSliderUp()
extern void VideoVCR_OnVideoSliderUp_m57DAAB9784ADC718852E2572DABAC44546C1EAE1 (void);
// 0x0000004D System.Void VideoVCR::.ctor()
extern void VideoVCR__ctor_m44F9A9C7A3BCA8F31ED617BF04AE57613DDF616A (void);
// 0x0000004E System.Void WagonWheelManager::PlayAnimation(System.Int32)
extern void WagonWheelManager_PlayAnimation_m0D365F74357CC854514D501A3BD3CBC4E2D95D5F (void);
// 0x0000004F System.Void WagonWheelManager::.ctor()
extern void WagonWheelManager__ctor_mF1AA3FD185EFA8F56E870987C826728546C4B21E (void);
// 0x00000050 System.Void Crosstales.OnlineCheck.Demo.SpeedCheck::Start()
extern void SpeedCheck_Start_m456F2C4F0D2EA69AF308C61043A0101F78F0F6B0 (void);
// 0x00000051 System.Void Crosstales.OnlineCheck.Demo.SpeedCheck::OnDestroy()
extern void SpeedCheck_OnDestroy_m17BAC76C7A267C8E5CBD7AEABB0A7DB2249FA5DF (void);
// 0x00000052 System.Void Crosstales.OnlineCheck.Demo.SpeedCheck::OnClickSpeedInfoBtn()
extern void SpeedCheck_OnClickSpeedInfoBtn_mA1373B25946BC1E6ED5D6B8CD6485397672F40D0 (void);
// 0x00000053 System.Void Crosstales.OnlineCheck.Demo.SpeedCheck::OnClickSpeedExitBtn()
extern void SpeedCheck_OnClickSpeedExitBtn_m46ECE177987EBC484FB31F4140094BE7054598EC (void);
// 0x00000054 System.Void Crosstales.OnlineCheck.Demo.SpeedCheck::OnAutoSpeedCheckBtn()
extern void SpeedCheck_OnAutoSpeedCheckBtn_m212E9D130D438DD02E5C7D0FAD55D343D6564CAC (void);
// 0x00000055 System.Void Crosstales.OnlineCheck.Demo.SpeedCheck::OnClickSpeedTestBtn()
extern void SpeedCheck_OnClickSpeedTestBtn_m3F6CB2EB19F3FD247BA9292D5B0CD2EA9893EAAD (void);
// 0x00000056 System.Void Crosstales.OnlineCheck.Demo.SpeedCheck::onTestCompleted(Crosstales.OnlineCheck.Tool.SpeedTestNET.Model.Server,System.Double,System.Double,System.Double)
extern void SpeedCheck_onTestCompleted_m7420BE2FB2AE7EBBA8EC88A99FC43B58F7EC72A1 (void);
// 0x00000057 System.Collections.IEnumerator Crosstales.OnlineCheck.Demo.SpeedCheck::ResetSpeedTestCheck()
extern void SpeedCheck_ResetSpeedTestCheck_mA096A0F8602D61EC9D018A6115B092ED25F254BC (void);
// 0x00000058 System.Void Crosstales.OnlineCheck.Demo.SpeedCheck::.ctor()
extern void SpeedCheck__ctor_m9F63AD20826DCE3C117DCF1CD46854C4B19F7E5B (void);
// 0x00000059 System.Void Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16::.ctor(System.Int32)
extern void U3CResetSpeedTestCheckU3Ed__16__ctor_m3D053FE8D19035A8F7E29F2EEF8D7E1892B0B68F (void);
// 0x0000005A System.Void Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16::System.IDisposable.Dispose()
extern void U3CResetSpeedTestCheckU3Ed__16_System_IDisposable_Dispose_mCD152DEBBE8174D003FF33217C5BD072053482C8 (void);
// 0x0000005B System.Boolean Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16::MoveNext()
extern void U3CResetSpeedTestCheckU3Ed__16_MoveNext_m2ADEAD718840B9B8FF62BDE9B86A6654B93D2770 (void);
// 0x0000005C System.Object Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetSpeedTestCheckU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD32DC79D428A42EE1AFBAD15A32C1E8A132EC22 (void);
// 0x0000005D System.Void Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16::System.Collections.IEnumerator.Reset()
extern void U3CResetSpeedTestCheckU3Ed__16_System_Collections_IEnumerator_Reset_m4DF85D13FFF1B60EFF83963ECAA4A5728B292A01 (void);
// 0x0000005E System.Object Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CResetSpeedTestCheckU3Ed__16_System_Collections_IEnumerator_get_Current_m9A98DBB9701B673D1CEE0E764B117C6A24593E87 (void);
// 0x0000005F UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.DynamicPrefab::get_alternativePrefab()
extern void DynamicPrefab_get_alternativePrefab_m4B48EB6B3CFE5493EDF32163F0A185A3E0B284F6 (void);
// 0x00000060 System.Void UnityEngine.XR.ARFoundation.Samples.DynamicPrefab::set_alternativePrefab(UnityEngine.GameObject)
extern void DynamicPrefab_set_alternativePrefab_mB496995B03DBC0C9F41FE31C6E09E6D711311204 (void);
// 0x00000061 System.Void UnityEngine.XR.ARFoundation.Samples.DynamicPrefab::SetError(System.String)
extern void DynamicPrefab_SetError_mC4122FB4973F14E169E99825FA9A01DFA9A6B061 (void);
// 0x00000062 System.Void UnityEngine.XR.ARFoundation.Samples.DynamicPrefab::Update()
extern void DynamicPrefab_Update_m34D182813AFDCF28D8C664D835F61912273079CF (void);
// 0x00000063 System.Void UnityEngine.XR.ARFoundation.Samples.DynamicPrefab::.ctor()
extern void DynamicPrefab__ctor_m7B0ABDA21E114912AB522C7DBB5413668AE9D425 (void);
// 0x00000064 UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::get_imageLibrary()
extern void PrefabImagePairManager_get_imageLibrary_mF75838258C00F909150CB75F5989903E7AF21286 (void);
// 0x00000065 System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::set_imageLibrary(UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary)
extern void PrefabImagePairManager_set_imageLibrary_m5598C2AC4459C6D5135CB5AC153B803874958BC9 (void);
// 0x00000066 System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::OnBeforeSerialize()
extern void PrefabImagePairManager_OnBeforeSerialize_m5B178C81BC1C1982D01E7CABB3DA6C8152F5F451 (void);
// 0x00000067 System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::OnAfterDeserialize()
extern void PrefabImagePairManager_OnAfterDeserialize_m10CAF6488BD05E15650CCAC2D8DDFDBEBE4C6D53 (void);
// 0x00000068 System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::Awake()
extern void PrefabImagePairManager_Awake_m5D1BADF276ACEBE409796C5783389B4DDBC6A076 (void);
// 0x00000069 System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::OnEnable()
extern void PrefabImagePairManager_OnEnable_mA5D8272C6A44CC8ED3E26144CC2424F0B3F48133 (void);
// 0x0000006A System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::OnDisable()
extern void PrefabImagePairManager_OnDisable_mC339AF0AAEBDB53FEC7E643E1A69D9560A23890C (void);
// 0x0000006B System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::OnTrackedImagesChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void PrefabImagePairManager_OnTrackedImagesChanged_m8950068F3FE83FBE39C0D38F1EA06FA58F82DF5E (void);
// 0x0000006C System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::Interaction1Editor()
extern void PrefabImagePairManager_Interaction1Editor_m4130DB0CCFDB7964330885CE97B75CD92DBFC2B6 (void);
// 0x0000006D System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::Interaction2Editor()
extern void PrefabImagePairManager_Interaction2Editor_mE918E1C78F95613301EF36F8FD822C257E505ACC (void);
// 0x0000006E System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::AssignPrefab(UnityEngine.XR.ARFoundation.ARTrackedImage)
extern void PrefabImagePairManager_AssignPrefab_m3A612DEED64B82D56C3FAACF8BF4E46A7729BA72 (void);
// 0x0000006F System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::AssignInstantiatedPrefab(UnityEngine.XR.ARFoundation.ARTrackedImage)
extern void PrefabImagePairManager_AssignInstantiatedPrefab_mA596673AAE1AC44CA632516170F79C12D234ED1D (void);
// 0x00000070 System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::DisableInstansiatedPrefab()
extern void PrefabImagePairManager_DisableInstansiatedPrefab_m30442AD60122D69FBAEC93CD05A5684D3EFAED2B (void);
// 0x00000071 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::GetPrefabForReferenceImage(UnityEngine.XR.ARSubsystems.XRReferenceImage)
extern void PrefabImagePairManager_GetPrefabForReferenceImage_m37CE15147B141D8ECAF155CDD7BBEAF86C109364 (void);
// 0x00000072 System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::SetPrefabForReferenceImage(UnityEngine.XR.ARSubsystems.XRReferenceImage,UnityEngine.GameObject)
extern void PrefabImagePairManager_SetPrefabForReferenceImage_m660F9185F078D8492942F596D6596986DCF68DC8 (void);
// 0x00000073 System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::LoadScene(System.String)
extern void PrefabImagePairManager_LoadScene_m503C6FB01E169E2675FA2B9C423B26A260CAA8A5 (void);
// 0x00000074 System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::.ctor()
extern void PrefabImagePairManager__ctor_m706F23D05C16A9911D7637808AE62800AFFE6760 (void);
// 0x00000075 System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager/NamedPrefab::.ctor(System.Guid,UnityEngine.GameObject)
extern void NamedPrefab__ctor_m4C5A1EAC954851954E10ADF5F4ED646967BAB252 (void);
// 0x00000076 RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.ApplyToMaterial::get_Player()
extern void ApplyToMaterial_get_Player_mE4A108466F43FD42227D8BB5692BD2F06EAF94AD (void);
// 0x00000077 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::set_Player(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void ApplyToMaterial_set_Player_mA02FDAAF0E3D063CE10B8F1923905C678222DD5C (void);
// 0x00000078 UnityEngine.Texture2D RenderHeads.Media.AVProVideo.ApplyToMaterial::get_DefaultTexture()
extern void ApplyToMaterial_get_DefaultTexture_m180433D010CA7CD13A61A527B0F3E2AD9222BF43 (void);
// 0x00000079 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::set_DefaultTexture(UnityEngine.Texture2D)
extern void ApplyToMaterial_set_DefaultTexture_m251DF798497E0D3BBD8F91BF02782059C50AD184 (void);
// 0x0000007A UnityEngine.Material RenderHeads.Media.AVProVideo.ApplyToMaterial::get_Material()
extern void ApplyToMaterial_get_Material_mFD0C0967C87536D80A3E4FC431B5B9819BCCC836 (void);
// 0x0000007B System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::set_Material(UnityEngine.Material)
extern void ApplyToMaterial_set_Material_mFD09E367E205B61D54F95F27970C922DDBFD672A (void);
// 0x0000007C System.String RenderHeads.Media.AVProVideo.ApplyToMaterial::get_TexturePropertyName()
extern void ApplyToMaterial_get_TexturePropertyName_m35529C314DBB9AEA691C1953B031F90AF4ECE4DB (void);
// 0x0000007D System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::set_TexturePropertyName(System.String)
extern void ApplyToMaterial_set_TexturePropertyName_m018C21AA0720812E6994511E85BC522767069B5A (void);
// 0x0000007E UnityEngine.Vector2 RenderHeads.Media.AVProVideo.ApplyToMaterial::get_Offset()
extern void ApplyToMaterial_get_Offset_m64B3B9C240C9270049F71AE82DA7F84DF132EDDA (void);
// 0x0000007F System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::set_Offset(UnityEngine.Vector2)
extern void ApplyToMaterial_set_Offset_mEFF618E2CC642A2FB7300DED24ECB7C10DF0F61E (void);
// 0x00000080 UnityEngine.Vector2 RenderHeads.Media.AVProVideo.ApplyToMaterial::get_Scale()
extern void ApplyToMaterial_get_Scale_mBC0A076D273D5B03ABE2F5F4FCCD92FCECBF3492 (void);
// 0x00000081 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::set_Scale(UnityEngine.Vector2)
extern void ApplyToMaterial_set_Scale_mA4F35496AC29B976312D4F0FC8DE8C955451655C (void);
// 0x00000082 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::Awake()
extern void ApplyToMaterial_Awake_mF57A0CD8AE163E94A9912335CB9982CDB6D51262 (void);
// 0x00000083 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::ChangeMediaPlayer(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void ApplyToMaterial_ChangeMediaPlayer_m09A51D5A1FD589173D510ACC0C87E0DD8D3CDC03 (void);
// 0x00000084 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::ForceUpdate()
extern void ApplyToMaterial_ForceUpdate_m055C83BFB481FD3A4B710438ADBE419461A2C1D4 (void);
// 0x00000085 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::OnMediaPlayerEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void ApplyToMaterial_OnMediaPlayerEvent_m69F8A3A958E48868103553214AB759BBA1363457 (void);
// 0x00000086 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::LateUpdate()
extern void ApplyToMaterial_LateUpdate_m646817D3F38932B8F643009F46C0CB2781AF8BD7 (void);
// 0x00000087 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::ApplyMapping(UnityEngine.Texture,System.Boolean,System.Int32)
extern void ApplyToMaterial_ApplyMapping_mE2AAA37264E1C02C0E6F95C65F70B75F0015BBEC (void);
// 0x00000088 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::Start()
extern void ApplyToMaterial_Start_m044ECBD6843E30F94943595D9C4F368D31AED3CE (void);
// 0x00000089 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::OnEnable()
extern void ApplyToMaterial_OnEnable_m035F0B8317DE8F40D4EE130F87DB645B734522CD (void);
// 0x0000008A System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::OnDisable()
extern void ApplyToMaterial_OnDisable_m80981B9631CE358B1E1E565CE03317BE2A1D6275 (void);
// 0x0000008B System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::SaveProperties()
extern void ApplyToMaterial_SaveProperties_m6D55871801FDC6D646FE5BA89560C1C69FDBA64B (void);
// 0x0000008C System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::RestoreProperties()
extern void ApplyToMaterial_RestoreProperties_mB23F25A23023C5361FBA87F66EFC7DAA45E6CCEF (void);
// 0x0000008D System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::.ctor()
extern void ApplyToMaterial__ctor_mFE05BA02F293D7CF587666EC6EC9E729038D9D7D (void);
// 0x0000008E RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.ApplyToMesh::get_Player()
extern void ApplyToMesh_get_Player_m34DFE94E5CF989DDA86FA1D4EE3A90B6BA1C5EAF (void);
// 0x0000008F System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::set_Player(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void ApplyToMesh_set_Player_m502F3E617C6AC30DF92BA41929CB134EB2F84049 (void);
// 0x00000090 UnityEngine.Texture2D RenderHeads.Media.AVProVideo.ApplyToMesh::get_DefaultTexture()
extern void ApplyToMesh_get_DefaultTexture_m9359CB062A2495DD0EEE5E557F806C33DCDEF4CD (void);
// 0x00000091 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::set_DefaultTexture(UnityEngine.Texture2D)
extern void ApplyToMesh_set_DefaultTexture_mBBF396CED119E409BD6B7E67E262B930A2C290BC (void);
// 0x00000092 UnityEngine.Renderer RenderHeads.Media.AVProVideo.ApplyToMesh::get_MeshRenderer()
extern void ApplyToMesh_get_MeshRenderer_mBEAC1E6DDF2B16EAF4660459DA1F72257E9D5637 (void);
// 0x00000093 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::set_MeshRenderer(UnityEngine.Renderer)
extern void ApplyToMesh_set_MeshRenderer_m761841DE09113CCDBC474B53A57DD8E73F2DEDC9 (void);
// 0x00000094 System.String RenderHeads.Media.AVProVideo.ApplyToMesh::get_TexturePropertyName()
extern void ApplyToMesh_get_TexturePropertyName_m4BB67C6E45D4CA9B8857C92D0283B37C0A3737A0 (void);
// 0x00000095 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::set_TexturePropertyName(System.String)
extern void ApplyToMesh_set_TexturePropertyName_mF4B305580BD656494F12364667F234A9CD461631 (void);
// 0x00000096 UnityEngine.Vector2 RenderHeads.Media.AVProVideo.ApplyToMesh::get_Offset()
extern void ApplyToMesh_get_Offset_m962C4C9F6DBFBE2A82764ED86D07B121B277AB71 (void);
// 0x00000097 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::set_Offset(UnityEngine.Vector2)
extern void ApplyToMesh_set_Offset_mD6E67B51416E24E379F879625AF88C180E399FFD (void);
// 0x00000098 UnityEngine.Vector2 RenderHeads.Media.AVProVideo.ApplyToMesh::get_Scale()
extern void ApplyToMesh_get_Scale_mB4B84855E8C37CCEB4CC2B0035F21839A9D611F2 (void);
// 0x00000099 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::set_Scale(UnityEngine.Vector2)
extern void ApplyToMesh_set_Scale_m05D49267C11285C8363B62505347A70B8ECCD579 (void);
// 0x0000009A System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::Awake()
extern void ApplyToMesh_Awake_m4CBAA34D4BF40F623053775EED39C51A86EC0DC2 (void);
// 0x0000009B System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::ForceUpdate()
extern void ApplyToMesh_ForceUpdate_mABC9DB44349E5A8FA76A69498FD9E6565588D821 (void);
// 0x0000009C System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::OnMediaPlayerEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void ApplyToMesh_OnMediaPlayerEvent_m33DADB5C28E62DB87A2A26525A8366D48423CFDE (void);
// 0x0000009D System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::ChangeMediaPlayer(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void ApplyToMesh_ChangeMediaPlayer_mD448992D5AF017C53569802DBF70D32982F8C786 (void);
// 0x0000009E System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::LateUpdate()
extern void ApplyToMesh_LateUpdate_mBDF6CB7F02FB0A8F21D144331A8B39F7D90F42F7 (void);
// 0x0000009F System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::ApplyMapping(UnityEngine.Texture,System.Boolean,System.Int32)
extern void ApplyToMesh_ApplyMapping_mE756014E8DC353A4C5CF586F0326208F566CF631 (void);
// 0x000000A0 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::OnEnable()
extern void ApplyToMesh_OnEnable_m6EF3DB604109E050F3F5B352CE89EF656935D0D6 (void);
// 0x000000A1 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::OnDisable()
extern void ApplyToMesh_OnDisable_mA5556CF1F9E0CF01DEC0C2AC3AD94CB052D0DCBC (void);
// 0x000000A2 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::OnDestroy()
extern void ApplyToMesh_OnDestroy_m24D24B6500CAA149ACA31C554D31BA83DC419122 (void);
// 0x000000A3 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::.ctor()
extern void ApplyToMesh__ctor_mAE162E1EDA8B1A4B37E7922883F862EA409BFA5E (void);
// 0x000000A4 System.Single[] RenderHeads.Media.AVProVideo.AudioChannelMixer::get_Channel()
extern void AudioChannelMixer_get_Channel_m99DF99E72031A4B3302AE53ACD3E6A250DFA4113 (void);
// 0x000000A5 System.Void RenderHeads.Media.AVProVideo.AudioChannelMixer::set_Channel(System.Single[])
extern void AudioChannelMixer_set_Channel_m1BCC18098285985B4C6F436501388EF980AE88C4 (void);
// 0x000000A6 System.Void RenderHeads.Media.AVProVideo.AudioChannelMixer::Reset()
extern void AudioChannelMixer_Reset_m5FA298BA5C4469880AC721438A70852F405E7F2B (void);
// 0x000000A7 System.Void RenderHeads.Media.AVProVideo.AudioChannelMixer::ChangeChannelCount(System.Int32)
extern void AudioChannelMixer_ChangeChannelCount_mA8C34DC1D9531D8F2B51A62CA407873549AAA80D (void);
// 0x000000A8 System.Void RenderHeads.Media.AVProVideo.AudioChannelMixer::OnAudioFilterRead(System.Single[],System.Int32)
extern void AudioChannelMixer_OnAudioFilterRead_mC9AC0C032748D23794E33037E08176BFF9C5CC0F (void);
// 0x000000A9 System.Void RenderHeads.Media.AVProVideo.AudioChannelMixer::.ctor()
extern void AudioChannelMixer__ctor_m8CC1FA9D958F49DD8D773727B0F39A6DB6B5E695 (void);
// 0x000000AA System.Void RenderHeads.Media.AVProVideo.AudioOutput::Awake()
extern void AudioOutput_Awake_m2050CC5A872FCFA6F082E504778D622CD91EBE61 (void);
// 0x000000AB System.Void RenderHeads.Media.AVProVideo.AudioOutput::Start()
extern void AudioOutput_Start_m9B921B55716EFE4F69D273E1C6495C07BC4DE846 (void);
// 0x000000AC System.Void RenderHeads.Media.AVProVideo.AudioOutput::OnDestroy()
extern void AudioOutput_OnDestroy_mE641C1F39AB1EB7080264AA2131D9B75A9168D94 (void);
// 0x000000AD System.Void RenderHeads.Media.AVProVideo.AudioOutput::Update()
extern void AudioOutput_Update_m3F2BE6A4F05F06B0697A872D0D65749A35668DA3 (void);
// 0x000000AE System.Void RenderHeads.Media.AVProVideo.AudioOutput::ChangeMediaPlayer(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void AudioOutput_ChangeMediaPlayer_mE93E585604B1A2F1CA0C9A1F90CAAB8D5638349F (void);
// 0x000000AF System.Void RenderHeads.Media.AVProVideo.AudioOutput::OnMediaPlayerEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void AudioOutput_OnMediaPlayerEvent_m4B36BC8A9CE646233797806FEFEF643C70AEB225 (void);
// 0x000000B0 System.Void RenderHeads.Media.AVProVideo.AudioOutput::ApplyAudioSettings(RenderHeads.Media.AVProVideo.MediaPlayer,UnityEngine.AudioSource)
extern void AudioOutput_ApplyAudioSettings_m060ADC350962EF9DF6EB5B91BD459BF87730BF2E (void);
// 0x000000B1 System.Void RenderHeads.Media.AVProVideo.AudioOutput::.ctor()
extern void AudioOutput__ctor_mF095BCD99C59435EAD19BABBE921E8E95FEFD87E (void);
// 0x000000B2 System.Void RenderHeads.Media.AVProVideo.CubemapCube::set_Player(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void CubemapCube_set_Player_mC2561BF5A0A3338AB38E2674D6A3A4C1E6364C27 (void);
// 0x000000B3 RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.CubemapCube::get_Player()
extern void CubemapCube_get_Player_m49D912263C8F85C5E546CBC23C01BE34A2CADE36 (void);
// 0x000000B4 System.Void RenderHeads.Media.AVProVideo.CubemapCube::Awake()
extern void CubemapCube_Awake_m9020A1A0D1DBB7D3D9E7C25AE922CAD5515E5009 (void);
// 0x000000B5 System.Void RenderHeads.Media.AVProVideo.CubemapCube::Start()
extern void CubemapCube_Start_m7E614C30B852BE7C23741004CBE240D7D8DF4B19 (void);
// 0x000000B6 System.Void RenderHeads.Media.AVProVideo.CubemapCube::OnDestroy()
extern void CubemapCube_OnDestroy_m03CF6F16B3D4F7C5A436F4B5A60F859AB35A5E7E (void);
// 0x000000B7 System.Void RenderHeads.Media.AVProVideo.CubemapCube::LateUpdate()
extern void CubemapCube_LateUpdate_m2107AA22FE82E8D44CA16C0C9EE4160882E34677 (void);
// 0x000000B8 System.Void RenderHeads.Media.AVProVideo.CubemapCube::BuildMesh()
extern void CubemapCube_BuildMesh_m4ACC62FD39677EC8809FED75D13BC7A013CDC443 (void);
// 0x000000B9 System.Void RenderHeads.Media.AVProVideo.CubemapCube::UpdateMeshUV(System.Int32,System.Int32,System.Boolean)
extern void CubemapCube_UpdateMeshUV_m9B8A7666F82E6454240303E13D825C047CCCBFD3 (void);
// 0x000000BA System.Void RenderHeads.Media.AVProVideo.CubemapCube::.ctor()
extern void CubemapCube__ctor_mD0E20870247685A4A708E47FD67F733E706B6CA9 (void);
// 0x000000BB System.Boolean RenderHeads.Media.AVProVideo.DebugOverlay::get_DisplayControls()
extern void DebugOverlay_get_DisplayControls_m9E30AE4713563051EBAD98AD1D5B0E6D603E154F (void);
// 0x000000BC System.Void RenderHeads.Media.AVProVideo.DebugOverlay::set_DisplayControls(System.Boolean)
extern void DebugOverlay_set_DisplayControls_m729B63EE184769C8058B9F1C39DD035AB6A75EE6 (void);
// 0x000000BD RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.DebugOverlay::get_CurrentMediaPlayer()
extern void DebugOverlay_get_CurrentMediaPlayer_mF84BBC4DB75C10951360A6BAA9FF59BCBC911773 (void);
// 0x000000BE System.Void RenderHeads.Media.AVProVideo.DebugOverlay::set_CurrentMediaPlayer(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void DebugOverlay_set_CurrentMediaPlayer_m2210A26D6F487CE9E1D8BC09A0854A8F08191DE8 (void);
// 0x000000BF System.Void RenderHeads.Media.AVProVideo.DebugOverlay::SetGuiPositionFromVideoIndex(System.Int32)
extern void DebugOverlay_SetGuiPositionFromVideoIndex_mD7937C27EF45A4075E62F4986E03E308298642FA (void);
// 0x000000C0 System.Void RenderHeads.Media.AVProVideo.DebugOverlay::Update()
extern void DebugOverlay_Update_mBD725BEF647A65E41F94454B5FB689F8962E813B (void);
// 0x000000C1 System.Void RenderHeads.Media.AVProVideo.DebugOverlay::OnGUI()
extern void DebugOverlay_OnGUI_m2619DB638775A95ADBE87BEC38C50927B0806A06 (void);
// 0x000000C2 System.Void RenderHeads.Media.AVProVideo.DebugOverlay::.ctor()
extern void DebugOverlay__ctor_mD49E797F7986FED987E143169289CD93C0C7700A (void);
// 0x000000C3 System.Void RenderHeads.Media.AVProVideo.DisplayBackground::OnRenderObject()
extern void DisplayBackground_OnRenderObject_m7F3E116F14E7C40A84E359A7CB6611DF98A31141 (void);
// 0x000000C4 System.Void RenderHeads.Media.AVProVideo.DisplayBackground::.ctor()
extern void DisplayBackground__ctor_m6DE367A42EBE333582DADA840EA9BFE32FB0201D (void);
// 0x000000C5 System.Void RenderHeads.Media.AVProVideo.DisplayIMGUI::Awake()
extern void DisplayIMGUI_Awake_m5B95BC021AA326F58BC59D9A4200099BF2A9B69A (void);
// 0x000000C6 System.Void RenderHeads.Media.AVProVideo.DisplayIMGUI::Start()
extern void DisplayIMGUI_Start_mC70CFF0D016EE39845E060EA8C8A120F49BDDA04 (void);
// 0x000000C7 System.Void RenderHeads.Media.AVProVideo.DisplayIMGUI::OnDestroy()
extern void DisplayIMGUI_OnDestroy_mCE806FEFAFF8AE38D2985B18EA983935793968BE (void);
// 0x000000C8 UnityEngine.Shader RenderHeads.Media.AVProVideo.DisplayIMGUI::GetRequiredShader()
extern void DisplayIMGUI_GetRequiredShader_mAF9F2F42650530BE702DB36599016A386EF3985F (void);
// 0x000000C9 System.Void RenderHeads.Media.AVProVideo.DisplayIMGUI::Update()
extern void DisplayIMGUI_Update_m1229BAE7E6313AB78108FDC97311D03342B25051 (void);
// 0x000000CA System.Void RenderHeads.Media.AVProVideo.DisplayIMGUI::OnGUI()
extern void DisplayIMGUI_OnGUI_mBCAB4C4D2D87324B9738790E55DC539FF0309144 (void);
// 0x000000CB UnityEngine.Rect RenderHeads.Media.AVProVideo.DisplayIMGUI::GetRect()
extern void DisplayIMGUI_GetRect_mBC72F8C464AB8CCBBF97C94CE0990F33A08ABD9E (void);
// 0x000000CC System.Void RenderHeads.Media.AVProVideo.DisplayIMGUI::.ctor()
extern void DisplayIMGUI__ctor_mD1F1F421BE9F9773F708105410E1C55196FC295A (void);
// 0x000000CD System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::Awake()
extern void DisplayUGUI_Awake_mFF0F97E26986BF0AE3EF5E228FF4D8186945168C (void);
// 0x000000CE System.Boolean RenderHeads.Media.AVProVideo.DisplayUGUI::HasMask(UnityEngine.GameObject)
extern void DisplayUGUI_HasMask_m3141EA6B1347981931F3F88C2048AC87E7281FE1 (void);
// 0x000000CF UnityEngine.Shader RenderHeads.Media.AVProVideo.DisplayUGUI::EnsureShader(UnityEngine.Shader,System.String)
extern void DisplayUGUI_EnsureShader_mB0B2D1E2DA7750E763E65FA9F7CC5DBF89922458 (void);
// 0x000000D0 UnityEngine.Shader RenderHeads.Media.AVProVideo.DisplayUGUI::EnsureAlphaPackingShader()
extern void DisplayUGUI_EnsureAlphaPackingShader_m93178AE0C55535E2B85CBCD9E1D538A667A1116A (void);
// 0x000000D1 UnityEngine.Shader RenderHeads.Media.AVProVideo.DisplayUGUI::EnsureStereoPackingShader()
extern void DisplayUGUI_EnsureStereoPackingShader_m822DB4235CDCF043B81BAD9EA8A183D4AD33AC8F (void);
// 0x000000D2 UnityEngine.Shader RenderHeads.Media.AVProVideo.DisplayUGUI::EnsureAndroidOESShader()
extern void DisplayUGUI_EnsureAndroidOESShader_m5795A04CCEDB9A0C84E37DB0453684D689D07114 (void);
// 0x000000D3 System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::Start()
extern void DisplayUGUI_Start_m95911CAB8F4411CAF241B6BDC09F70A7CC1C6411 (void);
// 0x000000D4 System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::OnDestroy()
extern void DisplayUGUI_OnDestroy_m59A5C1FD22FAA5720979AD4ABE61613246D7D770 (void);
// 0x000000D5 UnityEngine.Shader RenderHeads.Media.AVProVideo.DisplayUGUI::GetRequiredShader()
extern void DisplayUGUI_GetRequiredShader_mDCFE0B8691B2F19FD809B635878FBADF4E98ED80 (void);
// 0x000000D6 UnityEngine.Texture RenderHeads.Media.AVProVideo.DisplayUGUI::get_mainTexture()
extern void DisplayUGUI_get_mainTexture_m0A3AB5D6028B0E6753AC4F70F4E5CDD38B41666F (void);
// 0x000000D7 System.Boolean RenderHeads.Media.AVProVideo.DisplayUGUI::HasValidTexture()
extern void DisplayUGUI_HasValidTexture_mC954E6C7D29FADAFD9139D21AA818B346E723B0F (void);
// 0x000000D8 System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::UpdateInternalMaterial()
extern void DisplayUGUI_UpdateInternalMaterial_mB2A2E82D962F7CE3021149D53F42AE16708DFE62 (void);
// 0x000000D9 System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::LateUpdate()
extern void DisplayUGUI_LateUpdate_mCB3681D3492E0789D8CEB50C2AE80B7352F4A617 (void);
// 0x000000DA RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.DisplayUGUI::get_CurrentMediaPlayer()
extern void DisplayUGUI_get_CurrentMediaPlayer_mBA7C887352D4F639992E7370F4D41E19966E4FC9 (void);
// 0x000000DB System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::set_CurrentMediaPlayer(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void DisplayUGUI_set_CurrentMediaPlayer_mFD3D10BCB177079EF99106CFE592C0F9399C65CA (void);
// 0x000000DC UnityEngine.Rect RenderHeads.Media.AVProVideo.DisplayUGUI::get_uvRect()
extern void DisplayUGUI_get_uvRect_mF1C83C3753E5BE34FD5BC73992CE2D014FEE3DF0 (void);
// 0x000000DD System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::set_uvRect(UnityEngine.Rect)
extern void DisplayUGUI_set_uvRect_mE7DAA23A0D257FA9129BC242BA93E4ABCB464D0A (void);
// 0x000000DE System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::SetNativeSize()
extern void DisplayUGUI_SetNativeSize_m342646EAEE1E4D4D4AAF41E8DB65AD538C461F39 (void);
// 0x000000DF System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void DisplayUGUI_OnPopulateMesh_m7623DA3F87577B0EAB0728789A2F28F01B7FE337 (void);
// 0x000000E0 System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::OnFillVBO(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern void DisplayUGUI_OnFillVBO_mC3C5089E9453CF286F0E86574C63D47814AF537A (void);
// 0x000000E1 System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::_OnFillVBO(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern void DisplayUGUI__OnFillVBO_m20489F28078FE2CE16E4A76FE91E45B173FCFE44 (void);
// 0x000000E2 UnityEngine.Vector4 RenderHeads.Media.AVProVideo.DisplayUGUI::GetDrawingDimensions(UnityEngine.ScaleMode,UnityEngine.Rect&)
extern void DisplayUGUI_GetDrawingDimensions_mC9EDFA84D3A338C66DA05A0357582CB7D125E0FF (void);
// 0x000000E3 System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::.ctor()
extern void DisplayUGUI__ctor_m9EBC66FE1EA44F525B68602AE2224D3584AE7FF1 (void);
// 0x000000E4 System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::.cctor()
extern void DisplayUGUI__cctor_mE5CD163F8E1D998AA11277AE0462B73B59762902 (void);
// 0x000000E5 RenderHeads.Media.AVProVideo.Resampler RenderHeads.Media.AVProVideo.MediaPlayer::get_FrameResampler()
extern void MediaPlayer_get_FrameResampler_mA5D1C9C4E74656A2E8EBD462857C082471B1EF44 (void);
// 0x000000E6 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::get_Persistent()
extern void MediaPlayer_get_Persistent_m6E9CB96C4E83F49915BA1108C9E4EC9593E9908C (void);
// 0x000000E7 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_Persistent(System.Boolean)
extern void MediaPlayer_set_Persistent_m6C91414B9407383D70F9AC2CBD66C7896293002D (void);
// 0x000000E8 RenderHeads.Media.AVProVideo.VideoMapping RenderHeads.Media.AVProVideo.MediaPlayer::get_VideoLayoutMapping()
extern void MediaPlayer_get_VideoLayoutMapping_m8ED94E495E3588064D28C225F4B17861BD737396 (void);
// 0x000000E9 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_VideoLayoutMapping(RenderHeads.Media.AVProVideo.VideoMapping)
extern void MediaPlayer_set_VideoLayoutMapping_mF534FD94CA88B7E75BDF22202D0DE522B2306507 (void);
// 0x000000EA RenderHeads.Media.AVProVideo.IMediaInfo RenderHeads.Media.AVProVideo.MediaPlayer::get_Info()
extern void MediaPlayer_get_Info_m31D89D12C5A2BE09E77590653E52188AC74F4746 (void);
// 0x000000EB RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.MediaPlayer::get_Control()
extern void MediaPlayer_get_Control_m09EF1B68689FAF15E3803E83FD8B1CC4CF9A4EE6 (void);
// 0x000000EC RenderHeads.Media.AVProVideo.IMediaPlayer RenderHeads.Media.AVProVideo.MediaPlayer::get_Player()
extern void MediaPlayer_get_Player_m688CA3F36C5B88A097A30416BFDE70C27875D47E (void);
// 0x000000ED RenderHeads.Media.AVProVideo.IMediaProducer RenderHeads.Media.AVProVideo.MediaPlayer::get_TextureProducer()
extern void MediaPlayer_get_TextureProducer_m38F77755A7195D3385984C2590059B7A3BBAF31F (void);
// 0x000000EE RenderHeads.Media.AVProVideo.IMediaSubtitles RenderHeads.Media.AVProVideo.MediaPlayer::get_Subtitles()
extern void MediaPlayer_get_Subtitles_m9D74A06A13759FE8E694943D56FA46531C9D924F (void);
// 0x000000EF RenderHeads.Media.AVProVideo.MediaPlayerEvent RenderHeads.Media.AVProVideo.MediaPlayer::get_Events()
extern void MediaPlayer_get_Events_mA5CD0868386223CB631AD42B495F8A01D1D63C1D (void);
// 0x000000F0 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::get_VideoOpened()
extern void MediaPlayer_get_VideoOpened_m9B047E21B225B7C05A1635836CD7E6EED748A59C (void);
// 0x000000F1 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::get_PauseMediaOnAppPause()
extern void MediaPlayer_get_PauseMediaOnAppPause_m14DF4EECB146B8E774CBF6C78F1DB3D923B64209 (void);
// 0x000000F2 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_PauseMediaOnAppPause(System.Boolean)
extern void MediaPlayer_set_PauseMediaOnAppPause_m4DCD4C2098E25AF4C8BC0CFC4464B2C011BEAAC4 (void);
// 0x000000F3 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::get_PlayMediaOnAppUnpause()
extern void MediaPlayer_get_PlayMediaOnAppUnpause_m923367EB740D51525A8B8096CA993C0963418C44 (void);
// 0x000000F4 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_PlayMediaOnAppUnpause(System.Boolean)
extern void MediaPlayer_set_PlayMediaOnAppUnpause_mEC4C0321DA42B46A741CAB5DFA25B950A359B890 (void);
// 0x000000F5 RenderHeads.Media.AVProVideo.FileFormat RenderHeads.Media.AVProVideo.MediaPlayer::get_ForceFileFormat()
extern void MediaPlayer_get_ForceFileFormat_m75BF54921E38236F4879E14AAE14972D01B1DC42 (void);
// 0x000000F6 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_ForceFileFormat(RenderHeads.Media.AVProVideo.FileFormat)
extern void MediaPlayer_set_ForceFileFormat_mFE5E142A459082AB602AE5F15D647CEF0110E4E5 (void);
// 0x000000F7 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_AudioHeadTransform(UnityEngine.Transform)
extern void MediaPlayer_set_AudioHeadTransform_mDDCF7045FE909EA02DD378A97ACB3139BF18886E (void);
// 0x000000F8 UnityEngine.Transform RenderHeads.Media.AVProVideo.MediaPlayer::get_AudioHeadTransform()
extern void MediaPlayer_get_AudioHeadTransform_m8538C01175917D5165A7E9E7C904D286D4A0DA6A (void);
// 0x000000F9 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::get_AudioFocusEnabled()
extern void MediaPlayer_get_AudioFocusEnabled_m1DAE492C86B9F9FB0CA8CF3D678D3925591B6DA6 (void);
// 0x000000FA System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_AudioFocusEnabled(System.Boolean)
extern void MediaPlayer_set_AudioFocusEnabled_m5EBCF68B000D74A5A5ED7EAC742AE0D6A0853006 (void);
// 0x000000FB System.Single RenderHeads.Media.AVProVideo.MediaPlayer::get_AudioFocusOffLevelDB()
extern void MediaPlayer_get_AudioFocusOffLevelDB_mFB5664F3DBC37BF72F2AE7DE2C2895951C827280 (void);
// 0x000000FC System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_AudioFocusOffLevelDB(System.Single)
extern void MediaPlayer_set_AudioFocusOffLevelDB_mC3493C18678823B175A84E961433C81B00818233 (void);
// 0x000000FD System.Single RenderHeads.Media.AVProVideo.MediaPlayer::get_AudioFocusWidthDegrees()
extern void MediaPlayer_get_AudioFocusWidthDegrees_m4AD628A0C678E594418E6097AF928781DAC28260 (void);
// 0x000000FE System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_AudioFocusWidthDegrees(System.Single)
extern void MediaPlayer_set_AudioFocusWidthDegrees_mEF4585BFD0DD1729F5B68CA9AA45DB96B357E324 (void);
// 0x000000FF UnityEngine.Transform RenderHeads.Media.AVProVideo.MediaPlayer::get_AudioFocusTransform()
extern void MediaPlayer_get_AudioFocusTransform_m9EA0BCFF10AF45B1212EB8B37CF20B5A8BFFCB87 (void);
// 0x00000100 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_AudioFocusTransform(UnityEngine.Transform)
extern void MediaPlayer_set_AudioFocusTransform_mFDD80C7474172588A4D8D8DDDB39C2E0B96E64DE (void);
// 0x00000101 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsWindows()
extern void MediaPlayer_get_PlatformOptionsWindows_mB04A1E0F25F0FEEDE3AD6DE7A38F19A9BBE94A9C (void);
// 0x00000102 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsMacOSX RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsMacOSX()
extern void MediaPlayer_get_PlatformOptionsMacOSX_m7C394154D29BE6F5F5DA3A596D2934B31928AD71 (void);
// 0x00000103 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsIOS RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsIOS()
extern void MediaPlayer_get_PlatformOptionsIOS_m18AF9591D60E768734738E7D861DDEDA8BBE225E (void);
// 0x00000104 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsTVOS RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsTVOS()
extern void MediaPlayer_get_PlatformOptionsTVOS_mCA2000A95FD7F486DD6EC451D9A6C0A6FE17FB33 (void);
// 0x00000105 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsAndroid()
extern void MediaPlayer_get_PlatformOptionsAndroid_mC6FF33FAE33B28D492A641ACB61887CA8D607A64 (void);
// 0x00000106 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsPhone RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsWindowsPhone()
extern void MediaPlayer_get_PlatformOptionsWindowsPhone_m0CDCF5B8C8BFC20434FC99CD5E45579A9996DE01 (void);
// 0x00000107 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsUWP RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsWindowsUWP()
extern void MediaPlayer_get_PlatformOptionsWindowsUWP_m99E824A214F92807AD51BF8D94E372353AEB92EF (void);
// 0x00000108 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWebGL RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsWebGL()
extern void MediaPlayer_get_PlatformOptionsWebGL_m2C8597C9207355C2165BE1E920117297D65AF2DD (void);
// 0x00000109 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsPS4 RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsPS4()
extern void MediaPlayer_get_PlatformOptionsPS4_m36924934DD4641997B4050EFEF1B37D99D9281B6 (void);
// 0x0000010A System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Awake()
extern void MediaPlayer_Awake_m8047A05C5C88BB633E53DDCE11D45492562B0E38 (void);
// 0x0000010B System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Initialise()
extern void MediaPlayer_Initialise_m128309834479E31C3BA710EF8EB0FB6DDD844F10 (void);
// 0x0000010C System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Start()
extern void MediaPlayer_Start_mC75491DEE917AF37C03558386EA1EEF9244A94FF (void);
// 0x0000010D System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::OpenVideoFromFile(RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation,System.String,System.Boolean)
extern void MediaPlayer_OpenVideoFromFile_mD7D917C6B56F5CC9B76A04D8E4B24219277BF881 (void);
// 0x0000010E System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::OpenVideoFromBuffer(System.Byte[],System.Boolean)
extern void MediaPlayer_OpenVideoFromBuffer_m07835953B9BC11740F437E015962DF20E8A846DB (void);
// 0x0000010F System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::StartOpenChunkedVideoFromBuffer(System.UInt64,System.Boolean)
extern void MediaPlayer_StartOpenChunkedVideoFromBuffer_mBAA741B63075DC5AFA34855DD7A0D9C9F20280A6 (void);
// 0x00000110 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::AddChunkToVideoBuffer(System.Byte[],System.UInt64,System.UInt64)
extern void MediaPlayer_AddChunkToVideoBuffer_mC98E6C5ECE8D4E35CD3752B22560C89B8BC4D53C (void);
// 0x00000111 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::EndOpenChunkedVideoFromBuffer()
extern void MediaPlayer_EndOpenChunkedVideoFromBuffer_mCF5BC7764BFC8267CBCB714C16C52B48BB6A0D1D (void);
// 0x00000112 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::get_SubtitlesEnabled()
extern void MediaPlayer_get_SubtitlesEnabled_m8BEA5B290997DB1F1594BB4D63FA3B4CF5554546 (void);
// 0x00000113 System.String RenderHeads.Media.AVProVideo.MediaPlayer::get_SubtitlePath()
extern void MediaPlayer_get_SubtitlePath_mA4DF6F97D82637A48FBC64B6807FE9A14E86942C (void);
// 0x00000114 RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation RenderHeads.Media.AVProVideo.MediaPlayer::get_SubtitleLocation()
extern void MediaPlayer_get_SubtitleLocation_m4C5F2093153FF318EB69E55E79610D622978D404 (void);
// 0x00000115 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::EnableSubtitles(RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation,System.String)
extern void MediaPlayer_EnableSubtitles_m9D823D2CE08F6A45DF743C39C956CACF159E428F (void);
// 0x00000116 System.Collections.IEnumerator RenderHeads.Media.AVProVideo.MediaPlayer::LoadSubtitlesCoroutine(System.String,RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation,System.String)
extern void MediaPlayer_LoadSubtitlesCoroutine_mED609F44A8A99CD62D067088B7AF30B2FB347B00 (void);
// 0x00000117 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::DisableSubtitles()
extern void MediaPlayer_DisableSubtitles_mE90CCBDA202A190C89435EE61431FF9B7EFA9CD9 (void);
// 0x00000118 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::OpenVideoFromBufferInternal(System.Byte[])
extern void MediaPlayer_OpenVideoFromBufferInternal_m500CA58C2AC38BC44A7867BCA50B03B300DA7A40 (void);
// 0x00000119 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::StartOpenVideoFromBufferInternal(System.UInt64)
extern void MediaPlayer_StartOpenVideoFromBufferInternal_m5FC043B08CE067F82C6A73ED091B2AC60CB19AF8 (void);
// 0x0000011A System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::AddChunkToBufferInternal(System.Byte[],System.UInt64,System.UInt64)
extern void MediaPlayer_AddChunkToBufferInternal_m15D5E059514F18AECBB8AEF35F31A9288B8E9DFB (void);
// 0x0000011B System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::EndOpenVideoFromBufferInternal()
extern void MediaPlayer_EndOpenVideoFromBufferInternal_m660B1978A8C4A40A8752768DBCA8ED61FCBFBB9F (void);
// 0x0000011C System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::OpenVideoFromFile()
extern void MediaPlayer_OpenVideoFromFile_m0CB31F5C91B384E9E901839D6445BFA5B0E520AA (void);
// 0x0000011D System.Void RenderHeads.Media.AVProVideo.MediaPlayer::SetPlaybackOptions()
extern void MediaPlayer_SetPlaybackOptions_m001CE720516D25516A43176E91A931D885750BCD (void);
// 0x0000011E System.Void RenderHeads.Media.AVProVideo.MediaPlayer::CloseVideo()
extern void MediaPlayer_CloseVideo_mB754997E2B47198216C0BFCDF6249EF07B9F1DDD (void);
// 0x0000011F System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Play()
extern void MediaPlayer_Play_m893CBBB81F9F455B7AA764398A50A007D3C28F6C (void);
// 0x00000120 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Pause()
extern void MediaPlayer_Pause_m7194C52FC0A95953B2582B2615AE700418954A94 (void);
// 0x00000121 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Stop()
extern void MediaPlayer_Stop_m132DCE4F115F01FEF30DE82B8E779492F5DABB1C (void);
// 0x00000122 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Rewind(System.Boolean)
extern void MediaPlayer_Rewind_m0CCEAC97B35578A2390B8BB6D91B1DE6E74C4C6E (void);
// 0x00000123 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Update()
extern void MediaPlayer_Update_m9629B44815C23F82A4B92831BB8F059BADDFFA03 (void);
// 0x00000124 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::LateUpdate()
extern void MediaPlayer_LateUpdate_mD2FB1DB825D252C1ABE5642FC9DC2E93609D74DC (void);
// 0x00000125 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::UpdateResampler()
extern void MediaPlayer_UpdateResampler_m0475B150B1145815617029BC13BED0E4E92737E2 (void);
// 0x00000126 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::OnEnable()
extern void MediaPlayer_OnEnable_m897CA0E84D5D0C31B0E4FFCCF5D4DCF638CD37DB (void);
// 0x00000127 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::OnDisable()
extern void MediaPlayer_OnDisable_m78371D06654BB553C440CE1FF966B8FB12184738 (void);
// 0x00000128 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::OnDestroy()
extern void MediaPlayer_OnDestroy_m3C9CB00122C2B00AE6C1A213F45AAAAFC8510374 (void);
// 0x00000129 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::OnApplicationQuit()
extern void MediaPlayer_OnApplicationQuit_mDDCE2EC733D7BAD2CFDBDB4408CDEC4238F0D22D (void);
// 0x0000012A System.Void RenderHeads.Media.AVProVideo.MediaPlayer::StartRenderCoroutine()
extern void MediaPlayer_StartRenderCoroutine_m1B7326C4BD351B016DA8CF7C22FBF6E23F279BB2 (void);
// 0x0000012B System.Void RenderHeads.Media.AVProVideo.MediaPlayer::StopRenderCoroutine()
extern void MediaPlayer_StopRenderCoroutine_m2470F527360D2E59EBE37AEA9554C299BD8C1A4B (void);
// 0x0000012C System.Collections.IEnumerator RenderHeads.Media.AVProVideo.MediaPlayer::FinalRenderCapture()
extern void MediaPlayer_FinalRenderCapture_m58C8CA21EA62D3E54003358527F735B1D3AA5E4D (void);
// 0x0000012D RenderHeads.Media.AVProVideo.Platform RenderHeads.Media.AVProVideo.MediaPlayer::GetPlatform()
extern void MediaPlayer_GetPlatform_mF8F85C1DA7A645F8FD49C6883EE6D6B757DA9EED (void);
// 0x0000012E RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions RenderHeads.Media.AVProVideo.MediaPlayer::GetCurrentPlatformOptions()
extern void MediaPlayer_GetCurrentPlatformOptions_mF9802FA83DE27AF5AD7DC75CCD2A5CD3ACFE9F23 (void);
// 0x0000012F System.String RenderHeads.Media.AVProVideo.MediaPlayer::GetPath(RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation)
extern void MediaPlayer_GetPath_m636FB7C9C4A734D17EB3E751F36F06B7E41D0127 (void);
// 0x00000130 System.String RenderHeads.Media.AVProVideo.MediaPlayer::GetFilePath(System.String,RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation)
extern void MediaPlayer_GetFilePath_mF4E9F9BBD1065AD613CF3668DE8D7D480C711654 (void);
// 0x00000131 System.String RenderHeads.Media.AVProVideo.MediaPlayer::GetPlatformVideoApiString()
extern void MediaPlayer_GetPlatformVideoApiString_mF405C309E21A305CDA7DF801CFE28CDE54E4C2E3 (void);
// 0x00000132 System.Int64 RenderHeads.Media.AVProVideo.MediaPlayer::GetPlatformFileOffset()
extern void MediaPlayer_GetPlatformFileOffset_m8D563F7C0EA9019475A2CE2E861D4589172961D3 (void);
// 0x00000133 System.String RenderHeads.Media.AVProVideo.MediaPlayer::GetPlatformHttpHeaderJson()
extern void MediaPlayer_GetPlatformHttpHeaderJson_m2BDAF4A3F40C19E2B0F4D9C5AD600E16092B3537 (void);
// 0x00000134 System.String RenderHeads.Media.AVProVideo.MediaPlayer::GetPlatformFilePath(RenderHeads.Media.AVProVideo.Platform,System.String&,RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation&)
extern void MediaPlayer_GetPlatformFilePath_m1D468EF33A86AB45181043CC2DA0C2B67FBDE93A (void);
// 0x00000135 RenderHeads.Media.AVProVideo.BaseMediaPlayer RenderHeads.Media.AVProVideo.MediaPlayer::CreatePlatformMediaPlayer()
extern void MediaPlayer_CreatePlatformMediaPlayer_mAB58FF0E922DEAFF7469A110F06F5A73C68C3D63 (void);
// 0x00000136 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::ForceWaitForNewFrame(System.Int32,System.Single)
extern void MediaPlayer_ForceWaitForNewFrame_m1046EED13F8662393B894B54CC23A05D78422818 (void);
// 0x00000137 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::UpdateAudioFocus()
extern void MediaPlayer_UpdateAudioFocus_mE87DD4523D129731752A21DC7EA8576D6CF91C6F (void);
// 0x00000138 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::UpdateAudioHeadTransform()
extern void MediaPlayer_UpdateAudioHeadTransform_m0225CB79EA7CC27C6D1B733131FE02D47C024CFE (void);
// 0x00000139 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::UpdateErrors()
extern void MediaPlayer_UpdateErrors_m0D505DD36E930DB184DA50E7E33C5C5A0FE04834 (void);
// 0x0000013A System.Void RenderHeads.Media.AVProVideo.MediaPlayer::UpdateEvents()
extern void MediaPlayer_UpdateEvents_mED689D58F23FF7DE4DFC378EE47F794282D250D9 (void);
// 0x0000013B System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::IsHandleEvent(RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType)
extern void MediaPlayer_IsHandleEvent_m726602EE6992313D563E5B2047D101FEC2F891AF (void);
// 0x0000013C System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::FireEventIfPossible(RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,System.Boolean)
extern void MediaPlayer_FireEventIfPossible_m293D957FEFC91283E20C5961163D7EF1D2DE70A6 (void);
// 0x0000013D System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::CanFireEvent(RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,System.Boolean)
extern void MediaPlayer_CanFireEvent_m83C6F32B773BFBDE243A3A35E408B3ADA19AE1E0 (void);
// 0x0000013E System.Void RenderHeads.Media.AVProVideo.MediaPlayer::OnApplicationFocus(System.Boolean)
extern void MediaPlayer_OnApplicationFocus_m86226190DEB9CB9C8E6129F0C0E68F83A2EE926D (void);
// 0x0000013F System.Void RenderHeads.Media.AVProVideo.MediaPlayer::OnApplicationPause(System.Boolean)
extern void MediaPlayer_OnApplicationPause_mF42C50F6DCD8006332C2ED6937769D7230EE2152 (void);
// 0x00000140 UnityEngine.Camera RenderHeads.Media.AVProVideo.MediaPlayer::GetDummyCamera()
extern void MediaPlayer_GetDummyCamera_m1ECE958660114FCD2F55A4A438E63A6F704A9DD1 (void);
// 0x00000141 System.Collections.IEnumerator RenderHeads.Media.AVProVideo.MediaPlayer::ExtractFrameCoroutine(UnityEngine.Texture2D,RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame,System.Single,System.Boolean,System.Int32,System.Int32)
extern void MediaPlayer_ExtractFrameCoroutine_mD4F518A5EEB7146A9BA59E65744A9B23BC328326 (void);
// 0x00000142 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::ExtractFrameAsync(UnityEngine.Texture2D,RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame,System.Single,System.Boolean,System.Int32,System.Int32)
extern void MediaPlayer_ExtractFrameAsync_mCD2524D9B9A46380F41C1C1255DEF63F59355B8D (void);
// 0x00000143 UnityEngine.Texture2D RenderHeads.Media.AVProVideo.MediaPlayer::ExtractFrame(UnityEngine.Texture2D,System.Single,System.Boolean,System.Int32,System.Int32)
extern void MediaPlayer_ExtractFrame_mF766C48B5BB0B0A19848DFE5B23F57303D2B7D2F (void);
// 0x00000144 UnityEngine.Texture RenderHeads.Media.AVProVideo.MediaPlayer::ExtractFrame(System.Single,System.Boolean,System.Int32,System.Int32)
extern void MediaPlayer_ExtractFrame_mE9CBD363BE0AA46B764AE61EB8CBDE75FF6ADD38 (void);
// 0x00000145 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::.ctor()
extern void MediaPlayer__ctor_m7AFCA18804DA72195D0AFF38BFE22EE771016DF7 (void);
// 0x00000146 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::.cctor()
extern void MediaPlayer__cctor_mECCF8FF4371383413465A3EA0FECCF53DBA2E4B1 (void);
// 0x00000147 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/Setup::.ctor()
extern void Setup__ctor_m8D471ACF422DEC1844B08DE6298E7EF61F6E9241 (void);
// 0x00000148 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::IsModified()
extern void PlatformOptions_IsModified_m9329B32428B8862D0994558AF2B7C086A9DA8651 (void);
// 0x00000149 System.String RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::GetKeyServerURL()
extern void PlatformOptions_GetKeyServerURL_mE036407B5A12BE2646A522D99A703389C89C44AE (void);
// 0x0000014A System.String RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::GetKeyServerAuthToken()
extern void PlatformOptions_GetKeyServerAuthToken_m5889D3593EEEC1FCC11C2631A9596F009DAFD887 (void);
// 0x0000014B System.String RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::GetDecryptionKey()
extern void PlatformOptions_GetDecryptionKey_mB1D0BF403F838B666D818FA8D5164948838A880D (void);
// 0x0000014C System.String RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::StringAsJsonString(System.String)
extern void PlatformOptions_StringAsJsonString_m7A9A91C0D27FFA0DAFB694F2BFAB563817DDDD1A (void);
// 0x0000014D System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader> RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::ParseJsonHTTPHeadersIntoHTTPHeaderList(System.String)
extern void PlatformOptions_ParseJsonHTTPHeadersIntoHTTPHeaderList_m0C5A914EF74EA3DB7ABCEB1FF75F5B782E38E59D (void);
// 0x0000014E System.Void RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::.ctor()
extern void PlatformOptions__ctor_m0765FB48C8311B03E858B0E7ACB26870CA7E1FD5 (void);
// 0x0000014F System.Void RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader::.ctor(System.String,System.String)
extern void HTTPHeader__ctor_mBD26E64BA6EB557D29C8E5E346802BD3BF6E199E (void);
// 0x00000150 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::IsModified()
extern void OptionsWindows_IsModified_m3D5B03949B095E226B87FDF5C679D1B12AC97950 (void);
// 0x00000151 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::.ctor()
extern void OptionsWindows__ctor_m74A15D2B8EA4695A1B55EC0A9EE37B7447672251 (void);
// 0x00000152 System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::GetHTTPHeadersAsJSON()
extern void OptionsApple_GetHTTPHeadersAsJSON_m6ED8E32FCAB2E1C13FF1EFD6FBA2F51B97E191EE (void);
// 0x00000153 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::IsModified()
extern void OptionsApple_IsModified_m97B8AED361C97F06BDA0D49A52D0019DD5BC53D5 (void);
// 0x00000154 System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::GetKeyServerURL()
extern void OptionsApple_GetKeyServerURL_m0F7BC1C13BDD299CE027EAD0F0205E4EDBE82D46 (void);
// 0x00000155 System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::GetKeyServerAuthToken()
extern void OptionsApple_GetKeyServerAuthToken_m75A801C049884609FBDDE0A31EE4C645D2469384 (void);
// 0x00000156 System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::GetDecryptionKey()
extern void OptionsApple_GetDecryptionKey_m235356057A28338A12FCFCBAE5168FDC352C4510 (void);
// 0x00000157 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::OnBeforeSerialize()
extern void OptionsApple_OnBeforeSerialize_mEC86ADC5FE27FE8262188BBDA14549197D892528 (void);
// 0x00000158 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::OnAfterDeserialize()
extern void OptionsApple_OnAfterDeserialize_mE8385FA36B16F9CD6712D898349E1D31E9FF22AF (void);
// 0x00000159 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::.ctor()
extern void OptionsApple__ctor_m2E26273BAAC19F3078E37CB3B52FC00BBB5EA0CC (void);
// 0x0000015A System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsMacOSX::.ctor()
extern void OptionsMacOSX__ctor_m86B15B9DB09B66C526637AF36DF9DB4B0A6AFB9B (void);
// 0x0000015B System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsIOS::IsModified()
extern void OptionsIOS_IsModified_m2BCBA6E16A9330881C279C03219FE45E5B851A8D (void);
// 0x0000015C System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsIOS::.ctor()
extern void OptionsIOS__ctor_m71CBF1B11498CE1D3234585F5E17D693B145E506 (void);
// 0x0000015D System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsTVOS::.ctor()
extern void OptionsTVOS__ctor_m94BC448264C97F1BF954AA3B81474CD21B946D7A (void);
// 0x0000015E System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::GetHTTPHeadersAsJSON()
extern void OptionsAndroid_GetHTTPHeadersAsJSON_m9E64F1365E2533A0453989BFDAFE29BB83FC727D (void);
// 0x0000015F System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::IsModified()
extern void OptionsAndroid_IsModified_m0F1E3B1CB75E5A9869D889293636CA2939D0C0B3 (void);
// 0x00000160 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::OnBeforeSerialize()
extern void OptionsAndroid_OnBeforeSerialize_m35E7F079E61905E64DE47F08350B60ABED4B6113 (void);
// 0x00000161 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::OnAfterDeserialize()
extern void OptionsAndroid_OnAfterDeserialize_m7AA8F7C03489DC53548184AD191C47F864BAF649 (void);
// 0x00000162 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::.ctor()
extern void OptionsAndroid__ctor_m186BC313EEA6B49A9CF88DD62AF031FC2E992545 (void);
// 0x00000163 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsPhone::IsModified()
extern void OptionsWindowsPhone_IsModified_m9F12B86CC99A844734D7EB5CABB84ABB36AD5762 (void);
// 0x00000164 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsPhone::.ctor()
extern void OptionsWindowsPhone__ctor_mAAD92EF239A0FFDA5EF025204F7DEEBE47A9227C (void);
// 0x00000165 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsUWP::IsModified()
extern void OptionsWindowsUWP_IsModified_m692328A5F9DE126E140EEA459B48E17BEFF762E7 (void);
// 0x00000166 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsUWP::.ctor()
extern void OptionsWindowsUWP__ctor_m8D80FBF23ADE92ACC0A48F4A065B3C5980EEAEA0 (void);
// 0x00000167 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWebGL::IsModified()
extern void OptionsWebGL_IsModified_m582458478E2767579052067E272E029FFF4435AD (void);
// 0x00000168 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWebGL::.ctor()
extern void OptionsWebGL__ctor_mED1A980016C73E0A2B17B9B3C0C6C6CD21ADD66F (void);
// 0x00000169 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsPS4::.ctor()
extern void OptionsPS4__ctor_mF620860B6E7CA8A6C5699D098C5CC345D0ADC151 (void);
// 0x0000016A System.Void RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame::.ctor(System.Object,System.IntPtr)
extern void ProcessExtractedFrame__ctor_m2D45C0063D44914869401FA6A6EEBAE26CC5753A (void);
// 0x0000016B System.Void RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame::Invoke(UnityEngine.Texture2D)
extern void ProcessExtractedFrame_Invoke_m93D4170ADCD901788FA035AD906788CD07C8ED5F (void);
// 0x0000016C System.IAsyncResult RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame::BeginInvoke(UnityEngine.Texture2D,System.AsyncCallback,System.Object)
extern void ProcessExtractedFrame_BeginInvoke_mD54E1E8BC0E9A671D24705DC6221FB4D8B284C2D (void);
// 0x0000016D System.Void RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame::EndInvoke(System.IAsyncResult)
extern void ProcessExtractedFrame_EndInvoke_m95A67F49C84E75C01BDD7931DFA69D12ADEC60C2 (void);
// 0x0000016E System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::.ctor(System.Int32)
extern void U3CLoadSubtitlesCoroutineU3Ed__166__ctor_mAF47DBCEE88F6F816BE1F9BA4F33F3BAA344957E (void);
// 0x0000016F System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::System.IDisposable.Dispose()
extern void U3CLoadSubtitlesCoroutineU3Ed__166_System_IDisposable_Dispose_m7A3DE1124596B52D5185CEE3AEE6322EA3FBF978 (void);
// 0x00000170 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::MoveNext()
extern void U3CLoadSubtitlesCoroutineU3Ed__166_MoveNext_m4517ABFF4B0805678FDC2DC2181EFBF10A2C5E7E (void);
// 0x00000171 System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC6D7BB5CC4CDCB04948293FC163F9E337D46A775 (void);
// 0x00000172 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::System.Collections.IEnumerator.Reset()
extern void U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_IEnumerator_Reset_m625A44CCFB701C03434E399A2A8AFD83D81C3628 (void);
// 0x00000173 System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::System.Collections.IEnumerator.get_Current()
extern void U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_IEnumerator_get_Current_m5653252B6D3E7BDF005249225ABEEB9B867D1C55 (void);
// 0x00000174 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::.ctor(System.Int32)
extern void U3CFinalRenderCaptureU3Ed__188__ctor_m498CF3113FF234A782856D3BE00DED342F375BEC (void);
// 0x00000175 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::System.IDisposable.Dispose()
extern void U3CFinalRenderCaptureU3Ed__188_System_IDisposable_Dispose_m8A0688DA71BC5C7ED01C0415FD9CDA8D2B52D5F4 (void);
// 0x00000176 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::MoveNext()
extern void U3CFinalRenderCaptureU3Ed__188_MoveNext_m3D0C170BAF177C94DFF4DBA6DEA23C3B8B58F119 (void);
// 0x00000177 System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFinalRenderCaptureU3Ed__188_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF833D26918BB2E538C49FF8C63CE97F8CBCC3EE (void);
// 0x00000178 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::System.Collections.IEnumerator.Reset()
extern void U3CFinalRenderCaptureU3Ed__188_System_Collections_IEnumerator_Reset_m22429013FDB26856E4D1FD9EF1749DD600BF547C (void);
// 0x00000179 System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::System.Collections.IEnumerator.get_Current()
extern void U3CFinalRenderCaptureU3Ed__188_System_Collections_IEnumerator_get_Current_m021BA9DE6276EB6F40BC8FFA6746895F17044C30 (void);
// 0x0000017A System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::.ctor(System.Int32)
extern void U3CExtractFrameCoroutineU3Ed__209__ctor_m42A11F4ACB5EABD7F3B2FF42F17377F8988B55F0 (void);
// 0x0000017B System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::System.IDisposable.Dispose()
extern void U3CExtractFrameCoroutineU3Ed__209_System_IDisposable_Dispose_m2813638F893988A28291B082C41B4C2CCFA5E316 (void);
// 0x0000017C System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::MoveNext()
extern void U3CExtractFrameCoroutineU3Ed__209_MoveNext_mCF2B7B2C334A8068ADCA0AA5DD16D8DA1E0A6F75 (void);
// 0x0000017D System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExtractFrameCoroutineU3Ed__209_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EB04B181B3AF0D8F31D45DB7FE4073A601B9FD7 (void);
// 0x0000017E System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::System.Collections.IEnumerator.Reset()
extern void U3CExtractFrameCoroutineU3Ed__209_System_Collections_IEnumerator_Reset_m03E84C88DE36A133F5C616284E160B269D57B779 (void);
// 0x0000017F System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::System.Collections.IEnumerator.get_Current()
extern void U3CExtractFrameCoroutineU3Ed__209_System_Collections_IEnumerator_get_Current_mC4C324C31F1A54748026CBCAA87DC42C1925C5A7 (void);
// 0x00000180 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem> RenderHeads.Media.AVProVideo.MediaPlaylist::get_Items()
extern void MediaPlaylist_get_Items_mC8072512F2147BB99CE39BC0C1078E028502B6B1 (void);
// 0x00000181 System.Boolean RenderHeads.Media.AVProVideo.MediaPlaylist::HasItemAt(System.Int32)
extern void MediaPlaylist_HasItemAt_m1105AAC4E62308C4458754326FAD9CC16164FE35 (void);
// 0x00000182 System.Void RenderHeads.Media.AVProVideo.MediaPlaylist::.ctor()
extern void MediaPlaylist__ctor_m4CC954DBB400E5D53DAD84FFE2EABD297CCEF4E1 (void);
// 0x00000183 System.Void RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem::.ctor()
extern void MediaItem__ctor_m73DE5A23DA10B87FEEF58F5D2586BB873842F64C (void);
// 0x00000184 RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_CurrentPlayer()
extern void PlaylistMediaPlayer_get_CurrentPlayer_m1751355EE254A83806EE901FFA6449B6D08352A6 (void);
// 0x00000185 RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_NextPlayer()
extern void PlaylistMediaPlayer_get_NextPlayer_mA89F35DE1AC4379084FE0D4D4D3EA95D14951DDC (void);
// 0x00000186 RenderHeads.Media.AVProVideo.MediaPlaylist RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_Playlist()
extern void PlaylistMediaPlayer_get_Playlist_m240FC5177F46FC8261C853A3FA27A402A360310A (void);
// 0x00000187 System.Int32 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_PlaylistIndex()
extern void PlaylistMediaPlayer_get_PlaylistIndex_m20A06F17E58AA9D87FFD9C3BEC9077B21539437F (void);
// 0x00000188 RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_PlaylistItem()
extern void PlaylistMediaPlayer_get_PlaylistItem_mA4E871ADF14640FDEAC5316DCBC3A2ED245743ED (void);
// 0x00000189 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/PlaylistLoopMode RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_LoopMode()
extern void PlaylistMediaPlayer_get_LoopMode_mD9CFB9BC55B5EE17B230C9AF4C96A39B7E3E8137 (void);
// 0x0000018A System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::set_LoopMode(RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/PlaylistLoopMode)
extern void PlaylistMediaPlayer_set_LoopMode_mBA3D3DF7E85A00349B1E99F7660C0A8967761FC3 (void);
// 0x0000018B System.Boolean RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_AutoProgress()
extern void PlaylistMediaPlayer_get_AutoProgress_mBFFB849689D77DE5405741D9329B327275DC20DF (void);
// 0x0000018C System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::set_AutoProgress(System.Boolean)
extern void PlaylistMediaPlayer_set_AutoProgress_mFCD8BE1C0679F0718235963464B1BFD76CA03FAD (void);
// 0x0000018D RenderHeads.Media.AVProVideo.IMediaInfo RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_Info()
extern void PlaylistMediaPlayer_get_Info_m91BC4D0078EB0CC1D274F80FB813130286DDDF76 (void);
// 0x0000018E RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_Control()
extern void PlaylistMediaPlayer_get_Control_m2D6BE399D5F6F2C960BA89DCDF8BBE4F7B4FA8B2 (void);
// 0x0000018F RenderHeads.Media.AVProVideo.IMediaProducer RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_TextureProducer()
extern void PlaylistMediaPlayer_get_TextureProducer_m1A4A252AA9F073469C8B561682FF6AD5AC642B3E (void);
// 0x00000190 System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::SwapPlayers()
extern void PlaylistMediaPlayer_SwapPlayers_m895D87766AAC9EE9DD79A3ED6B88CA08CEE6D24F (void);
// 0x00000191 UnityEngine.Texture RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::GetCurrentTexture()
extern void PlaylistMediaPlayer_GetCurrentTexture_mBA3E6A0C5EA4FC014CE70F20ED89350EDA31A760 (void);
// 0x00000192 UnityEngine.Texture RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::GetNextTexture()
extern void PlaylistMediaPlayer_GetNextTexture_mBE518E44FB19D787FC6E6E9D37ABFFA1BAF7E3F5 (void);
// 0x00000193 System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::Awake()
extern void PlaylistMediaPlayer_Awake_m0B04AAD55AA08635C10B7A5DB5B007C1FBC2CB33 (void);
// 0x00000194 System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::OnDestroy()
extern void PlaylistMediaPlayer_OnDestroy_mDC144076293DE79A7A6952C0D6947F60301C08B7 (void);
// 0x00000195 System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::Start()
extern void PlaylistMediaPlayer_Start_m97424C5D29DEBD4074D78B21342765B5680CC154 (void);
// 0x00000196 System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::OnVideoEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void PlaylistMediaPlayer_OnVideoEvent_mE307B1229E23A62D6DF4B84FAEFC3BB4BA40FC4A (void);
// 0x00000197 System.Boolean RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::PrevItem()
extern void PlaylistMediaPlayer_PrevItem_mAE4FBCFC966A108A16555C8E453241DC05242480 (void);
// 0x00000198 System.Boolean RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::NextItem()
extern void PlaylistMediaPlayer_NextItem_m86F07BE1F10805326B8689891DD8B1EAA7B97F7C (void);
// 0x00000199 System.Boolean RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::CanJumpToItem(System.Int32)
extern void PlaylistMediaPlayer_CanJumpToItem_m6FA442232726937A3B3C896EF98B34A4B6AC1C28 (void);
// 0x0000019A System.Boolean RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::JumpToItem(System.Int32)
extern void PlaylistMediaPlayer_JumpToItem_m736307ADA3E4272E8DEE0E5943839BE53D0DE428 (void);
// 0x0000019B System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::OpenVideoFile(RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem)
extern void PlaylistMediaPlayer_OpenVideoFile_mF9A2B693202B8CFBCA3E30F29D20BDFDA17ED24D (void);
// 0x0000019C System.Boolean RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::IsTransitioning()
extern void PlaylistMediaPlayer_IsTransitioning_mAAB05AF108E1D397A74983196FDE9AA144EA1D75 (void);
// 0x0000019D System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::SetTransition(RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Transition,System.Single,RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing/Preset)
extern void PlaylistMediaPlayer_SetTransition_mC9E8F83C2938901C03638117EEA37402CF9A484B (void);
// 0x0000019E System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::Update()
extern void PlaylistMediaPlayer_Update_mF64D00943BB14A6DCE979652CC3C98BB9BA38970 (void);
// 0x0000019F UnityEngine.Texture RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::GetTexture(System.Int32)
extern void PlaylistMediaPlayer_GetTexture_m1BB29461D42B3D57174CA54AF34CD77F08D40970 (void);
// 0x000001A0 System.Int32 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::GetTextureCount()
extern void PlaylistMediaPlayer_GetTextureCount_m64439E3889377765B5D6D8E9922D2E3143C9111F (void);
// 0x000001A1 System.Int32 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::GetTextureFrameCount()
extern void PlaylistMediaPlayer_GetTextureFrameCount_m2DEB36885CFD829DB790F45E19657A22DEEAADE5 (void);
// 0x000001A2 System.Boolean RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::SupportsTextureFrameCount()
extern void PlaylistMediaPlayer_SupportsTextureFrameCount_m74B1AB14B4679209F4E78430D6E1E4653F6B1FE0 (void);
// 0x000001A3 System.Int64 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::GetTextureTimeStamp()
extern void PlaylistMediaPlayer_GetTextureTimeStamp_m64EB4837340D40E20769BDBAA85DCF3A99AC4289 (void);
// 0x000001A4 System.Boolean RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::RequiresVerticalFlip()
extern void PlaylistMediaPlayer_RequiresVerticalFlip_mA3CE7E8CEDF0A6AAF512710BB9900ADAC3454467 (void);
// 0x000001A5 UnityEngine.Matrix4x4 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::GetYpCbCrTransform()
extern void PlaylistMediaPlayer_GetYpCbCrTransform_m28579BE4D3C39DB1F57A0A2D1BB806AFB67A6396 (void);
// 0x000001A6 System.String RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::GetTransitionName(RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Transition)
extern void PlaylistMediaPlayer_GetTransitionName_mF0B1C3DF7A9BD9B5FE388D6EEE22F3F378406AC9 (void);
// 0x000001A7 System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::.ctor()
extern void PlaylistMediaPlayer__ctor_m766DE61F2610CBFA8C3F71C75DC69FFAEAECE2BC (void);
// 0x000001A8 System.Func`2<System.Single,System.Single> RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::GetFunction(RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing/Preset)
extern void Easing_GetFunction_m6BF9C691739142093824E65BF9D0B3959716E3B5 (void);
// 0x000001A9 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::PowerEaseIn(System.Single,System.Single)
extern void Easing_PowerEaseIn_m452740837B6A23AB4DA085C09A56239B2D61E9A3 (void);
// 0x000001AA System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::PowerEaseOut(System.Single,System.Single)
extern void Easing_PowerEaseOut_m9029AE6D2160B8497333B9D381DDA8B364311565 (void);
// 0x000001AB System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::PowerEaseInOut(System.Single,System.Single)
extern void Easing_PowerEaseInOut_m0C91FD6F72C67D6C21A0BAC622714175010F60C8 (void);
// 0x000001AC System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::Step(System.Single)
extern void Easing_Step_m74FF1FA20E4A7FE4232FBD5B0EFBEA036171605C (void);
// 0x000001AD System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::Linear(System.Single)
extern void Easing_Linear_mA407428D581C8BA5C60991186DE52D521D8EEC5B (void);
// 0x000001AE System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InQuad(System.Single)
extern void Easing_InQuad_m835BD34EF6B489D51D8B7E31D5421BC89DF65998 (void);
// 0x000001AF System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::OutQuad(System.Single)
extern void Easing_OutQuad_m810849EFC4CDD6477028DA82FB5006E3428407DD (void);
// 0x000001B0 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InOutQuad(System.Single)
extern void Easing_InOutQuad_mD67729B41C8D095A565A1AEDA43FE0FF363E303B (void);
// 0x000001B1 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InCubic(System.Single)
extern void Easing_InCubic_m0F57ABFDA1DAF00A462FC55900C346C7B4308B08 (void);
// 0x000001B2 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::OutCubic(System.Single)
extern void Easing_OutCubic_m2693C08EED8C8A119C6362B759A27AABFE9C32FF (void);
// 0x000001B3 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InOutCubic(System.Single)
extern void Easing_InOutCubic_m2C0042F5722F61B0CF3B2C63DDADC2C045F41523 (void);
// 0x000001B4 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InQuart(System.Single)
extern void Easing_InQuart_m9C1DD3B8219F88569905FB1B38B88E042A0D3245 (void);
// 0x000001B5 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::OutQuart(System.Single)
extern void Easing_OutQuart_m494232308BA295350A858BE1E2FA475AC3A85FFF (void);
// 0x000001B6 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InOutQuart(System.Single)
extern void Easing_InOutQuart_m2A57949CDE900057F1F9D17C482242DCB35AE50A (void);
// 0x000001B7 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InQuint(System.Single)
extern void Easing_InQuint_m59758F196F7FFE458A32F6F14B5429946A6FB4BC (void);
// 0x000001B8 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::OutQuint(System.Single)
extern void Easing_OutQuint_m0494D176295021B3512BAB04407A172F302A322E (void);
// 0x000001B9 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InOutQuint(System.Single)
extern void Easing_InOutQuint_mA57462E560637C403AD73D55CF03A4ECD29F2C51 (void);
// 0x000001BA System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InExpo(System.Single)
extern void Easing_InExpo_m611AB71C3B0E391EE9824BFB4271CABA990D2AFE (void);
// 0x000001BB System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::OutExpo(System.Single)
extern void Easing_OutExpo_mA71C6B4FE36A0F88D3FD06106B0BE7F3E24B8356 (void);
// 0x000001BC System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InOutExpo(System.Single)
extern void Easing_InOutExpo_mD33AD178CF4F37856E199F4247D55D1B6C904F36 (void);
// 0x000001BD System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::.ctor()
extern void Easing__ctor_m6AAB60805D5F0E34EA019B566FE2E5092839629B (void);
// 0x000001BE System.Void RenderHeads.Media.AVProVideo.StreamParserEvent::.ctor()
extern void StreamParserEvent__ctor_m10D8AECEF91142015F01F1AD53FB6620076ABEEB (void);
// 0x000001BF RenderHeads.Media.AVProVideo.StreamParserEvent RenderHeads.Media.AVProVideo.StreamParser::get_Events()
extern void StreamParser_get_Events_mF85B81A4D0100E1F93469081B01C0DBF08BA5D71 (void);
// 0x000001C0 System.Void RenderHeads.Media.AVProVideo.StreamParser::LoadFile()
extern void StreamParser_LoadFile_m80B6CF097D6F5AB208AE211B53889DDCBB9E7D29 (void);
// 0x000001C1 System.Boolean RenderHeads.Media.AVProVideo.StreamParser::get_Loaded()
extern void StreamParser_get_Loaded_mB6EE8D6717C8F31B7EB9C49B7705AAA2F8F126DB (void);
// 0x000001C2 RenderHeads.Media.AVProVideo.Stream RenderHeads.Media.AVProVideo.StreamParser::get_Root()
extern void StreamParser_get_Root_mDB49D875218539FBF4D3D0CF06B0E90E8DF23632 (void);
// 0x000001C3 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream> RenderHeads.Media.AVProVideo.StreamParser::get_SubStreams()
extern void StreamParser_get_SubStreams_m0DAF07B3B6AAF909DEC0364E5BBD8890E126463F (void);
// 0x000001C4 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream/Chunk> RenderHeads.Media.AVProVideo.StreamParser::get_Chunks()
extern void StreamParser_get_Chunks_mB511155C7EBC54307595D5E96DBBEEDB98823F2C (void);
// 0x000001C5 System.Void RenderHeads.Media.AVProVideo.StreamParser::ParseStream()
extern void StreamParser_ParseStream_m60F0235E79C98DB722D6B66D2B22DF80B6B7CD68 (void);
// 0x000001C6 System.Void RenderHeads.Media.AVProVideo.StreamParser::Start()
extern void StreamParser_Start_mE229B98CDA5625FEF2C2A2D15B031733959DFE63 (void);
// 0x000001C7 System.Void RenderHeads.Media.AVProVideo.StreamParser::.ctor()
extern void StreamParser__ctor_m909BFDB96C07C73380E69E633D9BFC5231CAECA1 (void);
// 0x000001C8 System.Void RenderHeads.Media.AVProVideo.SubtitlesUGUI::Start()
extern void SubtitlesUGUI_Start_mC041EE4B381BF8033D645A45F984A4D7DB0DA7AB (void);
// 0x000001C9 System.Void RenderHeads.Media.AVProVideo.SubtitlesUGUI::OnDestroy()
extern void SubtitlesUGUI_OnDestroy_m0CE8B38E786461A415B46B005667A2AAB3C4C76E (void);
// 0x000001CA System.Void RenderHeads.Media.AVProVideo.SubtitlesUGUI::ChangeMediaPlayer(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void SubtitlesUGUI_ChangeMediaPlayer_mB75F9DA4FEE5F0FB014C48D8AF7AB99C6E96CBA3 (void);
// 0x000001CB System.Void RenderHeads.Media.AVProVideo.SubtitlesUGUI::OnMediaPlayerEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void SubtitlesUGUI_OnMediaPlayerEvent_m0889CE3960F338D3C52F9BC57154AD81309FE4FD (void);
// 0x000001CC System.Void RenderHeads.Media.AVProVideo.SubtitlesUGUI::.ctor()
extern void SubtitlesUGUI__ctor_m385A64B013A82341D0C1AFEF543AA6EF0030BD68 (void);
// 0x000001CD RenderHeads.Media.AVProVideo.StereoEye RenderHeads.Media.AVProVideo.UpdateStereoMaterial::get_ForceEyeMode()
extern void UpdateStereoMaterial_get_ForceEyeMode_mAA88FF989F0306A4DB3CAD03C01C4FCEAAFBE418 (void);
// 0x000001CE System.Void RenderHeads.Media.AVProVideo.UpdateStereoMaterial::set_ForceEyeMode(RenderHeads.Media.AVProVideo.StereoEye)
extern void UpdateStereoMaterial_set_ForceEyeMode_m639E4719904B8B45B6E4526D22828C13C19AB2F1 (void);
// 0x000001CF System.Void RenderHeads.Media.AVProVideo.UpdateStereoMaterial::Awake()
extern void UpdateStereoMaterial_Awake_mA3C7B3BB2DBFAE0AAC82CDFEF23C376815B89C86 (void);
// 0x000001D0 System.Void RenderHeads.Media.AVProVideo.UpdateStereoMaterial::SetupMaterial(UnityEngine.Material,UnityEngine.Camera)
extern void UpdateStereoMaterial_SetupMaterial_mB987CF198D1E92D591D042B972B70FF8014D0D86 (void);
// 0x000001D1 System.Void RenderHeads.Media.AVProVideo.UpdateStereoMaterial::LateUpdate()
extern void UpdateStereoMaterial_LateUpdate_mAE667AD7A7D03A816D7587857476225A3452C917 (void);
// 0x000001D2 System.Void RenderHeads.Media.AVProVideo.UpdateStereoMaterial::.ctor()
extern void UpdateStereoMaterial__ctor_m079674C3F2EB6A8C1E30F41DA6C7882FFC4F156D (void);
// 0x000001D3 System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer::InitialisePlatform()
extern void AndroidMediaPlayer_InitialisePlatform_m480201A9079FA34579B451A400CCC8EE2AD0A46B (void);
// 0x000001D4 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::DeinitPlatform()
extern void AndroidMediaPlayer_DeinitPlatform_m543239108E6CF61381C9A9F232191BB8BA1AE6C7 (void);
// 0x000001D5 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::IssuePluginEvent(RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native/AVPPluginEvent,System.Int32)
extern void AndroidMediaPlayer_IssuePluginEvent_m3E2D662F77DB83B324DE913AAD729E3440DB1933 (void);
// 0x000001D6 System.IntPtr RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetMethod(System.String,System.String)
extern void AndroidMediaPlayer_GetMethod_mB9421A67B0C015294A2243755869A7FD6D2684DD (void);
// 0x000001D7 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::.ctor(System.Boolean,System.Boolean,RenderHeads.Media.AVProVideo.Android/VideoApi,System.Boolean,RenderHeads.Media.AVProVideo.Audio360ChannelMode,System.Boolean)
extern void AndroidMediaPlayer__ctor_m1B15AD791EDF44D15962EA6E6B7A700B19AEC7AC (void);
// 0x000001D8 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::SetOptions(System.Boolean,System.Boolean)
extern void AndroidMediaPlayer_SetOptions_mD5FA3F77CE556E5DCB766ECD3E98153B1A3A13CF (void);
// 0x000001D9 System.Int64 RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetEstimatedTotalBandwidthUsed()
extern void AndroidMediaPlayer_GetEstimatedTotalBandwidthUsed_mD0670C9C445A414188096BAE985C2A13BAA10A79 (void);
// 0x000001DA System.String RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetVersion()
extern void AndroidMediaPlayer_GetVersion_mBEEF6D463C589B2D77E2DC426B086FB0E0EBB767 (void);
// 0x000001DB System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer::OpenVideoFromFile(System.String,System.Int64,System.String,System.UInt32,System.UInt32,System.Int32)
extern void AndroidMediaPlayer_OpenVideoFromFile_m2AE46CD4B2FD52910F379AB05D4132016026668A (void);
// 0x000001DC System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::DisplayLoadFailureSuggestion(System.String)
extern void AndroidMediaPlayer_DisplayLoadFailureSuggestion_m20B6B867CC50C83B70DC675FF0685E3ADF20FF69 (void);
// 0x000001DD RenderHeads.Media.AVProVideo.TimeRange[] RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetSeekableTimeRanges()
extern void AndroidMediaPlayer_GetSeekableTimeRanges_mE768FA5824407CF368593A784AD996977E14D67E (void);
// 0x000001DE System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::CloseVideo()
extern void AndroidMediaPlayer_CloseVideo_m6E563DC56D2E42E28783C06BA460F991DADD2C36 (void);
// 0x000001DF System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::SetLooping(System.Boolean)
extern void AndroidMediaPlayer_SetLooping_m39379D67BC8CB4B5F9EC20125BB1E41463BA33FC (void);
// 0x000001E0 System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer::IsLooping()
extern void AndroidMediaPlayer_IsLooping_m919DC1146B5D67F3BB83B9E9F4A2B845CD04DE32 (void);
// 0x000001E1 System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer::HasVideo()
extern void AndroidMediaPlayer_HasVideo_mF835E7400852494346995C6F047963CC02DE1B0A (void);
// 0x000001E2 System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer::HasAudio()
extern void AndroidMediaPlayer_HasAudio_mFC08845B8D5E21CEE01971D2F231342C013890CE (void);
// 0x000001E3 System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer::HasMetaData()
extern void AndroidMediaPlayer_HasMetaData_m17372F23398AC08EAC9038C9BFAF701D2CCB8D1D (void);
// 0x000001E4 System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer::CanPlay()
extern void AndroidMediaPlayer_CanPlay_mB89A45F797154675CED208F5ADA838B61CDAE3FA (void);
// 0x000001E5 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::Play()
extern void AndroidMediaPlayer_Play_mF361FB6C44A9B93788BC88CA17A5CC5BF19B9C9E (void);
// 0x000001E6 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::Pause()
extern void AndroidMediaPlayer_Pause_m06843D7F1A991D6B8CC54EADAC5212A728E84C5D (void);
// 0x000001E7 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::Stop()
extern void AndroidMediaPlayer_Stop_m3C526DF8017006EB7141BC93CCD86C27A8938059 (void);
// 0x000001E8 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::Seek(System.Single)
extern void AndroidMediaPlayer_Seek_mD1F0B5A7FCF0D9D15A88C3AAF30750C6605B6C63 (void);
// 0x000001E9 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::SeekFast(System.Single)
extern void AndroidMediaPlayer_SeekFast_mC0B7E9D82491E726D96FBB0BF27FD57EA4D55700 (void);
// 0x000001EA System.Single RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetCurrentTimeMs()
extern void AndroidMediaPlayer_GetCurrentTimeMs_m0E09C8DB596112B159C5DE432A1A297AC15AC010 (void);
// 0x000001EB System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::SetPlaybackRate(System.Single)
extern void AndroidMediaPlayer_SetPlaybackRate_m9C7A15A3066C532E4C35BF3C45386AA02C9536A2 (void);
// 0x000001EC System.Single RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetPlaybackRate()
extern void AndroidMediaPlayer_GetPlaybackRate_m1E21FC984F8C8907D65594EF97AAA0E0578E3701 (void);
// 0x000001ED System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::SetAudioHeadRotation(UnityEngine.Quaternion)
extern void AndroidMediaPlayer_SetAudioHeadRotation_m9FB361D7EF747188EE5DD1122A8C127CB6F9ABEA (void);
// 0x000001EE System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::ResetAudioHeadRotation()
extern void AndroidMediaPlayer_ResetAudioHeadRotation_m9E44B2552B23396A9561931320C4EFDCC731CCD6 (void);
// 0x000001EF System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::SetAudioFocusEnabled(System.Boolean)
extern void AndroidMediaPlayer_SetAudioFocusEnabled_m458407BAA9A828D19B84F7A680CE5FDFEB67294F (void);
// 0x000001F0 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::SetAudioFocusProperties(System.Single,System.Single)
extern void AndroidMediaPlayer_SetAudioFocusProperties_mA38D484ED73825A02BB1B77486A0D14E7E2DC55F (void);
// 0x000001F1 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::SetAudioFocusRotation(UnityEngine.Quaternion)
extern void AndroidMediaPlayer_SetAudioFocusRotation_m601BA3B6BED1822A3A359BC64D060B1C4A91DAD5 (void);
// 0x000001F2 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::ResetAudioFocus()
extern void AndroidMediaPlayer_ResetAudioFocus_m69D72B8AA10F623618362399DF3BF22690472FDF (void);
// 0x000001F3 System.Single RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetDurationMs()
extern void AndroidMediaPlayer_GetDurationMs_mCCCA1447E5327A482E743C4C09FDAE6E6EA0B983 (void);
// 0x000001F4 System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetVideoWidth()
extern void AndroidMediaPlayer_GetVideoWidth_m16FE92F5869CEE50248DD6E4275958F281A780AE (void);
// 0x000001F5 System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetVideoHeight()
extern void AndroidMediaPlayer_GetVideoHeight_mC4692049F412A9AD40C1B48C7D9E661470910F6A (void);
// 0x000001F6 System.Single RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetVideoFrameRate()
extern void AndroidMediaPlayer_GetVideoFrameRate_mA568FC635EB0F98B056B6B849530F96CC0052178 (void);
// 0x000001F7 System.Single RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetBufferingProgress()
extern void AndroidMediaPlayer_GetBufferingProgress_m6FCA8697E4FF968F65C10C02CC7BAC104537812A (void);
// 0x000001F8 System.Single RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetVideoDisplayRate()
extern void AndroidMediaPlayer_GetVideoDisplayRate_m3490C43F697872564761EB7551A39055939D58F9 (void);
// 0x000001F9 System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer::IsSeeking()
extern void AndroidMediaPlayer_IsSeeking_m82478BFB427140B55F97E645A4909EB50DE24507 (void);
// 0x000001FA System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer::IsPlaying()
extern void AndroidMediaPlayer_IsPlaying_mCCDF763B05413E25A2EF1C3E06F1BECEBD2F5C32 (void);
// 0x000001FB System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer::IsPaused()
extern void AndroidMediaPlayer_IsPaused_m2E8BA6090923F0FA1599B41D4CEE8BAD9A18E1A6 (void);
// 0x000001FC System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer::IsFinished()
extern void AndroidMediaPlayer_IsFinished_m8359DAECFB7B45EE771A513726263E8305660701 (void);
// 0x000001FD System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer::IsBuffering()
extern void AndroidMediaPlayer_IsBuffering_m338F6D018438F07A428E180AE14F3788F9331B87 (void);
// 0x000001FE UnityEngine.Texture RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetTexture(System.Int32)
extern void AndroidMediaPlayer_GetTexture_m88EFE1420352CD43E68466BC0E871F4CBFB624F6 (void);
// 0x000001FF System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetTextureFrameCount()
extern void AndroidMediaPlayer_GetTextureFrameCount_m2DE2A07B7C6312BCAB7EB9A9E9612761A55697F7 (void);
// 0x00000200 System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer::RequiresVerticalFlip()
extern void AndroidMediaPlayer_RequiresVerticalFlip_mC137D1CF8B11BB589DB78AD45D14FA5B5E33FB27 (void);
// 0x00000201 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::MuteAudio(System.Boolean)
extern void AndroidMediaPlayer_MuteAudio_m16E42D79701DEA22B7618858924154D6C29E0ECB (void);
// 0x00000202 System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer::IsMuted()
extern void AndroidMediaPlayer_IsMuted_mFC0B6D41E4B76B944055D76E6BC1EA53CCCDE5F1 (void);
// 0x00000203 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::SetVolume(System.Single)
extern void AndroidMediaPlayer_SetVolume_m56A858CAF5BFB63DEE97BFEDFFBBB61DBF3DD292 (void);
// 0x00000204 System.Single RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetVolume()
extern void AndroidMediaPlayer_GetVolume_m672CDE50510B244D00380D8319C9B37377264D07 (void);
// 0x00000205 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::SetBalance(System.Single)
extern void AndroidMediaPlayer_SetBalance_m82507EB2574C17FD1BCE36315247615678EBDA8F (void);
// 0x00000206 System.Single RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetBalance()
extern void AndroidMediaPlayer_GetBalance_mE44177BDF3589C3C3781F0271D06660EEA89F58F (void);
// 0x00000207 System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetAudioTrackCount()
extern void AndroidMediaPlayer_GetAudioTrackCount_m8BAE816BAD45339E79788D9789DE1E4C01671934 (void);
// 0x00000208 System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetCurrentAudioTrack()
extern void AndroidMediaPlayer_GetCurrentAudioTrack_mD5F02E3C3EE5D6386A614E5F9DB214D28FE9AC09 (void);
// 0x00000209 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::SetAudioTrack(System.Int32)
extern void AndroidMediaPlayer_SetAudioTrack_mED4B457A3336003AF32C627BDFD0D63DB46CB4A3 (void);
// 0x0000020A System.String RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetCurrentAudioTrackId()
extern void AndroidMediaPlayer_GetCurrentAudioTrackId_m19E248BD4EAE9EC25B12622A781C49D21A3347FC (void);
// 0x0000020B System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetCurrentAudioTrackBitrate()
extern void AndroidMediaPlayer_GetCurrentAudioTrackBitrate_mEF4835D92452A69B93C53E5ABC4194ED19CFAA87 (void);
// 0x0000020C System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetVideoTrackCount()
extern void AndroidMediaPlayer_GetVideoTrackCount_m343670506A273A19D7041ACA203BB771FF1398BD (void);
// 0x0000020D System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetCurrentVideoTrack()
extern void AndroidMediaPlayer_GetCurrentVideoTrack_m2D18EEC7AB27ADE8DD8E6D696EA3AA40BE0FA212 (void);
// 0x0000020E System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::SetVideoTrack(System.Int32)
extern void AndroidMediaPlayer_SetVideoTrack_mBD6EA9410456C268ABC2F6887E338A558D49DC5E (void);
// 0x0000020F System.String RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetCurrentVideoTrackId()
extern void AndroidMediaPlayer_GetCurrentVideoTrackId_m42087352ED729D988BA1647A92159C11C66E46F6 (void);
// 0x00000210 System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetCurrentVideoTrackBitrate()
extern void AndroidMediaPlayer_GetCurrentVideoTrackBitrate_mCC362CFB895705BF8DD9499896C231830923DC92 (void);
// 0x00000211 System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer::WaitForNextFrame(UnityEngine.Camera,System.Int32)
extern void AndroidMediaPlayer_WaitForNextFrame_m74257982CD48A21241DF33116E7B5763319F3EF5 (void);
// 0x00000212 System.Int64 RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetTextureTimeStamp()
extern void AndroidMediaPlayer_GetTextureTimeStamp_m0F46582DD8F72AF16C446D257AB2BF0438B2C8F6 (void);
// 0x00000213 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::Render()
extern void AndroidMediaPlayer_Render_m7D1D31EB1D47F7C3F364B1A1505FCD6BA4B49A8E (void);
// 0x00000214 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::ApplyTextureProperties(UnityEngine.Texture)
extern void AndroidMediaPlayer_ApplyTextureProperties_mCA2AFCDF2E2DBE88B5AF1231D83D9584436CC23A (void);
// 0x00000215 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::OnEnable()
extern void AndroidMediaPlayer_OnEnable_m0669084265C2E598C6783B37E02843CDA9645487 (void);
// 0x00000216 System.Double RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetCurrentDateTimeSecondsSince1970()
extern void AndroidMediaPlayer_GetCurrentDateTimeSecondsSince1970_m1996DEA0135A2B616FF955F17E47EE7F6521F6AD (void);
// 0x00000217 System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::Update()
extern void AndroidMediaPlayer_Update_mAC17B1D28433B63126C6AC5443715190C94ACB59 (void);
// 0x00000218 System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer::PlayerSupportsLinearColorSpace()
extern void AndroidMediaPlayer_PlayerSupportsLinearColorSpace_mC51B9420468C3D9D2057F6AD686893F20B076E56 (void);
// 0x00000219 System.Single[] RenderHeads.Media.AVProVideo.AndroidMediaPlayer::GetTextureTransform()
extern void AndroidMediaPlayer_GetTextureTransform_m5E3801E6B0309F8304C546CA5470E529552892DB (void);
// 0x0000021A System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::Dispose()
extern void AndroidMediaPlayer_Dispose_m5D885EAFC86ED5B4342130B07F10E4DAE88B1B8B (void);
// 0x0000021B System.Void RenderHeads.Media.AVProVideo.AndroidMediaPlayer::.cctor()
extern void AndroidMediaPlayer__cctor_m5A9C337DD25CA992A66E7F7323A58D60319AB992 (void);
// 0x0000021C System.IntPtr RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::GetRenderEventFunc()
extern void Native_GetRenderEventFunc_m8754CC53B2673E22F28728FA5751AE43B8D59288 (void);
// 0x0000021D System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::_GetWidth(System.Int32)
extern void Native__GetWidth_m9E38517D03739EA72116C26F9827A14695F8985B (void);
// 0x0000021E System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::_GetHeight(System.Int32)
extern void Native__GetHeight_m673F6642C668BEAD1B496AE131F9DDEB3B3D917A (void);
// 0x0000021F System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::_GetTextureHandle(System.Int32)
extern void Native__GetTextureHandle_m2ED72AAF09B6E50426219C52804B1797DF195407 (void);
// 0x00000220 System.Int64 RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::_GetDuration(System.Int32)
extern void Native__GetDuration_m13527EA823F50088CFD4894B1397BF81AB4D46FC (void);
// 0x00000221 System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::_GetLastErrorCode(System.Int32)
extern void Native__GetLastErrorCode_mBB2A86F4C659D10A66A21B1380E862D77B55F3A9 (void);
// 0x00000222 System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::_GetFrameCount(System.Int32)
extern void Native__GetFrameCount_m6BDCABAF44BE8EA126AFB183F61E614486B981B7 (void);
// 0x00000223 System.Single RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::_GetVideoDisplayRate(System.Int32)
extern void Native__GetVideoDisplayRate_mD91DA764D68277343C42FB3E237382A6B423451E (void);
// 0x00000224 System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::_CanPlay(System.Int32)
extern void Native__CanPlay_mD6AAADE54253E1AA636C1D12466BC9A8118E5500 (void);
// 0x00000225 RenderHeads.Media.AVProVideo.AudioOutputManager RenderHeads.Media.AVProVideo.AudioOutputManager::get_Instance()
extern void AudioOutputManager_get_Instance_m0FA3BF968DEFAABF81F0320CF0414CF046256E52 (void);
// 0x00000226 System.Void RenderHeads.Media.AVProVideo.AudioOutputManager::.ctor()
extern void AudioOutputManager__ctor_mCE33BCA911A8FC47B76DD802B62E515704DD28D9 (void);
// 0x00000227 System.Void RenderHeads.Media.AVProVideo.AudioOutputManager::RequestAudio(RenderHeads.Media.AVProVideo.AudioOutput,RenderHeads.Media.AVProVideo.MediaPlayer,System.Single[],System.Int32,System.Int32,RenderHeads.Media.AVProVideo.AudioOutput/AudioOutputMode)
extern void AudioOutputManager_RequestAudio_m3857D9FE727B0A158F9C14D8D5B3007A82CE3EE3 (void);
// 0x00000228 System.Void RenderHeads.Media.AVProVideo.AudioOutputManager::GrabAudio(RenderHeads.Media.AVProVideo.MediaPlayer,System.Single[],System.Int32)
extern void AudioOutputManager_GrabAudio_mC8F779789094C4AB7DE1F4184DC67CF832437626 (void);
// 0x00000229 System.Void RenderHeads.Media.AVProVideo.AudioOutputManager::.cctor()
extern void AudioOutputManager__cctor_m93A8EBDA4878BBF3BAE16216FF077C08E146A9F2 (void);
// 0x0000022A System.String RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetVersion()
// 0x0000022B System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::OpenVideoFromFile(System.String,System.Int64,System.String,System.UInt32,System.UInt32,System.Int32)
// 0x0000022C System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::OpenVideoFromBuffer(System.Byte[])
extern void BaseMediaPlayer_OpenVideoFromBuffer_m134E4F1C8165FA91E817C96FBF308309835CF778 (void);
// 0x0000022D System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::StartOpenVideoFromBuffer(System.UInt64)
extern void BaseMediaPlayer_StartOpenVideoFromBuffer_m4C94D7A26125607E3E09ABA045A81856B7810DDC (void);
// 0x0000022E System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::AddChunkToVideoBuffer(System.Byte[],System.UInt64,System.UInt64)
extern void BaseMediaPlayer_AddChunkToVideoBuffer_m602ACCC42A26A01A4D0FA2DBBBE17CE14B60623A (void);
// 0x0000022F System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::EndOpenVideoFromBuffer()
extern void BaseMediaPlayer_EndOpenVideoFromBuffer_m15070526688817C8F1B9FCA520F2B717E1EE4778 (void);
// 0x00000230 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::CloseVideo()
extern void BaseMediaPlayer_CloseVideo_m373660E73B61E71573599E243369E2644FAFBB7E (void);
// 0x00000231 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetLooping(System.Boolean)
// 0x00000232 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsLooping()
// 0x00000233 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::HasMetaData()
// 0x00000234 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::CanPlay()
// 0x00000235 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::Play()
// 0x00000236 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::Pause()
// 0x00000237 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::Stop()
// 0x00000238 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::Rewind()
extern void BaseMediaPlayer_Rewind_m4BAB9CB48D97823AEAB7C080BCC25B6E1D740CE1 (void);
// 0x00000239 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::Seek(System.Single)
// 0x0000023A System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SeekFast(System.Single)
// 0x0000023B System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SeekWithTolerance(System.Single,System.Single,System.Single)
extern void BaseMediaPlayer_SeekWithTolerance_mEC503F8FEC5A80F477E3575FC672BD8ED0490887 (void);
// 0x0000023C System.Single RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCurrentTimeMs()
// 0x0000023D System.Double RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCurrentDateTimeSecondsSince1970()
extern void BaseMediaPlayer_GetCurrentDateTimeSecondsSince1970_m2AE51645D964D68D961DFFC6FA58CAEAC58164FC (void);
// 0x0000023E RenderHeads.Media.AVProVideo.TimeRange[] RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetSeekableTimeRanges()
extern void BaseMediaPlayer_GetSeekableTimeRanges_m08DDB97A81459F64B413B522C83AC8C3FF2F15AB (void);
// 0x0000023F System.Single RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetPlaybackRate()
// 0x00000240 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetPlaybackRate(System.Single)
// 0x00000241 System.Single RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetDurationMs()
// 0x00000242 System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetVideoWidth()
// 0x00000243 System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetVideoHeight()
// 0x00000244 UnityEngine.Rect RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCropRect()
extern void BaseMediaPlayer_GetCropRect_m707AC32D1019217E87B59075DFCF0DC20B50629C (void);
// 0x00000245 System.Single RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetVideoDisplayRate()
// 0x00000246 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::HasAudio()
// 0x00000247 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::HasVideo()
// 0x00000248 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsSeeking()
// 0x00000249 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsPlaying()
// 0x0000024A System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsPaused()
// 0x0000024B System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsFinished()
// 0x0000024C System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsBuffering()
// 0x0000024D System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::WaitForNextFrame(UnityEngine.Camera,System.Int32)
extern void BaseMediaPlayer_WaitForNextFrame_m175788A07BC1B028ED46DA90CEA3FD660D93F5D5 (void);
// 0x0000024E System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetPlayWithoutBuffering(System.Boolean)
extern void BaseMediaPlayer_SetPlayWithoutBuffering_mDAD0D181E0E6E2608A607A4DFD6353AD45A7DDB4 (void);
// 0x0000024F System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetKeyServerURL(System.String)
extern void BaseMediaPlayer_SetKeyServerURL_mE4AA76F9CF0A2A5F309AABDAB03E1FB594D270AB (void);
// 0x00000250 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetKeyServerAuthToken(System.String)
extern void BaseMediaPlayer_SetKeyServerAuthToken_m07C8376EDA9B10AE8A83027026C84AA9012D01E7 (void);
// 0x00000251 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetDecryptionKeyBase64(System.String)
extern void BaseMediaPlayer_SetDecryptionKeyBase64_m3B93AC51A6121F98C2DE0BCA3F33C84B4E2FDC89 (void);
// 0x00000252 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetDecryptionKey(System.Byte[])
extern void BaseMediaPlayer_SetDecryptionKey_m746B002F4B8CCDAAFD62E07F2F1387A7D77C86B5 (void);
// 0x00000253 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsExternalPlaybackSupported()
extern void BaseMediaPlayer_IsExternalPlaybackSupported_mFBF93ED5DFB83E439C71D5B4BFEA49E96261468E (void);
// 0x00000254 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsExternalPlaybackActive()
extern void BaseMediaPlayer_IsExternalPlaybackActive_m21F5436F2AB3B67A7FA552F8491800CF827CE7AE (void);
// 0x00000255 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetAllowsExternalPlayback(System.Boolean)
extern void BaseMediaPlayer_SetAllowsExternalPlayback_m8631EF8181635E2922D869AF101C7B47B7D3ED03 (void);
// 0x00000256 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetExternalPlaybackFillMode(RenderHeads.Media.AVProVideo.ExternalPlaybackFillMode)
extern void BaseMediaPlayer_SetExternalPlaybackFillMode_m5FEBB0CD331B8B72DB72B456EA97C95893956DE7 (void);
// 0x00000257 System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetTextureCount()
extern void BaseMediaPlayer_GetTextureCount_m0445A325571CC9BF9E137116EB8DC7FB266456AD (void);
// 0x00000258 UnityEngine.Texture RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetTexture(System.Int32)
// 0x00000259 System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetTextureFrameCount()
// 0x0000025A System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::SupportsTextureFrameCount()
extern void BaseMediaPlayer_SupportsTextureFrameCount_m15822F5CB71CBA1C8E9A3BF8AE3667669DDB8D01 (void);
// 0x0000025B System.Int64 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetTextureTimeStamp()
extern void BaseMediaPlayer_GetTextureTimeStamp_m84E4AD62112F198FCD4323F72BF9C3C452670D33 (void);
// 0x0000025C System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::RequiresVerticalFlip()
// 0x0000025D System.Single[] RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetTextureTransform()
extern void BaseMediaPlayer_GetTextureTransform_m98D9246501ECC4E8DD87CC6AA859744822D85595 (void);
// 0x0000025E UnityEngine.Matrix4x4 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetYpCbCrTransform()
extern void BaseMediaPlayer_GetYpCbCrTransform_mD00E4468DD772DCDCE9F6212A6E91F67E5BDB32B (void);
// 0x0000025F System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::MuteAudio(System.Boolean)
// 0x00000260 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsMuted()
// 0x00000261 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetVolume(System.Single)
// 0x00000262 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetBalance(System.Single)
extern void BaseMediaPlayer_SetBalance_mE7328C33DDC2481A410B7246F376E78FDA63B095 (void);
// 0x00000263 System.Single RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetVolume()
// 0x00000264 System.Single RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetBalance()
extern void BaseMediaPlayer_GetBalance_mA00225EC805B19E2F047EC08F86DDB66713507AD (void);
// 0x00000265 System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetAudioTrackCount()
// 0x00000266 System.String RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetAudioTrackId(System.Int32)
extern void BaseMediaPlayer_GetAudioTrackId_mA09717258EF38368BCDECE30E869E6FEDF53E7BB (void);
// 0x00000267 System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCurrentAudioTrack()
// 0x00000268 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetAudioTrack(System.Int32)
// 0x00000269 System.String RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCurrentAudioTrackId()
// 0x0000026A System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCurrentAudioTrackBitrate()
// 0x0000026B System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetNumAudioChannels()
extern void BaseMediaPlayer_GetNumAudioChannels_m05C04C2C20BA4DDECD348FBEF310E6F9D02A6A93 (void);
// 0x0000026C System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetAudioHeadRotation(UnityEngine.Quaternion)
extern void BaseMediaPlayer_SetAudioHeadRotation_m0FDB6CCA5645F168962610AAA27C7090FBA4B9AA (void);
// 0x0000026D System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::ResetAudioHeadRotation()
extern void BaseMediaPlayer_ResetAudioHeadRotation_mF18B61A31B1258F93440706C215DD9C39AFD4959 (void);
// 0x0000026E System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetAudioChannelMode(RenderHeads.Media.AVProVideo.Audio360ChannelMode)
extern void BaseMediaPlayer_SetAudioChannelMode_m64D15EDD5249F88EEA97A238599BA58F5AF5DFCB (void);
// 0x0000026F System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetAudioFocusEnabled(System.Boolean)
extern void BaseMediaPlayer_SetAudioFocusEnabled_mE058263D9542C3049C1724662A933C962BEC2184 (void);
// 0x00000270 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetAudioFocusProperties(System.Single,System.Single)
extern void BaseMediaPlayer_SetAudioFocusProperties_m37793B9D6A5AB9DA90758ECBD41955FF19136425 (void);
// 0x00000271 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetAudioFocusRotation(UnityEngine.Quaternion)
extern void BaseMediaPlayer_SetAudioFocusRotation_m5EF05840EF3F89744DD8530A77AA9FDB758A55DF (void);
// 0x00000272 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::ResetAudioFocus()
extern void BaseMediaPlayer_ResetAudioFocus_m078F6C60AF26359CBA240D8E7D1B29BC7D33FC08 (void);
// 0x00000273 System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetVideoTrackCount()
// 0x00000274 System.String RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetVideoTrackId(System.Int32)
extern void BaseMediaPlayer_GetVideoTrackId_m56A282DE65FC35EB45F4D2A8BF023704CD07E08E (void);
// 0x00000275 System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCurrentVideoTrack()
// 0x00000276 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetVideoTrack(System.Int32)
// 0x00000277 System.String RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCurrentVideoTrackId()
// 0x00000278 System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCurrentVideoTrackBitrate()
// 0x00000279 System.Single RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetVideoFrameRate()
// 0x0000027A System.Int64 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetEstimatedTotalBandwidthUsed()
extern void BaseMediaPlayer_GetEstimatedTotalBandwidthUsed_mABDDF3B4A4C4C13DBF0B21A7F4D3355F5EE42378 (void);
// 0x0000027B System.Single RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetBufferingProgress()
// 0x0000027C System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::Update()
// 0x0000027D System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::Render()
// 0x0000027E System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::Dispose()
// 0x0000027F RenderHeads.Media.AVProVideo.ErrorCode RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetLastError()
extern void BaseMediaPlayer_GetLastError_m27835B8FE01C2BD4A3A11F3E19954C6CCE1F9F49 (void);
// 0x00000280 System.Int64 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetLastExtendedErrorCode()
extern void BaseMediaPlayer_GetLastExtendedErrorCode_m9CB3BEEEF755DD537924A8AF3C65A66C2960D0A9 (void);
// 0x00000281 System.String RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetPlayerDescription()
extern void BaseMediaPlayer_GetPlayerDescription_mAC3B33841136795CBCB2392129408A3BE3E23DD1 (void);
// 0x00000282 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::PlayerSupportsLinearColorSpace()
extern void BaseMediaPlayer_PlayerSupportsLinearColorSpace_mEE3FA26619E524C0957C4EF8F4929E2FDBFC55EB (void);
// 0x00000283 System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetBufferedTimeRangeCount()
extern void BaseMediaPlayer_GetBufferedTimeRangeCount_m9C7B5DAD54EAB1715C132CAFA47B49FFA25911C7 (void);
// 0x00000284 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetBufferedTimeRange(System.Int32,System.Single&,System.Single&)
extern void BaseMediaPlayer_GetBufferedTimeRange_m91FC04998E849840BFD38B9C29CEEC454048898B (void);
// 0x00000285 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetTextureProperties(UnityEngine.FilterMode,UnityEngine.TextureWrapMode,System.Int32)
extern void BaseMediaPlayer_SetTextureProperties_m56C85F5335096B90788F5254CDD8152D55C61958 (void);
// 0x00000286 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::ApplyTextureProperties(UnityEngine.Texture)
extern void BaseMediaPlayer_ApplyTextureProperties_m1246CB9C705FD6503D2A83AC72F0EC958BDE8FFB (void);
// 0x00000287 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::GrabAudio(System.Single[],System.Int32,System.Int32)
extern void BaseMediaPlayer_GrabAudio_mF1226A61BCA9163DF537217992D9021F08E77D50 (void);
// 0x00000288 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsExpectingNewVideoFrame()
extern void BaseMediaPlayer_IsExpectingNewVideoFrame_m324814339D1C4706D661DB3F1FAE9BB3471B73ED (void);
// 0x00000289 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsPlaybackStalled()
extern void BaseMediaPlayer_IsPlaybackStalled_m7B55CC9A93E63AB0B8264E9D020E1127E9A61430 (void);
// 0x0000028A System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::LoadSubtitlesSRT(System.String)
extern void BaseMediaPlayer_LoadSubtitlesSRT_m16BFC16E815DA0EE34F7BFE945ECAC8D58984CAF (void);
// 0x0000028B System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::UpdateSubtitles()
extern void BaseMediaPlayer_UpdateSubtitles_m252AA6D660B684D7BA29EDAA192AD1CA2A2A1060 (void);
// 0x0000028C System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetSubtitleIndex()
extern void BaseMediaPlayer_GetSubtitleIndex_mAD1280C3023C0BFB1E6BBD429DA82FD2DABE0ABA (void);
// 0x0000028D System.String RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetSubtitleText()
extern void BaseMediaPlayer_GetSubtitleText_mF785A8FB753F87861E8B49E9D92542B2B62B97AF (void);
// 0x0000028E System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::OnEnable()
extern void BaseMediaPlayer_OnEnable_mB1C7A49A7094946EC40A0C87787370B23FFBD8F3 (void);
// 0x0000028F System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::.ctor()
extern void BaseMediaPlayer__ctor_m78CFF1B03053002A3BC81ED985D97B2C6620DF6F (void);
// 0x00000290 System.Int32 RenderHeads.Media.AVProVideo.HLSStream::get_Width()
extern void HLSStream_get_Width_m16F540A61C08618F2E304A2D3A472EA3D389A93E (void);
// 0x00000291 System.Int32 RenderHeads.Media.AVProVideo.HLSStream::get_Height()
extern void HLSStream_get_Height_m05E464AD1D98CC86B4B1BBAA5156F0FCF3863B00 (void);
// 0x00000292 System.Int32 RenderHeads.Media.AVProVideo.HLSStream::get_Bandwidth()
extern void HLSStream_get_Bandwidth_mC92C78A84A647F52BE28598637440EFB9791B9F1 (void);
// 0x00000293 System.String RenderHeads.Media.AVProVideo.HLSStream::get_URL()
extern void HLSStream_get_URL_m0A2F7A7BD8371A523AC0383B8945DD09A06D57A9 (void);
// 0x00000294 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream/Chunk> RenderHeads.Media.AVProVideo.HLSStream::GetAllChunks()
extern void HLSStream_GetAllChunks_m396DFE284171978B993139E5A214144BBC2CF8DF (void);
// 0x00000295 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream/Chunk> RenderHeads.Media.AVProVideo.HLSStream::GetChunks()
extern void HLSStream_GetChunks_m76E7020A3D3E1BE6B00CBA7E0ACB7A12147C765B (void);
// 0x00000296 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream> RenderHeads.Media.AVProVideo.HLSStream::GetAllStreams()
extern void HLSStream_GetAllStreams_m998B4F10CA609027B6C1EF3A4982E74A640C8FE0 (void);
// 0x00000297 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream> RenderHeads.Media.AVProVideo.HLSStream::GetStreams()
extern void HLSStream_GetStreams_m812E329BA7D9359C66C283A016B88FBCCD1A98B9 (void);
// 0x00000298 System.Boolean RenderHeads.Media.AVProVideo.HLSStream::ExtractStreamInfo(System.String,System.Int32&,System.Int32&,System.Int32&)
extern void HLSStream_ExtractStreamInfo_m5ED2D89F6D261EB618E5C59CC4DBAC5A0E6DFB99 (void);
// 0x00000299 System.Boolean RenderHeads.Media.AVProVideo.HLSStream::IsChunk(System.String)
extern void HLSStream_IsChunk_m6D74B3C9DB8CEE57ACE3824B158155072E7E22CC (void);
// 0x0000029A System.Void RenderHeads.Media.AVProVideo.HLSStream::ParseFile(System.String[],System.String)
extern void HLSStream_ParseFile_m3A9E7F907CEBC0A9753A08AC1494498140259836 (void);
// 0x0000029B System.Void RenderHeads.Media.AVProVideo.HLSStream::.ctor(System.String,System.Int32,System.Int32,System.Int32)
extern void HLSStream__ctor_m08A3FF8331FD0168BB333295FBEE28FA8D58037D (void);
// 0x0000029C System.Boolean RenderHeads.Media.AVProVideo.HLSStream::MyRemoteCertificateValidationCallback(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void HLSStream_MyRemoteCertificateValidationCallback_m64548F918CD1567D632FDBE5C5B55DB1A762D8E6 (void);
// 0x0000029D System.Boolean RenderHeads.Media.AVProVideo.MediaPlayerEvent::HasListeners()
extern void MediaPlayerEvent_HasListeners_mFF8DA58E4E6A89CB253CCD70011ED15F0F9B2A3E (void);
// 0x0000029E System.Void RenderHeads.Media.AVProVideo.MediaPlayerEvent::AddListener(UnityEngine.Events.UnityAction`3<RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode>)
extern void MediaPlayerEvent_AddListener_m9439DDAFF5E3D1DF3772724199060F845ACCAD76 (void);
// 0x0000029F System.Void RenderHeads.Media.AVProVideo.MediaPlayerEvent::RemoveListener(UnityEngine.Events.UnityAction`3<RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode>)
extern void MediaPlayerEvent_RemoveListener_m75D2C5FE8969BC958EAB4F6BFFF13C1AA2929CB9 (void);
// 0x000002A0 System.Void RenderHeads.Media.AVProVideo.MediaPlayerEvent::RemoveAllListeners()
extern void MediaPlayerEvent_RemoveAllListeners_mE6BFC8409069B9F01BC092F141DC626BAC18A5A4 (void);
// 0x000002A1 System.Void RenderHeads.Media.AVProVideo.MediaPlayerEvent::.ctor()
extern void MediaPlayerEvent__ctor_m6DE59D660F6A26BB62D04A29DDECE0868FD8367E (void);
// 0x000002A2 System.Void RenderHeads.Media.AVProVideo.IMediaPlayer::OnEnable()
// 0x000002A3 System.Void RenderHeads.Media.AVProVideo.IMediaPlayer::Update()
// 0x000002A4 System.Void RenderHeads.Media.AVProVideo.IMediaPlayer::Render()
// 0x000002A5 System.Boolean RenderHeads.Media.AVProVideo.IMediaSubtitles::LoadSubtitlesSRT(System.String)
// 0x000002A6 System.Int32 RenderHeads.Media.AVProVideo.IMediaSubtitles::GetSubtitleIndex()
// 0x000002A7 System.String RenderHeads.Media.AVProVideo.IMediaSubtitles::GetSubtitleText()
// 0x000002A8 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::OpenVideoFromFile(System.String,System.Int64,System.String,System.UInt32,System.UInt32,System.Int32)
// 0x000002A9 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::OpenVideoFromBuffer(System.Byte[])
// 0x000002AA System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::StartOpenVideoFromBuffer(System.UInt64)
// 0x000002AB System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::AddChunkToVideoBuffer(System.Byte[],System.UInt64,System.UInt64)
// 0x000002AC System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::EndOpenVideoFromBuffer()
// 0x000002AD System.Void RenderHeads.Media.AVProVideo.IMediaControl::CloseVideo()
// 0x000002AE System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetLooping(System.Boolean)
// 0x000002AF System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsLooping()
// 0x000002B0 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::HasMetaData()
// 0x000002B1 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::CanPlay()
// 0x000002B2 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsPlaying()
// 0x000002B3 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsSeeking()
// 0x000002B4 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsPaused()
// 0x000002B5 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsFinished()
// 0x000002B6 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsBuffering()
// 0x000002B7 System.Void RenderHeads.Media.AVProVideo.IMediaControl::Play()
// 0x000002B8 System.Void RenderHeads.Media.AVProVideo.IMediaControl::Pause()
// 0x000002B9 System.Void RenderHeads.Media.AVProVideo.IMediaControl::Stop()
// 0x000002BA System.Void RenderHeads.Media.AVProVideo.IMediaControl::Rewind()
// 0x000002BB System.Void RenderHeads.Media.AVProVideo.IMediaControl::Seek(System.Single)
// 0x000002BC System.Void RenderHeads.Media.AVProVideo.IMediaControl::SeekFast(System.Single)
// 0x000002BD System.Void RenderHeads.Media.AVProVideo.IMediaControl::SeekWithTolerance(System.Single,System.Single,System.Single)
// 0x000002BE System.Single RenderHeads.Media.AVProVideo.IMediaControl::GetCurrentTimeMs()
// 0x000002BF System.Double RenderHeads.Media.AVProVideo.IMediaControl::GetCurrentDateTimeSecondsSince1970()
// 0x000002C0 RenderHeads.Media.AVProVideo.TimeRange[] RenderHeads.Media.AVProVideo.IMediaControl::GetSeekableTimeRanges()
// 0x000002C1 System.Single RenderHeads.Media.AVProVideo.IMediaControl::GetPlaybackRate()
// 0x000002C2 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetPlaybackRate(System.Single)
// 0x000002C3 System.Void RenderHeads.Media.AVProVideo.IMediaControl::MuteAudio(System.Boolean)
// 0x000002C4 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsMuted()
// 0x000002C5 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetVolume(System.Single)
// 0x000002C6 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetBalance(System.Single)
// 0x000002C7 System.Single RenderHeads.Media.AVProVideo.IMediaControl::GetVolume()
// 0x000002C8 System.Single RenderHeads.Media.AVProVideo.IMediaControl::GetBalance()
// 0x000002C9 System.Int32 RenderHeads.Media.AVProVideo.IMediaControl::GetCurrentAudioTrack()
// 0x000002CA System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetAudioTrack(System.Int32)
// 0x000002CB System.Int32 RenderHeads.Media.AVProVideo.IMediaControl::GetCurrentVideoTrack()
// 0x000002CC System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetVideoTrack(System.Int32)
// 0x000002CD System.Single RenderHeads.Media.AVProVideo.IMediaControl::GetBufferingProgress()
// 0x000002CE System.Int32 RenderHeads.Media.AVProVideo.IMediaControl::GetBufferedTimeRangeCount()
// 0x000002CF System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::GetBufferedTimeRange(System.Int32,System.Single&,System.Single&)
// 0x000002D0 RenderHeads.Media.AVProVideo.ErrorCode RenderHeads.Media.AVProVideo.IMediaControl::GetLastError()
// 0x000002D1 System.Int64 RenderHeads.Media.AVProVideo.IMediaControl::GetLastExtendedErrorCode()
// 0x000002D2 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetTextureProperties(UnityEngine.FilterMode,UnityEngine.TextureWrapMode,System.Int32)
// 0x000002D3 System.Void RenderHeads.Media.AVProVideo.IMediaControl::GrabAudio(System.Single[],System.Int32,System.Int32)
// 0x000002D4 System.Int32 RenderHeads.Media.AVProVideo.IMediaControl::GetNumAudioChannels()
// 0x000002D5 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetAudioHeadRotation(UnityEngine.Quaternion)
// 0x000002D6 System.Void RenderHeads.Media.AVProVideo.IMediaControl::ResetAudioHeadRotation()
// 0x000002D7 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetAudioChannelMode(RenderHeads.Media.AVProVideo.Audio360ChannelMode)
// 0x000002D8 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetAudioFocusEnabled(System.Boolean)
// 0x000002D9 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetAudioFocusProperties(System.Single,System.Single)
// 0x000002DA System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetAudioFocusRotation(UnityEngine.Quaternion)
// 0x000002DB System.Void RenderHeads.Media.AVProVideo.IMediaControl::ResetAudioFocus()
// 0x000002DC System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::WaitForNextFrame(UnityEngine.Camera,System.Int32)
// 0x000002DD System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetPlayWithoutBuffering(System.Boolean)
// 0x000002DE System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetKeyServerURL(System.String)
// 0x000002DF System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetKeyServerAuthToken(System.String)
// 0x000002E0 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetDecryptionKeyBase64(System.String)
// 0x000002E1 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetDecryptionKey(System.Byte[])
// 0x000002E2 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsExternalPlaybackSupported()
// 0x000002E3 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsExternalPlaybackActive()
// 0x000002E4 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetAllowsExternalPlayback(System.Boolean)
// 0x000002E5 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetExternalPlaybackFillMode(RenderHeads.Media.AVProVideo.ExternalPlaybackFillMode)
// 0x000002E6 System.Single RenderHeads.Media.AVProVideo.IMediaInfo::GetDurationMs()
// 0x000002E7 System.Int32 RenderHeads.Media.AVProVideo.IMediaInfo::GetVideoWidth()
// 0x000002E8 System.Int32 RenderHeads.Media.AVProVideo.IMediaInfo::GetVideoHeight()
// 0x000002E9 UnityEngine.Rect RenderHeads.Media.AVProVideo.IMediaInfo::GetCropRect()
// 0x000002EA System.Single RenderHeads.Media.AVProVideo.IMediaInfo::GetVideoFrameRate()
// 0x000002EB System.Single RenderHeads.Media.AVProVideo.IMediaInfo::GetVideoDisplayRate()
// 0x000002EC System.Boolean RenderHeads.Media.AVProVideo.IMediaInfo::HasVideo()
// 0x000002ED System.Boolean RenderHeads.Media.AVProVideo.IMediaInfo::HasAudio()
// 0x000002EE System.Int32 RenderHeads.Media.AVProVideo.IMediaInfo::GetAudioTrackCount()
// 0x000002EF System.String RenderHeads.Media.AVProVideo.IMediaInfo::GetAudioTrackId(System.Int32)
// 0x000002F0 System.String RenderHeads.Media.AVProVideo.IMediaInfo::GetCurrentAudioTrackId()
// 0x000002F1 System.Int32 RenderHeads.Media.AVProVideo.IMediaInfo::GetCurrentAudioTrackBitrate()
// 0x000002F2 System.Int32 RenderHeads.Media.AVProVideo.IMediaInfo::GetVideoTrackCount()
// 0x000002F3 System.String RenderHeads.Media.AVProVideo.IMediaInfo::GetVideoTrackId(System.Int32)
// 0x000002F4 System.String RenderHeads.Media.AVProVideo.IMediaInfo::GetCurrentVideoTrackId()
// 0x000002F5 System.Int32 RenderHeads.Media.AVProVideo.IMediaInfo::GetCurrentVideoTrackBitrate()
// 0x000002F6 System.String RenderHeads.Media.AVProVideo.IMediaInfo::GetPlayerDescription()
// 0x000002F7 System.Boolean RenderHeads.Media.AVProVideo.IMediaInfo::PlayerSupportsLinearColorSpace()
// 0x000002F8 System.Boolean RenderHeads.Media.AVProVideo.IMediaInfo::IsPlaybackStalled()
// 0x000002F9 System.Single[] RenderHeads.Media.AVProVideo.IMediaInfo::GetTextureTransform()
// 0x000002FA System.Int64 RenderHeads.Media.AVProVideo.IMediaInfo::GetEstimatedTotalBandwidthUsed()
// 0x000002FB System.Int32 RenderHeads.Media.AVProVideo.IMediaProducer::GetTextureCount()
// 0x000002FC UnityEngine.Texture RenderHeads.Media.AVProVideo.IMediaProducer::GetTexture(System.Int32)
// 0x000002FD System.Int32 RenderHeads.Media.AVProVideo.IMediaProducer::GetTextureFrameCount()
// 0x000002FE System.Boolean RenderHeads.Media.AVProVideo.IMediaProducer::SupportsTextureFrameCount()
// 0x000002FF System.Int64 RenderHeads.Media.AVProVideo.IMediaProducer::GetTextureTimeStamp()
// 0x00000300 System.Boolean RenderHeads.Media.AVProVideo.IMediaProducer::RequiresVerticalFlip()
// 0x00000301 UnityEngine.Matrix4x4 RenderHeads.Media.AVProVideo.IMediaProducer::GetYpCbCrTransform()
// 0x00000302 System.Boolean RenderHeads.Media.AVProVideo.Subtitle::IsBefore(System.Single)
extern void Subtitle_IsBefore_mEB6A9661105B086871CA8892E2946E932AC3EB8A (void);
// 0x00000303 System.Boolean RenderHeads.Media.AVProVideo.Subtitle::IsTime(System.Single)
extern void Subtitle_IsTime_m3D2E48B7176629A4FF0E078F374C0F17CB710867 (void);
// 0x00000304 System.Void RenderHeads.Media.AVProVideo.Subtitle::.ctor()
extern void Subtitle__ctor_mE3CE9C6801FDCF8DB4808F367CF21B5F1D0F4892 (void);
// 0x00000305 System.String RenderHeads.Media.AVProVideo.Helper::GetName(RenderHeads.Media.AVProVideo.Platform)
extern void Helper_GetName_mF480B4F71E50B7139C818AA117F8F902DD1860CF (void);
// 0x00000306 System.String RenderHeads.Media.AVProVideo.Helper::GetErrorMessage(RenderHeads.Media.AVProVideo.ErrorCode)
extern void Helper_GetErrorMessage_m4C1C40C98BF5FB37C5EFB47DA99091FB45CC7ECC (void);
// 0x00000307 System.String[] RenderHeads.Media.AVProVideo.Helper::GetPlatformNames()
extern void Helper_GetPlatformNames_mB9EF4BB7B890FAA59C305A276A296294E75DD7B6 (void);
// 0x00000308 System.Void RenderHeads.Media.AVProVideo.Helper::LogInfo(System.String,UnityEngine.Object)
extern void Helper_LogInfo_mF7C65B5BD7AE1E6F4DE4D9869F9F53F4A07FA8B4 (void);
// 0x00000309 System.String RenderHeads.Media.AVProVideo.Helper::GetTimeString(System.Single,System.Boolean)
extern void Helper_GetTimeString_m6D36005C6785C5FF84AF046BBBDF4F9D3C20348D (void);
// 0x0000030A RenderHeads.Media.AVProVideo.Orientation RenderHeads.Media.AVProVideo.Helper::GetOrientation(System.Single[])
extern void Helper_GetOrientation_mE372CD72592447C80D008B0FBF653ECC87FB7627 (void);
// 0x0000030B UnityEngine.Matrix4x4 RenderHeads.Media.AVProVideo.Helper::GetMatrixForOrientation(RenderHeads.Media.AVProVideo.Orientation)
extern void Helper_GetMatrixForOrientation_mF32F5F6FCA1096DEC54DF70BBB009337D446C347 (void);
// 0x0000030C System.Void RenderHeads.Media.AVProVideo.Helper::SetupStereoEyeModeMaterial(UnityEngine.Material,RenderHeads.Media.AVProVideo.StereoEye)
extern void Helper_SetupStereoEyeModeMaterial_mAB7BBF51055B0ED8D6FFA203F9B4D3C9657EE7B6 (void);
// 0x0000030D System.Void RenderHeads.Media.AVProVideo.Helper::SetupLayoutMaterial(UnityEngine.Material,RenderHeads.Media.AVProVideo.VideoMapping)
extern void Helper_SetupLayoutMaterial_m8FF6AE3A80F67BE136F3E897B3ED40432AF1561E (void);
// 0x0000030E System.Void RenderHeads.Media.AVProVideo.Helper::SetupStereoMaterial(UnityEngine.Material,RenderHeads.Media.AVProVideo.StereoPacking,System.Boolean)
extern void Helper_SetupStereoMaterial_m255072662315CD8120ACD4158AE9FBAFD95572BC (void);
// 0x0000030F System.Void RenderHeads.Media.AVProVideo.Helper::SetupAlphaPackedMaterial(UnityEngine.Material,RenderHeads.Media.AVProVideo.AlphaPacking)
extern void Helper_SetupAlphaPackedMaterial_m95EB9F22C50015469A1B534E559D08DA2D1A7731 (void);
// 0x00000310 System.Void RenderHeads.Media.AVProVideo.Helper::SetupGammaMaterial(UnityEngine.Material,System.Boolean)
extern void Helper_SetupGammaMaterial_m84DCEAE514041A0DDB9732FA134BDBB11FFEDC81 (void);
// 0x00000311 System.Int32 RenderHeads.Media.AVProVideo.Helper::ConvertTimeSecondsToFrame(System.Single,System.Single)
extern void Helper_ConvertTimeSecondsToFrame_mB7F74397C75BC73D0BEF49BCFE0A8AC91B4D5481 (void);
// 0x00000312 System.Single RenderHeads.Media.AVProVideo.Helper::ConvertFrameToTimeSeconds(System.Int32,System.Single)
extern void Helper_ConvertFrameToTimeSeconds_mFA3A16E697B47197B35028CAD6763D6456709D30 (void);
// 0x00000313 System.Single RenderHeads.Media.AVProVideo.Helper::FindNextKeyFrameTimeSeconds(System.Single,System.Single,System.Int32)
extern void Helper_FindNextKeyFrameTimeSeconds_m0F9CB57EC2DB2FDA95753C860445F1B4E7E3A303 (void);
// 0x00000314 System.DateTime RenderHeads.Media.AVProVideo.Helper::ConvertSecondsSince1970ToDateTime(System.Double)
extern void Helper_ConvertSecondsSince1970ToDateTime_mDA52C14BF59C4D4E9CBB2E7A60035AACBC791C13 (void);
// 0x00000315 System.Void RenderHeads.Media.AVProVideo.Helper::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.ScaleMode,RenderHeads.Media.AVProVideo.AlphaPacking,UnityEngine.Material)
extern void Helper_DrawTexture_m165CB51168074BB81EB25C61A0F6F1C6DFD93469 (void);
// 0x00000316 UnityEngine.Texture2D RenderHeads.Media.AVProVideo.Helper::GetReadableTexture(UnityEngine.Texture,System.Boolean,RenderHeads.Media.AVProVideo.Orientation,UnityEngine.Texture2D)
extern void Helper_GetReadableTexture_mF4D3B41AFFF220B865DD21C89F30F920DA919777 (void);
// 0x00000317 System.Int32 RenderHeads.Media.AVProVideo.Helper::ParseTimeToMs(System.String)
extern void Helper_ParseTimeToMs_m1C11922E300EF60DC1F4A2363B816E2C3405733D (void);
// 0x00000318 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Subtitle> RenderHeads.Media.AVProVideo.Helper::LoadSubtitlesSRT(System.String)
extern void Helper_LoadSubtitlesSRT_m48A32A35BA5B9C1361B13F4D18CF5377AB5809AB (void);
// 0x00000319 System.String RenderHeads.Media.AVProVideo.NullMediaPlayer::GetVersion()
extern void NullMediaPlayer_GetVersion_m7C99CA824683CEC53FE8AF2D36FF0A4284ECBBB5 (void);
// 0x0000031A System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::OpenVideoFromFile(System.String,System.Int64,System.String,System.UInt32,System.UInt32,System.Int32)
extern void NullMediaPlayer_OpenVideoFromFile_m534926435809260C8EAAA704B462FBB14E5E143E (void);
// 0x0000031B System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::CloseVideo()
extern void NullMediaPlayer_CloseVideo_m98F9C4A0B29978BD67EEAFF5FBA01BFC23A9CCA5 (void);
// 0x0000031C System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::SetLooping(System.Boolean)
extern void NullMediaPlayer_SetLooping_m9FC9EA6D16B86443398BBC53518A4B69C9F24B94 (void);
// 0x0000031D System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::IsLooping()
extern void NullMediaPlayer_IsLooping_m3F846DAB2AC8DC7FFE87B96C1D94DA18DD6C32D5 (void);
// 0x0000031E System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::HasMetaData()
extern void NullMediaPlayer_HasMetaData_mEB1731555ACD733D078418AB66BCA287A8173A7A (void);
// 0x0000031F System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::CanPlay()
extern void NullMediaPlayer_CanPlay_mBC0BA1989FF91ADCE1C283064D0C2802B1C69719 (void);
// 0x00000320 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::HasAudio()
extern void NullMediaPlayer_HasAudio_m4A189F945DF02097107E0921C1F5244E706B0014 (void);
// 0x00000321 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::HasVideo()
extern void NullMediaPlayer_HasVideo_mFEAF2E0AC280F012BCE8F7F33E24E909CCCB3D93 (void);
// 0x00000322 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::Play()
extern void NullMediaPlayer_Play_mD82F4A24CA224CACFA1F6947B0BB300F9EBEA2D5 (void);
// 0x00000323 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::Pause()
extern void NullMediaPlayer_Pause_mBDAB6D4554622DD80531637A1714FDC164BBF900 (void);
// 0x00000324 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::Stop()
extern void NullMediaPlayer_Stop_mA15CBEAFB6199C2B4B93B867F738957B179350E3 (void);
// 0x00000325 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::IsSeeking()
extern void NullMediaPlayer_IsSeeking_m62DEB4CD700CD1146B0742F7E9DBA06B1CAEB063 (void);
// 0x00000326 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::IsPlaying()
extern void NullMediaPlayer_IsPlaying_mDF0CD38A5AA667CB5E3F6343D1023D0FF24532C6 (void);
// 0x00000327 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::IsPaused()
extern void NullMediaPlayer_IsPaused_m42F8DDE720F124B6F3D3BDA2D9345D8D5DF62A1D (void);
// 0x00000328 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::IsFinished()
extern void NullMediaPlayer_IsFinished_m17889412A5FA6C6902031FBE08CE95DA314B003C (void);
// 0x00000329 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::IsBuffering()
extern void NullMediaPlayer_IsBuffering_mAED83677C608F001B38D2C953D8B6F2F6F29CF31 (void);
// 0x0000032A System.Single RenderHeads.Media.AVProVideo.NullMediaPlayer::GetDurationMs()
extern void NullMediaPlayer_GetDurationMs_m5E820AF34F931F3F04673D5927EDF7C8D404FE73 (void);
// 0x0000032B System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetVideoWidth()
extern void NullMediaPlayer_GetVideoWidth_m263369B6E47B8A5E64D2A590F5460817227C96A4 (void);
// 0x0000032C System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetVideoHeight()
extern void NullMediaPlayer_GetVideoHeight_mEE476C17AC2AF67323C782E80655DA023B2F9140 (void);
// 0x0000032D System.Single RenderHeads.Media.AVProVideo.NullMediaPlayer::GetVideoDisplayRate()
extern void NullMediaPlayer_GetVideoDisplayRate_mF2BC451FEB2AB8CF7CE1175385EB1E4B00A3CB4A (void);
// 0x0000032E UnityEngine.Texture RenderHeads.Media.AVProVideo.NullMediaPlayer::GetTexture(System.Int32)
extern void NullMediaPlayer_GetTexture_m34E6C85782D9839BDB6E6E8F8397389610364219 (void);
// 0x0000032F System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetTextureFrameCount()
extern void NullMediaPlayer_GetTextureFrameCount_mB96F9D1EEC5C3C34D47FF7934565F66C50F850C0 (void);
// 0x00000330 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::RequiresVerticalFlip()
extern void NullMediaPlayer_RequiresVerticalFlip_m55410C146647CF663790A6760EC0D57866733056 (void);
// 0x00000331 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::Seek(System.Single)
extern void NullMediaPlayer_Seek_m957E145D3E5BADE75C1F92A4128B273D7C84B9E9 (void);
// 0x00000332 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::SeekFast(System.Single)
extern void NullMediaPlayer_SeekFast_m47B3ED648072D952C1D8B319DC9336440CFD0073 (void);
// 0x00000333 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::SeekWithTolerance(System.Single,System.Single,System.Single)
extern void NullMediaPlayer_SeekWithTolerance_mF69D2CD514E0E2D30C3D7899B6A718FD423FA238 (void);
// 0x00000334 System.Single RenderHeads.Media.AVProVideo.NullMediaPlayer::GetCurrentTimeMs()
extern void NullMediaPlayer_GetCurrentTimeMs_m617406675A7481B7FE2A2E9E42A0C69B4E34A71A (void);
// 0x00000335 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::SetPlaybackRate(System.Single)
extern void NullMediaPlayer_SetPlaybackRate_m008BA6284CBF3ACD1A3A4A6065ECC5CF2BF92800 (void);
// 0x00000336 System.Single RenderHeads.Media.AVProVideo.NullMediaPlayer::GetPlaybackRate()
extern void NullMediaPlayer_GetPlaybackRate_mC46869B3BD974209F002516E769C67535AA7D41D (void);
// 0x00000337 System.Single RenderHeads.Media.AVProVideo.NullMediaPlayer::GetBufferingProgress()
extern void NullMediaPlayer_GetBufferingProgress_m53D138EE5A3C0889B2ADFC42B53B88129154C298 (void);
// 0x00000338 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::MuteAudio(System.Boolean)
extern void NullMediaPlayer_MuteAudio_m4CD4D798B8FD80DB3BAD93D7F394D7D4503C6A35 (void);
// 0x00000339 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::IsMuted()
extern void NullMediaPlayer_IsMuted_m3F50A8BC58A4F531D03C10AB9503645DDAAB1FA3 (void);
// 0x0000033A System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::SetVolume(System.Single)
extern void NullMediaPlayer_SetVolume_mBC578B78DD441C5062EFE47030821F6B1CB3A116 (void);
// 0x0000033B System.Single RenderHeads.Media.AVProVideo.NullMediaPlayer::GetVolume()
extern void NullMediaPlayer_GetVolume_mF4238FB12F49EFE6F3C67298BD69F2FCA9B61055 (void);
// 0x0000033C System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetAudioTrackCount()
extern void NullMediaPlayer_GetAudioTrackCount_m6B7B7D53E16FC24A044F3112CEC035D12BF0AE22 (void);
// 0x0000033D System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetCurrentAudioTrack()
extern void NullMediaPlayer_GetCurrentAudioTrack_mEC3ABD06F85003D24F160CE8DE3490524A625B8A (void);
// 0x0000033E System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::SetAudioTrack(System.Int32)
extern void NullMediaPlayer_SetAudioTrack_mEB9EE4EFC70C0352F9E8A9EF175F77E0B7CA54FD (void);
// 0x0000033F System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetVideoTrackCount()
extern void NullMediaPlayer_GetVideoTrackCount_m0F4DBD7A105BCF05EC8122E18887FB79D71046AC (void);
// 0x00000340 System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetCurrentVideoTrack()
extern void NullMediaPlayer_GetCurrentVideoTrack_m3074C13149D2B5FA92D821348EB2DFEC7E5CA95D (void);
// 0x00000341 System.String RenderHeads.Media.AVProVideo.NullMediaPlayer::GetCurrentAudioTrackId()
extern void NullMediaPlayer_GetCurrentAudioTrackId_mBE5D1E0A9182F9AFB9DB883A3463BEB06DB5EBC3 (void);
// 0x00000342 System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetCurrentAudioTrackBitrate()
extern void NullMediaPlayer_GetCurrentAudioTrackBitrate_mD41ACEA1199BF8867ECA085A01027B6C16BFA4AC (void);
// 0x00000343 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::SetVideoTrack(System.Int32)
extern void NullMediaPlayer_SetVideoTrack_m2988DF0CC727E4CD488CF7DF5D9084AE6B6501D3 (void);
// 0x00000344 System.String RenderHeads.Media.AVProVideo.NullMediaPlayer::GetCurrentVideoTrackId()
extern void NullMediaPlayer_GetCurrentVideoTrackId_m6FC3DEE140CE674A7484617A4E0F5328B7464BDC (void);
// 0x00000345 System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetCurrentVideoTrackBitrate()
extern void NullMediaPlayer_GetCurrentVideoTrackBitrate_m68EC94EA88DC7AEEBFF0EE8D888FC70D2CC189DA (void);
// 0x00000346 System.Single RenderHeads.Media.AVProVideo.NullMediaPlayer::GetVideoFrameRate()
extern void NullMediaPlayer_GetVideoFrameRate_m7FA1B9BE33A1D36AA4948E00D7A4EF044D47D864 (void);
// 0x00000347 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::Update()
extern void NullMediaPlayer_Update_m4BAB2D62057BB9C5CCF9DB8BEAAC9F9C016BFA5F (void);
// 0x00000348 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::Render()
extern void NullMediaPlayer_Render_m8C59CCD4F58F96B12C313C5E030A7DEF544F7979 (void);
// 0x00000349 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::Dispose()
extern void NullMediaPlayer_Dispose_m395EBD5C0579F5AD2D531ECB2358D16658416597 (void);
// 0x0000034A System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::.ctor()
extern void NullMediaPlayer__ctor_mF2E673280640BB0B17A8CFA19769F9FCD966E708 (void);
// 0x0000034B System.Int32 RenderHeads.Media.AVProVideo.Resampler::get_DroppedFrames()
extern void Resampler_get_DroppedFrames_m89C73FBFFA361B76C942696E7E67C16D37BEC16E (void);
// 0x0000034C System.Int32 RenderHeads.Media.AVProVideo.Resampler::get_FrameDisplayedTimer()
extern void Resampler_get_FrameDisplayedTimer_m1FEA8F30EFE891EBD7578CCA9C13C139C5BC2148 (void);
// 0x0000034D System.Int64 RenderHeads.Media.AVProVideo.Resampler::get_BaseTimestamp()
extern void Resampler_get_BaseTimestamp_mD32E51FEDE63B04FF2EF5AFC81521E011CA2BBA5 (void);
// 0x0000034E System.Void RenderHeads.Media.AVProVideo.Resampler::set_BaseTimestamp(System.Int64)
extern void Resampler_set_BaseTimestamp_m75489EB5F4E3106BE10E99382C9158761CF260BF (void);
// 0x0000034F System.Single RenderHeads.Media.AVProVideo.Resampler::get_ElapsedTimeSinceBase()
extern void Resampler_get_ElapsedTimeSinceBase_mAF0A0C70D042DA9A93F12CC2808EF6A757D4E0DD (void);
// 0x00000350 System.Void RenderHeads.Media.AVProVideo.Resampler::set_ElapsedTimeSinceBase(System.Single)
extern void Resampler_set_ElapsedTimeSinceBase_mB4ECC7BE68BF64866EF400A59295F560D1B73962 (void);
// 0x00000351 System.Single RenderHeads.Media.AVProVideo.Resampler::get_LastT()
extern void Resampler_get_LastT_m14664B1A60E1A35DEC05847B385D1B0F20D53BC4 (void);
// 0x00000352 System.Void RenderHeads.Media.AVProVideo.Resampler::set_LastT(System.Single)
extern void Resampler_set_LastT_mA6EEBE901C8F444E4C0755399E984A17348F798F (void);
// 0x00000353 System.Int64 RenderHeads.Media.AVProVideo.Resampler::get_TextureTimeStamp()
extern void Resampler_get_TextureTimeStamp_mE0BF12F7A977BE1B3461A8315EB26D1BB0654205 (void);
// 0x00000354 System.Void RenderHeads.Media.AVProVideo.Resampler::set_TextureTimeStamp(System.Int64)
extern void Resampler_set_TextureTimeStamp_m45BAB50E6FF3E9CB4BC9F410FD36A311781FDAF8 (void);
// 0x00000355 System.Void RenderHeads.Media.AVProVideo.Resampler::OnVideoEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void Resampler_OnVideoEvent_mF3C34FC5C11E42C476CAD167D1916122CD1E753A (void);
// 0x00000356 System.Void RenderHeads.Media.AVProVideo.Resampler::.ctor(RenderHeads.Media.AVProVideo.MediaPlayer,System.String,System.Int32,RenderHeads.Media.AVProVideo.Resampler/ResampleMode)
extern void Resampler__ctor_m76645A02F144B2210AAFA5DA1222498798E26B36 (void);
// 0x00000357 UnityEngine.Texture[] RenderHeads.Media.AVProVideo.Resampler::get_OutputTexture()
extern void Resampler_get_OutputTexture_m9FCDEA6FCB2223759563C21987CAF332074D5F14 (void);
// 0x00000358 System.Void RenderHeads.Media.AVProVideo.Resampler::Reset()
extern void Resampler_Reset_mA045C7BB3F8C0EF92D4A90846A862728280F1867 (void);
// 0x00000359 System.Void RenderHeads.Media.AVProVideo.Resampler::Release()
extern void Resampler_Release_m9E52FCA3DF6A9E683048B79DD2844CE78489408E (void);
// 0x0000035A System.Void RenderHeads.Media.AVProVideo.Resampler::ReleaseRenderTextures()
extern void Resampler_ReleaseRenderTextures_mD81331355472932144F5BB614DC9773DD680F813 (void);
// 0x0000035B System.Void RenderHeads.Media.AVProVideo.Resampler::ConstructRenderTextures()
extern void Resampler_ConstructRenderTextures_mD4EC34B39ED92D4CE61B331A77779EE99C509E97 (void);
// 0x0000035C System.Boolean RenderHeads.Media.AVProVideo.Resampler::CheckRenderTexturesValid()
extern void Resampler_CheckRenderTexturesValid_m7C496A52108FF27225FC3EDF1D49ED8118F0618B (void);
// 0x0000035D System.Int32 RenderHeads.Media.AVProVideo.Resampler::FindBeforeFrameIndex(System.Int32)
extern void Resampler_FindBeforeFrameIndex_m6F4116CB277A02CF0D8C5F11CE357EA689B95D2E (void);
// 0x0000035E System.Int32 RenderHeads.Media.AVProVideo.Resampler::FindClosestFrame(System.Int32)
extern void Resampler_FindClosestFrame_m38E7996995CC032423255F3987B93702E1C517D4 (void);
// 0x0000035F System.Void RenderHeads.Media.AVProVideo.Resampler::PointUpdate()
extern void Resampler_PointUpdate_mF8FFCFCB3EF9316A248B1A6038D546614D1B45FA (void);
// 0x00000360 System.Void RenderHeads.Media.AVProVideo.Resampler::SampleFrame(System.Int32,System.Int32)
extern void Resampler_SampleFrame_m5DFC2F51A491DCF401463401EB0DE30C211F0552 (void);
// 0x00000361 System.Void RenderHeads.Media.AVProVideo.Resampler::SampleFrames(System.Int32,System.Int32,System.Int32,System.Single)
extern void Resampler_SampleFrames_mF3E75BEC6D4007294FB03E9B4B0A440B7A45770E (void);
// 0x00000362 System.Void RenderHeads.Media.AVProVideo.Resampler::LinearUpdate()
extern void Resampler_LinearUpdate_mFAB465F1C8C876BDFA896FC9D5407602B0815ED0 (void);
// 0x00000363 System.Void RenderHeads.Media.AVProVideo.Resampler::InvalidateBuffer()
extern void Resampler_InvalidateBuffer_mC0AF6A0D43E9FB55417DC2197ED0CB18F7D16141 (void);
// 0x00000364 System.Single RenderHeads.Media.AVProVideo.Resampler::GuessFrameRate()
extern void Resampler_GuessFrameRate_mB3FB29DBB3EBB30F8B632D16A38713DD1D41F3E9 (void);
// 0x00000365 System.Void RenderHeads.Media.AVProVideo.Resampler::Update()
extern void Resampler_Update_m3254C81FF143CAA0F94478AD9F3A36DA35889409 (void);
// 0x00000366 System.Void RenderHeads.Media.AVProVideo.Resampler::UpdateTimestamp()
extern void Resampler_UpdateTimestamp_mCD50651F28142B6D657B9E08552E409EF71C1329 (void);
// 0x00000367 System.Void RenderHeads.Media.AVProVideo.Resampler/TimestampedRenderTexture::.ctor()
extern void TimestampedRenderTexture__ctor_m8BD6A57BEACA9639A33C95B79E1EC9E3F02292D3 (void);
// 0x00000368 System.Int32 RenderHeads.Media.AVProVideo.Stream::get_Width()
// 0x00000369 System.Int32 RenderHeads.Media.AVProVideo.Stream::get_Height()
// 0x0000036A System.Int32 RenderHeads.Media.AVProVideo.Stream::get_Bandwidth()
// 0x0000036B System.String RenderHeads.Media.AVProVideo.Stream::get_URL()
// 0x0000036C System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream/Chunk> RenderHeads.Media.AVProVideo.Stream::GetAllChunks()
// 0x0000036D System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream/Chunk> RenderHeads.Media.AVProVideo.Stream::GetChunks()
// 0x0000036E System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream> RenderHeads.Media.AVProVideo.Stream::GetAllStreams()
// 0x0000036F System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream> RenderHeads.Media.AVProVideo.Stream::GetStreams()
// 0x00000370 System.Void RenderHeads.Media.AVProVideo.Stream::.ctor()
extern void Stream__ctor_m0EF9AC2B34988BCFFA29F5B6CA363E7550F99864 (void);
// 0x00000371 System.Void RenderHeads.Media.AVProVideo.Demos.AutoRotate::Awake()
extern void AutoRotate_Awake_m9125985087782E449B8BDE1701AFA7E1F52E2CC0 (void);
// 0x00000372 System.Void RenderHeads.Media.AVProVideo.Demos.AutoRotate::Update()
extern void AutoRotate_Update_m996711F457A3DAB5E32728F7E7531152AC3FFCE7 (void);
// 0x00000373 System.Void RenderHeads.Media.AVProVideo.Demos.AutoRotate::Randomise()
extern void AutoRotate_Randomise_m151CF6AB83A74D336DE9CBB95B880E0C0C888A41 (void);
// 0x00000374 System.Void RenderHeads.Media.AVProVideo.Demos.AutoRotate::.ctor()
extern void AutoRotate__ctor_mF6EA1D9394B3FFD722CEBCE5E005942640D4FFFD (void);
// 0x00000375 System.Void RenderHeads.Media.AVProVideo.Demos.DemoInfo::.ctor()
extern void DemoInfo__ctor_m84D3B4449BF5A454F70EA714E53CB4C0A3FF0F14 (void);
// 0x00000376 System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::Start()
extern void FrameExtract_Start_m86C6F9E8C0BB8CA9C0E35FE161494E4D4FD26B88 (void);
// 0x00000377 System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::OnMediaPlayerEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void FrameExtract_OnMediaPlayerEvent_m236350A2DA194DC03BBCDE1572854A509E8752AD (void);
// 0x00000378 System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::OnNewMediaReady()
extern void FrameExtract_OnNewMediaReady_m1BDB897A36FD26056774A42DB69D5E997DFD0C0A (void);
// 0x00000379 System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::OnDestroy()
extern void FrameExtract_OnDestroy_m3C828BF1AAE7DB99DA82981E3B7FF33B91B93EC9 (void);
// 0x0000037A System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::Update()
extern void FrameExtract_Update_m2FE9DE4DCCB9974FEF4DBDD9C12AD87F62B3CB61 (void);
// 0x0000037B System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::ProcessExtractedFrame(UnityEngine.Texture2D)
extern void FrameExtract_ProcessExtractedFrame_mB0F64766802F1839EFB80F52B330E19B2BA45BAF (void);
// 0x0000037C System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::ExtractNextFrame()
extern void FrameExtract_ExtractNextFrame_m76FADAC81FF292A7BFBC9A3CCA3B381BB4A4A2BE (void);
// 0x0000037D System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::OnGUI()
extern void FrameExtract_OnGUI_m1D35E04595001C25AB77D243933F4829E465D8F2 (void);
// 0x0000037E System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::.ctor()
extern void FrameExtract__ctor_m47ECE9903AD1855FF054A482D0AC956BD406EF8A (void);
// 0x0000037F System.Void RenderHeads.Media.AVProVideo.Demos.Mapping3D::Update()
extern void Mapping3D_Update_mC2E7D7CD818334EC01889C136FF7B60B09075F45 (void);
// 0x00000380 System.Void RenderHeads.Media.AVProVideo.Demos.Mapping3D::SpawnCube()
extern void Mapping3D_SpawnCube_mE69B5D32E8B56DBF702C22117A9D68168D6509AA (void);
// 0x00000381 System.Void RenderHeads.Media.AVProVideo.Demos.Mapping3D::RemoveCube()
extern void Mapping3D_RemoveCube_m97989618E2B42C976A7D429EFE3E49E38A2A35CC (void);
// 0x00000382 System.Void RenderHeads.Media.AVProVideo.Demos.Mapping3D::.ctor()
extern void Mapping3D__ctor_m8280D65CDEA1260848492F0D75A1AA64A9F83C62 (void);
// 0x00000383 System.Void RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::Update()
extern void SampleApp_Multiple_Update_m9F7C7DA6A701547501069744203715D30A5BC456 (void);
// 0x00000384 System.Void RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::UpdateVideosLayout()
extern void SampleApp_Multiple_UpdateVideosLayout_m6DDFEC63572493C1FEA957C7F0F167A6FA335B90 (void);
// 0x00000385 System.Void RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::AddVideoClicked()
extern void SampleApp_Multiple_AddVideoClicked_m36B1B0E32A8A76354FFA4F1E35E2DD9B13303655 (void);
// 0x00000386 System.Void RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::RemoveVideoClicked()
extern void SampleApp_Multiple_RemoveVideoClicked_m85ED7A8638AE26D4B2B55ACDB3F587950A34CD1C (void);
// 0x00000387 System.Void RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::OnDestroy()
extern void SampleApp_Multiple_OnDestroy_m74B83A6EE2274993028C30FB1A94DA756D3A2CFF (void);
// 0x00000388 System.Void RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::.ctor()
extern void SampleApp_Multiple__ctor_m7430B511051CC8DA439F95F2709FC059AA01041A (void);
// 0x00000389 System.Void RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack::OnEnable()
extern void ChangeAudioTrack_OnEnable_mEA7D6510235C5A0FB91F6438512256E5188F7C66 (void);
// 0x0000038A System.Void RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack::Update()
extern void ChangeAudioTrack_Update_mD496150E0FFEAAEE985F707B963F98476688A17F (void);
// 0x0000038B System.Boolean RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack::IsVideoLoaded()
extern void ChangeAudioTrack_IsVideoLoaded_mBB2EF585989E345FB94140E762CB98F5A5D4C08B (void);
// 0x0000038C System.Boolean RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack::DoChangeAudioTrack(RenderHeads.Media.AVProVideo.MediaPlayer,System.Int32)
extern void ChangeAudioTrack_DoChangeAudioTrack_mC2192C7319F71BBED2CFC0412F494A3C1EDF07A2 (void);
// 0x0000038D System.Void RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack::.ctor()
extern void ChangeAudioTrack__ctor_mDE6DF674164766E2F99A48794099FD4CED7F4B5A (void);
// 0x0000038E System.Void RenderHeads.Media.AVProVideo.Demos.ChangeStereoMode::Update()
extern void ChangeStereoMode_Update_m4A80BCC9F758E8FA5B25C087E5ABA1122BDC7948 (void);
// 0x0000038F System.Void RenderHeads.Media.AVProVideo.Demos.ChangeStereoMode::.ctor()
extern void ChangeStereoMode__ctor_m983D047D9A67B39A904DD375F3B7C6A90A7EABA9 (void);
// 0x00000390 System.Void RenderHeads.Media.AVProVideo.Demos.ChangeVideoExample::LoadVideo(System.String)
extern void ChangeVideoExample_LoadVideo_m9DB2C4845DB26310D2E0C5DAD82B67A77D61BA96 (void);
// 0x00000391 System.Void RenderHeads.Media.AVProVideo.Demos.ChangeVideoExample::OnGUI()
extern void ChangeVideoExample_OnGUI_m22143D11AA05FE3B4FA2AAE0E365496ABBA19F8A (void);
// 0x00000392 System.Void RenderHeads.Media.AVProVideo.Demos.ChangeVideoExample::.ctor()
extern void ChangeVideoExample__ctor_m858B516767895F2F48E40A6B5434B49C52568876 (void);
// 0x00000393 System.Void RenderHeads.Media.AVProVideo.Demos.LoadFromBuffer::Start()
extern void LoadFromBuffer_Start_m634D4F939D473E4A64850FCA4B22EA686B544D21 (void);
// 0x00000394 System.Void RenderHeads.Media.AVProVideo.Demos.LoadFromBuffer::.ctor()
extern void LoadFromBuffer__ctor_m5C20613912E8A1EC5862B3CF68BEA3849CBC8CB6 (void);
// 0x00000395 System.Void RenderHeads.Media.AVProVideo.Demos.LoadFromBufferInChunks::Start()
extern void LoadFromBufferInChunks_Start_m75693C9231FB7808DD89670B4039530B4E126867 (void);
// 0x00000396 System.Void RenderHeads.Media.AVProVideo.Demos.LoadFromBufferInChunks::.ctor()
extern void LoadFromBufferInChunks__ctor_mC211F50F13C2E652530CBB381FF5BF87E93C43C9 (void);
// 0x00000397 System.Void RenderHeads.Media.AVProVideo.Demos.NativeMediaOpen::Start()
extern void NativeMediaOpen_Start_m832C6A4189EFA0E2FF8B779897AEA8DE06056C01 (void);
// 0x00000398 System.Void RenderHeads.Media.AVProVideo.Demos.NativeMediaOpen::Update()
extern void NativeMediaOpen_Update_m9EF14073FF9F5665B22BC915BB61FD0F8904A059 (void);
// 0x00000399 System.Void RenderHeads.Media.AVProVideo.Demos.NativeMediaOpen::OnGUI()
extern void NativeMediaOpen_OnGUI_mF2F3448171A524013F380712F4E3A77C7A526301 (void);
// 0x0000039A System.Void RenderHeads.Media.AVProVideo.Demos.NativeMediaOpen::.ctor()
extern void NativeMediaOpen__ctor_m7FB80BD87A11C55DB1F6A378E470349999257019 (void);
// 0x0000039B System.Void RenderHeads.Media.AVProVideo.Demos.PlaybackSync::Start()
extern void PlaybackSync_Start_m3D3D76F2F508945BDB5E5B8B8FBCD356D8EB7625 (void);
// 0x0000039C System.Void RenderHeads.Media.AVProVideo.Demos.PlaybackSync::LateUpdate()
extern void PlaybackSync_LateUpdate_mA6CD6504C41A1CA36064EBD796FF4AE62059D077 (void);
// 0x0000039D System.Boolean RenderHeads.Media.AVProVideo.Demos.PlaybackSync::IsAllVideosLoaded()
extern void PlaybackSync_IsAllVideosLoaded_m35A6AD92B328914235EAA739F9EDA31C51C98373 (void);
// 0x0000039E System.Boolean RenderHeads.Media.AVProVideo.Demos.PlaybackSync::IsVideoLoaded(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void PlaybackSync_IsVideoLoaded_m75D4E04E968D92C842F60FA97F8D18C348C02E48 (void);
// 0x0000039F System.Boolean RenderHeads.Media.AVProVideo.Demos.PlaybackSync::IsPlaybackFinished(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void PlaybackSync_IsPlaybackFinished_m6140287F20C6D0CAB8B90A809B6723E750447874 (void);
// 0x000003A0 System.Void RenderHeads.Media.AVProVideo.Demos.PlaybackSync::.ctor()
extern void PlaybackSync__ctor_m720134ED1E5E955CB902CC1C2F6C9C181E7B2113 (void);
// 0x000003A1 System.Void RenderHeads.Media.AVProVideo.Demos.StartEndPoint::OnEnable()
extern void StartEndPoint_OnEnable_mCF4D9FDD4C1A09E27602E8E662E9A0A5CE7E4351 (void);
// 0x000003A2 System.Void RenderHeads.Media.AVProVideo.Demos.StartEndPoint::Update()
extern void StartEndPoint_Update_m7633632951E35EC0D4756080159746EE5D8379D6 (void);
// 0x000003A3 System.Boolean RenderHeads.Media.AVProVideo.Demos.StartEndPoint::IsVideoLoaded(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void StartEndPoint_IsVideoLoaded_mF5D3FF9061F94EAB62EE5468F1CF975385B5496E (void);
// 0x000003A4 System.Void RenderHeads.Media.AVProVideo.Demos.StartEndPoint::DoStart(RenderHeads.Media.AVProVideo.MediaPlayer,System.Single)
extern void StartEndPoint_DoStart_m23A60257901F9B7E4165978AF7C8E83E9B6A3F87 (void);
// 0x000003A5 System.Void RenderHeads.Media.AVProVideo.Demos.StartEndPoint::DoCheckEnd(RenderHeads.Media.AVProVideo.MediaPlayer,System.Single)
extern void StartEndPoint_DoCheckEnd_m06CA2EECC3AB3626B18CB1ED771A73FD79509021 (void);
// 0x000003A6 System.Void RenderHeads.Media.AVProVideo.Demos.StartEndPoint::DoCheckLoop(RenderHeads.Media.AVProVideo.MediaPlayer,System.Single,System.Single)
extern void StartEndPoint_DoCheckLoop_m873C604C0022D7BF5AAADF6C867E14B5FBA599A8 (void);
// 0x000003A7 System.Void RenderHeads.Media.AVProVideo.Demos.StartEndPoint::.ctor()
extern void StartEndPoint__ctor_m757CF90B0BF79B7C2D78AF20A4E70240D3BFE7DD (void);
// 0x000003A8 System.Void RenderHeads.Media.AVProVideo.Demos.VideoTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void VideoTrigger_OnTriggerEnter_m8E48C1631203B0C8B819052BCC1DAC93DB05B355 (void);
// 0x000003A9 System.Void RenderHeads.Media.AVProVideo.Demos.VideoTrigger::OnTriggerExit(UnityEngine.Collider)
extern void VideoTrigger_OnTriggerExit_m0251AFF2B0005DED8E6A0A346D9A53F30A770DF5 (void);
// 0x000003AA System.Void RenderHeads.Media.AVProVideo.Demos.VideoTrigger::Update()
extern void VideoTrigger_Update_mC8A9CCDE353D055366F4E4F77776323E831ECAC8 (void);
// 0x000003AB System.Void RenderHeads.Media.AVProVideo.Demos.VideoTrigger::.ctor()
extern void VideoTrigger__ctor_mA7BE38FA20CA4D80F20E2872AA4B69CD9062483E (void);
// 0x000003AC System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::Start()
extern void SimpleController_Start_m02353FB587A70A57549F79657AF412735BF7FD61 (void);
// 0x000003AD System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::OnDestroy()
extern void SimpleController_OnDestroy_mE538A96108461D97185E1235FB65D55BB1D072D8 (void);
// 0x000003AE System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::OnMediaPlayerEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void SimpleController_OnMediaPlayerEvent_m38F7547BC6E4DFACA11F414B4BFFA154B569EB91 (void);
// 0x000003AF System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::AddEvent(RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType)
extern void SimpleController_AddEvent_mDD46200976ADB7ACC54977D0D3B0DF8C59280256 (void);
// 0x000003B0 System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::GatherProperties()
extern void SimpleController_GatherProperties_mDF1C212311EDA1A49FF150BB5C782E07E3618B33 (void);
// 0x000003B1 System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::Update()
extern void SimpleController_Update_mBE9CB73F11479998D432232B6B5BCB8E9734D489 (void);
// 0x000003B2 System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::LoadVideo(System.String,System.Boolean)
extern void SimpleController_LoadVideo_m8C759CAA63682B2134A9EBB067032F88B9E441A6 (void);
// 0x000003B3 System.Boolean RenderHeads.Media.AVProVideo.Demos.SimpleController::VideoIsReady(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void SimpleController_VideoIsReady_m5F71FECD8B7EB71A09C8A25336E6E676A454242B (void);
// 0x000003B4 System.Boolean RenderHeads.Media.AVProVideo.Demos.SimpleController::AudioIsReady(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void SimpleController_AudioIsReady_mEFF0519289C815E43EE68EAC005C286BE2B5907E (void);
// 0x000003B5 System.Collections.IEnumerator RenderHeads.Media.AVProVideo.Demos.SimpleController::LoadVideoWithFading()
extern void SimpleController_LoadVideoWithFading_mEA1895F18C5A29022FFB95B240850EF57A01DEBE (void);
// 0x000003B6 System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::OnGUI()
extern void SimpleController_OnGUI_mC8633AEF945EBE382AA75F4F08FCB6C0329F2744 (void);
// 0x000003B7 System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::.ctor()
extern void SimpleController__ctor_mD6B3049AD749E6755E2B8DD9BB518920984B81E5 (void);
// 0x000003B8 System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::.ctor(System.Int32)
extern void U3CLoadVideoWithFadingU3Ed__23__ctor_m82DE7FDB17C4F4F5D90E14A3642D40C45237FA91 (void);
// 0x000003B9 System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::System.IDisposable.Dispose()
extern void U3CLoadVideoWithFadingU3Ed__23_System_IDisposable_Dispose_mF6B335E78513CCE1A4BA3C32A075E004595E0B7A (void);
// 0x000003BA System.Boolean RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::MoveNext()
extern void U3CLoadVideoWithFadingU3Ed__23_MoveNext_mFEC22F4329D83829C97271AF392350A673BB9EF6 (void);
// 0x000003BB System.Object RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadVideoWithFadingU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB51738CA40EBA5A1083400C83666804A6D24A214 (void);
// 0x000003BC System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::System.Collections.IEnumerator.Reset()
extern void U3CLoadVideoWithFadingU3Ed__23_System_Collections_IEnumerator_Reset_m44DA52D0E0CC3CD7D5F21583F357767AF45EBF94 (void);
// 0x000003BD System.Object RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CLoadVideoWithFadingU3Ed__23_System_Collections_IEnumerator_get_Current_m6AEC2F56DE7688F3E09466C723AEBB3ABCA718FE (void);
// 0x000003BE System.Boolean RenderHeads.Media.AVProVideo.Demos.SphereDemo::IsVrPresent()
extern void SphereDemo_IsVrPresent_m91FA26D14A2CE24DD8E2A2285BC981029803A721 (void);
// 0x000003BF System.Void RenderHeads.Media.AVProVideo.Demos.SphereDemo::Start()
extern void SphereDemo_Start_mA153F49EF2EE8361A940C391079742029ED1C615 (void);
// 0x000003C0 System.Void RenderHeads.Media.AVProVideo.Demos.SphereDemo::OnDestroy()
extern void SphereDemo_OnDestroy_m10D2C2F74809D59AA1C1A1E1EECD1F937AC5D045 (void);
// 0x000003C1 System.Void RenderHeads.Media.AVProVideo.Demos.SphereDemo::Update()
extern void SphereDemo_Update_mB0615BAEE1D57161B31DA2FFCB5CE2F373F97485 (void);
// 0x000003C2 System.Void RenderHeads.Media.AVProVideo.Demos.SphereDemo::LateUpdate()
extern void SphereDemo_LateUpdate_m599660EA44EE585A4BDC24F320700F47B853B3A6 (void);
// 0x000003C3 System.Void RenderHeads.Media.AVProVideo.Demos.SphereDemo::.ctor()
extern void SphereDemo__ctor_m027FA668E0A76EF3803026D83722D87902CF8EF6 (void);
// 0x000003C4 RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.VCR::get_PlayingPlayer()
extern void VCR_get_PlayingPlayer_m07A887D5B87224B4B98BEC287E08B536FB67EAAC (void);
// 0x000003C5 RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.VCR::get_LoadingPlayer()
extern void VCR_get_LoadingPlayer_m3F44CEBE706099BC4B4E92B73D42C4EE19837C54 (void);
// 0x000003C6 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::SwapPlayers()
extern void VCR_SwapPlayers_mEE914161C5FCB29390CD537F58A4D8E3514E0B3D (void);
// 0x000003C7 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnOpenVideoFile()
extern void VCR_OnOpenVideoFile_m598FE969E2763FD9059F0364244C7F8E8170BFD0 (void);
// 0x000003C8 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnAutoStartChange()
extern void VCR_OnAutoStartChange_m55A6A92415F1264AF2995139E2E31C6739AE8D45 (void);
// 0x000003C9 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnMuteChange()
extern void VCR_OnMuteChange_m82D467CAD88BAEE790C9812C5832AE42430C7542 (void);
// 0x000003CA System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnPlayButton()
extern void VCR_OnPlayButton_m42FC3F109D3027E145A5376C50189CA77FF2AE6A (void);
// 0x000003CB System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnPauseButton()
extern void VCR_OnPauseButton_m4F7AD3FE09504E9CEE370D596EA101DAC7C9C720 (void);
// 0x000003CC System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnVideoSeekSlider()
extern void VCR_OnVideoSeekSlider_mA467027887666B5C2118E7FB06E6F29781AAFF3C (void);
// 0x000003CD System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnVideoSliderDown()
extern void VCR_OnVideoSliderDown_mF133C85D1605CF4ECB859C4FB8903814C9B47664 (void);
// 0x000003CE System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnVideoSliderUp()
extern void VCR_OnVideoSliderUp_m2B41A78E17AB58F70534D9338BD10EE262D81132 (void);
// 0x000003CF System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnAudioVolumeSlider()
extern void VCR_OnAudioVolumeSlider_mB0B4C468DEC2D93CFED5E3F57BC6A706D1DE9ADF (void);
// 0x000003D0 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnRewindButton()
extern void VCR_OnRewindButton_m1C0C84A34B0AE9C32339268A409C8B683641F8BE (void);
// 0x000003D1 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::Awake()
extern void VCR_Awake_m47158A5A2FCD8A3DCB2A179D7A89C997B8EE740F (void);
// 0x000003D2 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::Start()
extern void VCR_Start_m08A6046EBF3DBC05BA4207FCA9DF98478C464373 (void);
// 0x000003D3 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnDestroy()
extern void VCR_OnDestroy_mD4375B8F9F107C8A3E690A519C84A013C8EC2127 (void);
// 0x000003D4 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::Update()
extern void VCR_Update_mA2E77C835DF6BAE61F095BAA6E544605C7964605 (void);
// 0x000003D5 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnVideoEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void VCR_OnVideoEvent_m705EFA9010D3DF0257BABC09F5E994CB338BA3B0 (void);
// 0x000003D6 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::.ctor()
extern void VCR__ctor_mA15FDE1E3FD5F12907E1EEA46EB16DCDF887D2EC (void);
// 0x000003D7 RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.CustomVCR::get_PlayingPlayer()
extern void CustomVCR_get_PlayingPlayer_m6EF241E17063D2CB4B787F8DFEA1927EFDFC19BC (void);
// 0x000003D8 RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.CustomVCR::get_LoadingPlayer()
extern void CustomVCR_get_LoadingPlayer_m09F7BA34CCA16BB5ECB100CC2EE55E20882511FB (void);
// 0x000003D9 System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::SwapPlayers()
extern void CustomVCR_SwapPlayers_m9ED54DDC7C91B2D4DB2E168E0C0816D20FFF65EB (void);
// 0x000003DA System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::OnOpenVideoFile(System.Int32)
extern void CustomVCR_OnOpenVideoFile_mCE6CF8E315D03113D941FD1E0C6BEAA1CD2E2622 (void);
// 0x000003DB System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::OnAutoStartChange()
extern void CustomVCR_OnAutoStartChange_mCC8E88778A330ECBE32C4F9C331A2F063CF8F502 (void);
// 0x000003DC System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::OnMuteChange()
extern void CustomVCR_OnMuteChange_m13BAD578D3D674586C8F65DF65DD9407D94EE428 (void);
// 0x000003DD System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::OnPlayButton()
extern void CustomVCR_OnPlayButton_m4CF288953443E8630597A76248431DCCC79FA170 (void);
// 0x000003DE System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::OnPauseButton()
extern void CustomVCR_OnPauseButton_m06934FAA9EDF7962B291E466F5854A0037216673 (void);
// 0x000003DF System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::OnVideoSeekSlider()
extern void CustomVCR_OnVideoSeekSlider_m1D59BDB05518F4FD5B3777A6779A4681C12136DD (void);
// 0x000003E0 System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::OnVideoSliderDown()
extern void CustomVCR_OnVideoSliderDown_mA476D7BD1D3C51267B16C557DD9324844AF9379F (void);
// 0x000003E1 System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::OnVideoSliderUp()
extern void CustomVCR_OnVideoSliderUp_m926342FF46ADA5E7E2E0B1B00D0ED64ABD83DADB (void);
// 0x000003E2 System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::OnAudioVolumeSlider()
extern void CustomVCR_OnAudioVolumeSlider_m9EFE453A75AEDF9122D021141506F1F0B8E3FD65 (void);
// 0x000003E3 System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::OnRewindButton()
extern void CustomVCR_OnRewindButton_mEF0F2E27DE5D4DE9376C49EABA6989F4C27298DC (void);
// 0x000003E4 System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::Awake()
extern void CustomVCR_Awake_m16CA8271866220DFE77152671008F4ABB4C11A0D (void);
// 0x000003E5 System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::Start()
extern void CustomVCR_Start_m5DD1F517FE36D204C69C5E0D5906F45BA1FB5F0B (void);
// 0x000003E6 System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::OnDestroy()
extern void CustomVCR_OnDestroy_m1288DBB6534B8F7E16A8D532D6F124335D875C3D (void);
// 0x000003E7 System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::Update()
extern void CustomVCR_Update_m2BC2EB5E957B5D7FB16056271C21A56424111244 (void);
// 0x000003E8 System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::OnVideoEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void CustomVCR_OnVideoEvent_m42C4E6A6DE11F3EF63315E63B0433B7E32D275CF (void);
// 0x000003E9 System.Void RenderHeads.Media.AVProVideo.Demos.CustomVCR::.ctor()
extern void CustomVCR__ctor_m9F1009D9F62A9F5983E8309C07AAC68EA2EE962C (void);
static Il2CppMethodPointer s_methodPointers[1001] = 
{
	AnimController_Update_m62BF22EDB961D930B5C47C37CD39A85B9FD6BCD6,
	AnimController_OnVideoSeekSlider_m97E38D19C384286555B65C9BF061D334E9A7DD53,
	AnimController_OnVideoSliderDown_mA0269AD69FFE9BD501C66C3B6CE35E7D0A4D8BDB,
	AnimController_OnVideoSliderUp_m3E3DC3C4026ADC7B7777B4C0A38C68E46AF51915,
	AnimController_AnimatorIsPlaying_m9E3D3BD45749386977C5609A3C0EAFCA660D45DF,
	AnimController_OnPlayButtonClick_mBDE53ED2D5DF7A3CA639B181EDD192F2AE098770,
	AnimController_OnPauseButtonClick_m44347B3B4BAA4361522DE815DCBFE48131422B3C,
	AnimController__ctor_mAAE2BBA5BF1073E17D9467F41758607C12ADEE81,
	CameraRotator_Awake_m5AA3F2BBC511618BECD9E5EA08D71750D4522200,
	CameraRotator_Start_mC6E87BF2AB2273215EA011F2270A709A26DDD1F1,
	CameraRotator_Update_m4F5601D14C4E9D02EFF48E41B31E793DCDCB9818,
	CameraRotator_LateUpdate_m2D6A96DB32FB22127B3D23F84638FE647935C534,
	CameraRotator_SetCamPos_m526BABFF2202592FD21411706FD3630E6E9A1F47,
	CameraRotator__ctor_mD803289E569C0F7265EBAD8FCDC37C5F62D7A8BB,
	HotspotTap_Start_mACCDD1BC5002355168339126DA00732691780C4A,
	HotspotTap_OnButtonTap_m93BFD0C52481FC75D28CED13C89190213A0F20C2,
	HotspotTap__ctor_mEE35A82DB49A8D49E927ED579F25934B1F68E159,
	IntroScreenInteraction_OnEnable_mF86035837AD455CFEBFE3161338EA0C7FC08CEC8,
	IntroScreenInteraction_OnValueChange_m82EABD7C220921A30D8D0F4A0CED16F8620CEACF,
	IntroScreenInteraction_InitUI_m153FB04F2A1F4E32CB6D16CAC3AFED2211EAEF4A,
	IntroScreenInteraction_ToggleARFoundationImageTracking_m289318574A11CC5F04260A2DE4DDA1F619133811,
	IntroScreenInteraction__ctor_mE876D0F3D3D9BCF02E104722D05DBD23B45836E8,
	MediaPlayerConfig_Awake_m45FA577B2B9AC5C2931BBD9371BA897017D3E9A0,
	MediaPlayerConfig__ctor_m9E9E71C77DB5E1EF5922239A716F38DCA62DEB1B,
	ObjectPositionUpdate_Awake_m4F27CD742E2A60BF6262945F870D718A25642CEB,
	ObjectPositionUpdate_OnEnable_m07FE1D359D60E197369ACE7DA91D0EC1D5E39E15,
	ObjectPositionUpdate_LateUpdate_m02514C4458EDA9045C6828B55CFDC9345C827BE3,
	ObjectPositionUpdate__ctor_m611A04727AD89618B6322B3E6AA4D04C5AF6D747,
	PreloaderController_Start_mC76B8BFBE38CB643C446F6EFFA3AE956A168A755,
	PreloaderController_DisablePreloaderPanel_m5B3399000BD62311676914B199EF4140B6F37CD5,
	PreloaderController__ctor_mBB2B6D0AF69CB8F8372E1A80AD7CD8FE60F790AF,
	U3CDisablePreloaderPanelU3Ed__3__ctor_m78A4AD06EDA904CACCAC2FCFE42949BB0D950658,
	U3CDisablePreloaderPanelU3Ed__3_System_IDisposable_Dispose_mB1FFDC5C6C5877BA63FC0D5EDD299367F062C01E,
	U3CDisablePreloaderPanelU3Ed__3_MoveNext_m1295B0BBDDE925FC3BAC462DC722EF59F4301A3E,
	U3CDisablePreloaderPanelU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5E78A5B68092F270355E899D99CE2D748705A525,
	U3CDisablePreloaderPanelU3Ed__3_System_Collections_IEnumerator_Reset_mF42974D782E8ABEF2D7C266ACC18B0C5F80EF45D,
	U3CDisablePreloaderPanelU3Ed__3_System_Collections_IEnumerator_get_Current_mF638D8B9A629459A3E4DE1EAB729369B457AFD82,
	RotateLoader_Update_m317AFE64800E56B51476F10523A505852CE33431,
	RotateLoader__ctor_mFE2DD9D37030A6953D0D2851B0EBAA892EE7EDE3,
	RotateSphere__ctor_m48C2ED24E2F00620B54810B89C23B5F4CE52C54A,
	SafeArea_Awake_m83B9F45A0F995E167FCBB289812CA2A8F5286A47,
	SafeArea_Update_mC20518F2F8D9C1949F1E15D905E3C505D55B7A56,
	SafeArea_Refresh_m7802DCD0A93F2BBE509BEB7B43EB519B9671A9F1,
	SafeArea_GetSafeArea_mA3F3655E84713B922EE78D97FFBE3A80DA1B09C9,
	SafeArea_ApplySafeArea_m12670F705115000C12AFBC1F426E71E558115A87,
	SafeArea__ctor_m8BEAF767B44EA70C714E36C298FC7DF3F810C3E5,
	SphereRotation_Start_mB8D37EA2CD913BC0C058A6B36A0027A939E927EA,
	SphereRotation_Update_m12DF0B48D4E34CA74B87CC3A368A29D908CF5403,
	SphereRotation__ctor_mDC557F1C3ED6E800FA70137DE8C55CF043E99339,
	VideoTimeDisplay_Start_mFA4D6284EDA3B3AB7D7022BEAB98C9527968AB22,
	VideoTimeDisplay_Update_m524CCDF8A8837D4396BCE9549A1880CB038A73D1,
	VideoTimeDisplay__ctor_m5CD881BC6D41DF1DA09C7632EEBC7D037CCCAB93,
	OptionPanelManager_LoadScene_mD7098291659EE92D1FAD0B25FDF5F4C97B9FD249,
	OptionPanelManager__ctor_m81960761A72AB856A636A4C63E9720B065D36F6C,
	VideoPlayerController_Start_mBCAB3295960ECCA9AD99039F2F176B1D9E6A8FCA,
	VideoPlayerController_Update_m348487057CC7EB8BAB768D127A11DBDE72BC0861,
	VideoPlayerController_InitComponent_mFA3B6FF2C70D94290C042B40C7763AB528EF02E6,
	VideoPlayerController_StartVideoPlaying_mBA95547320C9617A544BC8A692280E0F2040F6EF,
	VideoPlayerController_OnVideoButtonClick_m4524089D8D6A643A7F464F8EAA9BFC87321154AC,
	VideoPlayerController__ctor_m37CB855AE1301D1DD45D07E62CB382896A550D46,
	U3CStartVideoPlayingU3Ed__13__ctor_mA06BAE3A6CF5203156A73FD7AF9108A145AF0EC6,
	U3CStartVideoPlayingU3Ed__13_System_IDisposable_Dispose_m8E449EF5BA253A939DCF11CAE39A4C8355DA6489,
	U3CStartVideoPlayingU3Ed__13_MoveNext_m5012F6C3645CCBD3E50073D4D641AF494BD1A12B,
	U3CStartVideoPlayingU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDAAAB33056322C26FB2E7185CA14F33A10CE42D0,
	U3CStartVideoPlayingU3Ed__13_System_Collections_IEnumerator_Reset_m149BF3B02F1A023E95FAC7BBD2A5CF729F128F37,
	U3CStartVideoPlayingU3Ed__13_System_Collections_IEnumerator_get_Current_mEC51760B8923B9C8CC3193FD8D8275B4A5E8FC9B,
	VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B,
	VideoVCR_get_LoadingPlayer_m8B3CEF700E0B37E6D1287D8F37E9264BF9C1B67E,
	VideoVCR_Start_m833675486A1F6707D25A6F64A4D84F9C5EEEA3B8,
	VideoVCR_OnOpenVideoFile_m63A5EFAA337F2DE7919B1A623776E3B85CE4E310,
	VideoVCR_Update_m3D389660CD8DB181C6F3BB44D523AB359057F6F4,
	VideoVCR_OnVideoEvent_m5A55B048083EAAD1F09A39BA3416B3F44100CD7F,
	VideoVCR_OnNewMediaReady_mEDC2336A6CEA55B77B6A3A7631F66019A527CF21,
	VideoVCR_OnVideoSeekSlider_m6BA20F479F74EC5A5A6ABBF271ED25B4F5C947AB,
	VideoVCR_OnVideoSliderDown_mCBE099D7D6C532F1EA05175253E3FB4050B67548,
	VideoVCR_OnVideoSliderUp_m57DAAB9784ADC718852E2572DABAC44546C1EAE1,
	VideoVCR__ctor_m44F9A9C7A3BCA8F31ED617BF04AE57613DDF616A,
	WagonWheelManager_PlayAnimation_m0D365F74357CC854514D501A3BD3CBC4E2D95D5F,
	WagonWheelManager__ctor_mF1AA3FD185EFA8F56E870987C826728546C4B21E,
	SpeedCheck_Start_m456F2C4F0D2EA69AF308C61043A0101F78F0F6B0,
	SpeedCheck_OnDestroy_m17BAC76C7A267C8E5CBD7AEABB0A7DB2249FA5DF,
	SpeedCheck_OnClickSpeedInfoBtn_mA1373B25946BC1E6ED5D6B8CD6485397672F40D0,
	SpeedCheck_OnClickSpeedExitBtn_m46ECE177987EBC484FB31F4140094BE7054598EC,
	SpeedCheck_OnAutoSpeedCheckBtn_m212E9D130D438DD02E5C7D0FAD55D343D6564CAC,
	SpeedCheck_OnClickSpeedTestBtn_m3F6CB2EB19F3FD247BA9292D5B0CD2EA9893EAAD,
	SpeedCheck_onTestCompleted_m7420BE2FB2AE7EBBA8EC88A99FC43B58F7EC72A1,
	SpeedCheck_ResetSpeedTestCheck_mA096A0F8602D61EC9D018A6115B092ED25F254BC,
	SpeedCheck__ctor_m9F63AD20826DCE3C117DCF1CD46854C4B19F7E5B,
	U3CResetSpeedTestCheckU3Ed__16__ctor_m3D053FE8D19035A8F7E29F2EEF8D7E1892B0B68F,
	U3CResetSpeedTestCheckU3Ed__16_System_IDisposable_Dispose_mCD152DEBBE8174D003FF33217C5BD072053482C8,
	U3CResetSpeedTestCheckU3Ed__16_MoveNext_m2ADEAD718840B9B8FF62BDE9B86A6654B93D2770,
	U3CResetSpeedTestCheckU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD32DC79D428A42EE1AFBAD15A32C1E8A132EC22,
	U3CResetSpeedTestCheckU3Ed__16_System_Collections_IEnumerator_Reset_m4DF85D13FFF1B60EFF83963ECAA4A5728B292A01,
	U3CResetSpeedTestCheckU3Ed__16_System_Collections_IEnumerator_get_Current_m9A98DBB9701B673D1CEE0E764B117C6A24593E87,
	DynamicPrefab_get_alternativePrefab_m4B48EB6B3CFE5493EDF32163F0A185A3E0B284F6,
	DynamicPrefab_set_alternativePrefab_mB496995B03DBC0C9F41FE31C6E09E6D711311204,
	DynamicPrefab_SetError_mC4122FB4973F14E169E99825FA9A01DFA9A6B061,
	DynamicPrefab_Update_m34D182813AFDCF28D8C664D835F61912273079CF,
	DynamicPrefab__ctor_m7B0ABDA21E114912AB522C7DBB5413668AE9D425,
	PrefabImagePairManager_get_imageLibrary_mF75838258C00F909150CB75F5989903E7AF21286,
	PrefabImagePairManager_set_imageLibrary_m5598C2AC4459C6D5135CB5AC153B803874958BC9,
	PrefabImagePairManager_OnBeforeSerialize_m5B178C81BC1C1982D01E7CABB3DA6C8152F5F451,
	PrefabImagePairManager_OnAfterDeserialize_m10CAF6488BD05E15650CCAC2D8DDFDBEBE4C6D53,
	PrefabImagePairManager_Awake_m5D1BADF276ACEBE409796C5783389B4DDBC6A076,
	PrefabImagePairManager_OnEnable_mA5D8272C6A44CC8ED3E26144CC2424F0B3F48133,
	PrefabImagePairManager_OnDisable_mC339AF0AAEBDB53FEC7E643E1A69D9560A23890C,
	PrefabImagePairManager_OnTrackedImagesChanged_m8950068F3FE83FBE39C0D38F1EA06FA58F82DF5E,
	PrefabImagePairManager_Interaction1Editor_m4130DB0CCFDB7964330885CE97B75CD92DBFC2B6,
	PrefabImagePairManager_Interaction2Editor_mE918E1C78F95613301EF36F8FD822C257E505ACC,
	PrefabImagePairManager_AssignPrefab_m3A612DEED64B82D56C3FAACF8BF4E46A7729BA72,
	PrefabImagePairManager_AssignInstantiatedPrefab_mA596673AAE1AC44CA632516170F79C12D234ED1D,
	PrefabImagePairManager_DisableInstansiatedPrefab_m30442AD60122D69FBAEC93CD05A5684D3EFAED2B,
	PrefabImagePairManager_GetPrefabForReferenceImage_m37CE15147B141D8ECAF155CDD7BBEAF86C109364,
	PrefabImagePairManager_SetPrefabForReferenceImage_m660F9185F078D8492942F596D6596986DCF68DC8,
	PrefabImagePairManager_LoadScene_m503C6FB01E169E2675FA2B9C423B26A260CAA8A5,
	PrefabImagePairManager__ctor_m706F23D05C16A9911D7637808AE62800AFFE6760,
	NamedPrefab__ctor_m4C5A1EAC954851954E10ADF5F4ED646967BAB252,
	ApplyToMaterial_get_Player_mE4A108466F43FD42227D8BB5692BD2F06EAF94AD,
	ApplyToMaterial_set_Player_mA02FDAAF0E3D063CE10B8F1923905C678222DD5C,
	ApplyToMaterial_get_DefaultTexture_m180433D010CA7CD13A61A527B0F3E2AD9222BF43,
	ApplyToMaterial_set_DefaultTexture_m251DF798497E0D3BBD8F91BF02782059C50AD184,
	ApplyToMaterial_get_Material_mFD0C0967C87536D80A3E4FC431B5B9819BCCC836,
	ApplyToMaterial_set_Material_mFD09E367E205B61D54F95F27970C922DDBFD672A,
	ApplyToMaterial_get_TexturePropertyName_m35529C314DBB9AEA691C1953B031F90AF4ECE4DB,
	ApplyToMaterial_set_TexturePropertyName_m018C21AA0720812E6994511E85BC522767069B5A,
	ApplyToMaterial_get_Offset_m64B3B9C240C9270049F71AE82DA7F84DF132EDDA,
	ApplyToMaterial_set_Offset_mEFF618E2CC642A2FB7300DED24ECB7C10DF0F61E,
	ApplyToMaterial_get_Scale_mBC0A076D273D5B03ABE2F5F4FCCD92FCECBF3492,
	ApplyToMaterial_set_Scale_mA4F35496AC29B976312D4F0FC8DE8C955451655C,
	ApplyToMaterial_Awake_mF57A0CD8AE163E94A9912335CB9982CDB6D51262,
	ApplyToMaterial_ChangeMediaPlayer_m09A51D5A1FD589173D510ACC0C87E0DD8D3CDC03,
	ApplyToMaterial_ForceUpdate_m055C83BFB481FD3A4B710438ADBE419461A2C1D4,
	ApplyToMaterial_OnMediaPlayerEvent_m69F8A3A958E48868103553214AB759BBA1363457,
	ApplyToMaterial_LateUpdate_m646817D3F38932B8F643009F46C0CB2781AF8BD7,
	ApplyToMaterial_ApplyMapping_mE2AAA37264E1C02C0E6F95C65F70B75F0015BBEC,
	ApplyToMaterial_Start_m044ECBD6843E30F94943595D9C4F368D31AED3CE,
	ApplyToMaterial_OnEnable_m035F0B8317DE8F40D4EE130F87DB645B734522CD,
	ApplyToMaterial_OnDisable_m80981B9631CE358B1E1E565CE03317BE2A1D6275,
	ApplyToMaterial_SaveProperties_m6D55871801FDC6D646FE5BA89560C1C69FDBA64B,
	ApplyToMaterial_RestoreProperties_mB23F25A23023C5361FBA87F66EFC7DAA45E6CCEF,
	ApplyToMaterial__ctor_mFE05BA02F293D7CF587666EC6EC9E729038D9D7D,
	ApplyToMesh_get_Player_m34DFE94E5CF989DDA86FA1D4EE3A90B6BA1C5EAF,
	ApplyToMesh_set_Player_m502F3E617C6AC30DF92BA41929CB134EB2F84049,
	ApplyToMesh_get_DefaultTexture_m9359CB062A2495DD0EEE5E557F806C33DCDEF4CD,
	ApplyToMesh_set_DefaultTexture_mBBF396CED119E409BD6B7E67E262B930A2C290BC,
	ApplyToMesh_get_MeshRenderer_mBEAC1E6DDF2B16EAF4660459DA1F72257E9D5637,
	ApplyToMesh_set_MeshRenderer_m761841DE09113CCDBC474B53A57DD8E73F2DEDC9,
	ApplyToMesh_get_TexturePropertyName_m4BB67C6E45D4CA9B8857C92D0283B37C0A3737A0,
	ApplyToMesh_set_TexturePropertyName_mF4B305580BD656494F12364667F234A9CD461631,
	ApplyToMesh_get_Offset_m962C4C9F6DBFBE2A82764ED86D07B121B277AB71,
	ApplyToMesh_set_Offset_mD6E67B51416E24E379F879625AF88C180E399FFD,
	ApplyToMesh_get_Scale_mB4B84855E8C37CCEB4CC2B0035F21839A9D611F2,
	ApplyToMesh_set_Scale_m05D49267C11285C8363B62505347A70B8ECCD579,
	ApplyToMesh_Awake_m4CBAA34D4BF40F623053775EED39C51A86EC0DC2,
	ApplyToMesh_ForceUpdate_mABC9DB44349E5A8FA76A69498FD9E6565588D821,
	ApplyToMesh_OnMediaPlayerEvent_m33DADB5C28E62DB87A2A26525A8366D48423CFDE,
	ApplyToMesh_ChangeMediaPlayer_mD448992D5AF017C53569802DBF70D32982F8C786,
	ApplyToMesh_LateUpdate_mBDF6CB7F02FB0A8F21D144331A8B39F7D90F42F7,
	ApplyToMesh_ApplyMapping_mE756014E8DC353A4C5CF586F0326208F566CF631,
	ApplyToMesh_OnEnable_m6EF3DB604109E050F3F5B352CE89EF656935D0D6,
	ApplyToMesh_OnDisable_mA5556CF1F9E0CF01DEC0C2AC3AD94CB052D0DCBC,
	ApplyToMesh_OnDestroy_m24D24B6500CAA149ACA31C554D31BA83DC419122,
	ApplyToMesh__ctor_mAE162E1EDA8B1A4B37E7922883F862EA409BFA5E,
	AudioChannelMixer_get_Channel_m99DF99E72031A4B3302AE53ACD3E6A250DFA4113,
	AudioChannelMixer_set_Channel_m1BCC18098285985B4C6F436501388EF980AE88C4,
	AudioChannelMixer_Reset_m5FA298BA5C4469880AC721438A70852F405E7F2B,
	AudioChannelMixer_ChangeChannelCount_mA8C34DC1D9531D8F2B51A62CA407873549AAA80D,
	AudioChannelMixer_OnAudioFilterRead_mC9AC0C032748D23794E33037E08176BFF9C5CC0F,
	AudioChannelMixer__ctor_m8CC1FA9D958F49DD8D773727B0F39A6DB6B5E695,
	AudioOutput_Awake_m2050CC5A872FCFA6F082E504778D622CD91EBE61,
	AudioOutput_Start_m9B921B55716EFE4F69D273E1C6495C07BC4DE846,
	AudioOutput_OnDestroy_mE641C1F39AB1EB7080264AA2131D9B75A9168D94,
	AudioOutput_Update_m3F2BE6A4F05F06B0697A872D0D65749A35668DA3,
	AudioOutput_ChangeMediaPlayer_mE93E585604B1A2F1CA0C9A1F90CAAB8D5638349F,
	AudioOutput_OnMediaPlayerEvent_m4B36BC8A9CE646233797806FEFEF643C70AEB225,
	AudioOutput_ApplyAudioSettings_m060ADC350962EF9DF6EB5B91BD459BF87730BF2E,
	AudioOutput__ctor_mF095BCD99C59435EAD19BABBE921E8E95FEFD87E,
	CubemapCube_set_Player_mC2561BF5A0A3338AB38E2674D6A3A4C1E6364C27,
	CubemapCube_get_Player_m49D912263C8F85C5E546CBC23C01BE34A2CADE36,
	CubemapCube_Awake_m9020A1A0D1DBB7D3D9E7C25AE922CAD5515E5009,
	CubemapCube_Start_m7E614C30B852BE7C23741004CBE240D7D8DF4B19,
	CubemapCube_OnDestroy_m03CF6F16B3D4F7C5A436F4B5A60F859AB35A5E7E,
	CubemapCube_LateUpdate_m2107AA22FE82E8D44CA16C0C9EE4160882E34677,
	CubemapCube_BuildMesh_m4ACC62FD39677EC8809FED75D13BC7A013CDC443,
	CubemapCube_UpdateMeshUV_m9B8A7666F82E6454240303E13D825C047CCCBFD3,
	CubemapCube__ctor_mD0E20870247685A4A708E47FD67F733E706B6CA9,
	DebugOverlay_get_DisplayControls_m9E30AE4713563051EBAD98AD1D5B0E6D603E154F,
	DebugOverlay_set_DisplayControls_m729B63EE184769C8058B9F1C39DD035AB6A75EE6,
	DebugOverlay_get_CurrentMediaPlayer_mF84BBC4DB75C10951360A6BAA9FF59BCBC911773,
	DebugOverlay_set_CurrentMediaPlayer_m2210A26D6F487CE9E1D8BC09A0854A8F08191DE8,
	DebugOverlay_SetGuiPositionFromVideoIndex_mD7937C27EF45A4075E62F4986E03E308298642FA,
	DebugOverlay_Update_mBD725BEF647A65E41F94454B5FB689F8962E813B,
	DebugOverlay_OnGUI_m2619DB638775A95ADBE87BEC38C50927B0806A06,
	DebugOverlay__ctor_mD49E797F7986FED987E143169289CD93C0C7700A,
	DisplayBackground_OnRenderObject_m7F3E116F14E7C40A84E359A7CB6611DF98A31141,
	DisplayBackground__ctor_m6DE367A42EBE333582DADA840EA9BFE32FB0201D,
	DisplayIMGUI_Awake_m5B95BC021AA326F58BC59D9A4200099BF2A9B69A,
	DisplayIMGUI_Start_mC70CFF0D016EE39845E060EA8C8A120F49BDDA04,
	DisplayIMGUI_OnDestroy_mCE806FEFAFF8AE38D2985B18EA983935793968BE,
	DisplayIMGUI_GetRequiredShader_mAF9F2F42650530BE702DB36599016A386EF3985F,
	DisplayIMGUI_Update_m1229BAE7E6313AB78108FDC97311D03342B25051,
	DisplayIMGUI_OnGUI_mBCAB4C4D2D87324B9738790E55DC539FF0309144,
	DisplayIMGUI_GetRect_mBC72F8C464AB8CCBBF97C94CE0990F33A08ABD9E,
	DisplayIMGUI__ctor_mD1F1F421BE9F9773F708105410E1C55196FC295A,
	DisplayUGUI_Awake_mFF0F97E26986BF0AE3EF5E228FF4D8186945168C,
	DisplayUGUI_HasMask_m3141EA6B1347981931F3F88C2048AC87E7281FE1,
	DisplayUGUI_EnsureShader_mB0B2D1E2DA7750E763E65FA9F7CC5DBF89922458,
	DisplayUGUI_EnsureAlphaPackingShader_m93178AE0C55535E2B85CBCD9E1D538A667A1116A,
	DisplayUGUI_EnsureStereoPackingShader_m822DB4235CDCF043B81BAD9EA8A183D4AD33AC8F,
	DisplayUGUI_EnsureAndroidOESShader_m5795A04CCEDB9A0C84E37DB0453684D689D07114,
	DisplayUGUI_Start_m95911CAB8F4411CAF241B6BDC09F70A7CC1C6411,
	DisplayUGUI_OnDestroy_m59A5C1FD22FAA5720979AD4ABE61613246D7D770,
	DisplayUGUI_GetRequiredShader_mDCFE0B8691B2F19FD809B635878FBADF4E98ED80,
	DisplayUGUI_get_mainTexture_m0A3AB5D6028B0E6753AC4F70F4E5CDD38B41666F,
	DisplayUGUI_HasValidTexture_mC954E6C7D29FADAFD9139D21AA818B346E723B0F,
	DisplayUGUI_UpdateInternalMaterial_mB2A2E82D962F7CE3021149D53F42AE16708DFE62,
	DisplayUGUI_LateUpdate_mCB3681D3492E0789D8CEB50C2AE80B7352F4A617,
	DisplayUGUI_get_CurrentMediaPlayer_mBA7C887352D4F639992E7370F4D41E19966E4FC9,
	DisplayUGUI_set_CurrentMediaPlayer_mFD3D10BCB177079EF99106CFE592C0F9399C65CA,
	DisplayUGUI_get_uvRect_mF1C83C3753E5BE34FD5BC73992CE2D014FEE3DF0,
	DisplayUGUI_set_uvRect_mE7DAA23A0D257FA9129BC242BA93E4ABCB464D0A,
	DisplayUGUI_SetNativeSize_m342646EAEE1E4D4D4AAF41E8DB65AD538C461F39,
	DisplayUGUI_OnPopulateMesh_m7623DA3F87577B0EAB0728789A2F28F01B7FE337,
	DisplayUGUI_OnFillVBO_mC3C5089E9453CF286F0E86574C63D47814AF537A,
	DisplayUGUI__OnFillVBO_m20489F28078FE2CE16E4A76FE91E45B173FCFE44,
	DisplayUGUI_GetDrawingDimensions_mC9EDFA84D3A338C66DA05A0357582CB7D125E0FF,
	DisplayUGUI__ctor_m9EBC66FE1EA44F525B68602AE2224D3584AE7FF1,
	DisplayUGUI__cctor_mE5CD163F8E1D998AA11277AE0462B73B59762902,
	MediaPlayer_get_FrameResampler_mA5D1C9C4E74656A2E8EBD462857C082471B1EF44,
	MediaPlayer_get_Persistent_m6E9CB96C4E83F49915BA1108C9E4EC9593E9908C,
	MediaPlayer_set_Persistent_m6C91414B9407383D70F9AC2CBD66C7896293002D,
	MediaPlayer_get_VideoLayoutMapping_m8ED94E495E3588064D28C225F4B17861BD737396,
	MediaPlayer_set_VideoLayoutMapping_mF534FD94CA88B7E75BDF22202D0DE522B2306507,
	MediaPlayer_get_Info_m31D89D12C5A2BE09E77590653E52188AC74F4746,
	MediaPlayer_get_Control_m09EF1B68689FAF15E3803E83FD8B1CC4CF9A4EE6,
	MediaPlayer_get_Player_m688CA3F36C5B88A097A30416BFDE70C27875D47E,
	MediaPlayer_get_TextureProducer_m38F77755A7195D3385984C2590059B7A3BBAF31F,
	MediaPlayer_get_Subtitles_m9D74A06A13759FE8E694943D56FA46531C9D924F,
	MediaPlayer_get_Events_mA5CD0868386223CB631AD42B495F8A01D1D63C1D,
	MediaPlayer_get_VideoOpened_m9B047E21B225B7C05A1635836CD7E6EED748A59C,
	MediaPlayer_get_PauseMediaOnAppPause_m14DF4EECB146B8E774CBF6C78F1DB3D923B64209,
	MediaPlayer_set_PauseMediaOnAppPause_m4DCD4C2098E25AF4C8BC0CFC4464B2C011BEAAC4,
	MediaPlayer_get_PlayMediaOnAppUnpause_m923367EB740D51525A8B8096CA993C0963418C44,
	MediaPlayer_set_PlayMediaOnAppUnpause_mEC4C0321DA42B46A741CAB5DFA25B950A359B890,
	MediaPlayer_get_ForceFileFormat_m75BF54921E38236F4879E14AAE14972D01B1DC42,
	MediaPlayer_set_ForceFileFormat_mFE5E142A459082AB602AE5F15D647CEF0110E4E5,
	MediaPlayer_set_AudioHeadTransform_mDDCF7045FE909EA02DD378A97ACB3139BF18886E,
	MediaPlayer_get_AudioHeadTransform_m8538C01175917D5165A7E9E7C904D286D4A0DA6A,
	MediaPlayer_get_AudioFocusEnabled_m1DAE492C86B9F9FB0CA8CF3D678D3925591B6DA6,
	MediaPlayer_set_AudioFocusEnabled_m5EBCF68B000D74A5A5ED7EAC742AE0D6A0853006,
	MediaPlayer_get_AudioFocusOffLevelDB_mFB5664F3DBC37BF72F2AE7DE2C2895951C827280,
	MediaPlayer_set_AudioFocusOffLevelDB_mC3493C18678823B175A84E961433C81B00818233,
	MediaPlayer_get_AudioFocusWidthDegrees_m4AD628A0C678E594418E6097AF928781DAC28260,
	MediaPlayer_set_AudioFocusWidthDegrees_mEF4585BFD0DD1729F5B68CA9AA45DB96B357E324,
	MediaPlayer_get_AudioFocusTransform_m9EA0BCFF10AF45B1212EB8B37CF20B5A8BFFCB87,
	MediaPlayer_set_AudioFocusTransform_mFDD80C7474172588A4D8D8DDDB39C2E0B96E64DE,
	MediaPlayer_get_PlatformOptionsWindows_mB04A1E0F25F0FEEDE3AD6DE7A38F19A9BBE94A9C,
	MediaPlayer_get_PlatformOptionsMacOSX_m7C394154D29BE6F5F5DA3A596D2934B31928AD71,
	MediaPlayer_get_PlatformOptionsIOS_m18AF9591D60E768734738E7D861DDEDA8BBE225E,
	MediaPlayer_get_PlatformOptionsTVOS_mCA2000A95FD7F486DD6EC451D9A6C0A6FE17FB33,
	MediaPlayer_get_PlatformOptionsAndroid_mC6FF33FAE33B28D492A641ACB61887CA8D607A64,
	MediaPlayer_get_PlatformOptionsWindowsPhone_m0CDCF5B8C8BFC20434FC99CD5E45579A9996DE01,
	MediaPlayer_get_PlatformOptionsWindowsUWP_m99E824A214F92807AD51BF8D94E372353AEB92EF,
	MediaPlayer_get_PlatformOptionsWebGL_m2C8597C9207355C2165BE1E920117297D65AF2DD,
	MediaPlayer_get_PlatformOptionsPS4_m36924934DD4641997B4050EFEF1B37D99D9281B6,
	MediaPlayer_Awake_m8047A05C5C88BB633E53DDCE11D45492562B0E38,
	MediaPlayer_Initialise_m128309834479E31C3BA710EF8EB0FB6DDD844F10,
	MediaPlayer_Start_mC75491DEE917AF37C03558386EA1EEF9244A94FF,
	MediaPlayer_OpenVideoFromFile_mD7D917C6B56F5CC9B76A04D8E4B24219277BF881,
	MediaPlayer_OpenVideoFromBuffer_m07835953B9BC11740F437E015962DF20E8A846DB,
	MediaPlayer_StartOpenChunkedVideoFromBuffer_mBAA741B63075DC5AFA34855DD7A0D9C9F20280A6,
	MediaPlayer_AddChunkToVideoBuffer_mC98E6C5ECE8D4E35CD3752B22560C89B8BC4D53C,
	MediaPlayer_EndOpenChunkedVideoFromBuffer_mCF5BC7764BFC8267CBCB714C16C52B48BB6A0D1D,
	MediaPlayer_get_SubtitlesEnabled_m8BEA5B290997DB1F1594BB4D63FA3B4CF5554546,
	MediaPlayer_get_SubtitlePath_mA4DF6F97D82637A48FBC64B6807FE9A14E86942C,
	MediaPlayer_get_SubtitleLocation_m4C5F2093153FF318EB69E55E79610D622978D404,
	MediaPlayer_EnableSubtitles_m9D823D2CE08F6A45DF743C39C956CACF159E428F,
	MediaPlayer_LoadSubtitlesCoroutine_mED609F44A8A99CD62D067088B7AF30B2FB347B00,
	MediaPlayer_DisableSubtitles_mE90CCBDA202A190C89435EE61431FF9B7EFA9CD9,
	MediaPlayer_OpenVideoFromBufferInternal_m500CA58C2AC38BC44A7867BCA50B03B300DA7A40,
	MediaPlayer_StartOpenVideoFromBufferInternal_m5FC043B08CE067F82C6A73ED091B2AC60CB19AF8,
	MediaPlayer_AddChunkToBufferInternal_m15D5E059514F18AECBB8AEF35F31A9288B8E9DFB,
	MediaPlayer_EndOpenVideoFromBufferInternal_m660B1978A8C4A40A8752768DBCA8ED61FCBFBB9F,
	MediaPlayer_OpenVideoFromFile_m0CB31F5C91B384E9E901839D6445BFA5B0E520AA,
	MediaPlayer_SetPlaybackOptions_m001CE720516D25516A43176E91A931D885750BCD,
	MediaPlayer_CloseVideo_mB754997E2B47198216C0BFCDF6249EF07B9F1DDD,
	MediaPlayer_Play_m893CBBB81F9F455B7AA764398A50A007D3C28F6C,
	MediaPlayer_Pause_m7194C52FC0A95953B2582B2615AE700418954A94,
	MediaPlayer_Stop_m132DCE4F115F01FEF30DE82B8E779492F5DABB1C,
	MediaPlayer_Rewind_m0CCEAC97B35578A2390B8BB6D91B1DE6E74C4C6E,
	MediaPlayer_Update_m9629B44815C23F82A4B92831BB8F059BADDFFA03,
	MediaPlayer_LateUpdate_mD2FB1DB825D252C1ABE5642FC9DC2E93609D74DC,
	MediaPlayer_UpdateResampler_m0475B150B1145815617029BC13BED0E4E92737E2,
	MediaPlayer_OnEnable_m897CA0E84D5D0C31B0E4FFCCF5D4DCF638CD37DB,
	MediaPlayer_OnDisable_m78371D06654BB553C440CE1FF966B8FB12184738,
	MediaPlayer_OnDestroy_m3C9CB00122C2B00AE6C1A213F45AAAAFC8510374,
	MediaPlayer_OnApplicationQuit_mDDCE2EC733D7BAD2CFDBDB4408CDEC4238F0D22D,
	MediaPlayer_StartRenderCoroutine_m1B7326C4BD351B016DA8CF7C22FBF6E23F279BB2,
	MediaPlayer_StopRenderCoroutine_m2470F527360D2E59EBE37AEA9554C299BD8C1A4B,
	MediaPlayer_FinalRenderCapture_m58C8CA21EA62D3E54003358527F735B1D3AA5E4D,
	MediaPlayer_GetPlatform_mF8F85C1DA7A645F8FD49C6883EE6D6B757DA9EED,
	MediaPlayer_GetCurrentPlatformOptions_mF9802FA83DE27AF5AD7DC75CCD2A5CD3ACFE9F23,
	MediaPlayer_GetPath_m636FB7C9C4A734D17EB3E751F36F06B7E41D0127,
	MediaPlayer_GetFilePath_mF4E9F9BBD1065AD613CF3668DE8D7D480C711654,
	MediaPlayer_GetPlatformVideoApiString_mF405C309E21A305CDA7DF801CFE28CDE54E4C2E3,
	MediaPlayer_GetPlatformFileOffset_m8D563F7C0EA9019475A2CE2E861D4589172961D3,
	MediaPlayer_GetPlatformHttpHeaderJson_m2BDAF4A3F40C19E2B0F4D9C5AD600E16092B3537,
	MediaPlayer_GetPlatformFilePath_m1D468EF33A86AB45181043CC2DA0C2B67FBDE93A,
	MediaPlayer_CreatePlatformMediaPlayer_mAB58FF0E922DEAFF7469A110F06F5A73C68C3D63,
	MediaPlayer_ForceWaitForNewFrame_m1046EED13F8662393B894B54CC23A05D78422818,
	MediaPlayer_UpdateAudioFocus_mE87DD4523D129731752A21DC7EA8576D6CF91C6F,
	MediaPlayer_UpdateAudioHeadTransform_m0225CB79EA7CC27C6D1B733131FE02D47C024CFE,
	MediaPlayer_UpdateErrors_m0D505DD36E930DB184DA50E7E33C5C5A0FE04834,
	MediaPlayer_UpdateEvents_mED689D58F23FF7DE4DFC378EE47F794282D250D9,
	MediaPlayer_IsHandleEvent_m726602EE6992313D563E5B2047D101FEC2F891AF,
	MediaPlayer_FireEventIfPossible_m293D957FEFC91283E20C5961163D7EF1D2DE70A6,
	MediaPlayer_CanFireEvent_m83C6F32B773BFBDE243A3A35E408B3ADA19AE1E0,
	MediaPlayer_OnApplicationFocus_m86226190DEB9CB9C8E6129F0C0E68F83A2EE926D,
	MediaPlayer_OnApplicationPause_mF42C50F6DCD8006332C2ED6937769D7230EE2152,
	MediaPlayer_GetDummyCamera_m1ECE958660114FCD2F55A4A438E63A6F704A9DD1,
	MediaPlayer_ExtractFrameCoroutine_mD4F518A5EEB7146A9BA59E65744A9B23BC328326,
	MediaPlayer_ExtractFrameAsync_mCD2524D9B9A46380F41C1C1255DEF63F59355B8D,
	MediaPlayer_ExtractFrame_mF766C48B5BB0B0A19848DFE5B23F57303D2B7D2F,
	MediaPlayer_ExtractFrame_mE9CBD363BE0AA46B764AE61EB8CBDE75FF6ADD38,
	MediaPlayer__ctor_m7AFCA18804DA72195D0AFF38BFE22EE771016DF7,
	MediaPlayer__cctor_mECCF8FF4371383413465A3EA0FECCF53DBA2E4B1,
	Setup__ctor_m8D471ACF422DEC1844B08DE6298E7EF61F6E9241,
	PlatformOptions_IsModified_m9329B32428B8862D0994558AF2B7C086A9DA8651,
	PlatformOptions_GetKeyServerURL_mE036407B5A12BE2646A522D99A703389C89C44AE,
	PlatformOptions_GetKeyServerAuthToken_m5889D3593EEEC1FCC11C2631A9596F009DAFD887,
	PlatformOptions_GetDecryptionKey_mB1D0BF403F838B666D818FA8D5164948838A880D,
	PlatformOptions_StringAsJsonString_m7A9A91C0D27FFA0DAFB694F2BFAB563817DDDD1A,
	PlatformOptions_ParseJsonHTTPHeadersIntoHTTPHeaderList_m0C5A914EF74EA3DB7ABCEB1FF75F5B782E38E59D,
	PlatformOptions__ctor_m0765FB48C8311B03E858B0E7ACB26870CA7E1FD5,
	HTTPHeader__ctor_mBD26E64BA6EB557D29C8E5E346802BD3BF6E199E,
	OptionsWindows_IsModified_m3D5B03949B095E226B87FDF5C679D1B12AC97950,
	OptionsWindows__ctor_m74A15D2B8EA4695A1B55EC0A9EE37B7447672251,
	OptionsApple_GetHTTPHeadersAsJSON_m6ED8E32FCAB2E1C13FF1EFD6FBA2F51B97E191EE,
	OptionsApple_IsModified_m97B8AED361C97F06BDA0D49A52D0019DD5BC53D5,
	OptionsApple_GetKeyServerURL_m0F7BC1C13BDD299CE027EAD0F0205E4EDBE82D46,
	OptionsApple_GetKeyServerAuthToken_m75A801C049884609FBDDE0A31EE4C645D2469384,
	OptionsApple_GetDecryptionKey_m235356057A28338A12FCFCBAE5168FDC352C4510,
	OptionsApple_OnBeforeSerialize_mEC86ADC5FE27FE8262188BBDA14549197D892528,
	OptionsApple_OnAfterDeserialize_mE8385FA36B16F9CD6712D898349E1D31E9FF22AF,
	OptionsApple__ctor_m2E26273BAAC19F3078E37CB3B52FC00BBB5EA0CC,
	OptionsMacOSX__ctor_m86B15B9DB09B66C526637AF36DF9DB4B0A6AFB9B,
	OptionsIOS_IsModified_m2BCBA6E16A9330881C279C03219FE45E5B851A8D,
	OptionsIOS__ctor_m71CBF1B11498CE1D3234585F5E17D693B145E506,
	OptionsTVOS__ctor_m94BC448264C97F1BF954AA3B81474CD21B946D7A,
	OptionsAndroid_GetHTTPHeadersAsJSON_m9E64F1365E2533A0453989BFDAFE29BB83FC727D,
	OptionsAndroid_IsModified_m0F1E3B1CB75E5A9869D889293636CA2939D0C0B3,
	OptionsAndroid_OnBeforeSerialize_m35E7F079E61905E64DE47F08350B60ABED4B6113,
	OptionsAndroid_OnAfterDeserialize_m7AA8F7C03489DC53548184AD191C47F864BAF649,
	OptionsAndroid__ctor_m186BC313EEA6B49A9CF88DD62AF031FC2E992545,
	OptionsWindowsPhone_IsModified_m9F12B86CC99A844734D7EB5CABB84ABB36AD5762,
	OptionsWindowsPhone__ctor_mAAD92EF239A0FFDA5EF025204F7DEEBE47A9227C,
	OptionsWindowsUWP_IsModified_m692328A5F9DE126E140EEA459B48E17BEFF762E7,
	OptionsWindowsUWP__ctor_m8D80FBF23ADE92ACC0A48F4A065B3C5980EEAEA0,
	OptionsWebGL_IsModified_m582458478E2767579052067E272E029FFF4435AD,
	OptionsWebGL__ctor_mED1A980016C73E0A2B17B9B3C0C6C6CD21ADD66F,
	OptionsPS4__ctor_mF620860B6E7CA8A6C5699D098C5CC345D0ADC151,
	ProcessExtractedFrame__ctor_m2D45C0063D44914869401FA6A6EEBAE26CC5753A,
	ProcessExtractedFrame_Invoke_m93D4170ADCD901788FA035AD906788CD07C8ED5F,
	ProcessExtractedFrame_BeginInvoke_mD54E1E8BC0E9A671D24705DC6221FB4D8B284C2D,
	ProcessExtractedFrame_EndInvoke_m95A67F49C84E75C01BDD7931DFA69D12ADEC60C2,
	U3CLoadSubtitlesCoroutineU3Ed__166__ctor_mAF47DBCEE88F6F816BE1F9BA4F33F3BAA344957E,
	U3CLoadSubtitlesCoroutineU3Ed__166_System_IDisposable_Dispose_m7A3DE1124596B52D5185CEE3AEE6322EA3FBF978,
	U3CLoadSubtitlesCoroutineU3Ed__166_MoveNext_m4517ABFF4B0805678FDC2DC2181EFBF10A2C5E7E,
	U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC6D7BB5CC4CDCB04948293FC163F9E337D46A775,
	U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_IEnumerator_Reset_m625A44CCFB701C03434E399A2A8AFD83D81C3628,
	U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_IEnumerator_get_Current_m5653252B6D3E7BDF005249225ABEEB9B867D1C55,
	U3CFinalRenderCaptureU3Ed__188__ctor_m498CF3113FF234A782856D3BE00DED342F375BEC,
	U3CFinalRenderCaptureU3Ed__188_System_IDisposable_Dispose_m8A0688DA71BC5C7ED01C0415FD9CDA8D2B52D5F4,
	U3CFinalRenderCaptureU3Ed__188_MoveNext_m3D0C170BAF177C94DFF4DBA6DEA23C3B8B58F119,
	U3CFinalRenderCaptureU3Ed__188_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF833D26918BB2E538C49FF8C63CE97F8CBCC3EE,
	U3CFinalRenderCaptureU3Ed__188_System_Collections_IEnumerator_Reset_m22429013FDB26856E4D1FD9EF1749DD600BF547C,
	U3CFinalRenderCaptureU3Ed__188_System_Collections_IEnumerator_get_Current_m021BA9DE6276EB6F40BC8FFA6746895F17044C30,
	U3CExtractFrameCoroutineU3Ed__209__ctor_m42A11F4ACB5EABD7F3B2FF42F17377F8988B55F0,
	U3CExtractFrameCoroutineU3Ed__209_System_IDisposable_Dispose_m2813638F893988A28291B082C41B4C2CCFA5E316,
	U3CExtractFrameCoroutineU3Ed__209_MoveNext_mCF2B7B2C334A8068ADCA0AA5DD16D8DA1E0A6F75,
	U3CExtractFrameCoroutineU3Ed__209_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EB04B181B3AF0D8F31D45DB7FE4073A601B9FD7,
	U3CExtractFrameCoroutineU3Ed__209_System_Collections_IEnumerator_Reset_m03E84C88DE36A133F5C616284E160B269D57B779,
	U3CExtractFrameCoroutineU3Ed__209_System_Collections_IEnumerator_get_Current_mC4C324C31F1A54748026CBCAA87DC42C1925C5A7,
	MediaPlaylist_get_Items_mC8072512F2147BB99CE39BC0C1078E028502B6B1,
	MediaPlaylist_HasItemAt_m1105AAC4E62308C4458754326FAD9CC16164FE35,
	MediaPlaylist__ctor_m4CC954DBB400E5D53DAD84FFE2EABD297CCEF4E1,
	MediaItem__ctor_m73DE5A23DA10B87FEEF58F5D2586BB873842F64C,
	PlaylistMediaPlayer_get_CurrentPlayer_m1751355EE254A83806EE901FFA6449B6D08352A6,
	PlaylistMediaPlayer_get_NextPlayer_mA89F35DE1AC4379084FE0D4D4D3EA95D14951DDC,
	PlaylistMediaPlayer_get_Playlist_m240FC5177F46FC8261C853A3FA27A402A360310A,
	PlaylistMediaPlayer_get_PlaylistIndex_m20A06F17E58AA9D87FFD9C3BEC9077B21539437F,
	PlaylistMediaPlayer_get_PlaylistItem_mA4E871ADF14640FDEAC5316DCBC3A2ED245743ED,
	PlaylistMediaPlayer_get_LoopMode_mD9CFB9BC55B5EE17B230C9AF4C96A39B7E3E8137,
	PlaylistMediaPlayer_set_LoopMode_mBA3D3DF7E85A00349B1E99F7660C0A8967761FC3,
	PlaylistMediaPlayer_get_AutoProgress_mBFFB849689D77DE5405741D9329B327275DC20DF,
	PlaylistMediaPlayer_set_AutoProgress_mFCD8BE1C0679F0718235963464B1BFD76CA03FAD,
	PlaylistMediaPlayer_get_Info_m91BC4D0078EB0CC1D274F80FB813130286DDDF76,
	PlaylistMediaPlayer_get_Control_m2D6BE399D5F6F2C960BA89DCDF8BBE4F7B4FA8B2,
	PlaylistMediaPlayer_get_TextureProducer_m1A4A252AA9F073469C8B561682FF6AD5AC642B3E,
	PlaylistMediaPlayer_SwapPlayers_m895D87766AAC9EE9DD79A3ED6B88CA08CEE6D24F,
	PlaylistMediaPlayer_GetCurrentTexture_mBA3E6A0C5EA4FC014CE70F20ED89350EDA31A760,
	PlaylistMediaPlayer_GetNextTexture_mBE518E44FB19D787FC6E6E9D37ABFFA1BAF7E3F5,
	PlaylistMediaPlayer_Awake_m0B04AAD55AA08635C10B7A5DB5B007C1FBC2CB33,
	PlaylistMediaPlayer_OnDestroy_mDC144076293DE79A7A6952C0D6947F60301C08B7,
	PlaylistMediaPlayer_Start_m97424C5D29DEBD4074D78B21342765B5680CC154,
	PlaylistMediaPlayer_OnVideoEvent_mE307B1229E23A62D6DF4B84FAEFC3BB4BA40FC4A,
	PlaylistMediaPlayer_PrevItem_mAE4FBCFC966A108A16555C8E453241DC05242480,
	PlaylistMediaPlayer_NextItem_m86F07BE1F10805326B8689891DD8B1EAA7B97F7C,
	PlaylistMediaPlayer_CanJumpToItem_m6FA442232726937A3B3C896EF98B34A4B6AC1C28,
	PlaylistMediaPlayer_JumpToItem_m736307ADA3E4272E8DEE0E5943839BE53D0DE428,
	PlaylistMediaPlayer_OpenVideoFile_mF9A2B693202B8CFBCA3E30F29D20BDFDA17ED24D,
	PlaylistMediaPlayer_IsTransitioning_mAAB05AF108E1D397A74983196FDE9AA144EA1D75,
	PlaylistMediaPlayer_SetTransition_mC9E8F83C2938901C03638117EEA37402CF9A484B,
	PlaylistMediaPlayer_Update_mF64D00943BB14A6DCE979652CC3C98BB9BA38970,
	PlaylistMediaPlayer_GetTexture_m1BB29461D42B3D57174CA54AF34CD77F08D40970,
	PlaylistMediaPlayer_GetTextureCount_m64439E3889377765B5D6D8E9922D2E3143C9111F,
	PlaylistMediaPlayer_GetTextureFrameCount_m2DEB36885CFD829DB790F45E19657A22DEEAADE5,
	PlaylistMediaPlayer_SupportsTextureFrameCount_m74B1AB14B4679209F4E78430D6E1E4653F6B1FE0,
	PlaylistMediaPlayer_GetTextureTimeStamp_m64EB4837340D40E20769BDBAA85DCF3A99AC4289,
	PlaylistMediaPlayer_RequiresVerticalFlip_mA3CE7E8CEDF0A6AAF512710BB9900ADAC3454467,
	PlaylistMediaPlayer_GetYpCbCrTransform_m28579BE4D3C39DB1F57A0A2D1BB806AFB67A6396,
	PlaylistMediaPlayer_GetTransitionName_mF0B1C3DF7A9BD9B5FE388D6EEE22F3F378406AC9,
	PlaylistMediaPlayer__ctor_m766DE61F2610CBFA8C3F71C75DC69FFAEAECE2BC,
	Easing_GetFunction_m6BF9C691739142093824E65BF9D0B3959716E3B5,
	Easing_PowerEaseIn_m452740837B6A23AB4DA085C09A56239B2D61E9A3,
	Easing_PowerEaseOut_m9029AE6D2160B8497333B9D381DDA8B364311565,
	Easing_PowerEaseInOut_m0C91FD6F72C67D6C21A0BAC622714175010F60C8,
	Easing_Step_m74FF1FA20E4A7FE4232FBD5B0EFBEA036171605C,
	Easing_Linear_mA407428D581C8BA5C60991186DE52D521D8EEC5B,
	Easing_InQuad_m835BD34EF6B489D51D8B7E31D5421BC89DF65998,
	Easing_OutQuad_m810849EFC4CDD6477028DA82FB5006E3428407DD,
	Easing_InOutQuad_mD67729B41C8D095A565A1AEDA43FE0FF363E303B,
	Easing_InCubic_m0F57ABFDA1DAF00A462FC55900C346C7B4308B08,
	Easing_OutCubic_m2693C08EED8C8A119C6362B759A27AABFE9C32FF,
	Easing_InOutCubic_m2C0042F5722F61B0CF3B2C63DDADC2C045F41523,
	Easing_InQuart_m9C1DD3B8219F88569905FB1B38B88E042A0D3245,
	Easing_OutQuart_m494232308BA295350A858BE1E2FA475AC3A85FFF,
	Easing_InOutQuart_m2A57949CDE900057F1F9D17C482242DCB35AE50A,
	Easing_InQuint_m59758F196F7FFE458A32F6F14B5429946A6FB4BC,
	Easing_OutQuint_m0494D176295021B3512BAB04407A172F302A322E,
	Easing_InOutQuint_mA57462E560637C403AD73D55CF03A4ECD29F2C51,
	Easing_InExpo_m611AB71C3B0E391EE9824BFB4271CABA990D2AFE,
	Easing_OutExpo_mA71C6B4FE36A0F88D3FD06106B0BE7F3E24B8356,
	Easing_InOutExpo_mD33AD178CF4F37856E199F4247D55D1B6C904F36,
	Easing__ctor_m6AAB60805D5F0E34EA019B566FE2E5092839629B,
	StreamParserEvent__ctor_m10D8AECEF91142015F01F1AD53FB6620076ABEEB,
	StreamParser_get_Events_mF85B81A4D0100E1F93469081B01C0DBF08BA5D71,
	StreamParser_LoadFile_m80B6CF097D6F5AB208AE211B53889DDCBB9E7D29,
	StreamParser_get_Loaded_mB6EE8D6717C8F31B7EB9C49B7705AAA2F8F126DB,
	StreamParser_get_Root_mDB49D875218539FBF4D3D0CF06B0E90E8DF23632,
	StreamParser_get_SubStreams_m0DAF07B3B6AAF909DEC0364E5BBD8890E126463F,
	StreamParser_get_Chunks_mB511155C7EBC54307595D5E96DBBEEDB98823F2C,
	StreamParser_ParseStream_m60F0235E79C98DB722D6B66D2B22DF80B6B7CD68,
	StreamParser_Start_mE229B98CDA5625FEF2C2A2D15B031733959DFE63,
	StreamParser__ctor_m909BFDB96C07C73380E69E633D9BFC5231CAECA1,
	SubtitlesUGUI_Start_mC041EE4B381BF8033D645A45F984A4D7DB0DA7AB,
	SubtitlesUGUI_OnDestroy_m0CE8B38E786461A415B46B005667A2AAB3C4C76E,
	SubtitlesUGUI_ChangeMediaPlayer_mB75F9DA4FEE5F0FB014C48D8AF7AB99C6E96CBA3,
	SubtitlesUGUI_OnMediaPlayerEvent_m0889CE3960F338D3C52F9BC57154AD81309FE4FD,
	SubtitlesUGUI__ctor_m385A64B013A82341D0C1AFEF543AA6EF0030BD68,
	UpdateStereoMaterial_get_ForceEyeMode_mAA88FF989F0306A4DB3CAD03C01C4FCEAAFBE418,
	UpdateStereoMaterial_set_ForceEyeMode_m639E4719904B8B45B6E4526D22828C13C19AB2F1,
	UpdateStereoMaterial_Awake_mA3C7B3BB2DBFAE0AAC82CDFEF23C376815B89C86,
	UpdateStereoMaterial_SetupMaterial_mB987CF198D1E92D591D042B972B70FF8014D0D86,
	UpdateStereoMaterial_LateUpdate_mAE667AD7A7D03A816D7587857476225A3452C917,
	UpdateStereoMaterial__ctor_m079674C3F2EB6A8C1E30F41DA6C7882FFC4F156D,
	AndroidMediaPlayer_InitialisePlatform_m480201A9079FA34579B451A400CCC8EE2AD0A46B,
	AndroidMediaPlayer_DeinitPlatform_m543239108E6CF61381C9A9F232191BB8BA1AE6C7,
	AndroidMediaPlayer_IssuePluginEvent_m3E2D662F77DB83B324DE913AAD729E3440DB1933,
	AndroidMediaPlayer_GetMethod_mB9421A67B0C015294A2243755869A7FD6D2684DD,
	AndroidMediaPlayer__ctor_m1B15AD791EDF44D15962EA6E6B7A700B19AEC7AC,
	AndroidMediaPlayer_SetOptions_mD5FA3F77CE556E5DCB766ECD3E98153B1A3A13CF,
	AndroidMediaPlayer_GetEstimatedTotalBandwidthUsed_mD0670C9C445A414188096BAE985C2A13BAA10A79,
	AndroidMediaPlayer_GetVersion_mBEEF6D463C589B2D77E2DC426B086FB0E0EBB767,
	AndroidMediaPlayer_OpenVideoFromFile_m2AE46CD4B2FD52910F379AB05D4132016026668A,
	AndroidMediaPlayer_DisplayLoadFailureSuggestion_m20B6B867CC50C83B70DC675FF0685E3ADF20FF69,
	AndroidMediaPlayer_GetSeekableTimeRanges_mE768FA5824407CF368593A784AD996977E14D67E,
	AndroidMediaPlayer_CloseVideo_m6E563DC56D2E42E28783C06BA460F991DADD2C36,
	AndroidMediaPlayer_SetLooping_m39379D67BC8CB4B5F9EC20125BB1E41463BA33FC,
	AndroidMediaPlayer_IsLooping_m919DC1146B5D67F3BB83B9E9F4A2B845CD04DE32,
	AndroidMediaPlayer_HasVideo_mF835E7400852494346995C6F047963CC02DE1B0A,
	AndroidMediaPlayer_HasAudio_mFC08845B8D5E21CEE01971D2F231342C013890CE,
	AndroidMediaPlayer_HasMetaData_m17372F23398AC08EAC9038C9BFAF701D2CCB8D1D,
	AndroidMediaPlayer_CanPlay_mB89A45F797154675CED208F5ADA838B61CDAE3FA,
	AndroidMediaPlayer_Play_mF361FB6C44A9B93788BC88CA17A5CC5BF19B9C9E,
	AndroidMediaPlayer_Pause_m06843D7F1A991D6B8CC54EADAC5212A728E84C5D,
	AndroidMediaPlayer_Stop_m3C526DF8017006EB7141BC93CCD86C27A8938059,
	AndroidMediaPlayer_Seek_mD1F0B5A7FCF0D9D15A88C3AAF30750C6605B6C63,
	AndroidMediaPlayer_SeekFast_mC0B7E9D82491E726D96FBB0BF27FD57EA4D55700,
	AndroidMediaPlayer_GetCurrentTimeMs_m0E09C8DB596112B159C5DE432A1A297AC15AC010,
	AndroidMediaPlayer_SetPlaybackRate_m9C7A15A3066C532E4C35BF3C45386AA02C9536A2,
	AndroidMediaPlayer_GetPlaybackRate_m1E21FC984F8C8907D65594EF97AAA0E0578E3701,
	AndroidMediaPlayer_SetAudioHeadRotation_m9FB361D7EF747188EE5DD1122A8C127CB6F9ABEA,
	AndroidMediaPlayer_ResetAudioHeadRotation_m9E44B2552B23396A9561931320C4EFDCC731CCD6,
	AndroidMediaPlayer_SetAudioFocusEnabled_m458407BAA9A828D19B84F7A680CE5FDFEB67294F,
	AndroidMediaPlayer_SetAudioFocusProperties_mA38D484ED73825A02BB1B77486A0D14E7E2DC55F,
	AndroidMediaPlayer_SetAudioFocusRotation_m601BA3B6BED1822A3A359BC64D060B1C4A91DAD5,
	AndroidMediaPlayer_ResetAudioFocus_m69D72B8AA10F623618362399DF3BF22690472FDF,
	AndroidMediaPlayer_GetDurationMs_mCCCA1447E5327A482E743C4C09FDAE6E6EA0B983,
	AndroidMediaPlayer_GetVideoWidth_m16FE92F5869CEE50248DD6E4275958F281A780AE,
	AndroidMediaPlayer_GetVideoHeight_mC4692049F412A9AD40C1B48C7D9E661470910F6A,
	AndroidMediaPlayer_GetVideoFrameRate_mA568FC635EB0F98B056B6B849530F96CC0052178,
	AndroidMediaPlayer_GetBufferingProgress_m6FCA8697E4FF968F65C10C02CC7BAC104537812A,
	AndroidMediaPlayer_GetVideoDisplayRate_m3490C43F697872564761EB7551A39055939D58F9,
	AndroidMediaPlayer_IsSeeking_m82478BFB427140B55F97E645A4909EB50DE24507,
	AndroidMediaPlayer_IsPlaying_mCCDF763B05413E25A2EF1C3E06F1BECEBD2F5C32,
	AndroidMediaPlayer_IsPaused_m2E8BA6090923F0FA1599B41D4CEE8BAD9A18E1A6,
	AndroidMediaPlayer_IsFinished_m8359DAECFB7B45EE771A513726263E8305660701,
	AndroidMediaPlayer_IsBuffering_m338F6D018438F07A428E180AE14F3788F9331B87,
	AndroidMediaPlayer_GetTexture_m88EFE1420352CD43E68466BC0E871F4CBFB624F6,
	AndroidMediaPlayer_GetTextureFrameCount_m2DE2A07B7C6312BCAB7EB9A9E9612761A55697F7,
	AndroidMediaPlayer_RequiresVerticalFlip_mC137D1CF8B11BB589DB78AD45D14FA5B5E33FB27,
	AndroidMediaPlayer_MuteAudio_m16E42D79701DEA22B7618858924154D6C29E0ECB,
	AndroidMediaPlayer_IsMuted_mFC0B6D41E4B76B944055D76E6BC1EA53CCCDE5F1,
	AndroidMediaPlayer_SetVolume_m56A858CAF5BFB63DEE97BFEDFFBBB61DBF3DD292,
	AndroidMediaPlayer_GetVolume_m672CDE50510B244D00380D8319C9B37377264D07,
	AndroidMediaPlayer_SetBalance_m82507EB2574C17FD1BCE36315247615678EBDA8F,
	AndroidMediaPlayer_GetBalance_mE44177BDF3589C3C3781F0271D06660EEA89F58F,
	AndroidMediaPlayer_GetAudioTrackCount_m8BAE816BAD45339E79788D9789DE1E4C01671934,
	AndroidMediaPlayer_GetCurrentAudioTrack_mD5F02E3C3EE5D6386A614E5F9DB214D28FE9AC09,
	AndroidMediaPlayer_SetAudioTrack_mED4B457A3336003AF32C627BDFD0D63DB46CB4A3,
	AndroidMediaPlayer_GetCurrentAudioTrackId_m19E248BD4EAE9EC25B12622A781C49D21A3347FC,
	AndroidMediaPlayer_GetCurrentAudioTrackBitrate_mEF4835D92452A69B93C53E5ABC4194ED19CFAA87,
	AndroidMediaPlayer_GetVideoTrackCount_m343670506A273A19D7041ACA203BB771FF1398BD,
	AndroidMediaPlayer_GetCurrentVideoTrack_m2D18EEC7AB27ADE8DD8E6D696EA3AA40BE0FA212,
	AndroidMediaPlayer_SetVideoTrack_mBD6EA9410456C268ABC2F6887E338A558D49DC5E,
	AndroidMediaPlayer_GetCurrentVideoTrackId_m42087352ED729D988BA1647A92159C11C66E46F6,
	AndroidMediaPlayer_GetCurrentVideoTrackBitrate_mCC362CFB895705BF8DD9499896C231830923DC92,
	AndroidMediaPlayer_WaitForNextFrame_m74257982CD48A21241DF33116E7B5763319F3EF5,
	AndroidMediaPlayer_GetTextureTimeStamp_m0F46582DD8F72AF16C446D257AB2BF0438B2C8F6,
	AndroidMediaPlayer_Render_m7D1D31EB1D47F7C3F364B1A1505FCD6BA4B49A8E,
	AndroidMediaPlayer_ApplyTextureProperties_mCA2AFCDF2E2DBE88B5AF1231D83D9584436CC23A,
	AndroidMediaPlayer_OnEnable_m0669084265C2E598C6783B37E02843CDA9645487,
	AndroidMediaPlayer_GetCurrentDateTimeSecondsSince1970_m1996DEA0135A2B616FF955F17E47EE7F6521F6AD,
	AndroidMediaPlayer_Update_mAC17B1D28433B63126C6AC5443715190C94ACB59,
	AndroidMediaPlayer_PlayerSupportsLinearColorSpace_mC51B9420468C3D9D2057F6AD686893F20B076E56,
	AndroidMediaPlayer_GetTextureTransform_m5E3801E6B0309F8304C546CA5470E529552892DB,
	AndroidMediaPlayer_Dispose_m5D885EAFC86ED5B4342130B07F10E4DAE88B1B8B,
	AndroidMediaPlayer__cctor_m5A9C337DD25CA992A66E7F7323A58D60319AB992,
	Native_GetRenderEventFunc_m8754CC53B2673E22F28728FA5751AE43B8D59288,
	Native__GetWidth_m9E38517D03739EA72116C26F9827A14695F8985B,
	Native__GetHeight_m673F6642C668BEAD1B496AE131F9DDEB3B3D917A,
	Native__GetTextureHandle_m2ED72AAF09B6E50426219C52804B1797DF195407,
	Native__GetDuration_m13527EA823F50088CFD4894B1397BF81AB4D46FC,
	Native__GetLastErrorCode_mBB2A86F4C659D10A66A21B1380E862D77B55F3A9,
	Native__GetFrameCount_m6BDCABAF44BE8EA126AFB183F61E614486B981B7,
	Native__GetVideoDisplayRate_mD91DA764D68277343C42FB3E237382A6B423451E,
	Native__CanPlay_mD6AAADE54253E1AA636C1D12466BC9A8118E5500,
	AudioOutputManager_get_Instance_m0FA3BF968DEFAABF81F0320CF0414CF046256E52,
	AudioOutputManager__ctor_mCE33BCA911A8FC47B76DD802B62E515704DD28D9,
	AudioOutputManager_RequestAudio_m3857D9FE727B0A158F9C14D8D5B3007A82CE3EE3,
	AudioOutputManager_GrabAudio_mC8F779789094C4AB7DE1F4184DC67CF832437626,
	AudioOutputManager__cctor_m93A8EBDA4878BBF3BAE16216FF077C08E146A9F2,
	NULL,
	NULL,
	BaseMediaPlayer_OpenVideoFromBuffer_m134E4F1C8165FA91E817C96FBF308309835CF778,
	BaseMediaPlayer_StartOpenVideoFromBuffer_m4C94D7A26125607E3E09ABA045A81856B7810DDC,
	BaseMediaPlayer_AddChunkToVideoBuffer_m602ACCC42A26A01A4D0FA2DBBBE17CE14B60623A,
	BaseMediaPlayer_EndOpenVideoFromBuffer_m15070526688817C8F1B9FCA520F2B717E1EE4778,
	BaseMediaPlayer_CloseVideo_m373660E73B61E71573599E243369E2644FAFBB7E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseMediaPlayer_Rewind_m4BAB9CB48D97823AEAB7C080BCC25B6E1D740CE1,
	NULL,
	NULL,
	BaseMediaPlayer_SeekWithTolerance_mEC503F8FEC5A80F477E3575FC672BD8ED0490887,
	NULL,
	BaseMediaPlayer_GetCurrentDateTimeSecondsSince1970_m2AE51645D964D68D961DFFC6FA58CAEAC58164FC,
	BaseMediaPlayer_GetSeekableTimeRanges_m08DDB97A81459F64B413B522C83AC8C3FF2F15AB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseMediaPlayer_GetCropRect_m707AC32D1019217E87B59075DFCF0DC20B50629C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseMediaPlayer_WaitForNextFrame_m175788A07BC1B028ED46DA90CEA3FD660D93F5D5,
	BaseMediaPlayer_SetPlayWithoutBuffering_mDAD0D181E0E6E2608A607A4DFD6353AD45A7DDB4,
	BaseMediaPlayer_SetKeyServerURL_mE4AA76F9CF0A2A5F309AABDAB03E1FB594D270AB,
	BaseMediaPlayer_SetKeyServerAuthToken_m07C8376EDA9B10AE8A83027026C84AA9012D01E7,
	BaseMediaPlayer_SetDecryptionKeyBase64_m3B93AC51A6121F98C2DE0BCA3F33C84B4E2FDC89,
	BaseMediaPlayer_SetDecryptionKey_m746B002F4B8CCDAAFD62E07F2F1387A7D77C86B5,
	BaseMediaPlayer_IsExternalPlaybackSupported_mFBF93ED5DFB83E439C71D5B4BFEA49E96261468E,
	BaseMediaPlayer_IsExternalPlaybackActive_m21F5436F2AB3B67A7FA552F8491800CF827CE7AE,
	BaseMediaPlayer_SetAllowsExternalPlayback_m8631EF8181635E2922D869AF101C7B47B7D3ED03,
	BaseMediaPlayer_SetExternalPlaybackFillMode_m5FEBB0CD331B8B72DB72B456EA97C95893956DE7,
	BaseMediaPlayer_GetTextureCount_m0445A325571CC9BF9E137116EB8DC7FB266456AD,
	NULL,
	NULL,
	BaseMediaPlayer_SupportsTextureFrameCount_m15822F5CB71CBA1C8E9A3BF8AE3667669DDB8D01,
	BaseMediaPlayer_GetTextureTimeStamp_m84E4AD62112F198FCD4323F72BF9C3C452670D33,
	NULL,
	BaseMediaPlayer_GetTextureTransform_m98D9246501ECC4E8DD87CC6AA859744822D85595,
	BaseMediaPlayer_GetYpCbCrTransform_mD00E4468DD772DCDCE9F6212A6E91F67E5BDB32B,
	NULL,
	NULL,
	NULL,
	BaseMediaPlayer_SetBalance_mE7328C33DDC2481A410B7246F376E78FDA63B095,
	NULL,
	BaseMediaPlayer_GetBalance_mA00225EC805B19E2F047EC08F86DDB66713507AD,
	NULL,
	BaseMediaPlayer_GetAudioTrackId_mA09717258EF38368BCDECE30E869E6FEDF53E7BB,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseMediaPlayer_GetNumAudioChannels_m05C04C2C20BA4DDECD348FBEF310E6F9D02A6A93,
	BaseMediaPlayer_SetAudioHeadRotation_m0FDB6CCA5645F168962610AAA27C7090FBA4B9AA,
	BaseMediaPlayer_ResetAudioHeadRotation_mF18B61A31B1258F93440706C215DD9C39AFD4959,
	BaseMediaPlayer_SetAudioChannelMode_m64D15EDD5249F88EEA97A238599BA58F5AF5DFCB,
	BaseMediaPlayer_SetAudioFocusEnabled_mE058263D9542C3049C1724662A933C962BEC2184,
	BaseMediaPlayer_SetAudioFocusProperties_m37793B9D6A5AB9DA90758ECBD41955FF19136425,
	BaseMediaPlayer_SetAudioFocusRotation_m5EF05840EF3F89744DD8530A77AA9FDB758A55DF,
	BaseMediaPlayer_ResetAudioFocus_m078F6C60AF26359CBA240D8E7D1B29BC7D33FC08,
	NULL,
	BaseMediaPlayer_GetVideoTrackId_m56A282DE65FC35EB45F4D2A8BF023704CD07E08E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseMediaPlayer_GetEstimatedTotalBandwidthUsed_mABDDF3B4A4C4C13DBF0B21A7F4D3355F5EE42378,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseMediaPlayer_GetLastError_m27835B8FE01C2BD4A3A11F3E19954C6CCE1F9F49,
	BaseMediaPlayer_GetLastExtendedErrorCode_m9CB3BEEEF755DD537924A8AF3C65A66C2960D0A9,
	BaseMediaPlayer_GetPlayerDescription_mAC3B33841136795CBCB2392129408A3BE3E23DD1,
	BaseMediaPlayer_PlayerSupportsLinearColorSpace_mEE3FA26619E524C0957C4EF8F4929E2FDBFC55EB,
	BaseMediaPlayer_GetBufferedTimeRangeCount_m9C7B5DAD54EAB1715C132CAFA47B49FFA25911C7,
	BaseMediaPlayer_GetBufferedTimeRange_m91FC04998E849840BFD38B9C29CEEC454048898B,
	BaseMediaPlayer_SetTextureProperties_m56C85F5335096B90788F5254CDD8152D55C61958,
	BaseMediaPlayer_ApplyTextureProperties_m1246CB9C705FD6503D2A83AC72F0EC958BDE8FFB,
	BaseMediaPlayer_GrabAudio_mF1226A61BCA9163DF537217992D9021F08E77D50,
	BaseMediaPlayer_IsExpectingNewVideoFrame_m324814339D1C4706D661DB3F1FAE9BB3471B73ED,
	BaseMediaPlayer_IsPlaybackStalled_m7B55CC9A93E63AB0B8264E9D020E1127E9A61430,
	BaseMediaPlayer_LoadSubtitlesSRT_m16BFC16E815DA0EE34F7BFE945ECAC8D58984CAF,
	BaseMediaPlayer_UpdateSubtitles_m252AA6D660B684D7BA29EDAA192AD1CA2A2A1060,
	BaseMediaPlayer_GetSubtitleIndex_mAD1280C3023C0BFB1E6BBD429DA82FD2DABE0ABA,
	BaseMediaPlayer_GetSubtitleText_mF785A8FB753F87861E8B49E9D92542B2B62B97AF,
	BaseMediaPlayer_OnEnable_mB1C7A49A7094946EC40A0C87787370B23FFBD8F3,
	BaseMediaPlayer__ctor_m78CFF1B03053002A3BC81ED985D97B2C6620DF6F,
	HLSStream_get_Width_m16F540A61C08618F2E304A2D3A472EA3D389A93E,
	HLSStream_get_Height_m05E464AD1D98CC86B4B1BBAA5156F0FCF3863B00,
	HLSStream_get_Bandwidth_mC92C78A84A647F52BE28598637440EFB9791B9F1,
	HLSStream_get_URL_m0A2F7A7BD8371A523AC0383B8945DD09A06D57A9,
	HLSStream_GetAllChunks_m396DFE284171978B993139E5A214144BBC2CF8DF,
	HLSStream_GetChunks_m76E7020A3D3E1BE6B00CBA7E0ACB7A12147C765B,
	HLSStream_GetAllStreams_m998B4F10CA609027B6C1EF3A4982E74A640C8FE0,
	HLSStream_GetStreams_m812E329BA7D9359C66C283A016B88FBCCD1A98B9,
	HLSStream_ExtractStreamInfo_m5ED2D89F6D261EB618E5C59CC4DBAC5A0E6DFB99,
	HLSStream_IsChunk_m6D74B3C9DB8CEE57ACE3824B158155072E7E22CC,
	HLSStream_ParseFile_m3A9E7F907CEBC0A9753A08AC1494498140259836,
	HLSStream__ctor_m08A3FF8331FD0168BB333295FBEE28FA8D58037D,
	HLSStream_MyRemoteCertificateValidationCallback_m64548F918CD1567D632FDBE5C5B55DB1A762D8E6,
	MediaPlayerEvent_HasListeners_mFF8DA58E4E6A89CB253CCD70011ED15F0F9B2A3E,
	MediaPlayerEvent_AddListener_m9439DDAFF5E3D1DF3772724199060F845ACCAD76,
	MediaPlayerEvent_RemoveListener_m75D2C5FE8969BC958EAB4F6BFFF13C1AA2929CB9,
	MediaPlayerEvent_RemoveAllListeners_mE6BFC8409069B9F01BC092F141DC626BAC18A5A4,
	MediaPlayerEvent__ctor_m6DE59D660F6A26BB62D04A29DDECE0868FD8367E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Subtitle_IsBefore_mEB6A9661105B086871CA8892E2946E932AC3EB8A,
	Subtitle_IsTime_m3D2E48B7176629A4FF0E078F374C0F17CB710867,
	Subtitle__ctor_mE3CE9C6801FDCF8DB4808F367CF21B5F1D0F4892,
	Helper_GetName_mF480B4F71E50B7139C818AA117F8F902DD1860CF,
	Helper_GetErrorMessage_m4C1C40C98BF5FB37C5EFB47DA99091FB45CC7ECC,
	Helper_GetPlatformNames_mB9EF4BB7B890FAA59C305A276A296294E75DD7B6,
	Helper_LogInfo_mF7C65B5BD7AE1E6F4DE4D9869F9F53F4A07FA8B4,
	Helper_GetTimeString_m6D36005C6785C5FF84AF046BBBDF4F9D3C20348D,
	Helper_GetOrientation_mE372CD72592447C80D008B0FBF653ECC87FB7627,
	Helper_GetMatrixForOrientation_mF32F5F6FCA1096DEC54DF70BBB009337D446C347,
	Helper_SetupStereoEyeModeMaterial_mAB7BBF51055B0ED8D6FFA203F9B4D3C9657EE7B6,
	Helper_SetupLayoutMaterial_m8FF6AE3A80F67BE136F3E897B3ED40432AF1561E,
	Helper_SetupStereoMaterial_m255072662315CD8120ACD4158AE9FBAFD95572BC,
	Helper_SetupAlphaPackedMaterial_m95EB9F22C50015469A1B534E559D08DA2D1A7731,
	Helper_SetupGammaMaterial_m84DCEAE514041A0DDB9732FA134BDBB11FFEDC81,
	Helper_ConvertTimeSecondsToFrame_mB7F74397C75BC73D0BEF49BCFE0A8AC91B4D5481,
	Helper_ConvertFrameToTimeSeconds_mFA3A16E697B47197B35028CAD6763D6456709D30,
	Helper_FindNextKeyFrameTimeSeconds_m0F9CB57EC2DB2FDA95753C860445F1B4E7E3A303,
	Helper_ConvertSecondsSince1970ToDateTime_mDA52C14BF59C4D4E9CBB2E7A60035AACBC791C13,
	Helper_DrawTexture_m165CB51168074BB81EB25C61A0F6F1C6DFD93469,
	Helper_GetReadableTexture_mF4D3B41AFFF220B865DD21C89F30F920DA919777,
	Helper_ParseTimeToMs_m1C11922E300EF60DC1F4A2363B816E2C3405733D,
	Helper_LoadSubtitlesSRT_m48A32A35BA5B9C1361B13F4D18CF5377AB5809AB,
	NullMediaPlayer_GetVersion_m7C99CA824683CEC53FE8AF2D36FF0A4284ECBBB5,
	NullMediaPlayer_OpenVideoFromFile_m534926435809260C8EAAA704B462FBB14E5E143E,
	NullMediaPlayer_CloseVideo_m98F9C4A0B29978BD67EEAFF5FBA01BFC23A9CCA5,
	NullMediaPlayer_SetLooping_m9FC9EA6D16B86443398BBC53518A4B69C9F24B94,
	NullMediaPlayer_IsLooping_m3F846DAB2AC8DC7FFE87B96C1D94DA18DD6C32D5,
	NullMediaPlayer_HasMetaData_mEB1731555ACD733D078418AB66BCA287A8173A7A,
	NullMediaPlayer_CanPlay_mBC0BA1989FF91ADCE1C283064D0C2802B1C69719,
	NullMediaPlayer_HasAudio_m4A189F945DF02097107E0921C1F5244E706B0014,
	NullMediaPlayer_HasVideo_mFEAF2E0AC280F012BCE8F7F33E24E909CCCB3D93,
	NullMediaPlayer_Play_mD82F4A24CA224CACFA1F6947B0BB300F9EBEA2D5,
	NullMediaPlayer_Pause_mBDAB6D4554622DD80531637A1714FDC164BBF900,
	NullMediaPlayer_Stop_mA15CBEAFB6199C2B4B93B867F738957B179350E3,
	NullMediaPlayer_IsSeeking_m62DEB4CD700CD1146B0742F7E9DBA06B1CAEB063,
	NullMediaPlayer_IsPlaying_mDF0CD38A5AA667CB5E3F6343D1023D0FF24532C6,
	NullMediaPlayer_IsPaused_m42F8DDE720F124B6F3D3BDA2D9345D8D5DF62A1D,
	NullMediaPlayer_IsFinished_m17889412A5FA6C6902031FBE08CE95DA314B003C,
	NullMediaPlayer_IsBuffering_mAED83677C608F001B38D2C953D8B6F2F6F29CF31,
	NullMediaPlayer_GetDurationMs_m5E820AF34F931F3F04673D5927EDF7C8D404FE73,
	NullMediaPlayer_GetVideoWidth_m263369B6E47B8A5E64D2A590F5460817227C96A4,
	NullMediaPlayer_GetVideoHeight_mEE476C17AC2AF67323C782E80655DA023B2F9140,
	NullMediaPlayer_GetVideoDisplayRate_mF2BC451FEB2AB8CF7CE1175385EB1E4B00A3CB4A,
	NullMediaPlayer_GetTexture_m34E6C85782D9839BDB6E6E8F8397389610364219,
	NullMediaPlayer_GetTextureFrameCount_mB96F9D1EEC5C3C34D47FF7934565F66C50F850C0,
	NullMediaPlayer_RequiresVerticalFlip_m55410C146647CF663790A6760EC0D57866733056,
	NullMediaPlayer_Seek_m957E145D3E5BADE75C1F92A4128B273D7C84B9E9,
	NullMediaPlayer_SeekFast_m47B3ED648072D952C1D8B319DC9336440CFD0073,
	NullMediaPlayer_SeekWithTolerance_mF69D2CD514E0E2D30C3D7899B6A718FD423FA238,
	NullMediaPlayer_GetCurrentTimeMs_m617406675A7481B7FE2A2E9E42A0C69B4E34A71A,
	NullMediaPlayer_SetPlaybackRate_m008BA6284CBF3ACD1A3A4A6065ECC5CF2BF92800,
	NullMediaPlayer_GetPlaybackRate_mC46869B3BD974209F002516E769C67535AA7D41D,
	NullMediaPlayer_GetBufferingProgress_m53D138EE5A3C0889B2ADFC42B53B88129154C298,
	NullMediaPlayer_MuteAudio_m4CD4D798B8FD80DB3BAD93D7F394D7D4503C6A35,
	NullMediaPlayer_IsMuted_m3F50A8BC58A4F531D03C10AB9503645DDAAB1FA3,
	NullMediaPlayer_SetVolume_mBC578B78DD441C5062EFE47030821F6B1CB3A116,
	NullMediaPlayer_GetVolume_mF4238FB12F49EFE6F3C67298BD69F2FCA9B61055,
	NullMediaPlayer_GetAudioTrackCount_m6B7B7D53E16FC24A044F3112CEC035D12BF0AE22,
	NullMediaPlayer_GetCurrentAudioTrack_mEC3ABD06F85003D24F160CE8DE3490524A625B8A,
	NullMediaPlayer_SetAudioTrack_mEB9EE4EFC70C0352F9E8A9EF175F77E0B7CA54FD,
	NullMediaPlayer_GetVideoTrackCount_m0F4DBD7A105BCF05EC8122E18887FB79D71046AC,
	NullMediaPlayer_GetCurrentVideoTrack_m3074C13149D2B5FA92D821348EB2DFEC7E5CA95D,
	NullMediaPlayer_GetCurrentAudioTrackId_mBE5D1E0A9182F9AFB9DB883A3463BEB06DB5EBC3,
	NullMediaPlayer_GetCurrentAudioTrackBitrate_mD41ACEA1199BF8867ECA085A01027B6C16BFA4AC,
	NullMediaPlayer_SetVideoTrack_m2988DF0CC727E4CD488CF7DF5D9084AE6B6501D3,
	NullMediaPlayer_GetCurrentVideoTrackId_m6FC3DEE140CE674A7484617A4E0F5328B7464BDC,
	NullMediaPlayer_GetCurrentVideoTrackBitrate_m68EC94EA88DC7AEEBFF0EE8D888FC70D2CC189DA,
	NullMediaPlayer_GetVideoFrameRate_m7FA1B9BE33A1D36AA4948E00D7A4EF044D47D864,
	NullMediaPlayer_Update_m4BAB2D62057BB9C5CCF9DB8BEAAC9F9C016BFA5F,
	NullMediaPlayer_Render_m8C59CCD4F58F96B12C313C5E030A7DEF544F7979,
	NullMediaPlayer_Dispose_m395EBD5C0579F5AD2D531ECB2358D16658416597,
	NullMediaPlayer__ctor_mF2E673280640BB0B17A8CFA19769F9FCD966E708,
	Resampler_get_DroppedFrames_m89C73FBFFA361B76C942696E7E67C16D37BEC16E,
	Resampler_get_FrameDisplayedTimer_m1FEA8F30EFE891EBD7578CCA9C13C139C5BC2148,
	Resampler_get_BaseTimestamp_mD32E51FEDE63B04FF2EF5AFC81521E011CA2BBA5,
	Resampler_set_BaseTimestamp_m75489EB5F4E3106BE10E99382C9158761CF260BF,
	Resampler_get_ElapsedTimeSinceBase_mAF0A0C70D042DA9A93F12CC2808EF6A757D4E0DD,
	Resampler_set_ElapsedTimeSinceBase_mB4ECC7BE68BF64866EF400A59295F560D1B73962,
	Resampler_get_LastT_m14664B1A60E1A35DEC05847B385D1B0F20D53BC4,
	Resampler_set_LastT_mA6EEBE901C8F444E4C0755399E984A17348F798F,
	Resampler_get_TextureTimeStamp_mE0BF12F7A977BE1B3461A8315EB26D1BB0654205,
	Resampler_set_TextureTimeStamp_m45BAB50E6FF3E9CB4BC9F410FD36A311781FDAF8,
	Resampler_OnVideoEvent_mF3C34FC5C11E42C476CAD167D1916122CD1E753A,
	Resampler__ctor_m76645A02F144B2210AAFA5DA1222498798E26B36,
	Resampler_get_OutputTexture_m9FCDEA6FCB2223759563C21987CAF332074D5F14,
	Resampler_Reset_mA045C7BB3F8C0EF92D4A90846A862728280F1867,
	Resampler_Release_m9E52FCA3DF6A9E683048B79DD2844CE78489408E,
	Resampler_ReleaseRenderTextures_mD81331355472932144F5BB614DC9773DD680F813,
	Resampler_ConstructRenderTextures_mD4EC34B39ED92D4CE61B331A77779EE99C509E97,
	Resampler_CheckRenderTexturesValid_m7C496A52108FF27225FC3EDF1D49ED8118F0618B,
	Resampler_FindBeforeFrameIndex_m6F4116CB277A02CF0D8C5F11CE357EA689B95D2E,
	Resampler_FindClosestFrame_m38E7996995CC032423255F3987B93702E1C517D4,
	Resampler_PointUpdate_mF8FFCFCB3EF9316A248B1A6038D546614D1B45FA,
	Resampler_SampleFrame_m5DFC2F51A491DCF401463401EB0DE30C211F0552,
	Resampler_SampleFrames_mF3E75BEC6D4007294FB03E9B4B0A440B7A45770E,
	Resampler_LinearUpdate_mFAB465F1C8C876BDFA896FC9D5407602B0815ED0,
	Resampler_InvalidateBuffer_mC0AF6A0D43E9FB55417DC2197ED0CB18F7D16141,
	Resampler_GuessFrameRate_mB3FB29DBB3EBB30F8B632D16A38713DD1D41F3E9,
	Resampler_Update_m3254C81FF143CAA0F94478AD9F3A36DA35889409,
	Resampler_UpdateTimestamp_mCD50651F28142B6D657B9E08552E409EF71C1329,
	TimestampedRenderTexture__ctor_m8BD6A57BEACA9639A33C95B79E1EC9E3F02292D3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Stream__ctor_m0EF9AC2B34988BCFFA29F5B6CA363E7550F99864,
	AutoRotate_Awake_m9125985087782E449B8BDE1701AFA7E1F52E2CC0,
	AutoRotate_Update_m996711F457A3DAB5E32728F7E7531152AC3FFCE7,
	AutoRotate_Randomise_m151CF6AB83A74D336DE9CBB95B880E0C0C888A41,
	AutoRotate__ctor_mF6EA1D9394B3FFD722CEBCE5E005942640D4FFFD,
	DemoInfo__ctor_m84D3B4449BF5A454F70EA714E53CB4C0A3FF0F14,
	FrameExtract_Start_m86C6F9E8C0BB8CA9C0E35FE161494E4D4FD26B88,
	FrameExtract_OnMediaPlayerEvent_m236350A2DA194DC03BBCDE1572854A509E8752AD,
	FrameExtract_OnNewMediaReady_m1BDB897A36FD26056774A42DB69D5E997DFD0C0A,
	FrameExtract_OnDestroy_m3C828BF1AAE7DB99DA82981E3B7FF33B91B93EC9,
	FrameExtract_Update_m2FE9DE4DCCB9974FEF4DBDD9C12AD87F62B3CB61,
	FrameExtract_ProcessExtractedFrame_mB0F64766802F1839EFB80F52B330E19B2BA45BAF,
	FrameExtract_ExtractNextFrame_m76FADAC81FF292A7BFBC9A3CCA3B381BB4A4A2BE,
	FrameExtract_OnGUI_m1D35E04595001C25AB77D243933F4829E465D8F2,
	FrameExtract__ctor_m47ECE9903AD1855FF054A482D0AC956BD406EF8A,
	Mapping3D_Update_mC2E7D7CD818334EC01889C136FF7B60B09075F45,
	Mapping3D_SpawnCube_mE69B5D32E8B56DBF702C22117A9D68168D6509AA,
	Mapping3D_RemoveCube_m97989618E2B42C976A7D429EFE3E49E38A2A35CC,
	Mapping3D__ctor_m8280D65CDEA1260848492F0D75A1AA64A9F83C62,
	SampleApp_Multiple_Update_m9F7C7DA6A701547501069744203715D30A5BC456,
	SampleApp_Multiple_UpdateVideosLayout_m6DDFEC63572493C1FEA957C7F0F167A6FA335B90,
	SampleApp_Multiple_AddVideoClicked_m36B1B0E32A8A76354FFA4F1E35E2DD9B13303655,
	SampleApp_Multiple_RemoveVideoClicked_m85ED7A8638AE26D4B2B55ACDB3F587950A34CD1C,
	SampleApp_Multiple_OnDestroy_m74B83A6EE2274993028C30FB1A94DA756D3A2CFF,
	SampleApp_Multiple__ctor_m7430B511051CC8DA439F95F2709FC059AA01041A,
	ChangeAudioTrack_OnEnable_mEA7D6510235C5A0FB91F6438512256E5188F7C66,
	ChangeAudioTrack_Update_mD496150E0FFEAAEE985F707B963F98476688A17F,
	ChangeAudioTrack_IsVideoLoaded_mBB2EF585989E345FB94140E762CB98F5A5D4C08B,
	ChangeAudioTrack_DoChangeAudioTrack_mC2192C7319F71BBED2CFC0412F494A3C1EDF07A2,
	ChangeAudioTrack__ctor_mDE6DF674164766E2F99A48794099FD4CED7F4B5A,
	ChangeStereoMode_Update_m4A80BCC9F758E8FA5B25C087E5ABA1122BDC7948,
	ChangeStereoMode__ctor_m983D047D9A67B39A904DD375F3B7C6A90A7EABA9,
	ChangeVideoExample_LoadVideo_m9DB2C4845DB26310D2E0C5DAD82B67A77D61BA96,
	ChangeVideoExample_OnGUI_m22143D11AA05FE3B4FA2AAE0E365496ABBA19F8A,
	ChangeVideoExample__ctor_m858B516767895F2F48E40A6B5434B49C52568876,
	LoadFromBuffer_Start_m634D4F939D473E4A64850FCA4B22EA686B544D21,
	LoadFromBuffer__ctor_m5C20613912E8A1EC5862B3CF68BEA3849CBC8CB6,
	LoadFromBufferInChunks_Start_m75693C9231FB7808DD89670B4039530B4E126867,
	LoadFromBufferInChunks__ctor_mC211F50F13C2E652530CBB381FF5BF87E93C43C9,
	NativeMediaOpen_Start_m832C6A4189EFA0E2FF8B779897AEA8DE06056C01,
	NativeMediaOpen_Update_m9EF14073FF9F5665B22BC915BB61FD0F8904A059,
	NativeMediaOpen_OnGUI_mF2F3448171A524013F380712F4E3A77C7A526301,
	NativeMediaOpen__ctor_m7FB80BD87A11C55DB1F6A378E470349999257019,
	PlaybackSync_Start_m3D3D76F2F508945BDB5E5B8B8FBCD356D8EB7625,
	PlaybackSync_LateUpdate_mA6CD6504C41A1CA36064EBD796FF4AE62059D077,
	PlaybackSync_IsAllVideosLoaded_m35A6AD92B328914235EAA739F9EDA31C51C98373,
	PlaybackSync_IsVideoLoaded_m75D4E04E968D92C842F60FA97F8D18C348C02E48,
	PlaybackSync_IsPlaybackFinished_m6140287F20C6D0CAB8B90A809B6723E750447874,
	PlaybackSync__ctor_m720134ED1E5E955CB902CC1C2F6C9C181E7B2113,
	StartEndPoint_OnEnable_mCF4D9FDD4C1A09E27602E8E662E9A0A5CE7E4351,
	StartEndPoint_Update_m7633632951E35EC0D4756080159746EE5D8379D6,
	StartEndPoint_IsVideoLoaded_mF5D3FF9061F94EAB62EE5468F1CF975385B5496E,
	StartEndPoint_DoStart_m23A60257901F9B7E4165978AF7C8E83E9B6A3F87,
	StartEndPoint_DoCheckEnd_m06CA2EECC3AB3626B18CB1ED771A73FD79509021,
	StartEndPoint_DoCheckLoop_m873C604C0022D7BF5AAADF6C867E14B5FBA599A8,
	StartEndPoint__ctor_m757CF90B0BF79B7C2D78AF20A4E70240D3BFE7DD,
	VideoTrigger_OnTriggerEnter_m8E48C1631203B0C8B819052BCC1DAC93DB05B355,
	VideoTrigger_OnTriggerExit_m0251AFF2B0005DED8E6A0A346D9A53F30A770DF5,
	VideoTrigger_Update_mC8A9CCDE353D055366F4E4F77776323E831ECAC8,
	VideoTrigger__ctor_mA7BE38FA20CA4D80F20E2872AA4B69CD9062483E,
	SimpleController_Start_m02353FB587A70A57549F79657AF412735BF7FD61,
	SimpleController_OnDestroy_mE538A96108461D97185E1235FB65D55BB1D072D8,
	SimpleController_OnMediaPlayerEvent_m38F7547BC6E4DFACA11F414B4BFFA154B569EB91,
	SimpleController_AddEvent_mDD46200976ADB7ACC54977D0D3B0DF8C59280256,
	SimpleController_GatherProperties_mDF1C212311EDA1A49FF150BB5C782E07E3618B33,
	SimpleController_Update_mBE9CB73F11479998D432232B6B5BCB8E9734D489,
	SimpleController_LoadVideo_m8C759CAA63682B2134A9EBB067032F88B9E441A6,
	SimpleController_VideoIsReady_m5F71FECD8B7EB71A09C8A25336E6E676A454242B,
	SimpleController_AudioIsReady_mEFF0519289C815E43EE68EAC005C286BE2B5907E,
	SimpleController_LoadVideoWithFading_mEA1895F18C5A29022FFB95B240850EF57A01DEBE,
	SimpleController_OnGUI_mC8633AEF945EBE382AA75F4F08FCB6C0329F2744,
	SimpleController__ctor_mD6B3049AD749E6755E2B8DD9BB518920984B81E5,
	U3CLoadVideoWithFadingU3Ed__23__ctor_m82DE7FDB17C4F4F5D90E14A3642D40C45237FA91,
	U3CLoadVideoWithFadingU3Ed__23_System_IDisposable_Dispose_mF6B335E78513CCE1A4BA3C32A075E004595E0B7A,
	U3CLoadVideoWithFadingU3Ed__23_MoveNext_mFEC22F4329D83829C97271AF392350A673BB9EF6,
	U3CLoadVideoWithFadingU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB51738CA40EBA5A1083400C83666804A6D24A214,
	U3CLoadVideoWithFadingU3Ed__23_System_Collections_IEnumerator_Reset_m44DA52D0E0CC3CD7D5F21583F357767AF45EBF94,
	U3CLoadVideoWithFadingU3Ed__23_System_Collections_IEnumerator_get_Current_m6AEC2F56DE7688F3E09466C723AEBB3ABCA718FE,
	SphereDemo_IsVrPresent_m91FA26D14A2CE24DD8E2A2285BC981029803A721,
	SphereDemo_Start_mA153F49EF2EE8361A940C391079742029ED1C615,
	SphereDemo_OnDestroy_m10D2C2F74809D59AA1C1A1E1EECD1F937AC5D045,
	SphereDemo_Update_mB0615BAEE1D57161B31DA2FFCB5CE2F373F97485,
	SphereDemo_LateUpdate_m599660EA44EE585A4BDC24F320700F47B853B3A6,
	SphereDemo__ctor_m027FA668E0A76EF3803026D83722D87902CF8EF6,
	VCR_get_PlayingPlayer_m07A887D5B87224B4B98BEC287E08B536FB67EAAC,
	VCR_get_LoadingPlayer_m3F44CEBE706099BC4B4E92B73D42C4EE19837C54,
	VCR_SwapPlayers_mEE914161C5FCB29390CD537F58A4D8E3514E0B3D,
	VCR_OnOpenVideoFile_m598FE969E2763FD9059F0364244C7F8E8170BFD0,
	VCR_OnAutoStartChange_m55A6A92415F1264AF2995139E2E31C6739AE8D45,
	VCR_OnMuteChange_m82D467CAD88BAEE790C9812C5832AE42430C7542,
	VCR_OnPlayButton_m42FC3F109D3027E145A5376C50189CA77FF2AE6A,
	VCR_OnPauseButton_m4F7AD3FE09504E9CEE370D596EA101DAC7C9C720,
	VCR_OnVideoSeekSlider_mA467027887666B5C2118E7FB06E6F29781AAFF3C,
	VCR_OnVideoSliderDown_mF133C85D1605CF4ECB859C4FB8903814C9B47664,
	VCR_OnVideoSliderUp_m2B41A78E17AB58F70534D9338BD10EE262D81132,
	VCR_OnAudioVolumeSlider_mB0B4C468DEC2D93CFED5E3F57BC6A706D1DE9ADF,
	VCR_OnRewindButton_m1C0C84A34B0AE9C32339268A409C8B683641F8BE,
	VCR_Awake_m47158A5A2FCD8A3DCB2A179D7A89C997B8EE740F,
	VCR_Start_m08A6046EBF3DBC05BA4207FCA9DF98478C464373,
	VCR_OnDestroy_mD4375B8F9F107C8A3E690A519C84A013C8EC2127,
	VCR_Update_mA2E77C835DF6BAE61F095BAA6E544605C7964605,
	VCR_OnVideoEvent_m705EFA9010D3DF0257BABC09F5E994CB338BA3B0,
	VCR__ctor_mA15FDE1E3FD5F12907E1EEA46EB16DCDF887D2EC,
	CustomVCR_get_PlayingPlayer_m6EF241E17063D2CB4B787F8DFEA1927EFDFC19BC,
	CustomVCR_get_LoadingPlayer_m09F7BA34CCA16BB5ECB100CC2EE55E20882511FB,
	CustomVCR_SwapPlayers_m9ED54DDC7C91B2D4DB2E168E0C0816D20FFF65EB,
	CustomVCR_OnOpenVideoFile_mCE6CF8E315D03113D941FD1E0C6BEAA1CD2E2622,
	CustomVCR_OnAutoStartChange_mCC8E88778A330ECBE32C4F9C331A2F063CF8F502,
	CustomVCR_OnMuteChange_m13BAD578D3D674586C8F65DF65DD9407D94EE428,
	CustomVCR_OnPlayButton_m4CF288953443E8630597A76248431DCCC79FA170,
	CustomVCR_OnPauseButton_m06934FAA9EDF7962B291E466F5854A0037216673,
	CustomVCR_OnVideoSeekSlider_m1D59BDB05518F4FD5B3777A6779A4681C12136DD,
	CustomVCR_OnVideoSliderDown_mA476D7BD1D3C51267B16C557DD9324844AF9379F,
	CustomVCR_OnVideoSliderUp_m926342FF46ADA5E7E2E0B1B00D0ED64ABD83DADB,
	CustomVCR_OnAudioVolumeSlider_m9EFE453A75AEDF9122D021141506F1F0B8E3FD65,
	CustomVCR_OnRewindButton_mEF0F2E27DE5D4DE9376C49EABA6989F4C27298DC,
	CustomVCR_Awake_m16CA8271866220DFE77152671008F4ABB4C11A0D,
	CustomVCR_Start_m5DD1F517FE36D204C69C5E0D5906F45BA1FB5F0B,
	CustomVCR_OnDestroy_m1288DBB6534B8F7E16A8D532D6F124335D875C3D,
	CustomVCR_Update_m2BC2EB5E957B5D7FB16056271C21A56424111244,
	CustomVCR_OnVideoEvent_m42C4E6A6DE11F3EF63315E63B0433B7E32D275CF,
	CustomVCR__ctor_m9F1009D9F62A9F5983E8309C07AAC68EA2EE962C,
};
extern void NamedPrefab__ctor_m4C5A1EAC954851954E10ADF5F4ED646967BAB252_AdjustorThunk (void);
extern void HTTPHeader__ctor_mBD26E64BA6EB557D29C8E5E346802BD3BF6E199E_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[2] = 
{
	{ 0x06000075, NamedPrefab__ctor_m4C5A1EAC954851954E10ADF5F4ED646967BAB252_AdjustorThunk },
	{ 0x0600014F, HTTPHeader__ctor_mBD26E64BA6EB557D29C8E5E346802BD3BF6E199E_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1001] = 
{
	3244,
	3244,
	3244,
	3244,
	3217,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	2688,
	3244,
	2686,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3195,
	3244,
	2651,
	3244,
	3217,
	3195,
	3244,
	3195,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3208,
	2681,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	2666,
	3244,
	3244,
	3244,
	3244,
	3195,
	2666,
	3244,
	2651,
	3244,
	3217,
	3195,
	3244,
	3195,
	3195,
	3195,
	3244,
	3244,
	3244,
	942,
	3244,
	3244,
	3244,
	3244,
	3244,
	2651,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	603,
	3195,
	3244,
	2651,
	3244,
	3217,
	3195,
	3244,
	3195,
	3195,
	2666,
	2666,
	3244,
	3244,
	3195,
	2666,
	3244,
	3244,
	3244,
	3244,
	3244,
	2601,
	3244,
	3244,
	2666,
	2666,
	3244,
	2025,
	1587,
	2666,
	3244,
	1321,
	3195,
	2666,
	3195,
	2666,
	3195,
	2666,
	3195,
	2666,
	3239,
	2708,
	3239,
	2708,
	3244,
	2666,
	3244,
	942,
	3244,
	963,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3195,
	2666,
	3195,
	2666,
	3195,
	2666,
	3195,
	2666,
	3239,
	2708,
	3239,
	2708,
	3244,
	3244,
	942,
	2666,
	3244,
	963,
	3244,
	3244,
	3244,
	3244,
	3195,
	2666,
	3244,
	2651,
	1539,
	3244,
	3244,
	3244,
	3244,
	3244,
	2666,
	942,
	4661,
	3244,
	2666,
	3195,
	3244,
	3244,
	3244,
	3244,
	3244,
	913,
	3244,
	3217,
	2686,
	3195,
	2666,
	2651,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3195,
	3244,
	3244,
	3208,
	3244,
	3244,
	4962,
	4369,
	5074,
	5074,
	3195,
	3244,
	3244,
	3195,
	3195,
	3217,
	3244,
	3244,
	3195,
	2666,
	3208,
	2681,
	3244,
	2666,
	2666,
	2666,
	1262,
	3244,
	5093,
	3195,
	3217,
	2686,
	3179,
	2651,
	3195,
	3195,
	3195,
	3195,
	3195,
	3195,
	3217,
	3217,
	2686,
	3217,
	2686,
	3179,
	2651,
	2666,
	3195,
	3217,
	2686,
	3219,
	2688,
	3219,
	2688,
	3195,
	2666,
	3195,
	3195,
	3195,
	3195,
	3195,
	3195,
	3195,
	3195,
	3195,
	3244,
	3244,
	3244,
	839,
	1210,
	1200,
	850,
	3217,
	3217,
	3195,
	3179,
	1195,
	771,
	3244,
	2295,
	2280,
	850,
	3217,
	3217,
	3244,
	3244,
	3244,
	3244,
	3244,
	2686,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3195,
	5068,
	3195,
	4924,
	4365,
	3195,
	3180,
	3195,
	754,
	3195,
	1197,
	3244,
	3244,
	3244,
	3244,
	2279,
	1196,
	1196,
	2686,
	2686,
	5074,
	141,
	196,
	253,
	505,
	3244,
	5093,
	3244,
	3217,
	3195,
	3195,
	3195,
	4927,
	4927,
	3244,
	1542,
	3217,
	3244,
	3195,
	3217,
	3195,
	3195,
	3195,
	3244,
	3244,
	3244,
	3244,
	3217,
	3244,
	3244,
	3195,
	3217,
	3244,
	3244,
	3244,
	3217,
	3244,
	3217,
	3244,
	3217,
	3244,
	3244,
	1541,
	2666,
	776,
	2666,
	2651,
	3244,
	3217,
	3195,
	3244,
	3195,
	2651,
	3244,
	3217,
	3195,
	3244,
	3195,
	2651,
	3244,
	3217,
	3195,
	3244,
	3195,
	3195,
	2279,
	3244,
	3244,
	3195,
	3195,
	3195,
	3179,
	3195,
	3179,
	2651,
	3217,
	2686,
	3195,
	3195,
	3195,
	3244,
	3195,
	3195,
	3244,
	3244,
	3244,
	942,
	3217,
	3217,
	2279,
	2279,
	2666,
	3217,
	921,
	3244,
	2006,
	3179,
	3179,
	3217,
	3180,
	3217,
	3191,
	4924,
	3244,
	4924,
	4549,
	4549,
	4549,
	4984,
	4984,
	4984,
	4984,
	4984,
	4984,
	4984,
	4984,
	4984,
	4984,
	4984,
	4984,
	4984,
	4984,
	4984,
	4984,
	4984,
	3244,
	3244,
	3195,
	3244,
	3217,
	3195,
	3195,
	3195,
	3244,
	3244,
	3244,
	3244,
	3244,
	2666,
	942,
	3244,
	3179,
	2651,
	3244,
	1542,
	3244,
	3244,
	5082,
	5093,
	4639,
	1121,
	201,
	1568,
	3180,
	3195,
	154,
	2666,
	3195,
	3244,
	2686,
	3217,
	3217,
	3217,
	3217,
	3217,
	3244,
	3244,
	3244,
	2688,
	2688,
	3219,
	2688,
	3219,
	2675,
	3244,
	2686,
	1574,
	2675,
	3244,
	3219,
	3179,
	3179,
	3219,
	3219,
	3219,
	3217,
	3217,
	3217,
	3217,
	3217,
	2006,
	3179,
	3217,
	2686,
	3217,
	2688,
	3219,
	2688,
	3219,
	3179,
	3179,
	2651,
	3195,
	3179,
	3179,
	3179,
	2651,
	3195,
	3179,
	1207,
	3180,
	3244,
	2666,
	3244,
	3162,
	3244,
	3217,
	3195,
	3244,
	5093,
	5070,
	4837,
	4837,
	4837,
	4850,
	4837,
	4837,
	4979,
	4959,
	5074,
	3244,
	185,
	953,
	5093,
	3195,
	154,
	2295,
	2280,
	850,
	3217,
	3244,
	2686,
	3217,
	3217,
	3217,
	3244,
	3244,
	3244,
	3244,
	2688,
	2688,
	982,
	3219,
	3162,
	3195,
	3219,
	2688,
	3219,
	3179,
	3179,
	3208,
	3219,
	3217,
	3217,
	3217,
	3217,
	3217,
	3217,
	3217,
	1207,
	2686,
	2666,
	2666,
	2666,
	2666,
	3217,
	3217,
	2686,
	2651,
	3179,
	2006,
	3179,
	3217,
	3180,
	3217,
	3195,
	3191,
	2686,
	3217,
	2688,
	2688,
	3219,
	3219,
	3179,
	2006,
	3179,
	2651,
	3195,
	3179,
	3179,
	2675,
	3244,
	2651,
	2686,
	1574,
	2675,
	3244,
	3179,
	2006,
	3179,
	2651,
	3195,
	3179,
	3219,
	3180,
	3219,
	3244,
	3244,
	3244,
	3179,
	3180,
	3195,
	3217,
	3179,
	830,
	911,
	2666,
	942,
	3217,
	3217,
	2295,
	3244,
	3179,
	3195,
	3244,
	3244,
	3179,
	3179,
	3179,
	3195,
	3195,
	3195,
	3195,
	3195,
	537,
	4962,
	1542,
	607,
	544,
	3217,
	2666,
	2666,
	3244,
	3244,
	3244,
	3244,
	3244,
	2295,
	3179,
	3195,
	154,
	2295,
	2280,
	850,
	3217,
	3244,
	2686,
	3217,
	3217,
	3217,
	3217,
	3217,
	3217,
	3217,
	3217,
	3244,
	3244,
	3244,
	3244,
	2688,
	2688,
	982,
	3219,
	3162,
	3195,
	3219,
	2688,
	2686,
	3217,
	2688,
	2688,
	3219,
	3219,
	3179,
	2651,
	3179,
	2651,
	3219,
	3179,
	830,
	3179,
	3180,
	911,
	942,
	3179,
	2675,
	3244,
	2651,
	2686,
	1574,
	2675,
	3244,
	1207,
	2686,
	2666,
	2666,
	2666,
	2666,
	3217,
	3217,
	2686,
	2651,
	3219,
	3179,
	3179,
	3208,
	3219,
	3219,
	3217,
	3217,
	3179,
	2006,
	3195,
	3179,
	3179,
	2006,
	3195,
	3179,
	3195,
	3217,
	3217,
	3195,
	3180,
	3179,
	2006,
	3179,
	3217,
	3180,
	3217,
	3191,
	2319,
	2319,
	3244,
	4924,
	4924,
	5074,
	4661,
	4380,
	4841,
	4871,
	4656,
	4656,
	4209,
	4656,
	4666,
	4312,
	4541,
	4085,
	4795,
	3633,
	3780,
	4841,
	4927,
	3195,
	154,
	3244,
	2686,
	3217,
	3217,
	3217,
	3217,
	3217,
	3244,
	3244,
	3244,
	3217,
	3217,
	3217,
	3217,
	3217,
	3219,
	3179,
	3179,
	3219,
	2006,
	3179,
	3217,
	2688,
	2688,
	982,
	3219,
	2688,
	3219,
	3219,
	2686,
	3217,
	2688,
	3219,
	3179,
	3179,
	2651,
	3179,
	3179,
	3195,
	3179,
	2651,
	3195,
	3179,
	3219,
	3244,
	3244,
	3244,
	3244,
	3179,
	3179,
	3180,
	2652,
	3219,
	2688,
	3219,
	2688,
	3180,
	2652,
	942,
	620,
	3195,
	3244,
	3244,
	3244,
	3244,
	3217,
	1872,
	1872,
	3244,
	1419,
	580,
	3244,
	3244,
	3219,
	3244,
	3244,
	3244,
	3179,
	3179,
	3179,
	3195,
	3195,
	3195,
	3195,
	3195,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	942,
	3244,
	3244,
	3244,
	2666,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3217,
	4471,
	3244,
	3244,
	3244,
	2666,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3217,
	4962,
	4962,
	3244,
	3244,
	3244,
	4962,
	4667,
	4667,
	4223,
	3244,
	2666,
	2666,
	3244,
	3244,
	3244,
	3244,
	942,
	2651,
	3244,
	3244,
	1545,
	4962,
	4962,
	3195,
	3244,
	3244,
	2651,
	3244,
	3217,
	3195,
	3244,
	3195,
	5082,
	3244,
	3244,
	3244,
	3244,
	3244,
	3195,
	3195,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	942,
	3244,
	3195,
	3195,
	3244,
	2651,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	3244,
	942,
	3244,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1001,
	s_methodPointers,
	2,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
