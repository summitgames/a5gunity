﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct InterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct InterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31;
// System.Func`2<System.Single,System.Single>
struct Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149;
// System.Collections.Generic.List`1<UnityEngine.Events.UnityAction`3<RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode>>
struct List_1_t3C68D72B7485781E67A15171BF3DE6D27003CBF3;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t815A476B0A21E183042059E705F9E505478CD8AE;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F;
// System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader>
struct List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452;
// System.Collections.Generic.Queue`1<System.String>
struct Queue_1_tD2C03A5990B5958D85846D872A22AA67F3E8F97D;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// UnityEngine.Events.UnityAction`3<RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode>
struct UnityAction_3_t8232B65BAFB61715E1BA6DC746E10C91EB537ADB;
// UnityEngine.Events.UnityAction`3<System.Object,System.Int32Enum,System.Int32Enum>
struct UnityAction_3_t5380F08944E494DE28EBADE9711BB8DF4EC87123;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader[]
struct HTTPHeaderU5BU5D_tDC8254FC7749720A2ED8557C46398F574DFAA8B0;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E;
// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// RenderHeads.Media.AVProVideo.DisplayIMGUI
struct DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04;
// RenderHeads.Media.AVProVideo.DisplayUGUI
struct DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// UnityEngine.GUISkin
struct GUISkin_tE353D65D4618423B574BAD31F5C5AC1B967E32C6;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.IDisposable
struct IDisposable_t099785737FC6A1E3699919A94109383715A8D807;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// RenderHeads.Media.AVProVideo.IMediaControl
struct IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C;
// RenderHeads.Media.AVProVideo.IMediaInfo
struct IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4;
// RenderHeads.Media.AVProVideo.IMediaPlayer
struct IMediaPlayer_t2E66F33315EA803836A22C719212E062847381B9;
// RenderHeads.Media.AVProVideo.IMediaProducer
struct IMediaProducer_tC1A2D084AE2332166A227C6B2216B214DAB4D978;
// RenderHeads.Media.AVProVideo.IMediaSubtitles
struct IMediaSubtitles_t8E1536FBC8120C1C678E06AA4A1B9CE49B434747;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// RenderHeads.Media.AVProVideo.MediaPlayer
struct MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64;
// RenderHeads.Media.AVProVideo.MediaPlayerEvent
struct MediaPlayerEvent_t0E890CDB48C2168A48E68F40D0E63D1B3C4A0107;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// PreloaderController
struct PreloaderController_tD3EB462D0A9954E9D9785017331EFFC5C9072801;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.RenderTexture
struct RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849;
// RenderHeads.Media.AVProVideo.Resampler
struct Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// UnityEngine.Shader
struct Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39;
// RenderHeads.Media.AVProVideo.Demos.SimpleController
struct SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65;
// UnityEngine.UI.Slider
struct Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A;
// Crosstales.OnlineCheck.Demo.SpeedCheck
struct SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E;
// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396;
// UnityEngine.Networking.UploadHandler
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA;
// System.Uri
struct Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// VideoPlayerController
struct VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579;
// VideoTimeDisplay
struct VideoTimeDisplay_t99E522A668D79E64AD5F7E5D3676A8B3D78B8832;
// RenderHeads.Media.AVProVideo.Demos.VideoTrigger
struct VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656;
// VideoVCR
struct VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// WagonWheelManager
struct WagonWheelManager_t06317F447976C4D78C9E31F402AA7122B0F187C5;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013;
// UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209
struct U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010;
// RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188
struct U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2;
// RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166
struct U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB;
// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid
struct OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393;
// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple
struct OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E;
// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsIOS
struct OptionsIOS_t868D1748A3E07299A108C1E60F97753CA38954A5;
// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsMacOSX
struct OptionsMacOSX_t1B3B40C8ABFE68B24D80EA87E10EFA9C1972A2B3;
// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsPS4
struct OptionsPS4_t43756CE0642DB94AA9A8E30A58524CDC7BD082D1;
// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsTVOS
struct OptionsTVOS_tFDA4033A636E29A9005B4D9092A32298FFE8911B;
// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWebGL
struct OptionsWebGL_tA7A6C5EB6C224751A8B5DF0B99DE0C1A3DD15B05;
// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows
struct OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1;
// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsPhone
struct OptionsWindowsPhone_t5B5890235D85585DFCBEC0D280F332C274B6E63C;
// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsUWP
struct OptionsWindowsUWP_tBA5BE5D51A12AA6360DF87691342C5EE7B8F7F66;
// RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions
struct PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F;
// RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame
struct ProcessExtractedFrame_t9BDA228D84FCD6905FA985C6759CD5E40590AA45;
// RenderHeads.Media.AVProVideo.MediaPlayer/Setup
struct Setup_tD0FAF61885F9D7910834A6774C9282AE0C7D28B6;
// RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem
struct MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF;
// RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing
struct Easing_tDD586F922397A60A9F213AA64B7AFC753FE0686D;
// PreloaderController/<DisablePreloaderPanel>d__3
struct U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE;
// RenderHeads.Media.AVProVideo.Resampler/TimestampedRenderTexture
struct TimestampedRenderTexture_t8DDA74DDD11B5CCACD2920F8CD536801ACD00326;
// RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23
struct U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9;
// UnityEngine.UI.Slider/SliderEvent
struct SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780;
// Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16
struct U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817;
// VideoPlayerController/<StartVideoPlaying>d__13
struct U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723;

IL2CPP_EXTERN_C RuntimeClass* Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventType_tB2FE909C749F1B861A530F105A083945CB226BBF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IMediaPlayer_t2E66F33315EA803836A22C719212E062847381B9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IMediaProducer_tC1A2D084AE2332166A227C6B2216B214DAB4D978_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IMediaSubtitles_t8E1536FBC8120C1C678E06AA4A1B9CE49B434747_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Path_tF1D95B78D57C1C1211BA6633FF2AC22FD6C48921_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_3_t8232B65BAFB61715E1BA6DC746E10C91EB537ADB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral069BDB2BE9E83605F3B9AB20CB78B6D7E09B49FC;
IL2CPP_EXTERN_C String_t* _stringLiteral0C3C6829C3CCF8020C6AC45B87963ADC095CD44A;
IL2CPP_EXTERN_C String_t* _stringLiteral1DA847B0C8711F8529FBC7BC20711A1361A8B323;
IL2CPP_EXTERN_C String_t* _stringLiteral271909F8F2564394AD0733493FE831469E5734A1;
IL2CPP_EXTERN_C String_t* _stringLiteral45CFEEE4F62A474970E25FE47E07529750C5411E;
IL2CPP_EXTERN_C String_t* _stringLiteral4D8D9C94AC5DA5FCED2EC8A64E10E714A2515C30;
IL2CPP_EXTERN_C String_t* _stringLiteral59569CD1945760103A81F13277CC2DEBE8564737;
IL2CPP_EXTERN_C String_t* _stringLiteral5962E944D7340CE47999BF097B4AFD70C1501FB9;
IL2CPP_EXTERN_C String_t* _stringLiteral5A1278AB54EFEBBD1E0E03AB2677F22D39311C27;
IL2CPP_EXTERN_C String_t* _stringLiteral5FB35B2106DEBEFCD4761E77F85FD7B04718828B;
IL2CPP_EXTERN_C String_t* _stringLiteral77F51302B6EA5AF286BAF279CFF1D7437660DCCD;
IL2CPP_EXTERN_C String_t* _stringLiteral785F17F45C331C415D0A7458E6AAC36966399C51;
IL2CPP_EXTERN_C String_t* _stringLiteral7BC6A4DAF813268120D1FDB6C6954972C951796C;
IL2CPP_EXTERN_C String_t* _stringLiteral7F3238CD8C342B06FB9AB185C610175C84625462;
IL2CPP_EXTERN_C String_t* _stringLiteral848E5ED630B3142F565DD995C6E8D30187ED33CD;
IL2CPP_EXTERN_C String_t* _stringLiteral98866EAF371FBAAD526DEA44B3C0575E306213C7;
IL2CPP_EXTERN_C String_t* _stringLiteralA7C3FCA8C63E127B542B38A5CA5E3FEEDDD1B122;
IL2CPP_EXTERN_C String_t* _stringLiteralB2FF4E9D9185B1A580BC4440E2FC284851F9F147;
IL2CPP_EXTERN_C String_t* _stringLiteralB78F235D4291950A7D101307609C259F3E1F033F;
IL2CPP_EXTERN_C String_t* _stringLiteralBE583588F94930CFEE0452285EBE5C048F7DDF5B;
IL2CPP_EXTERN_C String_t* _stringLiteralCCCB24758B8281580D9CE13BCDDFE5C7584D4DCD;
IL2CPP_EXTERN_C String_t* _stringLiteralD5746C0BD05E9E09663FA764FFF2206D493AFA08;
IL2CPP_EXTERN_C String_t* _stringLiteralD8673C209A09B6EA196C0F9C6D5946365880B09D;
IL2CPP_EXTERN_C String_t* _stringLiteralDD5B87595462B1299CC2AA421A01B45CED796A03;
IL2CPP_EXTERN_C String_t* _stringLiteralE7A46075109789D1792549A284B05CF42BE37425;
IL2CPP_EXTERN_C String_t* _stringLiteralF18840F490E42D3CE48CDCBF47229C1C240F8ABE;
IL2CPP_EXTERN_C String_t* _stringLiteralF5FC1450686FCAF113D3F1C5FAEA1371BEAC8879;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_InCubic_m0F57ABFDA1DAF00A462FC55900C346C7B4308B08_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_InExpo_m611AB71C3B0E391EE9824BFB4271CABA990D2AFE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_InOutCubic_m2C0042F5722F61B0CF3B2C63DDADC2C045F41523_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_InOutExpo_mD33AD178CF4F37856E199F4247D55D1B6C904F36_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_InOutQuad_mD67729B41C8D095A565A1AEDA43FE0FF363E303B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_InOutQuart_m2A57949CDE900057F1F9D17C482242DCB35AE50A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_InOutQuint_mA57462E560637C403AD73D55CF03A4ECD29F2C51_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_InQuad_m835BD34EF6B489D51D8B7E31D5421BC89DF65998_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_InQuart_m9C1DD3B8219F88569905FB1B38B88E042A0D3245_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_InQuint_m59758F196F7FFE458A32F6F14B5429946A6FB4BC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_Linear_mA407428D581C8BA5C60991186DE52D521D8EEC5B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_OutCubic_m2693C08EED8C8A119C6362B759A27AABFE9C32FF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_OutExpo_mA71C6B4FE36A0F88D3FD06106B0BE7F3E24B8356_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_OutQuad_m810849EFC4CDD6477028DA82FB5006E3428407DD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_OutQuart_m494232308BA295350A858BE1E2FA475AC3A85FFF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_OutQuint_m0494D176295021B3512BAB04407A172F302A322E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Easing_Step_m74FF1FA20E4A7FE4232FBD5B0EFBEA036171605C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mED07ADBCE34435B4A44D92F49320193766170074_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mA4972EA6AD7EDA75836F78B588F59FC023AB4DDA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m199DB87BCE947106FBA38E19FDFE80CB65B61144_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CDisablePreloaderPanelU3Ed__3_System_Collections_IEnumerator_Reset_mF42974D782E8ABEF2D7C266ACC18B0C5F80EF45D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CExtractFrameCoroutineU3Ed__209_System_Collections_IEnumerator_Reset_m03E84C88DE36A133F5C616284E160B269D57B779_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CFinalRenderCaptureU3Ed__188_System_Collections_IEnumerator_Reset_m22429013FDB26856E4D1FD9EF1749DD600BF547C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_IEnumerator_Reset_m625A44CCFB701C03434E399A2A8AFD83D81C3628_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CLoadVideoWithFadingU3Ed__23_System_Collections_IEnumerator_Reset_m44DA52D0E0CC3CD7D5F21583F357767AF45EBF94_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CResetSpeedTestCheckU3Ed__16_System_Collections_IEnumerator_Reset_m4DF85D13FFF1B60EFF83963ECAA4A5728B292A01_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CStartVideoPlayingU3Ed__13_System_Collections_IEnumerator_Reset_m149BF3B02F1A023E95FAC7BBD2A5CF729F128F37_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityAction_3__ctor_mA257195F2A551B515268BFCB39C301E4ACE73057_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* VideoVCR_OnVideoEvent_m5A55B048083EAAD1F09A39BA3416B3F44100CD7F_RuntimeMethod_var;
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_com;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_com;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_com;
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_pinvoke;
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_com;

struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
struct HTTPHeaderU5BU5D_tDC8254FC7749720A2ED8557C46398F574DFAA8B0;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____items_1)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_StaticFields, ____emptyArray_5)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader>
struct List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	HTTPHeaderU5BU5D_tDC8254FC7749720A2ED8557C46398F574DFAA8B0* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452, ____items_1)); }
	inline HTTPHeaderU5BU5D_tDC8254FC7749720A2ED8557C46398F574DFAA8B0* get__items_1() const { return ____items_1; }
	inline HTTPHeaderU5BU5D_tDC8254FC7749720A2ED8557C46398F574DFAA8B0** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(HTTPHeaderU5BU5D_tDC8254FC7749720A2ED8557C46398F574DFAA8B0* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	HTTPHeaderU5BU5D_tDC8254FC7749720A2ED8557C46398F574DFAA8B0* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452_StaticFields, ____emptyArray_5)); }
	inline HTTPHeaderU5BU5D_tDC8254FC7749720A2ED8557C46398F574DFAA8B0* get__emptyArray_5() const { return ____emptyArray_5; }
	inline HTTPHeaderU5BU5D_tDC8254FC7749720A2ED8557C46398F574DFAA8B0** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(HTTPHeaderU5BU5D_tDC8254FC7749720A2ED8557C46398F574DFAA8B0* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.Text.StringBuilder
struct StringBuilder_t  : public RuntimeObject
{
public:
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t * ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;

public:
	inline static int32_t get_offset_of_m_ChunkChars_0() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkChars_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_m_ChunkChars_0() const { return ___m_ChunkChars_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_m_ChunkChars_0() { return &___m_ChunkChars_0; }
	inline void set_m_ChunkChars_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___m_ChunkChars_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkChars_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkPrevious_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkPrevious_1)); }
	inline StringBuilder_t * get_m_ChunkPrevious_1() const { return ___m_ChunkPrevious_1; }
	inline StringBuilder_t ** get_address_of_m_ChunkPrevious_1() { return &___m_ChunkPrevious_1; }
	inline void set_m_ChunkPrevious_1(StringBuilder_t * value)
	{
		___m_ChunkPrevious_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkPrevious_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkLength_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkLength_2)); }
	inline int32_t get_m_ChunkLength_2() const { return ___m_ChunkLength_2; }
	inline int32_t* get_address_of_m_ChunkLength_2() { return &___m_ChunkLength_2; }
	inline void set_m_ChunkLength_2(int32_t value)
	{
		___m_ChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_m_ChunkOffset_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkOffset_3)); }
	inline int32_t get_m_ChunkOffset_3() const { return ___m_ChunkOffset_3; }
	inline int32_t* get_address_of_m_ChunkOffset_3() { return &___m_ChunkOffset_3; }
	inline void set_m_ChunkOffset_3(int32_t value)
	{
		___m_ChunkOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_MaxCapacity_4)); }
	inline int32_t get_m_MaxCapacity_4() const { return ___m_MaxCapacity_4; }
	inline int32_t* get_address_of_m_MaxCapacity_4() { return &___m_MaxCapacity_4; }
	inline void set_m_MaxCapacity_4(int32_t value)
	{
		___m_MaxCapacity_4 = value;
	}
};


// UnityEngine.Events.UnityEventBase
struct UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// RenderHeads.Media.AVProVideo.WebGL
struct WebGL_tF23A88CB1FE5EFFAA1FF2E6968F9570133D27B9C  : public RuntimeObject
{
public:

public:
};


// RenderHeads.Media.AVProVideo.Windows
struct Windows_tA95FC52994B4E08EB1E068791BACD0CF20C1B460  : public RuntimeObject
{
public:

public:
};


// UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209
struct U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010  : public RuntimeObject
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.Texture2D RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::target
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___target_2;
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::<>4__this
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ___U3CU3E4__this_3;
	// System.Single RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::timeSeconds
	float ___timeSeconds_4;
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::timeThresholdMs
	int32_t ___timeThresholdMs_5;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::accurateSeek
	bool ___accurateSeek_6;
	// RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::callback
	ProcessExtractedFrame_t9BDA228D84FCD6905FA985C6759CD5E40590AA45 * ___callback_7;
	// UnityEngine.Texture2D RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::<result>5__2
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___U3CresultU3E5__2_8;
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::<currFc>5__3
	int32_t ___U3CcurrFcU3E5__3_9;
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::<iterations>5__4
	int32_t ___U3CiterationsU3E5__4_10;
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::<maxIterations>5__5
	int32_t ___U3CmaxIterationsU3E5__5_11;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010, ___target_2)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_target_2() const { return ___target_2; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010, ___U3CU3E4__this_3)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_timeSeconds_4() { return static_cast<int32_t>(offsetof(U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010, ___timeSeconds_4)); }
	inline float get_timeSeconds_4() const { return ___timeSeconds_4; }
	inline float* get_address_of_timeSeconds_4() { return &___timeSeconds_4; }
	inline void set_timeSeconds_4(float value)
	{
		___timeSeconds_4 = value;
	}

	inline static int32_t get_offset_of_timeThresholdMs_5() { return static_cast<int32_t>(offsetof(U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010, ___timeThresholdMs_5)); }
	inline int32_t get_timeThresholdMs_5() const { return ___timeThresholdMs_5; }
	inline int32_t* get_address_of_timeThresholdMs_5() { return &___timeThresholdMs_5; }
	inline void set_timeThresholdMs_5(int32_t value)
	{
		___timeThresholdMs_5 = value;
	}

	inline static int32_t get_offset_of_accurateSeek_6() { return static_cast<int32_t>(offsetof(U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010, ___accurateSeek_6)); }
	inline bool get_accurateSeek_6() const { return ___accurateSeek_6; }
	inline bool* get_address_of_accurateSeek_6() { return &___accurateSeek_6; }
	inline void set_accurateSeek_6(bool value)
	{
		___accurateSeek_6 = value;
	}

	inline static int32_t get_offset_of_callback_7() { return static_cast<int32_t>(offsetof(U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010, ___callback_7)); }
	inline ProcessExtractedFrame_t9BDA228D84FCD6905FA985C6759CD5E40590AA45 * get_callback_7() const { return ___callback_7; }
	inline ProcessExtractedFrame_t9BDA228D84FCD6905FA985C6759CD5E40590AA45 ** get_address_of_callback_7() { return &___callback_7; }
	inline void set_callback_7(ProcessExtractedFrame_t9BDA228D84FCD6905FA985C6759CD5E40590AA45 * value)
	{
		___callback_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CresultU3E5__2_8() { return static_cast<int32_t>(offsetof(U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010, ___U3CresultU3E5__2_8)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_U3CresultU3E5__2_8() const { return ___U3CresultU3E5__2_8; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_U3CresultU3E5__2_8() { return &___U3CresultU3E5__2_8; }
	inline void set_U3CresultU3E5__2_8(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___U3CresultU3E5__2_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CresultU3E5__2_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcurrFcU3E5__3_9() { return static_cast<int32_t>(offsetof(U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010, ___U3CcurrFcU3E5__3_9)); }
	inline int32_t get_U3CcurrFcU3E5__3_9() const { return ___U3CcurrFcU3E5__3_9; }
	inline int32_t* get_address_of_U3CcurrFcU3E5__3_9() { return &___U3CcurrFcU3E5__3_9; }
	inline void set_U3CcurrFcU3E5__3_9(int32_t value)
	{
		___U3CcurrFcU3E5__3_9 = value;
	}

	inline static int32_t get_offset_of_U3CiterationsU3E5__4_10() { return static_cast<int32_t>(offsetof(U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010, ___U3CiterationsU3E5__4_10)); }
	inline int32_t get_U3CiterationsU3E5__4_10() const { return ___U3CiterationsU3E5__4_10; }
	inline int32_t* get_address_of_U3CiterationsU3E5__4_10() { return &___U3CiterationsU3E5__4_10; }
	inline void set_U3CiterationsU3E5__4_10(int32_t value)
	{
		___U3CiterationsU3E5__4_10 = value;
	}

	inline static int32_t get_offset_of_U3CmaxIterationsU3E5__5_11() { return static_cast<int32_t>(offsetof(U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010, ___U3CmaxIterationsU3E5__5_11)); }
	inline int32_t get_U3CmaxIterationsU3E5__5_11() const { return ___U3CmaxIterationsU3E5__5_11; }
	inline int32_t* get_address_of_U3CmaxIterationsU3E5__5_11() { return &___U3CmaxIterationsU3E5__5_11; }
	inline void set_U3CmaxIterationsU3E5__5_11(int32_t value)
	{
		___U3CmaxIterationsU3E5__5_11 = value;
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188
struct U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2  : public RuntimeObject
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::<>4__this
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ___U3CU3E4__this_2;
	// UnityEngine.YieldInstruction RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::<wait>5__2
	YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF * ___U3CwaitU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2, ___U3CU3E4__this_2)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CwaitU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2, ___U3CwaitU3E5__2_3)); }
	inline YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF * get_U3CwaitU3E5__2_3() const { return ___U3CwaitU3E5__2_3; }
	inline YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF ** get_address_of_U3CwaitU3E5__2_3() { return &___U3CwaitU3E5__2_3; }
	inline void set_U3CwaitU3E5__2_3(YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF * value)
	{
		___U3CwaitU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CwaitU3E5__2_3), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer/Setup
struct Setup_tD0FAF61885F9D7910834A6774C9282AE0C7D28B6  : public RuntimeObject
{
public:
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/Setup::persistent
	bool ___persistent_0;

public:
	inline static int32_t get_offset_of_persistent_0() { return static_cast<int32_t>(offsetof(Setup_tD0FAF61885F9D7910834A6774C9282AE0C7D28B6, ___persistent_0)); }
	inline bool get_persistent_0() const { return ___persistent_0; }
	inline bool* get_address_of_persistent_0() { return &___persistent_0; }
	inline void set_persistent_0(bool value)
	{
		___persistent_0 = value;
	}
};


// PreloaderController/<DisablePreloaderPanel>d__3
struct U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A  : public RuntimeObject
{
public:
	// System.Int32 PreloaderController/<DisablePreloaderPanel>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PreloaderController/<DisablePreloaderPanel>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// PreloaderController PreloaderController/<DisablePreloaderPanel>d__3::<>4__this
	PreloaderController_tD3EB462D0A9954E9D9785017331EFFC5C9072801 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A, ___U3CU3E4__this_2)); }
	inline PreloaderController_tD3EB462D0A9954E9D9785017331EFFC5C9072801 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PreloaderController_tD3EB462D0A9954E9D9785017331EFFC5C9072801 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PreloaderController_tD3EB462D0A9954E9D9785017331EFFC5C9072801 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Resampler/TimestampedRenderTexture
struct TimestampedRenderTexture_t8DDA74DDD11B5CCACD2920F8CD536801ACD00326  : public RuntimeObject
{
public:
	// UnityEngine.RenderTexture RenderHeads.Media.AVProVideo.Resampler/TimestampedRenderTexture::texture
	RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * ___texture_0;
	// System.Int64 RenderHeads.Media.AVProVideo.Resampler/TimestampedRenderTexture::timestamp
	int64_t ___timestamp_1;
	// System.Boolean RenderHeads.Media.AVProVideo.Resampler/TimestampedRenderTexture::used
	bool ___used_2;

public:
	inline static int32_t get_offset_of_texture_0() { return static_cast<int32_t>(offsetof(TimestampedRenderTexture_t8DDA74DDD11B5CCACD2920F8CD536801ACD00326, ___texture_0)); }
	inline RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * get_texture_0() const { return ___texture_0; }
	inline RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** get_address_of_texture_0() { return &___texture_0; }
	inline void set_texture_0(RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * value)
	{
		___texture_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___texture_0), (void*)value);
	}

	inline static int32_t get_offset_of_timestamp_1() { return static_cast<int32_t>(offsetof(TimestampedRenderTexture_t8DDA74DDD11B5CCACD2920F8CD536801ACD00326, ___timestamp_1)); }
	inline int64_t get_timestamp_1() const { return ___timestamp_1; }
	inline int64_t* get_address_of_timestamp_1() { return &___timestamp_1; }
	inline void set_timestamp_1(int64_t value)
	{
		___timestamp_1 = value;
	}

	inline static int32_t get_offset_of_used_2() { return static_cast<int32_t>(offsetof(TimestampedRenderTexture_t8DDA74DDD11B5CCACD2920F8CD536801ACD00326, ___used_2)); }
	inline bool get_used_2() const { return ___used_2; }
	inline bool* get_address_of_used_2() { return &___used_2; }
	inline void set_used_2(bool value)
	{
		___used_2 = value;
	}
};


// RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23
struct U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9  : public RuntimeObject
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// RenderHeads.Media.AVProVideo.Demos.SimpleController RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::<>4__this
	SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * ___U3CU3E4__this_2;
	// System.Single RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::<fade>5__2
	float ___U3CfadeU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9, ___U3CU3E4__this_2)); }
	inline SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfadeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9, ___U3CfadeU3E5__2_3)); }
	inline float get_U3CfadeU3E5__2_3() const { return ___U3CfadeU3E5__2_3; }
	inline float* get_address_of_U3CfadeU3E5__2_3() { return &___U3CfadeU3E5__2_3; }
	inline void set_U3CfadeU3E5__2_3(float value)
	{
		___U3CfadeU3E5__2_3 = value;
	}
};


// Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16
struct U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817  : public RuntimeObject
{
public:
	// System.Int32 Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Crosstales.OnlineCheck.Demo.SpeedCheck Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16::<>4__this
	SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5 * ___U3CU3E4__this_2;
	// UnityEngine.WaitForSeconds Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16::<waitTime>5__2
	WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * ___U3CwaitTimeU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817, ___U3CU3E4__this_2)); }
	inline SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CwaitTimeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817, ___U3CwaitTimeU3E5__2_3)); }
	inline WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * get_U3CwaitTimeU3E5__2_3() const { return ___U3CwaitTimeU3E5__2_3; }
	inline WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 ** get_address_of_U3CwaitTimeU3E5__2_3() { return &___U3CwaitTimeU3E5__2_3; }
	inline void set_U3CwaitTimeU3E5__2_3(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * value)
	{
		___U3CwaitTimeU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CwaitTimeU3E5__2_3), (void*)value);
	}
};


// VideoPlayerController/<StartVideoPlaying>d__13
struct U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723  : public RuntimeObject
{
public:
	// System.Int32 VideoPlayerController/<StartVideoPlaying>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object VideoPlayerController/<StartVideoPlaying>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// VideoPlayerController VideoPlayerController/<StartVideoPlaying>d__13::<>4__this
	VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723, ___U3CU3E4__this_2)); }
	inline VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`3<RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode>
struct UnityEvent_3_tB461FA83F8F4F36316A85FA0F3B0162F4D8B3C27  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_3_tB461FA83F8F4F36316A85FA0F3B0162F4D8B3C27, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Char
struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.DrivenRectTransformTracker
struct DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2__padding[1];
	};

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Guid
struct Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rngAccess_12), (void*)value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rng_13), (void*)value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fastRng_14), (void*)value);
	}
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int64
struct Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Rect
struct Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:

public:
};


// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	float ___m_Seconds_0;
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=144
struct __StaticArrayInitTypeSizeU3D144_t4B2822F6C46C674B47DD7F528A50B6B894D896D4 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D144_t4B2822F6C46C674B47DD7F528A50B6B894D896D4__padding[144];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24
struct __StaticArrayInitTypeSizeU3D24_t2F23740D8943FC7C06AD3DD80B71D65744F140F2 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t2F23740D8943FC7C06AD3DD80B71D65744F140F2__padding[24];
	};

public:
};


// RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native
struct Native_tFCED11327BFE98E6DC45D28B4BAE14B171A3026A 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Native_tFCED11327BFE98E6DC45D28B4BAE14B171A3026A__padding[1];
	};

public:
};


// UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager/NamedPrefab
struct NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E 
{
public:
	// System.String UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager/NamedPrefab::imageGuid
	String_t* ___imageGuid_0;
	// UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager/NamedPrefab::imagePrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___imagePrefab_1;

public:
	inline static int32_t get_offset_of_imageGuid_0() { return static_cast<int32_t>(offsetof(NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E, ___imageGuid_0)); }
	inline String_t* get_imageGuid_0() const { return ___imageGuid_0; }
	inline String_t** get_address_of_imageGuid_0() { return &___imageGuid_0; }
	inline void set_imageGuid_0(String_t* value)
	{
		___imageGuid_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___imageGuid_0), (void*)value);
	}

	inline static int32_t get_offset_of_imagePrefab_1() { return static_cast<int32_t>(offsetof(NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E, ___imagePrefab_1)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_imagePrefab_1() const { return ___imagePrefab_1; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_imagePrefab_1() { return &___imagePrefab_1; }
	inline void set_imagePrefab_1(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___imagePrefab_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___imagePrefab_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager/NamedPrefab
struct NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E_marshaled_pinvoke
{
	char* ___imageGuid_0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___imagePrefab_1;
};
// Native definition for COM marshalling of UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager/NamedPrefab
struct NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E_marshaled_com
{
	Il2CppChar* ___imageGuid_0;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___imagePrefab_1;
};

// RenderHeads.Media.AVProVideo.Stream/Chunk
struct Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996 
{
public:
	// System.String RenderHeads.Media.AVProVideo.Stream/Chunk::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of RenderHeads.Media.AVProVideo.Stream/Chunk
struct Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshaled_pinvoke
{
	char* ___name_0;
};
// Native definition for COM marshalling of RenderHeads.Media.AVProVideo.Stream/Chunk
struct Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshaled_com
{
	Il2CppChar* ___name_0;
};

// RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader
struct HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED 
{
public:
	// System.String RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader::header
	String_t* ___header_0;
	// System.String RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader
struct HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED_marshaled_pinvoke
{
	char* ___header_0;
	char* ___value_1;
};
// Native definition for COM marshalling of RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader
struct HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED_marshaled_com
{
	Il2CppChar* ___header_0;
	Il2CppChar* ___value_1;
};

// RenderHeads.Media.AVProVideo.AlphaPacking
struct AlphaPacking_t08A0996E0F883B0FE7596288A98D9F20B516EB6B 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.AlphaPacking::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AlphaPacking_t08A0996E0F883B0FE7596288A98D9F20B516EB6B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86, ___m_completeCallback_1)); }
	inline Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_completeCallback_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};

// RenderHeads.Media.AVProVideo.Audio360ChannelMode
struct Audio360ChannelMode_tA3DDB0D2213452F185F4BE7B093BE0F0B1097A45 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.Audio360ChannelMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Audio360ChannelMode_tA3DDB0D2213452F185F4BE7B093BE0F0B1097A45, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.CertificateHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.UI.ColorBlock
struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};

struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields
{
public:
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___defaultColorBlock_7;

public:
	inline static int32_t get_offset_of_defaultColorBlock_7() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields, ___defaultColorBlock_7)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_defaultColorBlock_7() const { return ___defaultColorBlock_7; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_defaultColorBlock_7() { return &___defaultColorBlock_7; }
	inline void set_defaultColorBlock_7(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___defaultColorBlock_7 = value;
	}
};


// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// RenderHeads.Media.AVProVideo.ErrorCode
struct ErrorCode_t9F73777E9EF3C8D7239F71CE06BA8DBEB6383398 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.ErrorCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ErrorCode_t9F73777E9EF3C8D7239F71CE06BA8DBEB6383398, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// RenderHeads.Media.AVProVideo.FileFormat
struct FileFormat_t4CEE033FE31E39D4F7FD6F2677459BA02EBB64D6 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.FileFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileFormat_t4CEE033FE31E39D4F7FD6F2677459BA02EBB64D6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.FilterMode
struct FilterMode_tE90A08FD96A142C761463D65E524BCDBFEEE3D19 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterMode_tE90A08FD96A142C761463D65E524BCDBFEEE3D19, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayerEvent
struct MediaPlayerEvent_t0E890CDB48C2168A48E68F40D0E63D1B3C4A0107  : public UnityEvent_3_tB461FA83F8F4F36316A85FA0F3B0162F4D8B3C27
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Events.UnityAction`3<RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode>> RenderHeads.Media.AVProVideo.MediaPlayerEvent::_listeners
	List_1_t3C68D72B7485781E67A15171BF3DE6D27003CBF3 * ____listeners_4;

public:
	inline static int32_t get_offset_of__listeners_4() { return static_cast<int32_t>(offsetof(MediaPlayerEvent_t0E890CDB48C2168A48E68F40D0E63D1B3C4A0107, ____listeners_4)); }
	inline List_1_t3C68D72B7485781E67A15171BF3DE6D27003CBF3 * get__listeners_4() const { return ____listeners_4; }
	inline List_1_t3C68D72B7485781E67A15171BF3DE6D27003CBF3 ** get_address_of__listeners_4() { return &____listeners_4; }
	inline void set__listeners_4(List_1_t3C68D72B7485781E67A15171BF3DE6D27003CBF3 * value)
	{
		____listeners_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____listeners_4), (void*)value);
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// RenderHeads.Media.AVProVideo.Orientation
struct Orientation_t6E8B49E8A499351CF3CC58460727BA682B8BDEE5 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.Orientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Orientation_t6E8B49E8A499351CF3CC58460727BA682B8BDEE5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ScaleMode
struct ScaleMode_t2D41D186D047D8156791981072D8E7F8759ABB49 
{
public:
	// System.Int32 UnityEngine.ScaleMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScaleMode_t2D41D186D047D8156791981072D8E7F8759ABB49, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.StereoPacking
struct StereoPacking_tF6748DDF6CC9D716FF61BF864D2E86B6AC666417 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.StereoPacking::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StereoPacking_tF6748DDF6CC9D716FF61BF864D2E86B6AC666417, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TextureFormat
struct TextureFormat_tBED5388A0445FE978F97B41D247275B036407932 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_tBED5388A0445FE978F97B41D247275B036407932, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TextureWrapMode
struct TextureWrapMode_t86DDA8206E4AA784A1218D0DE3C5F6826D7549EB 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureWrapMode_t86DDA8206E4AA784A1218D0DE3C5F6826D7549EB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Networking.UploadHandler
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UploadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// RenderHeads.Media.AVProVideo.VideoMapping
struct VideoMapping_t2907CFE2514E5F2A5EF241F1E896897D0C5A1B97 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.VideoMapping::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoMapping_t2907CFE2514E5F2A5EF241F1E896897D0C5A1B97, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.Android/VideoApi
struct VideoApi_t201FD31C8CC9D7BBFEEADD3E3F410D31A0ED4D11 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.Android/VideoApi::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoApi_t201FD31C8CC9D7BBFEEADD3E3F410D31A0ED4D11, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.AudioOutput/AudioOutputMode
struct AudioOutputMode_t5A9924CE725C742FF5EFCA7661760494A8BF476A 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.AudioOutput/AudioOutputMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AudioOutputMode_t5A9924CE725C742FF5EFCA7661760494A8BF476A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// CameraRotator/RotateMethod
struct RotateMethod_tA20D3650834BF1949780248591BD5AAA4D348972 
{
public:
	// System.Int32 CameraRotator/RotateMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotateMethod_tA20D3650834BF1949780248591BD5AAA4D348972, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.CubemapCube/Layout
struct Layout_t82CE5B7FE64133A185935E2165FBC71989CA9621 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.CubemapCube/Layout::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Layout_t82CE5B7FE64133A185935E2165FBC71989CA9621, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARFoundation.Samples.DynamicPrefab/State
struct State_tF1E5036791B824A9712B112D0ECFBC2935F1E8AD 
{
public:
	// System.Int32 UnityEngine.XR.ARFoundation.Samples.DynamicPrefab/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_tF1E5036791B824A9712B112D0ECFBC2935F1E8AD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/FillMethod
struct FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/Type
struct Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation
struct FileLocation_tF1B24F1135C98C08748305C198F011AE743E3848 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileLocation_tF1B24F1135C98C08748305C198F011AE743E3848, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType
struct EventType_tB2FE909C749F1B861A530F105A083945CB226BBF 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventType_tB2FE909C749F1B861A530F105A083945CB226BBF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Navigation/Mode
struct Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.Demos.PlaybackSync/State
struct State_t7FFEAADD0FEA4C45402967A4CC39F4A810EE3F18 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.PlaybackSync/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t7FFEAADD0FEA4C45402967A4CC39F4A810EE3F18, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/PlaylistLoopMode
struct PlaylistLoopMode_tBAA9F2A40182000269D0AB873FAD6EE9FE421C20 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/PlaylistLoopMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlaylistLoopMode_tBAA9F2A40182000269D0AB873FAD6EE9FE421C20, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/ProgressMode
struct ProgressMode_t201CA66A118D4479DFF0161E5905D4218DD9C3B5 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/ProgressMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProgressMode_t201CA66A118D4479DFF0161E5905D4218DD9C3B5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/StartMode
struct StartMode_tC03471371A7D93B5C29E918C49C1FEB010956C34 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/StartMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StartMode_tC03471371A7D93B5C29E918C49C1FEB010956C34, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Transition
struct Transition_tF4587614ED989BEDD70DA556EB3656E1A1516889 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_tF4587614ED989BEDD70DA556EB3656E1A1516889, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.Resampler/ResampleMode
struct ResampleMode_tFFFEFF22DDDB52DFAD53C9B32593A99B79756008 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.Resampler/ResampleMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ResampleMode_tFFFEFF22DDDB52DFAD53C9B32593A99B79756008, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable/Transition
struct Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Slider/Direction
struct Direction_tFC329DCFF9844C052301C90100CA0F5FA9C65961 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_tFC329DCFF9844C052301C90100CA0F5FA9C65961, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.StreamParser/StreamType
struct StreamType_t159A79DF51AF8FAC98081FFEFAF2512478946FA0 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.StreamParser/StreamType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamType_t159A79DF51AF8FAC98081FFEFAF2512478946FA0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.StreamParserEvent/EventType
struct EventType_tDAA5213D8EA92CE89CF479DA69AE4601C0FDC145 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.StreamParserEvent/EventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventType_tDAA5213D8EA92CE89CF479DA69AE4601C0FDC145, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.WebGL/ExternalLibrary
struct ExternalLibrary_tAE358BA3C284C9FBD2F68FEA378B0435AD37CE27 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.WebGL/ExternalLibrary::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExternalLibrary_tAE358BA3C284C9FBD2F68FEA378B0435AD37CE27, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.Windows/VideoApi
struct VideoApi_t6C615ADD96323BF98E193811D09159CE9201F809 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.Windows/VideoApi::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoApi_t6C615ADD96323BF98E193811D09159CE9201F809, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native/AVPPluginEvent
struct AVPPluginEvent_tA8BE23671B87EC820C7432BF4A2F5D89B1AF9015 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native/AVPPluginEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AVPPluginEvent_tA8BE23671B87EC820C7432BF4A2F5D89B1AF9015, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple/AudioMode
struct AudioMode_t865D2CC5B34D3F031D43BF1F1ABE3ED6EC1EE1D2 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple/AudioMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AudioMode_t865D2CC5B34D3F031D43BF1F1ABE3ED6EC1EE1D2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/ParseJSONHeadersState
struct ParseJSONHeadersState_t5CD965BF2A3699A3262EF753199E93A7DA771CEB 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/ParseJSONHeadersState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParseJSONHeadersState_t5CD965BF2A3699A3262EF753199E93A7DA771CEB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing/Preset
struct Preset_t32906DE31F4ED8317EF9EDCDF5572C33C70942B9 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing/Preset::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Preset_t32906DE31F4ED8317EF9EDCDF5572C33C70942B9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_WrapAround_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_WrapAround_1)); }
	inline bool get_m_WrapAround_1() const { return ___m_WrapAround_1; }
	inline bool* get_address_of_m_WrapAround_1() { return &___m_WrapAround_1; }
	inline void set_m_WrapAround_1(bool value)
	{
		___m_WrapAround_1 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_2() const { return ___m_SelectOnUp_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_2() { return &___m_SelectOnUp_2; }
	inline void set_m_SelectOnUp_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_3() const { return ___m_SelectOnDown_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_3() { return &___m_SelectOnDown_3; }
	inline void set_m_SelectOnDown_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_4() const { return ___m_SelectOnLeft_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_4() { return &___m_SelectOnLeft_4; }
	inline void set_m_SelectOnLeft_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_5() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_5)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_5() const { return ___m_SelectOnRight_5; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_5() { return &___m_SelectOnRight_5; }
	inline void set_m_SelectOnRight_5(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};

// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UnityWebRequest::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::m_DownloadHandler
	DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * ___m_DownloadHandler_1;
	// UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::m_UploadHandler
	UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA * ___m_UploadHandler_2;
	// UnityEngine.Networking.CertificateHandler UnityEngine.Networking.UnityWebRequest::m_CertificateHandler
	CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E * ___m_CertificateHandler_3;
	// System.Uri UnityEngine.Networking.UnityWebRequest::m_Uri
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___m_Uri_4;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeCertificateHandlerOnDispose>k__BackingField
	bool ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeDownloadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeUploadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_DownloadHandler_1() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_DownloadHandler_1)); }
	inline DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * get_m_DownloadHandler_1() const { return ___m_DownloadHandler_1; }
	inline DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB ** get_address_of_m_DownloadHandler_1() { return &___m_DownloadHandler_1; }
	inline void set_m_DownloadHandler_1(DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * value)
	{
		___m_DownloadHandler_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DownloadHandler_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_UploadHandler_2() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_UploadHandler_2)); }
	inline UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA * get_m_UploadHandler_2() const { return ___m_UploadHandler_2; }
	inline UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA ** get_address_of_m_UploadHandler_2() { return &___m_UploadHandler_2; }
	inline void set_m_UploadHandler_2(UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA * value)
	{
		___m_UploadHandler_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UploadHandler_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_CertificateHandler_3() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_CertificateHandler_3)); }
	inline CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E * get_m_CertificateHandler_3() const { return ___m_CertificateHandler_3; }
	inline CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E ** get_address_of_m_CertificateHandler_3() { return &___m_CertificateHandler_3; }
	inline void set_m_CertificateHandler_3(CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E * value)
	{
		___m_CertificateHandler_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CertificateHandler_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uri_4() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_Uri_4)); }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * get_m_Uri_4() const { return ___m_Uri_4; }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 ** get_address_of_m_Uri_4() { return &___m_Uri_4; }
	inline void set_m_Uri_4(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * value)
	{
		___m_Uri_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uri_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5)); }
	inline bool get_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5() const { return ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5() { return &___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5; }
	inline void set_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5(bool value)
	{
		___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6)); }
	inline bool get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6() const { return ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6() { return &___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6; }
	inline void set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6(bool value)
	{
		___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7)); }
	inline bool get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7() const { return ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7() { return &___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7; }
	inline void set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7(bool value)
	{
		___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_pinvoke ___m_DownloadHandler_1;
	UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_pinvoke ___m_UploadHandler_2;
	CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_pinvoke ___m_CertificateHandler_3;
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_com
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_com* ___m_DownloadHandler_1;
	UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_com* ___m_UploadHandler_2;
	CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_com* ___m_CertificateHandler_3;
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};

// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396  : public AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAsyncOperation::<webRequest>k__BackingField
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * ___U3CwebRequestU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CwebRequestU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396, ___U3CwebRequestU3Ek__BackingField_2)); }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * get_U3CwebRequestU3Ek__BackingField_2() const { return ___U3CwebRequestU3Ek__BackingField_2; }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E ** get_address_of_U3CwebRequestU3Ek__BackingField_2() { return &___U3CwebRequestU3Ek__BackingField_2; }
	inline void set_U3CwebRequestU3Ek__BackingField_2(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * value)
	{
		___U3CwebRequestU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CwebRequestU3Ek__BackingField_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396_marshaled_pinvoke : public AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86_marshaled_pinvoke
{
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_pinvoke* ___U3CwebRequestU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396_marshaled_com : public AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86_marshaled_com
{
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_com* ___U3CwebRequestU3Ek__BackingField_2;
};

// RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166
struct U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB  : public RuntimeObject
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::url
	String_t* ___url_2;
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::<>4__this
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ___U3CU3E4__this_3;
	// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::fileLocation
	int32_t ___fileLocation_4;
	// System.String RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::filePath
	String_t* ___filePath_5;
	// UnityEngine.Networking.UnityWebRequest RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::<www>5__2
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * ___U3CwwwU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___url_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB, ___U3CU3E4__this_3)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_fileLocation_4() { return static_cast<int32_t>(offsetof(U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB, ___fileLocation_4)); }
	inline int32_t get_fileLocation_4() const { return ___fileLocation_4; }
	inline int32_t* get_address_of_fileLocation_4() { return &___fileLocation_4; }
	inline void set_fileLocation_4(int32_t value)
	{
		___fileLocation_4 = value;
	}

	inline static int32_t get_offset_of_filePath_5() { return static_cast<int32_t>(offsetof(U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB, ___filePath_5)); }
	inline String_t* get_filePath_5() const { return ___filePath_5; }
	inline String_t** get_address_of_filePath_5() { return &___filePath_5; }
	inline void set_filePath_5(String_t* value)
	{
		___filePath_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filePath_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB, ___U3CwwwU3E5__2_6)); }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * get_U3CwwwU3E5__2_6() const { return ___U3CwwwU3E5__2_6; }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E ** get_address_of_U3CwwwU3E5__2_6() { return &___U3CwwwU3E5__2_6; }
	inline void set_U3CwwwU3E5__2_6(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * value)
	{
		___U3CwwwU3E5__2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CwwwU3E5__2_6), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions
struct PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F  : public RuntimeObject
{
public:
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::overridePath
	bool ___overridePath_0;
	// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::pathLocation
	int32_t ___pathLocation_1;
	// System.String RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::path
	String_t* ___path_2;

public:
	inline static int32_t get_offset_of_overridePath_0() { return static_cast<int32_t>(offsetof(PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F, ___overridePath_0)); }
	inline bool get_overridePath_0() const { return ___overridePath_0; }
	inline bool* get_address_of_overridePath_0() { return &___overridePath_0; }
	inline void set_overridePath_0(bool value)
	{
		___overridePath_0 = value;
	}

	inline static int32_t get_offset_of_pathLocation_1() { return static_cast<int32_t>(offsetof(PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F, ___pathLocation_1)); }
	inline int32_t get_pathLocation_1() const { return ___pathLocation_1; }
	inline int32_t* get_address_of_pathLocation_1() { return &___pathLocation_1; }
	inline void set_pathLocation_1(int32_t value)
	{
		___pathLocation_1 = value;
	}

	inline static int32_t get_offset_of_path_2() { return static_cast<int32_t>(offsetof(PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F, ___path_2)); }
	inline String_t* get_path_2() const { return ___path_2; }
	inline String_t** get_address_of_path_2() { return &___path_2; }
	inline void set_path_2(String_t* value)
	{
		___path_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_2), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem
struct MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF  : public RuntimeObject
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem::fileLocation
	int32_t ___fileLocation_0;
	// System.String RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem::filePath
	String_t* ___filePath_1;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem::loop
	bool ___loop_2;
	// RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/StartMode RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem::startMode
	int32_t ___startMode_3;
	// RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/ProgressMode RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem::progressMode
	int32_t ___progressMode_4;
	// System.Single RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem::progressTimeSeconds
	float ___progressTimeSeconds_5;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem::autoPlay
	bool ___autoPlay_6;
	// RenderHeads.Media.AVProVideo.StereoPacking RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem::stereoPacking
	int32_t ___stereoPacking_7;
	// RenderHeads.Media.AVProVideo.AlphaPacking RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem::alphaPacking
	int32_t ___alphaPacking_8;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem::isOverrideTransition
	bool ___isOverrideTransition_9;
	// RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Transition RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem::overrideTransition
	int32_t ___overrideTransition_10;
	// System.Single RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem::overrideTransitionDuration
	float ___overrideTransitionDuration_11;
	// RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem::overrideTransitionEasing
	Easing_tDD586F922397A60A9F213AA64B7AFC753FE0686D * ___overrideTransitionEasing_12;

public:
	inline static int32_t get_offset_of_fileLocation_0() { return static_cast<int32_t>(offsetof(MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF, ___fileLocation_0)); }
	inline int32_t get_fileLocation_0() const { return ___fileLocation_0; }
	inline int32_t* get_address_of_fileLocation_0() { return &___fileLocation_0; }
	inline void set_fileLocation_0(int32_t value)
	{
		___fileLocation_0 = value;
	}

	inline static int32_t get_offset_of_filePath_1() { return static_cast<int32_t>(offsetof(MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF, ___filePath_1)); }
	inline String_t* get_filePath_1() const { return ___filePath_1; }
	inline String_t** get_address_of_filePath_1() { return &___filePath_1; }
	inline void set_filePath_1(String_t* value)
	{
		___filePath_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filePath_1), (void*)value);
	}

	inline static int32_t get_offset_of_loop_2() { return static_cast<int32_t>(offsetof(MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF, ___loop_2)); }
	inline bool get_loop_2() const { return ___loop_2; }
	inline bool* get_address_of_loop_2() { return &___loop_2; }
	inline void set_loop_2(bool value)
	{
		___loop_2 = value;
	}

	inline static int32_t get_offset_of_startMode_3() { return static_cast<int32_t>(offsetof(MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF, ___startMode_3)); }
	inline int32_t get_startMode_3() const { return ___startMode_3; }
	inline int32_t* get_address_of_startMode_3() { return &___startMode_3; }
	inline void set_startMode_3(int32_t value)
	{
		___startMode_3 = value;
	}

	inline static int32_t get_offset_of_progressMode_4() { return static_cast<int32_t>(offsetof(MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF, ___progressMode_4)); }
	inline int32_t get_progressMode_4() const { return ___progressMode_4; }
	inline int32_t* get_address_of_progressMode_4() { return &___progressMode_4; }
	inline void set_progressMode_4(int32_t value)
	{
		___progressMode_4 = value;
	}

	inline static int32_t get_offset_of_progressTimeSeconds_5() { return static_cast<int32_t>(offsetof(MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF, ___progressTimeSeconds_5)); }
	inline float get_progressTimeSeconds_5() const { return ___progressTimeSeconds_5; }
	inline float* get_address_of_progressTimeSeconds_5() { return &___progressTimeSeconds_5; }
	inline void set_progressTimeSeconds_5(float value)
	{
		___progressTimeSeconds_5 = value;
	}

	inline static int32_t get_offset_of_autoPlay_6() { return static_cast<int32_t>(offsetof(MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF, ___autoPlay_6)); }
	inline bool get_autoPlay_6() const { return ___autoPlay_6; }
	inline bool* get_address_of_autoPlay_6() { return &___autoPlay_6; }
	inline void set_autoPlay_6(bool value)
	{
		___autoPlay_6 = value;
	}

	inline static int32_t get_offset_of_stereoPacking_7() { return static_cast<int32_t>(offsetof(MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF, ___stereoPacking_7)); }
	inline int32_t get_stereoPacking_7() const { return ___stereoPacking_7; }
	inline int32_t* get_address_of_stereoPacking_7() { return &___stereoPacking_7; }
	inline void set_stereoPacking_7(int32_t value)
	{
		___stereoPacking_7 = value;
	}

	inline static int32_t get_offset_of_alphaPacking_8() { return static_cast<int32_t>(offsetof(MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF, ___alphaPacking_8)); }
	inline int32_t get_alphaPacking_8() const { return ___alphaPacking_8; }
	inline int32_t* get_address_of_alphaPacking_8() { return &___alphaPacking_8; }
	inline void set_alphaPacking_8(int32_t value)
	{
		___alphaPacking_8 = value;
	}

	inline static int32_t get_offset_of_isOverrideTransition_9() { return static_cast<int32_t>(offsetof(MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF, ___isOverrideTransition_9)); }
	inline bool get_isOverrideTransition_9() const { return ___isOverrideTransition_9; }
	inline bool* get_address_of_isOverrideTransition_9() { return &___isOverrideTransition_9; }
	inline void set_isOverrideTransition_9(bool value)
	{
		___isOverrideTransition_9 = value;
	}

	inline static int32_t get_offset_of_overrideTransition_10() { return static_cast<int32_t>(offsetof(MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF, ___overrideTransition_10)); }
	inline int32_t get_overrideTransition_10() const { return ___overrideTransition_10; }
	inline int32_t* get_address_of_overrideTransition_10() { return &___overrideTransition_10; }
	inline void set_overrideTransition_10(int32_t value)
	{
		___overrideTransition_10 = value;
	}

	inline static int32_t get_offset_of_overrideTransitionDuration_11() { return static_cast<int32_t>(offsetof(MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF, ___overrideTransitionDuration_11)); }
	inline float get_overrideTransitionDuration_11() const { return ___overrideTransitionDuration_11; }
	inline float* get_address_of_overrideTransitionDuration_11() { return &___overrideTransitionDuration_11; }
	inline void set_overrideTransitionDuration_11(float value)
	{
		___overrideTransitionDuration_11 = value;
	}

	inline static int32_t get_offset_of_overrideTransitionEasing_12() { return static_cast<int32_t>(offsetof(MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF, ___overrideTransitionEasing_12)); }
	inline Easing_tDD586F922397A60A9F213AA64B7AFC753FE0686D * get_overrideTransitionEasing_12() const { return ___overrideTransitionEasing_12; }
	inline Easing_tDD586F922397A60A9F213AA64B7AFC753FE0686D ** get_address_of_overrideTransitionEasing_12() { return &___overrideTransitionEasing_12; }
	inline void set_overrideTransitionEasing_12(Easing_tDD586F922397A60A9F213AA64B7AFC753FE0686D * value)
	{
		___overrideTransitionEasing_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___overrideTransitionEasing_12), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing
struct Easing_tDD586F922397A60A9F213AA64B7AFC753FE0686D  : public RuntimeObject
{
public:
	// RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing/Preset RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::preset
	int32_t ___preset_0;

public:
	inline static int32_t get_offset_of_preset_0() { return static_cast<int32_t>(offsetof(Easing_tDD586F922397A60A9F213AA64B7AFC753FE0686D, ___preset_0)); }
	inline int32_t get_preset_0() const { return ___preset_0; }
	inline int32_t* get_address_of_preset_0() { return &___preset_0; }
	inline void set_preset_0(int32_t value)
	{
		___preset_0 = value;
	}
};


// System.Func`2<System.Single,System.Single>
struct Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`3<RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode>
struct UnityAction_3_t8232B65BAFB61715E1BA6DC746E10C91EB537ADB  : public MulticastDelegate_t
{
public:

public:
};


// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF  : public Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid
struct OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393  : public PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F
{
public:
	// RenderHeads.Media.AVProVideo.Android/VideoApi RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::videoApi
	int32_t ___videoApi_3;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::useFastOesPath
	bool ___useFastOesPath_4;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::showPosterFrame
	bool ___showPosterFrame_5;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::enableAudio360
	bool ___enableAudio360_6;
	// RenderHeads.Media.AVProVideo.Audio360ChannelMode RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::audio360ChannelMode
	int32_t ___audio360ChannelMode_7;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::preferSoftwareDecoder
	bool ___preferSoftwareDecoder_8;
	// System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader> RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::httpHeaders
	List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * ___httpHeaders_9;
	// System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::httpHeaderJson
	String_t* ___httpHeaderJson_10;
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::fileOffset
	int32_t ___fileOffset_11;

public:
	inline static int32_t get_offset_of_videoApi_3() { return static_cast<int32_t>(offsetof(OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393, ___videoApi_3)); }
	inline int32_t get_videoApi_3() const { return ___videoApi_3; }
	inline int32_t* get_address_of_videoApi_3() { return &___videoApi_3; }
	inline void set_videoApi_3(int32_t value)
	{
		___videoApi_3 = value;
	}

	inline static int32_t get_offset_of_useFastOesPath_4() { return static_cast<int32_t>(offsetof(OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393, ___useFastOesPath_4)); }
	inline bool get_useFastOesPath_4() const { return ___useFastOesPath_4; }
	inline bool* get_address_of_useFastOesPath_4() { return &___useFastOesPath_4; }
	inline void set_useFastOesPath_4(bool value)
	{
		___useFastOesPath_4 = value;
	}

	inline static int32_t get_offset_of_showPosterFrame_5() { return static_cast<int32_t>(offsetof(OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393, ___showPosterFrame_5)); }
	inline bool get_showPosterFrame_5() const { return ___showPosterFrame_5; }
	inline bool* get_address_of_showPosterFrame_5() { return &___showPosterFrame_5; }
	inline void set_showPosterFrame_5(bool value)
	{
		___showPosterFrame_5 = value;
	}

	inline static int32_t get_offset_of_enableAudio360_6() { return static_cast<int32_t>(offsetof(OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393, ___enableAudio360_6)); }
	inline bool get_enableAudio360_6() const { return ___enableAudio360_6; }
	inline bool* get_address_of_enableAudio360_6() { return &___enableAudio360_6; }
	inline void set_enableAudio360_6(bool value)
	{
		___enableAudio360_6 = value;
	}

	inline static int32_t get_offset_of_audio360ChannelMode_7() { return static_cast<int32_t>(offsetof(OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393, ___audio360ChannelMode_7)); }
	inline int32_t get_audio360ChannelMode_7() const { return ___audio360ChannelMode_7; }
	inline int32_t* get_address_of_audio360ChannelMode_7() { return &___audio360ChannelMode_7; }
	inline void set_audio360ChannelMode_7(int32_t value)
	{
		___audio360ChannelMode_7 = value;
	}

	inline static int32_t get_offset_of_preferSoftwareDecoder_8() { return static_cast<int32_t>(offsetof(OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393, ___preferSoftwareDecoder_8)); }
	inline bool get_preferSoftwareDecoder_8() const { return ___preferSoftwareDecoder_8; }
	inline bool* get_address_of_preferSoftwareDecoder_8() { return &___preferSoftwareDecoder_8; }
	inline void set_preferSoftwareDecoder_8(bool value)
	{
		___preferSoftwareDecoder_8 = value;
	}

	inline static int32_t get_offset_of_httpHeaders_9() { return static_cast<int32_t>(offsetof(OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393, ___httpHeaders_9)); }
	inline List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * get_httpHeaders_9() const { return ___httpHeaders_9; }
	inline List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 ** get_address_of_httpHeaders_9() { return &___httpHeaders_9; }
	inline void set_httpHeaders_9(List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * value)
	{
		___httpHeaders_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___httpHeaders_9), (void*)value);
	}

	inline static int32_t get_offset_of_httpHeaderJson_10() { return static_cast<int32_t>(offsetof(OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393, ___httpHeaderJson_10)); }
	inline String_t* get_httpHeaderJson_10() const { return ___httpHeaderJson_10; }
	inline String_t** get_address_of_httpHeaderJson_10() { return &___httpHeaderJson_10; }
	inline void set_httpHeaderJson_10(String_t* value)
	{
		___httpHeaderJson_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___httpHeaderJson_10), (void*)value);
	}

	inline static int32_t get_offset_of_fileOffset_11() { return static_cast<int32_t>(offsetof(OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393, ___fileOffset_11)); }
	inline int32_t get_fileOffset_11() const { return ___fileOffset_11; }
	inline int32_t* get_address_of_fileOffset_11() { return &___fileOffset_11; }
	inline void set_fileOffset_11(int32_t value)
	{
		___fileOffset_11 = value;
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple
struct OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E  : public PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple/AudioMode RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::audioMode
	int32_t ___audioMode_3;
	// System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader> RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::httpHeaders
	List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * ___httpHeaders_4;
	// System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::httpHeaderJson
	String_t* ___httpHeaderJson_5;
	// System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::keyServerURLOverride
	String_t* ___keyServerURLOverride_6;
	// System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::keyServerAuthToken
	String_t* ___keyServerAuthToken_7;
	// System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::base64EncodedKeyBlob
	String_t* ___base64EncodedKeyBlob_8;

public:
	inline static int32_t get_offset_of_audioMode_3() { return static_cast<int32_t>(offsetof(OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E, ___audioMode_3)); }
	inline int32_t get_audioMode_3() const { return ___audioMode_3; }
	inline int32_t* get_address_of_audioMode_3() { return &___audioMode_3; }
	inline void set_audioMode_3(int32_t value)
	{
		___audioMode_3 = value;
	}

	inline static int32_t get_offset_of_httpHeaders_4() { return static_cast<int32_t>(offsetof(OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E, ___httpHeaders_4)); }
	inline List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * get_httpHeaders_4() const { return ___httpHeaders_4; }
	inline List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 ** get_address_of_httpHeaders_4() { return &___httpHeaders_4; }
	inline void set_httpHeaders_4(List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * value)
	{
		___httpHeaders_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___httpHeaders_4), (void*)value);
	}

	inline static int32_t get_offset_of_httpHeaderJson_5() { return static_cast<int32_t>(offsetof(OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E, ___httpHeaderJson_5)); }
	inline String_t* get_httpHeaderJson_5() const { return ___httpHeaderJson_5; }
	inline String_t** get_address_of_httpHeaderJson_5() { return &___httpHeaderJson_5; }
	inline void set_httpHeaderJson_5(String_t* value)
	{
		___httpHeaderJson_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___httpHeaderJson_5), (void*)value);
	}

	inline static int32_t get_offset_of_keyServerURLOverride_6() { return static_cast<int32_t>(offsetof(OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E, ___keyServerURLOverride_6)); }
	inline String_t* get_keyServerURLOverride_6() const { return ___keyServerURLOverride_6; }
	inline String_t** get_address_of_keyServerURLOverride_6() { return &___keyServerURLOverride_6; }
	inline void set_keyServerURLOverride_6(String_t* value)
	{
		___keyServerURLOverride_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keyServerURLOverride_6), (void*)value);
	}

	inline static int32_t get_offset_of_keyServerAuthToken_7() { return static_cast<int32_t>(offsetof(OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E, ___keyServerAuthToken_7)); }
	inline String_t* get_keyServerAuthToken_7() const { return ___keyServerAuthToken_7; }
	inline String_t** get_address_of_keyServerAuthToken_7() { return &___keyServerAuthToken_7; }
	inline void set_keyServerAuthToken_7(String_t* value)
	{
		___keyServerAuthToken_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keyServerAuthToken_7), (void*)value);
	}

	inline static int32_t get_offset_of_base64EncodedKeyBlob_8() { return static_cast<int32_t>(offsetof(OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E, ___base64EncodedKeyBlob_8)); }
	inline String_t* get_base64EncodedKeyBlob_8() const { return ___base64EncodedKeyBlob_8; }
	inline String_t** get_address_of_base64EncodedKeyBlob_8() { return &___base64EncodedKeyBlob_8; }
	inline void set_base64EncodedKeyBlob_8(String_t* value)
	{
		___base64EncodedKeyBlob_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___base64EncodedKeyBlob_8), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsPS4
struct OptionsPS4_t43756CE0642DB94AA9A8E30A58524CDC7BD082D1  : public PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F
{
public:

public:
};


// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWebGL
struct OptionsWebGL_tA7A6C5EB6C224751A8B5DF0B99DE0C1A3DD15B05  : public PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F
{
public:
	// RenderHeads.Media.AVProVideo.WebGL/ExternalLibrary RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWebGL::externalLibrary
	int32_t ___externalLibrary_3;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWebGL::useTextureMips
	bool ___useTextureMips_4;

public:
	inline static int32_t get_offset_of_externalLibrary_3() { return static_cast<int32_t>(offsetof(OptionsWebGL_tA7A6C5EB6C224751A8B5DF0B99DE0C1A3DD15B05, ___externalLibrary_3)); }
	inline int32_t get_externalLibrary_3() const { return ___externalLibrary_3; }
	inline int32_t* get_address_of_externalLibrary_3() { return &___externalLibrary_3; }
	inline void set_externalLibrary_3(int32_t value)
	{
		___externalLibrary_3 = value;
	}

	inline static int32_t get_offset_of_useTextureMips_4() { return static_cast<int32_t>(offsetof(OptionsWebGL_tA7A6C5EB6C224751A8B5DF0B99DE0C1A3DD15B05, ___useTextureMips_4)); }
	inline bool get_useTextureMips_4() const { return ___useTextureMips_4; }
	inline bool* get_address_of_useTextureMips_4() { return &___useTextureMips_4; }
	inline void set_useTextureMips_4(bool value)
	{
		___useTextureMips_4 = value;
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows
struct OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1  : public PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F
{
public:
	// RenderHeads.Media.AVProVideo.Windows/VideoApi RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::videoApi
	int32_t ___videoApi_3;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::useHardwareDecoding
	bool ___useHardwareDecoding_4;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::useUnityAudio
	bool ___useUnityAudio_5;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::forceAudioResample
	bool ___forceAudioResample_6;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::useTextureMips
	bool ___useTextureMips_7;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::hintAlphaChannel
	bool ___hintAlphaChannel_8;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::useLowLatency
	bool ___useLowLatency_9;
	// System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::forceAudioOutputDeviceName
	String_t* ___forceAudioOutputDeviceName_10;
	// System.Collections.Generic.List`1<System.String> RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::preferredFilters
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___preferredFilters_11;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::enableAudio360
	bool ___enableAudio360_12;
	// RenderHeads.Media.AVProVideo.Audio360ChannelMode RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::audio360ChannelMode
	int32_t ___audio360ChannelMode_13;

public:
	inline static int32_t get_offset_of_videoApi_3() { return static_cast<int32_t>(offsetof(OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1, ___videoApi_3)); }
	inline int32_t get_videoApi_3() const { return ___videoApi_3; }
	inline int32_t* get_address_of_videoApi_3() { return &___videoApi_3; }
	inline void set_videoApi_3(int32_t value)
	{
		___videoApi_3 = value;
	}

	inline static int32_t get_offset_of_useHardwareDecoding_4() { return static_cast<int32_t>(offsetof(OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1, ___useHardwareDecoding_4)); }
	inline bool get_useHardwareDecoding_4() const { return ___useHardwareDecoding_4; }
	inline bool* get_address_of_useHardwareDecoding_4() { return &___useHardwareDecoding_4; }
	inline void set_useHardwareDecoding_4(bool value)
	{
		___useHardwareDecoding_4 = value;
	}

	inline static int32_t get_offset_of_useUnityAudio_5() { return static_cast<int32_t>(offsetof(OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1, ___useUnityAudio_5)); }
	inline bool get_useUnityAudio_5() const { return ___useUnityAudio_5; }
	inline bool* get_address_of_useUnityAudio_5() { return &___useUnityAudio_5; }
	inline void set_useUnityAudio_5(bool value)
	{
		___useUnityAudio_5 = value;
	}

	inline static int32_t get_offset_of_forceAudioResample_6() { return static_cast<int32_t>(offsetof(OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1, ___forceAudioResample_6)); }
	inline bool get_forceAudioResample_6() const { return ___forceAudioResample_6; }
	inline bool* get_address_of_forceAudioResample_6() { return &___forceAudioResample_6; }
	inline void set_forceAudioResample_6(bool value)
	{
		___forceAudioResample_6 = value;
	}

	inline static int32_t get_offset_of_useTextureMips_7() { return static_cast<int32_t>(offsetof(OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1, ___useTextureMips_7)); }
	inline bool get_useTextureMips_7() const { return ___useTextureMips_7; }
	inline bool* get_address_of_useTextureMips_7() { return &___useTextureMips_7; }
	inline void set_useTextureMips_7(bool value)
	{
		___useTextureMips_7 = value;
	}

	inline static int32_t get_offset_of_hintAlphaChannel_8() { return static_cast<int32_t>(offsetof(OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1, ___hintAlphaChannel_8)); }
	inline bool get_hintAlphaChannel_8() const { return ___hintAlphaChannel_8; }
	inline bool* get_address_of_hintAlphaChannel_8() { return &___hintAlphaChannel_8; }
	inline void set_hintAlphaChannel_8(bool value)
	{
		___hintAlphaChannel_8 = value;
	}

	inline static int32_t get_offset_of_useLowLatency_9() { return static_cast<int32_t>(offsetof(OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1, ___useLowLatency_9)); }
	inline bool get_useLowLatency_9() const { return ___useLowLatency_9; }
	inline bool* get_address_of_useLowLatency_9() { return &___useLowLatency_9; }
	inline void set_useLowLatency_9(bool value)
	{
		___useLowLatency_9 = value;
	}

	inline static int32_t get_offset_of_forceAudioOutputDeviceName_10() { return static_cast<int32_t>(offsetof(OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1, ___forceAudioOutputDeviceName_10)); }
	inline String_t* get_forceAudioOutputDeviceName_10() const { return ___forceAudioOutputDeviceName_10; }
	inline String_t** get_address_of_forceAudioOutputDeviceName_10() { return &___forceAudioOutputDeviceName_10; }
	inline void set_forceAudioOutputDeviceName_10(String_t* value)
	{
		___forceAudioOutputDeviceName_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___forceAudioOutputDeviceName_10), (void*)value);
	}

	inline static int32_t get_offset_of_preferredFilters_11() { return static_cast<int32_t>(offsetof(OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1, ___preferredFilters_11)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_preferredFilters_11() const { return ___preferredFilters_11; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_preferredFilters_11() { return &___preferredFilters_11; }
	inline void set_preferredFilters_11(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___preferredFilters_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___preferredFilters_11), (void*)value);
	}

	inline static int32_t get_offset_of_enableAudio360_12() { return static_cast<int32_t>(offsetof(OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1, ___enableAudio360_12)); }
	inline bool get_enableAudio360_12() const { return ___enableAudio360_12; }
	inline bool* get_address_of_enableAudio360_12() { return &___enableAudio360_12; }
	inline void set_enableAudio360_12(bool value)
	{
		___enableAudio360_12 = value;
	}

	inline static int32_t get_offset_of_audio360ChannelMode_13() { return static_cast<int32_t>(offsetof(OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1, ___audio360ChannelMode_13)); }
	inline int32_t get_audio360ChannelMode_13() const { return ___audio360ChannelMode_13; }
	inline int32_t* get_address_of_audio360ChannelMode_13() { return &___audio360ChannelMode_13; }
	inline void set_audio360ChannelMode_13(int32_t value)
	{
		___audio360ChannelMode_13 = value;
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsPhone
struct OptionsWindowsPhone_t5B5890235D85585DFCBEC0D280F332C274B6E63C  : public PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F
{
public:
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsPhone::useHardwareDecoding
	bool ___useHardwareDecoding_3;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsPhone::useUnityAudio
	bool ___useUnityAudio_4;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsPhone::forceAudioResample
	bool ___forceAudioResample_5;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsPhone::useTextureMips
	bool ___useTextureMips_6;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsPhone::useLowLatency
	bool ___useLowLatency_7;

public:
	inline static int32_t get_offset_of_useHardwareDecoding_3() { return static_cast<int32_t>(offsetof(OptionsWindowsPhone_t5B5890235D85585DFCBEC0D280F332C274B6E63C, ___useHardwareDecoding_3)); }
	inline bool get_useHardwareDecoding_3() const { return ___useHardwareDecoding_3; }
	inline bool* get_address_of_useHardwareDecoding_3() { return &___useHardwareDecoding_3; }
	inline void set_useHardwareDecoding_3(bool value)
	{
		___useHardwareDecoding_3 = value;
	}

	inline static int32_t get_offset_of_useUnityAudio_4() { return static_cast<int32_t>(offsetof(OptionsWindowsPhone_t5B5890235D85585DFCBEC0D280F332C274B6E63C, ___useUnityAudio_4)); }
	inline bool get_useUnityAudio_4() const { return ___useUnityAudio_4; }
	inline bool* get_address_of_useUnityAudio_4() { return &___useUnityAudio_4; }
	inline void set_useUnityAudio_4(bool value)
	{
		___useUnityAudio_4 = value;
	}

	inline static int32_t get_offset_of_forceAudioResample_5() { return static_cast<int32_t>(offsetof(OptionsWindowsPhone_t5B5890235D85585DFCBEC0D280F332C274B6E63C, ___forceAudioResample_5)); }
	inline bool get_forceAudioResample_5() const { return ___forceAudioResample_5; }
	inline bool* get_address_of_forceAudioResample_5() { return &___forceAudioResample_5; }
	inline void set_forceAudioResample_5(bool value)
	{
		___forceAudioResample_5 = value;
	}

	inline static int32_t get_offset_of_useTextureMips_6() { return static_cast<int32_t>(offsetof(OptionsWindowsPhone_t5B5890235D85585DFCBEC0D280F332C274B6E63C, ___useTextureMips_6)); }
	inline bool get_useTextureMips_6() const { return ___useTextureMips_6; }
	inline bool* get_address_of_useTextureMips_6() { return &___useTextureMips_6; }
	inline void set_useTextureMips_6(bool value)
	{
		___useTextureMips_6 = value;
	}

	inline static int32_t get_offset_of_useLowLatency_7() { return static_cast<int32_t>(offsetof(OptionsWindowsPhone_t5B5890235D85585DFCBEC0D280F332C274B6E63C, ___useLowLatency_7)); }
	inline bool get_useLowLatency_7() const { return ___useLowLatency_7; }
	inline bool* get_address_of_useLowLatency_7() { return &___useLowLatency_7; }
	inline void set_useLowLatency_7(bool value)
	{
		___useLowLatency_7 = value;
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsUWP
struct OptionsWindowsUWP_tBA5BE5D51A12AA6360DF87691342C5EE7B8F7F66  : public PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F
{
public:
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsUWP::useHardwareDecoding
	bool ___useHardwareDecoding_3;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsUWP::useUnityAudio
	bool ___useUnityAudio_4;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsUWP::forceAudioResample
	bool ___forceAudioResample_5;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsUWP::useTextureMips
	bool ___useTextureMips_6;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsUWP::useLowLatency
	bool ___useLowLatency_7;

public:
	inline static int32_t get_offset_of_useHardwareDecoding_3() { return static_cast<int32_t>(offsetof(OptionsWindowsUWP_tBA5BE5D51A12AA6360DF87691342C5EE7B8F7F66, ___useHardwareDecoding_3)); }
	inline bool get_useHardwareDecoding_3() const { return ___useHardwareDecoding_3; }
	inline bool* get_address_of_useHardwareDecoding_3() { return &___useHardwareDecoding_3; }
	inline void set_useHardwareDecoding_3(bool value)
	{
		___useHardwareDecoding_3 = value;
	}

	inline static int32_t get_offset_of_useUnityAudio_4() { return static_cast<int32_t>(offsetof(OptionsWindowsUWP_tBA5BE5D51A12AA6360DF87691342C5EE7B8F7F66, ___useUnityAudio_4)); }
	inline bool get_useUnityAudio_4() const { return ___useUnityAudio_4; }
	inline bool* get_address_of_useUnityAudio_4() { return &___useUnityAudio_4; }
	inline void set_useUnityAudio_4(bool value)
	{
		___useUnityAudio_4 = value;
	}

	inline static int32_t get_offset_of_forceAudioResample_5() { return static_cast<int32_t>(offsetof(OptionsWindowsUWP_tBA5BE5D51A12AA6360DF87691342C5EE7B8F7F66, ___forceAudioResample_5)); }
	inline bool get_forceAudioResample_5() const { return ___forceAudioResample_5; }
	inline bool* get_address_of_forceAudioResample_5() { return &___forceAudioResample_5; }
	inline void set_forceAudioResample_5(bool value)
	{
		___forceAudioResample_5 = value;
	}

	inline static int32_t get_offset_of_useTextureMips_6() { return static_cast<int32_t>(offsetof(OptionsWindowsUWP_tBA5BE5D51A12AA6360DF87691342C5EE7B8F7F66, ___useTextureMips_6)); }
	inline bool get_useTextureMips_6() const { return ___useTextureMips_6; }
	inline bool* get_address_of_useTextureMips_6() { return &___useTextureMips_6; }
	inline void set_useTextureMips_6(bool value)
	{
		___useTextureMips_6 = value;
	}

	inline static int32_t get_offset_of_useLowLatency_7() { return static_cast<int32_t>(offsetof(OptionsWindowsUWP_tBA5BE5D51A12AA6360DF87691342C5EE7B8F7F66, ___useLowLatency_7)); }
	inline bool get_useLowLatency_7() const { return ___useLowLatency_7; }
	inline bool* get_address_of_useLowLatency_7() { return &___useLowLatency_7; }
	inline void set_useLowLatency_7(bool value)
	{
		___useLowLatency_7 = value;
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame
struct ProcessExtractedFrame_t9BDA228D84FCD6905FA985C6759CD5E40590AA45  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072  : public Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1
{
public:

public:
};

struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsIOS
struct OptionsIOS_t868D1748A3E07299A108C1E60F97753CA38954A5  : public OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E
{
public:
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsIOS::useYpCbCr420Textures
	bool ___useYpCbCr420Textures_9;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsIOS::resumePlaybackOnAudioSessionRouteChange
	bool ___resumePlaybackOnAudioSessionRouteChange_10;

public:
	inline static int32_t get_offset_of_useYpCbCr420Textures_9() { return static_cast<int32_t>(offsetof(OptionsIOS_t868D1748A3E07299A108C1E60F97753CA38954A5, ___useYpCbCr420Textures_9)); }
	inline bool get_useYpCbCr420Textures_9() const { return ___useYpCbCr420Textures_9; }
	inline bool* get_address_of_useYpCbCr420Textures_9() { return &___useYpCbCr420Textures_9; }
	inline void set_useYpCbCr420Textures_9(bool value)
	{
		___useYpCbCr420Textures_9 = value;
	}

	inline static int32_t get_offset_of_resumePlaybackOnAudioSessionRouteChange_10() { return static_cast<int32_t>(offsetof(OptionsIOS_t868D1748A3E07299A108C1E60F97753CA38954A5, ___resumePlaybackOnAudioSessionRouteChange_10)); }
	inline bool get_resumePlaybackOnAudioSessionRouteChange_10() const { return ___resumePlaybackOnAudioSessionRouteChange_10; }
	inline bool* get_address_of_resumePlaybackOnAudioSessionRouteChange_10() { return &___resumePlaybackOnAudioSessionRouteChange_10; }
	inline void set_resumePlaybackOnAudioSessionRouteChange_10(bool value)
	{
		___resumePlaybackOnAudioSessionRouteChange_10 = value;
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsMacOSX
struct OptionsMacOSX_t1B3B40C8ABFE68B24D80EA87E10EFA9C1972A2B3  : public OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E
{
public:

public:
};


// RenderHeads.Media.AVProVideo.DisplayIMGUI
struct DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.DisplayIMGUI::_mediaPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mediaPlayer_6;
	// System.Boolean RenderHeads.Media.AVProVideo.DisplayIMGUI::_displayInEditor
	bool ____displayInEditor_7;
	// UnityEngine.ScaleMode RenderHeads.Media.AVProVideo.DisplayIMGUI::_scaleMode
	int32_t ____scaleMode_8;
	// UnityEngine.Color RenderHeads.Media.AVProVideo.DisplayIMGUI::_color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ____color_9;
	// System.Boolean RenderHeads.Media.AVProVideo.DisplayIMGUI::_alphaBlend
	bool ____alphaBlend_10;
	// System.Boolean RenderHeads.Media.AVProVideo.DisplayIMGUI::_useDepth
	bool ____useDepth_11;
	// System.Int32 RenderHeads.Media.AVProVideo.DisplayIMGUI::_depth
	int32_t ____depth_12;
	// System.Boolean RenderHeads.Media.AVProVideo.DisplayIMGUI::_fullScreen
	bool ____fullScreen_13;
	// System.Single RenderHeads.Media.AVProVideo.DisplayIMGUI::_x
	float ____x_14;
	// System.Single RenderHeads.Media.AVProVideo.DisplayIMGUI::_y
	float ____y_15;
	// System.Single RenderHeads.Media.AVProVideo.DisplayIMGUI::_width
	float ____width_16;
	// System.Single RenderHeads.Media.AVProVideo.DisplayIMGUI::_height
	float ____height_17;
	// UnityEngine.Material RenderHeads.Media.AVProVideo.DisplayIMGUI::_material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ____material_24;

public:
	inline static int32_t get_offset_of__mediaPlayer_6() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04, ____mediaPlayer_6)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mediaPlayer_6() const { return ____mediaPlayer_6; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mediaPlayer_6() { return &____mediaPlayer_6; }
	inline void set__mediaPlayer_6(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mediaPlayer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_6), (void*)value);
	}

	inline static int32_t get_offset_of__displayInEditor_7() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04, ____displayInEditor_7)); }
	inline bool get__displayInEditor_7() const { return ____displayInEditor_7; }
	inline bool* get_address_of__displayInEditor_7() { return &____displayInEditor_7; }
	inline void set__displayInEditor_7(bool value)
	{
		____displayInEditor_7 = value;
	}

	inline static int32_t get_offset_of__scaleMode_8() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04, ____scaleMode_8)); }
	inline int32_t get__scaleMode_8() const { return ____scaleMode_8; }
	inline int32_t* get_address_of__scaleMode_8() { return &____scaleMode_8; }
	inline void set__scaleMode_8(int32_t value)
	{
		____scaleMode_8 = value;
	}

	inline static int32_t get_offset_of__color_9() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04, ____color_9)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get__color_9() const { return ____color_9; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of__color_9() { return &____color_9; }
	inline void set__color_9(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		____color_9 = value;
	}

	inline static int32_t get_offset_of__alphaBlend_10() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04, ____alphaBlend_10)); }
	inline bool get__alphaBlend_10() const { return ____alphaBlend_10; }
	inline bool* get_address_of__alphaBlend_10() { return &____alphaBlend_10; }
	inline void set__alphaBlend_10(bool value)
	{
		____alphaBlend_10 = value;
	}

	inline static int32_t get_offset_of__useDepth_11() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04, ____useDepth_11)); }
	inline bool get__useDepth_11() const { return ____useDepth_11; }
	inline bool* get_address_of__useDepth_11() { return &____useDepth_11; }
	inline void set__useDepth_11(bool value)
	{
		____useDepth_11 = value;
	}

	inline static int32_t get_offset_of__depth_12() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04, ____depth_12)); }
	inline int32_t get__depth_12() const { return ____depth_12; }
	inline int32_t* get_address_of__depth_12() { return &____depth_12; }
	inline void set__depth_12(int32_t value)
	{
		____depth_12 = value;
	}

	inline static int32_t get_offset_of__fullScreen_13() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04, ____fullScreen_13)); }
	inline bool get__fullScreen_13() const { return ____fullScreen_13; }
	inline bool* get_address_of__fullScreen_13() { return &____fullScreen_13; }
	inline void set__fullScreen_13(bool value)
	{
		____fullScreen_13 = value;
	}

	inline static int32_t get_offset_of__x_14() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04, ____x_14)); }
	inline float get__x_14() const { return ____x_14; }
	inline float* get_address_of__x_14() { return &____x_14; }
	inline void set__x_14(float value)
	{
		____x_14 = value;
	}

	inline static int32_t get_offset_of__y_15() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04, ____y_15)); }
	inline float get__y_15() const { return ____y_15; }
	inline float* get_address_of__y_15() { return &____y_15; }
	inline void set__y_15(float value)
	{
		____y_15 = value;
	}

	inline static int32_t get_offset_of__width_16() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04, ____width_16)); }
	inline float get__width_16() const { return ____width_16; }
	inline float* get_address_of__width_16() { return &____width_16; }
	inline void set__width_16(float value)
	{
		____width_16 = value;
	}

	inline static int32_t get_offset_of__height_17() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04, ____height_17)); }
	inline float get__height_17() const { return ____height_17; }
	inline float* get_address_of__height_17() { return &____height_17; }
	inline void set__height_17(float value)
	{
		____height_17 = value;
	}

	inline static int32_t get_offset_of__material_24() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04, ____material_24)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get__material_24() const { return ____material_24; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of__material_24() { return &____material_24; }
	inline void set__material_24(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		____material_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____material_24), (void*)value);
	}
};

struct DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_StaticFields
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.DisplayIMGUI::_propAlphaPack
	int32_t ____propAlphaPack_18;
	// System.Int32 RenderHeads.Media.AVProVideo.DisplayIMGUI::_propVertScale
	int32_t ____propVertScale_19;
	// System.Int32 RenderHeads.Media.AVProVideo.DisplayIMGUI::_propApplyGamma
	int32_t ____propApplyGamma_20;
	// System.Int32 RenderHeads.Media.AVProVideo.DisplayIMGUI::_propChromaTex
	int32_t ____propChromaTex_21;
	// System.Int32 RenderHeads.Media.AVProVideo.DisplayIMGUI::_propYpCbCrTransform
	int32_t ____propYpCbCrTransform_22;
	// UnityEngine.Shader RenderHeads.Media.AVProVideo.DisplayIMGUI::_shaderAlphaPacking
	Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * ____shaderAlphaPacking_23;

public:
	inline static int32_t get_offset_of__propAlphaPack_18() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_StaticFields, ____propAlphaPack_18)); }
	inline int32_t get__propAlphaPack_18() const { return ____propAlphaPack_18; }
	inline int32_t* get_address_of__propAlphaPack_18() { return &____propAlphaPack_18; }
	inline void set__propAlphaPack_18(int32_t value)
	{
		____propAlphaPack_18 = value;
	}

	inline static int32_t get_offset_of__propVertScale_19() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_StaticFields, ____propVertScale_19)); }
	inline int32_t get__propVertScale_19() const { return ____propVertScale_19; }
	inline int32_t* get_address_of__propVertScale_19() { return &____propVertScale_19; }
	inline void set__propVertScale_19(int32_t value)
	{
		____propVertScale_19 = value;
	}

	inline static int32_t get_offset_of__propApplyGamma_20() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_StaticFields, ____propApplyGamma_20)); }
	inline int32_t get__propApplyGamma_20() const { return ____propApplyGamma_20; }
	inline int32_t* get_address_of__propApplyGamma_20() { return &____propApplyGamma_20; }
	inline void set__propApplyGamma_20(int32_t value)
	{
		____propApplyGamma_20 = value;
	}

	inline static int32_t get_offset_of__propChromaTex_21() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_StaticFields, ____propChromaTex_21)); }
	inline int32_t get__propChromaTex_21() const { return ____propChromaTex_21; }
	inline int32_t* get_address_of__propChromaTex_21() { return &____propChromaTex_21; }
	inline void set__propChromaTex_21(int32_t value)
	{
		____propChromaTex_21 = value;
	}

	inline static int32_t get_offset_of__propYpCbCrTransform_22() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_StaticFields, ____propYpCbCrTransform_22)); }
	inline int32_t get__propYpCbCrTransform_22() const { return ____propYpCbCrTransform_22; }
	inline int32_t* get_address_of__propYpCbCrTransform_22() { return &____propYpCbCrTransform_22; }
	inline void set__propYpCbCrTransform_22(int32_t value)
	{
		____propYpCbCrTransform_22 = value;
	}

	inline static int32_t get_offset_of__shaderAlphaPacking_23() { return static_cast<int32_t>(offsetof(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04_StaticFields, ____shaderAlphaPacking_23)); }
	inline Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * get__shaderAlphaPacking_23() const { return ____shaderAlphaPacking_23; }
	inline Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 ** get_address_of__shaderAlphaPacking_23() { return &____shaderAlphaPacking_23; }
	inline void set__shaderAlphaPacking_23(Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * value)
	{
		____shaderAlphaPacking_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____shaderAlphaPacking_23), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer
struct MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation RenderHeads.Media.AVProVideo.MediaPlayer::m_VideoLocation
	int32_t ___m_VideoLocation_4;
	// System.String RenderHeads.Media.AVProVideo.MediaPlayer::m_VideoPath
	String_t* ___m_VideoPath_5;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_AutoOpen
	bool ___m_AutoOpen_6;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_AutoStart
	bool ___m_AutoStart_7;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_Loop
	bool ___m_Loop_8;
	// System.Single RenderHeads.Media.AVProVideo.MediaPlayer::m_Volume
	float ___m_Volume_9;
	// System.Single RenderHeads.Media.AVProVideo.MediaPlayer::m_Balance
	float ___m_Balance_10;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_Muted
	bool ___m_Muted_11;
	// System.Single RenderHeads.Media.AVProVideo.MediaPlayer::m_PlaybackRate
	float ___m_PlaybackRate_12;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_Resample
	bool ___m_Resample_13;
	// RenderHeads.Media.AVProVideo.Resampler/ResampleMode RenderHeads.Media.AVProVideo.MediaPlayer::m_ResampleMode
	int32_t ___m_ResampleMode_14;
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer::m_ResampleBufferSize
	int32_t ___m_ResampleBufferSize_15;
	// RenderHeads.Media.AVProVideo.Resampler RenderHeads.Media.AVProVideo.MediaPlayer::m_Resampler
	Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876 * ___m_Resampler_16;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_Persistent
	bool ___m_Persistent_17;
	// RenderHeads.Media.AVProVideo.VideoMapping RenderHeads.Media.AVProVideo.MediaPlayer::m_videoMapping
	int32_t ___m_videoMapping_18;
	// RenderHeads.Media.AVProVideo.StereoPacking RenderHeads.Media.AVProVideo.MediaPlayer::m_StereoPacking
	int32_t ___m_StereoPacking_19;
	// RenderHeads.Media.AVProVideo.AlphaPacking RenderHeads.Media.AVProVideo.MediaPlayer::m_AlphaPacking
	int32_t ___m_AlphaPacking_20;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_DisplayDebugStereoColorTint
	bool ___m_DisplayDebugStereoColorTint_21;
	// UnityEngine.FilterMode RenderHeads.Media.AVProVideo.MediaPlayer::m_FilterMode
	int32_t ___m_FilterMode_22;
	// UnityEngine.TextureWrapMode RenderHeads.Media.AVProVideo.MediaPlayer::m_WrapMode
	int32_t ___m_WrapMode_23;
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer::m_AnisoLevel
	int32_t ___m_AnisoLevel_24;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_LoadSubtitles
	bool ___m_LoadSubtitles_25;
	// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation RenderHeads.Media.AVProVideo.MediaPlayer::m_SubtitleLocation
	int32_t ___m_SubtitleLocation_26;
	// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation RenderHeads.Media.AVProVideo.MediaPlayer::m_queueSubtitleLocation
	int32_t ___m_queueSubtitleLocation_27;
	// System.String RenderHeads.Media.AVProVideo.MediaPlayer::m_SubtitlePath
	String_t* ___m_SubtitlePath_28;
	// System.String RenderHeads.Media.AVProVideo.MediaPlayer::m_queueSubtitlePath
	String_t* ___m_queueSubtitlePath_29;
	// UnityEngine.Coroutine RenderHeads.Media.AVProVideo.MediaPlayer::m_loadSubtitlesRoutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ___m_loadSubtitlesRoutine_30;
	// UnityEngine.Transform RenderHeads.Media.AVProVideo.MediaPlayer::m_AudioHeadTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_AudioHeadTransform_31;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_AudioFocusEnabled
	bool ___m_AudioFocusEnabled_32;
	// UnityEngine.Transform RenderHeads.Media.AVProVideo.MediaPlayer::m_AudioFocusTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_AudioFocusTransform_33;
	// System.Single RenderHeads.Media.AVProVideo.MediaPlayer::m_AudioFocusWidthDegrees
	float ___m_AudioFocusWidthDegrees_34;
	// System.Single RenderHeads.Media.AVProVideo.MediaPlayer::m_AudioFocusOffLevelDB
	float ___m_AudioFocusOffLevelDB_35;
	// RenderHeads.Media.AVProVideo.MediaPlayerEvent RenderHeads.Media.AVProVideo.MediaPlayer::m_events
	MediaPlayerEvent_t0E890CDB48C2168A48E68F40D0E63D1B3C4A0107 * ___m_events_36;
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer::m_eventMask
	int32_t ___m_eventMask_37;
	// RenderHeads.Media.AVProVideo.FileFormat RenderHeads.Media.AVProVideo.MediaPlayer::m_forceFileFormat
	int32_t ___m_forceFileFormat_38;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::_pauseMediaOnAppPause
	bool ____pauseMediaOnAppPause_39;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::_playMediaOnAppUnpause
	bool ____playMediaOnAppUnpause_40;
	// RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.MediaPlayer::m_Control
	RuntimeObject* ___m_Control_41;
	// RenderHeads.Media.AVProVideo.IMediaProducer RenderHeads.Media.AVProVideo.MediaPlayer::m_Texture
	RuntimeObject* ___m_Texture_42;
	// RenderHeads.Media.AVProVideo.IMediaInfo RenderHeads.Media.AVProVideo.MediaPlayer::m_Info
	RuntimeObject* ___m_Info_43;
	// RenderHeads.Media.AVProVideo.IMediaPlayer RenderHeads.Media.AVProVideo.MediaPlayer::m_Player
	RuntimeObject* ___m_Player_44;
	// RenderHeads.Media.AVProVideo.IMediaSubtitles RenderHeads.Media.AVProVideo.MediaPlayer::m_Subtitles
	RuntimeObject* ___m_Subtitles_45;
	// System.IDisposable RenderHeads.Media.AVProVideo.MediaPlayer::m_Dispose
	RuntimeObject* ___m_Dispose_46;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_VideoOpened
	bool ___m_VideoOpened_47;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_AutoStartTriggered
	bool ___m_AutoStartTriggered_48;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_WasPlayingOnPause
	bool ___m_WasPlayingOnPause_49;
	// UnityEngine.Coroutine RenderHeads.Media.AVProVideo.MediaPlayer::_renderingCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ____renderingCoroutine_50;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_EventFired_ReadyToPlay
	bool ___m_EventFired_ReadyToPlay_52;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_EventFired_Started
	bool ___m_EventFired_Started_53;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_EventFired_FirstFrameReady
	bool ___m_EventFired_FirstFrameReady_54;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_EventFired_FinishedPlaying
	bool ___m_EventFired_FinishedPlaying_55;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_EventFired_MetaDataReady
	bool ___m_EventFired_MetaDataReady_56;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_EventState_PlaybackStalled
	bool ___m_EventState_PlaybackStalled_57;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_EventState_PlaybackBuffering
	bool ___m_EventState_PlaybackBuffering_58;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_EventState_PlaybackSeeking
	bool ___m_EventState_PlaybackSeeking_59;
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer::m_EventState_PreviousWidth
	int32_t ___m_EventState_PreviousWidth_60;
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer::m_EventState_PreviousHeight
	int32_t ___m_EventState_PreviousHeight_61;
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer::m_previousSubtitleIndex
	int32_t ___m_previousSubtitleIndex_62;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_FinishedFrameOpenCheck
	bool ___m_FinishedFrameOpenCheck_64;
	// System.UInt32 RenderHeads.Media.AVProVideo.MediaPlayer::m_sourceSampleRate
	uint32_t ___m_sourceSampleRate_65;
	// System.UInt32 RenderHeads.Media.AVProVideo.MediaPlayer::m_sourceChannels
	uint32_t ___m_sourceChannels_66;
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::m_manuallySetAudioSourceProperties
	bool ___m_manuallySetAudioSourceProperties_67;
	// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows RenderHeads.Media.AVProVideo.MediaPlayer::_optionsWindows
	OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1 * ____optionsWindows_68;
	// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsMacOSX RenderHeads.Media.AVProVideo.MediaPlayer::_optionsMacOSX
	OptionsMacOSX_t1B3B40C8ABFE68B24D80EA87E10EFA9C1972A2B3 * ____optionsMacOSX_69;
	// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsIOS RenderHeads.Media.AVProVideo.MediaPlayer::_optionsIOS
	OptionsIOS_t868D1748A3E07299A108C1E60F97753CA38954A5 * ____optionsIOS_70;
	// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsTVOS RenderHeads.Media.AVProVideo.MediaPlayer::_optionsTVOS
	OptionsTVOS_tFDA4033A636E29A9005B4D9092A32298FFE8911B * ____optionsTVOS_71;
	// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid RenderHeads.Media.AVProVideo.MediaPlayer::_optionsAndroid
	OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393 * ____optionsAndroid_72;
	// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsPhone RenderHeads.Media.AVProVideo.MediaPlayer::_optionsWindowsPhone
	OptionsWindowsPhone_t5B5890235D85585DFCBEC0D280F332C274B6E63C * ____optionsWindowsPhone_73;
	// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsUWP RenderHeads.Media.AVProVideo.MediaPlayer::_optionsWindowsUWP
	OptionsWindowsUWP_tBA5BE5D51A12AA6360DF87691342C5EE7B8F7F66 * ____optionsWindowsUWP_74;
	// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWebGL RenderHeads.Media.AVProVideo.MediaPlayer::_optionsWebGL
	OptionsWebGL_tA7A6C5EB6C224751A8B5DF0B99DE0C1A3DD15B05 * ____optionsWebGL_75;
	// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsPS4 RenderHeads.Media.AVProVideo.MediaPlayer::_optionsPS4
	OptionsPS4_t43756CE0642DB94AA9A8E30A58524CDC7BD082D1 * ____optionsPS4_76;

public:
	inline static int32_t get_offset_of_m_VideoLocation_4() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_VideoLocation_4)); }
	inline int32_t get_m_VideoLocation_4() const { return ___m_VideoLocation_4; }
	inline int32_t* get_address_of_m_VideoLocation_4() { return &___m_VideoLocation_4; }
	inline void set_m_VideoLocation_4(int32_t value)
	{
		___m_VideoLocation_4 = value;
	}

	inline static int32_t get_offset_of_m_VideoPath_5() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_VideoPath_5)); }
	inline String_t* get_m_VideoPath_5() const { return ___m_VideoPath_5; }
	inline String_t** get_address_of_m_VideoPath_5() { return &___m_VideoPath_5; }
	inline void set_m_VideoPath_5(String_t* value)
	{
		___m_VideoPath_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_VideoPath_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_AutoOpen_6() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_AutoOpen_6)); }
	inline bool get_m_AutoOpen_6() const { return ___m_AutoOpen_6; }
	inline bool* get_address_of_m_AutoOpen_6() { return &___m_AutoOpen_6; }
	inline void set_m_AutoOpen_6(bool value)
	{
		___m_AutoOpen_6 = value;
	}

	inline static int32_t get_offset_of_m_AutoStart_7() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_AutoStart_7)); }
	inline bool get_m_AutoStart_7() const { return ___m_AutoStart_7; }
	inline bool* get_address_of_m_AutoStart_7() { return &___m_AutoStart_7; }
	inline void set_m_AutoStart_7(bool value)
	{
		___m_AutoStart_7 = value;
	}

	inline static int32_t get_offset_of_m_Loop_8() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_Loop_8)); }
	inline bool get_m_Loop_8() const { return ___m_Loop_8; }
	inline bool* get_address_of_m_Loop_8() { return &___m_Loop_8; }
	inline void set_m_Loop_8(bool value)
	{
		___m_Loop_8 = value;
	}

	inline static int32_t get_offset_of_m_Volume_9() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_Volume_9)); }
	inline float get_m_Volume_9() const { return ___m_Volume_9; }
	inline float* get_address_of_m_Volume_9() { return &___m_Volume_9; }
	inline void set_m_Volume_9(float value)
	{
		___m_Volume_9 = value;
	}

	inline static int32_t get_offset_of_m_Balance_10() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_Balance_10)); }
	inline float get_m_Balance_10() const { return ___m_Balance_10; }
	inline float* get_address_of_m_Balance_10() { return &___m_Balance_10; }
	inline void set_m_Balance_10(float value)
	{
		___m_Balance_10 = value;
	}

	inline static int32_t get_offset_of_m_Muted_11() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_Muted_11)); }
	inline bool get_m_Muted_11() const { return ___m_Muted_11; }
	inline bool* get_address_of_m_Muted_11() { return &___m_Muted_11; }
	inline void set_m_Muted_11(bool value)
	{
		___m_Muted_11 = value;
	}

	inline static int32_t get_offset_of_m_PlaybackRate_12() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_PlaybackRate_12)); }
	inline float get_m_PlaybackRate_12() const { return ___m_PlaybackRate_12; }
	inline float* get_address_of_m_PlaybackRate_12() { return &___m_PlaybackRate_12; }
	inline void set_m_PlaybackRate_12(float value)
	{
		___m_PlaybackRate_12 = value;
	}

	inline static int32_t get_offset_of_m_Resample_13() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_Resample_13)); }
	inline bool get_m_Resample_13() const { return ___m_Resample_13; }
	inline bool* get_address_of_m_Resample_13() { return &___m_Resample_13; }
	inline void set_m_Resample_13(bool value)
	{
		___m_Resample_13 = value;
	}

	inline static int32_t get_offset_of_m_ResampleMode_14() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_ResampleMode_14)); }
	inline int32_t get_m_ResampleMode_14() const { return ___m_ResampleMode_14; }
	inline int32_t* get_address_of_m_ResampleMode_14() { return &___m_ResampleMode_14; }
	inline void set_m_ResampleMode_14(int32_t value)
	{
		___m_ResampleMode_14 = value;
	}

	inline static int32_t get_offset_of_m_ResampleBufferSize_15() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_ResampleBufferSize_15)); }
	inline int32_t get_m_ResampleBufferSize_15() const { return ___m_ResampleBufferSize_15; }
	inline int32_t* get_address_of_m_ResampleBufferSize_15() { return &___m_ResampleBufferSize_15; }
	inline void set_m_ResampleBufferSize_15(int32_t value)
	{
		___m_ResampleBufferSize_15 = value;
	}

	inline static int32_t get_offset_of_m_Resampler_16() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_Resampler_16)); }
	inline Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876 * get_m_Resampler_16() const { return ___m_Resampler_16; }
	inline Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876 ** get_address_of_m_Resampler_16() { return &___m_Resampler_16; }
	inline void set_m_Resampler_16(Resampler_t63EBB9FF62A3C7567E2A247BBF7527E51CB34876 * value)
	{
		___m_Resampler_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Resampler_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_Persistent_17() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_Persistent_17)); }
	inline bool get_m_Persistent_17() const { return ___m_Persistent_17; }
	inline bool* get_address_of_m_Persistent_17() { return &___m_Persistent_17; }
	inline void set_m_Persistent_17(bool value)
	{
		___m_Persistent_17 = value;
	}

	inline static int32_t get_offset_of_m_videoMapping_18() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_videoMapping_18)); }
	inline int32_t get_m_videoMapping_18() const { return ___m_videoMapping_18; }
	inline int32_t* get_address_of_m_videoMapping_18() { return &___m_videoMapping_18; }
	inline void set_m_videoMapping_18(int32_t value)
	{
		___m_videoMapping_18 = value;
	}

	inline static int32_t get_offset_of_m_StereoPacking_19() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_StereoPacking_19)); }
	inline int32_t get_m_StereoPacking_19() const { return ___m_StereoPacking_19; }
	inline int32_t* get_address_of_m_StereoPacking_19() { return &___m_StereoPacking_19; }
	inline void set_m_StereoPacking_19(int32_t value)
	{
		___m_StereoPacking_19 = value;
	}

	inline static int32_t get_offset_of_m_AlphaPacking_20() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_AlphaPacking_20)); }
	inline int32_t get_m_AlphaPacking_20() const { return ___m_AlphaPacking_20; }
	inline int32_t* get_address_of_m_AlphaPacking_20() { return &___m_AlphaPacking_20; }
	inline void set_m_AlphaPacking_20(int32_t value)
	{
		___m_AlphaPacking_20 = value;
	}

	inline static int32_t get_offset_of_m_DisplayDebugStereoColorTint_21() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_DisplayDebugStereoColorTint_21)); }
	inline bool get_m_DisplayDebugStereoColorTint_21() const { return ___m_DisplayDebugStereoColorTint_21; }
	inline bool* get_address_of_m_DisplayDebugStereoColorTint_21() { return &___m_DisplayDebugStereoColorTint_21; }
	inline void set_m_DisplayDebugStereoColorTint_21(bool value)
	{
		___m_DisplayDebugStereoColorTint_21 = value;
	}

	inline static int32_t get_offset_of_m_FilterMode_22() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_FilterMode_22)); }
	inline int32_t get_m_FilterMode_22() const { return ___m_FilterMode_22; }
	inline int32_t* get_address_of_m_FilterMode_22() { return &___m_FilterMode_22; }
	inline void set_m_FilterMode_22(int32_t value)
	{
		___m_FilterMode_22 = value;
	}

	inline static int32_t get_offset_of_m_WrapMode_23() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_WrapMode_23)); }
	inline int32_t get_m_WrapMode_23() const { return ___m_WrapMode_23; }
	inline int32_t* get_address_of_m_WrapMode_23() { return &___m_WrapMode_23; }
	inline void set_m_WrapMode_23(int32_t value)
	{
		___m_WrapMode_23 = value;
	}

	inline static int32_t get_offset_of_m_AnisoLevel_24() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_AnisoLevel_24)); }
	inline int32_t get_m_AnisoLevel_24() const { return ___m_AnisoLevel_24; }
	inline int32_t* get_address_of_m_AnisoLevel_24() { return &___m_AnisoLevel_24; }
	inline void set_m_AnisoLevel_24(int32_t value)
	{
		___m_AnisoLevel_24 = value;
	}

	inline static int32_t get_offset_of_m_LoadSubtitles_25() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_LoadSubtitles_25)); }
	inline bool get_m_LoadSubtitles_25() const { return ___m_LoadSubtitles_25; }
	inline bool* get_address_of_m_LoadSubtitles_25() { return &___m_LoadSubtitles_25; }
	inline void set_m_LoadSubtitles_25(bool value)
	{
		___m_LoadSubtitles_25 = value;
	}

	inline static int32_t get_offset_of_m_SubtitleLocation_26() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_SubtitleLocation_26)); }
	inline int32_t get_m_SubtitleLocation_26() const { return ___m_SubtitleLocation_26; }
	inline int32_t* get_address_of_m_SubtitleLocation_26() { return &___m_SubtitleLocation_26; }
	inline void set_m_SubtitleLocation_26(int32_t value)
	{
		___m_SubtitleLocation_26 = value;
	}

	inline static int32_t get_offset_of_m_queueSubtitleLocation_27() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_queueSubtitleLocation_27)); }
	inline int32_t get_m_queueSubtitleLocation_27() const { return ___m_queueSubtitleLocation_27; }
	inline int32_t* get_address_of_m_queueSubtitleLocation_27() { return &___m_queueSubtitleLocation_27; }
	inline void set_m_queueSubtitleLocation_27(int32_t value)
	{
		___m_queueSubtitleLocation_27 = value;
	}

	inline static int32_t get_offset_of_m_SubtitlePath_28() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_SubtitlePath_28)); }
	inline String_t* get_m_SubtitlePath_28() const { return ___m_SubtitlePath_28; }
	inline String_t** get_address_of_m_SubtitlePath_28() { return &___m_SubtitlePath_28; }
	inline void set_m_SubtitlePath_28(String_t* value)
	{
		___m_SubtitlePath_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SubtitlePath_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_queueSubtitlePath_29() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_queueSubtitlePath_29)); }
	inline String_t* get_m_queueSubtitlePath_29() const { return ___m_queueSubtitlePath_29; }
	inline String_t** get_address_of_m_queueSubtitlePath_29() { return &___m_queueSubtitlePath_29; }
	inline void set_m_queueSubtitlePath_29(String_t* value)
	{
		___m_queueSubtitlePath_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_queueSubtitlePath_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_loadSubtitlesRoutine_30() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_loadSubtitlesRoutine_30)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get_m_loadSubtitlesRoutine_30() const { return ___m_loadSubtitlesRoutine_30; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of_m_loadSubtitlesRoutine_30() { return &___m_loadSubtitlesRoutine_30; }
	inline void set_m_loadSubtitlesRoutine_30(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		___m_loadSubtitlesRoutine_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_loadSubtitlesRoutine_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_AudioHeadTransform_31() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_AudioHeadTransform_31)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_AudioHeadTransform_31() const { return ___m_AudioHeadTransform_31; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_AudioHeadTransform_31() { return &___m_AudioHeadTransform_31; }
	inline void set_m_AudioHeadTransform_31(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_AudioHeadTransform_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AudioHeadTransform_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_AudioFocusEnabled_32() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_AudioFocusEnabled_32)); }
	inline bool get_m_AudioFocusEnabled_32() const { return ___m_AudioFocusEnabled_32; }
	inline bool* get_address_of_m_AudioFocusEnabled_32() { return &___m_AudioFocusEnabled_32; }
	inline void set_m_AudioFocusEnabled_32(bool value)
	{
		___m_AudioFocusEnabled_32 = value;
	}

	inline static int32_t get_offset_of_m_AudioFocusTransform_33() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_AudioFocusTransform_33)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_AudioFocusTransform_33() const { return ___m_AudioFocusTransform_33; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_AudioFocusTransform_33() { return &___m_AudioFocusTransform_33; }
	inline void set_m_AudioFocusTransform_33(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_AudioFocusTransform_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AudioFocusTransform_33), (void*)value);
	}

	inline static int32_t get_offset_of_m_AudioFocusWidthDegrees_34() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_AudioFocusWidthDegrees_34)); }
	inline float get_m_AudioFocusWidthDegrees_34() const { return ___m_AudioFocusWidthDegrees_34; }
	inline float* get_address_of_m_AudioFocusWidthDegrees_34() { return &___m_AudioFocusWidthDegrees_34; }
	inline void set_m_AudioFocusWidthDegrees_34(float value)
	{
		___m_AudioFocusWidthDegrees_34 = value;
	}

	inline static int32_t get_offset_of_m_AudioFocusOffLevelDB_35() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_AudioFocusOffLevelDB_35)); }
	inline float get_m_AudioFocusOffLevelDB_35() const { return ___m_AudioFocusOffLevelDB_35; }
	inline float* get_address_of_m_AudioFocusOffLevelDB_35() { return &___m_AudioFocusOffLevelDB_35; }
	inline void set_m_AudioFocusOffLevelDB_35(float value)
	{
		___m_AudioFocusOffLevelDB_35 = value;
	}

	inline static int32_t get_offset_of_m_events_36() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_events_36)); }
	inline MediaPlayerEvent_t0E890CDB48C2168A48E68F40D0E63D1B3C4A0107 * get_m_events_36() const { return ___m_events_36; }
	inline MediaPlayerEvent_t0E890CDB48C2168A48E68F40D0E63D1B3C4A0107 ** get_address_of_m_events_36() { return &___m_events_36; }
	inline void set_m_events_36(MediaPlayerEvent_t0E890CDB48C2168A48E68F40D0E63D1B3C4A0107 * value)
	{
		___m_events_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_events_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_eventMask_37() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_eventMask_37)); }
	inline int32_t get_m_eventMask_37() const { return ___m_eventMask_37; }
	inline int32_t* get_address_of_m_eventMask_37() { return &___m_eventMask_37; }
	inline void set_m_eventMask_37(int32_t value)
	{
		___m_eventMask_37 = value;
	}

	inline static int32_t get_offset_of_m_forceFileFormat_38() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_forceFileFormat_38)); }
	inline int32_t get_m_forceFileFormat_38() const { return ___m_forceFileFormat_38; }
	inline int32_t* get_address_of_m_forceFileFormat_38() { return &___m_forceFileFormat_38; }
	inline void set_m_forceFileFormat_38(int32_t value)
	{
		___m_forceFileFormat_38 = value;
	}

	inline static int32_t get_offset_of__pauseMediaOnAppPause_39() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ____pauseMediaOnAppPause_39)); }
	inline bool get__pauseMediaOnAppPause_39() const { return ____pauseMediaOnAppPause_39; }
	inline bool* get_address_of__pauseMediaOnAppPause_39() { return &____pauseMediaOnAppPause_39; }
	inline void set__pauseMediaOnAppPause_39(bool value)
	{
		____pauseMediaOnAppPause_39 = value;
	}

	inline static int32_t get_offset_of__playMediaOnAppUnpause_40() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ____playMediaOnAppUnpause_40)); }
	inline bool get__playMediaOnAppUnpause_40() const { return ____playMediaOnAppUnpause_40; }
	inline bool* get_address_of__playMediaOnAppUnpause_40() { return &____playMediaOnAppUnpause_40; }
	inline void set__playMediaOnAppUnpause_40(bool value)
	{
		____playMediaOnAppUnpause_40 = value;
	}

	inline static int32_t get_offset_of_m_Control_41() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_Control_41)); }
	inline RuntimeObject* get_m_Control_41() const { return ___m_Control_41; }
	inline RuntimeObject** get_address_of_m_Control_41() { return &___m_Control_41; }
	inline void set_m_Control_41(RuntimeObject* value)
	{
		___m_Control_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Control_41), (void*)value);
	}

	inline static int32_t get_offset_of_m_Texture_42() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_Texture_42)); }
	inline RuntimeObject* get_m_Texture_42() const { return ___m_Texture_42; }
	inline RuntimeObject** get_address_of_m_Texture_42() { return &___m_Texture_42; }
	inline void set_m_Texture_42(RuntimeObject* value)
	{
		___m_Texture_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Texture_42), (void*)value);
	}

	inline static int32_t get_offset_of_m_Info_43() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_Info_43)); }
	inline RuntimeObject* get_m_Info_43() const { return ___m_Info_43; }
	inline RuntimeObject** get_address_of_m_Info_43() { return &___m_Info_43; }
	inline void set_m_Info_43(RuntimeObject* value)
	{
		___m_Info_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Info_43), (void*)value);
	}

	inline static int32_t get_offset_of_m_Player_44() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_Player_44)); }
	inline RuntimeObject* get_m_Player_44() const { return ___m_Player_44; }
	inline RuntimeObject** get_address_of_m_Player_44() { return &___m_Player_44; }
	inline void set_m_Player_44(RuntimeObject* value)
	{
		___m_Player_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Player_44), (void*)value);
	}

	inline static int32_t get_offset_of_m_Subtitles_45() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_Subtitles_45)); }
	inline RuntimeObject* get_m_Subtitles_45() const { return ___m_Subtitles_45; }
	inline RuntimeObject** get_address_of_m_Subtitles_45() { return &___m_Subtitles_45; }
	inline void set_m_Subtitles_45(RuntimeObject* value)
	{
		___m_Subtitles_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Subtitles_45), (void*)value);
	}

	inline static int32_t get_offset_of_m_Dispose_46() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_Dispose_46)); }
	inline RuntimeObject* get_m_Dispose_46() const { return ___m_Dispose_46; }
	inline RuntimeObject** get_address_of_m_Dispose_46() { return &___m_Dispose_46; }
	inline void set_m_Dispose_46(RuntimeObject* value)
	{
		___m_Dispose_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Dispose_46), (void*)value);
	}

	inline static int32_t get_offset_of_m_VideoOpened_47() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_VideoOpened_47)); }
	inline bool get_m_VideoOpened_47() const { return ___m_VideoOpened_47; }
	inline bool* get_address_of_m_VideoOpened_47() { return &___m_VideoOpened_47; }
	inline void set_m_VideoOpened_47(bool value)
	{
		___m_VideoOpened_47 = value;
	}

	inline static int32_t get_offset_of_m_AutoStartTriggered_48() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_AutoStartTriggered_48)); }
	inline bool get_m_AutoStartTriggered_48() const { return ___m_AutoStartTriggered_48; }
	inline bool* get_address_of_m_AutoStartTriggered_48() { return &___m_AutoStartTriggered_48; }
	inline void set_m_AutoStartTriggered_48(bool value)
	{
		___m_AutoStartTriggered_48 = value;
	}

	inline static int32_t get_offset_of_m_WasPlayingOnPause_49() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_WasPlayingOnPause_49)); }
	inline bool get_m_WasPlayingOnPause_49() const { return ___m_WasPlayingOnPause_49; }
	inline bool* get_address_of_m_WasPlayingOnPause_49() { return &___m_WasPlayingOnPause_49; }
	inline void set_m_WasPlayingOnPause_49(bool value)
	{
		___m_WasPlayingOnPause_49 = value;
	}

	inline static int32_t get_offset_of__renderingCoroutine_50() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ____renderingCoroutine_50)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get__renderingCoroutine_50() const { return ____renderingCoroutine_50; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of__renderingCoroutine_50() { return &____renderingCoroutine_50; }
	inline void set__renderingCoroutine_50(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		____renderingCoroutine_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____renderingCoroutine_50), (void*)value);
	}

	inline static int32_t get_offset_of_m_EventFired_ReadyToPlay_52() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_EventFired_ReadyToPlay_52)); }
	inline bool get_m_EventFired_ReadyToPlay_52() const { return ___m_EventFired_ReadyToPlay_52; }
	inline bool* get_address_of_m_EventFired_ReadyToPlay_52() { return &___m_EventFired_ReadyToPlay_52; }
	inline void set_m_EventFired_ReadyToPlay_52(bool value)
	{
		___m_EventFired_ReadyToPlay_52 = value;
	}

	inline static int32_t get_offset_of_m_EventFired_Started_53() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_EventFired_Started_53)); }
	inline bool get_m_EventFired_Started_53() const { return ___m_EventFired_Started_53; }
	inline bool* get_address_of_m_EventFired_Started_53() { return &___m_EventFired_Started_53; }
	inline void set_m_EventFired_Started_53(bool value)
	{
		___m_EventFired_Started_53 = value;
	}

	inline static int32_t get_offset_of_m_EventFired_FirstFrameReady_54() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_EventFired_FirstFrameReady_54)); }
	inline bool get_m_EventFired_FirstFrameReady_54() const { return ___m_EventFired_FirstFrameReady_54; }
	inline bool* get_address_of_m_EventFired_FirstFrameReady_54() { return &___m_EventFired_FirstFrameReady_54; }
	inline void set_m_EventFired_FirstFrameReady_54(bool value)
	{
		___m_EventFired_FirstFrameReady_54 = value;
	}

	inline static int32_t get_offset_of_m_EventFired_FinishedPlaying_55() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_EventFired_FinishedPlaying_55)); }
	inline bool get_m_EventFired_FinishedPlaying_55() const { return ___m_EventFired_FinishedPlaying_55; }
	inline bool* get_address_of_m_EventFired_FinishedPlaying_55() { return &___m_EventFired_FinishedPlaying_55; }
	inline void set_m_EventFired_FinishedPlaying_55(bool value)
	{
		___m_EventFired_FinishedPlaying_55 = value;
	}

	inline static int32_t get_offset_of_m_EventFired_MetaDataReady_56() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_EventFired_MetaDataReady_56)); }
	inline bool get_m_EventFired_MetaDataReady_56() const { return ___m_EventFired_MetaDataReady_56; }
	inline bool* get_address_of_m_EventFired_MetaDataReady_56() { return &___m_EventFired_MetaDataReady_56; }
	inline void set_m_EventFired_MetaDataReady_56(bool value)
	{
		___m_EventFired_MetaDataReady_56 = value;
	}

	inline static int32_t get_offset_of_m_EventState_PlaybackStalled_57() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_EventState_PlaybackStalled_57)); }
	inline bool get_m_EventState_PlaybackStalled_57() const { return ___m_EventState_PlaybackStalled_57; }
	inline bool* get_address_of_m_EventState_PlaybackStalled_57() { return &___m_EventState_PlaybackStalled_57; }
	inline void set_m_EventState_PlaybackStalled_57(bool value)
	{
		___m_EventState_PlaybackStalled_57 = value;
	}

	inline static int32_t get_offset_of_m_EventState_PlaybackBuffering_58() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_EventState_PlaybackBuffering_58)); }
	inline bool get_m_EventState_PlaybackBuffering_58() const { return ___m_EventState_PlaybackBuffering_58; }
	inline bool* get_address_of_m_EventState_PlaybackBuffering_58() { return &___m_EventState_PlaybackBuffering_58; }
	inline void set_m_EventState_PlaybackBuffering_58(bool value)
	{
		___m_EventState_PlaybackBuffering_58 = value;
	}

	inline static int32_t get_offset_of_m_EventState_PlaybackSeeking_59() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_EventState_PlaybackSeeking_59)); }
	inline bool get_m_EventState_PlaybackSeeking_59() const { return ___m_EventState_PlaybackSeeking_59; }
	inline bool* get_address_of_m_EventState_PlaybackSeeking_59() { return &___m_EventState_PlaybackSeeking_59; }
	inline void set_m_EventState_PlaybackSeeking_59(bool value)
	{
		___m_EventState_PlaybackSeeking_59 = value;
	}

	inline static int32_t get_offset_of_m_EventState_PreviousWidth_60() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_EventState_PreviousWidth_60)); }
	inline int32_t get_m_EventState_PreviousWidth_60() const { return ___m_EventState_PreviousWidth_60; }
	inline int32_t* get_address_of_m_EventState_PreviousWidth_60() { return &___m_EventState_PreviousWidth_60; }
	inline void set_m_EventState_PreviousWidth_60(int32_t value)
	{
		___m_EventState_PreviousWidth_60 = value;
	}

	inline static int32_t get_offset_of_m_EventState_PreviousHeight_61() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_EventState_PreviousHeight_61)); }
	inline int32_t get_m_EventState_PreviousHeight_61() const { return ___m_EventState_PreviousHeight_61; }
	inline int32_t* get_address_of_m_EventState_PreviousHeight_61() { return &___m_EventState_PreviousHeight_61; }
	inline void set_m_EventState_PreviousHeight_61(int32_t value)
	{
		___m_EventState_PreviousHeight_61 = value;
	}

	inline static int32_t get_offset_of_m_previousSubtitleIndex_62() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_previousSubtitleIndex_62)); }
	inline int32_t get_m_previousSubtitleIndex_62() const { return ___m_previousSubtitleIndex_62; }
	inline int32_t* get_address_of_m_previousSubtitleIndex_62() { return &___m_previousSubtitleIndex_62; }
	inline void set_m_previousSubtitleIndex_62(int32_t value)
	{
		___m_previousSubtitleIndex_62 = value;
	}

	inline static int32_t get_offset_of_m_FinishedFrameOpenCheck_64() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_FinishedFrameOpenCheck_64)); }
	inline bool get_m_FinishedFrameOpenCheck_64() const { return ___m_FinishedFrameOpenCheck_64; }
	inline bool* get_address_of_m_FinishedFrameOpenCheck_64() { return &___m_FinishedFrameOpenCheck_64; }
	inline void set_m_FinishedFrameOpenCheck_64(bool value)
	{
		___m_FinishedFrameOpenCheck_64 = value;
	}

	inline static int32_t get_offset_of_m_sourceSampleRate_65() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_sourceSampleRate_65)); }
	inline uint32_t get_m_sourceSampleRate_65() const { return ___m_sourceSampleRate_65; }
	inline uint32_t* get_address_of_m_sourceSampleRate_65() { return &___m_sourceSampleRate_65; }
	inline void set_m_sourceSampleRate_65(uint32_t value)
	{
		___m_sourceSampleRate_65 = value;
	}

	inline static int32_t get_offset_of_m_sourceChannels_66() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_sourceChannels_66)); }
	inline uint32_t get_m_sourceChannels_66() const { return ___m_sourceChannels_66; }
	inline uint32_t* get_address_of_m_sourceChannels_66() { return &___m_sourceChannels_66; }
	inline void set_m_sourceChannels_66(uint32_t value)
	{
		___m_sourceChannels_66 = value;
	}

	inline static int32_t get_offset_of_m_manuallySetAudioSourceProperties_67() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ___m_manuallySetAudioSourceProperties_67)); }
	inline bool get_m_manuallySetAudioSourceProperties_67() const { return ___m_manuallySetAudioSourceProperties_67; }
	inline bool* get_address_of_m_manuallySetAudioSourceProperties_67() { return &___m_manuallySetAudioSourceProperties_67; }
	inline void set_m_manuallySetAudioSourceProperties_67(bool value)
	{
		___m_manuallySetAudioSourceProperties_67 = value;
	}

	inline static int32_t get_offset_of__optionsWindows_68() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ____optionsWindows_68)); }
	inline OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1 * get__optionsWindows_68() const { return ____optionsWindows_68; }
	inline OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1 ** get_address_of__optionsWindows_68() { return &____optionsWindows_68; }
	inline void set__optionsWindows_68(OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1 * value)
	{
		____optionsWindows_68 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____optionsWindows_68), (void*)value);
	}

	inline static int32_t get_offset_of__optionsMacOSX_69() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ____optionsMacOSX_69)); }
	inline OptionsMacOSX_t1B3B40C8ABFE68B24D80EA87E10EFA9C1972A2B3 * get__optionsMacOSX_69() const { return ____optionsMacOSX_69; }
	inline OptionsMacOSX_t1B3B40C8ABFE68B24D80EA87E10EFA9C1972A2B3 ** get_address_of__optionsMacOSX_69() { return &____optionsMacOSX_69; }
	inline void set__optionsMacOSX_69(OptionsMacOSX_t1B3B40C8ABFE68B24D80EA87E10EFA9C1972A2B3 * value)
	{
		____optionsMacOSX_69 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____optionsMacOSX_69), (void*)value);
	}

	inline static int32_t get_offset_of__optionsIOS_70() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ____optionsIOS_70)); }
	inline OptionsIOS_t868D1748A3E07299A108C1E60F97753CA38954A5 * get__optionsIOS_70() const { return ____optionsIOS_70; }
	inline OptionsIOS_t868D1748A3E07299A108C1E60F97753CA38954A5 ** get_address_of__optionsIOS_70() { return &____optionsIOS_70; }
	inline void set__optionsIOS_70(OptionsIOS_t868D1748A3E07299A108C1E60F97753CA38954A5 * value)
	{
		____optionsIOS_70 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____optionsIOS_70), (void*)value);
	}

	inline static int32_t get_offset_of__optionsTVOS_71() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ____optionsTVOS_71)); }
	inline OptionsTVOS_tFDA4033A636E29A9005B4D9092A32298FFE8911B * get__optionsTVOS_71() const { return ____optionsTVOS_71; }
	inline OptionsTVOS_tFDA4033A636E29A9005B4D9092A32298FFE8911B ** get_address_of__optionsTVOS_71() { return &____optionsTVOS_71; }
	inline void set__optionsTVOS_71(OptionsTVOS_tFDA4033A636E29A9005B4D9092A32298FFE8911B * value)
	{
		____optionsTVOS_71 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____optionsTVOS_71), (void*)value);
	}

	inline static int32_t get_offset_of__optionsAndroid_72() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ____optionsAndroid_72)); }
	inline OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393 * get__optionsAndroid_72() const { return ____optionsAndroid_72; }
	inline OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393 ** get_address_of__optionsAndroid_72() { return &____optionsAndroid_72; }
	inline void set__optionsAndroid_72(OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393 * value)
	{
		____optionsAndroid_72 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____optionsAndroid_72), (void*)value);
	}

	inline static int32_t get_offset_of__optionsWindowsPhone_73() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ____optionsWindowsPhone_73)); }
	inline OptionsWindowsPhone_t5B5890235D85585DFCBEC0D280F332C274B6E63C * get__optionsWindowsPhone_73() const { return ____optionsWindowsPhone_73; }
	inline OptionsWindowsPhone_t5B5890235D85585DFCBEC0D280F332C274B6E63C ** get_address_of__optionsWindowsPhone_73() { return &____optionsWindowsPhone_73; }
	inline void set__optionsWindowsPhone_73(OptionsWindowsPhone_t5B5890235D85585DFCBEC0D280F332C274B6E63C * value)
	{
		____optionsWindowsPhone_73 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____optionsWindowsPhone_73), (void*)value);
	}

	inline static int32_t get_offset_of__optionsWindowsUWP_74() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ____optionsWindowsUWP_74)); }
	inline OptionsWindowsUWP_tBA5BE5D51A12AA6360DF87691342C5EE7B8F7F66 * get__optionsWindowsUWP_74() const { return ____optionsWindowsUWP_74; }
	inline OptionsWindowsUWP_tBA5BE5D51A12AA6360DF87691342C5EE7B8F7F66 ** get_address_of__optionsWindowsUWP_74() { return &____optionsWindowsUWP_74; }
	inline void set__optionsWindowsUWP_74(OptionsWindowsUWP_tBA5BE5D51A12AA6360DF87691342C5EE7B8F7F66 * value)
	{
		____optionsWindowsUWP_74 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____optionsWindowsUWP_74), (void*)value);
	}

	inline static int32_t get_offset_of__optionsWebGL_75() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ____optionsWebGL_75)); }
	inline OptionsWebGL_tA7A6C5EB6C224751A8B5DF0B99DE0C1A3DD15B05 * get__optionsWebGL_75() const { return ____optionsWebGL_75; }
	inline OptionsWebGL_tA7A6C5EB6C224751A8B5DF0B99DE0C1A3DD15B05 ** get_address_of__optionsWebGL_75() { return &____optionsWebGL_75; }
	inline void set__optionsWebGL_75(OptionsWebGL_tA7A6C5EB6C224751A8B5DF0B99DE0C1A3DD15B05 * value)
	{
		____optionsWebGL_75 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____optionsWebGL_75), (void*)value);
	}

	inline static int32_t get_offset_of__optionsPS4_76() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64, ____optionsPS4_76)); }
	inline OptionsPS4_t43756CE0642DB94AA9A8E30A58524CDC7BD082D1 * get__optionsPS4_76() const { return ____optionsPS4_76; }
	inline OptionsPS4_t43756CE0642DB94AA9A8E30A58524CDC7BD082D1 ** get_address_of__optionsPS4_76() { return &____optionsPS4_76; }
	inline void set__optionsPS4_76(OptionsPS4_t43756CE0642DB94AA9A8E30A58524CDC7BD082D1 * value)
	{
		____optionsPS4_76 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____optionsPS4_76), (void*)value);
	}
};

struct MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_StaticFields
{
public:
	// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::s_GlobalStartup
	bool ___s_GlobalStartup_51;
	// UnityEngine.Camera RenderHeads.Media.AVProVideo.MediaPlayer::m_DummyCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___m_DummyCamera_63;

public:
	inline static int32_t get_offset_of_s_GlobalStartup_51() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_StaticFields, ___s_GlobalStartup_51)); }
	inline bool get_s_GlobalStartup_51() const { return ___s_GlobalStartup_51; }
	inline bool* get_address_of_s_GlobalStartup_51() { return &___s_GlobalStartup_51; }
	inline void set_s_GlobalStartup_51(bool value)
	{
		___s_GlobalStartup_51 = value;
	}

	inline static int32_t get_offset_of_m_DummyCamera_63() { return static_cast<int32_t>(offsetof(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_StaticFields, ___m_DummyCamera_63)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_m_DummyCamera_63() const { return ___m_DummyCamera_63; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_m_DummyCamera_63() { return &___m_DummyCamera_63; }
	inline void set_m_DummyCamera_63(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___m_DummyCamera_63 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DummyCamera_63), (void*)value);
	}
};


// PreloaderController
struct PreloaderController_tD3EB462D0A9954E9D9785017331EFFC5C9072801  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject PreloaderController::preLoaderPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___preLoaderPanel_4;
	// System.Single PreloaderController::preloaderDelay
	float ___preloaderDelay_5;

public:
	inline static int32_t get_offset_of_preLoaderPanel_4() { return static_cast<int32_t>(offsetof(PreloaderController_tD3EB462D0A9954E9D9785017331EFFC5C9072801, ___preLoaderPanel_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_preLoaderPanel_4() const { return ___preLoaderPanel_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_preLoaderPanel_4() { return &___preLoaderPanel_4; }
	inline void set_preLoaderPanel_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___preLoaderPanel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___preLoaderPanel_4), (void*)value);
	}

	inline static int32_t get_offset_of_preloaderDelay_5() { return static_cast<int32_t>(offsetof(PreloaderController_tD3EB462D0A9954E9D9785017331EFFC5C9072801, ___preloaderDelay_5)); }
	inline float get_preloaderDelay_5() const { return ___preloaderDelay_5; }
	inline float* get_address_of_preloaderDelay_5() { return &___preloaderDelay_5; }
	inline void set_preloaderDelay_5(float value)
	{
		___preloaderDelay_5 = value;
	}
};


// RenderHeads.Media.AVProVideo.Demos.SimpleController
struct SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String RenderHeads.Media.AVProVideo.Demos.SimpleController::_folder
	String_t* ____folder_4;
	// System.String[] RenderHeads.Media.AVProVideo.Demos.SimpleController::_filenames
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____filenames_5;
	// System.String[] RenderHeads.Media.AVProVideo.Demos.SimpleController::_streams
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____streams_6;
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.SimpleController::_mediaPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mediaPlayer_7;
	// RenderHeads.Media.AVProVideo.DisplayIMGUI RenderHeads.Media.AVProVideo.Demos.SimpleController::_display
	DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04 * ____display_8;
	// UnityEngine.GUISkin RenderHeads.Media.AVProVideo.Demos.SimpleController::_guiSkin
	GUISkin_tE353D65D4618423B574BAD31F5C5AC1B967E32C6 * ____guiSkin_9;
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.SimpleController::_width
	int32_t ____width_10;
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.SimpleController::_height
	int32_t ____height_11;
	// System.Single RenderHeads.Media.AVProVideo.Demos.SimpleController::_durationSeconds
	float ____durationSeconds_12;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.SimpleController::_useFading
	bool ____useFading_13;
	// System.Collections.Generic.Queue`1<System.String> RenderHeads.Media.AVProVideo.Demos.SimpleController::_eventLog
	Queue_1_tD2C03A5990B5958D85846D872A22AA67F3E8F97D * ____eventLog_14;
	// System.Single RenderHeads.Media.AVProVideo.Demos.SimpleController::_eventTimer
	float ____eventTimer_15;
	// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation RenderHeads.Media.AVProVideo.Demos.SimpleController::_nextVideoLocation
	int32_t ____nextVideoLocation_16;
	// System.String RenderHeads.Media.AVProVideo.Demos.SimpleController::_nextVideoPath
	String_t* ____nextVideoPath_17;

public:
	inline static int32_t get_offset_of__folder_4() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____folder_4)); }
	inline String_t* get__folder_4() const { return ____folder_4; }
	inline String_t** get_address_of__folder_4() { return &____folder_4; }
	inline void set__folder_4(String_t* value)
	{
		____folder_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____folder_4), (void*)value);
	}

	inline static int32_t get_offset_of__filenames_5() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____filenames_5)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__filenames_5() const { return ____filenames_5; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__filenames_5() { return &____filenames_5; }
	inline void set__filenames_5(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____filenames_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____filenames_5), (void*)value);
	}

	inline static int32_t get_offset_of__streams_6() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____streams_6)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__streams_6() const { return ____streams_6; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__streams_6() { return &____streams_6; }
	inline void set__streams_6(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____streams_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____streams_6), (void*)value);
	}

	inline static int32_t get_offset_of__mediaPlayer_7() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____mediaPlayer_7)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mediaPlayer_7() const { return ____mediaPlayer_7; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mediaPlayer_7() { return &____mediaPlayer_7; }
	inline void set__mediaPlayer_7(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mediaPlayer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_7), (void*)value);
	}

	inline static int32_t get_offset_of__display_8() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____display_8)); }
	inline DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04 * get__display_8() const { return ____display_8; }
	inline DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04 ** get_address_of__display_8() { return &____display_8; }
	inline void set__display_8(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04 * value)
	{
		____display_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____display_8), (void*)value);
	}

	inline static int32_t get_offset_of__guiSkin_9() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____guiSkin_9)); }
	inline GUISkin_tE353D65D4618423B574BAD31F5C5AC1B967E32C6 * get__guiSkin_9() const { return ____guiSkin_9; }
	inline GUISkin_tE353D65D4618423B574BAD31F5C5AC1B967E32C6 ** get_address_of__guiSkin_9() { return &____guiSkin_9; }
	inline void set__guiSkin_9(GUISkin_tE353D65D4618423B574BAD31F5C5AC1B967E32C6 * value)
	{
		____guiSkin_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____guiSkin_9), (void*)value);
	}

	inline static int32_t get_offset_of__width_10() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____width_10)); }
	inline int32_t get__width_10() const { return ____width_10; }
	inline int32_t* get_address_of__width_10() { return &____width_10; }
	inline void set__width_10(int32_t value)
	{
		____width_10 = value;
	}

	inline static int32_t get_offset_of__height_11() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____height_11)); }
	inline int32_t get__height_11() const { return ____height_11; }
	inline int32_t* get_address_of__height_11() { return &____height_11; }
	inline void set__height_11(int32_t value)
	{
		____height_11 = value;
	}

	inline static int32_t get_offset_of__durationSeconds_12() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____durationSeconds_12)); }
	inline float get__durationSeconds_12() const { return ____durationSeconds_12; }
	inline float* get_address_of__durationSeconds_12() { return &____durationSeconds_12; }
	inline void set__durationSeconds_12(float value)
	{
		____durationSeconds_12 = value;
	}

	inline static int32_t get_offset_of__useFading_13() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____useFading_13)); }
	inline bool get__useFading_13() const { return ____useFading_13; }
	inline bool* get_address_of__useFading_13() { return &____useFading_13; }
	inline void set__useFading_13(bool value)
	{
		____useFading_13 = value;
	}

	inline static int32_t get_offset_of__eventLog_14() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____eventLog_14)); }
	inline Queue_1_tD2C03A5990B5958D85846D872A22AA67F3E8F97D * get__eventLog_14() const { return ____eventLog_14; }
	inline Queue_1_tD2C03A5990B5958D85846D872A22AA67F3E8F97D ** get_address_of__eventLog_14() { return &____eventLog_14; }
	inline void set__eventLog_14(Queue_1_tD2C03A5990B5958D85846D872A22AA67F3E8F97D * value)
	{
		____eventLog_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____eventLog_14), (void*)value);
	}

	inline static int32_t get_offset_of__eventTimer_15() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____eventTimer_15)); }
	inline float get__eventTimer_15() const { return ____eventTimer_15; }
	inline float* get_address_of__eventTimer_15() { return &____eventTimer_15; }
	inline void set__eventTimer_15(float value)
	{
		____eventTimer_15 = value;
	}

	inline static int32_t get_offset_of__nextVideoLocation_16() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____nextVideoLocation_16)); }
	inline int32_t get__nextVideoLocation_16() const { return ____nextVideoLocation_16; }
	inline int32_t* get_address_of__nextVideoLocation_16() { return &____nextVideoLocation_16; }
	inline void set__nextVideoLocation_16(int32_t value)
	{
		____nextVideoLocation_16 = value;
	}

	inline static int32_t get_offset_of__nextVideoPath_17() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____nextVideoPath_17)); }
	inline String_t* get__nextVideoPath_17() const { return ____nextVideoPath_17; }
	inline String_t** get_address_of__nextVideoPath_17() { return &____nextVideoPath_17; }
	inline void set__nextVideoPath_17(String_t* value)
	{
		____nextVideoPath_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____nextVideoPath_17), (void*)value);
	}
};


// Crosstales.OnlineCheck.Demo.SpeedCheck
struct SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject Crosstales.OnlineCheck.Demo.SpeedCheck::MainScenePanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___MainScenePanel_4;
	// UnityEngine.GameObject Crosstales.OnlineCheck.Demo.SpeedCheck::speedCheckPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___speedCheckPanel_5;
	// UnityEngine.UI.Text Crosstales.OnlineCheck.Demo.SpeedCheck::Result
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___Result_6;
	// UnityEngine.UI.Button Crosstales.OnlineCheck.Demo.SpeedCheck::CheckButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___CheckButton_7;
	// UnityEngine.UI.Button Crosstales.OnlineCheck.Demo.SpeedCheck::BackButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___BackButton_8;
	// System.Single Crosstales.OnlineCheck.Demo.SpeedCheck::TimerAutoCheck
	float ___TimerAutoCheck_9;
	// System.Boolean Crosstales.OnlineCheck.Demo.SpeedCheck::isSpeedCheck
	bool ___isSpeedCheck_10;
	// System.Boolean Crosstales.OnlineCheck.Demo.SpeedCheck::isAutoSpeedCheck
	bool ___isAutoSpeedCheck_11;
	// UnityEngine.Coroutine Crosstales.OnlineCheck.Demo.SpeedCheck::co
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ___co_12;

public:
	inline static int32_t get_offset_of_MainScenePanel_4() { return static_cast<int32_t>(offsetof(SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5, ___MainScenePanel_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_MainScenePanel_4() const { return ___MainScenePanel_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_MainScenePanel_4() { return &___MainScenePanel_4; }
	inline void set_MainScenePanel_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___MainScenePanel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MainScenePanel_4), (void*)value);
	}

	inline static int32_t get_offset_of_speedCheckPanel_5() { return static_cast<int32_t>(offsetof(SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5, ___speedCheckPanel_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_speedCheckPanel_5() const { return ___speedCheckPanel_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_speedCheckPanel_5() { return &___speedCheckPanel_5; }
	inline void set_speedCheckPanel_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___speedCheckPanel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___speedCheckPanel_5), (void*)value);
	}

	inline static int32_t get_offset_of_Result_6() { return static_cast<int32_t>(offsetof(SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5, ___Result_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_Result_6() const { return ___Result_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_Result_6() { return &___Result_6; }
	inline void set_Result_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___Result_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Result_6), (void*)value);
	}

	inline static int32_t get_offset_of_CheckButton_7() { return static_cast<int32_t>(offsetof(SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5, ___CheckButton_7)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_CheckButton_7() const { return ___CheckButton_7; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_CheckButton_7() { return &___CheckButton_7; }
	inline void set_CheckButton_7(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___CheckButton_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CheckButton_7), (void*)value);
	}

	inline static int32_t get_offset_of_BackButton_8() { return static_cast<int32_t>(offsetof(SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5, ___BackButton_8)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_BackButton_8() const { return ___BackButton_8; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_BackButton_8() { return &___BackButton_8; }
	inline void set_BackButton_8(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___BackButton_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BackButton_8), (void*)value);
	}

	inline static int32_t get_offset_of_TimerAutoCheck_9() { return static_cast<int32_t>(offsetof(SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5, ___TimerAutoCheck_9)); }
	inline float get_TimerAutoCheck_9() const { return ___TimerAutoCheck_9; }
	inline float* get_address_of_TimerAutoCheck_9() { return &___TimerAutoCheck_9; }
	inline void set_TimerAutoCheck_9(float value)
	{
		___TimerAutoCheck_9 = value;
	}

	inline static int32_t get_offset_of_isSpeedCheck_10() { return static_cast<int32_t>(offsetof(SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5, ___isSpeedCheck_10)); }
	inline bool get_isSpeedCheck_10() const { return ___isSpeedCheck_10; }
	inline bool* get_address_of_isSpeedCheck_10() { return &___isSpeedCheck_10; }
	inline void set_isSpeedCheck_10(bool value)
	{
		___isSpeedCheck_10 = value;
	}

	inline static int32_t get_offset_of_isAutoSpeedCheck_11() { return static_cast<int32_t>(offsetof(SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5, ___isAutoSpeedCheck_11)); }
	inline bool get_isAutoSpeedCheck_11() const { return ___isAutoSpeedCheck_11; }
	inline bool* get_address_of_isAutoSpeedCheck_11() { return &___isAutoSpeedCheck_11; }
	inline void set_isAutoSpeedCheck_11(bool value)
	{
		___isAutoSpeedCheck_11 = value;
	}

	inline static int32_t get_offset_of_co_12() { return static_cast<int32_t>(offsetof(SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5, ___co_12)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get_co_12() const { return ___co_12; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of_co_12() { return &___co_12; }
	inline void set_co_12(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		___co_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___co_12), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// VideoPlayerController
struct VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer VideoPlayerController::mediaPlayerA
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ___mediaPlayerA_4;
	// RenderHeads.Media.AVProVideo.MediaPlayer VideoPlayerController::mediaPlayerB
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ___mediaPlayerB_5;
	// RenderHeads.Media.AVProVideo.MediaPlayer VideoPlayerController::mediaPlayerC
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ___mediaPlayerC_6;
	// RenderHeads.Media.AVProVideo.MediaPlayer VideoPlayerController::mediaPlayerD
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ___mediaPlayerD_7;
	// RenderHeads.Media.AVProVideo.DisplayUGUI VideoPlayerController::displayUGUIMain
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * ___displayUGUIMain_8;
	// RenderHeads.Media.AVProVideo.DisplayUGUI VideoPlayerController::displayUGUIA
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * ___displayUGUIA_9;
	// RenderHeads.Media.AVProVideo.DisplayUGUI VideoPlayerController::displayUGUIB
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * ___displayUGUIB_10;
	// RenderHeads.Media.AVProVideo.DisplayUGUI VideoPlayerController::displayUGUIC
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * ___displayUGUIC_11;
	// RenderHeads.Media.AVProVideo.DisplayUGUI VideoPlayerController::displayUGUID
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * ___displayUGUID_12;
	// System.Single VideoPlayerController::playStartDelay
	float ___playStartDelay_13;

public:
	inline static int32_t get_offset_of_mediaPlayerA_4() { return static_cast<int32_t>(offsetof(VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579, ___mediaPlayerA_4)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get_mediaPlayerA_4() const { return ___mediaPlayerA_4; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of_mediaPlayerA_4() { return &___mediaPlayerA_4; }
	inline void set_mediaPlayerA_4(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		___mediaPlayerA_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mediaPlayerA_4), (void*)value);
	}

	inline static int32_t get_offset_of_mediaPlayerB_5() { return static_cast<int32_t>(offsetof(VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579, ___mediaPlayerB_5)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get_mediaPlayerB_5() const { return ___mediaPlayerB_5; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of_mediaPlayerB_5() { return &___mediaPlayerB_5; }
	inline void set_mediaPlayerB_5(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		___mediaPlayerB_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mediaPlayerB_5), (void*)value);
	}

	inline static int32_t get_offset_of_mediaPlayerC_6() { return static_cast<int32_t>(offsetof(VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579, ___mediaPlayerC_6)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get_mediaPlayerC_6() const { return ___mediaPlayerC_6; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of_mediaPlayerC_6() { return &___mediaPlayerC_6; }
	inline void set_mediaPlayerC_6(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		___mediaPlayerC_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mediaPlayerC_6), (void*)value);
	}

	inline static int32_t get_offset_of_mediaPlayerD_7() { return static_cast<int32_t>(offsetof(VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579, ___mediaPlayerD_7)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get_mediaPlayerD_7() const { return ___mediaPlayerD_7; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of_mediaPlayerD_7() { return &___mediaPlayerD_7; }
	inline void set_mediaPlayerD_7(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		___mediaPlayerD_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mediaPlayerD_7), (void*)value);
	}

	inline static int32_t get_offset_of_displayUGUIMain_8() { return static_cast<int32_t>(offsetof(VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579, ___displayUGUIMain_8)); }
	inline DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * get_displayUGUIMain_8() const { return ___displayUGUIMain_8; }
	inline DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A ** get_address_of_displayUGUIMain_8() { return &___displayUGUIMain_8; }
	inline void set_displayUGUIMain_8(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * value)
	{
		___displayUGUIMain_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___displayUGUIMain_8), (void*)value);
	}

	inline static int32_t get_offset_of_displayUGUIA_9() { return static_cast<int32_t>(offsetof(VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579, ___displayUGUIA_9)); }
	inline DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * get_displayUGUIA_9() const { return ___displayUGUIA_9; }
	inline DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A ** get_address_of_displayUGUIA_9() { return &___displayUGUIA_9; }
	inline void set_displayUGUIA_9(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * value)
	{
		___displayUGUIA_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___displayUGUIA_9), (void*)value);
	}

	inline static int32_t get_offset_of_displayUGUIB_10() { return static_cast<int32_t>(offsetof(VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579, ___displayUGUIB_10)); }
	inline DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * get_displayUGUIB_10() const { return ___displayUGUIB_10; }
	inline DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A ** get_address_of_displayUGUIB_10() { return &___displayUGUIB_10; }
	inline void set_displayUGUIB_10(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * value)
	{
		___displayUGUIB_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___displayUGUIB_10), (void*)value);
	}

	inline static int32_t get_offset_of_displayUGUIC_11() { return static_cast<int32_t>(offsetof(VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579, ___displayUGUIC_11)); }
	inline DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * get_displayUGUIC_11() const { return ___displayUGUIC_11; }
	inline DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A ** get_address_of_displayUGUIC_11() { return &___displayUGUIC_11; }
	inline void set_displayUGUIC_11(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * value)
	{
		___displayUGUIC_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___displayUGUIC_11), (void*)value);
	}

	inline static int32_t get_offset_of_displayUGUID_12() { return static_cast<int32_t>(offsetof(VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579, ___displayUGUID_12)); }
	inline DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * get_displayUGUID_12() const { return ___displayUGUID_12; }
	inline DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A ** get_address_of_displayUGUID_12() { return &___displayUGUID_12; }
	inline void set_displayUGUID_12(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * value)
	{
		___displayUGUID_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___displayUGUID_12), (void*)value);
	}

	inline static int32_t get_offset_of_playStartDelay_13() { return static_cast<int32_t>(offsetof(VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579, ___playStartDelay_13)); }
	inline float get_playStartDelay_13() const { return ___playStartDelay_13; }
	inline float* get_address_of_playStartDelay_13() { return &___playStartDelay_13; }
	inline void set_playStartDelay_13(float value)
	{
		___playStartDelay_13 = value;
	}
};


// VideoTimeDisplay
struct VideoTimeDisplay_t99E522A668D79E64AD5F7E5D3676A8B3D78B8832  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer VideoTimeDisplay::mediaPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ___mediaPlayer_4;
	// System.Single VideoTimeDisplay::time
	float ___time_5;
	// System.Single VideoTimeDisplay::duration
	float ___duration_6;
	// System.Single VideoTimeDisplay::d
	float ___d_7;
	// UnityEngine.UI.Text VideoTimeDisplay::timerText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___timerText_8;

public:
	inline static int32_t get_offset_of_mediaPlayer_4() { return static_cast<int32_t>(offsetof(VideoTimeDisplay_t99E522A668D79E64AD5F7E5D3676A8B3D78B8832, ___mediaPlayer_4)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get_mediaPlayer_4() const { return ___mediaPlayer_4; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of_mediaPlayer_4() { return &___mediaPlayer_4; }
	inline void set_mediaPlayer_4(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		___mediaPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mediaPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of_time_5() { return static_cast<int32_t>(offsetof(VideoTimeDisplay_t99E522A668D79E64AD5F7E5D3676A8B3D78B8832, ___time_5)); }
	inline float get_time_5() const { return ___time_5; }
	inline float* get_address_of_time_5() { return &___time_5; }
	inline void set_time_5(float value)
	{
		___time_5 = value;
	}

	inline static int32_t get_offset_of_duration_6() { return static_cast<int32_t>(offsetof(VideoTimeDisplay_t99E522A668D79E64AD5F7E5D3676A8B3D78B8832, ___duration_6)); }
	inline float get_duration_6() const { return ___duration_6; }
	inline float* get_address_of_duration_6() { return &___duration_6; }
	inline void set_duration_6(float value)
	{
		___duration_6 = value;
	}

	inline static int32_t get_offset_of_d_7() { return static_cast<int32_t>(offsetof(VideoTimeDisplay_t99E522A668D79E64AD5F7E5D3676A8B3D78B8832, ___d_7)); }
	inline float get_d_7() const { return ___d_7; }
	inline float* get_address_of_d_7() { return &___d_7; }
	inline void set_d_7(float value)
	{
		___d_7 = value;
	}

	inline static int32_t get_offset_of_timerText_8() { return static_cast<int32_t>(offsetof(VideoTimeDisplay_t99E522A668D79E64AD5F7E5D3676A8B3D78B8832, ___timerText_8)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_timerText_8() const { return ___timerText_8; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_timerText_8() { return &___timerText_8; }
	inline void set_timerText_8(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___timerText_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___timerText_8), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.VideoTrigger
struct VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.VideoTrigger::_mediaPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mediaPlayer_4;
	// System.Single RenderHeads.Media.AVProVideo.Demos.VideoTrigger::_fadeTimeMs
	float ____fadeTimeMs_5;
	// System.Single RenderHeads.Media.AVProVideo.Demos.VideoTrigger::_fade
	float ____fade_6;
	// System.Single RenderHeads.Media.AVProVideo.Demos.VideoTrigger::_fadeDirection
	float ____fadeDirection_7;

public:
	inline static int32_t get_offset_of__mediaPlayer_4() { return static_cast<int32_t>(offsetof(VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656, ____mediaPlayer_4)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mediaPlayer_4() const { return ____mediaPlayer_4; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mediaPlayer_4() { return &____mediaPlayer_4; }
	inline void set__mediaPlayer_4(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mediaPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of__fadeTimeMs_5() { return static_cast<int32_t>(offsetof(VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656, ____fadeTimeMs_5)); }
	inline float get__fadeTimeMs_5() const { return ____fadeTimeMs_5; }
	inline float* get_address_of__fadeTimeMs_5() { return &____fadeTimeMs_5; }
	inline void set__fadeTimeMs_5(float value)
	{
		____fadeTimeMs_5 = value;
	}

	inline static int32_t get_offset_of__fade_6() { return static_cast<int32_t>(offsetof(VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656, ____fade_6)); }
	inline float get__fade_6() const { return ____fade_6; }
	inline float* get_address_of__fade_6() { return &____fade_6; }
	inline void set__fade_6(float value)
	{
		____fade_6 = value;
	}

	inline static int32_t get_offset_of__fadeDirection_7() { return static_cast<int32_t>(offsetof(VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656, ____fadeDirection_7)); }
	inline float get__fadeDirection_7() const { return ____fadeDirection_7; }
	inline float* get_address_of__fadeDirection_7() { return &____fadeDirection_7; }
	inline void set__fadeDirection_7(float value)
	{
		____fadeDirection_7 = value;
	}
};


// VideoVCR
struct VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer VideoVCR::_mediaPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mediaPlayer_4;
	// UnityEngine.RectTransform VideoVCR::_bufferedSliderRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ____bufferedSliderRect_5;
	// UnityEngine.UI.Slider VideoVCR::_videoSeekSlider
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * ____videoSeekSlider_6;
	// System.Single VideoVCR::_setVideoSeekSliderValue
	float ____setVideoSeekSliderValue_7;
	// System.Boolean VideoVCR::_wasPlayingOnScrub
	bool ____wasPlayingOnScrub_8;
	// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation VideoVCR::_location
	int32_t ____location_9;
	// System.String VideoVCR::_folder
	String_t* ____folder_10;
	// System.String VideoVCR::_videoFiles
	String_t* ____videoFiles_11;
	// UnityEngine.UI.Image VideoVCR::_bufferedSliderImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ____bufferedSliderImage_12;
	// RenderHeads.Media.AVProVideo.MediaPlayer VideoVCR::_loadingPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____loadingPlayer_13;
	// UnityEngine.Texture2D VideoVCR::_texture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ____texture_14;
	// System.Single VideoVCR::_timeStepSeconds
	float ____timeStepSeconds_15;

public:
	inline static int32_t get_offset_of__mediaPlayer_4() { return static_cast<int32_t>(offsetof(VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA, ____mediaPlayer_4)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mediaPlayer_4() const { return ____mediaPlayer_4; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mediaPlayer_4() { return &____mediaPlayer_4; }
	inline void set__mediaPlayer_4(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mediaPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of__bufferedSliderRect_5() { return static_cast<int32_t>(offsetof(VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA, ____bufferedSliderRect_5)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get__bufferedSliderRect_5() const { return ____bufferedSliderRect_5; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of__bufferedSliderRect_5() { return &____bufferedSliderRect_5; }
	inline void set__bufferedSliderRect_5(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		____bufferedSliderRect_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bufferedSliderRect_5), (void*)value);
	}

	inline static int32_t get_offset_of__videoSeekSlider_6() { return static_cast<int32_t>(offsetof(VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA, ____videoSeekSlider_6)); }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * get__videoSeekSlider_6() const { return ____videoSeekSlider_6; }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A ** get_address_of__videoSeekSlider_6() { return &____videoSeekSlider_6; }
	inline void set__videoSeekSlider_6(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * value)
	{
		____videoSeekSlider_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____videoSeekSlider_6), (void*)value);
	}

	inline static int32_t get_offset_of__setVideoSeekSliderValue_7() { return static_cast<int32_t>(offsetof(VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA, ____setVideoSeekSliderValue_7)); }
	inline float get__setVideoSeekSliderValue_7() const { return ____setVideoSeekSliderValue_7; }
	inline float* get_address_of__setVideoSeekSliderValue_7() { return &____setVideoSeekSliderValue_7; }
	inline void set__setVideoSeekSliderValue_7(float value)
	{
		____setVideoSeekSliderValue_7 = value;
	}

	inline static int32_t get_offset_of__wasPlayingOnScrub_8() { return static_cast<int32_t>(offsetof(VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA, ____wasPlayingOnScrub_8)); }
	inline bool get__wasPlayingOnScrub_8() const { return ____wasPlayingOnScrub_8; }
	inline bool* get_address_of__wasPlayingOnScrub_8() { return &____wasPlayingOnScrub_8; }
	inline void set__wasPlayingOnScrub_8(bool value)
	{
		____wasPlayingOnScrub_8 = value;
	}

	inline static int32_t get_offset_of__location_9() { return static_cast<int32_t>(offsetof(VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA, ____location_9)); }
	inline int32_t get__location_9() const { return ____location_9; }
	inline int32_t* get_address_of__location_9() { return &____location_9; }
	inline void set__location_9(int32_t value)
	{
		____location_9 = value;
	}

	inline static int32_t get_offset_of__folder_10() { return static_cast<int32_t>(offsetof(VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA, ____folder_10)); }
	inline String_t* get__folder_10() const { return ____folder_10; }
	inline String_t** get_address_of__folder_10() { return &____folder_10; }
	inline void set__folder_10(String_t* value)
	{
		____folder_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____folder_10), (void*)value);
	}

	inline static int32_t get_offset_of__videoFiles_11() { return static_cast<int32_t>(offsetof(VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA, ____videoFiles_11)); }
	inline String_t* get__videoFiles_11() const { return ____videoFiles_11; }
	inline String_t** get_address_of__videoFiles_11() { return &____videoFiles_11; }
	inline void set__videoFiles_11(String_t* value)
	{
		____videoFiles_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____videoFiles_11), (void*)value);
	}

	inline static int32_t get_offset_of__bufferedSliderImage_12() { return static_cast<int32_t>(offsetof(VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA, ____bufferedSliderImage_12)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get__bufferedSliderImage_12() const { return ____bufferedSliderImage_12; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of__bufferedSliderImage_12() { return &____bufferedSliderImage_12; }
	inline void set__bufferedSliderImage_12(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		____bufferedSliderImage_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bufferedSliderImage_12), (void*)value);
	}

	inline static int32_t get_offset_of__loadingPlayer_13() { return static_cast<int32_t>(offsetof(VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA, ____loadingPlayer_13)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__loadingPlayer_13() const { return ____loadingPlayer_13; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__loadingPlayer_13() { return &____loadingPlayer_13; }
	inline void set__loadingPlayer_13(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____loadingPlayer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____loadingPlayer_13), (void*)value);
	}

	inline static int32_t get_offset_of__texture_14() { return static_cast<int32_t>(offsetof(VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA, ____texture_14)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get__texture_14() const { return ____texture_14; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of__texture_14() { return &____texture_14; }
	inline void set__texture_14(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		____texture_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____texture_14), (void*)value);
	}

	inline static int32_t get_offset_of__timeStepSeconds_15() { return static_cast<int32_t>(offsetof(VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA, ____timeStepSeconds_15)); }
	inline float get__timeStepSeconds_15() const { return ____timeStepSeconds_15; }
	inline float* get_address_of__timeStepSeconds_15() { return &____timeStepSeconds_15; }
	inline void set__timeStepSeconds_15(float value)
	{
		____timeStepSeconds_15 = value;
	}
};


// WagonWheelManager
struct WagonWheelManager_t06317F447976C4D78C9E31F402AA7122B0F187C5  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Animator WagonWheelManager::animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___animator_4;

public:
	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(WagonWheelManager_t06317F447976C4D78C9E31F402AA7122B0F187C5, ___animator_4)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_animator_4() const { return ___animator_4; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animator_4), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer/OptionsTVOS
struct OptionsTVOS_tFDA4033A636E29A9005B4D9092A32298FFE8911B  : public OptionsIOS_t868D1748A3E07299A108C1E60F97753CA38954A5
{
public:

public:
};


// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_7)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_9)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_10)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_13)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_19)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Slider
struct Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_FillRect_20;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_HandleRect_21;
	// UnityEngine.UI.Slider/Direction UnityEngine.UI.Slider::m_Direction
	int32_t ___m_Direction_22;
	// System.Single UnityEngine.UI.Slider::m_MinValue
	float ___m_MinValue_23;
	// System.Single UnityEngine.UI.Slider::m_MaxValue
	float ___m_MaxValue_24;
	// System.Boolean UnityEngine.UI.Slider::m_WholeNumbers
	bool ___m_WholeNumbers_25;
	// System.Single UnityEngine.UI.Slider::m_Value
	float ___m_Value_26;
	// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::m_OnValueChanged
	SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * ___m_OnValueChanged_27;
	// UnityEngine.UI.Image UnityEngine.UI.Slider::m_FillImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___m_FillImage_28;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_FillTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_FillTransform_29;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillContainerRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_FillContainerRect_30;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_HandleTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_HandleTransform_31;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleContainerRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_HandleContainerRect_32;
	// UnityEngine.Vector2 UnityEngine.UI.Slider::m_Offset
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Offset_33;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Slider::m_Tracker
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  ___m_Tracker_34;
	// System.Boolean UnityEngine.UI.Slider::m_DelayedUpdateVisuals
	bool ___m_DelayedUpdateVisuals_35;

public:
	inline static int32_t get_offset_of_m_FillRect_20() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillRect_20)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_FillRect_20() const { return ___m_FillRect_20; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_FillRect_20() { return &___m_FillRect_20; }
	inline void set_m_FillRect_20(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_FillRect_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillRect_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleRect_21() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_HandleRect_21)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_HandleRect_21() const { return ___m_HandleRect_21; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_HandleRect_21() { return &___m_HandleRect_21; }
	inline void set_m_HandleRect_21(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_HandleRect_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleRect_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_Direction_22() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Direction_22)); }
	inline int32_t get_m_Direction_22() const { return ___m_Direction_22; }
	inline int32_t* get_address_of_m_Direction_22() { return &___m_Direction_22; }
	inline void set_m_Direction_22(int32_t value)
	{
		___m_Direction_22 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_23() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_MinValue_23)); }
	inline float get_m_MinValue_23() const { return ___m_MinValue_23; }
	inline float* get_address_of_m_MinValue_23() { return &___m_MinValue_23; }
	inline void set_m_MinValue_23(float value)
	{
		___m_MinValue_23 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_24() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_MaxValue_24)); }
	inline float get_m_MaxValue_24() const { return ___m_MaxValue_24; }
	inline float* get_address_of_m_MaxValue_24() { return &___m_MaxValue_24; }
	inline void set_m_MaxValue_24(float value)
	{
		___m_MaxValue_24 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_25() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_WholeNumbers_25)); }
	inline bool get_m_WholeNumbers_25() const { return ___m_WholeNumbers_25; }
	inline bool* get_address_of_m_WholeNumbers_25() { return &___m_WholeNumbers_25; }
	inline void set_m_WholeNumbers_25(bool value)
	{
		___m_WholeNumbers_25 = value;
	}

	inline static int32_t get_offset_of_m_Value_26() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Value_26)); }
	inline float get_m_Value_26() const { return ___m_Value_26; }
	inline float* get_address_of_m_Value_26() { return &___m_Value_26; }
	inline void set_m_Value_26(float value)
	{
		___m_Value_26 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_27() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_OnValueChanged_27)); }
	inline SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * get_m_OnValueChanged_27() const { return ___m_OnValueChanged_27; }
	inline SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 ** get_address_of_m_OnValueChanged_27() { return &___m_OnValueChanged_27; }
	inline void set_m_OnValueChanged_27(SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * value)
	{
		___m_OnValueChanged_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillImage_28() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillImage_28)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_m_FillImage_28() const { return ___m_FillImage_28; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_m_FillImage_28() { return &___m_FillImage_28; }
	inline void set_m_FillImage_28(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___m_FillImage_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillImage_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillTransform_29() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillTransform_29)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_FillTransform_29() const { return ___m_FillTransform_29; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_FillTransform_29() { return &___m_FillTransform_29; }
	inline void set_m_FillTransform_29(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_FillTransform_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillTransform_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillContainerRect_30() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillContainerRect_30)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_FillContainerRect_30() const { return ___m_FillContainerRect_30; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_FillContainerRect_30() { return &___m_FillContainerRect_30; }
	inline void set_m_FillContainerRect_30(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_FillContainerRect_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillContainerRect_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_31() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_HandleTransform_31)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_HandleTransform_31() const { return ___m_HandleTransform_31; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_HandleTransform_31() { return &___m_HandleTransform_31; }
	inline void set_m_HandleTransform_31(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_HandleTransform_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleTransform_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_32() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_HandleContainerRect_32)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_HandleContainerRect_32() const { return ___m_HandleContainerRect_32; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_HandleContainerRect_32() { return &___m_HandleContainerRect_32; }
	inline void set_m_HandleContainerRect_32(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_HandleContainerRect_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleContainerRect_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_Offset_33() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Offset_33)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Offset_33() const { return ___m_Offset_33; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Offset_33() { return &___m_Offset_33; }
	inline void set_m_Offset_33(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Offset_33 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_34() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Tracker_34)); }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  get_m_Tracker_34() const { return ___m_Tracker_34; }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * get_address_of_m_Tracker_34() { return &___m_Tracker_34; }
	inline void set_m_Tracker_34(DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  value)
	{
		___m_Tracker_34 = value;
	}

	inline static int32_t get_offset_of_m_DelayedUpdateVisuals_35() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_DelayedUpdateVisuals_35)); }
	inline bool get_m_DelayedUpdateVisuals_35() const { return ___m_DelayedUpdateVisuals_35; }
	inline bool* get_address_of_m_DelayedUpdateVisuals_35() { return &___m_DelayedUpdateVisuals_35; }
	inline void set_m_DelayedUpdateVisuals_35(bool value)
	{
		___m_DelayedUpdateVisuals_35 = value;
	}
};


// RenderHeads.Media.AVProVideo.DisplayUGUI
struct DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.DisplayUGUI::_mediaPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mediaPlayer_36;
	// UnityEngine.Rect RenderHeads.Media.AVProVideo.DisplayUGUI::m_UVRect
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___m_UVRect_37;
	// System.Boolean RenderHeads.Media.AVProVideo.DisplayUGUI::_setNativeSize
	bool ____setNativeSize_38;
	// UnityEngine.ScaleMode RenderHeads.Media.AVProVideo.DisplayUGUI::_scaleMode
	int32_t ____scaleMode_39;
	// System.Boolean RenderHeads.Media.AVProVideo.DisplayUGUI::_noDefaultDisplay
	bool ____noDefaultDisplay_40;
	// System.Boolean RenderHeads.Media.AVProVideo.DisplayUGUI::_displayInEditor
	bool ____displayInEditor_41;
	// UnityEngine.Texture RenderHeads.Media.AVProVideo.DisplayUGUI::_defaultTexture
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ____defaultTexture_42;
	// System.Int32 RenderHeads.Media.AVProVideo.DisplayUGUI::_lastWidth
	int32_t ____lastWidth_43;
	// System.Int32 RenderHeads.Media.AVProVideo.DisplayUGUI::_lastHeight
	int32_t ____lastHeight_44;
	// System.Boolean RenderHeads.Media.AVProVideo.DisplayUGUI::_flipY
	bool ____flipY_45;
	// UnityEngine.Texture RenderHeads.Media.AVProVideo.DisplayUGUI::_lastTexture
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ____lastTexture_46;
	// System.Boolean RenderHeads.Media.AVProVideo.DisplayUGUI::_userMaterial
	bool ____userMaterial_60;
	// UnityEngine.Material RenderHeads.Media.AVProVideo.DisplayUGUI::_material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ____material_61;
	// System.Collections.Generic.List`1<UnityEngine.UIVertex> RenderHeads.Media.AVProVideo.DisplayUGUI::_vertices
	List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * ____vertices_62;

public:
	inline static int32_t get_offset_of__mediaPlayer_36() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A, ____mediaPlayer_36)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mediaPlayer_36() const { return ____mediaPlayer_36; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mediaPlayer_36() { return &____mediaPlayer_36; }
	inline void set__mediaPlayer_36(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mediaPlayer_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_UVRect_37() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A, ___m_UVRect_37)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_m_UVRect_37() const { return ___m_UVRect_37; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_m_UVRect_37() { return &___m_UVRect_37; }
	inline void set_m_UVRect_37(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___m_UVRect_37 = value;
	}

	inline static int32_t get_offset_of__setNativeSize_38() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A, ____setNativeSize_38)); }
	inline bool get__setNativeSize_38() const { return ____setNativeSize_38; }
	inline bool* get_address_of__setNativeSize_38() { return &____setNativeSize_38; }
	inline void set__setNativeSize_38(bool value)
	{
		____setNativeSize_38 = value;
	}

	inline static int32_t get_offset_of__scaleMode_39() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A, ____scaleMode_39)); }
	inline int32_t get__scaleMode_39() const { return ____scaleMode_39; }
	inline int32_t* get_address_of__scaleMode_39() { return &____scaleMode_39; }
	inline void set__scaleMode_39(int32_t value)
	{
		____scaleMode_39 = value;
	}

	inline static int32_t get_offset_of__noDefaultDisplay_40() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A, ____noDefaultDisplay_40)); }
	inline bool get__noDefaultDisplay_40() const { return ____noDefaultDisplay_40; }
	inline bool* get_address_of__noDefaultDisplay_40() { return &____noDefaultDisplay_40; }
	inline void set__noDefaultDisplay_40(bool value)
	{
		____noDefaultDisplay_40 = value;
	}

	inline static int32_t get_offset_of__displayInEditor_41() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A, ____displayInEditor_41)); }
	inline bool get__displayInEditor_41() const { return ____displayInEditor_41; }
	inline bool* get_address_of__displayInEditor_41() { return &____displayInEditor_41; }
	inline void set__displayInEditor_41(bool value)
	{
		____displayInEditor_41 = value;
	}

	inline static int32_t get_offset_of__defaultTexture_42() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A, ____defaultTexture_42)); }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * get__defaultTexture_42() const { return ____defaultTexture_42; }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** get_address_of__defaultTexture_42() { return &____defaultTexture_42; }
	inline void set__defaultTexture_42(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		____defaultTexture_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____defaultTexture_42), (void*)value);
	}

	inline static int32_t get_offset_of__lastWidth_43() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A, ____lastWidth_43)); }
	inline int32_t get__lastWidth_43() const { return ____lastWidth_43; }
	inline int32_t* get_address_of__lastWidth_43() { return &____lastWidth_43; }
	inline void set__lastWidth_43(int32_t value)
	{
		____lastWidth_43 = value;
	}

	inline static int32_t get_offset_of__lastHeight_44() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A, ____lastHeight_44)); }
	inline int32_t get__lastHeight_44() const { return ____lastHeight_44; }
	inline int32_t* get_address_of__lastHeight_44() { return &____lastHeight_44; }
	inline void set__lastHeight_44(int32_t value)
	{
		____lastHeight_44 = value;
	}

	inline static int32_t get_offset_of__flipY_45() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A, ____flipY_45)); }
	inline bool get__flipY_45() const { return ____flipY_45; }
	inline bool* get_address_of__flipY_45() { return &____flipY_45; }
	inline void set__flipY_45(bool value)
	{
		____flipY_45 = value;
	}

	inline static int32_t get_offset_of__lastTexture_46() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A, ____lastTexture_46)); }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * get__lastTexture_46() const { return ____lastTexture_46; }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** get_address_of__lastTexture_46() { return &____lastTexture_46; }
	inline void set__lastTexture_46(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		____lastTexture_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lastTexture_46), (void*)value);
	}

	inline static int32_t get_offset_of__userMaterial_60() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A, ____userMaterial_60)); }
	inline bool get__userMaterial_60() const { return ____userMaterial_60; }
	inline bool* get_address_of__userMaterial_60() { return &____userMaterial_60; }
	inline void set__userMaterial_60(bool value)
	{
		____userMaterial_60 = value;
	}

	inline static int32_t get_offset_of__material_61() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A, ____material_61)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get__material_61() const { return ____material_61; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of__material_61() { return &____material_61; }
	inline void set__material_61(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		____material_61 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____material_61), (void*)value);
	}

	inline static int32_t get_offset_of__vertices_62() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A, ____vertices_62)); }
	inline List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * get__vertices_62() const { return ____vertices_62; }
	inline List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F ** get_address_of__vertices_62() { return &____vertices_62; }
	inline void set__vertices_62(List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * value)
	{
		____vertices_62 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____vertices_62), (void*)value);
	}
};

struct DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_StaticFields
{
public:
	// UnityEngine.Shader RenderHeads.Media.AVProVideo.DisplayUGUI::_shaderStereoPacking
	Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * ____shaderStereoPacking_47;
	// UnityEngine.Shader RenderHeads.Media.AVProVideo.DisplayUGUI::_shaderAlphaPacking
	Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * ____shaderAlphaPacking_48;
	// UnityEngine.Shader RenderHeads.Media.AVProVideo.DisplayUGUI::_shaderAndroidOES
	Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * ____shaderAndroidOES_49;
	// System.Int32 RenderHeads.Media.AVProVideo.DisplayUGUI::_propAlphaPack
	int32_t ____propAlphaPack_50;
	// System.Int32 RenderHeads.Media.AVProVideo.DisplayUGUI::_propVertScale
	int32_t ____propVertScale_51;
	// System.Int32 RenderHeads.Media.AVProVideo.DisplayUGUI::_propStereo
	int32_t ____propStereo_52;
	// System.Int32 RenderHeads.Media.AVProVideo.DisplayUGUI::_propApplyGamma
	int32_t ____propApplyGamma_53;
	// System.Int32 RenderHeads.Media.AVProVideo.DisplayUGUI::_propUseYpCbCr
	int32_t ____propUseYpCbCr_54;
	// System.Int32 RenderHeads.Media.AVProVideo.DisplayUGUI::_propChromaTex
	int32_t ____propChromaTex_56;
	// System.Int32 RenderHeads.Media.AVProVideo.DisplayUGUI::_propYpCbCrTransform
	int32_t ____propYpCbCrTransform_58;
	// System.Int32 RenderHeads.Media.AVProVideo.DisplayUGUI::_propCroppingScalars
	int32_t ____propCroppingScalars_59;
	// System.Collections.Generic.List`1<System.Int32> RenderHeads.Media.AVProVideo.DisplayUGUI::QuadIndices
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___QuadIndices_63;

public:
	inline static int32_t get_offset_of__shaderStereoPacking_47() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_StaticFields, ____shaderStereoPacking_47)); }
	inline Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * get__shaderStereoPacking_47() const { return ____shaderStereoPacking_47; }
	inline Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 ** get_address_of__shaderStereoPacking_47() { return &____shaderStereoPacking_47; }
	inline void set__shaderStereoPacking_47(Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * value)
	{
		____shaderStereoPacking_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____shaderStereoPacking_47), (void*)value);
	}

	inline static int32_t get_offset_of__shaderAlphaPacking_48() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_StaticFields, ____shaderAlphaPacking_48)); }
	inline Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * get__shaderAlphaPacking_48() const { return ____shaderAlphaPacking_48; }
	inline Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 ** get_address_of__shaderAlphaPacking_48() { return &____shaderAlphaPacking_48; }
	inline void set__shaderAlphaPacking_48(Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * value)
	{
		____shaderAlphaPacking_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____shaderAlphaPacking_48), (void*)value);
	}

	inline static int32_t get_offset_of__shaderAndroidOES_49() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_StaticFields, ____shaderAndroidOES_49)); }
	inline Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * get__shaderAndroidOES_49() const { return ____shaderAndroidOES_49; }
	inline Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 ** get_address_of__shaderAndroidOES_49() { return &____shaderAndroidOES_49; }
	inline void set__shaderAndroidOES_49(Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * value)
	{
		____shaderAndroidOES_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____shaderAndroidOES_49), (void*)value);
	}

	inline static int32_t get_offset_of__propAlphaPack_50() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_StaticFields, ____propAlphaPack_50)); }
	inline int32_t get__propAlphaPack_50() const { return ____propAlphaPack_50; }
	inline int32_t* get_address_of__propAlphaPack_50() { return &____propAlphaPack_50; }
	inline void set__propAlphaPack_50(int32_t value)
	{
		____propAlphaPack_50 = value;
	}

	inline static int32_t get_offset_of__propVertScale_51() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_StaticFields, ____propVertScale_51)); }
	inline int32_t get__propVertScale_51() const { return ____propVertScale_51; }
	inline int32_t* get_address_of__propVertScale_51() { return &____propVertScale_51; }
	inline void set__propVertScale_51(int32_t value)
	{
		____propVertScale_51 = value;
	}

	inline static int32_t get_offset_of__propStereo_52() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_StaticFields, ____propStereo_52)); }
	inline int32_t get__propStereo_52() const { return ____propStereo_52; }
	inline int32_t* get_address_of__propStereo_52() { return &____propStereo_52; }
	inline void set__propStereo_52(int32_t value)
	{
		____propStereo_52 = value;
	}

	inline static int32_t get_offset_of__propApplyGamma_53() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_StaticFields, ____propApplyGamma_53)); }
	inline int32_t get__propApplyGamma_53() const { return ____propApplyGamma_53; }
	inline int32_t* get_address_of__propApplyGamma_53() { return &____propApplyGamma_53; }
	inline void set__propApplyGamma_53(int32_t value)
	{
		____propApplyGamma_53 = value;
	}

	inline static int32_t get_offset_of__propUseYpCbCr_54() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_StaticFields, ____propUseYpCbCr_54)); }
	inline int32_t get__propUseYpCbCr_54() const { return ____propUseYpCbCr_54; }
	inline int32_t* get_address_of__propUseYpCbCr_54() { return &____propUseYpCbCr_54; }
	inline void set__propUseYpCbCr_54(int32_t value)
	{
		____propUseYpCbCr_54 = value;
	}

	inline static int32_t get_offset_of__propChromaTex_56() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_StaticFields, ____propChromaTex_56)); }
	inline int32_t get__propChromaTex_56() const { return ____propChromaTex_56; }
	inline int32_t* get_address_of__propChromaTex_56() { return &____propChromaTex_56; }
	inline void set__propChromaTex_56(int32_t value)
	{
		____propChromaTex_56 = value;
	}

	inline static int32_t get_offset_of__propYpCbCrTransform_58() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_StaticFields, ____propYpCbCrTransform_58)); }
	inline int32_t get__propYpCbCrTransform_58() const { return ____propYpCbCrTransform_58; }
	inline int32_t* get_address_of__propYpCbCrTransform_58() { return &____propYpCbCrTransform_58; }
	inline void set__propYpCbCrTransform_58(int32_t value)
	{
		____propYpCbCrTransform_58 = value;
	}

	inline static int32_t get_offset_of__propCroppingScalars_59() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_StaticFields, ____propCroppingScalars_59)); }
	inline int32_t get__propCroppingScalars_59() const { return ____propCroppingScalars_59; }
	inline int32_t* get_address_of__propCroppingScalars_59() { return &____propCroppingScalars_59; }
	inline void set__propCroppingScalars_59(int32_t value)
	{
		____propCroppingScalars_59 = value;
	}

	inline static int32_t get_offset_of_QuadIndices_63() { return static_cast<int32_t>(offsetof(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A_StaticFields, ___QuadIndices_63)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_QuadIndices_63() const { return ___QuadIndices_63; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_QuadIndices_63() { return &___QuadIndices_63; }
	inline void set_QuadIndices_63(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___QuadIndices_63 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___QuadIndices_63), (void*)value);
	}
};


// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;

public:
	inline static int32_t get_offset_of_m_Sprite_37() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Sprite_37)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_37() const { return ___m_Sprite_37; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_37() { return &___m_Sprite_37; }
	inline void set_m_Sprite_37(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_38() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_OverrideSprite_38)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_OverrideSprite_38() const { return ___m_OverrideSprite_38; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_OverrideSprite_38() { return &___m_OverrideSprite_38; }
	inline void set_m_OverrideSprite_38(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_OverrideSprite_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_39() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Type_39)); }
	inline int32_t get_m_Type_39() const { return ___m_Type_39; }
	inline int32_t* get_address_of_m_Type_39() { return &___m_Type_39; }
	inline void set_m_Type_39(int32_t value)
	{
		___m_Type_39 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_40() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PreserveAspect_40)); }
	inline bool get_m_PreserveAspect_40() const { return ___m_PreserveAspect_40; }
	inline bool* get_address_of_m_PreserveAspect_40() { return &___m_PreserveAspect_40; }
	inline void set_m_PreserveAspect_40(bool value)
	{
		___m_PreserveAspect_40 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_41() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillCenter_41)); }
	inline bool get_m_FillCenter_41() const { return ___m_FillCenter_41; }
	inline bool* get_address_of_m_FillCenter_41() { return &___m_FillCenter_41; }
	inline void set_m_FillCenter_41(bool value)
	{
		___m_FillCenter_41 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_42() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillMethod_42)); }
	inline int32_t get_m_FillMethod_42() const { return ___m_FillMethod_42; }
	inline int32_t* get_address_of_m_FillMethod_42() { return &___m_FillMethod_42; }
	inline void set_m_FillMethod_42(int32_t value)
	{
		___m_FillMethod_42 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_43() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillAmount_43)); }
	inline float get_m_FillAmount_43() const { return ___m_FillAmount_43; }
	inline float* get_address_of_m_FillAmount_43() { return &___m_FillAmount_43; }
	inline void set_m_FillAmount_43(float value)
	{
		___m_FillAmount_43 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_44() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillClockwise_44)); }
	inline bool get_m_FillClockwise_44() const { return ___m_FillClockwise_44; }
	inline bool* get_address_of_m_FillClockwise_44() { return &___m_FillClockwise_44; }
	inline void set_m_FillClockwise_44(bool value)
	{
		___m_FillClockwise_44 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_45() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillOrigin_45)); }
	inline int32_t get_m_FillOrigin_45() const { return ___m_FillOrigin_45; }
	inline int32_t* get_address_of_m_FillOrigin_45() { return &___m_FillOrigin_45; }
	inline void set_m_FillOrigin_45(int32_t value)
	{
		___m_FillOrigin_45 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_46() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_AlphaHitTestMinimumThreshold_46)); }
	inline float get_m_AlphaHitTestMinimumThreshold_46() const { return ___m_AlphaHitTestMinimumThreshold_46; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_46() { return &___m_AlphaHitTestMinimumThreshold_46; }
	inline void set_m_AlphaHitTestMinimumThreshold_46(float value)
	{
		___m_AlphaHitTestMinimumThreshold_46 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_47() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Tracked_47)); }
	inline bool get_m_Tracked_47() const { return ___m_Tracked_47; }
	inline bool* get_address_of_m_Tracked_47() { return &___m_Tracked_47; }
	inline void set_m_Tracked_47(bool value)
	{
		___m_Tracked_47 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_48() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_UseSpriteMesh_48)); }
	inline bool get_m_UseSpriteMesh_48() const { return ___m_UseSpriteMesh_48; }
	inline bool* get_address_of_m_UseSpriteMesh_48() { return &___m_UseSpriteMesh_48; }
	inline void set_m_UseSpriteMesh_48(bool value)
	{
		___m_UseSpriteMesh_48 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_49() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PixelsPerUnitMultiplier_49)); }
	inline float get_m_PixelsPerUnitMultiplier_49() const { return ___m_PixelsPerUnitMultiplier_49; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_49() { return &___m_PixelsPerUnitMultiplier_49; }
	inline void set_m_PixelsPerUnitMultiplier_49(float value)
	{
		___m_PixelsPerUnitMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_50() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_CachedReferencePixelsPerUnit_50)); }
	inline float get_m_CachedReferencePixelsPerUnit_50() const { return ___m_CachedReferencePixelsPerUnit_50; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_50() { return &___m_CachedReferencePixelsPerUnit_50; }
	inline void set_m_CachedReferencePixelsPerUnit_50(float value)
	{
		___m_CachedReferencePixelsPerUnit_50 = value;
	}
};

struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t815A476B0A21E183042059E705F9E505478CD8AE * ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_36() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_ETC1DefaultUI_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_ETC1DefaultUI_36() const { return ___s_ETC1DefaultUI_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_ETC1DefaultUI_36() { return &___s_ETC1DefaultUI_36; }
	inline void set_s_ETC1DefaultUI_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_ETC1DefaultUI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_51() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_VertScratch_51)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_VertScratch_51() const { return ___s_VertScratch_51; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_VertScratch_51() { return &___s_VertScratch_51; }
	inline void set_s_VertScratch_51(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_VertScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_52() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_UVScratch_52)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_UVScratch_52() const { return ___s_UVScratch_52; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_UVScratch_52() { return &___s_UVScratch_52; }
	inline void set_s_UVScratch_52(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_UVScratch_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_53() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Xy_53)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Xy_53() const { return ___s_Xy_53; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Xy_53() { return &___s_Xy_53; }
	inline void set_s_Xy_53(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Xy_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_54() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Uv_54)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Uv_54() const { return ___s_Uv_54; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Uv_54() { return &___s_Uv_54; }
	inline void set_s_Uv_54(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Uv_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_54), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_55() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___m_TrackedTexturelessImages_55)); }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE * get_m_TrackedTexturelessImages_55() const { return ___m_TrackedTexturelessImages_55; }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE ** get_address_of_m_TrackedTexturelessImages_55() { return &___m_TrackedTexturelessImages_55; }
	inline void set_m_TrackedTexturelessImages_55(List_1_t815A476B0A21E183042059E705F9E505478CD8AE * value)
	{
		___m_TrackedTexturelessImages_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_55), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_56() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Initialized_56)); }
	inline bool get_s_Initialized_56() const { return ___s_Initialized_56; }
	inline bool* get_address_of_s_Initialized_56() { return &___s_Initialized_56; }
	inline void set_s_Initialized_56(bool value)
	{
		___s_Initialized_56 = value;
	}
};


// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader[]
struct HTTPHeaderU5BU5D_tDC8254FC7749720A2ED8557C46398F574DFAA8B0  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  m_Items[1];

public:
	inline HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___header_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___value_1), (void*)NULL);
		#endif
	}
	inline HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___header_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___value_1), (void*)NULL);
		#endif
	}
};


// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Int32Enum,System.Int32Enum>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction_3__ctor_mABB5EF84732071D2C9DFC7386EA237B8FA1247C5_gshared (UnityAction_3_t5380F08944E494DE28EBADE9711BB8DF4EC87123 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_gshared_inline (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_gshared_inline (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mA4972EA6AD7EDA75836F78B588F59FC023AB4DDA_gshared (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mED07ADBCE34435B4A44D92F49320193766170074_gshared (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * __this, HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  ___item0, const RuntimeMethod* method);
// System.Void System.Func`2<System.Single,System.Single>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_gshared (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);

// System.Void VideoPlayerController::InitComponent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoPlayerController_InitComponent_mFA3B6FF2C70D94290C042B40C7763AB528EF02E6 (VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator VideoPlayerController::StartVideoPlaying()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* VideoPlayerController_StartVideoPlaying_mBA95547320C9617A544BC8A692280E0F2040F6EF (VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void VideoPlayerController/<StartVideoPlaying>d__13::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartVideoPlayingU3Ed__13__ctor_mA06BAE3A6CF5203156A73FD7AF9108A145AF0EC6 (U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87 (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method);
// System.String System.Single::ToString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_m15F10F2AFF80750906CEFCFB456EBA84F9D2E8D7 (float* __this, String_t* ___format0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MediaPlayer_Play_m893CBBB81F9F455B7AA764398A50A007D3C28F6C (MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Rewind(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MediaPlayer_Rewind_m0CCEAC97B35578A2390B8BB6D91B1DE6E74C4C6E (MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * __this, bool ___pause0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C (float ___value0, const RuntimeMethod* method);
// RenderHeads.Media.AVProVideo.MediaPlayer VideoVCR::get_PlayingPlayer()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___exists0, const RuntimeMethod* method);
// RenderHeads.Media.AVProVideo.MediaPlayerEvent RenderHeads.Media.AVProVideo.MediaPlayer::get_Events()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MediaPlayerEvent_t0E890CDB48C2168A48E68F40D0E63D1B3C4A0107 * MediaPlayer_get_Events_mA5CD0868386223CB631AD42B495F8A01D1D63C1D (MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`3<RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_3__ctor_mA257195F2A551B515268BFCB39C301E4ACE73057 (UnityAction_3_t8232B65BAFB61715E1BA6DC746E10C91EB537ADB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_3_t8232B65BAFB61715E1BA6DC746E10C91EB537ADB *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_3__ctor_mABB5EF84732071D2C9DFC7386EA237B8FA1247C5_gshared)(__this, ___object0, ___method1, method);
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayerEvent::AddListener(UnityEngine.Events.UnityAction`3<RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MediaPlayerEvent_AddListener_m9439DDAFF5E3D1DF3772724199060F845ACCAD76 (MediaPlayerEvent_t0E890CDB48C2168A48E68F40D0E63D1B3C4A0107 * __this, UnityAction_3_t8232B65BAFB61715E1BA6DC746E10C91EB537ADB * ___call0, const RuntimeMethod* method);
// System.Void VideoVCR::OnOpenVideoFile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoVCR_OnOpenVideoFile_m63A5EFAA337F2DE7919B1A623776E3B85CE4E310 (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method);
// RenderHeads.Media.AVProVideo.MediaPlayer VideoVCR::get_LoadingPlayer()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * VideoVCR_get_LoadingPlayer_m8B3CEF700E0B37E6D1287D8F37E9264BF9C1B67E_inline (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method);
// System.String System.IO.Path::Combine(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_Combine_mC22E47A9BB232F02ED3B6B5F6DD53338D37782EF (String_t* ___path10, String_t* ___path21, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C (String_t* ___value0, const RuntimeMethod* method);
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer::CloseVideo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MediaPlayer_CloseVideo_mB754997E2B47198216C0BFCDF6249EF07B9F1DDD (MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::OpenVideoFromFile(RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation,System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MediaPlayer_OpenVideoFromFile_mD7D917C6B56F5CC9B76A04D8E4B24219277BF881 (MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * __this, int32_t ___location0, String_t* ___path1, bool ___autoPlay2, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828 (const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_get_one_m9B2AFD26404B6DD0F520D19FC7F79371C5C18B42 (const RuntimeMethod* method);
// UnityEngine.UI.Image/Type UnityEngine.UI.Image::get_type()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Image_get_type_m730305AA6DAA0AF5C57A8AD2C1B8A97E6B0B8229_inline (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_fillAmount(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_fillAmount_m1D28CFC9B15A45AB6C561AA42BD8F305605E9E3C (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector2_set_Item_m817FDD0709F52F09ECBB949C29DEE88E73889CAD (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_anchorMin_mD9E6E95890B701A5190C12F5AE42E622246AF798 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_anchorMax_m67E04F54B5122804E32019D5FAE50C21CC67651D (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// System.Void VideoVCR::OnNewMediaReady()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoVCR_OnNewMediaReady_mEDC2336A6CEA55B77B6A3A7631F66019A527CF21 (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_mF138386223A07CBD4CE94672757E39D0EF718092 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, int32_t ___width0, int32_t ___height1, int32_t ___textureFormat2, bool ___mipChain3, const RuntimeMethod* method);
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Pause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MediaPlayer_Pause_m7194C52FC0A95953B2582B2615AE700418954A94 (MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * __this, const RuntimeMethod* method);
// System.Void VideoVCR::OnVideoSeekSlider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoVCR_OnVideoSeekSlider_m6BA20F479F74EC5A5A6ABBF271ED25B4F5C947AB (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetInteger(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetInteger_mFB04A03AF6C24978BF2BDE9161243F8F6B9762C5 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, int32_t ___value1, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// UnityEngine.Camera RenderHeads.Media.AVProVideo.MediaPlayer::GetDummyCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * MediaPlayer_GetDummyCamera_m1ECE958660114FCD2F55A4A438E63A6F704A9DD1 (const RuntimeMethod* method);
// RenderHeads.Media.AVProVideo.Orientation RenderHeads.Media.AVProVideo.Helper::GetOrientation(System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Helper_GetOrientation_mE372CD72592447C80D008B0FBF653ECC87FB7627 (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___t0, const RuntimeMethod* method);
// UnityEngine.Texture2D RenderHeads.Media.AVProVideo.Helper::GetReadableTexture(UnityEngine.Texture,System.Boolean,RenderHeads.Media.AVProVideo.Orientation,UnityEngine.Texture2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * Helper_GetReadableTexture_mF4D3B41AFFF220B865DD21C89F30F920DA919777 (Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___inputTexture0, bool ___requiresVerticalFlip1, int32_t ___ori2, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___targetTexture3, const RuntimeMethod* method);
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame::Invoke(UnityEngine.Texture2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProcessExtractedFrame_Invoke_m93D4170ADCD901788FA035AD906788CD07C8ED5F (ProcessExtractedFrame_t9BDA228D84FCD6905FA985C6759CD5E40590AA45 * __this, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___extractedFrame0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Behaviour::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800 (Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Application::get_isPlaying()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567 (const RuntimeMethod* method);
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Get(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * UnityWebRequest_Get_m9C24DB3E8BED0B0886F28DCD982A4741A9903B1A (String_t* ___uri0, const RuntimeMethod* method);
// UnityEngine.Networking.UnityWebRequestAsyncOperation UnityEngine.Networking.UnityWebRequest::SendWebRequest()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396 * UnityWebRequest_SendWebRequest_m990921023F56ECB8FF8C118894A317EB6E2F5B50 (UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isNetworkError()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityWebRequest_get_isNetworkError_m0126D38DA9B39159CB12F8DE0C3962A4BEEE5C03 (UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * __this, const RuntimeMethod* method);
// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::get_downloadHandler()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * UnityWebRequest_get_downloadHandler_mCE0A0C53A63419FE5AE25915AFB36EABE294C732 (UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * __this, const RuntimeMethod* method);
// System.String UnityEngine.Networking.DownloadHandler::get_text()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* DownloadHandler_get_text_mD89D7125640800A8F5C4B9401C080C405953828A (DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * __this, const RuntimeMethod* method);
// System.String UnityEngine.Networking.UnityWebRequest::get_error()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityWebRequest_get_error_m32B69D2365C1FE2310B5936C7C295B71A92CC2B4 (UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78 (String_t* ___str00, String_t* ___str11, String_t* ___str22, String_t* ___str33, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_mEFF048E5541EE45362C0AAD829E3FA4C2CAB9199 (RuntimeObject * ___message0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___context1, const RuntimeMethod* method);
// System.Void UnityEngine.Networking.UnityWebRequest::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityWebRequest_Dispose_m8032472F6BC2EC4FEE017DE7E4C440078BC4E1C8 (UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader>::get_Count()
inline int32_t List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_inline (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 *, const RuntimeMethod*))List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_gshared_inline)(__this, method);
}
// System.Void System.Text.StringBuilder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder__ctor_m5A81DE19E748F748E19FF13FB6FFD2547F9212D9 (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1 (StringBuilder_t * __this, String_t* ___value0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader>::get_Item(System.Int32)
inline HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_inline (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  (*) (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 *, int32_t, const RuntimeMethod*))List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_gshared_inline)(__this, ___index0, method);
}
// System.String RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::StringAsJsonString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlatformOptions_StringAsJsonString_m7A9A91C0D27FFA0DAFB694F2BFAB563817DDDD1A (String_t* ___str0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::AppendFormat(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_AppendFormat_m37B348187DD9186C2451ACCA3DBC4ABCD4632AD4 (StringBuilder_t * __this, String_t* ___format0, RuntimeObject * ___arg01, RuntimeObject * ___arg12, const RuntimeMethod* method);
// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::IsModified()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool PlatformOptions_IsModified_m9329B32428B8862D0994558AF2B7C086A9DA8651_inline (PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F * __this, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader> RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::ParseJsonHTTPHeadersIntoHTTPHeaderList(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * PlatformOptions_ParseJsonHTTPHeadersIntoHTTPHeaderList_m0C5A914EF74EA3DB7ABCEB1FF75F5B782E38E59D (String_t* ___httpHeaderJson0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader>::.ctor()
inline void List_1__ctor_mA4972EA6AD7EDA75836F78B588F59FC023AB4DDA (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 *, const RuntimeMethod*))List_1__ctor_mA4972EA6AD7EDA75836F78B588F59FC023AB4DDA_gshared)(__this, method);
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlatformOptions__ctor_m0765FB48C8311B03E858B0E7ACB26870CA7E1FD5 (PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F * __this, const RuntimeMethod* method);
// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::IsModified()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OptionsApple_IsModified_m97B8AED361C97F06BDA0D49A52D0019DD5BC53D5 (OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E * __this, const RuntimeMethod* method);
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionsApple__ctor_m2E26273BAAC19F3078E37CB3B52FC00BBB5EA0CC (OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E * __this, const RuntimeMethod* method);
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsIOS::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionsIOS__ctor_m71CBF1B11498CE1D3234585F5E17D693B145E506 (OptionsIOS_t868D1748A3E07299A108C1E60F97753CA38954A5 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
inline int32_t List_1_get_Count_m199DB87BCE947106FBA38E19FDFE80CB65B61144_inline (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9 (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70 (String_t* __this, int32_t ___index0, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B (String_t* __this, int32_t ___startIndex0, int32_t ___length1, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder__ctor_m9305A36F9CF53EDD80D132428999934C68904C77 (StringBuilder_t * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_m1ADA3C16E40BF253BCDB5F9579B4DBA9C3E5B22E (StringBuilder_t * __this, Il2CppChar ___value0, const RuntimeMethod* method);
// System.Boolean System.Char::IsWhiteSpace(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Char_IsWhiteSpace_m99A5E1BE1EB9F17EA530A67A607DA8C260BCBF99 (Il2CppChar ___c0, const RuntimeMethod* method);
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HTTPHeader__ctor_mBD26E64BA6EB557D29C8E5E346802BD3BF6E199E (HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED * __this, String_t* ___header0, String_t* ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader>::Add(!0)
inline void List_1_Add_mED07ADBCE34435B4A44D92F49320193766170074 (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * __this, HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 *, HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED , const RuntimeMethod*))List_1_Add_mED07ADBCE34435B4A44D92F49320193766170074_gshared)(__this, ___item0, method);
}
// System.Void UnityEngine.Debug::LogWarning(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_m24085D883C9E74D7AB423F0625E13259923960E7 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void System.Func`2<System.Single,System.Single>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_gshared)(__this, ___object0, ___method1, method);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A (int32_t ___minInclusive0, int32_t ___maxExclusive1, const RuntimeMethod* method);
// System.Func`2<System.Single,System.Single> RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::GetFunction(RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing/Preset)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * Easing_GetFunction_m6BF9C691739142093824E65BF9D0B3959716E3B5 (int32_t ___preset0, const RuntimeMethod* method);
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::PowerEaseIn(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_PowerEaseIn_m452740837B6A23AB4DA085C09A56239B2D61E9A3 (float ___t0, float ___power1, const RuntimeMethod* method);
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::PowerEaseOut(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_PowerEaseOut_m9029AE6D2160B8497333B9D381DDA8B364311565 (float ___t0, float ___power1, const RuntimeMethod* method);
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::PowerEaseInOut(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_PowerEaseInOut_m0C91FD6F72C67D6C21A0BAC622714175010F60C8 (float ___t0, float ___power1, const RuntimeMethod* method);
// System.String System.Guid::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Guid_ToString_mA3AB7742FB0E04808F580868E82BDEB93187FB75 (Guid_t * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager/NamedPrefab::.ctor(System.Guid,UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NamedPrefab__ctor_m4C5A1EAC954851954E10ADF5F4ED646967BAB252 (NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E * __this, Guid_t  ___guid0, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___prefab1, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4 (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * __this, float ___seconds0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method);
// System.Boolean RenderHeads.Media.AVProVideo.Demos.SimpleController::VideoIsReady(RenderHeads.Media.AVProVideo.MediaPlayer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SimpleController_VideoIsReady_m5F71FECD8B7EB71A09C8A25336E6E676A454242B (MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ___mp0, const RuntimeMethod* method);
// System.Boolean RenderHeads.Media.AVProVideo.Demos.SimpleController::AudioIsReady(RenderHeads.Media.AVProVideo.MediaPlayer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SimpleController_AudioIsReady_mEFF0519289C815E43EE68EAC005C286BE2B5907E (MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ___mp0, const RuntimeMethod* method);
// System.Void Crosstales.OnlineCheck.Demo.SpeedCheck::OnClickSpeedTestBtn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpeedCheck_OnClickSpeedTestBtn_m3F6CB2EB19F3FD247BA9292D5B0CD2EA9893EAAD (SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5 * __this, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL GetRenderEventFunc();
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _GetWidth(int32_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _GetHeight(int32_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _GetTextureHandle(int32_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
IL2CPP_EXTERN_C int64_t DEFAULT_CALL _GetDuration(int32_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _GetLastErrorCode(int32_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _GetFrameCount(int32_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
IL2CPP_EXTERN_C float DEFAULT_CALL _GetVideoDisplayRate(int32_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _CanPlay(int32_t);
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VideoPlayerController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoPlayerController_Start_mBCAB3295960ECCA9AD99039F2F176B1D9E6A8FCA (VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * __this, const RuntimeMethod* method)
{
	{
		// InitComponent();
		VideoPlayerController_InitComponent_mFA3B6FF2C70D94290C042B40C7763AB528EF02E6(__this, /*hidden argument*/NULL);
		// StartCoroutine(StartVideoPlaying());
		RuntimeObject* L_0;
		L_0 = VideoPlayerController_StartVideoPlaying_mBA95547320C9617A544BC8A692280E0F2040F6EF(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_1;
		L_1 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void VideoPlayerController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoPlayerController_Update_m348487057CC7EB8BAB768D127A11DBDE72BC0861 (VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void VideoPlayerController::InitComponent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoPlayerController_InitComponent_mFA3B6FF2C70D94290C042B40C7763AB528EF02E6 (VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * __this, const RuntimeMethod* method)
{
	{
		// displayUGUIA._mediaPlayer = mediaPlayerA;
		DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * L_0 = __this->get_displayUGUIA_9();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_1 = __this->get_mediaPlayerA_4();
		NullCheck(L_0);
		L_0->set__mediaPlayer_36(L_1);
		// displayUGUIB._mediaPlayer = mediaPlayerB;
		DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * L_2 = __this->get_displayUGUIB_10();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_3 = __this->get_mediaPlayerB_5();
		NullCheck(L_2);
		L_2->set__mediaPlayer_36(L_3);
		// displayUGUIC._mediaPlayer = mediaPlayerC;
		DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * L_4 = __this->get_displayUGUIC_11();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_5 = __this->get_mediaPlayerC_6();
		NullCheck(L_4);
		L_4->set__mediaPlayer_36(L_5);
		// displayUGUID._mediaPlayer = mediaPlayerD;
		DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * L_6 = __this->get_displayUGUID_12();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_7 = __this->get_mediaPlayerD_7();
		NullCheck(L_6);
		L_6->set__mediaPlayer_36(L_7);
		// displayUGUIMain._mediaPlayer = mediaPlayerA;
		DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * L_8 = __this->get_displayUGUIMain_8();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_9 = __this->get_mediaPlayerA_4();
		NullCheck(L_8);
		L_8->set__mediaPlayer_36(L_9);
		// }
		return;
	}
}
// System.Collections.IEnumerator VideoPlayerController::StartVideoPlaying()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* VideoPlayerController_StartVideoPlaying_mBA95547320C9617A544BC8A692280E0F2040F6EF (VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723 * L_0 = (U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723 *)il2cpp_codegen_object_new(U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723_il2cpp_TypeInfo_var);
		U3CStartVideoPlayingU3Ed__13__ctor_mA06BAE3A6CF5203156A73FD7AF9108A145AF0EC6(L_0, 0, /*hidden argument*/NULL);
		U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723 * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void VideoPlayerController::OnVideoButtonClick(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoPlayerController_OnVideoButtonClick_m4524089D8D6A643A7F464F8EAA9BFC87321154AC (VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * __this, String_t* ___mediaPlayerId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral069BDB2BE9E83605F3B9AB20CB78B6D7E09B49FC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral271909F8F2564394AD0733493FE831469E5734A1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral59569CD1945760103A81F13277CC2DEBE8564737);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB2FF4E9D9185B1A580BC4440E2FC284851F9F147);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___mediaPlayerId0;
		if (!L_0)
		{
			goto IL_007f;
		}
	}
	{
		String_t* L_1 = ___mediaPlayerId0;
		bool L_2;
		L_2 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, _stringLiteral271909F8F2564394AD0733493FE831469E5734A1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_3 = ___mediaPlayerId0;
		bool L_4;
		L_4 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_3, _stringLiteralB2FF4E9D9185B1A580BC4440E2FC284851F9F147, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_004a;
		}
	}
	{
		String_t* L_5 = ___mediaPlayerId0;
		bool L_6;
		L_6 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_5, _stringLiteral069BDB2BE9E83605F3B9AB20CB78B6D7E09B49FC, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_005c;
		}
	}
	{
		String_t* L_7 = ___mediaPlayerId0;
		bool L_8;
		L_8 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_7, _stringLiteral59569CD1945760103A81F13277CC2DEBE8564737, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_006e;
		}
	}
	{
		return;
	}

IL_0038:
	{
		// displayUGUIMain._mediaPlayer = mediaPlayerA;
		DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * L_9 = __this->get_displayUGUIMain_8();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_10 = __this->get_mediaPlayerA_4();
		NullCheck(L_9);
		L_9->set__mediaPlayer_36(L_10);
		// break;
		return;
	}

IL_004a:
	{
		// displayUGUIMain._mediaPlayer = mediaPlayerB;
		DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * L_11 = __this->get_displayUGUIMain_8();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_12 = __this->get_mediaPlayerB_5();
		NullCheck(L_11);
		L_11->set__mediaPlayer_36(L_12);
		// break;
		return;
	}

IL_005c:
	{
		// displayUGUIMain._mediaPlayer = mediaPlayerC;
		DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * L_13 = __this->get_displayUGUIMain_8();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_14 = __this->get_mediaPlayerC_6();
		NullCheck(L_13);
		L_13->set__mediaPlayer_36(L_14);
		// break;
		return;
	}

IL_006e:
	{
		// displayUGUIMain._mediaPlayer = mediaPlayerD;
		DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * L_15 = __this->get_displayUGUIMain_8();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_16 = __this->get_mediaPlayerD_7();
		NullCheck(L_15);
		L_15->set__mediaPlayer_36(L_16);
	}

IL_007f:
	{
		// }
		return;
	}
}
// System.Void VideoPlayerController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoPlayerController__ctor_m37CB855AE1301D1DD45D07E62CB382896A550D46 (VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * __this, const RuntimeMethod* method)
{
	{
		// public float playStartDelay = 5f;
		__this->set_playStartDelay_13((5.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VideoTimeDisplay::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoTimeDisplay_Start_mFA4D6284EDA3B3AB7D7022BEAB98C9527968AB22 (VideoTimeDisplay_t99E522A668D79E64AD5F7E5D3676A8B3D78B8832 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void VideoTimeDisplay::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoTimeDisplay_Update_m524CCDF8A8837D4396BCE9549A1880CB038A73D1 (VideoTimeDisplay_t99E522A668D79E64AD5F7E5D3676A8B3D78B8832 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1DA847B0C8711F8529FBC7BC20711A1361A8B323);
		s_Il2CppMethodInitialized = true;
	}
	{
		// time = mediaPlayer.Control.GetCurrentTimeMs();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_0 = __this->get_mediaPlayer_4();
		NullCheck(L_0);
		RuntimeObject* L_1;
		L_1 = VirtFuncInvoker0< RuntimeObject* >::Invoke(5 /* RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.MediaPlayer::get_Control() */, L_0);
		NullCheck(L_1);
		float L_2;
		L_2 = InterfaceFuncInvoker0< float >::Invoke(22 /* System.Single RenderHeads.Media.AVProVideo.IMediaControl::GetCurrentTimeMs() */, IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var, L_1);
		__this->set_time_5(L_2);
		// duration = mediaPlayer.Info.GetDurationMs();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_3 = __this->get_mediaPlayer_4();
		NullCheck(L_3);
		RuntimeObject* L_4;
		L_4 = VirtFuncInvoker0< RuntimeObject* >::Invoke(4 /* RenderHeads.Media.AVProVideo.IMediaInfo RenderHeads.Media.AVProVideo.MediaPlayer::get_Info() */, L_3);
		NullCheck(L_4);
		float L_5;
		L_5 = InterfaceFuncInvoker0< float >::Invoke(0 /* System.Single RenderHeads.Media.AVProVideo.IMediaInfo::GetDurationMs() */, IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4_il2cpp_TypeInfo_var, L_4);
		__this->set_duration_6(L_5);
		// d = Mathf.Clamp(time / duration, 0.0f, 1.0f);
		float L_6 = __this->get_time_5();
		float L_7 = __this->get_duration_6();
		float L_8;
		L_8 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(((float)((float)L_6/(float)L_7)), (0.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_d_7(L_8);
		// timerText.text = d.ToString("F4");
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_9 = __this->get_timerText_8();
		float* L_10 = __this->get_address_of_d_7();
		String_t* L_11;
		L_11 = Single_ToString_m15F10F2AFF80750906CEFCFB456EBA84F9D2E8D7((float*)L_10, _stringLiteral1DA847B0C8711F8529FBC7BC20711A1361A8B323, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_9, L_11);
		// }
		return;
	}
}
// System.Void VideoTimeDisplay::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoTimeDisplay__ctor_m5CD881BC6D41DF1DA09C7632EEBC7D037CCCAB93 (VideoTimeDisplay_t99E522A668D79E64AD5F7E5D3676A8B3D78B8832 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RenderHeads.Media.AVProVideo.Demos.VideoTrigger::OnTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoTrigger_OnTriggerEnter_m8E48C1631203B0C8B819052BCC1DAC93DB05B355 (VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_mediaPlayer != null)
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_0 = __this->get__mediaPlayer_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		// _mediaPlayer.Play();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_2 = __this->get__mediaPlayer_4();
		NullCheck(L_2);
		MediaPlayer_Play_m893CBBB81F9F455B7AA764398A50A007D3C28F6C(L_2, /*hidden argument*/NULL);
		// _fadeDirection = 1f;
		__this->set__fadeDirection_7((1.0f));
	}

IL_0024:
	{
		// }
		return;
	}
}
// System.Void RenderHeads.Media.AVProVideo.Demos.VideoTrigger::OnTriggerExit(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoTrigger_OnTriggerExit_m0251AFF2B0005DED8E6A0A346D9A53F30A770DF5 (VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_mediaPlayer != null)
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_0 = __this->get__mediaPlayer_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// _fadeDirection = -1f;
		__this->set__fadeDirection_7((-1.0f));
	}

IL_0019:
	{
		// }
		return;
	}
}
// System.Void RenderHeads.Media.AVProVideo.Demos.VideoTrigger::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoTrigger_Update_mC8A9CCDE353D055366F4E4F77776323E831ECAC8 (VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// if (_fadeDirection != 0f)
		float L_0 = __this->get__fadeDirection_7();
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_00b8;
		}
	}
	{
		// float speed = 1000 / _fadeTimeMs;
		float L_1 = __this->get__fadeTimeMs_5();
		V_0 = ((float)((float)(1000.0f)/(float)L_1));
		// _fade += Time.deltaTime * _fadeDirection * speed;
		float L_2 = __this->get__fade_6();
		float L_3;
		L_3 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_4 = __this->get__fadeDirection_7();
		float L_5 = V_0;
		__this->set__fade_6(((float)il2cpp_codegen_add((float)L_2, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_3, (float)L_4)), (float)L_5)))));
		// if (_fade <= 0f)
		float L_6 = __this->get__fade_6();
		if ((!(((float)L_6) <= ((float)(0.0f)))))
		{
			goto IL_005e;
		}
	}
	{
		// _mediaPlayer.Rewind(true);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_7 = __this->get__mediaPlayer_4();
		NullCheck(L_7);
		MediaPlayer_Rewind_m0CCEAC97B35578A2390B8BB6D91B1DE6E74C4C6E(L_7, (bool)1, /*hidden argument*/NULL);
		// _fadeDirection = 0f;
		__this->set__fadeDirection_7((0.0f));
		// }
		goto IL_0076;
	}

IL_005e:
	{
		// else if (_fade >= 1f)
		float L_8 = __this->get__fade_6();
		if ((!(((float)L_8) >= ((float)(1.0f)))))
		{
			goto IL_0076;
		}
	}
	{
		// _fadeDirection = 0f;
		__this->set__fadeDirection_7((0.0f));
	}

IL_0076:
	{
		// _fade = Mathf.Clamp01(_fade);
		float L_9 = __this->get__fade_6();
		float L_10;
		L_10 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_9, /*hidden argument*/NULL);
		__this->set__fade_6(L_10);
		// if (_mediaPlayer != null && _mediaPlayer.Control != null)
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_11 = __this->get__mediaPlayer_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_12;
		L_12 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_11, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00b8;
		}
	}
	{
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_13 = __this->get__mediaPlayer_4();
		NullCheck(L_13);
		RuntimeObject* L_14;
		L_14 = VirtFuncInvoker0< RuntimeObject* >::Invoke(5 /* RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.MediaPlayer::get_Control() */, L_13);
		if (!L_14)
		{
			goto IL_00b8;
		}
	}
	{
		// _mediaPlayer.Control.SetVolume(_fade);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_15 = __this->get__mediaPlayer_4();
		NullCheck(L_15);
		RuntimeObject* L_16;
		L_16 = VirtFuncInvoker0< RuntimeObject* >::Invoke(5 /* RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.MediaPlayer::get_Control() */, L_15);
		float L_17 = __this->get__fade_6();
		NullCheck(L_16);
		InterfaceActionInvoker1< float >::Invoke(29 /* System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetVolume(System.Single) */, IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var, L_16, L_17);
	}

IL_00b8:
	{
		// }
		return;
	}
}
// System.Void RenderHeads.Media.AVProVideo.Demos.VideoTrigger::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoTrigger__ctor_mA7BE38FA20CA4D80F20E2872AA4B69CD9062483E (VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656 * __this, const RuntimeMethod* method)
{
	{
		// private float _fadeTimeMs = 500f;
		__this->set__fadeTimeMs_5((500.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// RenderHeads.Media.AVProVideo.MediaPlayer VideoVCR::get_PlayingPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method)
{
	{
		// return _mediaPlayer;
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_0 = __this->get__mediaPlayer_4();
		return L_0;
	}
}
// RenderHeads.Media.AVProVideo.MediaPlayer VideoVCR::get_LoadingPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * VideoVCR_get_LoadingPlayer_m8B3CEF700E0B37E6D1287D8F37E9264BF9C1B67E (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method)
{
	{
		// return _mediaPlayer;
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_0 = __this->get__mediaPlayer_4();
		return L_0;
	}
}
// System.Void VideoVCR::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoVCR_Start_m833675486A1F6707D25A6F64A4D84F9C5EEEA3B8 (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_3__ctor_mA257195F2A551B515268BFCB39C301E4ACE73057_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_3_t8232B65BAFB61715E1BA6DC746E10C91EB537ADB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&VideoVCR_OnVideoEvent_m5A55B048083EAAD1F09A39BA3416B3F44100CD7F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (PlayingPlayer)
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_0;
		L_0 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003b;
		}
	}
	{
		// PlayingPlayer.Events.AddListener(OnVideoEvent);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_2;
		L_2 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		MediaPlayerEvent_t0E890CDB48C2168A48E68F40D0E63D1B3C4A0107 * L_3;
		L_3 = MediaPlayer_get_Events_mA5CD0868386223CB631AD42B495F8A01D1D63C1D(L_2, /*hidden argument*/NULL);
		UnityAction_3_t8232B65BAFB61715E1BA6DC746E10C91EB537ADB * L_4 = (UnityAction_3_t8232B65BAFB61715E1BA6DC746E10C91EB537ADB *)il2cpp_codegen_object_new(UnityAction_3_t8232B65BAFB61715E1BA6DC746E10C91EB537ADB_il2cpp_TypeInfo_var);
		UnityAction_3__ctor_mA257195F2A551B515268BFCB39C301E4ACE73057(L_4, __this, (intptr_t)((intptr_t)VideoVCR_OnVideoEvent_m5A55B048083EAAD1F09A39BA3416B3F44100CD7F_RuntimeMethod_var), /*hidden argument*/UnityAction_3__ctor_mA257195F2A551B515268BFCB39C301E4ACE73057_RuntimeMethod_var);
		NullCheck(L_3);
		MediaPlayerEvent_AddListener_m9439DDAFF5E3D1DF3772724199060F845ACCAD76(L_3, L_4, /*hidden argument*/NULL);
		// if (PlayingPlayer.m_AutoOpen)
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_5;
		L_5 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = L_5->get_m_AutoOpen_6();
		// OnOpenVideoFile();
		VideoVCR_OnOpenVideoFile_m63A5EFAA337F2DE7919B1A623776E3B85CE4E310(__this, /*hidden argument*/NULL);
	}

IL_003b:
	{
		// }
		return;
	}
}
// System.Void VideoVCR::OnOpenVideoFile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoVCR_OnOpenVideoFile_m63A5EFAA337F2DE7919B1A623776E3B85CE4E310 (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_tF1D95B78D57C1C1211BA6633FF2AC22FD6C48921_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// LoadingPlayer.m_VideoPath = System.IO.Path.Combine(_folder, _videoFiles);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_0;
		L_0 = VideoVCR_get_LoadingPlayer_m8B3CEF700E0B37E6D1287D8F37E9264BF9C1B67E_inline(__this, /*hidden argument*/NULL);
		String_t* L_1 = __this->get__folder_10();
		String_t* L_2 = __this->get__videoFiles_11();
		IL2CPP_RUNTIME_CLASS_INIT(Path_tF1D95B78D57C1C1211BA6633FF2AC22FD6C48921_il2cpp_TypeInfo_var);
		String_t* L_3;
		L_3 = Path_Combine_mC22E47A9BB232F02ED3B6B5F6DD53338D37782EF(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->set_m_VideoPath_5(L_3);
		// if (string.IsNullOrEmpty(LoadingPlayer.m_VideoPath))
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_4;
		L_4 = VideoVCR_get_LoadingPlayer_m8B3CEF700E0B37E6D1287D8F37E9264BF9C1B67E_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = L_4->get_m_VideoPath_5();
		bool L_6;
		L_6 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003b;
		}
	}
	{
		// LoadingPlayer.CloseVideo();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_7;
		L_7 = VideoVCR_get_LoadingPlayer_m8B3CEF700E0B37E6D1287D8F37E9264BF9C1B67E_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		MediaPlayer_CloseVideo_mB754997E2B47198216C0BFCDF6249EF07B9F1DDD(L_7, /*hidden argument*/NULL);
		// }
		goto IL_0069;
	}

IL_003b:
	{
		// Debug.Log(LoadingPlayer.m_VideoPath);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_8;
		L_8 = VideoVCR_get_LoadingPlayer_m8B3CEF700E0B37E6D1287D8F37E9264BF9C1B67E_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9 = L_8->get_m_VideoPath_5();
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_9, /*hidden argument*/NULL);
		// LoadingPlayer.OpenVideoFromFile(_location, LoadingPlayer.m_VideoPath);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_10;
		L_10 = VideoVCR_get_LoadingPlayer_m8B3CEF700E0B37E6D1287D8F37E9264BF9C1B67E_inline(__this, /*hidden argument*/NULL);
		int32_t L_11 = __this->get__location_9();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_12;
		L_12 = VideoVCR_get_LoadingPlayer_m8B3CEF700E0B37E6D1287D8F37E9264BF9C1B67E_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13 = L_12->get_m_VideoPath_5();
		NullCheck(L_10);
		bool L_14;
		L_14 = MediaPlayer_OpenVideoFromFile_mD7D917C6B56F5CC9B76A04D8E4B24219277BF881(L_10, L_11, L_13, (bool)1, /*hidden argument*/NULL);
	}

IL_0069:
	{
		// if (_bufferedSliderRect != null)
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_15 = __this->get__bufferedSliderRect_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_16;
		L_16 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_15, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0088;
		}
	}
	{
		// _bufferedSliderImage = _bufferedSliderRect.GetComponent<Image>();
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_17 = __this->get__bufferedSliderRect_5();
		NullCheck(L_17);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_18;
		L_18 = Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB(L_17, /*hidden argument*/Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB_RuntimeMethod_var);
		__this->set__bufferedSliderImage_12(L_18);
	}

IL_0088:
	{
		// }
		return;
	}
}
// System.Void VideoVCR::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoVCR_Update_m3D389660CD8DB181C6F3BB44D523AB359057F6F4 (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_4;
	memset((&V_4), 0, sizeof(V_4));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_5;
	memset((&V_5), 0, sizeof(V_5));
	{
		// if (PlayingPlayer && PlayingPlayer.Info != null && PlayingPlayer.Info.GetDurationMs() > 0f)
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_0;
		L_0 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0178;
		}
	}
	{
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_2;
		L_2 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		RuntimeObject* L_3;
		L_3 = VirtFuncInvoker0< RuntimeObject* >::Invoke(4 /* RenderHeads.Media.AVProVideo.IMediaInfo RenderHeads.Media.AVProVideo.MediaPlayer::get_Info() */, L_2);
		if (!L_3)
		{
			goto IL_0178;
		}
	}
	{
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_4;
		L_4 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		RuntimeObject* L_5;
		L_5 = VirtFuncInvoker0< RuntimeObject* >::Invoke(4 /* RenderHeads.Media.AVProVideo.IMediaInfo RenderHeads.Media.AVProVideo.MediaPlayer::get_Info() */, L_4);
		NullCheck(L_5);
		float L_6;
		L_6 = InterfaceFuncInvoker0< float >::Invoke(0 /* System.Single RenderHeads.Media.AVProVideo.IMediaInfo::GetDurationMs() */, IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4_il2cpp_TypeInfo_var, L_5);
		if ((!(((float)L_6) > ((float)(0.0f)))))
		{
			goto IL_0178;
		}
	}
	{
		// float time = PlayingPlayer.Control.GetCurrentTimeMs();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_7;
		L_7 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		RuntimeObject* L_8;
		L_8 = VirtFuncInvoker0< RuntimeObject* >::Invoke(5 /* RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.MediaPlayer::get_Control() */, L_7);
		NullCheck(L_8);
		float L_9;
		L_9 = InterfaceFuncInvoker0< float >::Invoke(22 /* System.Single RenderHeads.Media.AVProVideo.IMediaControl::GetCurrentTimeMs() */, IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var, L_8);
		// float duration = PlayingPlayer.Info.GetDurationMs();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_10;
		L_10 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		RuntimeObject* L_11;
		L_11 = VirtFuncInvoker0< RuntimeObject* >::Invoke(4 /* RenderHeads.Media.AVProVideo.IMediaInfo RenderHeads.Media.AVProVideo.MediaPlayer::get_Info() */, L_10);
		NullCheck(L_11);
		float L_12;
		L_12 = InterfaceFuncInvoker0< float >::Invoke(0 /* System.Single RenderHeads.Media.AVProVideo.IMediaInfo::GetDurationMs() */, IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4_il2cpp_TypeInfo_var, L_11);
		V_0 = L_12;
		// float d = Mathf.Clamp(time / duration, 0.0f, 1.0f);
		float L_13 = V_0;
		float L_14;
		L_14 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(((float)((float)L_9/(float)L_13)), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_1 = L_14;
		// _setVideoSeekSliderValue = d;
		float L_15 = V_1;
		__this->set__setVideoSeekSliderValue_7(L_15);
		// _videoSeekSlider.value = d;
		Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * L_16 = __this->get__videoSeekSlider_6();
		float L_17 = V_1;
		NullCheck(L_16);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_16, L_17);
		// if (_bufferedSliderRect != null)
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_18 = __this->get__bufferedSliderRect_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_19;
		L_19 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_18, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0178;
		}
	}
	{
		// if (PlayingPlayer.Control.IsBuffering())
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_20;
		L_20 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		RuntimeObject* L_21;
		L_21 = VirtFuncInvoker0< RuntimeObject* >::Invoke(5 /* RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.MediaPlayer::get_Control() */, L_20);
		NullCheck(L_21);
		bool L_22;
		L_22 = InterfaceFuncInvoker0< bool >::Invoke(14 /* System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsBuffering() */, IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var, L_21);
		if (!L_22)
		{
			goto IL_0178;
		}
	}
	{
		// float t1 = 0f;
		V_2 = (0.0f);
		// float t2 = PlayingPlayer.Control.GetBufferingProgress();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_23;
		L_23 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		RuntimeObject* L_24;
		L_24 = VirtFuncInvoker0< RuntimeObject* >::Invoke(5 /* RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.MediaPlayer::get_Control() */, L_23);
		NullCheck(L_24);
		float L_25;
		L_25 = InterfaceFuncInvoker0< float >::Invoke(37 /* System.Single RenderHeads.Media.AVProVideo.IMediaControl::GetBufferingProgress() */, IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var, L_24);
		V_3 = L_25;
		// if (t2 <= 0f)
		float L_26 = V_3;
		if ((!(((float)L_26) <= ((float)(0.0f)))))
		{
			goto IL_0114;
		}
	}
	{
		// if (PlayingPlayer.Control.GetBufferedTimeRangeCount() > 0)
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_27;
		L_27 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		RuntimeObject* L_28;
		L_28 = VirtFuncInvoker0< RuntimeObject* >::Invoke(5 /* RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.MediaPlayer::get_Control() */, L_27);
		NullCheck(L_28);
		int32_t L_29;
		L_29 = InterfaceFuncInvoker0< int32_t >::Invoke(38 /* System.Int32 RenderHeads.Media.AVProVideo.IMediaControl::GetBufferedTimeRangeCount() */, IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var, L_28);
		if ((((int32_t)L_29) <= ((int32_t)0)))
		{
			goto IL_0114;
		}
	}
	{
		// PlayingPlayer.Control.GetBufferedTimeRange(0, ref t1, ref t2);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_30;
		L_30 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_30);
		RuntimeObject* L_31;
		L_31 = VirtFuncInvoker0< RuntimeObject* >::Invoke(5 /* RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.MediaPlayer::get_Control() */, L_30);
		NullCheck(L_31);
		bool L_32;
		L_32 = InterfaceFuncInvoker3< bool, int32_t, float*, float* >::Invoke(39 /* System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::GetBufferedTimeRange(System.Int32,System.Single&,System.Single&) */, IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var, L_31, 0, (float*)(&V_2), (float*)(&V_3));
		// t1 /= PlayingPlayer.Info.GetDurationMs();
		float L_33 = V_2;
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_34;
		L_34 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_34);
		RuntimeObject* L_35;
		L_35 = VirtFuncInvoker0< RuntimeObject* >::Invoke(4 /* RenderHeads.Media.AVProVideo.IMediaInfo RenderHeads.Media.AVProVideo.MediaPlayer::get_Info() */, L_34);
		NullCheck(L_35);
		float L_36;
		L_36 = InterfaceFuncInvoker0< float >::Invoke(0 /* System.Single RenderHeads.Media.AVProVideo.IMediaInfo::GetDurationMs() */, IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4_il2cpp_TypeInfo_var, L_35);
		V_2 = ((float)((float)L_33/(float)L_36));
		// t2 /= PlayingPlayer.Info.GetDurationMs();
		float L_37 = V_3;
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_38;
		L_38 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_38);
		RuntimeObject* L_39;
		L_39 = VirtFuncInvoker0< RuntimeObject* >::Invoke(4 /* RenderHeads.Media.AVProVideo.IMediaInfo RenderHeads.Media.AVProVideo.MediaPlayer::get_Info() */, L_38);
		NullCheck(L_39);
		float L_40;
		L_40 = InterfaceFuncInvoker0< float >::Invoke(0 /* System.Single RenderHeads.Media.AVProVideo.IMediaInfo::GetDurationMs() */, IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4_il2cpp_TypeInfo_var, L_39);
		V_3 = ((float)((float)L_37/(float)L_40));
	}

IL_0114:
	{
		// Vector2 anchorMin = Vector2.zero;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_41;
		L_41 = Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828(/*hidden argument*/NULL);
		V_4 = L_41;
		// Vector2 anchorMax = Vector2.one;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_42;
		L_42 = Vector2_get_one_m9B2AFD26404B6DD0F520D19FC7F79371C5C18B42(/*hidden argument*/NULL);
		V_5 = L_42;
		// if (_bufferedSliderImage != null &&
		//     _bufferedSliderImage.type == Image.Type.Filled)
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_43 = __this->get__bufferedSliderImage_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_44;
		L_44 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_43, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_014c;
		}
	}
	{
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_45 = __this->get__bufferedSliderImage_12();
		NullCheck(L_45);
		int32_t L_46;
		L_46 = Image_get_type_m730305AA6DAA0AF5C57A8AD2C1B8A97E6B0B8229_inline(L_45, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_46) == ((uint32_t)3))))
		{
			goto IL_014c;
		}
	}
	{
		// _bufferedSliderImage.fillAmount = d;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_47 = __this->get__bufferedSliderImage_12();
		float L_48 = V_1;
		NullCheck(L_47);
		Image_set_fillAmount_m1D28CFC9B15A45AB6C561AA42BD8F305605E9E3C(L_47, L_48, /*hidden argument*/NULL);
		// }
		goto IL_015e;
	}

IL_014c:
	{
		// anchorMin[0] = t1;
		float L_49 = V_2;
		Vector2_set_Item_m817FDD0709F52F09ECBB949C29DEE88E73889CAD((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_4), 0, L_49, /*hidden argument*/NULL);
		// anchorMax[0] = t2;
		float L_50 = V_3;
		Vector2_set_Item_m817FDD0709F52F09ECBB949C29DEE88E73889CAD((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_5), 0, L_50, /*hidden argument*/NULL);
	}

IL_015e:
	{
		// _bufferedSliderRect.anchorMin = anchorMin;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_51 = __this->get__bufferedSliderRect_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_52 = V_4;
		NullCheck(L_51);
		RectTransform_set_anchorMin_mD9E6E95890B701A5190C12F5AE42E622246AF798(L_51, L_52, /*hidden argument*/NULL);
		// _bufferedSliderRect.anchorMax = anchorMax;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_53 = __this->get__bufferedSliderRect_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_54 = V_5;
		NullCheck(L_53);
		RectTransform_set_anchorMax_m67E04F54B5122804E32019D5FAE50C21CC67651D(L_53, L_54, /*hidden argument*/NULL);
	}

IL_0178:
	{
		// }
		return;
	}
}
// System.Void VideoVCR::OnVideoEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoVCR_OnVideoEvent_m5A55B048083EAAD1F09A39BA3416B3F44100CD7F (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ___mp0, int32_t ___et1, int32_t ___errorCode2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventType_tB2FE909C749F1B861A530F105A083945CB226BBF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD5B87595462B1299CC2AA421A01B45CED796A03);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___et1;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)1)))
		{
			case 0:
			{
				goto IL_0020;
			}
			case 1:
			{
				goto IL_0020;
			}
			case 2:
			{
				goto IL_001a;
			}
			case 3:
			{
				goto IL_0020;
			}
		}
	}
	{
		goto IL_0020;
	}

IL_001a:
	{
		// OnNewMediaReady();
		VideoVCR_OnNewMediaReady_mEDC2336A6CEA55B77B6A3A7631F66019A527CF21(__this, /*hidden argument*/NULL);
	}

IL_0020:
	{
		// Debug.Log("Event: " + et.ToString());
		RuntimeObject * L_1 = Box(EventType_tB2FE909C749F1B861A530F105A083945CB226BBF_il2cpp_TypeInfo_var, (&___et1));
		NullCheck(L_1);
		String_t* L_2;
		L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		___et1 = *(int32_t*)UnBox(L_1);
		String_t* L_3;
		L_3 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteralDD5B87595462B1299CC2AA421A01B45CED796A03, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void VideoVCR::OnNewMediaReady()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoVCR_OnNewMediaReady_mEDC2336A6CEA55B77B6A3A7631F66019A527CF21 (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RuntimeObject* G_B2_0 = NULL;
	RuntimeObject* G_B1_0 = NULL;
	{
		// IMediaInfo info = PlayingPlayer.Info;
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_0;
		L_0 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		RuntimeObject* L_1;
		L_1 = VirtFuncInvoker0< RuntimeObject* >::Invoke(4 /* RenderHeads.Media.AVProVideo.IMediaInfo RenderHeads.Media.AVProVideo.MediaPlayer::get_Info() */, L_0);
		// if (_texture != null)
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_2 = __this->get__texture_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		G_B1_0 = L_1;
		if (!L_3)
		{
			G_B2_0 = L_1;
			goto IL_002b;
		}
	}
	{
		// Texture2D.Destroy(_texture);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_4 = __this->get__texture_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_4, /*hidden argument*/NULL);
		// _texture = null;
		__this->set__texture_14((Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *)NULL);
		G_B2_0 = G_B1_0;
	}

IL_002b:
	{
		// int textureWidth = info.GetVideoWidth();
		RuntimeObject* L_5 = G_B2_0;
		NullCheck(L_5);
		int32_t L_6;
		L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 RenderHeads.Media.AVProVideo.IMediaInfo::GetVideoWidth() */, IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4_il2cpp_TypeInfo_var, L_5);
		V_0 = L_6;
		// int textureHeight = info.GetVideoHeight();
		NullCheck(L_5);
		int32_t L_7;
		L_7 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 RenderHeads.Media.AVProVideo.IMediaInfo::GetVideoHeight() */, IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4_il2cpp_TypeInfo_var, L_5);
		V_1 = L_7;
		// _texture = new Texture2D(textureWidth, textureHeight, TextureFormat.ARGB32, false);
		int32_t L_8 = V_0;
		int32_t L_9 = V_1;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_10 = (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *)il2cpp_codegen_object_new(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		Texture2D__ctor_mF138386223A07CBD4CE94672757E39D0EF718092(L_10, L_8, L_9, 5, (bool)0, /*hidden argument*/NULL);
		__this->set__texture_14(L_10);
		// _timeStepSeconds = (PlayingPlayer.Info.GetDurationMs() / 1000f) / (float)NumFrames;
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_11;
		L_11 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		RuntimeObject* L_12;
		L_12 = VirtFuncInvoker0< RuntimeObject* >::Invoke(4 /* RenderHeads.Media.AVProVideo.IMediaInfo RenderHeads.Media.AVProVideo.MediaPlayer::get_Info() */, L_11);
		NullCheck(L_12);
		float L_13;
		L_13 = InterfaceFuncInvoker0< float >::Invoke(0 /* System.Single RenderHeads.Media.AVProVideo.IMediaInfo::GetDurationMs() */, IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4_il2cpp_TypeInfo_var, L_12);
		__this->set__timeStepSeconds_15(((float)((float)((float)((float)L_13/(float)(1000.0f)))/(float)(8.0f))));
		// }
		return;
	}
}
// System.Void VideoVCR::OnVideoSeekSlider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoVCR_OnVideoSeekSlider_m6BA20F479F74EC5A5A6ABBF271ED25B4F5C947AB (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (PlayingPlayer && _videoSeekSlider && _videoSeekSlider.value != _setVideoSeekSliderValue)
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_0;
		L_0 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0059;
		}
	}
	{
		Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * L_2 = __this->get__videoSeekSlider_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0059;
		}
	}
	{
		Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * L_4 = __this->get__videoSeekSlider_6();
		NullCheck(L_4);
		float L_5;
		L_5 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_4);
		float L_6 = __this->get__setVideoSeekSliderValue_7();
		if ((((float)L_5) == ((float)L_6)))
		{
			goto IL_0059;
		}
	}
	{
		// PlayingPlayer.Control.Seek(_videoSeekSlider.value * PlayingPlayer.Info.GetDurationMs());
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_7;
		L_7 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		RuntimeObject* L_8;
		L_8 = VirtFuncInvoker0< RuntimeObject* >::Invoke(5 /* RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.MediaPlayer::get_Control() */, L_7);
		Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * L_9 = __this->get__videoSeekSlider_6();
		NullCheck(L_9);
		float L_10;
		L_10 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_9);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_11;
		L_11 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		RuntimeObject* L_12;
		L_12 = VirtFuncInvoker0< RuntimeObject* >::Invoke(4 /* RenderHeads.Media.AVProVideo.IMediaInfo RenderHeads.Media.AVProVideo.MediaPlayer::get_Info() */, L_11);
		NullCheck(L_12);
		float L_13;
		L_13 = InterfaceFuncInvoker0< float >::Invoke(0 /* System.Single RenderHeads.Media.AVProVideo.IMediaInfo::GetDurationMs() */, IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4_il2cpp_TypeInfo_var, L_12);
		NullCheck(L_8);
		InterfaceActionInvoker1< float >::Invoke(19 /* System.Void RenderHeads.Media.AVProVideo.IMediaControl::Seek(System.Single) */, IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var, L_8, ((float)il2cpp_codegen_multiply((float)L_10, (float)L_13)));
	}

IL_0059:
	{
		// }
		return;
	}
}
// System.Void VideoVCR::OnVideoSliderDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoVCR_OnVideoSliderDown_mCBE099D7D6C532F1EA05175253E3FB4050B67548 (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (PlayingPlayer)
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_0;
		L_0 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003c;
		}
	}
	{
		// _wasPlayingOnScrub = PlayingPlayer.Control.IsPlaying();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_2;
		L_2 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		RuntimeObject* L_3;
		L_3 = VirtFuncInvoker0< RuntimeObject* >::Invoke(5 /* RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.MediaPlayer::get_Control() */, L_2);
		NullCheck(L_3);
		bool L_4;
		L_4 = InterfaceFuncInvoker0< bool >::Invoke(10 /* System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsPlaying() */, IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var, L_3);
		__this->set__wasPlayingOnScrub_8(L_4);
		// if (_wasPlayingOnScrub)
		bool L_5 = __this->get__wasPlayingOnScrub_8();
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		// PlayingPlayer.Pause();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_6;
		L_6 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		MediaPlayer_Pause_m7194C52FC0A95953B2582B2615AE700418954A94(L_6, /*hidden argument*/NULL);
	}

IL_0036:
	{
		// OnVideoSeekSlider();
		VideoVCR_OnVideoSeekSlider_m6BA20F479F74EC5A5A6ABBF271ED25B4F5C947AB(__this, /*hidden argument*/NULL);
	}

IL_003c:
	{
		// }
		return;
	}
}
// System.Void VideoVCR::OnVideoSliderUp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoVCR_OnVideoSliderUp_m57DAAB9784ADC718852E2572DABAC44546C1EAE1 (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (PlayingPlayer && _wasPlayingOnScrub)
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_0;
		L_0 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		bool L_2 = __this->get__wasPlayingOnScrub_8();
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		// PlayingPlayer.Control.Play();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_3;
		L_3 = VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		RuntimeObject* L_4;
		L_4 = VirtFuncInvoker0< RuntimeObject* >::Invoke(5 /* RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.MediaPlayer::get_Control() */, L_3);
		NullCheck(L_4);
		InterfaceActionInvoker0::Invoke(15 /* System.Void RenderHeads.Media.AVProVideo.IMediaControl::Play() */, IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var, L_4);
		// _wasPlayingOnScrub = false;
		__this->set__wasPlayingOnScrub_8((bool)0);
	}

IL_002c:
	{
		// }
		return;
	}
}
// System.Void VideoVCR::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VideoVCR__ctor_m44F9A9C7A3BCA8F31ED617BF04AE57613DDF616A (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7BC6A4DAF813268120D1FDB6C6954972C951796C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public MediaPlayer.FileLocation _location = MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder;
		__this->set__location_9(2);
		// public string _folder = "Airtel5G/Video/";
		__this->set__folder_10(_stringLiteral7BC6A4DAF813268120D1FDB6C6954972C951796C);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WagonWheelManager::PlayAnimation(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WagonWheelManager_PlayAnimation_m0D365F74357CC854514D501A3BD3CBC4E2D95D5F (WagonWheelManager_t06317F447976C4D78C9E31F402AA7122B0F187C5 * __this, int32_t ___animIndex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5A1278AB54EFEBBD1E0E03AB2677F22D39311C27);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animator.SetInteger("Next",animIndex);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = __this->get_animator_4();
		int32_t L_1 = ___animIndex0;
		NullCheck(L_0);
		Animator_SetInteger_mFB04A03AF6C24978BF2BDE9161243F8F6B9762C5(L_0, _stringLiteral5A1278AB54EFEBBD1E0E03AB2677F22D39311C27, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void WagonWheelManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WagonWheelManager__ctor_mF1AA3FD185EFA8F56E870987C826728546C4B21E (WagonWheelManager_t06317F447976C4D78C9E31F402AA7122B0F187C5 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.IntPtr RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::GetRenderEventFunc()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t Native_GetRenderEventFunc_m8754CC53B2673E22F28728FA5751AE43B8D59288 (const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_AVProLocal_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("AVProLocal"), "GetRenderEventFunc", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(GetRenderEventFunc)();
	#else
	intptr_t returnValue = il2cppPInvokeFunc();
	#endif

	return returnValue;
}
// System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::_GetWidth(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Native__GetWidth_m9E38517D03739EA72116C26F9827A14695F8985B (int32_t ___iPlayerIndex0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_AVProLocal_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("AVProLocal"), "_GetWidth", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_GetWidth)(___iPlayerIndex0);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___iPlayerIndex0);
	#endif

	return returnValue;
}
// System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::_GetHeight(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Native__GetHeight_m673F6642C668BEAD1B496AE131F9DDEB3B3D917A (int32_t ___iPlayerIndex0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_AVProLocal_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("AVProLocal"), "_GetHeight", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_GetHeight)(___iPlayerIndex0);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___iPlayerIndex0);
	#endif

	return returnValue;
}
// System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::_GetTextureHandle(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Native__GetTextureHandle_m2ED72AAF09B6E50426219C52804B1797DF195407 (int32_t ___iPlayerIndex0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_AVProLocal_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("AVProLocal"), "_GetTextureHandle", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_GetTextureHandle)(___iPlayerIndex0);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___iPlayerIndex0);
	#endif

	return returnValue;
}
// System.Int64 RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::_GetDuration(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t Native__GetDuration_m13527EA823F50088CFD4894B1397BF81AB4D46FC (int32_t ___iPlayerIndex0, const RuntimeMethod* method)
{
	typedef int64_t (DEFAULT_CALL *PInvokeFunc) (int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_AVProLocal_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("AVProLocal"), "_GetDuration", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
	int64_t returnValue = reinterpret_cast<PInvokeFunc>(_GetDuration)(___iPlayerIndex0);
	#else
	int64_t returnValue = il2cppPInvokeFunc(___iPlayerIndex0);
	#endif

	return returnValue;
}
// System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::_GetLastErrorCode(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Native__GetLastErrorCode_mBB2A86F4C659D10A66A21B1380E862D77B55F3A9 (int32_t ___iPlayerIndex0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_AVProLocal_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("AVProLocal"), "_GetLastErrorCode", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_GetLastErrorCode)(___iPlayerIndex0);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___iPlayerIndex0);
	#endif

	return returnValue;
}
// System.Int32 RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::_GetFrameCount(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Native__GetFrameCount_m6BDCABAF44BE8EA126AFB183F61E614486B981B7 (int32_t ___iPlayerIndex0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_AVProLocal_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("AVProLocal"), "_GetFrameCount", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_GetFrameCount)(___iPlayerIndex0);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___iPlayerIndex0);
	#endif

	return returnValue;
}
// System.Single RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::_GetVideoDisplayRate(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Native__GetVideoDisplayRate_mD91DA764D68277343C42FB3E237382A6B423451E (int32_t ___iPlayerIndex0, const RuntimeMethod* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) (int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_AVProLocal_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("AVProLocal"), "_GetVideoDisplayRate", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
	float returnValue = reinterpret_cast<PInvokeFunc>(_GetVideoDisplayRate)(___iPlayerIndex0);
	#else
	float returnValue = il2cppPInvokeFunc(___iPlayerIndex0);
	#endif

	return returnValue;
}
// System.Boolean RenderHeads.Media.AVProVideo.AndroidMediaPlayer/Native::_CanPlay(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Native__CanPlay_mD6AAADE54253E1AA636C1D12466BC9A8118E5500 (int32_t ___iPlayerIndex0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_AVProLocal_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("AVProLocal"), "_CanPlay", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_AVProLocal_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_CanPlay)(___iPlayerIndex0);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___iPlayerIndex0);
	#endif

	return static_cast<bool>(returnValue);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CExtractFrameCoroutineU3Ed__209__ctor_m42A11F4ACB5EABD7F3B2FF42F17377F8988B55F0 (U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CExtractFrameCoroutineU3Ed__209_System_IDisposable_Dispose_m2813638F893988A28291B082C41B4C2CCFA5E316 (U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CExtractFrameCoroutineU3Ed__209_MoveNext_mCF2B7B2C334A8068ADCA0AA5DD16D8DA1E0A6F75 (U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMediaProducer_tC1A2D084AE2332166A227C6B2216B214DAB4D978_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * V_1 = NULL;
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * V_2 = NULL;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_1 = __this->get_U3CU3E4__this_3();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_011b;
			}
			case 2:
			{
				goto IL_01c6;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		__this->set_U3CU3E1__state_0((-1));
		// Texture2D result = target;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_3 = __this->get_target_2();
		__this->set_U3CresultU3E5__2_8(L_3);
		// Texture frame = null;
		V_2 = (Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE *)NULL;
		// if (m_Control != null)
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_4 = V_1;
		NullCheck(L_4);
		RuntimeObject* L_5 = L_4->get_m_Control_41();
		if (!L_5)
		{
			goto IL_016f;
		}
	}
	{
		// if (timeSeconds >= 0f)
		float L_6 = __this->get_timeSeconds_4();
		if ((!(((float)L_6) >= ((float)(0.0f)))))
		{
			goto IL_0162;
		}
	}
	{
		// Pause();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_7 = V_1;
		NullCheck(L_7);
		MediaPlayer_Pause_m7194C52FC0A95953B2582B2615AE700418954A94(L_7, /*hidden argument*/NULL);
		// float seekTimeMs = timeSeconds * 1000f;
		float L_8 = __this->get_timeSeconds_4();
		V_3 = ((float)il2cpp_codegen_multiply((float)L_8, (float)(1000.0f)));
		// if (TextureProducer.GetTexture() != null && (Mathf.Abs(m_Control.GetCurrentTimeMs() - seekTimeMs) < timeThresholdMs))
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_9 = V_1;
		NullCheck(L_9);
		RuntimeObject* L_10;
		L_10 = VirtFuncInvoker0< RuntimeObject* >::Invoke(7 /* RenderHeads.Media.AVProVideo.IMediaProducer RenderHeads.Media.AVProVideo.MediaPlayer::get_TextureProducer() */, L_9);
		NullCheck(L_10);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_11;
		L_11 = InterfaceFuncInvoker1< Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE *, int32_t >::Invoke(1 /* UnityEngine.Texture RenderHeads.Media.AVProVideo.IMediaProducer::GetTexture(System.Int32) */, IMediaProducer_tC1A2D084AE2332166A227C6B2216B214DAB4D978_il2cpp_TypeInfo_var, L_10, 0);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_12;
		L_12 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_11, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00a6;
		}
	}
	{
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_13 = V_1;
		NullCheck(L_13);
		RuntimeObject* L_14 = L_13->get_m_Control_41();
		NullCheck(L_14);
		float L_15;
		L_15 = InterfaceFuncInvoker0< float >::Invoke(22 /* System.Single RenderHeads.Media.AVProVideo.IMediaControl::GetCurrentTimeMs() */, IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var, L_14);
		float L_16 = V_3;
		float L_17;
		L_17 = fabsf(((float)il2cpp_codegen_subtract((float)L_15, (float)L_16)));
		int32_t L_18 = __this->get_timeThresholdMs_5();
		if ((!(((float)L_17) < ((float)((float)((float)L_18))))))
		{
			goto IL_00a6;
		}
	}
	{
		// frame = TextureProducer.GetTexture();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_19 = V_1;
		NullCheck(L_19);
		RuntimeObject* L_20;
		L_20 = VirtFuncInvoker0< RuntimeObject* >::Invoke(7 /* RenderHeads.Media.AVProVideo.IMediaProducer RenderHeads.Media.AVProVideo.MediaPlayer::get_TextureProducer() */, L_19);
		NullCheck(L_20);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_21;
		L_21 = InterfaceFuncInvoker1< Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE *, int32_t >::Invoke(1 /* UnityEngine.Texture RenderHeads.Media.AVProVideo.IMediaProducer::GetTexture(System.Int32) */, IMediaProducer_tC1A2D084AE2332166A227C6B2216B214DAB4D978_il2cpp_TypeInfo_var, L_20, 0);
		V_2 = L_21;
		// }
		goto IL_016f;
	}

IL_00a6:
	{
		// int preSeekFrameCount = m_Texture.GetTextureFrameCount();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_22 = V_1;
		NullCheck(L_22);
		RuntimeObject* L_23 = L_22->get_m_Texture_42();
		NullCheck(L_23);
		int32_t L_24;
		L_24 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 RenderHeads.Media.AVProVideo.IMediaProducer::GetTextureFrameCount() */, IMediaProducer_tC1A2D084AE2332166A227C6B2216B214DAB4D978_il2cpp_TypeInfo_var, L_23);
		V_4 = L_24;
		// if (accurateSeek)
		bool L_25 = __this->get_accurateSeek_6();
		if (!L_25)
		{
			goto IL_00c9;
		}
	}
	{
		// m_Control.Seek(seekTimeMs);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_26 = V_1;
		NullCheck(L_26);
		RuntimeObject* L_27 = L_26->get_m_Control_41();
		float L_28 = V_3;
		NullCheck(L_27);
		InterfaceActionInvoker1< float >::Invoke(19 /* System.Void RenderHeads.Media.AVProVideo.IMediaControl::Seek(System.Single) */, IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var, L_27, L_28);
		// }
		goto IL_00d5;
	}

IL_00c9:
	{
		// m_Control.SeekFast(seekTimeMs);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_29 = V_1;
		NullCheck(L_29);
		RuntimeObject* L_30 = L_29->get_m_Control_41();
		float L_31 = V_3;
		NullCheck(L_30);
		InterfaceActionInvoker1< float >::Invoke(20 /* System.Void RenderHeads.Media.AVProVideo.IMediaControl::SeekFast(System.Single) */, IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var, L_30, L_31);
	}

IL_00d5:
	{
		// if (!m_Control.WaitForNextFrame(GetDummyCamera(), preSeekFrameCount))
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_32 = V_1;
		NullCheck(L_32);
		RuntimeObject* L_33 = L_32->get_m_Control_41();
		IL2CPP_RUNTIME_CLASS_INIT(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64_il2cpp_TypeInfo_var);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_34;
		L_34 = MediaPlayer_GetDummyCamera_m1ECE958660114FCD2F55A4A438E63A6F704A9DD1(/*hidden argument*/NULL);
		int32_t L_35 = V_4;
		NullCheck(L_33);
		bool L_36;
		L_36 = InterfaceFuncInvoker2< bool, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C *, int32_t >::Invoke(52 /* System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::WaitForNextFrame(UnityEngine.Camera,System.Int32) */, IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var, L_33, L_34, L_35);
		if (L_36)
		{
			goto IL_0153;
		}
	}
	{
		// int currFc = TextureProducer.GetTextureFrameCount();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_37 = V_1;
		NullCheck(L_37);
		RuntimeObject* L_38;
		L_38 = VirtFuncInvoker0< RuntimeObject* >::Invoke(7 /* RenderHeads.Media.AVProVideo.IMediaProducer RenderHeads.Media.AVProVideo.MediaPlayer::get_TextureProducer() */, L_37);
		NullCheck(L_38);
		int32_t L_39;
		L_39 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 RenderHeads.Media.AVProVideo.IMediaProducer::GetTextureFrameCount() */, IMediaProducer_tC1A2D084AE2332166A227C6B2216B214DAB4D978_il2cpp_TypeInfo_var, L_38);
		__this->set_U3CcurrFcU3E5__3_9(L_39);
		// int iterations = 0;
		__this->set_U3CiterationsU3E5__4_10(0);
		// int maxIterations = 50;
		__this->set_U3CmaxIterationsU3E5__5_11(((int32_t)50));
		goto IL_0122;
	}

IL_010b:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_011b:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0122:
	{
		// while((currFc + 1) >= TextureProducer.GetTextureFrameCount() && iterations++ < maxIterations)
		int32_t L_40 = __this->get_U3CcurrFcU3E5__3_9();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_41 = V_1;
		NullCheck(L_41);
		RuntimeObject* L_42;
		L_42 = VirtFuncInvoker0< RuntimeObject* >::Invoke(7 /* RenderHeads.Media.AVProVideo.IMediaProducer RenderHeads.Media.AVProVideo.MediaPlayer::get_TextureProducer() */, L_41);
		NullCheck(L_42);
		int32_t L_43;
		L_43 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 RenderHeads.Media.AVProVideo.IMediaProducer::GetTextureFrameCount() */, IMediaProducer_tC1A2D084AE2332166A227C6B2216B214DAB4D978_il2cpp_TypeInfo_var, L_42);
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_40, (int32_t)1))) < ((int32_t)L_43)))
		{
			goto IL_0153;
		}
	}
	{
		int32_t L_44 = __this->get_U3CiterationsU3E5__4_10();
		V_5 = L_44;
		int32_t L_45 = V_5;
		__this->set_U3CiterationsU3E5__4_10(((int32_t)il2cpp_codegen_add((int32_t)L_45, (int32_t)1)));
		int32_t L_46 = V_5;
		int32_t L_47 = __this->get_U3CmaxIterationsU3E5__5_11();
		if ((((int32_t)L_46) < ((int32_t)L_47)))
		{
			goto IL_010b;
		}
	}

IL_0153:
	{
		// frame = TextureProducer.GetTexture();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_48 = V_1;
		NullCheck(L_48);
		RuntimeObject* L_49;
		L_49 = VirtFuncInvoker0< RuntimeObject* >::Invoke(7 /* RenderHeads.Media.AVProVideo.IMediaProducer RenderHeads.Media.AVProVideo.MediaPlayer::get_TextureProducer() */, L_48);
		NullCheck(L_49);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_50;
		L_50 = InterfaceFuncInvoker1< Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE *, int32_t >::Invoke(1 /* UnityEngine.Texture RenderHeads.Media.AVProVideo.IMediaProducer::GetTexture(System.Int32) */, IMediaProducer_tC1A2D084AE2332166A227C6B2216B214DAB4D978_il2cpp_TypeInfo_var, L_49, 0);
		V_2 = L_50;
		// }
		goto IL_016f;
	}

IL_0162:
	{
		// frame = TextureProducer.GetTexture();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_51 = V_1;
		NullCheck(L_51);
		RuntimeObject* L_52;
		L_52 = VirtFuncInvoker0< RuntimeObject* >::Invoke(7 /* RenderHeads.Media.AVProVideo.IMediaProducer RenderHeads.Media.AVProVideo.MediaPlayer::get_TextureProducer() */, L_51);
		NullCheck(L_52);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_53;
		L_53 = InterfaceFuncInvoker1< Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE *, int32_t >::Invoke(1 /* UnityEngine.Texture RenderHeads.Media.AVProVideo.IMediaProducer::GetTexture(System.Int32) */, IMediaProducer_tC1A2D084AE2332166A227C6B2216B214DAB4D978_il2cpp_TypeInfo_var, L_52, 0);
		V_2 = L_53;
	}

IL_016f:
	{
		// if (frame != null)
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_54 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_55;
		L_55 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_54, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_01a5;
		}
	}
	{
		// result = Helper.GetReadableTexture(frame, TextureProducer.RequiresVerticalFlip(), Helper.GetOrientation(Info.GetTextureTransform()), target);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_56 = V_2;
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_57 = V_1;
		NullCheck(L_57);
		RuntimeObject* L_58;
		L_58 = VirtFuncInvoker0< RuntimeObject* >::Invoke(7 /* RenderHeads.Media.AVProVideo.IMediaProducer RenderHeads.Media.AVProVideo.MediaPlayer::get_TextureProducer() */, L_57);
		NullCheck(L_58);
		bool L_59;
		L_59 = InterfaceFuncInvoker0< bool >::Invoke(5 /* System.Boolean RenderHeads.Media.AVProVideo.IMediaProducer::RequiresVerticalFlip() */, IMediaProducer_tC1A2D084AE2332166A227C6B2216B214DAB4D978_il2cpp_TypeInfo_var, L_58);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_60 = V_1;
		NullCheck(L_60);
		RuntimeObject* L_61;
		L_61 = VirtFuncInvoker0< RuntimeObject* >::Invoke(4 /* RenderHeads.Media.AVProVideo.IMediaInfo RenderHeads.Media.AVProVideo.MediaPlayer::get_Info() */, L_60);
		NullCheck(L_61);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_62;
		L_62 = InterfaceFuncInvoker0< SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* >::Invoke(19 /* System.Single[] RenderHeads.Media.AVProVideo.IMediaInfo::GetTextureTransform() */, IMediaInfo_t821A4681C9260C59D9101AE4DA5F3045FF512CD4_il2cpp_TypeInfo_var, L_61);
		int32_t L_63;
		L_63 = Helper_GetOrientation_mE372CD72592447C80D008B0FBF653ECC87FB7627(L_62, /*hidden argument*/NULL);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_64 = __this->get_target_2();
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_65;
		L_65 = Helper_GetReadableTexture_mF4D3B41AFFF220B865DD21C89F30F920DA919777(L_56, L_59, L_63, L_64, /*hidden argument*/NULL);
		__this->set_U3CresultU3E5__2_8(L_65);
	}

IL_01a5:
	{
		// callback(result);
		ProcessExtractedFrame_t9BDA228D84FCD6905FA985C6759CD5E40590AA45 * L_66 = __this->get_callback_7();
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_67 = __this->get_U3CresultU3E5__2_8();
		NullCheck(L_66);
		ProcessExtractedFrame_Invoke_m93D4170ADCD901788FA035AD906788CD07C8ED5F(L_66, L_67, /*hidden argument*/NULL);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_01c6:
	{
		__this->set_U3CU3E1__state_0((-1));
		// }
		return (bool)0;
	}
}
// System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CExtractFrameCoroutineU3Ed__209_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EB04B181B3AF0D8F31D45DB7FE4073A601B9FD7 (U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CExtractFrameCoroutineU3Ed__209_System_Collections_IEnumerator_Reset_m03E84C88DE36A133F5C616284E160B269D57B779 (U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CExtractFrameCoroutineU3Ed__209_System_Collections_IEnumerator_Reset_m03E84C88DE36A133F5C616284E160B269D57B779_RuntimeMethod_var)));
	}
}
// System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CExtractFrameCoroutineU3Ed__209_System_Collections_IEnumerator_get_Current_mC4C324C31F1A54748026CBCAA87DC42C1925C5A7 (U3CExtractFrameCoroutineU3Ed__209_tCCA33E2F32AD5C3385F0FF09BC6E3FDA1ADE3010 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CFinalRenderCaptureU3Ed__188__ctor_m498CF3113FF234A782856D3BE00DED342F375BEC (U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CFinalRenderCaptureU3Ed__188_System_IDisposable_Dispose_m8A0688DA71BC5C7ED01C0415FD9CDA8D2B52D5F4 (U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CFinalRenderCaptureU3Ed__188_MoveNext_m3D0C170BAF177C94DFF4DBA6DEA23C3B8B58F119 (U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMediaPlayer_t2E66F33315EA803836A22C719212E062847381B9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0040;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// YieldInstruction wait = new WaitForEndOfFrame();
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_4 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_4, /*hidden argument*/NULL);
		__this->set_U3CwaitU3E5__2_3(L_4);
		goto IL_0062;
	}

IL_002b:
	{
		// yield return wait;
		YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF * L_5 = __this->get_U3CwaitU3E5__2_3();
		__this->set_U3CU3E2__current_1(L_5);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0040:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (this.enabled)
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_6 = V_1;
		NullCheck(L_6);
		bool L_7;
		L_7 = Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0062;
		}
	}
	{
		// if (m_Player != null)
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_8 = V_1;
		NullCheck(L_8);
		RuntimeObject* L_9 = L_8->get_m_Player_44();
		if (!L_9)
		{
			goto IL_0062;
		}
	}
	{
		// m_Player.Render();
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_10 = V_1;
		NullCheck(L_10);
		RuntimeObject* L_11 = L_10->get_m_Player_44();
		NullCheck(L_11);
		InterfaceActionInvoker0::Invoke(2 /* System.Void RenderHeads.Media.AVProVideo.IMediaPlayer::Render() */, IMediaPlayer_t2E66F33315EA803836A22C719212E062847381B9_il2cpp_TypeInfo_var, L_11);
	}

IL_0062:
	{
		// while (Application.isPlaying)
		bool L_12;
		L_12 = Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567(/*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_002b;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CFinalRenderCaptureU3Ed__188_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF833D26918BB2E538C49FF8C63CE97F8CBCC3EE (U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CFinalRenderCaptureU3Ed__188_System_Collections_IEnumerator_Reset_m22429013FDB26856E4D1FD9EF1749DD600BF547C (U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CFinalRenderCaptureU3Ed__188_System_Collections_IEnumerator_Reset_m22429013FDB26856E4D1FD9EF1749DD600BF547C_RuntimeMethod_var)));
	}
}
// System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CFinalRenderCaptureU3Ed__188_System_Collections_IEnumerator_get_Current_m021BA9DE6276EB6F40BC8FFA6746895F17044C30 (U3CFinalRenderCaptureU3Ed__188_t099D41DC30AA6518A56EABA74AB3E14FF96EABE2 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadSubtitlesCoroutineU3Ed__166__ctor_mAF47DBCEE88F6F816BE1F9BA4F33F3BAA344957E (U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadSubtitlesCoroutineU3Ed__166_System_IDisposable_Dispose_m7A3DE1124596B52D5185CEE3AEE6322EA3FBF978 (U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CLoadSubtitlesCoroutineU3Ed__166_MoveNext_m4517ABFF4B0805678FDC2DC2181EFBF10A2C5E7E (U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMediaSubtitles_t8E1536FBC8120C1C678E06AA4A1B9CE49B434747_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral77F51302B6EA5AF286BAF279CFF1D7437660DCCD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral98866EAF371FBAAD526DEA44B3C0575E306213C7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5746C0BD05E9E09663FA764FFF2206D493AFA08);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * V_1 = NULL;
	String_t* V_2 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_1 = __this->get_U3CU3E4__this_3();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0049;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// UnityEngine.Networking.UnityWebRequest www = UnityEngine.Networking.UnityWebRequest.Get(url);
		String_t* L_4 = __this->get_url_2();
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_5;
		L_5 = UnityWebRequest_Get_m9C24DB3E8BED0B0886F28DCD982A4741A9903B1A(L_4, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E5__2_6(L_5);
		// yield return www.SendWebRequest();
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_6 = __this->get_U3CwwwU3E5__2_6();
		NullCheck(L_6);
		UnityWebRequestAsyncOperation_tDCAC6B6C7D51563F8DFD4963E3BE362470125396 * L_7;
		L_7 = UnityWebRequest_SendWebRequest_m990921023F56ECB8FF8C118894A317EB6E2F5B50(L_6, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_7);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0049:
	{
		__this->set_U3CU3E1__state_0((-1));
		// string subtitleData = string.Empty;
		String_t* L_8 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		V_2 = L_8;
		// if (!www.isNetworkError)
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_9 = __this->get_U3CwwwU3E5__2_6();
		NullCheck(L_9);
		bool L_10;
		L_10 = UnityWebRequest_get_isNetworkError_m0126D38DA9B39159CB12F8DE0C3962A4BEEE5C03(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0076;
		}
	}
	{
		// subtitleData = ((UnityEngine.Networking.DownloadHandler)www.downloadHandler).text;
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_11 = __this->get_U3CwwwU3E5__2_6();
		NullCheck(L_11);
		DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * L_12;
		L_12 = UnityWebRequest_get_downloadHandler_mCE0A0C53A63419FE5AE25915AFB36EABE294C732(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13;
		L_13 = DownloadHandler_get_text_mD89D7125640800A8F5C4B9401C080C405953828A(L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		// }
		goto IL_009b;
	}

IL_0076:
	{
		// Debug.LogError("[AVProVideo] Error loading subtitles '" + www.error + "' from " + url);
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_14 = __this->get_U3CwwwU3E5__2_6();
		NullCheck(L_14);
		String_t* L_15;
		L_15 = UnityWebRequest_get_error_m32B69D2365C1FE2310B5936C7C295B71A92CC2B4(L_14, /*hidden argument*/NULL);
		String_t* L_16 = __this->get_url_2();
		String_t* L_17;
		L_17 = String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78(_stringLiteral98866EAF371FBAAD526DEA44B3C0575E306213C7, L_15, _stringLiteralD5746C0BD05E9E09663FA764FFF2206D493AFA08, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(L_17, /*hidden argument*/NULL);
	}

IL_009b:
	{
		// if (m_Subtitles.LoadSubtitlesSRT(subtitleData))
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_18 = V_1;
		NullCheck(L_18);
		RuntimeObject* L_19 = L_18->get_m_Subtitles_45();
		String_t* L_20 = V_2;
		NullCheck(L_19);
		bool L_21;
		L_21 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(0 /* System.Boolean RenderHeads.Media.AVProVideo.IMediaSubtitles::LoadSubtitlesSRT(System.String) */, IMediaSubtitles_t8E1536FBC8120C1C678E06AA4A1B9CE49B434747_il2cpp_TypeInfo_var, L_19, L_20);
		if (!L_21)
		{
			goto IL_00ca;
		}
	}
	{
		// m_SubtitleLocation = fileLocation;
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_22 = V_1;
		int32_t L_23 = __this->get_fileLocation_4();
		NullCheck(L_22);
		L_22->set_m_SubtitleLocation_26(L_23);
		// m_SubtitlePath = filePath;
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_24 = V_1;
		String_t* L_25 = __this->get_filePath_5();
		NullCheck(L_24);
		L_24->set_m_SubtitlePath_28(L_25);
		// m_LoadSubtitles = false;
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_26 = V_1;
		NullCheck(L_26);
		L_26->set_m_LoadSubtitles_25((bool)0);
		// }
		goto IL_00e0;
	}

IL_00ca:
	{
		// Debug.LogError("[AVProVideo] Failed to load subtitles" + url, this);
		String_t* L_27 = __this->get_url_2();
		String_t* L_28;
		L_28 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral77F51302B6EA5AF286BAF279CFF1D7437660DCCD, L_27, /*hidden argument*/NULL);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_29 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_mEFF048E5541EE45362C0AAD829E3FA4C2CAB9199(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00e0:
	{
		// m_loadSubtitlesRoutine = null;
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_30 = V_1;
		NullCheck(L_30);
		L_30->set_m_loadSubtitlesRoutine_30((Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 *)NULL);
		// www.Dispose();
		UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * L_31 = __this->get_U3CwwwU3E5__2_6();
		NullCheck(L_31);
		UnityWebRequest_Dispose_m8032472F6BC2EC4FEE017DE7E4C440078BC4E1C8(L_31, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC6D7BB5CC4CDCB04948293FC163F9E337D46A775 (U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_IEnumerator_Reset_m625A44CCFB701C03434E399A2A8AFD83D81C3628 (U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_IEnumerator_Reset_m625A44CCFB701C03434E399A2A8AFD83D81C3628_RuntimeMethod_var)));
	}
}
// System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_IEnumerator_get_Current_m5653252B6D3E7BDF005249225ABEEB9B867D1C55 (U3CLoadSubtitlesCoroutineU3Ed__166_t5BE4E107FF94278FF8AD53A809258DC609C87BCB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::GetHTTPHeadersAsJSON()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* OptionsAndroid_GetHTTPHeadersAsJSON_m9E64F1365E2533A0453989BFDAFE29BB83FC727D (OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringBuilder_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0C3C6829C3CCF8020C6AC45B87963ADC095CD44A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4D8D9C94AC5DA5FCED2EC8A64E10E714A2515C30);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCCCB24758B8281580D9CE13BCDDFE5C7584D4DCD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD8673C209A09B6EA196C0F9C6D5946365880B09D);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	int32_t V_1 = 0;
	{
		// if (httpHeaders.Count > 0)
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_0 = __this->get_httpHeaders_9();
		NullCheck(L_0);
		int32_t L_1;
		L_1 = List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_inline(L_0, /*hidden argument*/List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_RuntimeMethod_var);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00be;
		}
	}
	{
		// System.Text.StringBuilder builder = new System.Text.StringBuilder();
		StringBuilder_t * L_2 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m5A81DE19E748F748E19FF13FB6FFD2547F9212D9(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		// int i = 0;
		V_1 = 0;
		// builder.Append("{");
		StringBuilder_t * L_3 = V_0;
		NullCheck(L_3);
		StringBuilder_t * L_4;
		L_4 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_3, _stringLiteral0C3C6829C3CCF8020C6AC45B87963ADC095CD44A, /*hidden argument*/NULL);
		// builder.AppendFormat("\"{0}\":\"{1}\"", StringAsJsonString(httpHeaders[i].header), StringAsJsonString(httpHeaders[i].value));
		StringBuilder_t * L_5 = V_0;
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_6 = __this->get_httpHeaders_9();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  L_8;
		L_8 = List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_inline(L_6, L_7, /*hidden argument*/List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_RuntimeMethod_var);
		String_t* L_9 = L_8.get_header_0();
		String_t* L_10;
		L_10 = PlatformOptions_StringAsJsonString_m7A9A91C0D27FFA0DAFB694F2BFAB563817DDDD1A(L_9, /*hidden argument*/NULL);
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_11 = __this->get_httpHeaders_9();
		int32_t L_12 = V_1;
		NullCheck(L_11);
		HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  L_13;
		L_13 = List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_inline(L_11, L_12, /*hidden argument*/List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_RuntimeMethod_var);
		String_t* L_14 = L_13.get_value_1();
		String_t* L_15;
		L_15 = PlatformOptions_StringAsJsonString_m7A9A91C0D27FFA0DAFB694F2BFAB563817DDDD1A(L_14, /*hidden argument*/NULL);
		NullCheck(L_5);
		StringBuilder_t * L_16;
		L_16 = StringBuilder_AppendFormat_m37B348187DD9186C2451ACCA3DBC4ABCD4632AD4(L_5, _stringLiteralCCCB24758B8281580D9CE13BCDDFE5C7584D4DCD, L_10, L_15, /*hidden argument*/NULL);
		// for (i = 1; i < httpHeaders.Count; ++i)
		V_1 = 1;
		goto IL_009d;
	}

IL_0061:
	{
		// builder.AppendFormat(",\"{0}\":\"{1}\"", StringAsJsonString(httpHeaders[i].header), StringAsJsonString(httpHeaders[i].value));
		StringBuilder_t * L_17 = V_0;
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_18 = __this->get_httpHeaders_9();
		int32_t L_19 = V_1;
		NullCheck(L_18);
		HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  L_20;
		L_20 = List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_inline(L_18, L_19, /*hidden argument*/List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_RuntimeMethod_var);
		String_t* L_21 = L_20.get_header_0();
		String_t* L_22;
		L_22 = PlatformOptions_StringAsJsonString_m7A9A91C0D27FFA0DAFB694F2BFAB563817DDDD1A(L_21, /*hidden argument*/NULL);
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_23 = __this->get_httpHeaders_9();
		int32_t L_24 = V_1;
		NullCheck(L_23);
		HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  L_25;
		L_25 = List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_inline(L_23, L_24, /*hidden argument*/List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_RuntimeMethod_var);
		String_t* L_26 = L_25.get_value_1();
		String_t* L_27;
		L_27 = PlatformOptions_StringAsJsonString_m7A9A91C0D27FFA0DAFB694F2BFAB563817DDDD1A(L_26, /*hidden argument*/NULL);
		NullCheck(L_17);
		StringBuilder_t * L_28;
		L_28 = StringBuilder_AppendFormat_m37B348187DD9186C2451ACCA3DBC4ABCD4632AD4(L_17, _stringLiteralD8673C209A09B6EA196C0F9C6D5946365880B09D, L_22, L_27, /*hidden argument*/NULL);
		// for (i = 1; i < httpHeaders.Count; ++i)
		int32_t L_29 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
	}

IL_009d:
	{
		// for (i = 1; i < httpHeaders.Count; ++i)
		int32_t L_30 = V_1;
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_31 = __this->get_httpHeaders_9();
		NullCheck(L_31);
		int32_t L_32;
		L_32 = List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_inline(L_31, /*hidden argument*/List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_RuntimeMethod_var);
		if ((((int32_t)L_30) < ((int32_t)L_32)))
		{
			goto IL_0061;
		}
	}
	{
		// builder.Append("}");
		StringBuilder_t * L_33 = V_0;
		NullCheck(L_33);
		StringBuilder_t * L_34;
		L_34 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_33, _stringLiteral4D8D9C94AC5DA5FCED2EC8A64E10E714A2515C30, /*hidden argument*/NULL);
		// return builder.ToString();
		StringBuilder_t * L_35 = V_0;
		NullCheck(L_35);
		String_t* L_36;
		L_36 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		return L_36;
	}

IL_00be:
	{
		// return httpHeaderJson;
		String_t* L_37 = __this->get_httpHeaderJson_10();
		return L_37;
	}
}
// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::IsModified()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OptionsAndroid_IsModified_m0F1E3B1CB75E5A9869D889293636CA2939D0C0B3 (OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return base.IsModified()
		//     || (fileOffset != 0)
		//     || useFastOesPath
		//     || showPosterFrame
		//     || (videoApi != Android.VideoApi.ExoPlayer)
		//     || (httpHeaders != null && httpHeaders.Count > 0)
		//     || enableAudio360
		//     || (audio360ChannelMode != Audio360ChannelMode.TBE_8_2)
		//     || preferSoftwareDecoder;
		bool L_0;
		L_0 = PlatformOptions_IsModified_m9329B32428B8862D0994558AF2B7C086A9DA8651_inline(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_1 = __this->get_fileOffset_11();
		if (L_1)
		{
			goto IL_0056;
		}
	}
	{
		bool L_2 = __this->get_useFastOesPath_4();
		if (L_2)
		{
			goto IL_0056;
		}
	}
	{
		bool L_3 = __this->get_showPosterFrame_5();
		if (L_3)
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_4 = __this->get_videoApi_3();
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_0056;
		}
	}
	{
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_5 = __this->get_httpHeaders_9();
		if (!L_5)
		{
			goto IL_003f;
		}
	}
	{
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_6 = __this->get_httpHeaders_9();
		NullCheck(L_6);
		int32_t L_7;
		L_7 = List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_inline(L_6, /*hidden argument*/List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_RuntimeMethod_var);
		if ((((int32_t)L_7) > ((int32_t)0)))
		{
			goto IL_0056;
		}
	}

IL_003f:
	{
		bool L_8 = __this->get_enableAudio360_6();
		if (L_8)
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_9 = __this->get_audio360ChannelMode_7();
		if (L_9)
		{
			goto IL_0056;
		}
	}
	{
		bool L_10 = __this->get_preferSoftwareDecoder_8();
		return L_10;
	}

IL_0056:
	{
		return (bool)1;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::OnBeforeSerialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionsAndroid_OnBeforeSerialize_m35E7F079E61905E64DE47F08350B60ABED4B6113 (OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (httpHeaders != null && httpHeaders.Count > 0 && httpHeaderJson.Length > 0)
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_0 = __this->get_httpHeaders_9();
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_1 = __this->get_httpHeaders_9();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_inline(L_1, /*hidden argument*/List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_RuntimeMethod_var);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_3 = __this->get_httpHeaderJson_10();
		NullCheck(L_3);
		int32_t L_4;
		L_4 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		// httpHeaderJson = null;
		__this->set_httpHeaderJson_10((String_t*)NULL);
	}

IL_002b:
	{
		// }
		return;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::OnAfterDeserialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionsAndroid_OnAfterDeserialize_m7AA8F7C03489DC53548184AD191C47F864BAF649 (OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393 * __this, const RuntimeMethod* method)
{
	{
		// if (httpHeaderJson == null || httpHeaderJson.Length == 0)
		String_t* L_0 = __this->get_httpHeaderJson_10();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		String_t* L_1 = __this->get_httpHeaderJson_10();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0016;
		}
	}

IL_0015:
	{
		// return;
		return;
	}

IL_0016:
	{
		// httpHeaders = ParseJsonHTTPHeadersIntoHTTPHeaderList(httpHeaderJson);
		String_t* L_3 = __this->get_httpHeaderJson_10();
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_4;
		L_4 = PlatformOptions_ParseJsonHTTPHeadersIntoHTTPHeaderList_m0C5A914EF74EA3DB7ABCEB1FF75F5B782E38E59D(L_3, /*hidden argument*/NULL);
		__this->set_httpHeaders_9(L_4);
		// if (httpHeaders != null)
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_5 = __this->get_httpHeaders_9();
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		// httpHeaderJson = null;
		__this->set_httpHeaderJson_10((String_t*)NULL);
	}

IL_0036:
	{
		// }
		return;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionsAndroid__ctor_m186BC313EEA6B49A9CF88DD62AF031FC2E992545 (OptionsAndroid_t2E0C0838EFBE50483D4DEAA3CEE43D90CC1C5393 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mA4972EA6AD7EDA75836F78B588F59FC023AB4DDA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public Android.VideoApi videoApi = Android.VideoApi.ExoPlayer;
		__this->set_videoApi_3(2);
		// public List<HTTPHeader> httpHeaders = new List<HTTPHeader>();
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_0 = (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 *)il2cpp_codegen_object_new(List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452_il2cpp_TypeInfo_var);
		List_1__ctor_mA4972EA6AD7EDA75836F78B588F59FC023AB4DDA(L_0, /*hidden argument*/List_1__ctor_mA4972EA6AD7EDA75836F78B588F59FC023AB4DDA_RuntimeMethod_var);
		__this->set_httpHeaders_9(L_0);
		PlatformOptions__ctor_m0765FB48C8311B03E858B0E7ACB26870CA7E1FD5(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::GetHTTPHeadersAsJSON()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* OptionsApple_GetHTTPHeadersAsJSON_m6ED8E32FCAB2E1C13FF1EFD6FBA2F51B97E191EE (OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringBuilder_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0C3C6829C3CCF8020C6AC45B87963ADC095CD44A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4D8D9C94AC5DA5FCED2EC8A64E10E714A2515C30);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCCCB24758B8281580D9CE13BCDDFE5C7584D4DCD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD8673C209A09B6EA196C0F9C6D5946365880B09D);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	int32_t V_1 = 0;
	{
		// if (httpHeaders.Count > 0)
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_0 = __this->get_httpHeaders_4();
		NullCheck(L_0);
		int32_t L_1;
		L_1 = List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_inline(L_0, /*hidden argument*/List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_RuntimeMethod_var);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00be;
		}
	}
	{
		// System.Text.StringBuilder builder = new System.Text.StringBuilder();
		StringBuilder_t * L_2 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m5A81DE19E748F748E19FF13FB6FFD2547F9212D9(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		// int i = 0;
		V_1 = 0;
		// builder.Append("{");
		StringBuilder_t * L_3 = V_0;
		NullCheck(L_3);
		StringBuilder_t * L_4;
		L_4 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_3, _stringLiteral0C3C6829C3CCF8020C6AC45B87963ADC095CD44A, /*hidden argument*/NULL);
		// builder.AppendFormat("\"{0}\":\"{1}\"", StringAsJsonString(httpHeaders[i].header), StringAsJsonString(httpHeaders[i].value));
		StringBuilder_t * L_5 = V_0;
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_6 = __this->get_httpHeaders_4();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  L_8;
		L_8 = List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_inline(L_6, L_7, /*hidden argument*/List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_RuntimeMethod_var);
		String_t* L_9 = L_8.get_header_0();
		String_t* L_10;
		L_10 = PlatformOptions_StringAsJsonString_m7A9A91C0D27FFA0DAFB694F2BFAB563817DDDD1A(L_9, /*hidden argument*/NULL);
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_11 = __this->get_httpHeaders_4();
		int32_t L_12 = V_1;
		NullCheck(L_11);
		HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  L_13;
		L_13 = List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_inline(L_11, L_12, /*hidden argument*/List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_RuntimeMethod_var);
		String_t* L_14 = L_13.get_value_1();
		String_t* L_15;
		L_15 = PlatformOptions_StringAsJsonString_m7A9A91C0D27FFA0DAFB694F2BFAB563817DDDD1A(L_14, /*hidden argument*/NULL);
		NullCheck(L_5);
		StringBuilder_t * L_16;
		L_16 = StringBuilder_AppendFormat_m37B348187DD9186C2451ACCA3DBC4ABCD4632AD4(L_5, _stringLiteralCCCB24758B8281580D9CE13BCDDFE5C7584D4DCD, L_10, L_15, /*hidden argument*/NULL);
		// for (i = 1; i < httpHeaders.Count; ++i)
		V_1 = 1;
		goto IL_009d;
	}

IL_0061:
	{
		// builder.AppendFormat(",\"{0}\":\"{1}\"", StringAsJsonString(httpHeaders[i].header), StringAsJsonString(httpHeaders[i].value));
		StringBuilder_t * L_17 = V_0;
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_18 = __this->get_httpHeaders_4();
		int32_t L_19 = V_1;
		NullCheck(L_18);
		HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  L_20;
		L_20 = List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_inline(L_18, L_19, /*hidden argument*/List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_RuntimeMethod_var);
		String_t* L_21 = L_20.get_header_0();
		String_t* L_22;
		L_22 = PlatformOptions_StringAsJsonString_m7A9A91C0D27FFA0DAFB694F2BFAB563817DDDD1A(L_21, /*hidden argument*/NULL);
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_23 = __this->get_httpHeaders_4();
		int32_t L_24 = V_1;
		NullCheck(L_23);
		HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  L_25;
		L_25 = List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_inline(L_23, L_24, /*hidden argument*/List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_RuntimeMethod_var);
		String_t* L_26 = L_25.get_value_1();
		String_t* L_27;
		L_27 = PlatformOptions_StringAsJsonString_m7A9A91C0D27FFA0DAFB694F2BFAB563817DDDD1A(L_26, /*hidden argument*/NULL);
		NullCheck(L_17);
		StringBuilder_t * L_28;
		L_28 = StringBuilder_AppendFormat_m37B348187DD9186C2451ACCA3DBC4ABCD4632AD4(L_17, _stringLiteralD8673C209A09B6EA196C0F9C6D5946365880B09D, L_22, L_27, /*hidden argument*/NULL);
		// for (i = 1; i < httpHeaders.Count; ++i)
		int32_t L_29 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
	}

IL_009d:
	{
		// for (i = 1; i < httpHeaders.Count; ++i)
		int32_t L_30 = V_1;
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_31 = __this->get_httpHeaders_4();
		NullCheck(L_31);
		int32_t L_32;
		L_32 = List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_inline(L_31, /*hidden argument*/List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_RuntimeMethod_var);
		if ((((int32_t)L_30) < ((int32_t)L_32)))
		{
			goto IL_0061;
		}
	}
	{
		// builder.Append("}");
		StringBuilder_t * L_33 = V_0;
		NullCheck(L_33);
		StringBuilder_t * L_34;
		L_34 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_33, _stringLiteral4D8D9C94AC5DA5FCED2EC8A64E10E714A2515C30, /*hidden argument*/NULL);
		// return builder.ToString();
		StringBuilder_t * L_35 = V_0;
		NullCheck(L_35);
		String_t* L_36;
		L_36 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		return L_36;
	}

IL_00be:
	{
		// return httpHeaderJson;
		String_t* L_37 = __this->get_httpHeaderJson_5();
		return L_37;
	}
}
// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::IsModified()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OptionsApple_IsModified_m97B8AED361C97F06BDA0D49A52D0019DD5BC53D5 (OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return (base.IsModified())
		// || (audioMode != AudioMode.SystemDirect)
		// || (httpHeaders != null && httpHeaders.Count > 0)
		// || (string.IsNullOrEmpty(httpHeaderJson) == false)
		// || (string.IsNullOrEmpty(keyServerURLOverride) == false)
		// || (string.IsNullOrEmpty(keyServerAuthToken) == false)
		// || (string.IsNullOrEmpty(base64EncodedKeyBlob) == false);
		bool L_0;
		L_0 = PlatformOptions_IsModified_m9329B32428B8862D0994558AF2B7C086A9DA8651_inline(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_1 = __this->get_audioMode_3();
		if (L_1)
		{
			goto IL_005c;
		}
	}
	{
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_2 = __this->get_httpHeaders_4();
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_3 = __this->get_httpHeaders_4();
		NullCheck(L_3);
		int32_t L_4;
		L_4 = List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_inline(L_3, /*hidden argument*/List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_RuntimeMethod_var);
		if ((((int32_t)L_4) > ((int32_t)0)))
		{
			goto IL_005c;
		}
	}

IL_0026:
	{
		String_t* L_5 = __this->get_httpHeaderJson_5();
		bool L_6;
		L_6 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_005c;
		}
	}
	{
		String_t* L_7 = __this->get_keyServerURLOverride_6();
		bool L_8;
		L_8 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005c;
		}
	}
	{
		String_t* L_9 = __this->get_keyServerAuthToken_7();
		bool L_10;
		L_10 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_005c;
		}
	}
	{
		String_t* L_11 = __this->get_base64EncodedKeyBlob_8();
		bool L_12;
		L_12 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_11, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_12) == ((int32_t)0))? 1 : 0);
	}

IL_005c:
	{
		return (bool)1;
	}
}
// System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::GetKeyServerURL()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* OptionsApple_GetKeyServerURL_m0F7BC1C13BDD299CE027EAD0F0205E4EDBE82D46 (OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E * __this, const RuntimeMethod* method)
{
	{
		// public override string GetKeyServerURL() { return keyServerURLOverride; }
		String_t* L_0 = __this->get_keyServerURLOverride_6();
		return L_0;
	}
}
// System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::GetKeyServerAuthToken()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* OptionsApple_GetKeyServerAuthToken_m75A801C049884609FBDDE0A31EE4C645D2469384 (OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E * __this, const RuntimeMethod* method)
{
	{
		// public override string GetKeyServerAuthToken() { return keyServerAuthToken; }
		String_t* L_0 = __this->get_keyServerAuthToken_7();
		return L_0;
	}
}
// System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::GetDecryptionKey()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* OptionsApple_GetDecryptionKey_m235356057A28338A12FCFCBAE5168FDC352C4510 (OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E * __this, const RuntimeMethod* method)
{
	{
		// public override string GetDecryptionKey() { return base64EncodedKeyBlob; }
		String_t* L_0 = __this->get_base64EncodedKeyBlob_8();
		return L_0;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::OnBeforeSerialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionsApple_OnBeforeSerialize_mEC86ADC5FE27FE8262188BBDA14549197D892528 (OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (httpHeaders != null && httpHeaders.Count > 0 && httpHeaderJson.Length > 0)
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_0 = __this->get_httpHeaders_4();
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_1 = __this->get_httpHeaders_4();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_inline(L_1, /*hidden argument*/List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_RuntimeMethod_var);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_3 = __this->get_httpHeaderJson_5();
		NullCheck(L_3);
		int32_t L_4;
		L_4 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		// httpHeaderJson = null;
		__this->set_httpHeaderJson_5((String_t*)NULL);
	}

IL_002b:
	{
		// }
		return;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::OnAfterDeserialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionsApple_OnAfterDeserialize_mE8385FA36B16F9CD6712D898349E1D31E9FF22AF (OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E * __this, const RuntimeMethod* method)
{
	{
		// if (httpHeaderJson == null || httpHeaderJson.Length == 0)
		String_t* L_0 = __this->get_httpHeaderJson_5();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		String_t* L_1 = __this->get_httpHeaderJson_5();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0016;
		}
	}

IL_0015:
	{
		// return;
		return;
	}

IL_0016:
	{
		// httpHeaders = ParseJsonHTTPHeadersIntoHTTPHeaderList(httpHeaderJson);
		String_t* L_3 = __this->get_httpHeaderJson_5();
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_4;
		L_4 = PlatformOptions_ParseJsonHTTPHeadersIntoHTTPHeaderList_m0C5A914EF74EA3DB7ABCEB1FF75F5B782E38E59D(L_3, /*hidden argument*/NULL);
		__this->set_httpHeaders_4(L_4);
		// if (httpHeaders != null)
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_5 = __this->get_httpHeaders_4();
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		// httpHeaderJson = null;
		__this->set_httpHeaderJson_5((String_t*)NULL);
	}

IL_0036:
	{
		// }
		return;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionsApple__ctor_m2E26273BAAC19F3078E37CB3B52FC00BBB5EA0CC (OptionsApple_tDE91FDE1B4DA8E98DF9D55A012F21AA882B6185E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mA4972EA6AD7EDA75836F78B588F59FC023AB4DDA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<HTTPHeader> httpHeaders = new List<HTTPHeader>();
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_0 = (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 *)il2cpp_codegen_object_new(List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452_il2cpp_TypeInfo_var);
		List_1__ctor_mA4972EA6AD7EDA75836F78B588F59FC023AB4DDA(L_0, /*hidden argument*/List_1__ctor_mA4972EA6AD7EDA75836F78B588F59FC023AB4DDA_RuntimeMethod_var);
		__this->set_httpHeaders_4(L_0);
		PlatformOptions__ctor_m0765FB48C8311B03E858B0E7ACB26870CA7E1FD5(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsIOS::IsModified()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OptionsIOS_IsModified_m2BCBA6E16A9330881C279C03219FE45E5B851A8D (OptionsIOS_t868D1748A3E07299A108C1E60F97753CA38954A5 * __this, const RuntimeMethod* method)
{
	{
		// return (base.IsModified())
		// || (useYpCbCr420Textures == false)
		// || (resumePlaybackOnAudioSessionRouteChange == true);
		bool L_0;
		L_0 = OptionsApple_IsModified_m97B8AED361C97F06BDA0D49A52D0019DD5BC53D5(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		bool L_1 = __this->get_useYpCbCr420Textures_9();
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		bool L_2 = __this->get_resumePlaybackOnAudioSessionRouteChange_10();
		return L_2;
	}

IL_0017:
	{
		return (bool)1;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsIOS::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionsIOS__ctor_m71CBF1B11498CE1D3234585F5E17D693B145E506 (OptionsIOS_t868D1748A3E07299A108C1E60F97753CA38954A5 * __this, const RuntimeMethod* method)
{
	{
		// public bool useYpCbCr420Textures = true;
		__this->set_useYpCbCr420Textures_9((bool)1);
		OptionsApple__ctor_m2E26273BAAC19F3078E37CB3B52FC00BBB5EA0CC(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsMacOSX::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionsMacOSX__ctor_m86B15B9DB09B66C526637AF36DF9DB4B0A6AFB9B (OptionsMacOSX_t1B3B40C8ABFE68B24D80EA87E10EFA9C1972A2B3 * __this, const RuntimeMethod* method)
{
	{
		OptionsApple__ctor_m2E26273BAAC19F3078E37CB3B52FC00BBB5EA0CC(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsPS4::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionsPS4__ctor_mF620860B6E7CA8A6C5699D098C5CC345D0ADC151 (OptionsPS4_t43756CE0642DB94AA9A8E30A58524CDC7BD082D1 * __this, const RuntimeMethod* method)
{
	{
		PlatformOptions__ctor_m0765FB48C8311B03E858B0E7ACB26870CA7E1FD5(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsTVOS::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionsTVOS__ctor_m94BC448264C97F1BF954AA3B81474CD21B946D7A (OptionsTVOS_tFDA4033A636E29A9005B4D9092A32298FFE8911B * __this, const RuntimeMethod* method)
{
	{
		OptionsIOS__ctor_m71CBF1B11498CE1D3234585F5E17D693B145E506(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWebGL::IsModified()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OptionsWebGL_IsModified_m582458478E2767579052067E272E029FFF4435AD (OptionsWebGL_tA7A6C5EB6C224751A8B5DF0B99DE0C1A3DD15B05 * __this, const RuntimeMethod* method)
{
	{
		// return (base.IsModified() || externalLibrary != WebGL.ExternalLibrary.None || useTextureMips);
		bool L_0;
		L_0 = PlatformOptions_IsModified_m9329B32428B8862D0994558AF2B7C086A9DA8651_inline(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = __this->get_externalLibrary_3();
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		bool L_2 = __this->get_useTextureMips_4();
		return L_2;
	}

IL_0017:
	{
		return (bool)1;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWebGL::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionsWebGL__ctor_mED1A980016C73E0A2B17B9B3C0C6C6CD21ADD66F (OptionsWebGL_tA7A6C5EB6C224751A8B5DF0B99DE0C1A3DD15B05 * __this, const RuntimeMethod* method)
{
	{
		PlatformOptions__ctor_m0765FB48C8311B03E858B0E7ACB26870CA7E1FD5(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::IsModified()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OptionsWindows_IsModified_m3D5B03949B095E226B87FDF5C679D1B12AC97950 (OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m199DB87BCE947106FBA38E19FDFE80CB65B61144_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return (base.IsModified() || !useHardwareDecoding || useTextureMips || hintAlphaChannel || useLowLatency || useUnityAudio || videoApi != Windows.VideoApi.MediaFoundation || !forceAudioResample || enableAudio360 || audio360ChannelMode != Audio360ChannelMode.TBE_8_2 || !string.IsNullOrEmpty(forceAudioOutputDeviceName) || preferredFilters.Count != 0);
		bool L_0;
		L_0 = PlatformOptions_IsModified_m9329B32428B8862D0994558AF2B7C086A9DA8651_inline(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_006c;
		}
	}
	{
		bool L_1 = __this->get_useHardwareDecoding_4();
		if (!L_1)
		{
			goto IL_006c;
		}
	}
	{
		bool L_2 = __this->get_useTextureMips_7();
		if (L_2)
		{
			goto IL_006c;
		}
	}
	{
		bool L_3 = __this->get_hintAlphaChannel_8();
		if (L_3)
		{
			goto IL_006c;
		}
	}
	{
		bool L_4 = __this->get_useLowLatency_9();
		if (L_4)
		{
			goto IL_006c;
		}
	}
	{
		bool L_5 = __this->get_useUnityAudio_5();
		if (L_5)
		{
			goto IL_006c;
		}
	}
	{
		int32_t L_6 = __this->get_videoApi_3();
		if (L_6)
		{
			goto IL_006c;
		}
	}
	{
		bool L_7 = __this->get_forceAudioResample_6();
		if (!L_7)
		{
			goto IL_006c;
		}
	}
	{
		bool L_8 = __this->get_enableAudio360_12();
		if (L_8)
		{
			goto IL_006c;
		}
	}
	{
		int32_t L_9 = __this->get_audio360ChannelMode_13();
		if (L_9)
		{
			goto IL_006c;
		}
	}
	{
		String_t* L_10 = __this->get_forceAudioOutputDeviceName_10();
		bool L_11;
		L_11 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_006c;
		}
	}
	{
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_12 = __this->get_preferredFilters_11();
		NullCheck(L_12);
		int32_t L_13;
		L_13 = List_1_get_Count_m199DB87BCE947106FBA38E19FDFE80CB65B61144_inline(L_12, /*hidden argument*/List_1_get_Count_m199DB87BCE947106FBA38E19FDFE80CB65B61144_RuntimeMethod_var);
		return (bool)((!(((uint32_t)L_13) <= ((uint32_t)0)))? 1 : 0);
	}

IL_006c:
	{
		return (bool)1;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionsWindows__ctor_m74A15D2B8EA4695A1B55EC0A9EE37B7447672251 (OptionsWindows_t1B6A30C98CCF2B0BBCB9309F39208A4CCD35C7A1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public bool useHardwareDecoding = true;
		__this->set_useHardwareDecoding_4((bool)1);
		// public bool forceAudioResample = true;
		__this->set_forceAudioResample_6((bool)1);
		// public string forceAudioOutputDeviceName = string.Empty;
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_forceAudioOutputDeviceName_10(L_0);
		// public List<string> preferredFilters = new List<string>();
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_1 = (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *)il2cpp_codegen_object_new(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_il2cpp_TypeInfo_var);
		List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9(L_1, /*hidden argument*/List_1__ctor_m30C52A4F2828D86CA3FAB0B1B583948F4DA9F1F9_RuntimeMethod_var);
		__this->set_preferredFilters_11(L_1);
		PlatformOptions__ctor_m0765FB48C8311B03E858B0E7ACB26870CA7E1FD5(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsPhone::IsModified()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OptionsWindowsPhone_IsModified_m9F12B86CC99A844734D7EB5CABB84ABB36AD5762 (OptionsWindowsPhone_t5B5890235D85585DFCBEC0D280F332C274B6E63C * __this, const RuntimeMethod* method)
{
	{
		// return (base.IsModified() || !useHardwareDecoding || useTextureMips || useLowLatency || useUnityAudio || !forceAudioResample);
		bool L_0;
		L_0 = PlatformOptions_IsModified_m9329B32428B8862D0994558AF2B7C086A9DA8651_inline(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0032;
		}
	}
	{
		bool L_1 = __this->get_useHardwareDecoding_3();
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		bool L_2 = __this->get_useTextureMips_6();
		if (L_2)
		{
			goto IL_0032;
		}
	}
	{
		bool L_3 = __this->get_useLowLatency_7();
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		bool L_4 = __this->get_useUnityAudio_4();
		if (L_4)
		{
			goto IL_0032;
		}
	}
	{
		bool L_5 = __this->get_forceAudioResample_5();
		return (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
	}

IL_0032:
	{
		return (bool)1;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsPhone::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionsWindowsPhone__ctor_mAAD92EF239A0FFDA5EF025204F7DEEBE47A9227C (OptionsWindowsPhone_t5B5890235D85585DFCBEC0D280F332C274B6E63C * __this, const RuntimeMethod* method)
{
	{
		// public bool useHardwareDecoding = true;
		__this->set_useHardwareDecoding_3((bool)1);
		// public bool forceAudioResample = true;
		__this->set_forceAudioResample_5((bool)1);
		PlatformOptions__ctor_m0765FB48C8311B03E858B0E7ACB26870CA7E1FD5(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsUWP::IsModified()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OptionsWindowsUWP_IsModified_m692328A5F9DE126E140EEA459B48E17BEFF762E7 (OptionsWindowsUWP_tBA5BE5D51A12AA6360DF87691342C5EE7B8F7F66 * __this, const RuntimeMethod* method)
{
	{
		// return (base.IsModified() || !useHardwareDecoding || useTextureMips || useLowLatency || useUnityAudio || !forceAudioResample);
		bool L_0;
		L_0 = PlatformOptions_IsModified_m9329B32428B8862D0994558AF2B7C086A9DA8651_inline(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0032;
		}
	}
	{
		bool L_1 = __this->get_useHardwareDecoding_3();
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		bool L_2 = __this->get_useTextureMips_6();
		if (L_2)
		{
			goto IL_0032;
		}
	}
	{
		bool L_3 = __this->get_useLowLatency_7();
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		bool L_4 = __this->get_useUnityAudio_4();
		if (L_4)
		{
			goto IL_0032;
		}
	}
	{
		bool L_5 = __this->get_forceAudioResample_5();
		return (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
	}

IL_0032:
	{
		return (bool)1;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsUWP::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionsWindowsUWP__ctor_m8D80FBF23ADE92ACC0A48F4A065B3C5980EEAEA0 (OptionsWindowsUWP_tBA5BE5D51A12AA6360DF87691342C5EE7B8F7F66 * __this, const RuntimeMethod* method)
{
	{
		// public bool useHardwareDecoding = true;
		__this->set_useHardwareDecoding_3((bool)1);
		// public bool forceAudioResample = true;
		__this->set_forceAudioResample_5((bool)1);
		PlatformOptions__ctor_m0765FB48C8311B03E858B0E7ACB26870CA7E1FD5(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::IsModified()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlatformOptions_IsModified_m9329B32428B8862D0994558AF2B7C086A9DA8651 (PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F * __this, const RuntimeMethod* method)
{
	{
		// return overridePath;     // The other variables don't matter if overridePath is false
		bool L_0 = __this->get_overridePath_0();
		return L_0;
	}
}
// System.String RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::GetKeyServerURL()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlatformOptions_GetKeyServerURL_mE036407B5A12BE2646A522D99A703389C89C44AE (PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F * __this, const RuntimeMethod* method)
{
	{
		// public virtual string GetKeyServerURL() { return null; }
		return (String_t*)NULL;
	}
}
// System.String RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::GetKeyServerAuthToken()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlatformOptions_GetKeyServerAuthToken_m5889D3593EEEC1FCC11C2631A9596F009DAFD887 (PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F * __this, const RuntimeMethod* method)
{
	{
		// public virtual string GetKeyServerAuthToken() { return null; }
		return (String_t*)NULL;
	}
}
// System.String RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::GetDecryptionKey()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlatformOptions_GetDecryptionKey_mB1D0BF403F838B666D818FA8D5164948838A880D (PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F * __this, const RuntimeMethod* method)
{
	{
		// public virtual string GetDecryptionKey() { return null; }
		return (String_t*)NULL;
	}
}
// System.String RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::StringAsJsonString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PlatformOptions_StringAsJsonString_m7A9A91C0D27FFA0DAFB694F2BFAB563817DDDD1A (String_t* ___str0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringBuilder_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5962E944D7340CE47999BF097B4AFD70C1501FB9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral785F17F45C331C415D0A7458E6AAC36966399C51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F3238CD8C342B06FB9AB185C610175C84625462);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral848E5ED630B3142F565DD995C6E8D30187ED33CD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7C3FCA8C63E127B542B38A5CA5E3FEEDDD1B122);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB78F235D4291950A7D101307609C259F3E1F033F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE7A46075109789D1792549A284B05CF42BE37425);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF18840F490E42D3CE48CDCBF47229C1C240F8ABE);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	{
		// System.Text.StringBuilder builder = null;
		V_0 = (StringBuilder_t *)NULL;
		// for (int i = 0; i < str.Length; ++i)
		V_1 = 0;
		goto IL_0168;
	}

IL_0009:
	{
		// switch (str[i])
		String_t* L_0 = ___str0;
		int32_t L_1 = V_1;
		NullCheck(L_0);
		Il2CppChar L_2;
		L_2 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_0, L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		Il2CppChar L_3 = V_2;
		if ((!(((uint32_t)L_3) <= ((uint32_t)((int32_t)34)))))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppChar L_4 = V_2;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)8)))
		{
			case 0:
			{
				goto IL_00b5;
			}
			case 1:
			{
				goto IL_0134;
			}
			case 2:
			{
				goto IL_00f6;
			}
			case 3:
			{
				goto IL_0153;
			}
			case 4:
			{
				goto IL_00d7;
			}
			case 5:
			{
				goto IL_0115;
			}
		}
	}
	{
		Il2CppChar L_5 = V_2;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)34))))
		{
			goto IL_004f;
		}
	}
	{
		goto IL_0153;
	}

IL_0040:
	{
		Il2CppChar L_6 = V_2;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)47))))
		{
			goto IL_0093;
		}
	}
	{
		Il2CppChar L_7 = V_2;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)92))))
		{
			goto IL_0071;
		}
	}
	{
		goto IL_0153;
	}

IL_004f:
	{
		// if (builder == null)
		StringBuilder_t * L_8 = V_0;
		if (L_8)
		{
			goto IL_0060;
		}
	}
	{
		// builder = new System.Text.StringBuilder(str.Substring(0, i));
		String_t* L_9 = ___str0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		String_t* L_11;
		L_11 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_9, 0, L_10, /*hidden argument*/NULL);
		StringBuilder_t * L_12 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m9305A36F9CF53EDD80D132428999934C68904C77(L_12, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
	}

IL_0060:
	{
		// builder.Append("\\\"");
		StringBuilder_t * L_13 = V_0;
		NullCheck(L_13);
		StringBuilder_t * L_14;
		L_14 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_13, _stringLiteral848E5ED630B3142F565DD995C6E8D30187ED33CD, /*hidden argument*/NULL);
		// break;
		goto IL_0164;
	}

IL_0071:
	{
		// if (builder == null)
		StringBuilder_t * L_15 = V_0;
		if (L_15)
		{
			goto IL_0082;
		}
	}
	{
		// builder = new System.Text.StringBuilder(str.Substring(0, i));
		String_t* L_16 = ___str0;
		int32_t L_17 = V_1;
		NullCheck(L_16);
		String_t* L_18;
		L_18 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_16, 0, L_17, /*hidden argument*/NULL);
		StringBuilder_t * L_19 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m9305A36F9CF53EDD80D132428999934C68904C77(L_19, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
	}

IL_0082:
	{
		// builder.Append("\\\\");
		StringBuilder_t * L_20 = V_0;
		NullCheck(L_20);
		StringBuilder_t * L_21;
		L_21 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_20, _stringLiteralF18840F490E42D3CE48CDCBF47229C1C240F8ABE, /*hidden argument*/NULL);
		// break;
		goto IL_0164;
	}

IL_0093:
	{
		// if (builder == null)
		StringBuilder_t * L_22 = V_0;
		if (L_22)
		{
			goto IL_00a4;
		}
	}
	{
		// builder = new System.Text.StringBuilder(str.Substring(0, i));
		String_t* L_23 = ___str0;
		int32_t L_24 = V_1;
		NullCheck(L_23);
		String_t* L_25;
		L_25 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_23, 0, L_24, /*hidden argument*/NULL);
		StringBuilder_t * L_26 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m9305A36F9CF53EDD80D132428999934C68904C77(L_26, L_25, /*hidden argument*/NULL);
		V_0 = L_26;
	}

IL_00a4:
	{
		// builder.Append("\\/");
		StringBuilder_t * L_27 = V_0;
		NullCheck(L_27);
		StringBuilder_t * L_28;
		L_28 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_27, _stringLiteralE7A46075109789D1792549A284B05CF42BE37425, /*hidden argument*/NULL);
		// break;
		goto IL_0164;
	}

IL_00b5:
	{
		// if (builder == null)
		StringBuilder_t * L_29 = V_0;
		if (L_29)
		{
			goto IL_00c6;
		}
	}
	{
		// builder = new System.Text.StringBuilder(str.Substring(0, i));
		String_t* L_30 = ___str0;
		int32_t L_31 = V_1;
		NullCheck(L_30);
		String_t* L_32;
		L_32 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_30, 0, L_31, /*hidden argument*/NULL);
		StringBuilder_t * L_33 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m9305A36F9CF53EDD80D132428999934C68904C77(L_33, L_32, /*hidden argument*/NULL);
		V_0 = L_33;
	}

IL_00c6:
	{
		// builder.Append("\\b");
		StringBuilder_t * L_34 = V_0;
		NullCheck(L_34);
		StringBuilder_t * L_35;
		L_35 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_34, _stringLiteral5962E944D7340CE47999BF097B4AFD70C1501FB9, /*hidden argument*/NULL);
		// break;
		goto IL_0164;
	}

IL_00d7:
	{
		// if (builder == null)
		StringBuilder_t * L_36 = V_0;
		if (L_36)
		{
			goto IL_00e8;
		}
	}
	{
		// builder = new System.Text.StringBuilder(str.Substring(0, i));
		String_t* L_37 = ___str0;
		int32_t L_38 = V_1;
		NullCheck(L_37);
		String_t* L_39;
		L_39 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_37, 0, L_38, /*hidden argument*/NULL);
		StringBuilder_t * L_40 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m9305A36F9CF53EDD80D132428999934C68904C77(L_40, L_39, /*hidden argument*/NULL);
		V_0 = L_40;
	}

IL_00e8:
	{
		// builder.Append("\\f");
		StringBuilder_t * L_41 = V_0;
		NullCheck(L_41);
		StringBuilder_t * L_42;
		L_42 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_41, _stringLiteralA7C3FCA8C63E127B542B38A5CA5E3FEEDDD1B122, /*hidden argument*/NULL);
		// break;
		goto IL_0164;
	}

IL_00f6:
	{
		// if (builder == null)
		StringBuilder_t * L_43 = V_0;
		if (L_43)
		{
			goto IL_0107;
		}
	}
	{
		// builder = new System.Text.StringBuilder(str.Substring(0, i));
		String_t* L_44 = ___str0;
		int32_t L_45 = V_1;
		NullCheck(L_44);
		String_t* L_46;
		L_46 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_44, 0, L_45, /*hidden argument*/NULL);
		StringBuilder_t * L_47 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m9305A36F9CF53EDD80D132428999934C68904C77(L_47, L_46, /*hidden argument*/NULL);
		V_0 = L_47;
	}

IL_0107:
	{
		// builder.Append("\\n");
		StringBuilder_t * L_48 = V_0;
		NullCheck(L_48);
		StringBuilder_t * L_49;
		L_49 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_48, _stringLiteral785F17F45C331C415D0A7458E6AAC36966399C51, /*hidden argument*/NULL);
		// break;
		goto IL_0164;
	}

IL_0115:
	{
		// if (builder == null)
		StringBuilder_t * L_50 = V_0;
		if (L_50)
		{
			goto IL_0126;
		}
	}
	{
		// builder = new System.Text.StringBuilder(str.Substring(0, i));
		String_t* L_51 = ___str0;
		int32_t L_52 = V_1;
		NullCheck(L_51);
		String_t* L_53;
		L_53 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_51, 0, L_52, /*hidden argument*/NULL);
		StringBuilder_t * L_54 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m9305A36F9CF53EDD80D132428999934C68904C77(L_54, L_53, /*hidden argument*/NULL);
		V_0 = L_54;
	}

IL_0126:
	{
		// builder.Append("\\r");
		StringBuilder_t * L_55 = V_0;
		NullCheck(L_55);
		StringBuilder_t * L_56;
		L_56 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_55, _stringLiteralB78F235D4291950A7D101307609C259F3E1F033F, /*hidden argument*/NULL);
		// break;
		goto IL_0164;
	}

IL_0134:
	{
		// if (builder == null)
		StringBuilder_t * L_57 = V_0;
		if (L_57)
		{
			goto IL_0145;
		}
	}
	{
		// builder = new System.Text.StringBuilder(str.Substring(0, i));
		String_t* L_58 = ___str0;
		int32_t L_59 = V_1;
		NullCheck(L_58);
		String_t* L_60;
		L_60 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_58, 0, L_59, /*hidden argument*/NULL);
		StringBuilder_t * L_61 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m9305A36F9CF53EDD80D132428999934C68904C77(L_61, L_60, /*hidden argument*/NULL);
		V_0 = L_61;
	}

IL_0145:
	{
		// builder.Append("\\t");
		StringBuilder_t * L_62 = V_0;
		NullCheck(L_62);
		StringBuilder_t * L_63;
		L_63 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_62, _stringLiteral7F3238CD8C342B06FB9AB185C610175C84625462, /*hidden argument*/NULL);
		// break;
		goto IL_0164;
	}

IL_0153:
	{
		// if (builder != null)
		StringBuilder_t * L_64 = V_0;
		if (!L_64)
		{
			goto IL_0164;
		}
	}
	{
		// builder.Append(str[i]);
		StringBuilder_t * L_65 = V_0;
		String_t* L_66 = ___str0;
		int32_t L_67 = V_1;
		NullCheck(L_66);
		Il2CppChar L_68;
		L_68 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_66, L_67, /*hidden argument*/NULL);
		NullCheck(L_65);
		StringBuilder_t * L_69;
		L_69 = StringBuilder_Append_m1ADA3C16E40BF253BCDB5F9579B4DBA9C3E5B22E(L_65, L_68, /*hidden argument*/NULL);
	}

IL_0164:
	{
		// for (int i = 0; i < str.Length; ++i)
		int32_t L_70 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_70, (int32_t)1));
	}

IL_0168:
	{
		// for (int i = 0; i < str.Length; ++i)
		int32_t L_71 = V_1;
		String_t* L_72 = ___str0;
		NullCheck(L_72);
		int32_t L_73;
		L_73 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_72, /*hidden argument*/NULL);
		if ((((int32_t)L_71) < ((int32_t)L_73)))
		{
			goto IL_0009;
		}
	}
	{
		// if (builder != null)
		StringBuilder_t * L_74 = V_0;
		if (!L_74)
		{
			goto IL_017e;
		}
	}
	{
		// return builder.ToString();
		StringBuilder_t * L_75 = V_0;
		NullCheck(L_75);
		String_t* L_76;
		L_76 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_75);
		return L_76;
	}

IL_017e:
	{
		// return str;
		String_t* L_77 = ___str0;
		return L_77;
	}
}
// System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader> RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::ParseJsonHTTPHeadersIntoHTTPHeaderList(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * PlatformOptions_ParseJsonHTTPHeadersIntoHTTPHeaderList_m0C5A914EF74EA3DB7ABCEB1FF75F5B782E38E59D (String_t* ___httpHeaderJson0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mED07ADBCE34435B4A44D92F49320193766170074_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mA4972EA6AD7EDA75836F78B588F59FC023AB4DDA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF5FC1450686FCAF113D3F1C5FAEA1371BEAC8879);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * V_4 = NULL;
	Il2CppChar V_5 = 0x0;
	Il2CppChar V_6 = 0x0;
	int32_t V_7 = 0;
	{
		// ParseJSONHeadersState state = ParseJSONHeadersState.Begin;
		V_0 = 0;
		// int j = 0;
		V_1 = 0;
		// string key = null;
		V_2 = (String_t*)NULL;
		// string value = null;
		V_3 = (String_t*)NULL;
		// List<HTTPHeader> headers = new List<HTTPHeader>();
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_0 = (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 *)il2cpp_codegen_object_new(List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452_il2cpp_TypeInfo_var);
		List_1__ctor_mA4972EA6AD7EDA75836F78B588F59FC023AB4DDA(L_0, /*hidden argument*/List_1__ctor_mA4972EA6AD7EDA75836F78B588F59FC023AB4DDA_RuntimeMethod_var);
		V_4 = L_0;
		// System.Char c = '\0';
		V_5 = 0;
		// System.Char pc = c;
		Il2CppChar L_1 = V_5;
		V_6 = L_1;
		// for (int i = 0; i < httpHeaderJson.Length; ++i)
		V_7 = 0;
		goto IL_015b;
	}

IL_001e:
	{
		// if (state == ParseJSONHeadersState.Finished || state == ParseJSONHeadersState.Failed)
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)7)))
		{
			goto IL_0168;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)8)))
		{
			goto IL_0168;
		}
	}
	{
		// pc = c;
		Il2CppChar L_4 = V_5;
		V_6 = L_4;
		// c = httpHeaderJson[i];
		String_t* L_5 = ___httpHeaderJson0;
		int32_t L_6 = V_7;
		NullCheck(L_5);
		Il2CppChar L_7;
		L_7 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_5, L_6, /*hidden argument*/NULL);
		V_5 = L_7;
		int32_t L_8 = V_0;
		switch (L_8)
		{
			case 0:
			{
				goto IL_0069;
			}
			case 1:
			{
				goto IL_0089;
			}
			case 2:
			{
				goto IL_00bb;
			}
			case 3:
			{
				goto IL_00dd;
			}
			case 4:
			{
				goto IL_00f4;
			}
			case 5:
			{
				goto IL_0110;
			}
			case 6:
			{
				goto IL_013a;
			}
			case 7:
			{
				goto IL_0155;
			}
			case 8:
			{
				goto IL_0155;
			}
		}
	}
	{
		goto IL_0155;
	}

IL_0069:
	{
		// if (System.Char.IsWhiteSpace(c))
		Il2CppChar L_9 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_il2cpp_TypeInfo_var);
		bool L_10;
		L_10 = Char_IsWhiteSpace_m99A5E1BE1EB9F17EA530A67A607DA8C260BCBF99(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0155;
		}
	}
	{
		// if (c == '{')
		Il2CppChar L_11 = V_5;
		if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)123)))))
		{
			goto IL_0082;
		}
	}
	{
		// state = ParseJSONHeadersState.FindKey;
		V_0 = 1;
		goto IL_0155;
	}

IL_0082:
	{
		// state = ParseJSONHeadersState.Failed;
		V_0 = 8;
		// break;
		goto IL_0155;
	}

IL_0089:
	{
		// if (System.Char.IsWhiteSpace(c))
		Il2CppChar L_12 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_il2cpp_TypeInfo_var);
		bool L_13;
		L_13 = Char_IsWhiteSpace_m99A5E1BE1EB9F17EA530A67A607DA8C260BCBF99(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0155;
		}
	}
	{
		// if (c == '"')
		Il2CppChar L_14 = V_5;
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_00a7;
		}
	}
	{
		// state = ParseJSONHeadersState.ReadKey;
		V_0 = 2;
		// j = i + 1;
		int32_t L_15 = V_7;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
		// }
		goto IL_0155;
	}

IL_00a7:
	{
		// else if (c == '}')
		Il2CppChar L_16 = V_5;
		if ((!(((uint32_t)L_16) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_00b4;
		}
	}
	{
		// state = ParseJSONHeadersState.Finished;
		V_0 = 7;
		// }
		goto IL_0155;
	}

IL_00b4:
	{
		// state = ParseJSONHeadersState.Failed;
		V_0 = 8;
		// break;
		goto IL_0155;
	}

IL_00bb:
	{
		// if (c == '"' && pc != '\\')
		Il2CppChar L_17 = V_5;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0155;
		}
	}
	{
		Il2CppChar L_18 = V_6;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)92))))
		{
			goto IL_0155;
		}
	}
	{
		// key = httpHeaderJson.Substring(j, i - j);
		String_t* L_19 = ___httpHeaderJson0;
		int32_t L_20 = V_1;
		int32_t L_21 = V_7;
		int32_t L_22 = V_1;
		NullCheck(L_19);
		String_t* L_23;
		L_23 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_19, L_20, ((int32_t)il2cpp_codegen_subtract((int32_t)L_21, (int32_t)L_22)), /*hidden argument*/NULL);
		V_2 = L_23;
		// state = ParseJSONHeadersState.FindColon;
		V_0 = 3;
		// }
		goto IL_0155;
	}

IL_00dd:
	{
		// if (System.Char.IsWhiteSpace(c))
		Il2CppChar L_24 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_il2cpp_TypeInfo_var);
		bool L_25;
		L_25 = Char_IsWhiteSpace_m99A5E1BE1EB9F17EA530A67A607DA8C260BCBF99(L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_0155;
		}
	}
	{
		// if (c == ':')
		Il2CppChar L_26 = V_5;
		if ((!(((uint32_t)L_26) == ((uint32_t)((int32_t)58)))))
		{
			goto IL_00f0;
		}
	}
	{
		// state = ParseJSONHeadersState.FindValue;
		V_0 = 4;
		goto IL_0155;
	}

IL_00f0:
	{
		// state = ParseJSONHeadersState.Failed;
		V_0 = 8;
		// break;
		goto IL_0155;
	}

IL_00f4:
	{
		// if (System.Char.IsWhiteSpace(c))
		Il2CppChar L_27 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_il2cpp_TypeInfo_var);
		bool L_28;
		L_28 = Char_IsWhiteSpace_m99A5E1BE1EB9F17EA530A67A607DA8C260BCBF99(L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_0155;
		}
	}
	{
		// if (c == '"')
		Il2CppChar L_29 = V_5;
		if ((!(((uint32_t)L_29) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_010c;
		}
	}
	{
		// state = ParseJSONHeadersState.ReadValue;
		V_0 = 5;
		// j = i + 1;
		int32_t L_30 = V_7;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
		// }
		goto IL_0155;
	}

IL_010c:
	{
		// state = ParseJSONHeadersState.Failed;
		V_0 = 8;
		// break;
		goto IL_0155;
	}

IL_0110:
	{
		// if (c == '"' && pc != '\\')
		Il2CppChar L_31 = V_5;
		if ((!(((uint32_t)L_31) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0155;
		}
	}
	{
		Il2CppChar L_32 = V_6;
		if ((((int32_t)L_32) == ((int32_t)((int32_t)92))))
		{
			goto IL_0155;
		}
	}
	{
		// value = httpHeaderJson.Substring(j, i - j);
		String_t* L_33 = ___httpHeaderJson0;
		int32_t L_34 = V_1;
		int32_t L_35 = V_7;
		int32_t L_36 = V_1;
		NullCheck(L_33);
		String_t* L_37;
		L_37 = String_Substring_m7A39A2AC0893AE940CF4CEC841326D56FFB9D86B(L_33, L_34, ((int32_t)il2cpp_codegen_subtract((int32_t)L_35, (int32_t)L_36)), /*hidden argument*/NULL);
		V_3 = L_37;
		// headers.Add(new HTTPHeader(key, value));
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_38 = V_4;
		String_t* L_39 = V_2;
		String_t* L_40 = V_3;
		HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  L_41;
		memset((&L_41), 0, sizeof(L_41));
		HTTPHeader__ctor_mBD26E64BA6EB557D29C8E5E346802BD3BF6E199E((&L_41), L_39, L_40, /*hidden argument*/NULL);
		NullCheck(L_38);
		List_1_Add_mED07ADBCE34435B4A44D92F49320193766170074(L_38, L_41, /*hidden argument*/List_1_Add_mED07ADBCE34435B4A44D92F49320193766170074_RuntimeMethod_var);
		// state = ParseJSONHeadersState.FindComma;
		V_0 = 6;
		// }
		goto IL_0155;
	}

IL_013a:
	{
		// if (System.Char.IsWhiteSpace(c))
		Il2CppChar L_42 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_il2cpp_TypeInfo_var);
		bool L_43;
		L_43 = Char_IsWhiteSpace_m99A5E1BE1EB9F17EA530A67A607DA8C260BCBF99(L_42, /*hidden argument*/NULL);
		if (L_43)
		{
			goto IL_0155;
		}
	}
	{
		// if (c == ',')
		Il2CppChar L_44 = V_5;
		if ((!(((uint32_t)L_44) == ((uint32_t)((int32_t)44)))))
		{
			goto IL_014d;
		}
	}
	{
		// state = ParseJSONHeadersState.FindKey;
		V_0 = 1;
		goto IL_0155;
	}

IL_014d:
	{
		// else if (c == '}')
		Il2CppChar L_45 = V_5;
		if ((!(((uint32_t)L_45) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_0155;
		}
	}
	{
		// state = ParseJSONHeadersState.Finished;
		V_0 = 7;
	}

IL_0155:
	{
		// for (int i = 0; i < httpHeaderJson.Length; ++i)
		int32_t L_46 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_46, (int32_t)1));
	}

IL_015b:
	{
		// for (int i = 0; i < httpHeaderJson.Length; ++i)
		int32_t L_47 = V_7;
		String_t* L_48 = ___httpHeaderJson0;
		NullCheck(L_48);
		int32_t L_49;
		L_49 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_48, /*hidden argument*/NULL);
		if ((((int32_t)L_47) < ((int32_t)L_49)))
		{
			goto IL_001e;
		}
	}

IL_0168:
	{
		// if (state == ParseJSONHeadersState.Finished)
		int32_t L_50 = V_0;
		if ((!(((uint32_t)L_50) == ((uint32_t)7))))
		{
			goto IL_016f;
		}
	}
	{
		// return headers;
		List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * L_51 = V_4;
		return L_51;
	}

IL_016f:
	{
		// Debug.LogWarning("Failed to convert HTTP headers from Json, you will need to do this manually.");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogWarning_m24085D883C9E74D7AB423F0625E13259923960E7(_stringLiteralF5FC1450686FCAF113D3F1C5FAEA1371BEAC8879, /*hidden argument*/NULL);
		// return null;
		return (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 *)NULL;
	}
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlatformOptions__ctor_m0765FB48C8311B03E858B0E7ACB26870CA7E1FD5 (PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F * __this, const RuntimeMethod* method)
{
	{
		// public FileLocation pathLocation = FileLocation.RelativeToStreamingAssetsFolder;
		__this->set_pathLocation_1(2);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProcessExtractedFrame__ctor_m2D45C0063D44914869401FA6A6EEBAE26CC5753A (ProcessExtractedFrame_t9BDA228D84FCD6905FA985C6759CD5E40590AA45 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame::Invoke(UnityEngine.Texture2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProcessExtractedFrame_Invoke_m93D4170ADCD901788FA035AD906788CD07C8ED5F (ProcessExtractedFrame_t9BDA228D84FCD6905FA985C6759CD5E40590AA45 * __this, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___extractedFrame0, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___extractedFrame0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___extractedFrame0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___extractedFrame0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___extractedFrame0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___extractedFrame0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___extractedFrame0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___extractedFrame0, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * >::Invoke(targetMethod, targetThis, ___extractedFrame0);
					else
						GenericVirtActionInvoker1< Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * >::Invoke(targetMethod, targetThis, ___extractedFrame0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___extractedFrame0);
					else
						VirtActionInvoker1< Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___extractedFrame0);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___extractedFrame0, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___extractedFrame0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame::BeginInvoke(UnityEngine.Texture2D,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ProcessExtractedFrame_BeginInvoke_mD54E1E8BC0E9A671D24705DC6221FB4D8B284C2D (ProcessExtractedFrame_t9BDA228D84FCD6905FA985C6759CD5E40590AA45 * __this, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___extractedFrame0, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___extractedFrame0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);;
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProcessExtractedFrame_EndInvoke_m95A67F49C84E75C01BDD7931DFA69D12ADEC60C2 (ProcessExtractedFrame_t9BDA228D84FCD6905FA985C6759CD5E40590AA45 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/Setup::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Setup__ctor_m8D471ACF422DEC1844B08DE6298E7EF61F6E9241 (Setup_tD0FAF61885F9D7910834A6774C9282AE0C7D28B6 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MediaItem__ctor_m73DE5A23DA10B87FEEF58F5D2586BB873842F64C (MediaItem_tC501E368D1B426969A72432E4015143FD9A278BF * __this, const RuntimeMethod* method)
{
	{
		// public MediaPlayer.FileLocation fileLocation = MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder;
		__this->set_fileLocation_0(2);
		// public float progressTimeSeconds = 0.5f;
		__this->set_progressTimeSeconds_5((0.5f));
		// public bool autoPlay = true;
		__this->set_autoPlay_6((bool)1);
		// public float overrideTransitionDuration = 1f;
		__this->set_overrideTransitionDuration_11((1.0f));
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Func`2<System.Single,System.Single> RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::GetFunction(RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing/Preset)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * Easing_GetFunction_m6BF9C691739142093824E65BF9D0B3959716E3B5 (int32_t ___preset0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_InCubic_m0F57ABFDA1DAF00A462FC55900C346C7B4308B08_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_InExpo_m611AB71C3B0E391EE9824BFB4271CABA990D2AFE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_InOutCubic_m2C0042F5722F61B0CF3B2C63DDADC2C045F41523_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_InOutExpo_mD33AD178CF4F37856E199F4247D55D1B6C904F36_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_InOutQuad_mD67729B41C8D095A565A1AEDA43FE0FF363E303B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_InOutQuart_m2A57949CDE900057F1F9D17C482242DCB35AE50A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_InOutQuint_mA57462E560637C403AD73D55CF03A4ECD29F2C51_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_InQuad_m835BD34EF6B489D51D8B7E31D5421BC89DF65998_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_InQuart_m9C1DD3B8219F88569905FB1B38B88E042A0D3245_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_InQuint_m59758F196F7FFE458A32F6F14B5429946A6FB4BC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_Linear_mA407428D581C8BA5C60991186DE52D521D8EEC5B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_OutCubic_m2693C08EED8C8A119C6362B759A27AABFE9C32FF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_OutExpo_mA71C6B4FE36A0F88D3FD06106B0BE7F3E24B8356_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_OutQuad_m810849EFC4CDD6477028DA82FB5006E3428407DD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_OutQuart_m494232308BA295350A858BE1E2FA475AC3A85FFF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_OutQuint_m0494D176295021B3512BAB04407A172F302A322E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Easing_Step_m74FF1FA20E4A7FE4232FBD5B0EFBEA036171605C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * V_0 = NULL;
	{
		// System.Func<float, float> result = null;
		V_0 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)NULL;
		int32_t L_0 = ___preset0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0059;
			}
			case 1:
			{
				goto IL_006b;
			}
			case 2:
			{
				goto IL_007d;
			}
			case 3:
			{
				goto IL_008f;
			}
			case 4:
			{
				goto IL_00a1;
			}
			case 5:
			{
				goto IL_00b3;
			}
			case 6:
			{
				goto IL_00c5;
			}
			case 7:
			{
				goto IL_00d7;
			}
			case 8:
			{
				goto IL_00e9;
			}
			case 9:
			{
				goto IL_00fb;
			}
			case 10:
			{
				goto IL_010d;
			}
			case 11:
			{
				goto IL_011c;
			}
			case 12:
			{
				goto IL_012b;
			}
			case 13:
			{
				goto IL_013a;
			}
			case 14:
			{
				goto IL_0149;
			}
			case 15:
			{
				goto IL_0158;
			}
			case 16:
			{
				goto IL_0167;
			}
			case 17:
			{
				goto IL_0176;
			}
			case 18:
			{
				goto IL_0186;
			}
		}
	}
	{
		goto IL_0194;
	}

IL_0059:
	{
		// result = Step;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_1 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_1, NULL, (intptr_t)((intptr_t)Easing_Step_m74FF1FA20E4A7FE4232FBD5B0EFBEA036171605C_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_1;
		// break;
		goto IL_0194;
	}

IL_006b:
	{
		// result = Linear;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_2 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_2, NULL, (intptr_t)((intptr_t)Easing_Linear_mA407428D581C8BA5C60991186DE52D521D8EEC5B_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_2;
		// break;
		goto IL_0194;
	}

IL_007d:
	{
		// result = InQuad;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_3 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_3, NULL, (intptr_t)((intptr_t)Easing_InQuad_m835BD34EF6B489D51D8B7E31D5421BC89DF65998_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_3;
		// break;
		goto IL_0194;
	}

IL_008f:
	{
		// result = OutQuad;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_4 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_4, NULL, (intptr_t)((intptr_t)Easing_OutQuad_m810849EFC4CDD6477028DA82FB5006E3428407DD_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_4;
		// break;
		goto IL_0194;
	}

IL_00a1:
	{
		// result = InOutQuad;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_5 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_5, NULL, (intptr_t)((intptr_t)Easing_InOutQuad_mD67729B41C8D095A565A1AEDA43FE0FF363E303B_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_5;
		// break;
		goto IL_0194;
	}

IL_00b3:
	{
		// result = InCubic;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_6 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_6, NULL, (intptr_t)((intptr_t)Easing_InCubic_m0F57ABFDA1DAF00A462FC55900C346C7B4308B08_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_6;
		// break;
		goto IL_0194;
	}

IL_00c5:
	{
		// result = OutCubic;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_7 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_7, NULL, (intptr_t)((intptr_t)Easing_OutCubic_m2693C08EED8C8A119C6362B759A27AABFE9C32FF_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_7;
		// break;
		goto IL_0194;
	}

IL_00d7:
	{
		// result = InOutCubic;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_8 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_8, NULL, (intptr_t)((intptr_t)Easing_InOutCubic_m2C0042F5722F61B0CF3B2C63DDADC2C045F41523_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_8;
		// break;
		goto IL_0194;
	}

IL_00e9:
	{
		// result = InQuint;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_9 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_9, NULL, (intptr_t)((intptr_t)Easing_InQuint_m59758F196F7FFE458A32F6F14B5429946A6FB4BC_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_9;
		// break;
		goto IL_0194;
	}

IL_00fb:
	{
		// result = OutQuint;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_10 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_10, NULL, (intptr_t)((intptr_t)Easing_OutQuint_m0494D176295021B3512BAB04407A172F302A322E_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_10;
		// break;
		goto IL_0194;
	}

IL_010d:
	{
		// result = InOutQuint;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_11 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_11, NULL, (intptr_t)((intptr_t)Easing_InOutQuint_mA57462E560637C403AD73D55CF03A4ECD29F2C51_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_11;
		// break;
		goto IL_0194;
	}

IL_011c:
	{
		// result = InQuart;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_12 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_12, NULL, (intptr_t)((intptr_t)Easing_InQuart_m9C1DD3B8219F88569905FB1B38B88E042A0D3245_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_12;
		// break;
		goto IL_0194;
	}

IL_012b:
	{
		// result = OutQuart;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_13 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_13, NULL, (intptr_t)((intptr_t)Easing_OutQuart_m494232308BA295350A858BE1E2FA475AC3A85FFF_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_13;
		// break;
		goto IL_0194;
	}

IL_013a:
	{
		// result = InOutQuart;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_14 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_14, NULL, (intptr_t)((intptr_t)Easing_InOutQuart_m2A57949CDE900057F1F9D17C482242DCB35AE50A_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_14;
		// break;
		goto IL_0194;
	}

IL_0149:
	{
		// result = InExpo;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_15 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_15, NULL, (intptr_t)((intptr_t)Easing_InExpo_m611AB71C3B0E391EE9824BFB4271CABA990D2AFE_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_15;
		// break;
		goto IL_0194;
	}

IL_0158:
	{
		// result = OutExpo;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_16 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_16, NULL, (intptr_t)((intptr_t)Easing_OutExpo_mA71C6B4FE36A0F88D3FD06106B0BE7F3E24B8356_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_16;
		// break;
		goto IL_0194;
	}

IL_0167:
	{
		// result = InOutExpo;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_17 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)il2cpp_codegen_object_new(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149_il2cpp_TypeInfo_var);
		Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB(L_17, NULL, (intptr_t)((intptr_t)Easing_InOutExpo_mD33AD178CF4F37856E199F4247D55D1B6C904F36_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m4F0F14ACA9CE9640DCA30B2651BAF3A917998BCB_RuntimeMethod_var);
		V_0 = L_17;
		// break;
		goto IL_0194;
	}

IL_0176:
	{
		// result = GetFunction((Preset)Random.Range(0, (int)Preset.Random));
		int32_t L_18;
		L_18 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, ((int32_t)17), /*hidden argument*/NULL);
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_19;
		L_19 = Easing_GetFunction_m6BF9C691739142093824E65BF9D0B3959716E3B5(L_18, /*hidden argument*/NULL);
		V_0 = L_19;
		// break;
		goto IL_0194;
	}

IL_0186:
	{
		// result = GetFunction((Preset)Random.Range((int)Preset.Step+1, (int)Preset.Random));
		int32_t L_20;
		L_20 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(1, ((int32_t)17), /*hidden argument*/NULL);
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_21;
		L_21 = Easing_GetFunction_m6BF9C691739142093824E65BF9D0B3959716E3B5(L_20, /*hidden argument*/NULL);
		V_0 = L_21;
	}

IL_0194:
	{
		// return result;
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_22 = V_0;
		return L_22;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::PowerEaseIn(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_PowerEaseIn_m452740837B6A23AB4DA085C09A56239B2D61E9A3 (float ___t0, float ___power1, const RuntimeMethod* method)
{
	{
		// return Mathf.Pow(t, power);
		float L_0 = ___t0;
		float L_1 = ___power1;
		float L_2;
		L_2 = powf(L_0, L_1);
		return L_2;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::PowerEaseOut(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_PowerEaseOut_m9029AE6D2160B8497333B9D381DDA8B364311565 (float ___t0, float ___power1, const RuntimeMethod* method)
{
	{
		// return 1f - Mathf.Abs(Mathf.Pow(t - 1f, power));
		float L_0 = ___t0;
		float L_1 = ___power1;
		float L_2;
		L_2 = powf(((float)il2cpp_codegen_subtract((float)L_0, (float)(1.0f))), L_1);
		float L_3;
		L_3 = fabsf(L_2);
		return ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_3));
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::PowerEaseInOut(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_PowerEaseInOut_m0C91FD6F72C67D6C21A0BAC622714175010F60C8 (float ___t0, float ___power1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// if (t < 0.5f)
		float L_0 = ___t0;
		if ((!(((float)L_0) < ((float)(0.5f)))))
		{
			goto IL_001e;
		}
	}
	{
		// result = PowerEaseIn(t * 2f, power) / 2f;
		float L_1 = ___t0;
		float L_2 = ___power1;
		float L_3;
		L_3 = Easing_PowerEaseIn_m452740837B6A23AB4DA085C09A56239B2D61E9A3(((float)il2cpp_codegen_multiply((float)L_1, (float)(2.0f))), L_2, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_3/(float)(2.0f)));
		// }
		goto IL_003e;
	}

IL_001e:
	{
		// result = PowerEaseOut(t * 2f - 1f, power) / 2f + 0.5f;
		float L_4 = ___t0;
		float L_5 = ___power1;
		float L_6;
		L_6 = Easing_PowerEaseOut_m9029AE6D2160B8497333B9D381DDA8B364311565(((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_4, (float)(2.0f))), (float)(1.0f))), L_5, /*hidden argument*/NULL);
		V_0 = ((float)il2cpp_codegen_add((float)((float)((float)L_6/(float)(2.0f))), (float)(0.5f)));
	}

IL_003e:
	{
		// return result;
		float L_7 = V_0;
		return L_7;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::Step(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_Step_m74FF1FA20E4A7FE4232FBD5B0EFBEA036171605C (float ___t0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// float result = 0f;
		V_0 = (0.0f);
		// if (t >= 0.5f)
		float L_0 = ___t0;
		if ((!(((float)L_0) >= ((float)(0.5f)))))
		{
			goto IL_0014;
		}
	}
	{
		// result = 1f;
		V_0 = (1.0f);
	}

IL_0014:
	{
		// return result;
		float L_1 = V_0;
		return L_1;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::Linear(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_Linear_mA407428D581C8BA5C60991186DE52D521D8EEC5B (float ___t0, const RuntimeMethod* method)
{
	{
		// return t;
		float L_0 = ___t0;
		return L_0;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InQuad(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_InQuad_m835BD34EF6B489D51D8B7E31D5421BC89DF65998 (float ___t0, const RuntimeMethod* method)
{
	{
		// return PowerEaseIn(t, 2f);
		float L_0 = ___t0;
		float L_1;
		L_1 = Easing_PowerEaseIn_m452740837B6A23AB4DA085C09A56239B2D61E9A3(L_0, (2.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::OutQuad(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_OutQuad_m810849EFC4CDD6477028DA82FB5006E3428407DD (float ___t0, const RuntimeMethod* method)
{
	{
		// return PowerEaseOut(t, 2f);
		float L_0 = ___t0;
		float L_1;
		L_1 = Easing_PowerEaseOut_m9029AE6D2160B8497333B9D381DDA8B364311565(L_0, (2.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InOutQuad(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_InOutQuad_mD67729B41C8D095A565A1AEDA43FE0FF363E303B (float ___t0, const RuntimeMethod* method)
{
	{
		// return PowerEaseInOut(t, 2f);
		float L_0 = ___t0;
		float L_1;
		L_1 = Easing_PowerEaseInOut_m0C91FD6F72C67D6C21A0BAC622714175010F60C8(L_0, (2.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InCubic(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_InCubic_m0F57ABFDA1DAF00A462FC55900C346C7B4308B08 (float ___t0, const RuntimeMethod* method)
{
	{
		// return PowerEaseIn(t, 3f);
		float L_0 = ___t0;
		float L_1;
		L_1 = Easing_PowerEaseIn_m452740837B6A23AB4DA085C09A56239B2D61E9A3(L_0, (3.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::OutCubic(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_OutCubic_m2693C08EED8C8A119C6362B759A27AABFE9C32FF (float ___t0, const RuntimeMethod* method)
{
	{
		// return PowerEaseOut(t, 3f);
		float L_0 = ___t0;
		float L_1;
		L_1 = Easing_PowerEaseOut_m9029AE6D2160B8497333B9D381DDA8B364311565(L_0, (3.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InOutCubic(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_InOutCubic_m2C0042F5722F61B0CF3B2C63DDADC2C045F41523 (float ___t0, const RuntimeMethod* method)
{
	{
		// return PowerEaseInOut(t, 3f);
		float L_0 = ___t0;
		float L_1;
		L_1 = Easing_PowerEaseInOut_m0C91FD6F72C67D6C21A0BAC622714175010F60C8(L_0, (3.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InQuart(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_InQuart_m9C1DD3B8219F88569905FB1B38B88E042A0D3245 (float ___t0, const RuntimeMethod* method)
{
	{
		// return PowerEaseIn(t, 4f);
		float L_0 = ___t0;
		float L_1;
		L_1 = Easing_PowerEaseIn_m452740837B6A23AB4DA085C09A56239B2D61E9A3(L_0, (4.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::OutQuart(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_OutQuart_m494232308BA295350A858BE1E2FA475AC3A85FFF (float ___t0, const RuntimeMethod* method)
{
	{
		// return PowerEaseOut(t, 4f);
		float L_0 = ___t0;
		float L_1;
		L_1 = Easing_PowerEaseOut_m9029AE6D2160B8497333B9D381DDA8B364311565(L_0, (4.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InOutQuart(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_InOutQuart_m2A57949CDE900057F1F9D17C482242DCB35AE50A (float ___t0, const RuntimeMethod* method)
{
	{
		// return PowerEaseInOut(t, 4f);
		float L_0 = ___t0;
		float L_1;
		L_1 = Easing_PowerEaseInOut_m0C91FD6F72C67D6C21A0BAC622714175010F60C8(L_0, (4.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InQuint(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_InQuint_m59758F196F7FFE458A32F6F14B5429946A6FB4BC (float ___t0, const RuntimeMethod* method)
{
	{
		// return PowerEaseIn(t, 5f);
		float L_0 = ___t0;
		float L_1;
		L_1 = Easing_PowerEaseIn_m452740837B6A23AB4DA085C09A56239B2D61E9A3(L_0, (5.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::OutQuint(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_OutQuint_m0494D176295021B3512BAB04407A172F302A322E (float ___t0, const RuntimeMethod* method)
{
	{
		// return PowerEaseOut(t, 5f);
		float L_0 = ___t0;
		float L_1;
		L_1 = Easing_PowerEaseOut_m9029AE6D2160B8497333B9D381DDA8B364311565(L_0, (5.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InOutQuint(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_InOutQuint_mA57462E560637C403AD73D55CF03A4ECD29F2C51 (float ___t0, const RuntimeMethod* method)
{
	{
		// return PowerEaseInOut(t, 5f);
		float L_0 = ___t0;
		float L_1;
		L_1 = Easing_PowerEaseInOut_m0C91FD6F72C67D6C21A0BAC622714175010F60C8(L_0, (5.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InExpo(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_InExpo_m611AB71C3B0E391EE9824BFB4271CABA990D2AFE (float ___t0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// float result = 0f;
		V_0 = (0.0f);
		// if (t != 0f)
		float L_0 = ___t0;
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0026;
		}
	}
	{
		// result = Mathf.Pow(2f, 10f * (t - 1f));
		float L_1 = ___t0;
		float L_2;
		L_2 = powf((2.0f), ((float)il2cpp_codegen_multiply((float)(10.0f), (float)((float)il2cpp_codegen_subtract((float)L_1, (float)(1.0f))))));
		V_0 = L_2;
	}

IL_0026:
	{
		// return result;
		float L_3 = V_0;
		return L_3;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::OutExpo(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_OutExpo_mA71C6B4FE36A0F88D3FD06106B0BE7F3E24B8356 (float ___t0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// float result = 1f;
		V_0 = (1.0f);
		// if (t != 1f)
		float L_0 = ___t0;
		if ((((float)L_0) == ((float)(1.0f))))
		{
			goto IL_0027;
		}
	}
	{
		// result = -Mathf.Pow(2f, -10f * t) + 1f;
		float L_1 = ___t0;
		float L_2;
		L_2 = powf((2.0f), ((float)il2cpp_codegen_multiply((float)(-10.0f), (float)L_1)));
		V_0 = ((float)il2cpp_codegen_add((float)((-L_2)), (float)(1.0f)));
	}

IL_0027:
	{
		// return result;
		float L_3 = V_0;
		return L_3;
	}
}
// System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InOutExpo(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Easing_InOutExpo_mD33AD178CF4F37856E199F4247D55D1B6C904F36 (float ___t0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// float result = 0f;
		V_0 = (0.0f);
		// if (t > 0f)
		float L_0 = ___t0;
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0075;
		}
	}
	{
		// result = 1f;
		V_0 = (1.0f);
		// if (t < 1f)
		float L_1 = ___t0;
		if ((!(((float)L_1) < ((float)(1.0f)))))
		{
			goto IL_0075;
		}
	}
	{
		// t *= 2f;
		float L_2 = ___t0;
		___t0 = ((float)il2cpp_codegen_multiply((float)L_2, (float)(2.0f)));
		// if (t < 1f)
		float L_3 = ___t0;
		if ((!(((float)L_3) < ((float)(1.0f)))))
		{
			goto IL_004d;
		}
	}
	{
		// result = 0.5f * Mathf.Pow(2f, 10f * (t - 1f));
		float L_4 = ___t0;
		float L_5;
		L_5 = powf((2.0f), ((float)il2cpp_codegen_multiply((float)(10.0f), (float)((float)il2cpp_codegen_subtract((float)L_4, (float)(1.0f))))));
		V_0 = ((float)il2cpp_codegen_multiply((float)(0.5f), (float)L_5));
		// }
		goto IL_0075;
	}

IL_004d:
	{
		// t--;
		float L_6 = ___t0;
		___t0 = ((float)il2cpp_codegen_subtract((float)L_6, (float)(1.0f)));
		// result = 0.5f * (-Mathf.Pow(2f, -10f * t) + 2f);
		float L_7 = ___t0;
		float L_8;
		L_8 = powf((2.0f), ((float)il2cpp_codegen_multiply((float)(-10.0f), (float)L_7)));
		V_0 = ((float)il2cpp_codegen_multiply((float)(0.5f), (float)((float)il2cpp_codegen_add((float)((-L_8)), (float)(2.0f)))));
	}

IL_0075:
	{
		// return result;
		float L_9 = V_0;
		return L_9;
	}
}
// System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Easing__ctor_m6AAB60805D5F0E34EA019B566FE2E5092839629B (Easing_tDD586F922397A60A9F213AA64B7AFC753FE0686D * __this, const RuntimeMethod* method)
{
	{
		// public Preset preset = Preset.Linear;
		__this->set_preset_0(1);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager/NamedPrefab
IL2CPP_EXTERN_C void NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E_marshal_pinvoke(const NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E& unmarshaled, NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E_marshaled_pinvoke& marshaled)
{
	Exception_t* ___imagePrefab_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'imagePrefab' of type 'NamedPrefab': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___imagePrefab_1Exception, NULL);
}
IL2CPP_EXTERN_C void NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E_marshal_pinvoke_back(const NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E_marshaled_pinvoke& marshaled, NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E& unmarshaled)
{
	Exception_t* ___imagePrefab_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'imagePrefab' of type 'NamedPrefab': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___imagePrefab_1Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager/NamedPrefab
IL2CPP_EXTERN_C void NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E_marshal_pinvoke_cleanup(NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager/NamedPrefab
IL2CPP_EXTERN_C void NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E_marshal_com(const NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E& unmarshaled, NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E_marshaled_com& marshaled)
{
	Exception_t* ___imagePrefab_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'imagePrefab' of type 'NamedPrefab': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___imagePrefab_1Exception, NULL);
}
IL2CPP_EXTERN_C void NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E_marshal_com_back(const NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E_marshaled_com& marshaled, NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E& unmarshaled)
{
	Exception_t* ___imagePrefab_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'imagePrefab' of type 'NamedPrefab': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___imagePrefab_1Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager/NamedPrefab
IL2CPP_EXTERN_C void NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E_marshal_com_cleanup(NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager/NamedPrefab::.ctor(System.Guid,UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NamedPrefab__ctor_m4C5A1EAC954851954E10ADF5F4ED646967BAB252 (NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E * __this, Guid_t  ___guid0, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___prefab1, const RuntimeMethod* method)
{
	{
		// imageGuid = guid.ToString();
		String_t* L_0;
		L_0 = Guid_ToString_mA3AB7742FB0E04808F580868E82BDEB93187FB75((Guid_t *)(&___guid0), /*hidden argument*/NULL);
		__this->set_imageGuid_0(L_0);
		// imagePrefab = prefab;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = ___prefab1;
		__this->set_imagePrefab_1(L_1);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void NamedPrefab__ctor_m4C5A1EAC954851954E10ADF5F4ED646967BAB252_AdjustorThunk (RuntimeObject * __this, Guid_t  ___guid0, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___prefab1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E * _thisAdjusted = reinterpret_cast<NamedPrefab_t5879F20FF51DA02E01C5939FCCA8628089EDF82E *>(__this + _offset);
	NamedPrefab__ctor_m4C5A1EAC954851954E10ADF5F4ED646967BAB252(_thisAdjusted, ___guid0, ___prefab1, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PreloaderController/<DisablePreloaderPanel>d__3::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDisablePreloaderPanelU3Ed__3__ctor_m78A4AD06EDA904CACCAC2FCFE42949BB0D950658 (U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void PreloaderController/<DisablePreloaderPanel>d__3::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDisablePreloaderPanelU3Ed__3_System_IDisposable_Dispose_mB1FFDC5C6C5877BA63FC0D5EDD299367F062C01E (U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean PreloaderController/<DisablePreloaderPanel>d__3::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CDisablePreloaderPanelU3Ed__3_MoveNext_m1295B0BBDDE925FC3BAC462DC722EF59F4301A3E (U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PreloaderController_tD3EB462D0A9954E9D9785017331EFFC5C9072801 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		PreloaderController_tD3EB462D0A9954E9D9785017331EFFC5C9072801 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0038;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// yield return new WaitForSeconds(preloaderDelay);
		PreloaderController_tD3EB462D0A9954E9D9785017331EFFC5C9072801 * L_4 = V_1;
		NullCheck(L_4);
		float L_5 = L_4->get_preloaderDelay_5();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_6 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_6, L_5, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_6);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0038:
	{
		__this->set_U3CU3E1__state_0((-1));
		// preLoaderPanel.SetActive(false);
		PreloaderController_tD3EB462D0A9954E9D9785017331EFFC5C9072801 * L_7 = V_1;
		NullCheck(L_7);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = L_7->get_preLoaderPanel_4();
		NullCheck(L_8);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_8, (bool)0, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object PreloaderController/<DisablePreloaderPanel>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CDisablePreloaderPanelU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5E78A5B68092F270355E899D99CE2D748705A525 (U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void PreloaderController/<DisablePreloaderPanel>d__3::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDisablePreloaderPanelU3Ed__3_System_Collections_IEnumerator_Reset_mF42974D782E8ABEF2D7C266ACC18B0C5F80EF45D (U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CDisablePreloaderPanelU3Ed__3_System_Collections_IEnumerator_Reset_mF42974D782E8ABEF2D7C266ACC18B0C5F80EF45D_RuntimeMethod_var)));
	}
}
// System.Object PreloaderController/<DisablePreloaderPanel>d__3::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CDisablePreloaderPanelU3Ed__3_System_Collections_IEnumerator_get_Current_mF638D8B9A629459A3E4DE1EAB729369B457AFD82 (U3CDisablePreloaderPanelU3Ed__3_tBBEF9BDDCBE3198210845D97C3FC47561C6D4C2A * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RenderHeads.Media.AVProVideo.Resampler/TimestampedRenderTexture::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TimestampedRenderTexture__ctor_m8BD6A57BEACA9639A33C95B79E1EC9E3F02292D3 (TimestampedRenderTexture_t8DDA74DDD11B5CCACD2920F8CD536801ACD00326 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadVideoWithFadingU3Ed__23__ctor_m82DE7FDB17C4F4F5D90E14A3642D40C45237FA91 (U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadVideoWithFadingU3Ed__23_System_IDisposable_Dispose_mF6B335E78513CCE1A4BA3C32A075E004595E0B7A (U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CLoadVideoWithFadingU3Ed__23_MoveNext_mFEC22F4329D83829C97271AF392350A673BB9EF6 (U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral45CFEEE4F62A474970E25FE47E07529750C5411E);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_003e;
			}
			case 1:
			{
				goto IL_00de;
			}
			case 2:
			{
				goto IL_0110;
			}
			case 3:
			{
				goto IL_012b;
			}
			case 4:
			{
				goto IL_0146;
			}
			case 5:
			{
				goto IL_019a;
			}
			case 6:
			{
				goto IL_01d6;
			}
			case 7:
			{
				goto IL_01f1;
			}
			case 8:
			{
				goto IL_020c;
			}
			case 9:
			{
				goto IL_02a2;
			}
		}
	}
	{
		return (bool)0;
	}

IL_003e:
	{
		__this->set_U3CU3E1__state_0((-1));
		// float fade = FadeDuration;
		__this->set_U3CfadeU3E5__2_3((0.25f));
		goto IL_00e5;
	}

IL_0055:
	{
		// fade -= Time.deltaTime;
		float L_3 = __this->get_U3CfadeU3E5__2_3();
		float L_4;
		L_4 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_U3CfadeU3E5__2_3(((float)il2cpp_codegen_subtract((float)L_3, (float)L_4)));
		// fade = Mathf.Clamp(fade, 0f, FadeDuration);
		float L_5 = __this->get_U3CfadeU3E5__2_3();
		float L_6;
		L_6 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_5, (0.0f), (0.25f), /*hidden argument*/NULL);
		__this->set_U3CfadeU3E5__2_3(L_6);
		// _display._color = new Color(1f, 1f, 1f, fade / FadeDuration);
		SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * L_7 = V_1;
		NullCheck(L_7);
		DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04 * L_8 = L_7->get__display_8();
		float L_9 = __this->get_U3CfadeU3E5__2_3();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_10), (1.0f), (1.0f), (1.0f), ((float)((float)L_9/(float)(0.25f))), /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set__color_9(L_10);
		// _display._mediaPlayer.Control.SetVolume(fade / FadeDuration);
		SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * L_11 = V_1;
		NullCheck(L_11);
		DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04 * L_12 = L_11->get__display_8();
		NullCheck(L_12);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_13 = L_12->get__mediaPlayer_6();
		NullCheck(L_13);
		RuntimeObject* L_14;
		L_14 = VirtFuncInvoker0< RuntimeObject* >::Invoke(5 /* RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.MediaPlayer::get_Control() */, L_13);
		float L_15 = __this->get_U3CfadeU3E5__2_3();
		NullCheck(L_14);
		InterfaceActionInvoker1< float >::Invoke(29 /* System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetVolume(System.Single) */, IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var, L_14, ((float)((float)L_15/(float)(0.25f))));
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_00de:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_00e5:
	{
		// while (fade > 0f && Application.isPlaying)
		float L_16 = __this->get_U3CfadeU3E5__2_3();
		if ((!(((float)L_16) > ((float)(0.0f)))))
		{
			goto IL_00fc;
		}
	}
	{
		bool L_17;
		L_17 = Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567(/*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0055;
		}
	}

IL_00fc:
	{
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_18 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_18, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_18);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0110:
	{
		__this->set_U3CU3E1__state_0((-1));
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_19 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_19, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_19);
		__this->set_U3CU3E1__state_0(3);
		return (bool)1;
	}

IL_012b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_20 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_20, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_20);
		__this->set_U3CU3E1__state_0(4);
		return (bool)1;
	}

IL_0146:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (Application.isPlaying)
		bool L_21;
		L_21 = Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567(/*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_02a9;
		}
	}
	{
		// if (!_mediaPlayer.OpenVideoFromFile(_nextVideoLocation, _nextVideoPath, _mediaPlayer.m_AutoStart))
		SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * L_22 = V_1;
		NullCheck(L_22);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_23 = L_22->get__mediaPlayer_7();
		SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * L_24 = V_1;
		NullCheck(L_24);
		int32_t L_25 = L_24->get__nextVideoLocation_16();
		SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * L_26 = V_1;
		NullCheck(L_26);
		String_t* L_27 = L_26->get__nextVideoPath_17();
		SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * L_28 = V_1;
		NullCheck(L_28);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_29 = L_28->get__mediaPlayer_7();
		NullCheck(L_29);
		bool L_30 = L_29->get_m_AutoStart_7();
		NullCheck(L_23);
		bool L_31;
		L_31 = MediaPlayer_OpenVideoFromFile_mD7D917C6B56F5CC9B76A04D8E4B24219277BF881(L_23, L_25, L_27, L_30, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_01a1;
		}
	}
	{
		// Debug.LogError("Failed to open video!");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(_stringLiteral45CFEEE4F62A474970E25FE47E07529750C5411E, /*hidden argument*/NULL);
		// }
		goto IL_02a9;
	}

IL_018a:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(5);
		return (bool)1;
	}

IL_019a:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_01a1:
	{
		// while (Application.isPlaying && (VideoIsReady(_mediaPlayer) || AudioIsReady(_mediaPlayer)))
		bool L_32;
		L_32 = Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567(/*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_01c2;
		}
	}
	{
		SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * L_33 = V_1;
		NullCheck(L_33);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_34 = L_33->get__mediaPlayer_7();
		bool L_35;
		L_35 = SimpleController_VideoIsReady_m5F71FECD8B7EB71A09C8A25336E6E676A454242B(L_34, /*hidden argument*/NULL);
		if (L_35)
		{
			goto IL_018a;
		}
	}
	{
		SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * L_36 = V_1;
		NullCheck(L_36);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_37 = L_36->get__mediaPlayer_7();
		bool L_38;
		L_38 = SimpleController_AudioIsReady_mEFF0519289C815E43EE68EAC005C286BE2B5907E(L_37, /*hidden argument*/NULL);
		if (L_38)
		{
			goto IL_018a;
		}
	}

IL_01c2:
	{
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_39 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_39, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_39);
		__this->set_U3CU3E1__state_0(6);
		return (bool)1;
	}

IL_01d6:
	{
		__this->set_U3CU3E1__state_0((-1));
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_40 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_40, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_40);
		__this->set_U3CU3E1__state_0(7);
		return (bool)1;
	}

IL_01f1:
	{
		__this->set_U3CU3E1__state_0((-1));
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_41 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_41, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_41);
		__this->set_U3CU3E1__state_0(8);
		return (bool)1;
	}

IL_020c:
	{
		__this->set_U3CU3E1__state_0((-1));
		goto IL_02a9;
	}

IL_0218:
	{
		// fade += Time.deltaTime;
		float L_42 = __this->get_U3CfadeU3E5__2_3();
		float L_43;
		L_43 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_U3CfadeU3E5__2_3(((float)il2cpp_codegen_add((float)L_42, (float)L_43)));
		// fade = Mathf.Clamp(fade, 0f, FadeDuration);
		float L_44 = __this->get_U3CfadeU3E5__2_3();
		float L_45;
		L_45 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_44, (0.0f), (0.25f), /*hidden argument*/NULL);
		__this->set_U3CfadeU3E5__2_3(L_45);
		// _display._color = new Color(1f, 1f, 1f, fade / FadeDuration);
		SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * L_46 = V_1;
		NullCheck(L_46);
		DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04 * L_47 = L_46->get__display_8();
		float L_48 = __this->get_U3CfadeU3E5__2_3();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_49;
		memset((&L_49), 0, sizeof(L_49));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_49), (1.0f), (1.0f), (1.0f), ((float)((float)L_48/(float)(0.25f))), /*hidden argument*/NULL);
		NullCheck(L_47);
		L_47->set__color_9(L_49);
		// _display._mediaPlayer.Control.SetVolume(fade / FadeDuration);
		SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * L_50 = V_1;
		NullCheck(L_50);
		DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04 * L_51 = L_50->get__display_8();
		NullCheck(L_51);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_52 = L_51->get__mediaPlayer_6();
		NullCheck(L_52);
		RuntimeObject* L_53;
		L_53 = VirtFuncInvoker0< RuntimeObject* >::Invoke(5 /* RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.MediaPlayer::get_Control() */, L_52);
		float L_54 = __this->get_U3CfadeU3E5__2_3();
		NullCheck(L_53);
		InterfaceActionInvoker1< float >::Invoke(29 /* System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetVolume(System.Single) */, IMediaControl_tB43619980AAA6A587A3E82A62068DA29D03CFC2C_il2cpp_TypeInfo_var, L_53, ((float)((float)L_54/(float)(0.25f))));
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(((int32_t)9));
		return (bool)1;
	}

IL_02a2:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_02a9:
	{
		// while (fade < FadeDuration && Application.isPlaying)
		float L_55 = __this->get_U3CfadeU3E5__2_3();
		if ((!(((float)L_55) < ((float)(0.25f)))))
		{
			goto IL_02c0;
		}
	}
	{
		bool L_56;
		L_56 = Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567(/*hidden argument*/NULL);
		if (L_56)
		{
			goto IL_0218;
		}
	}

IL_02c0:
	{
		// }
		return (bool)0;
	}
}
// System.Object RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadVideoWithFadingU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB51738CA40EBA5A1083400C83666804A6D24A214 (U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadVideoWithFadingU3Ed__23_System_Collections_IEnumerator_Reset_m44DA52D0E0CC3CD7D5F21583F357767AF45EBF94 (U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CLoadVideoWithFadingU3Ed__23_System_Collections_IEnumerator_Reset_m44DA52D0E0CC3CD7D5F21583F357767AF45EBF94_RuntimeMethod_var)));
	}
}
// System.Object RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadVideoWithFadingU3Ed__23_System_Collections_IEnumerator_get_Current_m6AEC2F56DE7688F3E09466C723AEBB3ABCA718FE (U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CResetSpeedTestCheckU3Ed__16__ctor_m3D053FE8D19035A8F7E29F2EEF8D7E1892B0B68F (U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CResetSpeedTestCheckU3Ed__16_System_IDisposable_Dispose_mCD152DEBBE8174D003FF33217C5BD072053482C8 (U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CResetSpeedTestCheckU3Ed__16_MoveNext_m2ADEAD718840B9B8FF62BDE9B86A6654B93D2770 (U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5FB35B2106DEBEFCD4761E77F85FD7B04718828B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBE583588F94930CFEE0452285EBE5C048F7DDF5B);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_0053;
			}
			case 2:
			{
				goto IL_0098;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		__this->set_U3CU3E1__state_0((-1));
		// Debug.Log("SPEED TEST START");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteralBE583588F94930CFEE0452285EBE5C048F7DDF5B, /*hidden argument*/NULL);
		// isAutoSpeedCheck = true;
		SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_isAutoSpeedCheck_11((bool)1);
		// yield return new WaitForSeconds(0.1f);
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_4 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_4, (0.100000001f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_4);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_U3CU3E1__state_0((-1));
		// WaitForSeconds waitTime = new WaitForSeconds(TimerAutoCheck);
		SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5 * L_5 = V_1;
		NullCheck(L_5);
		float L_6 = L_5->get_TimerAutoCheck_9();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_7 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_7, L_6, /*hidden argument*/NULL);
		__this->set_U3CwaitTimeU3E5__2_3(L_7);
	}

IL_006b:
	{
		// if (isSpeedCheck == false)
		SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5 * L_8 = V_1;
		NullCheck(L_8);
		bool L_9 = L_8->get_isSpeedCheck_10();
		if (L_9)
		{
			goto IL_0083;
		}
	}
	{
		// Debug.Log("Testing The Speed.");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral5FB35B2106DEBEFCD4761E77F85FD7B04718828B, /*hidden argument*/NULL);
		// OnClickSpeedTestBtn();
		SpeedCheck_t250CD01D9029D93FB35BCC9E954A4506604C34B5 * L_10 = V_1;
		NullCheck(L_10);
		SpeedCheck_OnClickSpeedTestBtn_m3F6CB2EB19F3FD247BA9292D5B0CD2EA9893EAAD(L_10, /*hidden argument*/NULL);
	}

IL_0083:
	{
		// yield return waitTime; // Wait till we get speed results.
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_11 = __this->get_U3CwaitTimeU3E5__2_3();
		__this->set_U3CU3E2__current_1(L_11);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0098:
	{
		__this->set_U3CU3E1__state_0((-1));
		// while (true)
		goto IL_006b;
	}
}
// System.Object Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CResetSpeedTestCheckU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD32DC79D428A42EE1AFBAD15A32C1E8A132EC22 (U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CResetSpeedTestCheckU3Ed__16_System_Collections_IEnumerator_Reset_m4DF85D13FFF1B60EFF83963ECAA4A5728B292A01 (U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CResetSpeedTestCheckU3Ed__16_System_Collections_IEnumerator_Reset_m4DF85D13FFF1B60EFF83963ECAA4A5728B292A01_RuntimeMethod_var)));
	}
}
// System.Object Crosstales.OnlineCheck.Demo.SpeedCheck/<ResetSpeedTestCheck>d__16::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CResetSpeedTestCheckU3Ed__16_System_Collections_IEnumerator_get_Current_m9A98DBB9701B673D1CEE0E764B117C6A24593E87 (U3CResetSpeedTestCheckU3Ed__16_t33B4B2E36184EAD420AD836C4828EB8D56262817 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: RenderHeads.Media.AVProVideo.Stream/Chunk
IL2CPP_EXTERN_C void Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshal_pinvoke(const Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996& unmarshaled, Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
}
IL2CPP_EXTERN_C void Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshal_pinvoke_back(const Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshaled_pinvoke& marshaled, Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
}
// Conversion method for clean up from marshalling of: RenderHeads.Media.AVProVideo.Stream/Chunk
IL2CPP_EXTERN_C void Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshal_pinvoke_cleanup(Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// Conversion methods for marshalling of: RenderHeads.Media.AVProVideo.Stream/Chunk
IL2CPP_EXTERN_C void Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshal_com(const Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996& unmarshaled, Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
}
IL2CPP_EXTERN_C void Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshal_com_back(const Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshaled_com& marshaled, Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
}
// Conversion method for clean up from marshalling of: RenderHeads.Media.AVProVideo.Stream/Chunk
IL2CPP_EXTERN_C void Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshal_com_cleanup(Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VideoPlayerController/<StartVideoPlaying>d__13::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartVideoPlayingU3Ed__13__ctor_mA06BAE3A6CF5203156A73FD7AF9108A145AF0EC6 (U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void VideoPlayerController/<StartVideoPlaying>d__13::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartVideoPlayingU3Ed__13_System_IDisposable_Dispose_m8E449EF5BA253A939DCF11CAE39A4C8355DA6489 (U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean VideoPlayerController/<StartVideoPlaying>d__13::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CStartVideoPlayingU3Ed__13_MoveNext_m5012F6C3645CCBD3E50073D4D641AF494BD1A12B (U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0038;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// yield return new WaitForSeconds(playStartDelay);
		VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * L_4 = V_1;
		NullCheck(L_4);
		float L_5 = L_4->get_playStartDelay_13();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_6 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_6, L_5, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_6);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0038:
	{
		__this->set_U3CU3E1__state_0((-1));
		// mediaPlayerA.Play();
		VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * L_7 = V_1;
		NullCheck(L_7);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_8 = L_7->get_mediaPlayerA_4();
		NullCheck(L_8);
		MediaPlayer_Play_m893CBBB81F9F455B7AA764398A50A007D3C28F6C(L_8, /*hidden argument*/NULL);
		// mediaPlayerB.Play();
		VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * L_9 = V_1;
		NullCheck(L_9);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_10 = L_9->get_mediaPlayerB_5();
		NullCheck(L_10);
		MediaPlayer_Play_m893CBBB81F9F455B7AA764398A50A007D3C28F6C(L_10, /*hidden argument*/NULL);
		// mediaPlayerC.Play();
		VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * L_11 = V_1;
		NullCheck(L_11);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_12 = L_11->get_mediaPlayerC_6();
		NullCheck(L_12);
		MediaPlayer_Play_m893CBBB81F9F455B7AA764398A50A007D3C28F6C(L_12, /*hidden argument*/NULL);
		// mediaPlayerD.Play();
		VideoPlayerController_t2BCE890BD56D7BA6613703A5260E61CF98A11579 * L_13 = V_1;
		NullCheck(L_13);
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_14 = L_13->get_mediaPlayerD_7();
		NullCheck(L_14);
		MediaPlayer_Play_m893CBBB81F9F455B7AA764398A50A007D3C28F6C(L_14, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object VideoPlayerController/<StartVideoPlaying>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartVideoPlayingU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDAAAB33056322C26FB2E7185CA14F33A10CE42D0 (U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void VideoPlayerController/<StartVideoPlaying>d__13::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartVideoPlayingU3Ed__13_System_Collections_IEnumerator_Reset_m149BF3B02F1A023E95FAC7BBD2A5CF729F128F37 (U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CStartVideoPlayingU3Ed__13_System_Collections_IEnumerator_Reset_m149BF3B02F1A023E95FAC7BBD2A5CF729F128F37_RuntimeMethod_var)));
	}
}
// System.Object VideoPlayerController/<StartVideoPlaying>d__13::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartVideoPlayingU3Ed__13_System_Collections_IEnumerator_get_Current_mEC51760B8923B9C8CC3193FD8D8275B4A5E8FC9B (U3CStartVideoPlayingU3Ed__13_tCAF489476BED9288792A919EDC65EEC9D9AFA723 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader
IL2CPP_EXTERN_C void HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED_marshal_pinvoke(const HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED& unmarshaled, HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED_marshaled_pinvoke& marshaled)
{
	marshaled.___header_0 = il2cpp_codegen_marshal_string(unmarshaled.get_header_0());
	marshaled.___value_1 = il2cpp_codegen_marshal_string(unmarshaled.get_value_1());
}
IL2CPP_EXTERN_C void HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED_marshal_pinvoke_back(const HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED_marshaled_pinvoke& marshaled, HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED& unmarshaled)
{
	unmarshaled.set_header_0(il2cpp_codegen_marshal_string_result(marshaled.___header_0));
	unmarshaled.set_value_1(il2cpp_codegen_marshal_string_result(marshaled.___value_1));
}
// Conversion method for clean up from marshalling of: RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader
IL2CPP_EXTERN_C void HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED_marshal_pinvoke_cleanup(HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___header_0);
	marshaled.___header_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___value_1);
	marshaled.___value_1 = NULL;
}
// Conversion methods for marshalling of: RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader
IL2CPP_EXTERN_C void HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED_marshal_com(const HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED& unmarshaled, HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED_marshaled_com& marshaled)
{
	marshaled.___header_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_header_0());
	marshaled.___value_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_value_1());
}
IL2CPP_EXTERN_C void HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED_marshal_com_back(const HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED_marshaled_com& marshaled, HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED& unmarshaled)
{
	unmarshaled.set_header_0(il2cpp_codegen_marshal_bstring_result(marshaled.___header_0));
	unmarshaled.set_value_1(il2cpp_codegen_marshal_bstring_result(marshaled.___value_1));
}
// Conversion method for clean up from marshalling of: RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader
IL2CPP_EXTERN_C void HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED_marshal_com_cleanup(HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___header_0);
	marshaled.___header_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___value_1);
	marshaled.___value_1 = NULL;
}
// System.Void RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HTTPHeader__ctor_mBD26E64BA6EB557D29C8E5E346802BD3BF6E199E (HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED * __this, String_t* ___header0, String_t* ___value1, const RuntimeMethod* method)
{
	{
		// public HTTPHeader(string header, string value) { this.header = header; this.value = value; }
		String_t* L_0 = ___header0;
		__this->set_header_0(L_0);
		// public HTTPHeader(string header, string value) { this.header = header; this.value = value; }
		String_t* L_1 = ___value1;
		__this->set_value_1(L_1);
		// public HTTPHeader(string header, string value) { this.header = header; this.value = value; }
		return;
	}
}
IL2CPP_EXTERN_C  void HTTPHeader__ctor_mBD26E64BA6EB557D29C8E5E346802BD3BF6E199E_AdjustorThunk (RuntimeObject * __this, String_t* ___header0, String_t* ___value1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED * _thisAdjusted = reinterpret_cast<HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED *>(__this + _offset);
	HTTPHeader__ctor_mBD26E64BA6EB557D29C8E5E346802BD3BF6E199E(_thisAdjusted, ___header0, ___value1, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * VideoVCR_get_PlayingPlayer_mB15973E86859AA8670C277023A774B458A5D3E8B_inline (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method)
{
	{
		// return _mediaPlayer;
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_0 = __this->get__mediaPlayer_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * VideoVCR_get_LoadingPlayer_m8B3CEF700E0B37E6D1287D8F37E9264BF9C1B67E_inline (VideoVCR_tC1985705A7B606FB20B00278F251B63A09C594EA * __this, const RuntimeMethod* method)
{
	{
		// return _mediaPlayer;
		MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * L_0 = __this->get__mediaPlayer_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Image_get_type_m730305AA6DAA0AF5C57A8AD2C1B8A97E6B0B8229_inline (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * __this, const RuntimeMethod* method)
{
	{
		// public Type type { get { return m_Type; } set { if (SetPropertyUtility.SetStruct(ref m_Type, value)) SetVerticesDirty(); } }
		int32_t L_0 = __this->get_m_Type_39();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool PlatformOptions_IsModified_m9329B32428B8862D0994558AF2B7C086A9DA8651_inline (PlatformOptions_tF31F6A79334BB550DBDF7604694280762695F59F * __this, const RuntimeMethod* method)
{
	{
		// return overridePath;     // The other variables don't matter if overridePath is false
		bool L_0 = __this->get_overridePath_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_stringLength_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mE8E665D4EF3BB8BAF09A305618E88C1DC1863ED4_gshared_inline (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  List_1_get_Item_m1FFDEB8822EC7E87A341E3E5A72932508F420C74_gshared_inline (List_1_tDDDB73BF208FFF7C9FE45EA1579BAE42D9C62452 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		HTTPHeaderU5BU5D_tDC8254FC7749720A2ED8557C46398F574DFAA8B0* L_2 = (HTTPHeaderU5BU5D_tDC8254FC7749720A2ED8557C46398F574DFAA8B0*)__this->get__items_1();
		int32_t L_3 = ___index0;
		HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED  L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((HTTPHeaderU5BU5D_tDC8254FC7749720A2ED8557C46398F574DFAA8B0*)L_2, (int32_t)L_3);
		return (HTTPHeader_tE4AEE44AA2E844D3B170C43DCC5F88AD10B710ED )L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
