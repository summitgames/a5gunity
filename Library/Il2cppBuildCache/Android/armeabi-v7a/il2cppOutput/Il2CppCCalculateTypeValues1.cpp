﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.DisplayUGUI>
struct List_1_tF5D950E434FC24D751780F604224065C6FC2169B;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// System.Collections.Generic.Queue`1<System.String>
struct Queue_1_tD2C03A5990B5958D85846D872A22AA67F3E8F97D;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// RenderHeads.Media.AVProVideo.MediaPlayer[]
struct MediaPlayerU5BU5D_t6835D5F6A0A307C2C9869C54C35E7ACC7A883224;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// RenderHeads.Media.AVProVideo.ApplyToMesh
struct ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2;
// RenderHeads.Media.AVProVideo.DisplayIMGUI
struct DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04;
// RenderHeads.Media.AVProVideo.DisplayUGUI
struct DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A;
// UnityEngine.GUISkin
struct GUISkin_tE353D65D4618423B574BAD31F5C5AC1B967E32C6;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// RenderHeads.Media.AVProVideo.MediaPlayer
struct MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.RenderTexture
struct RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849;
// RenderHeads.Media.AVProVideo.Demos.SimpleController
struct SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65;
// UnityEngine.UI.Slider
struct Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.UI.Toggle
struct Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// RenderHeads.Media.AVProVideo.Stream
struct Stream_t0213C8B2018B430058C9AE8677468C4A07E73ACF  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23
struct U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9  : public RuntimeObject
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// RenderHeads.Media.AVProVideo.Demos.SimpleController RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::<>4__this
	SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * ___U3CU3E4__this_2;
	// System.Single RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::<fade>5__2
	float ___U3CfadeU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9, ___U3CU3E4__this_2)); }
	inline SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfadeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9, ___U3CfadeU3E5__2_3)); }
	inline float get_U3CfadeU3E5__2_3() const { return ___U3CfadeU3E5__2_3; }
	inline float* get_address_of_U3CfadeU3E5__2_3() { return &___U3CfadeU3E5__2_3; }
	inline void set_U3CfadeU3E5__2_3(float value)
	{
		___U3CfadeU3E5__2_3 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=144
struct __StaticArrayInitTypeSizeU3D144_t4B2822F6C46C674B47DD7F528A50B6B894D896D4 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D144_t4B2822F6C46C674B47DD7F528A50B6B894D896D4__padding[144];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24
struct __StaticArrayInitTypeSizeU3D24_t2F23740D8943FC7C06AD3DD80B71D65744F140F2 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t2F23740D8943FC7C06AD3DD80B71D65744F140F2__padding[24];
	};

public:
};


// RenderHeads.Media.AVProVideo.Stream/Chunk
struct Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996 
{
public:
	// System.String RenderHeads.Media.AVProVideo.Stream/Chunk::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of RenderHeads.Media.AVProVideo.Stream/Chunk
struct Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshaled_pinvoke
{
	char* ___name_0;
};
// Native definition for COM marshalling of RenderHeads.Media.AVProVideo.Stream/Chunk
struct Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshaled_com
{
	Il2CppChar* ___name_0;
};

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=144 <PrivateImplementationDetails>::15E4544087AC886E83EA2C6F84406B56B09370208D305B0ED3C2A2CAE4BDF76B
	__StaticArrayInitTypeSizeU3D144_t4B2822F6C46C674B47DD7F528A50B6B894D896D4  ___15E4544087AC886E83EA2C6F84406B56B09370208D305B0ED3C2A2CAE4BDF76B_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1
	__StaticArrayInitTypeSizeU3D24_t2F23740D8943FC7C06AD3DD80B71D65744F140F2  ___753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_1;

public:
	inline static int32_t get_offset_of_U315E4544087AC886E83EA2C6F84406B56B09370208D305B0ED3C2A2CAE4BDF76B_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_StaticFields, ___15E4544087AC886E83EA2C6F84406B56B09370208D305B0ED3C2A2CAE4BDF76B_0)); }
	inline __StaticArrayInitTypeSizeU3D144_t4B2822F6C46C674B47DD7F528A50B6B894D896D4  get_U315E4544087AC886E83EA2C6F84406B56B09370208D305B0ED3C2A2CAE4BDF76B_0() const { return ___15E4544087AC886E83EA2C6F84406B56B09370208D305B0ED3C2A2CAE4BDF76B_0; }
	inline __StaticArrayInitTypeSizeU3D144_t4B2822F6C46C674B47DD7F528A50B6B894D896D4 * get_address_of_U315E4544087AC886E83EA2C6F84406B56B09370208D305B0ED3C2A2CAE4BDF76B_0() { return &___15E4544087AC886E83EA2C6F84406B56B09370208D305B0ED3C2A2CAE4BDF76B_0; }
	inline void set_U315E4544087AC886E83EA2C6F84406B56B09370208D305B0ED3C2A2CAE4BDF76B_0(__StaticArrayInitTypeSizeU3D144_t4B2822F6C46C674B47DD7F528A50B6B894D896D4  value)
	{
		___15E4544087AC886E83EA2C6F84406B56B09370208D305B0ED3C2A2CAE4BDF76B_0 = value;
	}

	inline static int32_t get_offset_of_U3753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_StaticFields, ___753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_1)); }
	inline __StaticArrayInitTypeSizeU3D24_t2F23740D8943FC7C06AD3DD80B71D65744F140F2  get_U3753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_1() const { return ___753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_1; }
	inline __StaticArrayInitTypeSizeU3D24_t2F23740D8943FC7C06AD3DD80B71D65744F140F2 * get_address_of_U3753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_1() { return &___753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_1; }
	inline void set_U3753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_1(__StaticArrayInitTypeSizeU3D24_t2F23740D8943FC7C06AD3DD80B71D65744F140F2  value)
	{
		___753D5E1ADA77B20B9959A1030B8E0BA5CF925F2881D3635C3F791E5A0AE0EEB1_1 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation
struct FileLocation_tF1B24F1135C98C08748305C198F011AE743E3848 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileLocation_tF1B24F1135C98C08748305C198F011AE743E3848, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.Demos.PlaybackSync/State
struct State_t7FFEAADD0FEA4C45402967A4CC39F4A810EE3F18 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.PlaybackSync/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t7FFEAADD0FEA4C45402967A4CC39F4A810EE3F18, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// RenderHeads.Media.AVProVideo.Demos.AutoRotate
struct AutoRotate_tD3FCE4EA299571DD8C213BB0EACF8FC27AFCD314  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single RenderHeads.Media.AVProVideo.Demos.AutoRotate::x
	float ___x_4;
	// System.Single RenderHeads.Media.AVProVideo.Demos.AutoRotate::y
	float ___y_5;
	// System.Single RenderHeads.Media.AVProVideo.Demos.AutoRotate::z
	float ___z_6;
	// System.Single RenderHeads.Media.AVProVideo.Demos.AutoRotate::_timer
	float ____timer_7;

public:
	inline static int32_t get_offset_of_x_4() { return static_cast<int32_t>(offsetof(AutoRotate_tD3FCE4EA299571DD8C213BB0EACF8FC27AFCD314, ___x_4)); }
	inline float get_x_4() const { return ___x_4; }
	inline float* get_address_of_x_4() { return &___x_4; }
	inline void set_x_4(float value)
	{
		___x_4 = value;
	}

	inline static int32_t get_offset_of_y_5() { return static_cast<int32_t>(offsetof(AutoRotate_tD3FCE4EA299571DD8C213BB0EACF8FC27AFCD314, ___y_5)); }
	inline float get_y_5() const { return ___y_5; }
	inline float* get_address_of_y_5() { return &___y_5; }
	inline void set_y_5(float value)
	{
		___y_5 = value;
	}

	inline static int32_t get_offset_of_z_6() { return static_cast<int32_t>(offsetof(AutoRotate_tD3FCE4EA299571DD8C213BB0EACF8FC27AFCD314, ___z_6)); }
	inline float get_z_6() const { return ___z_6; }
	inline float* get_address_of_z_6() { return &___z_6; }
	inline void set_z_6(float value)
	{
		___z_6 = value;
	}

	inline static int32_t get_offset_of__timer_7() { return static_cast<int32_t>(offsetof(AutoRotate_tD3FCE4EA299571DD8C213BB0EACF8FC27AFCD314, ____timer_7)); }
	inline float get__timer_7() const { return ____timer_7; }
	inline float* get_address_of__timer_7() { return &____timer_7; }
	inline void set__timer_7(float value)
	{
		____timer_7 = value;
	}
};


// RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack
struct ChangeAudioTrack_t554262C74032CB71DB111B0A48644FAD68A43402  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack::_mediaPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mediaPlayer_4;
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack::_trackIndex
	int32_t ____trackIndex_5;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack::_isQueued
	bool ____isQueued_6;

public:
	inline static int32_t get_offset_of__mediaPlayer_4() { return static_cast<int32_t>(offsetof(ChangeAudioTrack_t554262C74032CB71DB111B0A48644FAD68A43402, ____mediaPlayer_4)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mediaPlayer_4() const { return ____mediaPlayer_4; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mediaPlayer_4() { return &____mediaPlayer_4; }
	inline void set__mediaPlayer_4(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mediaPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of__trackIndex_5() { return static_cast<int32_t>(offsetof(ChangeAudioTrack_t554262C74032CB71DB111B0A48644FAD68A43402, ____trackIndex_5)); }
	inline int32_t get__trackIndex_5() const { return ____trackIndex_5; }
	inline int32_t* get_address_of__trackIndex_5() { return &____trackIndex_5; }
	inline void set__trackIndex_5(int32_t value)
	{
		____trackIndex_5 = value;
	}

	inline static int32_t get_offset_of__isQueued_6() { return static_cast<int32_t>(offsetof(ChangeAudioTrack_t554262C74032CB71DB111B0A48644FAD68A43402, ____isQueued_6)); }
	inline bool get__isQueued_6() const { return ____isQueued_6; }
	inline bool* get_address_of__isQueued_6() { return &____isQueued_6; }
	inline void set__isQueued_6(bool value)
	{
		____isQueued_6 = value;
	}
};


// RenderHeads.Media.AVProVideo.Demos.ChangeStereoMode
struct ChangeStereoMode_t616C6D3D4660B71D39C27071D35157877D33B136  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.ChangeStereoMode::_mediaPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mediaPlayer_4;
	// RenderHeads.Media.AVProVideo.ApplyToMesh RenderHeads.Media.AVProVideo.Demos.ChangeStereoMode::_applyToMesh
	ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2 * ____applyToMesh_5;

public:
	inline static int32_t get_offset_of__mediaPlayer_4() { return static_cast<int32_t>(offsetof(ChangeStereoMode_t616C6D3D4660B71D39C27071D35157877D33B136, ____mediaPlayer_4)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mediaPlayer_4() const { return ____mediaPlayer_4; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mediaPlayer_4() { return &____mediaPlayer_4; }
	inline void set__mediaPlayer_4(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mediaPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of__applyToMesh_5() { return static_cast<int32_t>(offsetof(ChangeStereoMode_t616C6D3D4660B71D39C27071D35157877D33B136, ____applyToMesh_5)); }
	inline ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2 * get__applyToMesh_5() const { return ____applyToMesh_5; }
	inline ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2 ** get_address_of__applyToMesh_5() { return &____applyToMesh_5; }
	inline void set__applyToMesh_5(ApplyToMesh_tEEFEDAD31A415D7E1966ED4B459301C83AB022A2 * value)
	{
		____applyToMesh_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____applyToMesh_5), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.ChangeVideoExample
struct ChangeVideoExample_t7A4744ABFD531F36394852968842D3CD8D832B23  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.ChangeVideoExample::_mediaPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mediaPlayer_4;

public:
	inline static int32_t get_offset_of__mediaPlayer_4() { return static_cast<int32_t>(offsetof(ChangeVideoExample_t7A4744ABFD531F36394852968842D3CD8D832B23, ____mediaPlayer_4)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mediaPlayer_4() const { return ____mediaPlayer_4; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mediaPlayer_4() { return &____mediaPlayer_4; }
	inline void set__mediaPlayer_4(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mediaPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_4), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.CustomVCR
struct CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.CustomVCR::_mediaPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mediaPlayer_4;
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.CustomVCR::_mediaPlayerB
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mediaPlayerB_5;
	// RenderHeads.Media.AVProVideo.DisplayUGUI RenderHeads.Media.AVProVideo.Demos.CustomVCR::_mediaDisplay
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * ____mediaDisplay_6;
	// UnityEngine.RectTransform RenderHeads.Media.AVProVideo.Demos.CustomVCR::_bufferedSliderRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ____bufferedSliderRect_7;
	// UnityEngine.UI.Slider RenderHeads.Media.AVProVideo.Demos.CustomVCR::_videoSeekSlider
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * ____videoSeekSlider_8;
	// System.Single RenderHeads.Media.AVProVideo.Demos.CustomVCR::_setVideoSeekSliderValue
	float ____setVideoSeekSliderValue_9;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.CustomVCR::_wasPlayingOnScrub
	bool ____wasPlayingOnScrub_10;
	// UnityEngine.UI.Slider RenderHeads.Media.AVProVideo.Demos.CustomVCR::_audioVolumeSlider
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * ____audioVolumeSlider_11;
	// System.Single RenderHeads.Media.AVProVideo.Demos.CustomVCR::_setAudioVolumeSliderValue
	float ____setAudioVolumeSliderValue_12;
	// UnityEngine.UI.Toggle RenderHeads.Media.AVProVideo.Demos.CustomVCR::_MuteToggle
	Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * ____MuteToggle_13;
	// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation RenderHeads.Media.AVProVideo.Demos.CustomVCR::_location
	int32_t ____location_14;
	// System.String[] RenderHeads.Media.AVProVideo.Demos.CustomVCR::_videoURL
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____videoURL_15;
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.CustomVCR::_VideoIndex
	int32_t ____VideoIndex_16;
	// UnityEngine.UI.Image RenderHeads.Media.AVProVideo.Demos.CustomVCR::_bufferedSliderImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ____bufferedSliderImage_17;
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.CustomVCR::_loadingPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____loadingPlayer_18;

public:
	inline static int32_t get_offset_of__mediaPlayer_4() { return static_cast<int32_t>(offsetof(CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A, ____mediaPlayer_4)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mediaPlayer_4() const { return ____mediaPlayer_4; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mediaPlayer_4() { return &____mediaPlayer_4; }
	inline void set__mediaPlayer_4(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mediaPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of__mediaPlayerB_5() { return static_cast<int32_t>(offsetof(CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A, ____mediaPlayerB_5)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mediaPlayerB_5() const { return ____mediaPlayerB_5; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mediaPlayerB_5() { return &____mediaPlayerB_5; }
	inline void set__mediaPlayerB_5(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mediaPlayerB_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayerB_5), (void*)value);
	}

	inline static int32_t get_offset_of__mediaDisplay_6() { return static_cast<int32_t>(offsetof(CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A, ____mediaDisplay_6)); }
	inline DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * get__mediaDisplay_6() const { return ____mediaDisplay_6; }
	inline DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A ** get_address_of__mediaDisplay_6() { return &____mediaDisplay_6; }
	inline void set__mediaDisplay_6(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * value)
	{
		____mediaDisplay_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaDisplay_6), (void*)value);
	}

	inline static int32_t get_offset_of__bufferedSliderRect_7() { return static_cast<int32_t>(offsetof(CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A, ____bufferedSliderRect_7)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get__bufferedSliderRect_7() const { return ____bufferedSliderRect_7; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of__bufferedSliderRect_7() { return &____bufferedSliderRect_7; }
	inline void set__bufferedSliderRect_7(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		____bufferedSliderRect_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bufferedSliderRect_7), (void*)value);
	}

	inline static int32_t get_offset_of__videoSeekSlider_8() { return static_cast<int32_t>(offsetof(CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A, ____videoSeekSlider_8)); }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * get__videoSeekSlider_8() const { return ____videoSeekSlider_8; }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A ** get_address_of__videoSeekSlider_8() { return &____videoSeekSlider_8; }
	inline void set__videoSeekSlider_8(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * value)
	{
		____videoSeekSlider_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____videoSeekSlider_8), (void*)value);
	}

	inline static int32_t get_offset_of__setVideoSeekSliderValue_9() { return static_cast<int32_t>(offsetof(CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A, ____setVideoSeekSliderValue_9)); }
	inline float get__setVideoSeekSliderValue_9() const { return ____setVideoSeekSliderValue_9; }
	inline float* get_address_of__setVideoSeekSliderValue_9() { return &____setVideoSeekSliderValue_9; }
	inline void set__setVideoSeekSliderValue_9(float value)
	{
		____setVideoSeekSliderValue_9 = value;
	}

	inline static int32_t get_offset_of__wasPlayingOnScrub_10() { return static_cast<int32_t>(offsetof(CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A, ____wasPlayingOnScrub_10)); }
	inline bool get__wasPlayingOnScrub_10() const { return ____wasPlayingOnScrub_10; }
	inline bool* get_address_of__wasPlayingOnScrub_10() { return &____wasPlayingOnScrub_10; }
	inline void set__wasPlayingOnScrub_10(bool value)
	{
		____wasPlayingOnScrub_10 = value;
	}

	inline static int32_t get_offset_of__audioVolumeSlider_11() { return static_cast<int32_t>(offsetof(CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A, ____audioVolumeSlider_11)); }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * get__audioVolumeSlider_11() const { return ____audioVolumeSlider_11; }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A ** get_address_of__audioVolumeSlider_11() { return &____audioVolumeSlider_11; }
	inline void set__audioVolumeSlider_11(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * value)
	{
		____audioVolumeSlider_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____audioVolumeSlider_11), (void*)value);
	}

	inline static int32_t get_offset_of__setAudioVolumeSliderValue_12() { return static_cast<int32_t>(offsetof(CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A, ____setAudioVolumeSliderValue_12)); }
	inline float get__setAudioVolumeSliderValue_12() const { return ____setAudioVolumeSliderValue_12; }
	inline float* get_address_of__setAudioVolumeSliderValue_12() { return &____setAudioVolumeSliderValue_12; }
	inline void set__setAudioVolumeSliderValue_12(float value)
	{
		____setAudioVolumeSliderValue_12 = value;
	}

	inline static int32_t get_offset_of__MuteToggle_13() { return static_cast<int32_t>(offsetof(CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A, ____MuteToggle_13)); }
	inline Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * get__MuteToggle_13() const { return ____MuteToggle_13; }
	inline Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E ** get_address_of__MuteToggle_13() { return &____MuteToggle_13; }
	inline void set__MuteToggle_13(Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * value)
	{
		____MuteToggle_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____MuteToggle_13), (void*)value);
	}

	inline static int32_t get_offset_of__location_14() { return static_cast<int32_t>(offsetof(CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A, ____location_14)); }
	inline int32_t get__location_14() const { return ____location_14; }
	inline int32_t* get_address_of__location_14() { return &____location_14; }
	inline void set__location_14(int32_t value)
	{
		____location_14 = value;
	}

	inline static int32_t get_offset_of__videoURL_15() { return static_cast<int32_t>(offsetof(CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A, ____videoURL_15)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__videoURL_15() const { return ____videoURL_15; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__videoURL_15() { return &____videoURL_15; }
	inline void set__videoURL_15(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____videoURL_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____videoURL_15), (void*)value);
	}

	inline static int32_t get_offset_of__VideoIndex_16() { return static_cast<int32_t>(offsetof(CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A, ____VideoIndex_16)); }
	inline int32_t get__VideoIndex_16() const { return ____VideoIndex_16; }
	inline int32_t* get_address_of__VideoIndex_16() { return &____VideoIndex_16; }
	inline void set__VideoIndex_16(int32_t value)
	{
		____VideoIndex_16 = value;
	}

	inline static int32_t get_offset_of__bufferedSliderImage_17() { return static_cast<int32_t>(offsetof(CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A, ____bufferedSliderImage_17)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get__bufferedSliderImage_17() const { return ____bufferedSliderImage_17; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of__bufferedSliderImage_17() { return &____bufferedSliderImage_17; }
	inline void set__bufferedSliderImage_17(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		____bufferedSliderImage_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bufferedSliderImage_17), (void*)value);
	}

	inline static int32_t get_offset_of__loadingPlayer_18() { return static_cast<int32_t>(offsetof(CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A, ____loadingPlayer_18)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__loadingPlayer_18() const { return ____loadingPlayer_18; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__loadingPlayer_18() { return &____loadingPlayer_18; }
	inline void set__loadingPlayer_18(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____loadingPlayer_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____loadingPlayer_18), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.DemoInfo
struct DemoInfo_tFC8CE5653450E76B5095669CD562A80FFD6712B7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String RenderHeads.Media.AVProVideo.Demos.DemoInfo::_title
	String_t* ____title_4;
	// System.String RenderHeads.Media.AVProVideo.Demos.DemoInfo::_description
	String_t* ____description_5;
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.DemoInfo::_media
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____media_6;

public:
	inline static int32_t get_offset_of__title_4() { return static_cast<int32_t>(offsetof(DemoInfo_tFC8CE5653450E76B5095669CD562A80FFD6712B7, ____title_4)); }
	inline String_t* get__title_4() const { return ____title_4; }
	inline String_t** get_address_of__title_4() { return &____title_4; }
	inline void set__title_4(String_t* value)
	{
		____title_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____title_4), (void*)value);
	}

	inline static int32_t get_offset_of__description_5() { return static_cast<int32_t>(offsetof(DemoInfo_tFC8CE5653450E76B5095669CD562A80FFD6712B7, ____description_5)); }
	inline String_t* get__description_5() const { return ____description_5; }
	inline String_t** get_address_of__description_5() { return &____description_5; }
	inline void set__description_5(String_t* value)
	{
		____description_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____description_5), (void*)value);
	}

	inline static int32_t get_offset_of__media_6() { return static_cast<int32_t>(offsetof(DemoInfo_tFC8CE5653450E76B5095669CD562A80FFD6712B7, ____media_6)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__media_6() const { return ____media_6; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__media_6() { return &____media_6; }
	inline void set__media_6(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____media_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____media_6), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.FrameExtract
struct FrameExtract_t410E78EB4F551F51E516EB118F22B05F30EB95F3  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.FrameExtract::_mediaPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mediaPlayer_5;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.FrameExtract::_accurateSeek
	bool ____accurateSeek_6;
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.FrameExtract::_timeoutMs
	int32_t ____timeoutMs_7;
	// UnityEngine.GUISkin RenderHeads.Media.AVProVideo.Demos.FrameExtract::_skin
	GUISkin_tE353D65D4618423B574BAD31F5C5AC1B967E32C6 * ____skin_8;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.FrameExtract::_asyncExtract
	bool ____asyncExtract_9;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.FrameExtract::_busyProcessingFrame
	bool ____busyProcessingFrame_10;
	// System.Single RenderHeads.Media.AVProVideo.Demos.FrameExtract::_timeStepSeconds
	float ____timeStepSeconds_11;
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.FrameExtract::_frameIndex
	int32_t ____frameIndex_12;
	// UnityEngine.Texture2D RenderHeads.Media.AVProVideo.Demos.FrameExtract::_texture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ____texture_13;
	// UnityEngine.RenderTexture RenderHeads.Media.AVProVideo.Demos.FrameExtract::_displaySheet
	RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * ____displaySheet_14;

public:
	inline static int32_t get_offset_of__mediaPlayer_5() { return static_cast<int32_t>(offsetof(FrameExtract_t410E78EB4F551F51E516EB118F22B05F30EB95F3, ____mediaPlayer_5)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mediaPlayer_5() const { return ____mediaPlayer_5; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mediaPlayer_5() { return &____mediaPlayer_5; }
	inline void set__mediaPlayer_5(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mediaPlayer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_5), (void*)value);
	}

	inline static int32_t get_offset_of__accurateSeek_6() { return static_cast<int32_t>(offsetof(FrameExtract_t410E78EB4F551F51E516EB118F22B05F30EB95F3, ____accurateSeek_6)); }
	inline bool get__accurateSeek_6() const { return ____accurateSeek_6; }
	inline bool* get_address_of__accurateSeek_6() { return &____accurateSeek_6; }
	inline void set__accurateSeek_6(bool value)
	{
		____accurateSeek_6 = value;
	}

	inline static int32_t get_offset_of__timeoutMs_7() { return static_cast<int32_t>(offsetof(FrameExtract_t410E78EB4F551F51E516EB118F22B05F30EB95F3, ____timeoutMs_7)); }
	inline int32_t get__timeoutMs_7() const { return ____timeoutMs_7; }
	inline int32_t* get_address_of__timeoutMs_7() { return &____timeoutMs_7; }
	inline void set__timeoutMs_7(int32_t value)
	{
		____timeoutMs_7 = value;
	}

	inline static int32_t get_offset_of__skin_8() { return static_cast<int32_t>(offsetof(FrameExtract_t410E78EB4F551F51E516EB118F22B05F30EB95F3, ____skin_8)); }
	inline GUISkin_tE353D65D4618423B574BAD31F5C5AC1B967E32C6 * get__skin_8() const { return ____skin_8; }
	inline GUISkin_tE353D65D4618423B574BAD31F5C5AC1B967E32C6 ** get_address_of__skin_8() { return &____skin_8; }
	inline void set__skin_8(GUISkin_tE353D65D4618423B574BAD31F5C5AC1B967E32C6 * value)
	{
		____skin_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____skin_8), (void*)value);
	}

	inline static int32_t get_offset_of__asyncExtract_9() { return static_cast<int32_t>(offsetof(FrameExtract_t410E78EB4F551F51E516EB118F22B05F30EB95F3, ____asyncExtract_9)); }
	inline bool get__asyncExtract_9() const { return ____asyncExtract_9; }
	inline bool* get_address_of__asyncExtract_9() { return &____asyncExtract_9; }
	inline void set__asyncExtract_9(bool value)
	{
		____asyncExtract_9 = value;
	}

	inline static int32_t get_offset_of__busyProcessingFrame_10() { return static_cast<int32_t>(offsetof(FrameExtract_t410E78EB4F551F51E516EB118F22B05F30EB95F3, ____busyProcessingFrame_10)); }
	inline bool get__busyProcessingFrame_10() const { return ____busyProcessingFrame_10; }
	inline bool* get_address_of__busyProcessingFrame_10() { return &____busyProcessingFrame_10; }
	inline void set__busyProcessingFrame_10(bool value)
	{
		____busyProcessingFrame_10 = value;
	}

	inline static int32_t get_offset_of__timeStepSeconds_11() { return static_cast<int32_t>(offsetof(FrameExtract_t410E78EB4F551F51E516EB118F22B05F30EB95F3, ____timeStepSeconds_11)); }
	inline float get__timeStepSeconds_11() const { return ____timeStepSeconds_11; }
	inline float* get_address_of__timeStepSeconds_11() { return &____timeStepSeconds_11; }
	inline void set__timeStepSeconds_11(float value)
	{
		____timeStepSeconds_11 = value;
	}

	inline static int32_t get_offset_of__frameIndex_12() { return static_cast<int32_t>(offsetof(FrameExtract_t410E78EB4F551F51E516EB118F22B05F30EB95F3, ____frameIndex_12)); }
	inline int32_t get__frameIndex_12() const { return ____frameIndex_12; }
	inline int32_t* get_address_of__frameIndex_12() { return &____frameIndex_12; }
	inline void set__frameIndex_12(int32_t value)
	{
		____frameIndex_12 = value;
	}

	inline static int32_t get_offset_of__texture_13() { return static_cast<int32_t>(offsetof(FrameExtract_t410E78EB4F551F51E516EB118F22B05F30EB95F3, ____texture_13)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get__texture_13() const { return ____texture_13; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of__texture_13() { return &____texture_13; }
	inline void set__texture_13(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		____texture_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____texture_13), (void*)value);
	}

	inline static int32_t get_offset_of__displaySheet_14() { return static_cast<int32_t>(offsetof(FrameExtract_t410E78EB4F551F51E516EB118F22B05F30EB95F3, ____displaySheet_14)); }
	inline RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * get__displaySheet_14() const { return ____displaySheet_14; }
	inline RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** get_address_of__displaySheet_14() { return &____displaySheet_14; }
	inline void set__displaySheet_14(RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * value)
	{
		____displaySheet_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____displaySheet_14), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.LoadFromBuffer
struct LoadFromBuffer_t4FDF3E21E03A6270CA1D4DABA65CC603A8280189  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.LoadFromBuffer::_mp
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mp_4;
	// System.String RenderHeads.Media.AVProVideo.Demos.LoadFromBuffer::_filename
	String_t* ____filename_5;

public:
	inline static int32_t get_offset_of__mp_4() { return static_cast<int32_t>(offsetof(LoadFromBuffer_t4FDF3E21E03A6270CA1D4DABA65CC603A8280189, ____mp_4)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mp_4() const { return ____mp_4; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mp_4() { return &____mp_4; }
	inline void set__mp_4(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mp_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mp_4), (void*)value);
	}

	inline static int32_t get_offset_of__filename_5() { return static_cast<int32_t>(offsetof(LoadFromBuffer_t4FDF3E21E03A6270CA1D4DABA65CC603A8280189, ____filename_5)); }
	inline String_t* get__filename_5() const { return ____filename_5; }
	inline String_t** get_address_of__filename_5() { return &____filename_5; }
	inline void set__filename_5(String_t* value)
	{
		____filename_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____filename_5), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.LoadFromBufferInChunks
struct LoadFromBufferInChunks_tFFCF1D595509528875CCD737262722559BE45788  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.LoadFromBufferInChunks::_mp
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mp_4;
	// System.String RenderHeads.Media.AVProVideo.Demos.LoadFromBufferInChunks::_filename
	String_t* ____filename_5;

public:
	inline static int32_t get_offset_of__mp_4() { return static_cast<int32_t>(offsetof(LoadFromBufferInChunks_tFFCF1D595509528875CCD737262722559BE45788, ____mp_4)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mp_4() const { return ____mp_4; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mp_4() { return &____mp_4; }
	inline void set__mp_4(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mp_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mp_4), (void*)value);
	}

	inline static int32_t get_offset_of__filename_5() { return static_cast<int32_t>(offsetof(LoadFromBufferInChunks_tFFCF1D595509528875CCD737262722559BE45788, ____filename_5)); }
	inline String_t* get__filename_5() const { return ____filename_5; }
	inline String_t** get_address_of__filename_5() { return &____filename_5; }
	inline void set__filename_5(String_t* value)
	{
		____filename_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____filename_5), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.Mapping3D
struct Mapping3D_tD245FFB24F152207D7C99D9AAB180568116C5D27  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject RenderHeads.Media.AVProVideo.Demos.Mapping3D::_cubePrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____cubePrefab_4;
	// System.Single RenderHeads.Media.AVProVideo.Demos.Mapping3D::_timer
	float ____timer_7;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> RenderHeads.Media.AVProVideo.Demos.Mapping3D::_cubes
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ____cubes_8;

public:
	inline static int32_t get_offset_of__cubePrefab_4() { return static_cast<int32_t>(offsetof(Mapping3D_tD245FFB24F152207D7C99D9AAB180568116C5D27, ____cubePrefab_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__cubePrefab_4() const { return ____cubePrefab_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__cubePrefab_4() { return &____cubePrefab_4; }
	inline void set__cubePrefab_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____cubePrefab_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cubePrefab_4), (void*)value);
	}

	inline static int32_t get_offset_of__timer_7() { return static_cast<int32_t>(offsetof(Mapping3D_tD245FFB24F152207D7C99D9AAB180568116C5D27, ____timer_7)); }
	inline float get__timer_7() const { return ____timer_7; }
	inline float* get_address_of__timer_7() { return &____timer_7; }
	inline void set__timer_7(float value)
	{
		____timer_7 = value;
	}

	inline static int32_t get_offset_of__cubes_8() { return static_cast<int32_t>(offsetof(Mapping3D_tD245FFB24F152207D7C99D9AAB180568116C5D27, ____cubes_8)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get__cubes_8() const { return ____cubes_8; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of__cubes_8() { return &____cubes_8; }
	inline void set__cubes_8(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		____cubes_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cubes_8), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.NativeMediaOpen
struct NativeMediaOpen_t97AA3DE898F9247ECB3A886B6A10BD345FA2D19C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.NativeMediaOpen::_player
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____player_4;

public:
	inline static int32_t get_offset_of__player_4() { return static_cast<int32_t>(offsetof(NativeMediaOpen_t97AA3DE898F9247ECB3A886B6A10BD345FA2D19C, ____player_4)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__player_4() const { return ____player_4; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__player_4() { return &____player_4; }
	inline void set__player_4(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____player_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____player_4), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.PlaybackSync
struct PlaybackSync_t81C52C0F1A279BD434628CA123D1D4D31C40F3B2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.PlaybackSync::_masterPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____masterPlayer_4;
	// RenderHeads.Media.AVProVideo.MediaPlayer[] RenderHeads.Media.AVProVideo.Demos.PlaybackSync::_slavePlayers
	MediaPlayerU5BU5D_t6835D5F6A0A307C2C9869C54C35E7ACC7A883224* ____slavePlayers_5;
	// System.Single RenderHeads.Media.AVProVideo.Demos.PlaybackSync::_toleranceMs
	float ____toleranceMs_6;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.PlaybackSync::_matchVideo
	bool ____matchVideo_7;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.PlaybackSync::_muteSlaves
	bool ____muteSlaves_8;
	// RenderHeads.Media.AVProVideo.Demos.PlaybackSync/State RenderHeads.Media.AVProVideo.Demos.PlaybackSync::_state
	int32_t ____state_9;

public:
	inline static int32_t get_offset_of__masterPlayer_4() { return static_cast<int32_t>(offsetof(PlaybackSync_t81C52C0F1A279BD434628CA123D1D4D31C40F3B2, ____masterPlayer_4)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__masterPlayer_4() const { return ____masterPlayer_4; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__masterPlayer_4() { return &____masterPlayer_4; }
	inline void set__masterPlayer_4(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____masterPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____masterPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of__slavePlayers_5() { return static_cast<int32_t>(offsetof(PlaybackSync_t81C52C0F1A279BD434628CA123D1D4D31C40F3B2, ____slavePlayers_5)); }
	inline MediaPlayerU5BU5D_t6835D5F6A0A307C2C9869C54C35E7ACC7A883224* get__slavePlayers_5() const { return ____slavePlayers_5; }
	inline MediaPlayerU5BU5D_t6835D5F6A0A307C2C9869C54C35E7ACC7A883224** get_address_of__slavePlayers_5() { return &____slavePlayers_5; }
	inline void set__slavePlayers_5(MediaPlayerU5BU5D_t6835D5F6A0A307C2C9869C54C35E7ACC7A883224* value)
	{
		____slavePlayers_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____slavePlayers_5), (void*)value);
	}

	inline static int32_t get_offset_of__toleranceMs_6() { return static_cast<int32_t>(offsetof(PlaybackSync_t81C52C0F1A279BD434628CA123D1D4D31C40F3B2, ____toleranceMs_6)); }
	inline float get__toleranceMs_6() const { return ____toleranceMs_6; }
	inline float* get_address_of__toleranceMs_6() { return &____toleranceMs_6; }
	inline void set__toleranceMs_6(float value)
	{
		____toleranceMs_6 = value;
	}

	inline static int32_t get_offset_of__matchVideo_7() { return static_cast<int32_t>(offsetof(PlaybackSync_t81C52C0F1A279BD434628CA123D1D4D31C40F3B2, ____matchVideo_7)); }
	inline bool get__matchVideo_7() const { return ____matchVideo_7; }
	inline bool* get_address_of__matchVideo_7() { return &____matchVideo_7; }
	inline void set__matchVideo_7(bool value)
	{
		____matchVideo_7 = value;
	}

	inline static int32_t get_offset_of__muteSlaves_8() { return static_cast<int32_t>(offsetof(PlaybackSync_t81C52C0F1A279BD434628CA123D1D4D31C40F3B2, ____muteSlaves_8)); }
	inline bool get__muteSlaves_8() const { return ____muteSlaves_8; }
	inline bool* get_address_of__muteSlaves_8() { return &____muteSlaves_8; }
	inline void set__muteSlaves_8(bool value)
	{
		____muteSlaves_8 = value;
	}

	inline static int32_t get_offset_of__state_9() { return static_cast<int32_t>(offsetof(PlaybackSync_t81C52C0F1A279BD434628CA123D1D4D31C40F3B2, ____state_9)); }
	inline int32_t get__state_9() const { return ____state_9; }
	inline int32_t* get_address_of__state_9() { return &____state_9; }
	inline void set__state_9(int32_t value)
	{
		____state_9 = value;
	}
};


// RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple
struct SampleApp_Multiple_tEC8B83D898EFB18F16A1D2A8A5BA3D26A8141F4A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::m_videoPath
	String_t* ___m_videoPath_4;
	// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::m_videoLocation
	int32_t ___m_videoLocation_5;
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::m_NumVideosAdded
	int32_t ___m_NumVideosAdded_6;
	// System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.DisplayUGUI> RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::m_aAddedVideos
	List_1_tF5D950E434FC24D751780F604224065C6FC2169B * ___m_aAddedVideos_7;

public:
	inline static int32_t get_offset_of_m_videoPath_4() { return static_cast<int32_t>(offsetof(SampleApp_Multiple_tEC8B83D898EFB18F16A1D2A8A5BA3D26A8141F4A, ___m_videoPath_4)); }
	inline String_t* get_m_videoPath_4() const { return ___m_videoPath_4; }
	inline String_t** get_address_of_m_videoPath_4() { return &___m_videoPath_4; }
	inline void set_m_videoPath_4(String_t* value)
	{
		___m_videoPath_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_videoPath_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_videoLocation_5() { return static_cast<int32_t>(offsetof(SampleApp_Multiple_tEC8B83D898EFB18F16A1D2A8A5BA3D26A8141F4A, ___m_videoLocation_5)); }
	inline int32_t get_m_videoLocation_5() const { return ___m_videoLocation_5; }
	inline int32_t* get_address_of_m_videoLocation_5() { return &___m_videoLocation_5; }
	inline void set_m_videoLocation_5(int32_t value)
	{
		___m_videoLocation_5 = value;
	}

	inline static int32_t get_offset_of_m_NumVideosAdded_6() { return static_cast<int32_t>(offsetof(SampleApp_Multiple_tEC8B83D898EFB18F16A1D2A8A5BA3D26A8141F4A, ___m_NumVideosAdded_6)); }
	inline int32_t get_m_NumVideosAdded_6() const { return ___m_NumVideosAdded_6; }
	inline int32_t* get_address_of_m_NumVideosAdded_6() { return &___m_NumVideosAdded_6; }
	inline void set_m_NumVideosAdded_6(int32_t value)
	{
		___m_NumVideosAdded_6 = value;
	}

	inline static int32_t get_offset_of_m_aAddedVideos_7() { return static_cast<int32_t>(offsetof(SampleApp_Multiple_tEC8B83D898EFB18F16A1D2A8A5BA3D26A8141F4A, ___m_aAddedVideos_7)); }
	inline List_1_tF5D950E434FC24D751780F604224065C6FC2169B * get_m_aAddedVideos_7() const { return ___m_aAddedVideos_7; }
	inline List_1_tF5D950E434FC24D751780F604224065C6FC2169B ** get_address_of_m_aAddedVideos_7() { return &___m_aAddedVideos_7; }
	inline void set_m_aAddedVideos_7(List_1_tF5D950E434FC24D751780F604224065C6FC2169B * value)
	{
		___m_aAddedVideos_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_aAddedVideos_7), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.SimpleController
struct SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String RenderHeads.Media.AVProVideo.Demos.SimpleController::_folder
	String_t* ____folder_4;
	// System.String[] RenderHeads.Media.AVProVideo.Demos.SimpleController::_filenames
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____filenames_5;
	// System.String[] RenderHeads.Media.AVProVideo.Demos.SimpleController::_streams
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____streams_6;
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.SimpleController::_mediaPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mediaPlayer_7;
	// RenderHeads.Media.AVProVideo.DisplayIMGUI RenderHeads.Media.AVProVideo.Demos.SimpleController::_display
	DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04 * ____display_8;
	// UnityEngine.GUISkin RenderHeads.Media.AVProVideo.Demos.SimpleController::_guiSkin
	GUISkin_tE353D65D4618423B574BAD31F5C5AC1B967E32C6 * ____guiSkin_9;
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.SimpleController::_width
	int32_t ____width_10;
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.SimpleController::_height
	int32_t ____height_11;
	// System.Single RenderHeads.Media.AVProVideo.Demos.SimpleController::_durationSeconds
	float ____durationSeconds_12;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.SimpleController::_useFading
	bool ____useFading_13;
	// System.Collections.Generic.Queue`1<System.String> RenderHeads.Media.AVProVideo.Demos.SimpleController::_eventLog
	Queue_1_tD2C03A5990B5958D85846D872A22AA67F3E8F97D * ____eventLog_14;
	// System.Single RenderHeads.Media.AVProVideo.Demos.SimpleController::_eventTimer
	float ____eventTimer_15;
	// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation RenderHeads.Media.AVProVideo.Demos.SimpleController::_nextVideoLocation
	int32_t ____nextVideoLocation_16;
	// System.String RenderHeads.Media.AVProVideo.Demos.SimpleController::_nextVideoPath
	String_t* ____nextVideoPath_17;

public:
	inline static int32_t get_offset_of__folder_4() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____folder_4)); }
	inline String_t* get__folder_4() const { return ____folder_4; }
	inline String_t** get_address_of__folder_4() { return &____folder_4; }
	inline void set__folder_4(String_t* value)
	{
		____folder_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____folder_4), (void*)value);
	}

	inline static int32_t get_offset_of__filenames_5() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____filenames_5)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__filenames_5() const { return ____filenames_5; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__filenames_5() { return &____filenames_5; }
	inline void set__filenames_5(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____filenames_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____filenames_5), (void*)value);
	}

	inline static int32_t get_offset_of__streams_6() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____streams_6)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__streams_6() const { return ____streams_6; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__streams_6() { return &____streams_6; }
	inline void set__streams_6(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____streams_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____streams_6), (void*)value);
	}

	inline static int32_t get_offset_of__mediaPlayer_7() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____mediaPlayer_7)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mediaPlayer_7() const { return ____mediaPlayer_7; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mediaPlayer_7() { return &____mediaPlayer_7; }
	inline void set__mediaPlayer_7(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mediaPlayer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_7), (void*)value);
	}

	inline static int32_t get_offset_of__display_8() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____display_8)); }
	inline DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04 * get__display_8() const { return ____display_8; }
	inline DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04 ** get_address_of__display_8() { return &____display_8; }
	inline void set__display_8(DisplayIMGUI_t80ADC02CC0C890CCAB96D6B048A857DFF16D5B04 * value)
	{
		____display_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____display_8), (void*)value);
	}

	inline static int32_t get_offset_of__guiSkin_9() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____guiSkin_9)); }
	inline GUISkin_tE353D65D4618423B574BAD31F5C5AC1B967E32C6 * get__guiSkin_9() const { return ____guiSkin_9; }
	inline GUISkin_tE353D65D4618423B574BAD31F5C5AC1B967E32C6 ** get_address_of__guiSkin_9() { return &____guiSkin_9; }
	inline void set__guiSkin_9(GUISkin_tE353D65D4618423B574BAD31F5C5AC1B967E32C6 * value)
	{
		____guiSkin_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____guiSkin_9), (void*)value);
	}

	inline static int32_t get_offset_of__width_10() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____width_10)); }
	inline int32_t get__width_10() const { return ____width_10; }
	inline int32_t* get_address_of__width_10() { return &____width_10; }
	inline void set__width_10(int32_t value)
	{
		____width_10 = value;
	}

	inline static int32_t get_offset_of__height_11() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____height_11)); }
	inline int32_t get__height_11() const { return ____height_11; }
	inline int32_t* get_address_of__height_11() { return &____height_11; }
	inline void set__height_11(int32_t value)
	{
		____height_11 = value;
	}

	inline static int32_t get_offset_of__durationSeconds_12() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____durationSeconds_12)); }
	inline float get__durationSeconds_12() const { return ____durationSeconds_12; }
	inline float* get_address_of__durationSeconds_12() { return &____durationSeconds_12; }
	inline void set__durationSeconds_12(float value)
	{
		____durationSeconds_12 = value;
	}

	inline static int32_t get_offset_of__useFading_13() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____useFading_13)); }
	inline bool get__useFading_13() const { return ____useFading_13; }
	inline bool* get_address_of__useFading_13() { return &____useFading_13; }
	inline void set__useFading_13(bool value)
	{
		____useFading_13 = value;
	}

	inline static int32_t get_offset_of__eventLog_14() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____eventLog_14)); }
	inline Queue_1_tD2C03A5990B5958D85846D872A22AA67F3E8F97D * get__eventLog_14() const { return ____eventLog_14; }
	inline Queue_1_tD2C03A5990B5958D85846D872A22AA67F3E8F97D ** get_address_of__eventLog_14() { return &____eventLog_14; }
	inline void set__eventLog_14(Queue_1_tD2C03A5990B5958D85846D872A22AA67F3E8F97D * value)
	{
		____eventLog_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____eventLog_14), (void*)value);
	}

	inline static int32_t get_offset_of__eventTimer_15() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____eventTimer_15)); }
	inline float get__eventTimer_15() const { return ____eventTimer_15; }
	inline float* get_address_of__eventTimer_15() { return &____eventTimer_15; }
	inline void set__eventTimer_15(float value)
	{
		____eventTimer_15 = value;
	}

	inline static int32_t get_offset_of__nextVideoLocation_16() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____nextVideoLocation_16)); }
	inline int32_t get__nextVideoLocation_16() const { return ____nextVideoLocation_16; }
	inline int32_t* get_address_of__nextVideoLocation_16() { return &____nextVideoLocation_16; }
	inline void set__nextVideoLocation_16(int32_t value)
	{
		____nextVideoLocation_16 = value;
	}

	inline static int32_t get_offset_of__nextVideoPath_17() { return static_cast<int32_t>(offsetof(SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65, ____nextVideoPath_17)); }
	inline String_t* get__nextVideoPath_17() const { return ____nextVideoPath_17; }
	inline String_t** get_address_of__nextVideoPath_17() { return &____nextVideoPath_17; }
	inline void set__nextVideoPath_17(String_t* value)
	{
		____nextVideoPath_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____nextVideoPath_17), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.SphereDemo
struct SphereDemo_t59016787C067365A7A981577502AC5BDC6B911E7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.SphereDemo::_zeroCameraPosition
	bool ____zeroCameraPosition_4;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.SphereDemo::_allowRecenter
	bool ____allowRecenter_5;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.SphereDemo::_allowVrToggle
	bool ____allowVrToggle_6;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.SphereDemo::_lockPitch
	bool ____lockPitch_7;
	// System.Single RenderHeads.Media.AVProVideo.Demos.SphereDemo::_spinX
	float ____spinX_8;
	// System.Single RenderHeads.Media.AVProVideo.Demos.SphereDemo::_spinY
	float ____spinY_9;

public:
	inline static int32_t get_offset_of__zeroCameraPosition_4() { return static_cast<int32_t>(offsetof(SphereDemo_t59016787C067365A7A981577502AC5BDC6B911E7, ____zeroCameraPosition_4)); }
	inline bool get__zeroCameraPosition_4() const { return ____zeroCameraPosition_4; }
	inline bool* get_address_of__zeroCameraPosition_4() { return &____zeroCameraPosition_4; }
	inline void set__zeroCameraPosition_4(bool value)
	{
		____zeroCameraPosition_4 = value;
	}

	inline static int32_t get_offset_of__allowRecenter_5() { return static_cast<int32_t>(offsetof(SphereDemo_t59016787C067365A7A981577502AC5BDC6B911E7, ____allowRecenter_5)); }
	inline bool get__allowRecenter_5() const { return ____allowRecenter_5; }
	inline bool* get_address_of__allowRecenter_5() { return &____allowRecenter_5; }
	inline void set__allowRecenter_5(bool value)
	{
		____allowRecenter_5 = value;
	}

	inline static int32_t get_offset_of__allowVrToggle_6() { return static_cast<int32_t>(offsetof(SphereDemo_t59016787C067365A7A981577502AC5BDC6B911E7, ____allowVrToggle_6)); }
	inline bool get__allowVrToggle_6() const { return ____allowVrToggle_6; }
	inline bool* get_address_of__allowVrToggle_6() { return &____allowVrToggle_6; }
	inline void set__allowVrToggle_6(bool value)
	{
		____allowVrToggle_6 = value;
	}

	inline static int32_t get_offset_of__lockPitch_7() { return static_cast<int32_t>(offsetof(SphereDemo_t59016787C067365A7A981577502AC5BDC6B911E7, ____lockPitch_7)); }
	inline bool get__lockPitch_7() const { return ____lockPitch_7; }
	inline bool* get_address_of__lockPitch_7() { return &____lockPitch_7; }
	inline void set__lockPitch_7(bool value)
	{
		____lockPitch_7 = value;
	}

	inline static int32_t get_offset_of__spinX_8() { return static_cast<int32_t>(offsetof(SphereDemo_t59016787C067365A7A981577502AC5BDC6B911E7, ____spinX_8)); }
	inline float get__spinX_8() const { return ____spinX_8; }
	inline float* get_address_of__spinX_8() { return &____spinX_8; }
	inline void set__spinX_8(float value)
	{
		____spinX_8 = value;
	}

	inline static int32_t get_offset_of__spinY_9() { return static_cast<int32_t>(offsetof(SphereDemo_t59016787C067365A7A981577502AC5BDC6B911E7, ____spinY_9)); }
	inline float get__spinY_9() const { return ____spinY_9; }
	inline float* get_address_of__spinY_9() { return &____spinY_9; }
	inline void set__spinY_9(float value)
	{
		____spinY_9 = value;
	}
};


// RenderHeads.Media.AVProVideo.Demos.StartEndPoint
struct StartEndPoint_t963F5A2BBE3B9E2CFE0166DCBF7FB0263CD74A5D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.StartEndPoint::_mediaPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mediaPlayer_4;
	// System.Single RenderHeads.Media.AVProVideo.Demos.StartEndPoint::_startPointSeconds
	float ____startPointSeconds_5;
	// System.Single RenderHeads.Media.AVProVideo.Demos.StartEndPoint::_endPointSeconds
	float ____endPointSeconds_6;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.StartEndPoint::_loop
	bool ____loop_7;
	// System.Single RenderHeads.Media.AVProVideo.Demos.StartEndPoint::_startLoopSeconds
	float ____startLoopSeconds_8;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.StartEndPoint::_isStartQueued
	bool ____isStartQueued_9;

public:
	inline static int32_t get_offset_of__mediaPlayer_4() { return static_cast<int32_t>(offsetof(StartEndPoint_t963F5A2BBE3B9E2CFE0166DCBF7FB0263CD74A5D, ____mediaPlayer_4)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mediaPlayer_4() const { return ____mediaPlayer_4; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mediaPlayer_4() { return &____mediaPlayer_4; }
	inline void set__mediaPlayer_4(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mediaPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of__startPointSeconds_5() { return static_cast<int32_t>(offsetof(StartEndPoint_t963F5A2BBE3B9E2CFE0166DCBF7FB0263CD74A5D, ____startPointSeconds_5)); }
	inline float get__startPointSeconds_5() const { return ____startPointSeconds_5; }
	inline float* get_address_of__startPointSeconds_5() { return &____startPointSeconds_5; }
	inline void set__startPointSeconds_5(float value)
	{
		____startPointSeconds_5 = value;
	}

	inline static int32_t get_offset_of__endPointSeconds_6() { return static_cast<int32_t>(offsetof(StartEndPoint_t963F5A2BBE3B9E2CFE0166DCBF7FB0263CD74A5D, ____endPointSeconds_6)); }
	inline float get__endPointSeconds_6() const { return ____endPointSeconds_6; }
	inline float* get_address_of__endPointSeconds_6() { return &____endPointSeconds_6; }
	inline void set__endPointSeconds_6(float value)
	{
		____endPointSeconds_6 = value;
	}

	inline static int32_t get_offset_of__loop_7() { return static_cast<int32_t>(offsetof(StartEndPoint_t963F5A2BBE3B9E2CFE0166DCBF7FB0263CD74A5D, ____loop_7)); }
	inline bool get__loop_7() const { return ____loop_7; }
	inline bool* get_address_of__loop_7() { return &____loop_7; }
	inline void set__loop_7(bool value)
	{
		____loop_7 = value;
	}

	inline static int32_t get_offset_of__startLoopSeconds_8() { return static_cast<int32_t>(offsetof(StartEndPoint_t963F5A2BBE3B9E2CFE0166DCBF7FB0263CD74A5D, ____startLoopSeconds_8)); }
	inline float get__startLoopSeconds_8() const { return ____startLoopSeconds_8; }
	inline float* get_address_of__startLoopSeconds_8() { return &____startLoopSeconds_8; }
	inline void set__startLoopSeconds_8(float value)
	{
		____startLoopSeconds_8 = value;
	}

	inline static int32_t get_offset_of__isStartQueued_9() { return static_cast<int32_t>(offsetof(StartEndPoint_t963F5A2BBE3B9E2CFE0166DCBF7FB0263CD74A5D, ____isStartQueued_9)); }
	inline bool get__isStartQueued_9() const { return ____isStartQueued_9; }
	inline bool* get_address_of__isStartQueued_9() { return &____isStartQueued_9; }
	inline void set__isStartQueued_9(bool value)
	{
		____isStartQueued_9 = value;
	}
};


// RenderHeads.Media.AVProVideo.Demos.VCR
struct VCR_t439D406D89424E5A32DA62F83830BA060CEC3459  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.VCR::_mediaPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mediaPlayer_4;
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.VCR::_mediaPlayerB
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mediaPlayerB_5;
	// RenderHeads.Media.AVProVideo.DisplayUGUI RenderHeads.Media.AVProVideo.Demos.VCR::_mediaDisplay
	DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * ____mediaDisplay_6;
	// UnityEngine.RectTransform RenderHeads.Media.AVProVideo.Demos.VCR::_bufferedSliderRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ____bufferedSliderRect_7;
	// UnityEngine.UI.Slider RenderHeads.Media.AVProVideo.Demos.VCR::_videoSeekSlider
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * ____videoSeekSlider_8;
	// System.Single RenderHeads.Media.AVProVideo.Demos.VCR::_setVideoSeekSliderValue
	float ____setVideoSeekSliderValue_9;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.VCR::_wasPlayingOnScrub
	bool ____wasPlayingOnScrub_10;
	// UnityEngine.UI.Slider RenderHeads.Media.AVProVideo.Demos.VCR::_audioVolumeSlider
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * ____audioVolumeSlider_11;
	// System.Single RenderHeads.Media.AVProVideo.Demos.VCR::_setAudioVolumeSliderValue
	float ____setAudioVolumeSliderValue_12;
	// UnityEngine.UI.Toggle RenderHeads.Media.AVProVideo.Demos.VCR::_AutoStartToggle
	Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * ____AutoStartToggle_13;
	// UnityEngine.UI.Toggle RenderHeads.Media.AVProVideo.Demos.VCR::_MuteToggle
	Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * ____MuteToggle_14;
	// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation RenderHeads.Media.AVProVideo.Demos.VCR::_location
	int32_t ____location_15;
	// System.String RenderHeads.Media.AVProVideo.Demos.VCR::_folder
	String_t* ____folder_16;
	// System.String[] RenderHeads.Media.AVProVideo.Demos.VCR::_videoFiles
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____videoFiles_17;
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.VCR::_VideoIndex
	int32_t ____VideoIndex_18;
	// UnityEngine.UI.Image RenderHeads.Media.AVProVideo.Demos.VCR::_bufferedSliderImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ____bufferedSliderImage_19;
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.VCR::_loadingPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____loadingPlayer_20;

public:
	inline static int32_t get_offset_of__mediaPlayer_4() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____mediaPlayer_4)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mediaPlayer_4() const { return ____mediaPlayer_4; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mediaPlayer_4() { return &____mediaPlayer_4; }
	inline void set__mediaPlayer_4(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mediaPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of__mediaPlayerB_5() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____mediaPlayerB_5)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mediaPlayerB_5() const { return ____mediaPlayerB_5; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mediaPlayerB_5() { return &____mediaPlayerB_5; }
	inline void set__mediaPlayerB_5(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mediaPlayerB_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayerB_5), (void*)value);
	}

	inline static int32_t get_offset_of__mediaDisplay_6() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____mediaDisplay_6)); }
	inline DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * get__mediaDisplay_6() const { return ____mediaDisplay_6; }
	inline DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A ** get_address_of__mediaDisplay_6() { return &____mediaDisplay_6; }
	inline void set__mediaDisplay_6(DisplayUGUI_t95261AB5F5908F93E133E41E726EAE212B7C606A * value)
	{
		____mediaDisplay_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaDisplay_6), (void*)value);
	}

	inline static int32_t get_offset_of__bufferedSliderRect_7() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____bufferedSliderRect_7)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get__bufferedSliderRect_7() const { return ____bufferedSliderRect_7; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of__bufferedSliderRect_7() { return &____bufferedSliderRect_7; }
	inline void set__bufferedSliderRect_7(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		____bufferedSliderRect_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bufferedSliderRect_7), (void*)value);
	}

	inline static int32_t get_offset_of__videoSeekSlider_8() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____videoSeekSlider_8)); }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * get__videoSeekSlider_8() const { return ____videoSeekSlider_8; }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A ** get_address_of__videoSeekSlider_8() { return &____videoSeekSlider_8; }
	inline void set__videoSeekSlider_8(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * value)
	{
		____videoSeekSlider_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____videoSeekSlider_8), (void*)value);
	}

	inline static int32_t get_offset_of__setVideoSeekSliderValue_9() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____setVideoSeekSliderValue_9)); }
	inline float get__setVideoSeekSliderValue_9() const { return ____setVideoSeekSliderValue_9; }
	inline float* get_address_of__setVideoSeekSliderValue_9() { return &____setVideoSeekSliderValue_9; }
	inline void set__setVideoSeekSliderValue_9(float value)
	{
		____setVideoSeekSliderValue_9 = value;
	}

	inline static int32_t get_offset_of__wasPlayingOnScrub_10() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____wasPlayingOnScrub_10)); }
	inline bool get__wasPlayingOnScrub_10() const { return ____wasPlayingOnScrub_10; }
	inline bool* get_address_of__wasPlayingOnScrub_10() { return &____wasPlayingOnScrub_10; }
	inline void set__wasPlayingOnScrub_10(bool value)
	{
		____wasPlayingOnScrub_10 = value;
	}

	inline static int32_t get_offset_of__audioVolumeSlider_11() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____audioVolumeSlider_11)); }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * get__audioVolumeSlider_11() const { return ____audioVolumeSlider_11; }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A ** get_address_of__audioVolumeSlider_11() { return &____audioVolumeSlider_11; }
	inline void set__audioVolumeSlider_11(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * value)
	{
		____audioVolumeSlider_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____audioVolumeSlider_11), (void*)value);
	}

	inline static int32_t get_offset_of__setAudioVolumeSliderValue_12() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____setAudioVolumeSliderValue_12)); }
	inline float get__setAudioVolumeSliderValue_12() const { return ____setAudioVolumeSliderValue_12; }
	inline float* get_address_of__setAudioVolumeSliderValue_12() { return &____setAudioVolumeSliderValue_12; }
	inline void set__setAudioVolumeSliderValue_12(float value)
	{
		____setAudioVolumeSliderValue_12 = value;
	}

	inline static int32_t get_offset_of__AutoStartToggle_13() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____AutoStartToggle_13)); }
	inline Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * get__AutoStartToggle_13() const { return ____AutoStartToggle_13; }
	inline Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E ** get_address_of__AutoStartToggle_13() { return &____AutoStartToggle_13; }
	inline void set__AutoStartToggle_13(Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * value)
	{
		____AutoStartToggle_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____AutoStartToggle_13), (void*)value);
	}

	inline static int32_t get_offset_of__MuteToggle_14() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____MuteToggle_14)); }
	inline Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * get__MuteToggle_14() const { return ____MuteToggle_14; }
	inline Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E ** get_address_of__MuteToggle_14() { return &____MuteToggle_14; }
	inline void set__MuteToggle_14(Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * value)
	{
		____MuteToggle_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____MuteToggle_14), (void*)value);
	}

	inline static int32_t get_offset_of__location_15() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____location_15)); }
	inline int32_t get__location_15() const { return ____location_15; }
	inline int32_t* get_address_of__location_15() { return &____location_15; }
	inline void set__location_15(int32_t value)
	{
		____location_15 = value;
	}

	inline static int32_t get_offset_of__folder_16() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____folder_16)); }
	inline String_t* get__folder_16() const { return ____folder_16; }
	inline String_t** get_address_of__folder_16() { return &____folder_16; }
	inline void set__folder_16(String_t* value)
	{
		____folder_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____folder_16), (void*)value);
	}

	inline static int32_t get_offset_of__videoFiles_17() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____videoFiles_17)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__videoFiles_17() const { return ____videoFiles_17; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__videoFiles_17() { return &____videoFiles_17; }
	inline void set__videoFiles_17(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____videoFiles_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____videoFiles_17), (void*)value);
	}

	inline static int32_t get_offset_of__VideoIndex_18() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____VideoIndex_18)); }
	inline int32_t get__VideoIndex_18() const { return ____VideoIndex_18; }
	inline int32_t* get_address_of__VideoIndex_18() { return &____VideoIndex_18; }
	inline void set__VideoIndex_18(int32_t value)
	{
		____VideoIndex_18 = value;
	}

	inline static int32_t get_offset_of__bufferedSliderImage_19() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____bufferedSliderImage_19)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get__bufferedSliderImage_19() const { return ____bufferedSliderImage_19; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of__bufferedSliderImage_19() { return &____bufferedSliderImage_19; }
	inline void set__bufferedSliderImage_19(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		____bufferedSliderImage_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bufferedSliderImage_19), (void*)value);
	}

	inline static int32_t get_offset_of__loadingPlayer_20() { return static_cast<int32_t>(offsetof(VCR_t439D406D89424E5A32DA62F83830BA060CEC3459, ____loadingPlayer_20)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__loadingPlayer_20() const { return ____loadingPlayer_20; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__loadingPlayer_20() { return &____loadingPlayer_20; }
	inline void set__loadingPlayer_20(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____loadingPlayer_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____loadingPlayer_20), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.VideoTrigger
struct VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.VideoTrigger::_mediaPlayer
	MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * ____mediaPlayer_4;
	// System.Single RenderHeads.Media.AVProVideo.Demos.VideoTrigger::_fadeTimeMs
	float ____fadeTimeMs_5;
	// System.Single RenderHeads.Media.AVProVideo.Demos.VideoTrigger::_fade
	float ____fade_6;
	// System.Single RenderHeads.Media.AVProVideo.Demos.VideoTrigger::_fadeDirection
	float ____fadeDirection_7;

public:
	inline static int32_t get_offset_of__mediaPlayer_4() { return static_cast<int32_t>(offsetof(VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656, ____mediaPlayer_4)); }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * get__mediaPlayer_4() const { return ____mediaPlayer_4; }
	inline MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 ** get_address_of__mediaPlayer_4() { return &____mediaPlayer_4; }
	inline void set__mediaPlayer_4(MediaPlayer_tBFEBB98148056EF0AC06CCFA55B06583E0EA4A64 * value)
	{
		____mediaPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of__fadeTimeMs_5() { return static_cast<int32_t>(offsetof(VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656, ____fadeTimeMs_5)); }
	inline float get__fadeTimeMs_5() const { return ____fadeTimeMs_5; }
	inline float* get_address_of__fadeTimeMs_5() { return &____fadeTimeMs_5; }
	inline void set__fadeTimeMs_5(float value)
	{
		____fadeTimeMs_5 = value;
	}

	inline static int32_t get_offset_of__fade_6() { return static_cast<int32_t>(offsetof(VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656, ____fade_6)); }
	inline float get__fade_6() const { return ____fade_6; }
	inline float* get_address_of__fade_6() { return &____fade_6; }
	inline void set__fade_6(float value)
	{
		____fade_6 = value;
	}

	inline static int32_t get_offset_of__fadeDirection_7() { return static_cast<int32_t>(offsetof(VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656, ____fadeDirection_7)); }
	inline float get__fadeDirection_7() const { return ____fadeDirection_7; }
	inline float* get_address_of__fadeDirection_7() { return &____fadeDirection_7; }
	inline void set__fadeDirection_7(float value)
	{
		____fadeDirection_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4559;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4559 = { sizeof (Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996)+ sizeof (RuntimeObject), sizeof(Chunk_tD7F28E5DBCCCDF1153E4087AA2B5C0737E4ED996_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4560;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4560 = { sizeof (Stream_t0213C8B2018B430058C9AE8677468C4A07E73ACF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4561;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4561 = { sizeof (AutoRotate_tD3FCE4EA299571DD8C213BB0EACF8FC27AFCD314), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4562;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4562 = { sizeof (DemoInfo_tFC8CE5653450E76B5095669CD562A80FFD6712B7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4563;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4563 = { sizeof (FrameExtract_t410E78EB4F551F51E516EB118F22B05F30EB95F3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4564;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4564 = { sizeof (Mapping3D_tD245FFB24F152207D7C99D9AAB180568116C5D27), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4565;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4565 = { sizeof (SampleApp_Multiple_tEC8B83D898EFB18F16A1D2A8A5BA3D26A8141F4A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4566;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4566 = { sizeof (ChangeAudioTrack_t554262C74032CB71DB111B0A48644FAD68A43402), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4567;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4567 = { sizeof (ChangeStereoMode_t616C6D3D4660B71D39C27071D35157877D33B136), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4568;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4568 = { sizeof (ChangeVideoExample_t7A4744ABFD531F36394852968842D3CD8D832B23), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4569;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4569 = { sizeof (LoadFromBuffer_t4FDF3E21E03A6270CA1D4DABA65CC603A8280189), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4570;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4570 = { sizeof (LoadFromBufferInChunks_tFFCF1D595509528875CCD737262722559BE45788), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4571;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4571 = { sizeof (NativeMediaOpen_t97AA3DE898F9247ECB3A886B6A10BD345FA2D19C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4572;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4572 = { sizeof (State_t7FFEAADD0FEA4C45402967A4CC39F4A810EE3F18)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4573;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4573 = { sizeof (PlaybackSync_t81C52C0F1A279BD434628CA123D1D4D31C40F3B2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4574;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4574 = { sizeof (StartEndPoint_t963F5A2BBE3B9E2CFE0166DCBF7FB0263CD74A5D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4575;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4575 = { sizeof (VideoTrigger_tB6EBE699DAF0F757453E986E4E70BED08BD49656), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4576;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4576 = { sizeof (U3CLoadVideoWithFadingU3Ed__23_t74E7CB70F3DE7F75CA253D4FC92BF66FDFF54FB9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4577;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4577 = { sizeof (SimpleController_tB8652B8023677B2CB1E85CEBD42FE9EBB1789E65), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4578;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4578 = { sizeof (SphereDemo_t59016787C067365A7A981577502AC5BDC6B911E7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4579;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4579 = { sizeof (VCR_t439D406D89424E5A32DA62F83830BA060CEC3459), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4580;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4580 = { sizeof (CustomVCR_t8CE78061CA3254720943BADD352C2145011EFD2A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4581;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4581 = { sizeof (__StaticArrayInitTypeSizeU3D24_t2F23740D8943FC7C06AD3DD80B71D65744F140F2)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D24_t2F23740D8943FC7C06AD3DD80B71D65744F140F2 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4582;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4582 = { sizeof (__StaticArrayInitTypeSizeU3D144_t4B2822F6C46C674B47DD7F528A50B6B894D896D4)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D144_t4B2822F6C46C674B47DD7F528A50B6B894D896D4 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4583;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4583 = { sizeof (U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528), -1, sizeof(U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_StaticFields), 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
