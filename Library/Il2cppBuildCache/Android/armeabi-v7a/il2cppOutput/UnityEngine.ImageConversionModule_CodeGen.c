﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Byte[] UnityEngine.ImageConversion::EncodeToTGA(UnityEngine.Texture2D)
extern void ImageConversion_EncodeToTGA_m2DD31B139814673A8CBF06B5089E4B6629F277B0 (void);
// 0x00000002 System.Byte[] UnityEngine.ImageConversion::EncodeToPNG(UnityEngine.Texture2D)
extern void ImageConversion_EncodeToPNG_mA598C2969C878ACC5AEA5FDC92C6199EB30D51ED (void);
// 0x00000003 System.Byte[] UnityEngine.ImageConversion::EncodeToJPG(UnityEngine.Texture2D,System.Int32)
extern void ImageConversion_EncodeToJPG_mE32249F45E643405652057EA8A35112AB4721641 (void);
// 0x00000004 System.Byte[] UnityEngine.ImageConversion::EncodeToJPG(UnityEngine.Texture2D)
extern void ImageConversion_EncodeToJPG_m75581ECC85FF41720A0CCB7602EF6E1D037C909F (void);
// 0x00000005 System.Byte[] UnityEngine.ImageConversion::EncodeToEXR(UnityEngine.Texture2D,UnityEngine.Texture2D/EXRFlags)
extern void ImageConversion_EncodeToEXR_mF1771F3F35480E47E31A104E1A4E5F7CC63049DE (void);
// 0x00000006 System.Byte[] UnityEngine.ImageConversion::EncodeToEXR(UnityEngine.Texture2D)
extern void ImageConversion_EncodeToEXR_mC0F94970CE17C855ACD00789303E72E9BF9B7118 (void);
// 0x00000007 System.Boolean UnityEngine.ImageConversion::LoadImage(UnityEngine.Texture2D,System.Byte[],System.Boolean)
extern void ImageConversion_LoadImage_m1E5C9BF6206ED40B23CDB28341B8A64E05C43683 (void);
// 0x00000008 System.Boolean UnityEngine.ImageConversion::LoadImage(UnityEngine.Texture2D,System.Byte[])
extern void ImageConversion_LoadImage_m5BB4FBA0565E698ED5C6F25F7A0A5F83CADD7477 (void);
static Il2CppMethodPointer s_methodPointers[8] = 
{
	ImageConversion_EncodeToTGA_m2DD31B139814673A8CBF06B5089E4B6629F277B0,
	ImageConversion_EncodeToPNG_mA598C2969C878ACC5AEA5FDC92C6199EB30D51ED,
	ImageConversion_EncodeToJPG_mE32249F45E643405652057EA8A35112AB4721641,
	ImageConversion_EncodeToJPG_m75581ECC85FF41720A0CCB7602EF6E1D037C909F,
	ImageConversion_EncodeToEXR_mF1771F3F35480E47E31A104E1A4E5F7CC63049DE,
	ImageConversion_EncodeToEXR_mC0F94970CE17C855ACD00789303E72E9BF9B7118,
	ImageConversion_LoadImage_m1E5C9BF6206ED40B23CDB28341B8A64E05C43683,
	ImageConversion_LoadImage_m5BB4FBA0565E698ED5C6F25F7A0A5F83CADD7477,
};
static const int32_t s_InvokerIndices[8] = 
{
	4927,
	4927,
	4365,
	4927,
	4365,
	4927,
	4061,
	4472,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_ImageConversionModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_ImageConversionModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_ImageConversionModule_CodeGenModule = 
{
	"UnityEngine.ImageConversionModule.dll",
	8,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_ImageConversionModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
