using RenderHeads.Media.AVProVideo;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VideoVCR : MonoBehaviour
{
    public MediaPlayer _mediaPlayer;
    public RectTransform _bufferedSliderRect;

    public Slider _videoSeekSlider;
    private float _setVideoSeekSliderValue;
    private bool _wasPlayingOnScrub;

    public MediaPlayer.FileLocation _location = MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder;
    public string _folder = "Airtel5G/Video/";
    public string _videoFiles;
    private Image _bufferedSliderImage;

    private MediaPlayer _loadingPlayer;

    public Texture2D _texture;
    private float _timeStepSeconds;
    private const int NumFrames = 8;

    public MediaPlayer PlayingPlayer
    {
        get
        {
            return _mediaPlayer;
        }
    }

    public MediaPlayer LoadingPlayer
    {
        get
        {
            return _mediaPlayer;
        }
    }

    void Start()
    {
        if (PlayingPlayer)
        {
            PlayingPlayer.Events.AddListener(OnVideoEvent);

            if (PlayingPlayer.m_AutoOpen)
            {
                //					RemoveOpenVideoButton();

                //					SetButtonEnabled( "PlayButton", !_mediaPlayer.m_AutoStart );
                //					SetButtonEnabled( "PauseButton", _mediaPlayer.m_AutoStart );
            }
            else
            {
                //					SetButtonEnabled( "PlayButton", false );
                //					SetButtonEnabled( "PauseButton", false );
            }

            //				SetButtonEnabled( "MuteButton", !_mediaPlayer.m_Muted );
            //				SetButtonEnabled( "UnmuteButton", _mediaPlayer.m_Muted );

            OnOpenVideoFile();
        }
    }

    public void OnOpenVideoFile()
    {
        LoadingPlayer.m_VideoPath = System.IO.Path.Combine(_folder, _videoFiles);

        if (string.IsNullOrEmpty(LoadingPlayer.m_VideoPath))
        {
            LoadingPlayer.CloseVideo();

        }
        else
        {
            Debug.Log(LoadingPlayer.m_VideoPath);
            LoadingPlayer.OpenVideoFromFile(_location, LoadingPlayer.m_VideoPath);
            //				SetButtonEnabled( "PlayButton", !_mediaPlayer.m_AutoStart );
            //				SetButtonEnabled( "PauseButton", _mediaPlayer.m_AutoStart );
        }

        if (_bufferedSliderRect != null)
        {
            _bufferedSliderImage = _bufferedSliderRect.GetComponent<Image>();
        }

    }

    void Update()
    {
        if (PlayingPlayer && PlayingPlayer.Info != null && PlayingPlayer.Info.GetDurationMs() > 0f)
        {
            float time = PlayingPlayer.Control.GetCurrentTimeMs();
            float duration = PlayingPlayer.Info.GetDurationMs();
            float d = Mathf.Clamp(time / duration, 0.0f, 1.0f);

            // Debug.Log(string.Format("time: {0}, duration: {1}, d: {2}", time, duration, d));

            _setVideoSeekSliderValue = d;
            _videoSeekSlider.value = d;

            if (_bufferedSliderRect != null)
            {
                if (PlayingPlayer.Control.IsBuffering())
                {
                    float t1 = 0f;
                    float t2 = PlayingPlayer.Control.GetBufferingProgress();
                    if (t2 <= 0f)
                    {
                        if (PlayingPlayer.Control.GetBufferedTimeRangeCount() > 0)
                        {
                            PlayingPlayer.Control.GetBufferedTimeRange(0, ref t1, ref t2);
                            t1 /= PlayingPlayer.Info.GetDurationMs();
                            t2 /= PlayingPlayer.Info.GetDurationMs();
                        }
                    }

                    Vector2 anchorMin = Vector2.zero;
                    Vector2 anchorMax = Vector2.one;

                    if (_bufferedSliderImage != null &&
                        _bufferedSliderImage.type == Image.Type.Filled)
                    {
                        _bufferedSliderImage.fillAmount = d;
                    }
                    else
                    {
                        anchorMin[0] = t1;
                        anchorMax[0] = t2;
                    }

                    _bufferedSliderRect.anchorMin = anchorMin;
                    _bufferedSliderRect.anchorMax = anchorMax;
                }
            }
        }
    }

    public void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
    {
        switch (et)
        {
            case MediaPlayerEvent.EventType.ReadyToPlay:
                break;
            case MediaPlayerEvent.EventType.Started:
                break;
            case MediaPlayerEvent.EventType.FirstFrameReady:
                //SwapPlayers();
                OnNewMediaReady();
                break;
            case MediaPlayerEvent.EventType.FinishedPlaying:
                break;
        }

        Debug.Log("Event: " + et.ToString());
    }

    private void OnNewMediaReady()
    {
        IMediaInfo info = PlayingPlayer.Info;

        // Create a texture the same resolution as our video
        if (_texture != null)
        {
            Texture2D.Destroy(_texture);
            _texture = null;
        }

        int textureWidth = info.GetVideoWidth();
        int textureHeight = info.GetVideoHeight();
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX || UNITY_IPHONE || UNITY_IOS || UNITY_TVOS
			Orientation ori = Helper.GetOrientation(_mediaPlayer.Info.GetTextureTransform());
			if (ori == Orientation.Portrait || ori == Orientation.PortraitFlipped)
			{
				textureWidth = info.GetVideoHeight();
				textureHeight = info.GetVideoWidth();
			}
#endif

        _texture = new Texture2D(textureWidth, textureHeight, TextureFormat.ARGB32, false);

        _timeStepSeconds = (PlayingPlayer.Info.GetDurationMs() / 1000f) / (float)NumFrames;

    }

    public void OnVideoSeekSlider()
    {
        if (PlayingPlayer && _videoSeekSlider && _videoSeekSlider.value != _setVideoSeekSliderValue)
        {
            //_texture = PlayingPlayer.ExtractFrame(_texture, _videoSeekSlider.value * PlayingPlayer.Info.GetDurationMs(), true, 250);
            PlayingPlayer.Control.Seek(_videoSeekSlider.value * PlayingPlayer.Info.GetDurationMs());
        }
    }

    public void OnVideoSliderDown()
    {
        if (PlayingPlayer)
        {
            _wasPlayingOnScrub = PlayingPlayer.Control.IsPlaying();
            if (_wasPlayingOnScrub)
            {
                PlayingPlayer.Pause();
                //					SetButtonEnabled( "PauseButton", false );
                //					SetButtonEnabled( "PlayButton", true );
            }
            OnVideoSeekSlider();
        }
    }
    public void OnVideoSliderUp()
    {
        if (PlayingPlayer && _wasPlayingOnScrub)
        {
            PlayingPlayer.Control.Play();
            _wasPlayingOnScrub = false;

            //				SetButtonEnabled( "PlayButton", false );
            //				SetButtonEnabled( "PauseButton", true );
        }
    }
}
