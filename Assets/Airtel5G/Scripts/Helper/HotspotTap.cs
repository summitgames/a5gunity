using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotspotTap : MonoBehaviour
{
    public GameObject informationPanel;
    private bool isPopupOpen = false;

    public void Start()
    {
        informationPanel.SetActive(false);
    }

    public void OnButtonTap()
    {
        informationPanel.SetActive(!isPopupOpen);
        isPopupOpen = !isPopupOpen;
    }
}
