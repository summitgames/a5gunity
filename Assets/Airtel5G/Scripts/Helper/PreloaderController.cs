using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreloaderController : MonoBehaviour
{
    public GameObject preLoaderPanel;
    public float preloaderDelay = 5f;


   


    // Start is called before the first frame update
    void Start()
    {
        preLoaderPanel.SetActive(true);
        StartCoroutine(DisablePreloaderPanel());
    }

    private IEnumerator DisablePreloaderPanel()
    {
        yield return new WaitForSeconds(preloaderDelay);
        preLoaderPanel.SetActive(false);
    }
}
