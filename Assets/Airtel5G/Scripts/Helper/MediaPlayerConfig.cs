using System.Collections;
using System.Collections.Generic;
using RenderHeads.Media.AVProVideo;
using UnityEngine;

public class MediaPlayerConfig : MonoBehaviour
{
    public MediaPlayer mediaPlayer;
    private void Awake()
    {
        //// Start adaptive stream using the highest resolution - ExoPlayer only
        //mediaPlayer.PlatformOptionsAndroid.videoApi = Android.VideoApi.ExoPlayer;
        //mediaPlayer.PlatformOptionsAndroid.startWithHighestBitrate = true;

        //// Set the maximum adaptive resolution to 1080p - ExoPlayer only
        //mediaPlayer.PlatformOptionsAndroid.videoApi = Android.VideoApi.ExoPlayer;
        //mediaPlayer.PlatformOptionsAndroid.preferredMaximumResolution = OptionsAndroid.Resolution._1080p;

        //// Set the peak adaptive bitrate to 4Mbps - ExoPlayer only
        //mediaPlayer.PlatformOptionsAndroid.videoApi = Android.VideoApi.ExoPlayer;
        //mediaPlayer.PlatformOptionsAndroid.preferredPeakBitRate = 4.0f;
        //mediaPlayer.PlatformOptionsAndroid.preferredPeakBitRateUnits = OptionsAndroid.BitRateUnits.Mbps;


        //// Adjust buffering - ExoPlayer only
        //mediaPlayer.PlatformOptionsAndroid.videoApi = Android.VideoApi.ExoPlayer;
        //// Set a buffer size of 50 seconds
        //mediaPlayer.PlatformOptionsAndroid.minBufferMs = 50000;
        //mediaPlayer.PlatformOptionsAndroid.maxBufferMs = 50000;
        //// Wait for 2.5 seconds of media to become buffered before playback begins or resumes after a seek
        //mediaPlayer.PlatformOptionsAndroid.bufferForPlaybackMs = 2500;
        //// Wait for 5.0 seconds of media to become buffered before playback starts after the buffer runs out due to bandwidth/connection issues
        //mediaPlayer.PlatformOptionsAndroid.bufferForPlaybackAfterRebufferMs = 5000;
    }
}
