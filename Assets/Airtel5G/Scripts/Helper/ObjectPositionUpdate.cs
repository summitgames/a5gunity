using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPositionUpdate : MonoBehaviour
{
    private Transform referencePosTransform;

    private void Awake()
    {
        referencePosTransform = Camera.main.transform;
    }


    private void OnEnable()
    {
        Debug.Log("root object : " + transform.root.name);
        referencePosTransform = Camera.main.transform;    
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = referencePosTransform.position;
    }
}
