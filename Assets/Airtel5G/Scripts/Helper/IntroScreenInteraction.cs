using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARFoundation.Samples;

public class IntroScreenInteraction : MonoBehaviour
{
    public GameObject introScreenPanel;
    public GameObject scanPanel;
    public Slider slider;

    [Header("ARFoundation Reference")]
    public ARTrackedImageManager arTrackedImageManager;
    public PrefabImagePairManager prefabImagePairManager;
    public DynamicPrefab dynamicPrefab;

    private void OnEnable()
    {
        InitUI();
        ToggleARFoundationImageTracking(false);
        slider.value = 0f;
    }

    public void OnValueChange(float value)
    {
        if (value >= 0.95f)
        {
            Debug.Log("Enable AR scane");
            introScreenPanel.SetActive(false);
            scanPanel.SetActive(true);
            ToggleARFoundationImageTracking(true);
        }
        else
        {
            Debug.Log("Disable AR scane");
        }
    }

    public void InitUI()
    {
        introScreenPanel.SetActive(true);
        scanPanel.SetActive(false);
    }

    public void ToggleARFoundationImageTracking(bool isEnable)
    {
        arTrackedImageManager.enabled = isEnable;
        prefabImagePairManager.enabled = isEnable;
        dynamicPrefab.enabled = isEnable;
    }
}
