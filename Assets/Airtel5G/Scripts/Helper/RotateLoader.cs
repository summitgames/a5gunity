using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateLoader : MonoBehaviour
{
    public float speed = 2f;
    private float speedMultiplayFactor = 100f;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.back * speed * speedMultiplayFactor* Time.deltaTime);
    }
}
