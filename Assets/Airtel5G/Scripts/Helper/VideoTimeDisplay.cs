using System.Collections;
using System.Collections.Generic;
using RenderHeads.Media.AVProVideo;
using UnityEngine;
using UnityEngine.UI;

public class VideoTimeDisplay : MonoBehaviour
{
    public MediaPlayer mediaPlayer;
    float time, duration, d;

    public Text timerText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        time = mediaPlayer.Control.GetCurrentTimeMs();
        duration = mediaPlayer.Info.GetDurationMs();
        d = Mathf.Clamp(time / duration, 0.0f, 1.0f);
        timerText.text = d.ToString("F4");
    }
}
