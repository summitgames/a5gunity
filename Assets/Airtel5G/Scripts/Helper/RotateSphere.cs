using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSphere : MonoBehaviour
{
    Vector3 mPreviousPosition = Vector3.zero;
    Vector3 mPositionDelta = Vector3.zero;

    float horizontal;
    float vertical;

    // Update is called once per frame
    #if UNITY_EDITOR
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            mPositionDelta = Input.mousePosition - mPreviousPosition;

            if (Vector3.Dot(transform.up, Vector3.up) >= 0)
            {
                transform.Rotate(transform.up, -Vector3.Dot(mPositionDelta, Camera.main.transform.right), Space.World);
            }
            else
            {
                transform.Rotate(transform.up, Vector3.Dot(mPositionDelta, Camera.main.transform.right), Space.World);
            }
        }

        mPreviousPosition = Input.mousePosition;
    }
    #endif
    
}
