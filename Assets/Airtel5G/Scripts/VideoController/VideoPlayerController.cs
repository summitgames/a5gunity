using System.Collections;
using System.Collections.Generic;
using RenderHeads.Media.AVProVideo;
using UnityEngine;
using UnityEngine.UI;

public class VideoPlayerController : MonoBehaviour
{
    [Header("Mediaplayers")]
    public MediaPlayer mediaPlayerA;
    public MediaPlayer mediaPlayerB;
    public MediaPlayer mediaPlayerC;
    public MediaPlayer mediaPlayerD;

    [Header("VideoDisplay")]
    public DisplayUGUI displayUGUIMain;
    public DisplayUGUI displayUGUIA;
    public DisplayUGUI displayUGUIB;
    public DisplayUGUI displayUGUIC;
    public DisplayUGUI displayUGUID;



    public float playStartDelay = 5f;

    //public Text mediaPlayerATime;
    //public Text mediaPlayerBTime;
    //public Text mediaPlayerCTime;
    //public Text mediaPlayerDTime;


    public void Start()
    {
        InitComponent();
        StartCoroutine(StartVideoPlaying());
    }

    private void Update()
    {
        //mediaPlayerATime.text = mediaPlayerA.Control.GetCurrentTimeMs().ToString();
        //mediaPlayerBTime.text = mediaPlayerB.Control.GetCurrentTimeMs().ToString();
        //mediaPlayerCTime.text = mediaPlayerC.Control.GetCurrentTimeMs().ToString();
        //mediaPlayerDTime.text = mediaPlayerD.Control.GetCurrentTimeMs().ToString();
    }

    public void InitComponent()
    {
        
        displayUGUIA._mediaPlayer = mediaPlayerA;
        displayUGUIB._mediaPlayer = mediaPlayerB;
        displayUGUIC._mediaPlayer = mediaPlayerC;
        displayUGUID._mediaPlayer = mediaPlayerD;
        displayUGUIMain._mediaPlayer = mediaPlayerA;
    }

    public IEnumerator StartVideoPlaying()
    {
        yield return new WaitForSeconds(playStartDelay);
        
        mediaPlayerA.Play();
        mediaPlayerB.Play();
        mediaPlayerC.Play();
        mediaPlayerD.Play();
    }

    public void OnVideoButtonClick(string mediaPlayerId)
    {
        
        switch (mediaPlayerId)
        {
            case "mediaPlayerA":
                displayUGUIMain._mediaPlayer = mediaPlayerA;
                break;
            case "mediaPlayerB":
                displayUGUIMain._mediaPlayer = mediaPlayerB;
                break;
            case "mediaPlayerC":
                displayUGUIMain._mediaPlayer = mediaPlayerC;
                break;
            case "mediaPlayerD":
                displayUGUIMain._mediaPlayer = mediaPlayerD;
                break;
        }
       
    }
}
