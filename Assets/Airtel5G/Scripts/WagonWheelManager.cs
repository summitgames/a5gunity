using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WagonWheelManager : MonoBehaviour
{
    public Animator animator;
    public void PlayAnimation(int animIndex)
    {
        animator.SetInteger("Next",animIndex);
    }
}
