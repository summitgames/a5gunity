using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;

public class OptionPanelManager : MonoBehaviour
{
    public void LoadScene(string scenename)
    {
        LoaderUtility.Initialize();
        SceneManager.LoadScene(scenename,LoadSceneMode.Single);
    }
}
