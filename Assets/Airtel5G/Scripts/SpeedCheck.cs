using System.Collections;
using UnityEngine;
using UnityEngine.UI;

using Crosstales.OnlineCheck;
using System.Net;

namespace Crosstales.OnlineCheck.Demo
{
    public class SpeedCheck : MonoBehaviour
    {
        #region Variables

        public GameObject MainScenePanel;
        public GameObject speedCheckPanel;
        public Text Result;
        public Button CheckButton;
        public Button BackButton;
        public float TimerAutoCheck = 20.0f;

        private bool isSpeedCheck = false;
        private bool isAutoSpeedCheck = false;
        private Coroutine co;
        #endregion

#if NET_4_6 || NET_STANDARD_2_0

        #region MonoBehaviour methods

        private void Start()
        {
            speedCheckPanel.SetActive(false);
            Result.text = "Please press the below button to test your network speed.";
            Tool.SpeedTestNET.SpeedTestNET.Instance.OnTestCompleted += onTestCompleted;

            if (Tool.SpeedTestNET.SpeedTestNET.Instance.LastServer != null)
                onTestCompleted(Tool.SpeedTestNET.SpeedTestNET.Instance.LastServer, Tool.SpeedTestNET.SpeedTestNET.Instance.LastDuration, Tool.SpeedTestNET.SpeedTestNET.Instance.LastDownloadSpeed, Tool.SpeedTestNET.SpeedTestNET.Instance.LastDownloadSpeed);
        }

        private void OnDestroy()
        {
            if (Tool.SpeedTestNET.SpeedTestNET.Instance != null)
                Tool.SpeedTestNET.SpeedTestNET.Instance.OnTestCompleted -= onTestCompleted;
        }

        #endregion


        #region Public methods

        public void OnClickSpeedInfoBtn()
        {
            speedCheckPanel.gameObject.SetActive(true);
            MainScenePanel.gameObject.SetActive(false);
            Result.text = "Please press the below button to test your network speed.";
        }

        public void OnClickSpeedExitBtn()
        {
            MainScenePanel.gameObject.SetActive(true);
            speedCheckPanel.gameObject.SetActive(false);
            if (isAutoSpeedCheck == true)
            {
                StopCoroutine(co);
            }
            isAutoSpeedCheck = false;
        }

        public void OnAutoSpeedCheckBtn()
        {
            isAutoSpeedCheck = true;
            isSpeedCheck = false;
            co = StartCoroutine(ResetSpeedTestCheck());
        }

        public void OnClickSpeedTestBtn()
        {
            if (Tool.SpeedTestNET.SpeedTestNET.Instance.isPlatformSupported)
            {
                Result.text = "<i>Please wait, while we test your speed...</i>";
                Tool.SpeedTestNET.SpeedTestNET.Instance.Test();
                CheckButton.gameObject.SetActive(false);
                BackButton.gameObject.SetActive(false);
                isSpeedCheck = true;
                TimerAutoCheck = 20.0f;
            }
            else
            {
                Result.text = "<color=red>Not supported under WSA and WebGL!</color>";
            }
        }

        #endregion


        #region Private methods

        private void onTestCompleted(Tool.SpeedTestNET.Model.Server server, double duration, double downloadspeed, double uploadspeed)
        {
            Result.text = $"<b>Speed Details</b> {System.Environment.NewLine}Download speed: <b>{Tool.SpeedTestNET.SpeedTestNET.Instance.LastDownloadSpeedMBps:N3} MBps</b> ({downloadspeed / 1000000:N3} Mbps){System.Environment.NewLine}Upload speed: <b>{Tool.SpeedTestNET.SpeedTestNET.Instance.LastUploadSpeedMBps:N3} MBps</b> ({uploadspeed / 1000000:N3} Mbps){System.Environment.NewLine}{server}{System.Environment.NewLine}";
            CheckButton.gameObject.SetActive(true);
            BackButton.gameObject.SetActive(true);
            isSpeedCheck = false;
            TimerAutoCheck = 20.0f;
        }

        private IEnumerator ResetSpeedTestCheck()
        {
            Debug.Log("SPEED TEST START");
            isAutoSpeedCheck = true;
            yield return new WaitForSeconds(0.1f);

            WaitForSeconds waitTime = new WaitForSeconds(TimerAutoCheck);
            while (true)
            {
                if (isSpeedCheck == false)
                {
                    Debug.Log("Testing The Speed.");
                    OnClickSpeedTestBtn();
                }
                yield return waitTime; // Wait till we get speed results.
            }
        }
        #endregion
#endif
    }
}