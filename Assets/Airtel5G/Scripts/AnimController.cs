using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimController : MonoBehaviour
{
    public Slider sliderScrubber;
    public Animator animator;
    bool isSliderPause = false;
    bool isPause=false;

    public GameObject playButton;
    public GameObject pauseButton;

    public void Update()
    {
        if (AnimatorIsPlaying()&&!isSliderPause && !isPause)
        {
            float animationTime = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;            
            sliderScrubber.value = animationTime;
        }
        else
        {
            animator.speed = 0;
        }

    }


    public void OnVideoSeekSlider()
    {
        float time = sliderScrubber.normalizedValue;
        animator.Play("Interaction3", -1, time);
    }

    public void OnVideoSliderDown()
    {
        if (!isSliderPause)
        {
            animator.speed = sliderScrubber.normalizedValue;
        }
        isSliderPause = true;
        OnVideoSeekSlider();

    }


    public void OnVideoSliderUp()
    {
        isSliderPause = false;
        OnVideoSeekSlider();
        animator.speed = 1;
    }

    bool AnimatorIsPlaying()
    {
        return animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1;
    }

    public void OnPlayButtonClick()
    {
        playButton.SetActive(false);
        pauseButton.SetActive(true);
        animator.speed = 1;
        isPause = false;
    }

    public void OnPauseButtonClick()
    {
        playButton.SetActive(true);
        pauseButton.SetActive(false);
        animator.speed = 0;
        isPause = true;
    }
}
