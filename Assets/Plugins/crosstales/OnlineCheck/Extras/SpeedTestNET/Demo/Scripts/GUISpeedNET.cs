﻿using UnityEngine;
using UnityEngine.UI;

namespace Crosstales.OnlineCheck.Demo
{
   /// <summary>GUI component for SpeedTestNET.</summary>
   public class GUISpeedNET : MonoBehaviour
   {
      #region Variables

      public Text Result;
      public Button CheckButton;

      #endregion

#if NET_4_6 || NET_STANDARD_2_0

      #region MonoBehaviour methods

      private void Start()
      {
         Tool.SpeedTestNET.SpeedTestNET.Instance.OnTestCompleted += onTestCompleted;

         if (Tool.SpeedTestNET.SpeedTestNET.Instance.LastServer != null)
            onTestCompleted(Tool.SpeedTestNET.SpeedTestNET.Instance.LastServer, Tool.SpeedTestNET.SpeedTestNET.Instance.LastDuration, Tool.SpeedTestNET.SpeedTestNET.Instance.LastDownloadSpeed, Tool.SpeedTestNET.SpeedTestNET.Instance.LastDownloadSpeed);
      }

      private void OnDestroy()
      {
         if (Tool.SpeedTestNET.SpeedTestNET.Instance != null)
            Tool.SpeedTestNET.SpeedTestNET.Instance.OnTestCompleted -= onTestCompleted;
      }

      #endregion


      #region Public methods

      public void Test()
      {
         if (Tool.SpeedTestNET.SpeedTestNET.Instance.isPlatformSupported)
         {
            Result.text = "<i>Please wait...</i>";
            Tool.SpeedTestNET.SpeedTestNET.Instance.Test();
         }
         else
         {
            Result.text = "<color=red>Not supported under WSA and WebGL!</color>";
         }
      }

      #endregion


      #region Private methods

      private void onTestCompleted(Tool.SpeedTestNET.Model.Server server, double duration, double downloadspeed, double uploadspeed)
      {
         Result.text = $"<b>Server</b>{System.Environment.NewLine}{server}{System.Environment.NewLine}Download speed: <b>{Tool.SpeedTestNET.SpeedTestNET.Instance.LastDownloadSpeedMBps:N3} MBps</b> ({downloadspeed / 1000000:N3} Mbps){System.Environment.NewLine}Upload speed: <b>{Tool.SpeedTestNET.SpeedTestNET.Instance.LastUploadSpeedMBps:N3} MBps</b> ({uploadspeed / 1000000:N3} Mbps){System.Environment.NewLine}Duration: <b>{duration:N3} seconds</b>";
      }

      #endregion

#endif
   }
}
// © 2020-2021 crosstales LLC (https://www.crosstales.com)