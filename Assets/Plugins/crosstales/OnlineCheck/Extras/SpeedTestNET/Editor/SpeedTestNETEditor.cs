﻿#if UNITY_EDITOR && (NET_4_6 || NET_STANDARD_2_0)
using UnityEngine;
using UnityEditor;

namespace Crosstales.OnlineCheck.EditorExtension
{
   /// <summary>Custom editor for the 'SpeedTestNET'-class.</summary>
   //[InitializeOnLoad]
   [CustomEditor(typeof(Tool.SpeedTestNET.SpeedTestNET))]
   public class SpeedTestNETEditor : Editor
   {
      #region Variables

      private Tool.SpeedTestNET.SpeedTestNET script;

      #endregion


      #region Editor methods

      private void OnEnable()
      {
         script = (Tool.SpeedTestNET.SpeedTestNET)target;
         EditorApplication.update += onUpdate;
      }

      private void OnDisable()
      {
         EditorApplication.update -= onUpdate;
      }

/*
        public override bool RequiresConstantRepaint()
        {
            return true;
        }
*/
      public override void OnInspectorGUI()
      {
         DrawDefaultInspector();

         EditorUtil.EditorHelper.SeparatorUI();

         if (script.isActiveAndEnabled)
         {
            GUILayout.Label("Speed Test Status", EditorStyles.boldLabel);

            if (script.LastDuration > 0)
            {
               GUILayout.Label("Server details:", EditorStyles.boldLabel);
               GUILayout.Label(Tool.SpeedTestNET.SpeedTestNET.Instance.LastServer.ToString());
               GUILayout.Label($"Download speed:\t{script.LastDownloadSpeedMBps:N3} MBps ({script.LastDownloadSpeed / 1000000:N3} Mbps)");
               GUILayout.Label($"Upload speed:{Util.Constants.TAB}{script.LastUploadSpeedMBps:N3} MBps ({script.LastUploadSpeed / 1000000:N3} Mbps)");
               GUILayout.Label($"Duration:\t\t{script.LastDuration:N3} seconds");
            }
            else
            {
               EditorGUILayout.HelpBox(script.isBusy ? "Testing the download speed, please wait..." : "Speed not tested.", MessageType.Info);
            }

            if (Util.Helper.isEditorMode)
            {
               GUI.enabled = !script.isBusy;

               if (GUILayout.Button(new GUIContent(" Refresh", EditorUtil.EditorHelper.Icon_Reset, "Restart the speed test.")))
               {
                  script.Test();
               }

               GUI.enabled = true;
            }
         }
         else
         {
            EditorGUILayout.HelpBox("Script is disabled!", MessageType.Info);
         }
      }

      #endregion


      #region Private methods

      private void onUpdate()
      {
         Repaint();
      }

      #endregion
   }
}
#endif
// © 2020-2021 crosstales LLC (https://www.crosstales.com)