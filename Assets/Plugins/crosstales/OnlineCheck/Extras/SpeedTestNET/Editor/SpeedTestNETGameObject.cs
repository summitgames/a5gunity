﻿#if UNITY_EDITOR
using UnityEditor;
using Crosstales.OnlineCheck.EditorUtil;

namespace Crosstales.OnlineCheck.EditorIntegration
{
   /// <summary>Editor component for the "Hierarchy"-menu.</summary>
   public static class SpeedTestNETGameObject
   {
      [MenuItem("GameObject/" + Util.Constants.ASSET_NAME + "/" + Util.Constants.SPEEDTESTNET_SCENE_OBJECT_NAME, false, EditorHelper.GO_ID + 4)]
      private static void AddSpeedTestNET()
      {
         EditorHelper.InstantiatePrefab(Util.Constants.SPEEDTESTNET_SCENE_OBJECT_NAME, $"{EditorConfig.ASSET_PATH}Extras/SpeedTestNET/Resources/Prefabs/");
      }

      [MenuItem("GameObject/" + Util.Constants.ASSET_NAME + "/" + Util.Constants.SPEEDTESTNET_SCENE_OBJECT_NAME, true)]
      private static bool AddSpeedTestNETValidator()
      {
         return !EditorHelper.isSpeedTestNETInScene;
      }
   }
}
#endif
// © 2020-2021 crosstales LLC (https://www.crosstales.com)