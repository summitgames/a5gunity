﻿#if PLAYMAKER && UNITY_EDITOR
using UnityEngine;
using HutongGames.PlayMaker.Actions;
using HutongGames.PlayMakerEditor;

namespace Crosstales.OnlineCheck.PlayMaker
{
   /// <summary>Custom editor for the Check-action.</summary>
   [CustomActionEditor(typeof(Check))]
   public class CheckEditor : CustomActionEditor
   {
      public override bool OnGUI()
      {
         bool isDirty = DrawDefaultInspector();

         if (!EditorUtil.EditorHelper.isOnlineCheckInScene)
         {
            EditorUtil.EditorHelper.OCUnavailable();
         }

         return isDirty || GUI.changed;
      }
   }
}
#endif
// © 2017-2021 crosstales LLC (https://www.crosstales.com)