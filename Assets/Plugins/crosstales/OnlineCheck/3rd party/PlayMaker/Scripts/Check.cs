﻿#if PLAYMAKER
namespace HutongGames.PlayMaker.Actions
{
   /// <summary>Check-action for Internet availability in PlayMaker.</summary>
   [HelpUrl("https://www.crosstales.com/media/data/assets/OnlineCheck/api/class_hutong_games_1_1_play_maker_1_1_actions_1_1_check.html")]
   [ActionCategory("Crosstales.OnlineCheck")]
   public class Check : BaseOCAction
   {
      #region Variables

      /// <summary>Minimum delay between checks in seconds (default: 4, range: 3 - 120).</summary>
      [Tooltip("Minimum delay between checks in seconds (default: 4, range: 3 - 120).")] [RequiredField]
      //[HasFloatSlider(3, 120)]
      public FsmInt IntervalMin = 4;

      /// <summary>Maximum delay between checks in seconds (default: 10, range: 4 - 120).</summary>
      [Tooltip("Maximum delay between checks in seconds (default: 10, range: 4 - 120).")] [RequiredField]
      //[HasFloatSlider(4, 120)]
      public FsmInt IntervalMax = 10;

      /// <summary>Timeout for every check in seconds (default: 2, range: 1 - 20).</summary>
      [Tooltip("Timeout for every check in seconds (default: 2, range: 1 - 20).")] [RequiredField]
      //[HasFloatSlider(4, 120)]
      public FsmInt Timeout = 2;

      /// <summary>Checks if an Internet connection is available (output variable).</summary>
      [Tooltip("Checks if an Internet connection is available (output variable).")] public FsmBool isInternetAvailable;

      #endregion


      #region Overridden methods

      public override void Awake()
      {
         Crosstales.OnlineCheck.OnlineCheck.Instance.IntervalMin = IntervalMin.Value;
         Crosstales.OnlineCheck.OnlineCheck.Instance.IntervalMax = IntervalMax.Value;
         Crosstales.OnlineCheck.OnlineCheck.Instance.Timeout = Timeout.Value;

         Crosstales.OnlineCheck.OnlineCheck.Instance.EndlessMode = true;
      }

      public override void OnEnter()
      {
         Crosstales.OnlineCheck.OnlineCheck.Instance.IntervalMin = IntervalMin.Value;
         Crosstales.OnlineCheck.OnlineCheck.Instance.IntervalMax = IntervalMax.Value;
         Crosstales.OnlineCheck.OnlineCheck.Instance.Timeout = Timeout.Value;

         isInternetAvailable.Value = Crosstales.OnlineCheck.OnlineCheck.Instance.isInternetAvailable;

         Fsm.Event(sendEvent);
         Finish();
      }

      #endregion
   }
}
#endif
// © 2017-2021 crosstales LLC (https://www.crosstales.com)