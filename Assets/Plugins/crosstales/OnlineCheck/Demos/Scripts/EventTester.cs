﻿using UnityEngine;

namespace Crosstales.OnlineCheck.Demo
{
   /// <summary>Simple test script for all UnityEvent-callbacks.</summary>
   [ExecuteInEditMode]
   [HelpURL("https://www.crosstales.com/media/data/assets/OnlineCheck/api/class_crosstales_1_1_online_check_1_1_demo_1_1_event_tester.html")]
   public class EventTester : MonoBehaviour
   {
      public void OnStatusChange(bool isConnected)
      {
         Debug.Log($"OnStatusChange: {isConnected}");
      }

      public void OnPingComplete(float time)
      {
         Debug.Log($"OnPingComplete: {time * 1000} milliseconds");
      }

      public void OnSpeedTestComplete(double duration, double speed)
      {
         Debug.Log($"OnSpeedTestComplete: {duration:N2} seconds - {Crosstales.OnlineCheck.Util.Helper.FormatBytesToHRF((long)speed / 8)} MBps (download)");
      }

      public void OnSpeedTestNETComplete(double duration, double speed, double uploadspeed)
      {
         Debug.Log($"OnSpeedTestNETComplete: {duration:N2} seconds - {Crosstales.OnlineCheck.Util.Helper.FormatBytesToHRF((long)speed / 8)} MBps (download) - {Crosstales.OnlineCheck.Util.Helper.FormatBytesToHRF((long)uploadspeed / 8)} MBps (upload)");
      }
   }
}
// © 2020-2021 crosstales LLC (https://www.crosstales.com)