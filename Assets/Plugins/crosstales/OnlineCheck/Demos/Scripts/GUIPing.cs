﻿using UnityEngine;
using UnityEngine.UI;

namespace Crosstales.OnlineCheck.Demo
{
   /// <summary>GUI component for PingCheck.</summary>
   public class GUIPing : MonoBehaviour
   {
      #region Variables

      public string Host = "google.com";
      public InputField Hostname;
      public Text Result;
      public Button CheckButton;

      #endregion


      #region MonoBehaviour methods

      private void Start()
      {
         Tool.PingCheck.Instance.OnPingCompleted += onPingCompleted;

         Hostname.text = Host;

         if (!string.IsNullOrEmpty(Tool.PingCheck.Instance.LastIP))
            onPingCompleted(Tool.PingCheck.Instance.LastHost, Tool.PingCheck.Instance.LastIP, Tool.PingCheck.Instance.LastPingTime);
      }

      private void OnDestroy()
      {
         if (Tool.PingCheck.Instance != null)
            Tool.PingCheck.Instance.OnPingCompleted -= onPingCompleted;
      }

      #endregion


      #region Public methods

      public void Ping()
      {
         if (Tool.PingCheck.Instance.isPlatformSupported)
         {
            if (string.IsNullOrEmpty(Hostname.text))
            {
               Result.text = "<color=red>Please enter a valid hostname like <i>google.com</i></color>";
            }
            else
            {
               Result.text = "<i>Please wait...</i>";
               Tool.PingCheck.Instance.Ping(Hostname.text);
            }
         }
         else
         {
            Result.text = "<color=red>Not supported under WebGL!</color>";
         }
      }

      #endregion


      #region Private methods

      private void onPingCompleted(string host, string ip, float time)
      {
         Result.text = $"{host} ({ip}){System.Environment.NewLine}Round-trip-time: <b>{time * 1000:N0} ms</b>";
      }

      #endregion
   }
}
// © 2020-2021 crosstales LLC (https://www.crosstales.com)