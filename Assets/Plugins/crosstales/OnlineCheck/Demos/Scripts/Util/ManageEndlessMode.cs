﻿using UnityEngine;

namespace Crosstales.OnlineCheck.Demo.Util
{
   /// <summary>Enable or disable EndlessMode at startup.</summary>
   [HelpURL("https://crosstales.com/media/data/assets/OnlineCheck/api/class_crosstales_1_1_online_check_1_1_demo_1_1_util_1_1_manage_endless_mode.html")]
   public class ManageEndlessMode : MonoBehaviour
   {
      public bool EndlessMode;

      public void Start()
      {
         OnlineCheck.Instance.EndlessMode = EndlessMode;
         //Debug.Log("ManageEndlessMode: " + EndlessMode);
      }
   }
}
// © 2017-2021 crosstales LLC (https://www.crosstales.com)