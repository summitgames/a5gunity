﻿using Crosstales.OnlineCheck.Util;
using UnityEngine;
using UnityEngine.UI;

namespace Crosstales.OnlineCheck.Demo
{
   /// <summary>GUI component for NetworkInfo.</summary>
   public class GUINetworkInfo : MonoBehaviour
   {
      #region Variables

      public Text Result;

      #endregion


      #region MonoBehaviour methods

      private void Start()
      {
         Refresh();
      }

      #endregion


      #region Public methods

      public void Refresh()
      {
         Result.text = NetworkInfo.isPlatformSupported ? $"Public IP: <b>{NetworkInfo.PublicIP}</b>{System.Environment.NewLine}{System.Environment.NewLine}All Network Interfaces: {System.Environment.NewLine}{NetworkInfo.getNetworkInterfaces(false).CTDump()}" : "<color=red>Not supported under WSA and WebGL!</color>";
      }

      #endregion
   }
}
// © 2020-2021 crosstales LLC (https://www.crosstales.com)