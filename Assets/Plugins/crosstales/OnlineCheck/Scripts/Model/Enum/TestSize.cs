﻿namespace Crosstales.OnlineCheck.Model.Enum
{
   /// <summary>The file sizes for SpeedTest.</summary>
   public enum TestSize
   {
      SMALL,
      MEDIUM,
      LARGE
   }
}
// © 2020-2021 crosstales LLC (https://www.crosstales.com)